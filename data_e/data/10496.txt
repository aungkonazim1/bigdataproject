August 2016 Graduation Address by Dr. Roy B. Van Arsdale - Commencement Office - University of Memphis    










 
 
 
     



 
    
    
    August 2016 Graduation Address by Dr. Roy B. Van Arsdale - 
      	Commencement Office
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Commencement Office
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Apply to Graduate  
                          Graduates  
                          Accommodations  
                          Diplomas  
                          Guests  
                          Faculty  
                      
                      
                         
                            August 2016 Commencement   
                            
                               
                                  
                                   Overview  
                                   Dr. Roy B. Van Arsdale  
                                        
                                         About Dr. Roy Van Arsdale  
                                        
                                         Read Address to Graduates  
                                        
                                         Watch Address to Graduates  
                                     
                                  
                                   Photo Album  
                                   View Commencement Program  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Commencement Office
                              	  
                          
                              	August 2016 Commencement
                              	  
                         August 2016 Graduation Address by Dr. Roy B. Van Arsdale 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      August 2016 Graduation Address by Dr. Roy B. Van Arsdale  
                     
                      I want to acknowledge and thank President Rudd and all the distinguished persons on
                        the stage, students receiving degrees, and family and friends of the students.
                      
                     
                      Looking over this audience reminds me of all the courses I have taken and some that
                        I wish I had taken – like a Speech Class.
                      
                     
                      To give you a perspective of my early life I will just relay a personal experience.
                        When visiting my high school wrestling coach at Bernardsville High School in New Jersey
                        a number of years ago he introduced me to his school principal as having been the
                        poorest student to have graduated from Bernardsville High School. I was stunned, but
                        after clarifying that he meant financially, I felt somewhat better. My purpose for
                        telling you this is to assure you that poverty may be an impediment but it is not
                        a barrier to success.
                      
                     
                      I see this commencement speech as my opportunity to relay some life lessons pressed
                        on me over the past 65 years. This moment is similar to an experience that I had when
                        I was a new hire with Union Carbide Corporation. I was headed to Brazil and was scheduled
                        to arrive at the Rio de Janeiro airport when the day before leaving an elderly Vice
                        President of the company pulled me aside for some unsolicited advice. He told me how
                        to get out of the Rio airport after my arrival. I would still be trying to get out
                        of that airport if he did not talk to me. Hopefully, my speech today will provide
                        some Rio advice.
                      
                     
                       Title of Commencement Speech: Two sides of the fence.  
                     
                      You have spent at least 16 years on the student side of the fence. On that side you
                        started with learning life's lessons at home and in school. For example:
                      
                     
                      You learned how to take care of yourself - from learning how to tie your shoes to
                        how to put on a cap and gown. Learning morals and the importance of integrity. Developing a work ethic – arriving on time and doing your best. Being respectful of others. Achieving self discipline. Developing your ability to focus.
                      
                     
                      Let me bring in the future college students in the audience by letting them know that
                        almost every student during the course of their college career has a bad semester.
                        Mine was my second semester of my freshman year at Rutgers University. I was emotionally
                        very down and had started to skip classes. During one of these skips my friend Jerry
                        asked me if I wanted to explore a cave he knew in upstate New Jersey. I got into his
                        car and then realized that I had not put on my lucky socks and so I ran back to my
                        room and switched into them. After entering the muddy narrow cave, somehow I got into
                        the lead. Slithering along on my stomach, my hand slipped and I started to slide head
                        first vertically down a rocky shaft to certain death. As I slid, my right foot flew
                        up into the air and got caught in the ceiling. Jerry grabbed me by my lucky sock and
                        pulled me out of the shaft.
                      
                     
                      My life lessons to you here are; 1) you should never skip classes, 2) when skipping
                        classes you may be putting your life at risk, and 3) if you do skip a class and do
                        something dangerous, make sure you are wearing your lucky socks.
                      
                     
                      You will continue to have bad times in life. Stay focused on your life goals and your
                        particular interests. As life's bad experiences beat on me, my refuge was my love
                        of nature. As a child I spent time hiking in woods and as an adult I immerse myself
                        in geology trips/reading/and conducting research. You should have emotionally healthy
                        places to go in your mind when times get tough.
                      
                     
                      Most of you students are today actually straddling the fence between being a student
                        and being a professional. This is an awkward position with one leg on either side
                        of the barbed wire. Questions abound in your minds.
                      
                     
                      Am I ready for the change of life style from a student to an 8-5 work day? I'm heading to graduate school, medical school, or law school - what will that be
                        like?
                      
                     
                      Graduation from college was an insecure time for me. I was suddenly out of the structured
                        academic life style and I was also academically out of gas. Fortunately, I had 8 months
                        before I started a Master of Science program in geology at the University of Cincinnati.
                        I needed that time to recharge my academic batteries.
                      
                     
                      One of my Cincinnati classmates was an older man who had retired from the oil business
                        and was pursuing a PhD in geology. His name was E.J. Webb and he looked and acted
                        quite a bit like Santa Claus. At one point the geology department at Cincinnati was
                        interviewing candidates for a professor position and wanted input from the graduate
                        students. I made the comment that we were graduating so what difference did it make
                        what our opinion was. Santa Claus suddenly changed to a very large angry man. He proceeded
                        to sternly explain that it is critical that the department hire the best person they
                        can so that the prestige of the department will continue to grow. EJ pointed out that
                        as the academic prestige of the department and university grows so does the value
                        of our degree.
                      
                     
                      This truth has stayed with me and helps me maintain rigor in the class room. It is
                        critical that we the faculty, maintain academic rigor at the University of Memphis
                        to maintain and improve the value of your degree in the years ahead. You graduates
                        can help this critical objective by staying involved in the academics of the university
                        after graduation through oversight committees, funding, positive academic pressure
                        on the University of Memphis administration, and providing internships.
                      
                     
                      After graduating from the University of Cincinnati my department chairman and academic
                        adviser Dr. Lattman helped me obtain a summer job with Chevron Oil Company in California.
                        At the end of the summer I returned to Cincinnati to undertake a PhD degree but had
                        lost my desire for academics and dropped out of school.
                      
                     
                      After floundering along for several weeks I received a call from Dr. Lattman asking
                        me if I wanted a job with Union Carbide Corporation of New York City to work in international
                        minerals exploration. Fortunately, I was living at home in New Jersey but unfortunately
                        I was broke so I had to borrow money from my younger brother for a sports coat to
                        wear to the interview. That was another of many humbling experiences in my life.
                      
                     
                      The end of my job interview was on the 48th floor of the Union Carbide building on
                        Park Avenue in Manhattan with the head honcho – the president of the Minerals Division
                        of the company. With a rather unpleasant grin he pulled out a box of minerals and
                        selected a specimen and asked me to identify it. Mineral identification is not one
                        of my strengths and so there I was faced with ruin. At that moment something took
                        over me and calmly informed me that this was an unusual variety of magnetite (a magnetic
                        mineral) and upon bouncing the specimen up and down in my hand I declared that it
                        was magnetite that contained a high percentage of some additional high density element.
                        Well, it was titanium rich magnetite and where my statement came from, to this day,
                        I have no idea, but I am very grateful for these cosmic moments that have occurred
                        in my life. His jaw dropped, I got the job, and I was off to South Africa, then Brazil,
                        and finally Australia. After two years I returned to graduate school at the University
                        of Utah for my PhD.
                      
                     
                      Upon graduating from the University of Utah I interviewed for a geologist position
                        with a major oil company. During the course of the interview he asked me why my chemistry
                        grade plummeted from my first semester to my second semester of my freshman year.
                        This interview taught me that grades are important. I accepted the job but then turned
                        it down when I got a telephone call from them asking me if I would spend my summers
                        doing field work in Alaska. As a single man I would have jumped at it, but by this
                        time I was married to Stephanie and we had a new baby. Family decisions are critical
                        to your happiness and success. Without happiness how can we be successful?
                      
                     
                      In the meantime a friend of mine in Kentucky had passed my name on to the chairman
                        of the geology department of Eastern Kentucky University and I was contacted about
                        a faculty position at EKU. The interview went well and I took the position. At some
                        point during my five years at EKU a colleague in the department told me that a major
                        reason he wanted to hire me was because the search committee received very positive
                        letters of reference on my behalf. This reinforced in me the importance of professional
                        connections and having well respected referees.
                      
                     
                      The chairman at EKU moved on to be the chairman of the geology department at the University
                        of Arkansas at Fayetteville and within a year asked if I would join him, which I did.
                        Things went well at Arkansas and after nine years I got a call from Dr. Arch Johnston
                        at the University of Memphis Center for Earthquake Research and Information. He asked
                        if I would be interested in taking a faculty position at the University of Memphis
                        in the department of geology, which I accepted. This move brought me to the Mississippi
                        River valley and allowed me to focus on the geologic history of the region and the
                        earthquakes of the New Madrid seismic zone. I relay this part of my life's story to
                        again illustrate the importance of professional relationships and point out that flexibility
                        has been helpful.
                      
                     
                      Let me summarize my speech with some suggested rules to live by. 
                     
                      
                        
                         Follow the rules of behavior you learned at home and in school. 
                        
                         Do not skip classes. Bad things happen when you do. 
                        
                         Life has bad semesters. Develop skills and interests to survive those difficult times. 
                        
                         Cultivate and maintain professional contacts. 
                        
                         Demand academic rigor at the University of Memphis now and in the future. The value
                           of your diploma and your children's diplomas depends on it.
                         
                        
                      
                     
                      I wish you well, suggest that you heed Rio advice, and I hope that your life is full
                        of cosmic moments.
                      
                     
                      Let me close by acknowledging my loving family here in the audience: my wife of 38
                        years Stephanie, my son Christopher and daughter Erica (both University of Memphis
                        graduates), and my grandchildren Jacob and Olivia.
                      
                     
                        
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         August 2016 Commencement 
                         
                            
                               
                                Overview  
                                Dr. Roy B. Van Arsdale  
                                     
                                      About Dr. Roy Van Arsdale  
                                     
                                      Read Address to Graduates  
                                     
                                      Watch Address to Graduates  
                                  
                               
                                Photo Album  
                                View Commencement Program  
                            
                         
                      
                      
                      
                         
                            
                                Contact Us  
                               Got questions? Let us know how we may assist you. 
                            
                            
                                Commencement Attendance Form  
                               Will you be attending Commencement?  Inform us! 
                            
                            
                                News  
                               Learn about possible date changes, future and past Commencements, Honors Assembly
                                 and more.
                               
                            
                            
                                Commencement Live  
                               Can't attend Commencement?  Watch it live! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


