Degrees - ICL - University of Memphis    










 
 
 
     



 
    
    
    Degrees - 
      	ICL
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Instruction and Curriculum Leadership 
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Programs  
                          Students  
                          Faculty  
                          ICL Faculty Research  
                          A-Z List  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	ICL
                              	  
                         Degrees 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      ICL Degrees 
                     
                      Bachelor of Science in Education (BSEd) 
                     
                      Major: Human Development and Learning 
                     
                      
                        
                         Concentration: None 
                        
                         TN Teacher License: Early Childhood Education (PreK-3) 
                        
                          Degree Sheet  
                        
                      
                     
                      Major: Integrative Studies  
                     
                      
                        
                         Concentration
                           
                            
                              
                               Youth Services (Non-Licensure)
                                 
                                  
                                    
                                      Degree Sheet  
                                    
                                  
                                 
                               
                              
                                Elementary 
                                 
                                  
                                    
                                     TN Teacher License: Middle Schools (4-8) 
                                    
                                      Degree Sheet  
                                    
                                  
                                 
                               
                              
                            
                           
                         
                        
                      
                     
                      Major: Teaching All Learners 
                     
                      
                        
                         Concentration: None 
                        
                         TN Teacher License: Elementary Education (K-6) and/or Special Education Modified (K-12) 
                        
                          Degree Sheet  
                        
                      
                     
                      Master of Science (MS) 
                     
                      Major: Instruction and Curriculum Leadership 
                     
                      
                        
                         Concentration 
                           
                            
                              
                               Early Childhood Education
                                 
                                  
                                    
                                      Degree Sheet  
                                    
                                  
                                 
                               
                              
                                Instruction and Curriculum Leadership  
                                 
                                  
                                    
                                       Degree Sheet   
                                    
                                  
                                 
                               
                              
                                Instructional Design   Technology 
                                 
                                  
                                    
                                       Degree Sheet   
                                    
                                  
                                 
                               
                              
                                Reading 
                                 
                                  
                                    
                                       Degree Sheet   
                                    
                                  
                                 
                               
                              
                                Special Education 
                                 
                                  
                                    
                                       Degree Sheet   
                                    
                                  
                                 
                               
                              
                            
                           
                         
                        
                      
                     
                       Major: School Library Information Specialist  
                     
                      
                        
                          Concentration: None 
                           
                            
                              
                                 Degree Sheet   
                              
                            
                           
                         
                        
                      
                     
                      Master of Arts in Teaching (MAT) 
                     
                      Major: Instruction and Curriculum Leadership 
                     
                      
                        
                         Concentration
                           
                            
                              
                               Early Childhood Education
                                 
                                  
                                    
                                     TN Teacher License: Early Childhood Education (PreK-3) 
                                    
                                      Degree Sheet  
                                    
                                  
                                 
                               
                              
                               Elementary Education
                                 
                                  
                                    
                                     TN Teacher License: Elementary Education (K-6) 
                                    
                                      Degree Sheet  
                                    
                                  
                                 
                               
                              
                               Secondary Education
                                 
                                  
                                    
                                     TN Teacher License: Biology, Chemistry, Earth Science, Economics, English, ESL, French,
                                       German, Government, History, Latin, Mathematics, Physics, Russian, Sociology, Spanish
                                     
                                    
                                      Degree Sheet  
                                    
                                  
                                 
                               
                              
                               Special Education
                                 
                                  
                                    
                                     TN Teacher License: Special Education Early Childhood (PreK-3), Special Education
                                       Modified (K-12), Special Education Comprehensive
                                     
                                    
                                      Degree Sheet  
                                    
                                  
                                 
                               
                              
                            
                           
                         
                        
                      
                     
                      DOCTOR OF EDUCATION (EDD) 
                     
                      Major: Instruction and Curriculum Leadership 
                     
                      
                        
                         Concentration
                           
                            
                              
                               Early Childhood Education
                                 
                                  
                                    
                                      Degree Sheet  
                                    
                                  
                                 
                               
                              
                               Instruction and Curriculum
                                 
                                  
                                    
                                      Degree Sheet  
                                    
                                  
                                 
                               
                              
                               Instructional Design   Technology
                                 
                                  
                                    
                                      Degree Sheet  
                                    
                                  
                                 
                               
                              
                               Reading
                                 
                                  
                                    
                                      Degree Sheet  
                                    
                                  
                                 
                               
                              
                               Special Education
                                 
                                  
                                    
                                      Degree Sheet  
                                    
                                  
                                 
                               
                              
                            
                           
                         
                        
                      
                     
                      Certificate Programs 
                     
                      Instruction and Curriculum 
                     
                      
                        
                          Urban Education  
                        
                          School Library Information Specialist  
                        
                          STEM Teacher Leadership  
                        
                      
                     
                      Instructional Design   Technology 
                     
                      
                        
                          Instructional Design and Technology  
                        
                      
                     
                      Reading 
                     
                      
                        
                          Literacy, Leadership, and Coaching Certificate  
                        
                      
                     
                      Special Education 
                     
                      
                        
                          Autism Studies  
                        
                      
                     
                        
                     
                       * Licenses are issued by the state - they do not come with a degree. The degree program
                           prepares the student to pass the   Praxis tests   required by the state of Tennessee to earn an initial teacher license.  
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                ICL Degrees  
                               Broaden your career choices by gaining the confidence you to need to succeed 
                            
                            
                                Apply Now  
                               Every journey starts with a step, now take yours! 
                            
                            
                                Advising  
                               Your academic success begins with advising. 
                            
                            
                                Contact Us  
                               Questions? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


