Loading a presentation from revision history in the Producer tool | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Loading a presentation from revision history in the Producer tool | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Capture    »    Editing in post-production   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Loading a presentation from revision history in the Producer tool                       
        
        
       
        
     
              
	 Important The  Revision History  drop-down list indicates the date and time of previously saved versions of the presentation you are editing. The most current saved file opens the next time you access Producer, but it will not appear in the revision history until your next Save.
 

 
	Load a presentation from revision history
 

  
		Select a date and time stamp of a revision from the   Revision History  drop-down list.
	 
	 
		Click  Load .
	 
      Audience:     Instructor       

    
           

                   ‹ Accessing Producer 
        
                   up 
        
                   Editing audio and video › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Capture  
  
      Participating in CaptureCast presentations    Understanding Capture components    Using Capture    Capture Station    Editing in post-production    Understanding the Producer tool    Accessing Producer    Loading a presentation from revision history in the Producer tool    Editing audio and video    Editing slides    Editing chapters    Adding captions    Adding closed captions with the Producer tool    Requesting captions from CaptionSync    Creating and adding SRT files      Understanding Capture Toolbox    Capture Central in Learning Environment    
                  
           
         

       
     

    
    
    
   
 
   
 
