Editing rubrics, achievement levels, criteria, or criteria groups | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Editing rubrics, achievement levels, criteria, or criteria groups  | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Rubrics    »    Rubrics basics   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Editing rubrics, achievement levels, criteria, or criteria groups                        
        
        
       
          
     
           Printer-friendly version       
	You can only edit a rubric from the org unit in which it was created and you cannot edit a rubric that is being used by a Competencies activity or ePortfolio item.
 

 
	Edit rubric properties
 

  
		On the Rubrics page, click on the rubric you want to edit.
	 
	 
		Make your changes.
	 
	 
		Click  Save .
	 
  
	Edit a level, criterion, or criteria group
 

  
		On the Rubrics page, click    Edit Levels  or    Edit Levels and Criteria  from the context menu of the rubric you want to modify.
	 
	 
		Click    Edit Level ,    Edit Criterion , or    Edit Criteria Group  from the context menu of the level, criterion or criteria group you want to edit.
	 
	 
		Make your changes.
	 
	 
		Click  Save .
	 
      Audience:    Instructor      

    
           

                   ‹ Adding rubric achievement levels , criteria, or criteria groups 
        
                   up 
        
                   Managing rubric sharing properties › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Rubrics  
  
      Rubrics basics    Understanding Rubrics    Accessing Rubrics    Creating holistic rubrics    Creating analytic rubrics    Adding rubric achievement levels , criteria, or criteria groups    Editing rubrics, achievement levels, criteria, or criteria groups     Managing rubric sharing properties    Managing rubric status settings    Copying rubrics    Reordering rubric achievement levels or criteria    Deleting rubrics    Deleting rubric achievement levels, criteria, or criteria groups    Viewing rubric statistics      Using Rubrics in Competencies    Using Rubrics in ePortfolio    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
