Copy a quiz with Question Library associations to another org unit | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 Copy a quiz with Question Library associations to another org unit | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   English  
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Quizzes    »    Quizzes and Question Library   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Copy a quiz with Question Library associations to another org unit                       
        
        
       
          
     
           Printer-friendly version        
		In the course offering that you want to copy the quiz to, on the navbar, click  Edit Course  >  Import/Export/Copy Components .
	 
	 
		Select  Copy Components from another Org Unit .
	 
	 
		Click  Search for offering .
	 
	 
		Locate and select the course containing the quiz you want to copy.
	 
	 
		Click  Add Selected  >  Select Components .
	 
	 
		Select  Question Library  and  Quizzes , then do one of the following:
		  
				To include all items, select  Copy all items , then click  Continue .
			 
			 
				To include only some items, select  Select individual items to copy , then click  Continue . On the  Select Items to Copy  page, select the items you want to copy. Click  Continue .
			 
		  
	 
		Click  Finish .
	 
      Audience:    Instructor      
    
         
               ‹ Delete a question pulled from Question Library in a quiz 
                     up 
                 
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Quizzes  
  
      Quizzes basics    Using Quizzes    Managing quiz questions and sections    Viewing quizzes    Quizzes and Question Library     Considerations around quizzes with associations to Question Library    Edit a question pulled from Question Library in a quiz    Delete a question pulled from Question Library in a quiz    Copy a quiz with Question Library associations to another org unit      
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
