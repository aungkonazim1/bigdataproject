Degree Sheets - CAS - University of Memphis    










 
 
 
     



 
    
    
    Degree Sheets - 
      	CAS
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			College of Arts   Sciences
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Areas of Study  
                          Research  
                          Scholarships  
                          Students  
                          Resources  
                          News  
                      
                      
                         
                            Advising Links   
                            
                               
                                  
                                   Advising Center Staff   Contact  
                                   Guide for Advisors  
                                   Newsletter for Advisors  
                                   Departments  
                                   Finding Your Advisor  
                                   First Year Students  
                                   Graduation/Degree Certification  
                                   Major and Minor Information  
                                   Major Four Year Plans  
                                   Meeting with your Advisor  
                                   Pre-Professional Advising  
                                   Upper Division Humanities  
                                   Upper Division Social Science  
                                   Withdrawal  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	CAS
                              	  
                          
                              	Advising
                              	  
                         Degree Sheets 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Degree Sheets 
                     
                      The College of Arts and Sciences has designed degree sheets to assist students and
                        advisors in determining courses remaining for graduation. Each department is encouraged
                        to create a degree sheet for their majors with General Education, degree requirements,
                        and major information. Students may also use the appropriate degree sheet below based
                        on their degree and catalog.
                      
                     
                      Each student is assigned to the catalog available upon admission, which is available
                        for 7 years. Sometimes, programs or majors change and a newer catalog might be more
                        advantageous for a specific student. Students should discuss the appropriate catalog
                        with their advisors.
                      
                     
                      
                     
                      
                        
                         
                           
                            
                              
                                2017 Fall B.A. Degree Sheet     2017 Fall B.S. Degree Sheet   2016 Fall B.A. Degree Sheet  
                              
                            
                           
                            
                              
                                2016 Fall B.S. Degree Sheet  
                              
                            
                           
                            
                              
                                2015 Fall B.A. Degree Sheet   2015 Fall B.S. Degree Sheet  
                              
                            
                           
                            
                              
                                2014 Fall B.A. Degree Sheet    2014 Fall B.S. Degree Sheet  
                              
                            
                           
                            
                              
                                2013 Fall B.A. Degree Sheet    2013 Fall B.S. Degree Sheet  
                              
                            
                           
                            
                              
                                2012 Fall B.A. Degree Sheet   2012 Fall B.S. Degree Sheet  
                              
                            
                           
                            
                              
                                2011 Fall B.A. Degree Sheet   2011 Fall B.S. Degree Sheet  
                              
                            
                           
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Advising Links 
                         
                            
                               
                                Advising Center Staff   Contact  
                                Guide for Advisors  
                                Newsletter for Advisors  
                                Departments  
                                Finding Your Advisor  
                                First Year Students  
                                Graduation/Degree Certification  
                                Major and Minor Information  
                                Major Four Year Plans  
                                Meeting with your Advisor  
                                Pre-Professional Advising  
                                Upper Division Humanities  
                                Upper Division Social Science  
                                Withdrawal  
                            
                         
                      
                      
                      
                         
                            
                                Apply now for an Undergraduate Degree  
                               Pursue your dream, broaden your career choices and gain the confidence you need to
                                 succeed
                               
                            
                            
                                Offering Master's and Doctoral Degrees  
                               Enhance your knowledge, skills and experience 
                            
                            
                                Community Engagement  
                               Teaching and serving the Memphis and Mid-South community through programs and events 
                            
                            
                                Contact Us  
                               Dean's Office Contacts, Advising, Department Chairs... 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


