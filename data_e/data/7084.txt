Exporting presentations in HTML format | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Exporting presentations in HTML format | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    ePortfolio    »    Importing and exporting items   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Exporting presentations in HTML format                       
        
        
       
          
     
           Printer-friendly version       
	If you have the appropriate permissions, you can export presentations to HTML format directly from a presentation's context menu.
 

 
	Exporting your presentation to HTML format enables you to keep a copy of your presentation in a viewable format external to the system. This is useful if you change institutions and you don't want to lose your work. Once you export your presentation to HTML, you can host it externally on your own website or save it on your computer to view offline.
 

 
	 Note  You cannot import an HTML presentation back into ePortfolio.
 

 
	Export your portfolio to HTML
 

  
		Click   ​ Export  from the context menu of the presentation you want to export.
	 
	 
		Choose  Export HTML version of presentation .
	 
	 
		If you want your HTML presentation to include links to pages in the system, select  All system links.  Links to pages within the system require users to log in to the system and have the appropriate permissions.
	 
	 
		Click  Next .
	 
	 
		Click the link for your export file when the file packaging is complete.
		 
			 Tip  If you want to navigate away from this page or close the window, you can retrieve your export package from the Import/Export area in the Recent Activity list.
		 
	 
	 
		Save the export file to your computer or storage device.
	 
  
	Changes to presentation properties during HTML export
 

    
				Property
			 
			 
				Notes
			 
		   
				 
					Action bar
				 
			 
			 
				 
					Does not include the action bar at the top of a presentation
				 
			 
		   
				 
					Artifacts
				 
			 
			 
				 
					Includes embedded artifacts and links to artifacts
				 

				 
					Embedded image artifacts retain their alt tags
				 

				 
					Embedded image and media artifacts retain their size settings
				 

				 
					Artifacts retain their type icon if displayed as a link inline
				 

				 
					Artifacts retain their title and description set using Edit Display Options
				 
			 
		   
				 
					Assessments
				 
			 
			 
				 
					Includes assessments made on items within the presentation
				 

				 
					Does not include assessments made on the presentation
				 
			 
		   
				 
					Comments
				 
			 
			 
				 
					Includes all comments for artifacts and reflections
				 

				 
					Does not include comments made on the presentation
				 
			 
		   
				 
					Forms
				 
			 
			 
				 
					Does not include forms linked via a quicklink
				 
			 
		   
				 
					Learning Objectives
				 
			 
			 
				 
					Includes embedded learning objectives and links to learning objectives
				 
			 
		   
				 
					Permissions
				 
			 
			 
				 
					Does not retain permissions; anyone can view content within an HTML presentation
				 
			 
		   
				 
					Presentation layout
				 
			 
			 
				 
					Page links, navigation settings, page settings, page names, and text areas remain the same
				 
			 
		   
				 
					Presentation theme
				 
			 
			 
				 
					Presentation themes retain their style settings, including custom replaced images
				 
			 
		   
				 
					Profile details
				 
			 
			 
				 
					Profile information exports as displayed in the presentation, including customized profile titles and social media icons
				 

				 
					Only the fields set to display are exported; hidden profile information does not export with the presentation
				 
			 
		   
				 
					Quicklinks
				 
			 
			 
				 
					All quicklinks retain their links to external URLs and artifacts stored within the export package
				 

				 
					Quicklinks to system resources require users to log in to the system and have the appropriate permissions
				 
			 
		   
				 
					Reflections
				 
			 
			 
				 
					Includes embedded reflections and links to reflections
				 

				 
					Does not include reflections associated with the presentation
				 
			 
		   
				 
					Social media
				 
			 
			 
				 
					Does not include social media icons from the top of public presentations
				 
			 
		   
				 
					Text areas
				 
			 
			 
				 
					Includes all text formatting, equations and custom characters
				 
			 
		        Audience:    Learner      

    
           

                   ‹ Exporting ePortfolio items 
        
                   up 
        
                   Understanding the changes made to exported item properties › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        ePortfolio  
  
      ePortfolio basics    Using artifacts    Creating and editing presentations    Creating collections    Managing comments and assessments in ePortfolio    Understanding the basic concepts in sharing    Importing and exporting items    Accessing the Import and Export tool    Importing ePortfolio items    Exporting ePortfolio items    Exporting presentations in HTML format    Understanding the changes made to exported item properties      
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
