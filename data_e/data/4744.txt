German Program - WLL - University of Memphis    










 
 
 
     



 
    
    
    German Program  - 
      	WLL
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of World Languages and Literatures
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          Languages  
                          People  
                          Study Abroad  
                          Scholarships  
                      
                      
                         
                            About the German Program   
                            
                               
                                  
                                   About  
                                   Why Study German  
                                   Program  
                                   Courses   Syllabi  
                                   Study Abroad  
                                   Activities  
                                   Tutoring  
                                   Faculty  
                                         Robert Kelz  
                                         Monika Nenon  
                                         Heike Polster  
                                     
                                  
                                   Contact  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	WLL
                              	  
                          
                              	German
                              	  
                         German Program  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      The German Program 
                     
                      Requirements for a Major in German 
                     
                       Students wishing to major in German must take twenty-four upper-division hours, that
                        is, eight courses in German at the 3000 or 4000 level. Prerequisite for upper-division
                        courses is the successful completion of GERM 2020.  All German majors should take GERM 3301 and 3302 (Conversation   Composition), GERM
                        3303 (German Translation) and GERM 3411 and 3412 (Introduction to German Literature
                        and Culture) at their earliest opportunity. Though offered at regular intervals, these
                        courses are not available every semester.  Students who are planning to teach German should consult with the German section head
                        and with the School of Education concerning courses which will meet the requirements
                        for secondary certification.  Students who wish to use their language skills in business should take GERM 3791 and
                        3792 (Business German).
                      
                     
                      The remainder of the major will consist of 4000-level German literature and film courses. 
                     
                      Requirements for a Minor in German 
                     
                      Students are required to take eighteen hours of German, including nine upper-division
                        hours.
                      
                     
                      It is highly recommended that students take one semester of Conversation and Composition
                        (GERM 3301 or 3302), one semester of Introduction to German Literature and Culture
                        (GERM 3411 or 3412), plus one additional 3000- or 4000 level course.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         About the German Program 
                         
                            
                               
                                About  
                                Why Study German  
                                Program  
                                Courses   Syllabi  
                                Study Abroad  
                                Activities  
                                Tutoring  
                                Faculty  
                                      Robert Kelz  
                                      Monika Nenon  
                                      Heike Polster  
                                  
                               
                                Contact  
                            
                         
                      
                      
                      
                         
                            
                                Events  
                               Including Language Fair, Film Festivals, Lectures, and other activities 
                            
                            
                                Student Help  
                               Language Media Center, Language Tables, Placement Testing and Tutoring 
                            
                            
                                Newsletter  
                               Get the News from World Languages and Literatures 
                            
                            
                                Contact Us!  
                               Email, location and hours for the main office. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/30/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


