James E. Jamison - Math Sciences - University of Memphis    










 
 
 
     



 
    
    
    James E. Jamison - 
      	Math Sciences
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Mathematical Sciences
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          People  
                          Undergraduate  
                          Graduate  
                          Research  
                          News  
                      
                      
                         
                            People   
                            
                               
                                  
                                   Research Faculty  
                                   Instructors  
                                   Visitors   Postdocs  
                                   Graduate Students  
                                   Staff  
                                   A-Z List  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Math Sciences
                              	  
                          
                              	People
                              	  
                         James E. Jamison 
                      
                   
                   
                       
                      
                     
                      
                     
                      
                        
                         
                           
                            
                              
                              
                              
                              
                               
                              
                              
                            
                           
                            
                              
                               
                                 
                                 
                                 
                                 James E. Jamison
                                 
                               
                              
                               
                                 
                                 
                                 
                                 Professor
                                 
                               
                              
                               
                                 
                                 
                                 
                                 
                                 
                                   Publications  
                                 
                                   Curriculum Vitae  
                                 
                                 
                               
                            
                         
                        
                        
                        
                        
                        
                         Dr. James Jamison (June 9, 1943 - November 28, 2014) 
                        
                        
                         It is with great sadness that the Department of Mathematical Sciences announces that
                           
                           Professor James Jamison  passed away on November 28, 2014. With strong determination
                           and resilience he fought a very difficult and heroic fight against cancer that lasted
                           for more than fifteen years. Nevertheless, during those years, he has continued a
                           very active and productive professional life both as a beloved and sought-after teacher
                           and mentor and a highly recognized and respected researcher in operator theory and
                           functional analysis; while greatly contributing as Chair of the department to its
                           notable advancement both within the university as well as at the national level. A
                           member of the department since 1970, he has left an enduring and most valuable print
                           on its development and its professional standing. He has continued to work on his
                           research, teaching, advising while maintaining a regular professional schedule up
                           to the very last days of his life. He will be sorely missed by his colleagues, friends
                           and students. A memorial services was held on Saturday, December 6, at Grace-St Luke's
                           Episcopal Church, 1270 Peabody Ave. Memphis. TN.
                         
                        
                         An Obituary has appeared in the Commercial Appeal,  December 4, 2014.  A scholarship
                           in memory of Professor James Jamison is in the process of being  created, if you want
                           to contribute to this initiative please make a gift in his memory and restricted to
                           the department of Mathematics Sciences fund at the University of Memphis
                         
                        
                        
                        
                      
                      
                   
                
                
                   
                      
                         People 
                         
                            
                               
                                Research Faculty  
                                Instructors  
                                Visitors   Postdocs  
                                Graduate Students  
                                Staff  
                                A-Z List  
                            
                         
                      
                      
                      
                         
                            
                                Student FAQ  
                               Student resources for advising, tutoring, and registering for classes. 
                            
                            
                                ALEKS  
                               Information on the ALEKS computerized placement test. 
                            
                            
                                Contact Us  
                               How to contact us, and whom to contact about what. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/9/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


