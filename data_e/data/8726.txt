Removing associations between competency structure elements | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Removing associations between competency structure elements | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Competencies    »    Creating and managing competency structure elements   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Removing associations between competency structure elements                       
        
        
       
          
     
           Printer-friendly version       
	You can remove associations between elements without deleting either element.
 

 
	Remove associations between competency structure elements
 

  
		On the Competency Home page, click on the element with associations you want to remove.
	 
	 
		Click    Structure .
	 
	 
		In the Edit Structure tab, you can do the following:
		  
				Select the check boxes beside the parent competencies and learning objectives you do not want the element to associate with, and click the    Remove Associations from Selected  icon in the corresponding table header.
			 
			 
				Select the check boxes beside the child competencies, learning objectives, and activities you do not want the element to associate with, and click the    Remove Associations from Selected  icon in the corresponding table header.
			 
		  
  
	Remove associations between a learning objective and an activity from another tool area
 

  
		Navigate to the tool that corresponds with the activity you want to remove associations from.
	 
	 
		Do one of the following:
		  
				 Dropbox folder  On the Dropbox Folders page, click    Edit  from the context menu of the folder with associations you want to remove.
			 
			 
				 Quiz  On the Manage Quizzes page, click on the quiz with associations you want to remove.
			 
			 
				 Survey  On the Manage Surveys page, click on the survey with associations you want to remove.
			 
			 
				 Grade item  On the Manage Grades page, click on the grade item with associations you want to remove.
			 
			 
				 Discussion topic  On the Discussions List page, click    Edit Topic  from the context menu of the topic with associations you want to remove.
			 
		  
	 
		Click on the  Objectives  tab.
	 
	 
		Click    Remove Learning Objective  from the context menu of the associated learning objective you want to remove.
	 
	 
		Click  Save and Close .
	 
      Audience:    Instructor      

    
           

                   ‹ Adding associations between competency structure elements 
        
                   up 
        
                   Sharing competency structures › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Competencies  
  
      Competency structure basics    Automating competency structure evaluation    Creating and managing competency structure elements    Understanding competency status settings    Creating competencies    Editing a Draft or In Review competency s details    Hiding or showing an Approved competency    Modifying an Approved competency    Copying a competency    Archiving a competency    Deleting competencies    Creating learning objectives    Editing a learning objective s details    Copying a learning objective    Deleting learning objectives    Creating activities    Adding associations between competency structure elements    Removing associations between competency structure elements    Sharing competency structures    Tracking competency versions    Viewing competency structure results    Overriding competency structure results    Managing independent learning objectives and independent activities      Evaluating competency structure activities    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
