Spring classes - UofM Lambuth - University of Memphis    










 
 
 
     



 
    
    
    Spring classes - 
      	UofM Lambuth
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			University of Memphis Lambuth
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Admissions  
                          Programs  
                          Campus Life  
                          Resources  
                          News  
                          Contact Us  
                      
                      
                         
                            Programs   
                            
                               
                                  
                                   Undergraduate  
                                   Graduate  
                                   Dual Enrollment  
                                   Course Schedule  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	UofM Lambuth
                              	  
                          
                              	Programs
                              	  
                         Spring classes 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Spring Class List 
                     
                       Please note: Schedule subject to change  
                     
                      
    TERM  COURSE   TITLE   CRN  DAY  TIME          INSTRUCTOR     1ST  ACCT 2010-503  Fundamentals of Acct I  21288  MW    01:00 pm 04:00 pm   Stella Eddleman     2ND  ACCT 2020-503  Fundamentals of Acct II  21287  MW    01:00 pm 04:00 pm   Stella Eddleman     1  BIOL 1020-501  Biology of Organisms  16025  MWF    11:30 am 12:25 pm   Charles Kubicek     1  BIOL 1021-511  Biology of Organisms Lab  21289  T    01:00 pm 03:00 pm   Charles Kubicek     1  BIOL 1021-513  Biology of Organisms Lab  21722  R    03:00 pm 05:00 pm   Charles Kubicek     1  BIOL 1110-501  General Biology I  19453  TR    11:20 am 12:45 pm   Rebecca Cook     1  BIOL 1111-511  General Biology I Lab  23061  R    01:15 pm 04:15 pm   Rebecca Cook     1  BIOL 1230-M50  Microbiology                    21560          Rebecca Cook     1  BIOL 1231-M50  Microbiology Lab  21561          Rebecca Cook     1  BIOL 2020-501  Anatomy/Physiology II  21562  TR    09:40 am 11:05 am   Forrest Brem     1  BIOL 2021-511  Anat/Physiology II Lab  21290  T    01:00 pm 03:00 pm   Forrest Brem     1  BIOL 2021-512  Anat/Physiology II Lab  21724  T    03:15 pm 05:15 pm   Forrest Brem     1  BIOL 3130-501  Cell Biology  23062  MW    09:10 am 10:35 am   Charles Kubicek     1  BIOL 3500-501  Micro I/Fundmntl Aspect  23063  TR    09:40 am 11:05 am   Rebecca Cook     1  BIOL 3505-511  Gen Microbiology Lab  23064  MW    02:00 pm 04:00 pm   Rebecca Cook     1  BIOL 4000-501  Research I  17637          Charles Kubicek     1  BIOL 4000-502  Research I  17119          Charles Kubicek     1  BIOL 4100-501  Evolution  24523  MW    09:10 am 10:35 am   Forrest Brem     1  BIOL 4503-511  Lab Tech In Biochem  25700  MW    01:00 pm 04:00 pm   Charles Kubicek     1  BIOL 4511-501  Biochemistry I  24522  TR    11:20 am 12:45 pm   Jermaine Johnson     1  CHEM 1120-501  General Chemistry II  10171  TR    09:40 am 11:05 am   Jermaine Johnson     1  CHEM 1121-511  General Chemistry II Lab  23065  W    03:00 pm 05:45 pm   Jermaine Johnson     1  CHEM 1121-512  General Chemistry II Lab  20696  T    03:00 pm 05:45 pm   Jermaine Johnson     1  CHEM 3501-511  Foundation/Bioorganic Chem Lab  23908  F    09:10 am 12:10 pm   Jermaine Johnson     1  CHEM 3511-501  Foundations/Bioorganic CHEM  23907  MWF    08:00 am 08:55 am   Jermaine Johnson     1  CJUS 2226-501  Introduction to Police  23066  MW    12:40 pm 02:05 pm   George Megelsh     1  CJUS 3540-501  Criminology  21564  TR    01:00 pm 02:25 pm   Sheri Jenkins Keenan     1  CJUS 4100-501  Ind Dir Studies                 12312          George Megelsh     1  CJUS 4110-501  Advanced Application in CJUS  24464  TR    09:40 am 11:05 am   Sheri Jenkins Keenan     1  CJUS 4126-501  CJUS Admin and Mgmt  23067  TR    11:20 am 12:45 pm   George Megelsh     1  CJUS 4130-501  Ethical Dilemmas/CJUS  17946  TR    02:40 pm 04:05 pm   George Megelsh     1  CJUS 4233-501  Organized Crime  24466  M    05:30 pm 08:30 pm   George Megelsh     1  CJUS 4235-501  Security Management  25599  W    05:30 pm 08:30 pm   Barry Michael     1  CJUS 4535-501  Capital Punishment/America  25600  T    05:30 pm 08:30 pm   Michelle Shirley     1  COMM 2101-501  Media and Info Literacy  23912  MW    02:20 pm 03:45 pm   Denis Grimes     1  COMM 2381-509  Oral Communication              16248  TR    09:40 am 11:05 am   Denis Grimes     1  COMM 2381-511  Oral Communication              22096  W    05:30 pm 08:30 pm   Denis Grimes     1  COMM 2381-514  Oral Communication              22589  MWF    09:10 am 10:05 am   Yiyi Yang     1  COMM 3012-501  Health Communication  25664  W    05:30 pm 08:30 pm   Yiyi Yang     1  COMM 3012-M50  Health Communication  23959          Yiyi Yang     1  COMM 4011-502  Communication in Organizations  16113  MWF    11:30 am 12:25 pm   Yiyi Yang     1  COMM 4341-501  Interpersonal Communication  24382  T    05:30 pm 08:30 pm   Denis Grimes     1  COMM 4802-501  Internship                      12390          Denis Grimes     1ST  COUN 4611-503  Intro to Counseling  16249  M    05:30 pm 08:30 pm   Paul Helton     2ND  COUN 4621-501  Human Interactions              16123  M    05:30 pm 08:30 pm   Paul Helton     1  ECON 2020-501  Intro to Microeconomics  22098  T    05:30 pm 07:30 pm   Garry Carroll     1  EDPR 2111-501  Development Across Lifespan  21565  TR    08:00 am 09:25 am   Mollie Carter     1ST  ELED 3242-502  Teaching/Learning Elem School  19121  R    09:00 am 12:00 pm   Torre Kelley     1  ELED 3251-502  Method/Teach Math/Elem Grades  19127  T    01:00 pm 04:00 pm   Annette Cornelius     1ST  ELED 3261-502  Method/Teach/Science/Elem Grde  19128  W    09:00 am 12:00 pm   Elizabeth Johnson     2ND  ELED 3271-502  Method/Social Studies/Elem Grd  19129  R    09:00 am 12:00 pm   Torre Kelley     1  ENGL 1010-504  English Composition  23194  MW    05:30 pm 06:55 pm   Nancy Bartholomew     1  ENGL 1020-503  English Comp/Analysis  16093  MWF    10:20 am 11:15 am   Gray Hilmerson     1  ENGL 1020-505  English Comp/Analysis  21291  MWF    08:00 am 08:55 am   Gray Hilmerson     1  ENGL 1020-507  English Comp/Analysis  22099  TR    09:40 am 11:05 am   Jaclyn Harding     1  ENGL 2201-505  Literary Heritage  21292  TR    11:20 am 12:45 pm   Jaclyn Harding     1  ENGL 2201-506  Literary Heritage  22100  TR    09:40 am 11:05 am   Joy Austin     1  ENGL 2201-507  Literary Heritage  11857  MW    02:20 pm 03:45 pm   Elizabeth Weston     1  ENGL 3220-502  British Lit since 1750  11892  W    05:30 pm 08:30 pm   Elizabeth Weston     1  ENGL 3329-501  Major Authors in American Lit  25661  TR    09:40 am 11:05 am   Gray Hilmerson     1  ENGL 3601-502  Technical and Profess Writing  22103  TR    01:00 pm 02:25 pm   Elizabeth Weston     1  ENGL 4454-501  Literary Movements  22234  T    05:30 pm 08:30 pm   Elizabeth Weston     1  ENGL 4456-501  Reading in Literary Nonfiction  25660  M    05:30 pm 08:30 pm   Gray Hilmerson     1  ENGL 4531-502  Methods and Techniques ESL  18107  M    05:00 pm 08:00 pm         1  ESCI 1301-505  Survey of World Regions  21568  MWF    08:00 am 08:55 am   Paul Mego     1  FREN 2020-501  Intermediate French II  23935  MW    02:20 pm 03:45 pm   Robin Rash     1  GERM 2020-501  Intermediate German II  24371          Robin Rash     1  HETH 4213-541  Community Health/Iss/Serv  25891          Emily Fite     1  HIST 1120-502  World Civilization II  10454  MWF    10:20 am 11:15 am   Roy Hopper     1  HIST 2020-504  The U S Since 1877  16211  W    05:30 pm 08:30 pm   Roy Hopper     1  ICL 3333-541  Stdnt Assess/Inst Dec Mkng  22468          Annette Cornelius     2ND  ICL 4001-541  Teaching/Diverse Environment  21605          Annette Cornelius     2ND  ICL 4001-542  Teaching/Diverse Environment  21293          Annette Cornelius     2ND  ICL 4021-501  Professional/Ethical Practices  20395  M    09:00 am 12:00 pm   Linda Page     1  ICL 4800-501  Residency II Prof Seminar  16139                1  ICL 4904-501  Residency II Clinical  16141                1  IDT 3600-541  Technology in Education  17863          Annette Cornelius     1  JRSM 1700-501  Survey of Media  25612  TR    11:20 am 12:45 pm   Tori Cliff     1  JRSM 4702-501  Media, Diversity and Society  25613  MW    12:40 pm 02:05 pm   Tori Cliff     1  JRSM 4704-501  Issues in Sports and Media  25616  MWF    10:20 am 11:15 am   Roxane Coche     1  LDPS 8181-501  Plcy Implmntn Ed Ldrshp         13609  F    05:30 pm 08:30 pm   Donald Hopper     1  LDPS 8320-502  Urb Ed: Hst Cntmp Persp         22335  S    01:00 pm 04:00 pm   Donald Hopper     1  LEAD 8003-501  Policy-Oriented Rsrch  13655  S    08:30 am 12:00 pm   Mary Boudreaux     1  LEAD 8070-501  Culminating Experience          20725          Donald Hopper     1  LEAD 9000-501  Dissertation                    17495          Reginald Green     1  MATH 1420-506  Foundations of Mathematics  22106  MWF    11:30 am 12:25 pm   Dawn Englert     1  MATH 1420-5E1  Foundations of Mathematics  16099  MW  TR    08:00 am 08:55 am 08:00 am 09:25 am   Dawn Englert Dawn Englert     1  MATH 1530-501  Prob/Statistics/Non Calculus  21299  MWF    09:10 am 10:05 am   Dawn Englert     1  MATH 1710-505  College Algebra  14963  MWF    10:20 am 11:15 am   Dawn Englert     1  MATH 1720-501  Trigonometry  19903  MWF    11:30 am 12:25 pm   Joseph Londino     1  MATH 1830-503  Elementary Calculus  16101  TR    11:20 am 12:45 pm   Joseph Londino     1  MGMT 3110-502  Organization and Mgmt  16339  T    01:00 pm 04:00 pm   Kelly Mollica     2ND  MGMT 3110-M51  Organization and Mgmt  25207          Kelly Mollica     1  MGMT 3510-503  Business Communications  21572  R    01:00 pm 04:00 pm   Martha Robinson     1  MGMT 3510-M50  Business Communications  20486          Martha Robinson     1  MGMT 3510-M51  Business Communications  20487          Martha Robinson     1ST  MGMT 4420-501  Orgnztl Behavior in Business  16159  M    05:30 pm 08:30 pm   Martha Robinson     1  MGMT 4510-M50  Interntl Business Communictn  15664          Martha Robinson     1  MGMT 4710-M50  Strategic Mgmt  21515          Kelly Mollica     1  MIS 2749-503  Foundation/Information Systems  19474  R    05:30 pm 08:30 pm   Paul Doran     1  MIS 3210-501  Crit Thinking Using Analytics  21274  W    05:30 pm 08:30 pm   Paul Doran     1  MRCH 4300-M50  Family Resource Management  23538          Hal Freeman     1  MRCH 4300-M51  Family Resource Management  24041          Hal Freeman     1  MUAP 1550-501  Guitar Intro Instr              23857                1  MUAP 3015-501  Recording Studio Vocal Ensmble  24373  T    05:30 pm 08:30 pm   Jeremy Tubbs     1  MUHL 3303-501  Western Music History           21573  TR    02:40 pm 04:05 pm   Jeremy Tubbs     1  MUHL 4805-501  History of Rock and Roll  23072  MW    12:40 pm 02:05 pm   Jeremy Tubbs     1  MUS 1030-503  Introduction to Music  21576  M    05:30 pm 08:30 pm   Jeremy Tubbs     1  MUS 1040-502  Music in America  23140  TR    11:20 am 12:45 pm   Jeremy Tubbs     1  MUTC 3702-501  Theory/Musicianship II  24713  MW    02:20 pm 03:45 pm   Jeremy Tubbs     1  NURS 3000-501  Pharmacology in Nursing  22019  M    01:00 pm 04:00 pm   Hoi Chung     2ND  NURS 3000-M59  Pharmacology in Nursing  24903          Y'Esha Williams     1  NURS 3005-501  Intro Professional Nursing  23075  F    09:00 am 11:00 am   Christie Schrotberger     1ST  NURS 3005-M59  Intro Professional Nursing  24904          Jill Dapremont     1  NURS 3006-501  Professional Nursing Seminar  23076  R    03:30 pm 04:30 pm         1  NURS 3006-502  Professional Nursing Seminar  23079  T    04:00 pm 05:00 pm         1  NURS 3006-504  Professional Nursing Seminar  23077  W    04:00 pm 05:00 pm         1ST  NURS 3006-M59  Professional Nursing Seminar  24905          Gloria Carr     1  NURS 3101-501  Health Assessment  22021  M    09:00 am 11:00 am         2ND  NURS 3101-M59  Health Assessment  24906          Sheri Howard     1  NURS 3103-511  Health Assessment Lab  22145  T    08:00 am 11:00 am         1  NURS 3103-512  Health Assessment Lab  22146  W    01:00 pm 04:00 pm   Michelle Baldwin     1  NURS 3103-514  Health Assessment Lab  22148  T    11:45 am 02:45 pm         2ND  NURS 3103-M59  Health Assessment Lab  24907          Sheri Howard     1  NURS 3105-501  Foundations PT-Centered Care  23080  T    03:00 pm 06:00 pm   Lauren Williams     1  NURS 3106-511  Foundational Nursing Skills  23081  W    07:00 am 01:00 pm         1  NURS 3106-512  Foundational Nursing Skills  23082  F    07:00 am 01:00 pm         1  NURS 3106-513  Foundational Nursing Skills  23083  F    07:00 am 01:00 pm         1  NURS 3106-514  Foundational Nursing Skills  23084  W    07:00 am 01:00 pm         1  NURS 3127-501  Mental Health Nursing  22024  M    09:00 am 12:00 pm         1ST  NURS 3129-501  Mental Health Nurs Practicum  22428  T    07:00 am 01:00 pm         1ST  NURS 3129-502  Mental Health Nurs Practicum  22429  R    07:00 am 01:00 pm         1ST  NURS 3129-503  Mental Health Nurs Practicum  24268  W    07:00 am 01:00 pm         2ND  NURS 3129-505  Mental Health Nurs Practicum  24272  T    07:00 am 01:00 pm         2ND  NURS 3129-506  Mental Health Nurs Practicum  24273  R    07:00 am 01:00 pm         2ND  NURS 3129-507  Mental Health Nurs Practicum  24274  W    07:00 am 01:00 pm         1  NURS 3205-501  Nurs Adult I/Common Hlth Alt  23555  M    02:00 pm 05:00 pm   Kathy O'Connor-Wray     1  NURS 3206-510  Nursing Adult I/Practicum  23928  W    06:30 am 02:30 pm   Kathy O'Connor-Wray     1  NURS 3206-511  Nursing Adult I/Practicum  23925  T    06:30 am 02:30 pm   Kathy O'Connor-Wray     1  NURS 3206-512  Nursing Adult I/Practicum  23926  W    06:30 am 02:30 pm   Kathy O'Connor-Wray     1  NURS 3206-513  Nursing Adult I/Practicum  23927  R    06:30 am 02:30 pm   Kathy O'Connor-Wray     1  NURS 3217-501  Nursing/Childbearing Family  20322  R    01:30 pm 03:30 pm   Janet Tucker     1  NURS 3219-511  Nurs Childbearing Family Pract  19175  T    07:00 am 01:00 pm   Julie Cupples     1  NURS 3219-512  Nurs Childbearing Family Pract  20510  M    07:00 am 01:00 pm         1  NURS 3219-513  Nurs Childbearing Family Pract  23929  T    01:00 pm 07:00 pm   Julie Cupples     1  NURS 3219-514  Nurs Childbearing Family Pract  24900  M    01:00 pm 07:00 pm   Julie Cupples     1  NURS 3227-501  Pediatric Nursing  20323  M    01:00 pm 03:00 pm   Rosemary McLaughlin     1ST  NURS 3229-511  Pediatric Nursing Practicum  19554  T    06:30 am 06:30 pm         2ND  NURS 3229-512  Pediatric Nursing Practicum  11229  R    06:30 am 06:30 pm   Rosemary McLaughlin     1ST  NURS 3229-513  Pediatric Nursing Practicum  11223  R    06:30 am 06:30 pm   Rosemary McLaughlin     1  NURS 3230-501  Gerontological Nursing  16397  W    09:00 am 12:00 pm   Michelle Baldwin     1  NURS 3231-501  Gerontological Nurs Practicum  23580  F    07:30 am 03:30 pm   Michelle Baldwin     1  NURS 3231-502  Gerontological Nurs Practicum  24292  T    07:30 am 03:30 pm   Michelle Baldwin     1  NURS 3231-503  Gerontological Nurs Practicum  24901  M    07:30 am 03:30 pm         1  NURS 3231-504  Gerontological Nurs Practicum  24902  T    07:30 am 03:30 pm   Renee Morris     1  NURS 3305-501  Nurs Adult II/Complex Hlt Alt  23557  M    09:00 am 12:00 pm   Renee Morris     1  NURS 3306-511  Nurs Adult II/Complex Pract  23930  T    06:30 am 02:30 pm         1  NURS 3306-512  Nurs Adult II/Complex Pract  23931  W    06:30 am 02:30 pm         1  NURS 3306-513  Nurs Adult II/Complex Pract  23932  T    06:30 am 02:30 pm   Renee Morris     1  NURS 3400-501  Clinical Pathophysiology  22022  R    02:00 pm 05:00 pm   Kathy Butler     1ST  NURS 3400-M59  Clinical Pathophysiology  24908                1  NURS 4110-501  Evidence Based Practice Nurs  21711  R    09:30 am 12:30 pm   Marie Gill     2ND  NURS 4110-M59  Evidence Based Practice Nurs  25348          Sohye Lee     1  NURS 4127-501  Community Health Nursing  18159  F    12:00 pm 03:00 pm   Rosemary McLaughlin     1ST  NURS 4127-M59  Community Health Nursing  25346          Joy Hoffman     1ST  NURS 4129-501  Community Hlth Nurs Practicum  23086  T    09:00 am 03:00 pm   Lauren Williams     1ST  NURS 4129-503  Community Hlth Nurs Practicum  24270  R    09:00 am 03:00 pm   Lauren Williams     1ST  NURS 4129-504  Community Hlth Nurs Practicum  24271  W    09:00 am 03:00 pm         2ND  NURS 4129-505  Community Hlth Nurs Practicum  24276  T    09:00 am 03:00 pm   Rosemary McLaughlin     2ND  NURS 4129-506  Community Hlth Nurs Practicum  24277  R    09:00 am 03:00 pm         2ND  NURS 4129-507  Community Hlth Nurs Practicum  24278  W    09:00 am 03:00 pm         1  NURS 4129-M59  Community Hlth Nurs Practicum  25347                1  NURS 4205-501  Transition Professional Nurs  24281  T    08:00 am 12:00 pm   Christie Schrotberger     1  NURS 4205-M59  Transition Professional Nurs  25204          Jill Dapremont     1  NURS 4206-501  Transition Prof Nurs Practicum  24290  T    01:00 pm 04:00 pm         1  NURS 4206-502  Transition Prof Nurs Practicum  24291  T    01:00 pm 04:00 pm         1  NURS 4206-M55  Transition Prof Nurs Practicum  25205          Jill Dapremont     1  NURS 4206-M56  Transition Prof Nurs Practicum  25331          Jill Dapremont     1  NURS 4206-M57  Transition Prof Nurs Practicum  25330          Jill Dapremont     1  NURS 4206-M58  Transition Prof Nurs Practicum  25329          Jill Dapremont     1  NURS 4206-M59  Transition Prof Nurs Practicum  25332          Jill Dapremont     1  NUTR 2202-541  Nutrition  21772          Carolyn Nasca     2ND  PADM 4226-502  Intro Nonprofit Organizations  18325  R    05:30 pm 08:30 pm         1  PBRL 3400-501  Survey of Public Relations  25615  TR    09:40 am 11:05 am   Tori Cliff     1  PBRL 4440-501  Public Relations Campaign  25614  MW    09:10 am 10:35 am   Tori Cliff     1ST  PETE 3604-502  Teach Phys Ed/Health/K-Middle  16166  T    04:15 pm 06:15 pm         1  PHIL 1102-501  Intro to Ethics  21580  MWF    10:20 am 11:15 am   Lucien Garrett     1  PHIL 1611-501  Elementary Logic                12079  MWF    09:10 am 10:05 am   Lucien Garrett     1  PHIL 3514-501  Biomedical Ethics               19316  T    05:30 pm 08:30 pm   Lucien Garrett     1  PHYS 2020-501  General Physics II/Trig  10384  TR    08:00 am 09:25 am   Joseph Londino     1  PHYS 2021-511  Gen Physics Lab II  23088  F    01:00 pm 03:00 pm   Joseph Londino     1  POLS 1102-503  Intro Modern Political Thought  21581  M    05:30 pm 08:30 pm   Paul Mego     1  POLS 1501-501  International Relations         16182  TR    11:20 am 12:45 pm   Paul Mego     1  POLS 3410-501  Contemporary Political Thght  25670  MWF    10:20 am 11:15 am   Paul Mego     1  POLS 4702-001  Independent Study               12190          Paul Mego     1  PSYC 1030-501  General Psychology  22115  MWF    08:00 am 08:55 am   Karen Baker     1  PSYC 1030-502  General Psychology  22116  TR    09:40 am 11:05 am   Cheryl Bowers     1  PSYC 1300-501  Careers in Psychology  25687  F    11:30 am 12:30 pm   Cheryl Bowers     1  PSYC 3020-501  Research and Statistics II  23089  TR    01:00 pm 03:00 pm   Cheryl Bowers     1  PSYC 3101-501  Psychology of Personality  22317  TR    08:00 am 09:25 am   Karen Baker     1  PSYC 3103-502  Child Psychology  23141  MW    12:40 pm 02:05 pm   Cheryl Bowers     1  PSYC 3520-501  Legal/Forensic Psyc  17988  MWF    09:10 am 10:05 am   Karen Baker     1  PSYC 4032-501  Research Methodology  24948  MWF    10:20 am 11:15 am   Cheryl Bowers     1  PSYC 4401-501  Intro to Clinical Psychology  17989  TR    11:20 am 12:45 pm   Karen Baker     1  PSYC 4503-501  Special Problems in Psyc  10774          Karen Baker     1  PSYC 4503-502  Special Problems in Psyc  17161          Cheryl Bowers     1  PSYC 4507-501  Psyc Internship  24372  F    01:00 pm 02:30 pm   Cheryl Bowers     1ST  RDNG 4240-504  Literacy in Grades K-4  19118  T    09:00 am 12:00 pm   Torre Kelley     2ND  RDNG 4241-503  Literacy in Grades 4-8  17868  T    09:00 am 12:00 pm   Torre Kelley     1  SPAN 1020-503  Elementary Spanish II  22120  MW    12:40 pm 02:05 pm   Robin Rash     1  SPAN 1020-504  Elementary Spanish II  22121  T    05:30 pm 08:30 pm   Robin Rash     1  SPAN 2020-501  Intermediate Spanish II  22441  TR    03:15 pm 04:40 pm   Robin Rash     1ST  SPED 2000-504  Foundation/Exceptional Learn  21306  R    05:00 pm 08:00 pm   Shari Baldwin     1ST  SPED 3802-502  Ed Assessment/Inclusive Set  19134  W    01:00 pm 04:00 pm   Linda Page     2ND  SPED 3804-501  SPED Methods II. Content Area  20397  M    01:00 pm 04:00 pm   Linda Page     1  SPED 7517-501  Func Anlys/Treat Prob Behv  20353  W    05:30 pm 08:30 pm   Neal Miller     1  SPED 7518-501  Evidence-Based Prac in ABA  18367  R    05:30 pm 08:30 pm   James Meindl     1  SPED 7519-501  Prac/Appld Behav Anlys  18369  M    05:30 pm 07:30 pm   Neal Miller     1  SPED 7520-501  Behaviorism Seminar  23858  T    05:30 pm 08:30 pm   James Meindl     1  SWRK 3902-501  Human Behavior/Soc Envrnmnt  21589  T    01:30 pm 04:30 pm   Benetra Johnson     1  SWRK 3904-501  Social Work Practice II  21541  W    05:30 pm 08:30 pm   Veronica Morrow     1  SWRK 3906-501  Social Work Practice III  21542  W    01:30 pm 04:30 pm   Benetra Johnson     1  SWRK 3920-501  Soc Welfare Policy/Programs  21533  T    05:30 pm 08:30 pm   Veronica Morrow     1  SWRK 4840-541  Integrative Field Seminar I  23655          Veronica Morrow     1  SWRK 4841-541  Integrative Field Seminar II  23656          Veronica Morrow     1  UNIV 3522-501  American Cinema                 22327  W    04:30 pm 07:30 pm   Niles Reddick     1  UNIV 3535-501  Family Communication            23947  M    05:30 pm 08:30 pm         1  UNIV 3581-505  Faith/Reason/Imagination  19137  TR    02:40 pm 04:05 pm   Lucien Garrett     1  UNIV 4110-501  Internship  21959          Cynthia Hill     1  UNIV 4110-502  Internship  21092          Cynthia Hill     1  UNIV 4110-504  Internship  17236          Cynthia Hill     1  UNIV 4110-505  Internship  18836          Cynthia Hill     1  UNIV 4110-506  Internship  17455          Cynthia Hill     1  UNIV 4110-507  Internship  18837          Cynthia Hill     1  UNIV 4110-541  Internship  21050          Cynthia Hill     1  UNIV 4380-501  Independent Study  17656          Jeremy Tubbs     1  UNIV 4527-M52  The Developing Adult            16926                1  UNIV 4995-542  Senior Project  22126          Cynthia Hill     1  UNIV 4995-543  Senior Project  15169          Cynthia Hill     
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Programs 
                         
                            
                               
                                Undergraduate  
                                Graduate  
                                Dual Enrollment  
                                Course Schedule  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now  
                               Take the first step toward your new future 
                            
                            
                                Lambuth Academy  
                               High School Students: Learn more about dual enrollment 
                            
                            
                                News   Events  
                               See what's happening at Lambuth 
                            
                            
                                Contact Us  
                               Questions? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/27/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


