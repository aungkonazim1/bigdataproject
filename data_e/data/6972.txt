Jani A. Johnson, Au.D., Ph.D. - School of Communication Sciences and Disorders - University of Memphis    










 
 
 
     



 
    
    
    Jani A. Johnson, Au.D., Ph.D. - 
      	School of Communication Sciences and Disorders
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			School of Communication Sciences and Disorders
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          People  
                          Research Areas  
                          Student Orgs  
                          Resources  
                          News  
                      
                      
                         
                            People   
                            
                               
                                  
                                   Staff  
                                   PhD Students  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	School of Communication Sciences and Disorders
                              	  
                          
                              	People
                              	  
                         Jani A. Johnson, Au.D., Ph.D. 
                      
                   
                   
                       
                      
                      
                        
                        	
                        
                        		  
                         
                           
                           
                            
                              
                              
                                                                   
                                 
                                 
                                  
                                 
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                    Jani A. Johnson, Au.D., Ph.D.
                                    
                                  
                                 
                                 
                                  
                                    
                                    Assistant Professor, School of Communication Sciences and Disorders 
                                    
                                  
                                 
                                 
                                  
                                    
                                     
                                       
                                        Phone 
                                       
                                        
                                          
                                          901.678.5800
                                          
                                        
                                       
                                     
                                    
                                     
                                       
                                        Email 
                                       
                                         jajhns10@memphis.edu  
                                       
                                     
                                    
                                     
                                       
                                        Fax 
                                       
                                        
                                          
                                          901.525.1282
                                          
                                        
                                       
                                     
                                    
                                  
                                 
                                 
                                  
                                    
                                     
                                       
                                        Office 
                                       
                                        
                                          
                                          4055 N. Park Loop, Room 4045
                                          
                                        
                                       
                                     
                                    
                                     
                                       
                                        Office Hours 
                                       
                                        
                                          
                                          By appointment
                                          
                                        
                                       
                                     
                                    
                                  
                                 
                                 
                                  
                                    										
                                    
                                    
                                      Website  
                                    
                                      CVS  
                                                                            
                                    
                                  
                                 
                               
                              
                              
                            
                           			 
                           			  
                           
                           
                            About Jani Johnson 
                           
                            Dr. Jani Johnson is an Assistant Professor in the School of Communication Sciences
                              and Disorders and has served on the faculty since 2016. She received her B.S. in Communicative
                              Disorders at Auburn University in 2003. She went on to pursue her Au.D. (2007) and
                              her Ph.D. (2011) from the University of Memphis, where she now teaches Introduction
                              to Hearing Aids, Adult Audiologic Rehabilitation and Aging, Psychosocial Adjustment
                              to Hearing Loss, and Evidence Based Practices in the Provision of Amplification. Dr.
                              Johnson is currently the director of the Hearing Aid Research Laboratory (HARL), which
                              has a history of producing high-quality evidence designed to improve diagnostic and
                              rehabilitative procedures for individuals with hearing impairment.
                            
                           
                            Education 
                           
                            
                              
                               Ph.D. Communication Sciences and Disorders, University of Memphis, 2011 
                              
                               Au.D. Audiology, University of Memphis, 2007 
                              
                               B.S. Communication Disorders, Auburn University, 2003 
                              
                            
                           
                            Experience 
                           
                            Professional 
                           
                            
                              
                               Assistant Professor of Audiology, Communication Sciences and Disorders - University
                                 of Memphis - 2016-present
                               
                              
                               Research Assistant Professor of Audiology, Communication Sciences and Disorders -
                                 University of Memphis - 2011-2016
                               
                              
                               Research Audiologist, Hearing Aid Research Laboratory - University of Memphis - 2007-2011 
                              
                               Clinical Instructor of Audiology, Memphis Speech and Hearing Center - University of
                                 Memphis - 2011
                               
                              
                            
                           
                            Research and Scholarly Activities 
                           
                            Research Interests 
                           
                            Multicultural Issues in Audiology and Adult Aural Rehabilitation with a focus on Hearing
                              Aids
                            
                           
                           
                         
                        								
                        
                        
                        
                        
                        	
                      
                      
                   
                
                
                   
                      
                         People 
                         
                            
                               
                                Staff  
                                PhD Students  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now  
                               Want to pursue a degree in Communication Sciences and Disorders?  Find out how to
                                 apply.
                               
                            
                            
                                Support the School  
                               Help support the School with the funding of various initiatives. 
                            
                            
                                Memphis Speech and Hearing Center  
                               Learn about our in-house clinical training facility. 
                            
                            
                                Contact Us  
                               Need more information about our School? 
                            
                         
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


