Removing simple sequencing from a SCORM package | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Removing simple sequencing from a SCORM package | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Content    »    Using SCORM in Content   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Removing simple sequencing from a SCORM package                       
        
        
       
        
     
             
 
	You can remove the sequencing and navigation rules from a SCORM package if you want to use some of the modules or topics in another course, or reorder modules and topics in the current course.
 

 
	Removing sequencing and navigation from a SCORM package resets progress tracking for those topics.
 

 
	Remove simple sequencing
 

  
		Click on the module you want to remove simple sequencing for from the Table of Contents.
	 
	 
		Click    Remove Sequencing  from the module title's context menu.
	 
	 
		Click  Yes  to confirm action.
	 
      Audience:     Instructor       

    
           

                   ‹ Creating and editing a SCORM package 
        
                   up 
        
                   Viewing SCORM reports on objects and users › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Content  
  
      Content basics    Creating course content    Managing and updating course content    Using SCORM in Content    Importing a SCORM package    Creating and editing a SCORM package    Removing simple sequencing from a SCORM package    Viewing SCORM reports on objects and users      Tracking content completion and participation    
                  
           
         

       
     

    
    
    
   
 
   
 
