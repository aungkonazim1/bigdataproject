Understanding competency structures | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Understanding competency structures | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Competencies    »    Competency structure basics   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Understanding competency structures                       
        
        
       
          
     
           Printer-friendly version       
	A competency structure is a hierarchy composed of three basic elements:
 

  
		Competencies
	 
	 
		Learning objectives
	 
	 
		Activities
	 
  
	Although you can create multiple activities, learning objectives, and competencies within a competency structure, the most fundamental structure must contain at least one competency, one learning objective, and one activity.
 

 
	 Note  If you share a competency structure with a child org unit, the child org unit must associate its own activities since activities cannot be shared between org units.
 

 
	Learning objectives
 

 
	Learning objectives are the skills, abilities, or knowledge a person must acquire to become competent in a given domain. Create learning objective statements that are directly measurable through related activities. Other vocabulary used to describe learning objectives can include "indicators", "criteria", "requirements", and "learning outcomes".
 

 
	Activities
 

 
	 Note  Starting with Learning Environment 9.2, you no longer create activities in the Competencies tool. Instead, you add associations between learning objectives and activities created within the course.
 

 
	Activities are the only elements that can be graded in a competency structure. You can use existing tools in Learning Environment to create activities, and you can create external activities such as a concert performance or an oral presentation (these are manual assessment activities).
 

 
	You can associate activities with relevant learning objectives, and have users complete them so you can evaluate their learning objective achievements. Activities include quizzes, surveys, dropbox folders, discussion topics, grade items, manual assessments, and content.
 

 
	You can add measurable criteria to the activity and set the assessment method as a requirement to completing the learning objective. If a user's activity assessment meets the minimum required threshold set for that activity (e.g. minimum rubric level score, minimum numeric score), then the user achieves or is on the path to achieving the associated learning objective.
 

 
	Competency structures inside and beyond your course
 

 
	If you create a competency structure as part of a course offering, it is accessible only within that course offering. For example, you can create a competency to represent the entire course, learning objectives to represent specific units, and associate activities to each unit.
 

 
	You can create competency structures inside org units such as departments, semesters, and the organization to track users’ achievements beyond a course offering. You can also share these competency structures with multiple course offerings and evaluate the competencies and learning objectives within specific child org units. This enables users to complete the competency in stages over time, working on different learning objectives within different courses. If a competency structure is large and complex, acquiring all the knowledge and skills associated with it might involve many learning experiences across several courses.
 
     Audience:    Instructor      

    
           

                   ‹ Accessing Competencies 
        
                   up 
        
                   Understanding competency structure relationships › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Competencies  
  
      Competency structure basics    Accessing Competencies    Understanding competency structures    Understanding competency structure relationships    Managing settings in Competencies      Automating competency structure evaluation    Creating and managing competency structure elements    Evaluating competency structure activities    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
