Profile for Angiline Powell | The University of Memphis  
   Profile for Angiline Powell | The University of Memphis   
 
 
 
  

 

 
 
     
 
    Faculty and Staff Profiles    
 

 
 
  
 
 

 
 
 

 
 
    Angiline Powell    
 
 
    Assoc Professor, Instruction Curriculum Leadership    
 
 
 Email:   apowell3@memphis.edu    
 
 
   Office Location:   424 B Ball Hall   
 
 
   Office Hours:  Monday 2:30 to 5:00  and 8:00 to 8:30; Tuesday 10:00 to 12:00   
 
 
 

   

 
 





 
 
 

  Profile       Biography Angiline (Angie) Powell 
 Angiline (Angie) Powell received her doctorate in mathematics education from the University of Alabama at Tuscaloosa.  Her dissertation was entitled “African Americans and Their Reflections of Exemplary Mathematics Teachers.”  Dr. Powell’s research involves  urban education, exemplary mathematics teaching practices, and technology for pre-service teachers. Dr. Powell taught middle and high school mathematics in Mobile County for eight years.  Formerly from Texas Christian University, she is currently the interim chair of the Instruction and Curriculum Leadership department at the University of Memphis.   

Dr. Powell  was awarded a grant for professional development from the Tennessee Higher Education Commission. She and her CoPI are currently implementing the grant that serves the two local merging school districts Memphis City Schools and Shelby County Schools. This grant is an expansion of a local grant that she was awarded in a previous year.    Additional Information       Class Resources    
  National Council Teachers of Mathematics  
         Student Advising/Mentoring      Doctoral - Karla Webb - 2009     Outreach     LeMoyne-Owens - Local Elementary Teachers  - October 18, 2008 - Strengthing Instruction in Tennesse Elementary Schools (SITES)   Woodway Elementary  - Career Day - May 2003 -  Fort Worth School District  Everman   Junior High School - Career Night - October 2003 -  Everman School District  Morningside Middle Afterschool Program - 6-8th grade students -  1999-2004 - Candidate  Commercial Appeal Spelling Bee -  Service Community -  2005 -  Commercial Appeal  Getwell Elementary School - Fourth Grade Teachers - 2005 - Memphis City Schools  Vollintine- Evergreen Newsletter Distributer - 1400 Block Residents - 2005-2010 - VECA Historic District  Pleasant View Elementary School  - K-6th teachers - August 2008 - &nbsp  Volluntine Evergreen Community Association  - Board Member Southwest Quadrant - 2001-2013 - &nbsp  National Lab Day - Students - May 11 2011 - Oakhaven Middle School  Haberman Education Foundation - Memphis City School Administrators - 2009 - &nbsp  Volluntine Evergreen Community Association - &nbsp - Fall 2013 - VECA Historic District    Service     UNIVERSITY - &nbsp - &nbsp   University - Faculty Senate - 2000 - 2001  University - Committee for Human Safeguards  - 2002 - 2003  University  - Faculty Tenure & Promotion Appeal Committee - 2013-2016  University  - Engaged Scholarship Networking Committee - Spring 2013  COLLEGE - &nbsp - &nbsp  College - Facilitator of Math, Science, and Technology Forum - 2000  College - Committee of the Institute of Mathematics, Science and Technology Education     - 2000 - 2004  College - Adams Chair (Endowed) Selection Committee   -  2001  College - Designer of Middle School Mathematics Major - 2002 - 2003  College - Writing Group - 2004-2005  College  - Core Team  - 2004 - present  College  - TEP Interviews - 2004 - 2010  College - Faculty Development Advisory Committee  - 2004-2008;     2005-2007    Co- Chair  College  - Co- Chair Professional Development - 2005-2006  College -  Urban Aspiration Team - 2005-2007  College  - Pi Day - 2006-2008, 2010  College - Welcome Back Students - 2006-2007; 2009 -2010  College  - Search Committee Health and Sports Science - 2007  College - Participated in Student Centered Culture Assessment - April 2007  PROFESSIONAL - &nbsp - &nbsp  Professional - Reviewer Proposal AERA Submissions - 2000-2001,  2005-2015  Professional -  Reviewer  Electronic Journal for the Integration of Technology in Education - 2002 - 2005  Professional - National Council of Teachers of Mathematics  Educational Materials Committee -  2004-2007  Professional - Reviewer Teaching Children Mathematics - 2006-2010  Professional  - Reviewer Mathematics Teaching in the Middle Schools -  2006-2010  Professional -  Reviewer for Mid-South Education Research Association   -  2007  Professional - Reviewer School Science Mathematics Association - 2007-2010  Professional - Reviewer Tennessee Association of Middle Schools - 2009  Professional - Middle School Journal Reviewer  - 2014-2015  Professional - Journal of Urban Mathematics Education Reviewer - 2013-2015  Professional  - Journal of Mathematics Education Leadership Reviewer - 2013  Professional - Faculty Women of Color at the Academy  - 2014  Departmental - Master Comprehensive Exam  -  2004 -  2015  Professional  - Membership Committee School Science Mathematics Association  - 2012-2015  Departmental - Participated in Integrating Video Technology with the Five Standards for Effective Pedagogy (2005-2006) and TLINC(2006) -  2005-2006; 2006  Professional  - Technology Committee Association of Mathematics Teacher Educators  - 2015 to present  Departmental - Faculty Research Grant (Chair)  - 2006-2007  Departmental - Financial Advisory Committee - 2006-2007                 Co-Chair 2007-2008  Departmental - Middle Grade Search Committee  - 2006-2007  Departmental - Elementary Education Search Commitee -  2007-2009  Departmental - Science Education Search - 2011-2012  Departmental - Mathematics Education Search - 2011-2012  Departmental - Elementary Education - 2011-2012  Departmental - Mathematics Education Search Committee - 2012  Departmental - Reaching the Core: Memphis Conference - 2013  Departmental - NCATE Writer Middle School SPA  - 2014    Consulting     Fort Worth Independent School District - August 2002  Math, Science and Technology Institute - October 2002  Math, Science and Technology Institute - October 2002  Math, Science and Technology Institute - February 2003  Math, Science and Technology Institute - April 2003  Fort Worth Independent School District  - August 2003  Everman High School - October 2003  Everman Junior High School - October 2003  Math, Science and Technology Institute - November 2003  Math, Science and Technology Institute -  November 2003  Memphis City Schools - 2005, 2009  Memphis City Schools/ The University of Alabama at Birmingham - July 2005  Fayette County, TN  - June 2005 to May 2007;  2008-2010  Brighton Elementary, Tipton County   - 2006-2007  Eastside Elementary School - January 2008  Educational Testing Service  - March 2008  Educational Testiing Service (SITES) - March 2008    Journal Articles     Powell, A.,    Reynolds, S.(2001). Fated, disregarded and overlooked: African American mathematics education.  Journal of Thought, 36 (1), 67-76.           Kelly, J., Stetson, R.,   Powell-Mikle, A. (2002).  Science adventures at the local museum.  Science   Children, 39 (7), 46-48.      Powell-Mikle, A. (2003). We are more than numbers! African American students' perceptions of exemplary mathematics teachers.  International Journal of Education Reform, 12,  84-96.      Powell-Mikle, A.,   Patton, M. (2004). The geometry of quilts: Funpacks and the African American experience.  Childhood Education, 80,  187-190.        Powell, A.,   Zhou, C. (2004). It takes only a spark! In M.F. Chappell   T. Pateracki (Eds.)  Searching for solutions: A guide for empowering the beginning teacher of mathematics  (p 15).   Reston, VA: NCTM.  Daane, C. J., Lowery, P. K.,   Powell, A. (Spring 1998). Activities pages.   Alabama Journal of Mathematics, 22 (1), 49-54.       Rousseau, C.,   Powell, A. (2005). Understanding the significance of context: A framework to examine equity and reform in secondary mathematics.  High   School   Journal   88 (4), 19-31.        Perkins, J. H., Powell, A.,   Key, S. (2006). The power of vocabulary knowledge: Social studies, science and mathematics.  Tennessee's Children,  (1), 25-30.            Ray, B., Powell, A.,   Strickland, J. (2006).   Technology integration and the pre-service teacher: A roadmap for reflection and observation during early field experiences   teaching   learning.  The Journal of Natural Inquiry   Reflective Practice, 21,  30-51.          Powell, A.,   Berry, R. (2007). Achieving success: Voices of successful African American mathematics students. In M. Brown   R. Bartee (Eds.).  Still not equal: Expanding educational opportunity in society  (pp.167-175). New York: Peter Lang.          Powell, A.,   Anderson, C. R. (Winter, 2007-2008).  Educators, parents and numeracy:  Partnerships for successful African American students.   Childhood Education, 84, 70-74.     Daane, C. J., Lowery, P. K.,   Powell, A. (Fall 1998). Activities pages.   Alabama Journal of Mathematics, 22( 2), 44-50.       Powell, A. (2005). Creating a caring community in math class.  Eisenhower National Clearing House Focus Review,12,  (20) 9-12.         Anderson, C. R.   Powell, A. (2009). " Metropolitan Perspective on Mathematics Education: Lessons Learned from a “Rural” School District"   Journal of Urban Mathematics Education,2  (1), 5-21.    Powell, A.   Seed, A. H. (March, 2010). Developing a caring ethic for middle school mathematics classrooms.  Middle School Journal 41(4). 44-48.      Presentations     *Powell, A. (1997, November).  Making the usual-unusual . Paper presented at Alabama Council of Teachers of Mathematics, Montgomery, AL.          *Powell, A. (1998, October).  Making the usual-unusual.  Paper presented at National Council of Teachers of Mathematics Southern Regional Conference,  Little Rock, AR.            *Powell, A. (2000, February).  Students perceptions of exemplary mathematics teaching practices .  Paper presented at National Association of African American Studies, Houston, TX.         *Powell, A. (2000, March).  Mathematics a wonderful kind of play . Paper presented at National Council of Teachers of Mathematics Regional, Mobile, AL.             *Powell, A. (2000, April).  Reflections on exemplary mathematics students . Paper presented at American Educational Research Association, New Orleans, LA.      *Kelly, J., Stetson, R., Stetson, E., Powell, A., Miller, E.  (2000, April).  Putting theory into practice: Reflective teaching and implications for use .  Paper presented at American Education Research Association,  New Orleans, LA.           *Powell, A. (2000, July).  Algebra   play.   Paper presented at Conference for the Advancement of Mathematics Teaching, Houston, TX.              *Powell, A. (2001, April).  African American mathematics students’ perceptions of peers education and mathematical experiences.  Paper presented at American Education Research Association, Seattle, WA.       *Powell, A.  ( 2001, April).  Teacher! You can build us a future:  Things that work with urban students.   Paper presented at National Council of Teachers of Mathematics, Orlando, FL.           *Powell, A.   (2001, July).    Teacher! You can build us a future:  Things that work with urban students.   Paper presented at Conference for the Advancement of Mathematics Teachers, San Antonio, TX.           *Powell, A. (2001, April).  African American mathematics students’ perceptions of peers education and mathematical experiences.  Paper presented at American Education Research Association, Seattle, WA.        *Powell, A.,   Ray, B. (2003, March).  Receptivity to mathematically-based electronic portfolios as assessment, reflection and anxiety.  Paper presented at Society for Information Technology and Teacher Education, Albuquerque, New Mexico.         *Powell, A.,   Patton, M.  (2003, July).  Altogether better: Funpacks for urban families.  Paper presented at Conference on the Advancement of Mathematics Teaching, Austin, TX.           *Powell, A. (2004, April).  Mathematics a wonderful kind of play.  Paper presented at National Council of Teachers of Mathematics, Philadelphia, PA.          *Powell, A.    Berry, R. Q.   (2004, September).  Still we rise: Voices of successful African American math students.   Paper presented at  Still Not Equal: Expanding Opportunites is a Global Society Patterson Conference, Washington DC .            * Powell, A.,   Key, S. (2004, October ). Can this teacher be saved?  Paper presented at School Science and Mathematics Association, Atlanta, GA.      *Powell, A.,   Silva, C. (2005, February).  Neither equal nor equitable:  Implementation of mathematics and language arts standards in urban schools . Paper presented at Research Council for Mathematics Learning, Little Rock, AR.             *Powell, A. (2005, April).  Mathematics funpacks: Fun for the family . Paper presented at National Council of Teachers of Mathematics, Anaheim, CA.              *Powell, A.,   Rousseau, C. (2005, October).  Rural, poor, African American, algebra:   Implementing change.  Paper presented at School Science and Mathematics, Fort Worth, TX.           *Key, S., Powell, A.,   Perkins, J. H. (2005, October).  The power of vocabulary: Its development and enhancement in the content areas.  Paper presented at Tennessee Association for Childhood Education International, Memphis, TN.            *Powell, A.,   Berry, R. (2006, April).  Successes and characteristics of successful African-American math students  .  Paper presented at American Educational Research Association, San Francisco, CA.            *Powell, A.,   Abraham, A. (2006, April).  Assessing the assessors .  Paper presented at National Council of Teachers of Mathematics, St. Louis, MO.          *Rousseau-Anderson, C., Powell, A., Watts, L., Franceschini, L.,   Vogel, T. (2007, April).  Down in the delta: Improving mathematics education for African American students in the rural south . Symposium presented at the American Educational Research Association, Chicago, IL          Powell, A. (2007, May).  Down in the delta.  Paper presented at the Southeastern Atlantic Section of the Society for Industrial and Applied Mathematics (SIAM), Memphis, TN.       *Powell, A. (2007, June).  You can build us a future! Things that work with urban mathematics students.   Paper presented at University of Memphis Panhellenic Building, Memphis, TN.    Powell,A. ( 2007, August).  Making sense of fractions:  I got it!  Paper presented at the Memphis Area Council of Teachers of Mathematics Miniconference, Memphis, TN.       Powell, A. (2007, November).  Good teaching matters!  You matter ... Keynote address presented at Alabama Association of Developmental Education(ALADE). Orange Beach, AL.        *McCullough, A., Powell, A.,   Rousseau Anderson, C. (2007, November).  Mathematics Professional Development in a Rural School District: Rewards and Challenges.  Paper presented at School, Scienceand Mathematics Conference (SSMA).  Indianapolis, IN.     
 

 

 

 

 
 
 
 

 
 

