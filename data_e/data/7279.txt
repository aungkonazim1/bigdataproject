Creating multi-select questions | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Creating multi-select questions | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Question Library    »    Question types   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Creating multi-select questions                       
        
        
       
          
     
           Printer-friendly version       
	Multi-select (M-S) questions require respondents to identify one or more correct answers in a list of possible answers. Unlike multiple choice (MC) questions, multi-select questions enable you to choose a grading format and allow users to select more than one answer.
 

 
	Create a multi-select question
 

  
		Enter a  Title.  Enter a  Points  value. Select a difficulty level in the  Difficulty  drop-down list.
	 
	 
		Enter your M-S question in the  Question Text  field.
	 
	 
		Click  Add a File  to upload an image to accompany your question. You can enter a description of the image in the  Description  field.
	 
	 
		Select an enumeration style from the  Enumeration  drop-down list.
	 
	 
		Select a display  Style .
	 
	 
		Choose a  Grading  format:
		  
				 All or nothing  Users receive full points for the question if they select all of the correct answers and none of the incorrect answers. Users receive zero points if they miss any correct answers or select any incorrect answers.
			 
			 
				 Right minus wrong Users receive points equal to the number of right answers they choose minus the number of incorrect answers they choose. To determine how much each answer is worth, the system takes the total number of points assigned to the question and divides it by the total number of answer choices.
				 
					 Example  If a question is worth 10 points and has 5 answer choices, each correct answer is worth 2 points, and each incorrect answer is worth 2 points (10/5 = 2). If a user gives 3 correct answers and 2 incorrect answers, 2 is the total number of points received for the question [(3-2)*2 = 2].
				 

				 
					 Note  Users can receive a minimum of zero on a question: they cannot receive a negative mark.
				 
			 
			 
				 Correct answers Users receive points for each correct answer they select and for each incorrect answer they leave blank. Incorrect answers selected and correct answers left blank are ignored.
				 
					 Example  Consider a question with a total of six potential answers, two answers being correct (in this case, choices a) and b) are the correct choices). The total points available for this question is 4.
				 

				    
								Response
							 
							 
								Score
							 
						   
								 
									a) selected
								 

								 
									b) selected
								 

								 
									c)
								 

								 
									d)
								 

								 
									e)
								 

								 
									f)
								 
							 
							 
								 
									6 out of 6 answers are chosen correctly. User receives full marks: 4 points.
								 
							 
						   
								 
									a) selected
								 

								 
									b)
								 

								 
									c) selected
								 

								 
									d)
								 

								 
									e)
								 

								 
									f)
								 
							 
							 
								 
									4 out of 6 answers are chosen correctly. User receives a mark of 4/6 * 4 = 2.6667 points.
								 
							 
						   
								 
									a) selected
								 

								 
									b)
								 

								 
									c) selected
								 

								 
									d) selected
								 

								 
									e)
								 

								 
									f)
								 
							 
							 
								 
									3 out of 6 answers are chosen correctly. User receives a mark of 3/6 * 4 = 2 points.
								 
							 
						    
		  
	 
		Select  Randomize options  to randomly generate the option orders presented to each user.
	 
	 
		Enter an answer option in each  Value  field of your M-S question. You can click    Add Option  and select the number of additional answer options you want to include in your question. To reduce the number of answer options, click the corresponding    Remove Entry  icon.
	 
	 
		In the  Correct  column, select the check box next to each correct answer option.
		 
			 Note  A multi-select question will not be auto-graded if no check boxes are set as Correct.
		 
	 
	 
		Provide comments and suggestions in the  Feedback ,  Question Hint , and  Question Feedback  fields.
	 
	 
		Click  Preview  to view your question.
	 
	 
		Click  Save  to return to the main page, click  Save and Copy  to save and create another M-S question that retains the copied properties, or click  Save and New  to continue creating new M-S questions.
	 
      Audience:    Instructor      

    
           

                   ‹ Creating multiple choice questions 
        
                   up 
        
                   Creating long answer questions › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Question Library  
  
      Question Library basics    Question types    Understanding regular expressions    Creating true or false questions    Creating multiple choice questions    Creating multi-select questions    Creating long answer questions    Creating short answer questions    Creating multi-short answer questions    Creating fill in the blanks questions    Creating matching questions    Creating ordering questions    Creating arithmetic questions    Creating significant figures questions    Creating Likert questions    Creating text information    Creating image information      
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
