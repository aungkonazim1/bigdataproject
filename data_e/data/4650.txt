Alternative Admissions to TEP - TEP - University of Memphis    










 
 
 
     



 
    
    
    Alternative Admissions to TEP - 
      	TEP
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Office of Teacher Education and Student Support Services
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Admission to TEP  
                          Advising  
                          Clinical Practice  
                          Graduation  
                          Teacher Licensure  
                      
                      
                         
                            TEP Admissions   
                            
                               
                                  
                                   Admissions  
                                        
                                         TEP Expectations  
                                        
                                         Alternative Admissions  
                                     
                                  
                                   Professional Dispositions  
                                         Introduction  
                                         Professional Educator Dispositions  
                                         Disqualification  
                                         Clinical Dispositional Referral  
                                         Dispositional Deficiency Form for Course of Communication  
                                         Faculty Reporting Professional Disposition  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	TEP
                              	  
                          
                              	About
                              	  
                         Alternative Admissions to TEP 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Alternative Admissions to TEP 
                     
                      
                        
                          Application  
                        
                      
                     
                      Students who are denied admission into the Teacher Education Program should follow
                        the steps outlined below.
                      
                     
                      Undergraduate Alternative Admission Based on Praxis I Core Academic Skills for Educators
                        Exams (CORE)
                      
                     
                      
                        
                         The candidate must meet the minimum cut-off score for the writing subtest and either
                           the math or reading or they cannot appeal.
                         
                        
                         The appealed Praxis I Core Academic Skills for Educators Exams (CORE) subtest score
                           cannot vary more than 3% (5 points) from the acceptable minimum score.
                         
                        
                         Candidates must meet the following to submit an appeal for alternative admission:
                           
                            
                              
                               Must have a GPA of 3.0 
                              
                               Must have taken the Praxis I Core Academic Skills for Educators Exams (CORE) subtest
                                 in question twice. (Please note the writing subtest must be passed)
                               
                              
                               If appealing the READING TEST, the candidate must have at least a 3.00 average in
                                 in their general education literature and history courses.
                               
                              
                               If appealing the MATHEMATICS TEST, the student must have a 3.00 average in required
                                 math courses.
                               
                              
                               All of the above courses must be from an accredited college. 
                              
                            
                           
                         
                        
                      
                     
                      Undergraduate Alternative Admission Based on Grade Point Average 
                     
                      Candidates appealing to be admitted to TEP because their grade point average has fallen
                        below 2.75 must meet the following requirements in order to appeal:
                      
                     
                      
                        
                         Must have passed all sections of the Praxis I Core Academic Skills for Educators Exams
                           (CORE).
                         
                        
                         Must provide, as part of their appeal, a plan describing the courses they will repeat
                           to raise their grade point average to the 2.75 minimum. No restricted courses can
                           be included in the plan.
                         
                        
                         Candidates failing to achieve alternative admission will be advised individually regarding
                           a remediation plan to improve their grade point average and/or Praxis I Core Academic
                           Skills for Educators Exams (CORE) performance. In some instances the candidate may
                           be counseled out of the program.
                         
                        
                      
                     
                      
                        
                          Special Note: Candidates cannot appeal if they lack both the necessary grade point
                              average and have failed to meet a minimum score on the Praxis I Core Academic Skills
                              for Educators Exams (CORE).  
                        
                      
                     
                      MAT   Licensure-Only Candidates Appeals Process for Admission to TEP 
                     
                      Appeals Based on Praxis I Core Academic Skills for Educators Exams (CORE) 
                     
                      
                        
                         Candidates must pass the writing subtest and either the math or reading subtest. 
                        
                         Candidates must have taken the Praxis I Core Academic Skills for Educators Exams (CORE)
                           subtest in question twice. (Please note the writing subtest must be passed.)
                         
                        
                         If appealing the Reading Subtest, the candidate must have at least a 3.0 average in
                           general education literature and history courses.
                         
                        
                         If appealing the Mathematics Subtest, the candidate must have at least a 3.0 average
                           in the Math courses taken in their undergraduate degree.
                         
                        
                         Must have a 3.5 GPA in graduate Licensure coursework. 
                        
                      
                     
                      
                        
                          Special Note: Graduate candidates cannot appeal based on failure to pass the Praxis
                              II Content Knowledge Exam.  
                        
                      
                     
                      Deadlines 
                     
                       Appeals   Alternative Admissions for Spring Semester  
                     
                      
                        
                         TEP appeals/alternative admissions deadlines for priority registration: October 15th
                           or the Monday closest to October 15th
                         
                        
                         TEP appeals/alternative admissions deadlines for regular registration: December 16th
                           or Monday closest to December 16th.
                         
                        
                      
                     
                       Appeals   Alternative Admissions for Summer Semester  
                     
                      
                        
                         TEP appeals/alternative admissions deadlines for priority registration: March 15th
                           or Monday closest to March 15th
                         
                        
                         TEP appeals/alternative admissions deadlines for regular registration: May 15th or
                           Monday closest to May 15th
                         
                        
                      
                     
                       Appeals   Alternative Admissions for Fall Semester  
                     
                      
                        
                         TEP appeals/alternative admissions deadlines for priority registration: March 15th
                           or Monday closest to March 15th
                         
                        
                         TEP appeals/alternative admissions for regular registration: July 31st or Monday closest
                           to July 31st
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         TEP Admissions 
                         
                            
                               
                                Admissions  
                                     
                                      TEP Expectations  
                                     
                                      Alternative Admissions  
                                  
                               
                                Professional Dispositions  
                                      Introduction  
                                      Professional Educator Dispositions  
                                      Disqualification  
                                      Clinical Dispositional Referral  
                                      Dispositional Deficiency Form for Course of Communication  
                                      Faculty Reporting Professional Disposition  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Praxis Exams  
                               Test dates, state requirements and study help.                             
                            
                            
                                edTPA  
                               Information about submission and score reporting dates, and more. 
                            
                            
                                Ready2Teach  
                               Tennessee Board of Regents Teacher Education Program 
                            
                            
                                Scholarships   Awards  
                               A list of scholarships   awards available at all levels of the TEP. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


