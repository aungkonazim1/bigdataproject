Welcome - Reference Books - LibGuides at University of Memphis Libraries   
 
	 
         
         
         
        
		 Welcome - Reference Books - LibGuides at University of Memphis Libraries 
		 
		 
 
 
 
 
 
 
 
 
 
 
 

		 
		 
		 
		 
		 
		 
         
         
				 
				 
             
         
        
        
            
		
		 
		
		 
		 
		
		 
		
		
	   
		         
        	 
     
	 
		 Skip to main content                  
                 
         
  		 
             
                
			 
				
					 
						 University of Memphis Libraries
						 
					 
					 
						 LibGuides
						 
					 
					 
						 Reference Books
						 
					 
					 Welcome
					 
			              
			 
				 
					
                 
                     
                         
                             
                             Search this Guide  
                             
                                 Search 
                             
                         
                     
                 				 
							 
             
                 Reference Books 
                 
                     A place to showcase interesting, useful and unusual reference books. 
                 
             
         
         
          
         
             
                 
                     
                         
                             
                 
                     
                         Welcome 
                        
                     
                 
                 
                     
                         Reference E-Books 
                        
                     
                 
                 
                     
                         New Reference Books 
                        
                     
                 
                 
                     
                         Atlases 
                        
                     
                 
                 
                     
                         American Ethnic Groups 
                        
                     
                 
                 
                     
                         Anthropology and Ethnology 
                        
                     
                 
                 
                     
                         Archaeology 
                        
                     
                 
                 
                     
                         Architecture 
                        
                     
                 
                 
                     
                         Art 
                        
                     
                 
                 
                     
                         Biology 
                        
                     
                 
                 
                     
                         Business and Economics 
                        
                     
                 
                 
                     
                         Chemistry 
                        
                     
                 
                 
                     
                         Communications and Media 
                        
                     
                 
                 
                     
                         Criminal Justice 
                        
                     
                 
                 
                     
                         Earth Sciences 
                        
                     
                 
                 
                     
                         Education 
                        
                     
                 
                 
                     
                         Engineering 
                        
                     
                 
                 
                     
                         Geography 
                        
                     
                 
                 
                     
                         Geology 
                        
                     
                 
                 
                     
                         Gender Studies 
                        
                     
                 
                 
                     
                         History 
                        
                     
                 
                 
                     
                         Judaic Studies 
                        
                     
                 
                 
                     
                         Languages 
                        
                     
                 
                 
                     
                         Law 
                        
                     
                 
                 
                     
                         Literature 
                        
                     
                 
                 
                     
                         Mathematics 
                        
                     
                 
                 
                     
                         Nursing and Health Sciences 
                        
                     
                 
                 
                     
                         Performing Arts 
                        
                     
                 
                 
                     
                         Philosophy 
                        
                     
                 
                 
                     
                         Physics 
                        
                     
                 
                 
                     
                         Political Science 
                        
                     
                 
                 
                     
                         Psychology 
                        
                     
                 
                 
                     
                         Religion 
                        
                     
                 
                 
                     
                         Sociology and Social Work 
                        
                     
                  
                            
                 
                     
                         
                        
							 
					 
						 
							 Engaging with the University Libraries
                                 
							 
								 
									
			 
  
		    
			 
				   

 
	 
		 
			 
			        Email Us  

			      Call the Research and Information Services desk @              901-  678-2208  

			     Visit the Research and Information Services Desk   

			      Schedule a research consultation  

			      Text us @ (901) 201-5389  

			       Visit our Research Help Guides  

			        Follow our   Twitter Feed  !  

			        Visit us on   Facebook  !  
			 
			   
		 
		 
			   
			   
		 
		 
			   
			   
		 
	 
 
		    
								 
								
							 
						 
					 
							 
							 
					 
						 
							 Ask me!
                                 
							 
								 
									
          
             
                 
              Ashley Roach-Freiman  
             
             
             
                 
                 
             
             
                 

 
 Schedule Appointment 
 

                 
             
             
                 
                 
             
              Contact:    Website / Blog Page   
             
              Social:    LinkedIn Page   
             
              Subjects:  Archaeology ,  Business and Management ,  English ,  Geography ,  Geology and Geophysics ,  Literature 
             
         
								 
								
							 
						 
					 
							 
                         
                     
                                          
                     
                 
                 
                     
                         
                             
     
                  
					 
						 
							 Choosing an essay topic
                                 
							 
								 
									
			 
				  Choosing an essay topic can be difficult sometimes but there are a few ways to make it easier.    
  There are lots of specialized subject encyclopedias in the Reference collection in the Learning Commons area that can help you get started.    
  Browse through an encyclopedia in a subject area you're interested in or which matches the course subject and see what looks like it would be fun to write about.  Encyclopedias and dictionaries are great at providing general background information as well as providing definitions of terms and concepts in the subject area.  
  Most subject encyclopedias come with a short bibliography attached to each article so you have a head start on creating your biblbiography.  
		    
								 
								
							 
						 
					   
					 
						 
							 Find a book in the UM Libraries
                                 
							 
								 
									
			 
				  
  
  
 Keyword 
 Title 
 Author 
 Subject 
 Call Number 
 ISBN/ISSN Number 
          
		    
								 
								
							 
						 
					   
					 
						 
							 Finding Reference books in the Online Catalog
                                 
							 
								 
									
			 
				  It's easy to find reference books on your topic.  Go to  Quick Search  on our library homepage and enter a general keyword for your topic.  Example: immigration united states.  
  When the results come up look for Location on the left side of the screen in the limit options and select   McWherter Library - 1st Floor - Reference Collection.   
		    
								 
								
							 
						 
					   
					 
						 
							 General Encyclopedias
                                 
							 
								 
									 
                          
                        
			 
				 
						 
					 
				 
					 
					 New Encyclopaedia Britannica
					  
					   Call Number: AE5 .E363 2010  
			  
                         
                        
			 
				 
						 
					 
				 
					 
					 The World Book Encyclopedia
					   by   World Book, Inc Staff
					   Call Number: AE5 .W55 2010  
			  
                         
                        
			 
				 
						 
					 
				 
					 
					 World Book Encyclopedia
					  
					   Call Number: E-book 
						  Tennessee Electronic Library 
						  
			  
                        
                             
                             
								 
								
							 
						 
					  
         
     
                          
                     
                    
                 
                     
                    
                     
                     
                      Next:  Reference E-Books    
                     
                                  
             
         
         
         
             
                 
                     
                         Last Updated:   Nov 3, 2016 3:13 PM                      
                     
                         URL:   http://libguides.memphis.edu/referencebooks                      
                     
                    	    Print Page                      	
                     
                 
                     Login to LibApps                  
             
             
                 
                     Report a problem  
                 
                 
                    
                     Subjects:  
                      Help/How-To Guides  
                                     
                 
                    
                     Tags:  
                      help guide ,  how to  
                                     
             
         
         
        
			 
				 
					 
					    
					    
					 
				 
			          
                              
                                
                 
                

		 
  	 
 
