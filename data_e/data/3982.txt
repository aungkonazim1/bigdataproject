EDPR Faculty - CEPR - University of Memphis    










 
 
 
     



 
    
    
    EDPR Faculty - 
      	CEPR
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Counseling, Educational Psychology   Research
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Counseling  
                          Counseling Psychology  
                          Ed Psych   Research  
                          Faculty  
                          Research  
                      
                      
                         
                            EDPR Menu   
                            
                               
                                  
                                   About EDPR  
                                   Degrees   Certificates  
                                         M.S.  
                                         Online M.S.  
                                         Ph.D.  
                                         Graduate Certificate in Qualitative Research  
                                         Graduate Certificate in Quantitative Research  
                                     
                                  
                                   Courses  
                                   Faculty  
                                   News  
                                   Student Resources  
                                   Admissions  
                                         Apply  
                                         M.S. Application  
                                         M.S. Online Application  
                                         Ph.D. Application  
                                         International Students  
                                         Graduate Assistantships  
                                         Graduate Catalog  
                                     
                                  
                                   Research  
                                         EDPR Colloquia  
                                         Childhood Study Group  
                                         QUIC  
                                         SPIRIT  
                                         Professional Associations  
                                         Statistical Consulting  
                                         Undergraduate Educational Research Team  
                                        
                                          
                                           
                                             
                                               Youth Development   Social Media Group  
                                             
                                           
                                          
                                        
                                     
                                  
                                   View our brochure!  
                                   Like us on Facebook!  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	CEPR
                              	  
                          
                              	EDPR
                              	  
                         EDPR Faculty 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      EDPR Faculty 
                     
                       Alison A. Happel , Ph.D., Georgia State University (2011). Qualitative methodology; Post-structural,
                        post-colonial, and feminist theories; Ecojustice and ecofeminist education.
                      
                     
                       Leigh M. Harrell-Williams , Ph.D., Virginia Tech (2009). Statistics and quantitative research methods; instrument
                        development; Rasch, IRT and SEM applications with educational and psychological data;
                        K-12 math teacher efficacy to teach statistics; assessment of behavioral and emotional
                        risk in K-12 students.
                      
                     
                       Yeh Hsueh , Ed.D., Harvard University (1997). Development of young children; Cross-cultural
                        early childhood education; Development of developmental psychology in the United States;
                        Children and media.
                      
                     
                       Michael M. Mackay , Ph.D., The University of Memphis (2013). Statistical and methodological consultant
                        for the College of Education, Health and Human Sciences. Research interests include
                        employee engagement; student engagement; survey construction and psychometrics; meta-analysis.
                      
                     
                       Christian E. Mueller , Ph.D. University of Kentucky (2006). Developmental trajectories of intrapersonal,
                        interpersonal and environmental factors influencing achievement motivation in at-risk
                        students (e.g., adolescents, gifted, minority); Measurement issues (e.g., Rasch modeling)
                        in education and psychology. 
                      
                     
                       Vicki Sallis Murrell , Ph.D., University of Memphis (2005). Toxic stress and its impact on behavior and
                        learning; Use of constructivism and cognitive-learning theories in online-mediated
                        teaching and learning; Impact of evolving technologies on cognitive processes; Training
                        faculty for online teaching. Program website developer.
                      
                     
                       Susan Naomi Nordstrom , Ph.D. The University of Georgia (2011). Poststructural and post humanist theories,
                        onto-epistemology, Deleuze, feminist new materialism, qualitative research methodology.
                      
                     
                       Ernest A. Rakow , Ph.D., University of Chicago (1974). Teacher evaluation; Innovative uses of statistics.
                        Interim Dean of the College of Education.
                      
                     
                       Karen Weddle-West , Ph.D., University of Tennessee (1982). Cultural diversity; Minority teachers; Child
                        psychology; Infant development; Adolescent pregnancy; At-risk children. Provost of
                        the University of Memphis.
                      
                     
                       Denise L. Winsor , Ph.D., University of Nevada, Las Vegas (2008). Child development; Development in
                        context; Early epistemological thinking; Technology in preschool-age children. Teacher
                        beliefs and the use of strategies for high-order thinking. Liason for undergraduate
                        programs. Program Coordinator for Educational Psychology.
                      
                     
                       Yonghong Jade Xu , Ph.D., University of Arizona (2003). Multivariate statistics and quantitative research
                        methods; Gender disparities in STEM disciplines; Structural equation modeling; Big
                        data analysis with statistical and data mining approaches. Program Coordinator for
                        Educational Research.
                      
                     
                       Chia-chen Yang , Ph.D., University of Wisconsin-Madison (2014). Adolescent development; Social media
                        use, social relationships, and identity development among youth.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         EDPR Menu 
                         
                            
                               
                                About EDPR  
                                Degrees   Certificates  
                                      M.S.  
                                      Online M.S.  
                                      Ph.D.  
                                      Graduate Certificate in Qualitative Research  
                                      Graduate Certificate in Quantitative Research  
                                  
                               
                                Courses  
                                Faculty  
                                News  
                                Student Resources  
                                Admissions  
                                      Apply  
                                      M.S. Application  
                                      M.S. Online Application  
                                      Ph.D. Application  
                                      International Students  
                                      Graduate Assistantships  
                                      Graduate Catalog  
                                  
                               
                                Research  
                                      EDPR Colloquia  
                                      Childhood Study Group  
                                      QUIC  
                                      SPIRIT  
                                      Professional Associations  
                                      Statistical Consulting  
                                      Undergraduate Educational Research Team  
                                     
                                       
                                        
                                          
                                            Youth Development   Social Media Group  
                                          
                                        
                                       
                                     
                                  
                               
                                View our brochure!  
                                Like us on Facebook!  
                            
                         
                      
                      
                      
                         
                            
                                About the Department  
                                
                            
                            
                                Student Organizations in CEPR  
                                
                            
                            
                                Department News  
                                
                            
                            
                                Contact Us  
                                
                            
                         
                      
                      
                      
                      
                        	
                         
                           		
                           
                           
                           
                           		
                            
                              
                               
                                 
                                  
                                     
                                    
                                  
                                 
                                  
                                    
                                  
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     #9 
                                    
                                     BestCollege.com 
                                    
                                     Online Master's in Educational Psychology 
                                    
                                  
                                 
                                  
                                    
                                     #10 
                                    
                                     BestCollegesOnline.org 
                                    
                                     Most Affordable Online Master's in Educational Psychology 
                                    
                                  
                                 
                                  
                                    
                                     #9 
                                    
                                     BestMastersDegrees.com 
                                    
                                     Most Affordable Online Master's in Educational Psychology 
                                    
                                  
                                 									 
                                  
                                    
                                     #14 
                                    
                                     BestMastersinPsychology.com 
                                    
                                     Best Online Master's in Educational Psychology 
                                    
                                  
                                 
                               
                              
                            
                           	
                           
                         
                        
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


