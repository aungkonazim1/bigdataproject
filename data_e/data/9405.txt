Earthworm Program: cleandir overview   
 
 
   Earthworm Program: cleandir overview 
 
 
 
 Earthworm Program: 
cleandir Overview 
 (last revised June 6, 2006)  
 
The  cleandir  program
is a rewrite of the old Menlo cleandir. Cleandir runs as a Windows
service (as opposed to running as a Windows Scheduled Task) and
monitors the directories specified in the configuration file.  The
criteria for deletion are as follows: If any file has a modification
date/time older than the age specified in the configuration file, that
file is deleted.   
 
 This module was contributed from the NEIC Hydra project by John
Patton.
 
  Warning: only usable under Windows.
  
  
    Module Index  |  cleandir Commands 
  
 
 
Contact:  
  Questions? Issues?  Subscribe to the Earthworm Google Groups List.   
  
  
 
 
