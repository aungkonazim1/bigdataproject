Frequently Asked Questions - Undergraduate Admissions and Orientation - University of Memphis    










 
 
 
     



 
    
    
    Frequently Asked Questions - 
      	Undergraduate Admissions and Orientation
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Undergraduate Admissions and Orientation
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Freshmen  
                          Transfer  
                          Readmit  
                          Graduate  
                          Law  
                          International  
                          High School  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Undergraduate Admissions and Orientation
                              	  
                         Frequently Asked Questions 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Frequently Asked Questions 
  What is the mailing address for the Office of Admissions?  
  I have a copies of documents that have been requested by the Office of Admissions.  Can I submit these copies to continue my admission process?  
  Must I readmit if I sat out a semester?  
  How can I find out which courses, taken at my former college, will transfer?  
  I received a letter requiring me to show that I have registered for Selective Service; however, I am not eligible yet.  What should I do?  
  How long will it take for my admission application to be processed?  
  What are the driving directions to the University of Memphis and location of the Office of Admissions?  
  How soon will I know if I’ve been accepted to the University of Memphis?  
  When I check my application status online, I see that I have incomplete items. What does this mean and how can I find out what items are incomplete?  
  I just realized that I submitted my online application with an error. Is there any way I can correct this error online?  
  I receive correspondence from the Office of Admissions via e-mail and regular mail.  Why don’t you send all correspondence via e-mail?  
 
      What is the mailing address for the Office of Admissions?  University of Memphis Office of Admissions 101 Wilder Tower Memphis, TN 38152-3520       I have copies of documents that have been requested by the Office of Admissions.  Can I submit these copies to continue my admission process?  The Office of Admissions requires official documentation for application processing.  Documents are considered official only if sent directly from the issuing institution.  College entrance examination scores (SAT/ACT) will be considered official if they appear on an official high school transcript.  Personal copies of requested documentation cannot be accepted.  Please see  Admission Requirements  for more information        Must I readmit if I sat out a semester?  Students who have sat out one regular semester (fall or spring) must be readmitted to the University.        How can I find out which courses, taken at my former college, may transfer?  Please refer to the online  transfer equivalency tables  to determine which courses  may  transfer.  If you do not find your institution listed you may wish to contact the Office of Admissions at (901) 678-2111 for more information.       I received a letter requiring me to show that I have registered for Selective Service; however, I am not eligible yet.  What should I do?  You may still be admitted without this information, however, when you become eligible and complete your registration for  Selective Service , please notify our office.       How long will it take for my admission application to be processed?  Once you apply online, please allow 2-3 business days for your application to be processed.  After we receive all of the required supporting documents, an admission decision will be made and you will be notified by mail and e-mail.       What are the driving directions to the University of Memphis and location of the Office of Admissions?  Please see the  Visit the Campus  page for information on  directions ,  campus map ,  parking , and arranging a tour.  The Office of Admissions is located in Room 101 of Wilder Tower.  Wilder Tower is located near the intersection of Patterson and Walker Streets.    
      How soon will I know if I’ve been accepted to the University of Memphis?  Only after all outstanding items are received can an admissions decision be made. The University of Memphis requires official documents to make an admissions decision. Please monitor your e-mail (or regular mail) for notification of what outstanding items are needed to continue processing your application. Decisions regarding graduate programs may take a little longer than undergraduate due to the cooperative efforts between the academic department and the Office of Admissions. 
      When I check my application status online, I see that I have incomplete items. What does this mean and how can I find out what items are incomplete?  When you submitted your application, several of the responses automatically triggered additional requests. For example, if you are male between 18-26, you will be asked to verify registration with Selective Service and/or if you indicated that you’ve earned college credit from another accredited institution of higher education, you will be asked to have an official transcript submitted for evaluation, etc…. You will be notified what, if any, items are outstanding after an analyst has reviewed your application – this may take 2-3 business days. 
      I just realized that I submitted my online application with an error. Is there any way I can correct this error online?  No. Once an online application has been submitted, changes can only be made by an admissions analyst. Please contact the Office of Admissions to request the correction. 
      I receive correspondence from the Office of Admissions via e-mail and regular mail.  Why don’t you send all correspondence via e-mail?   The Office of Admissions has a complex communication system that utilizes both e-mail (personal and official UofM) and regular mail. E-mail is the preferred method for corresponding with applicants. If you provided an e-mail address on the application for admission, we will communicate with you at that address.  If you are accepted, instructions for setting up your UofM e-mail account will be provided with your official acceptance letter (e-mail). It is important that you set up your UofM e-mail account. From this point forward, all correspondence will be sent to your official UofM e-mail account.                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Meet Your Counselor  
                               Our team can help you navigate the college planning process. 
                            
                            
                                #MEMBOUND?  
                               Sign up as a True Blue Tiger to receive information and updates. 
                            
                            
                                Campus Visits and Events  
                               Schedule a tour and view upcoming events. 
                            
                            
                                Contact Us  
                               Question?  The Office of Undergraduate Admissions and Orientation can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


