Educational Psychology &amp; Research - CEPR - University of Memphis    










 
 
 
     



 
    
    
    Educational Psychology   Research - 
      	CEPR
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Counseling, Educational Psychology   Research
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Counseling  
                          Counseling Psychology  
                          Ed Psych   Research  
                          Faculty  
                          Research  
                      
                      
                         
                            EDPR Menu   
                            
                               
                                  
                                   About EDPR  
                                   Degrees   Certificates  
                                         M.S.  
                                         Online M.S.  
                                         Ph.D.  
                                         Graduate Certificate in Qualitative Research  
                                         Graduate Certificate in Quantitative Research  
                                     
                                  
                                   Courses  
                                   Faculty  
                                   News  
                                   Student Resources  
                                   Admissions  
                                         Apply  
                                         M.S. Application  
                                         M.S. Online Application  
                                         Ph.D. Application  
                                         International Students  
                                         Graduate Assistantships  
                                         Graduate Catalog  
                                     
                                  
                                   Research  
                                         EDPR Colloquia  
                                         Childhood Study Group  
                                         QUIC  
                                         SPIRIT  
                                         Professional Associations  
                                         Statistical Consulting  
                                         Undergraduate Educational Research Team  
                                        
                                          
                                           
                                             
                                               Youth Development   Social Media Group  
                                             
                                           
                                          
                                        
                                     
                                  
                                   View our brochure!  
                                   Like us on Facebook!  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	CEPR
                              	  
                         
                           	EDPR
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Educational Psychology   Research 
                     
                       The Educational Psychology and Research (EDPR) Program  consists of two interrelated majors, Educational Psychology and Educational Research,
                        both offering doctorate (Ph.D.) and master's (M.S.) programs.  Research is a high priority  in both concentration areas, and student research, using an intensive research apprenticeship
                        model, is a focus of our programs. Our setting in an  urban area  offers rich diversity in research opportunities that include working with local schools
                        and community organizations.
                      
                     
                      Students in  Educational Psychology  can focus on  human development  within the domains of early childhood, adolescence, adulthood, and aging, focusing
                        on the cultural nature of these developmental processes in a changing society. Students
                        can also choose to study  cognition and motivation  in the domains of learning, critical thinking, self-regulation, and giftedness.
                      
                     
                      Students in  Educational Research  can choose a  quantitative  approach including measurement, statistical methods, research design, and institutional
                        research. They can also focus on a  qualitative  approach such as contemporary theories, strategies, research design, data collection
                        and analysis.
                      
                     
                       Our  Master's of Science  (M.S.) program  prepares students with diverse backgrounds in education-related fields. Upon graduation,
                        many graduates from the M.S. program advance their professional careers. With a strong
                        emphasis on research in its coursework and daily activities, the program also prepares
                        M.S. degree students for entering the Ph.D. program.
                      
                     
                       The  Doctor of Philosophy  (Ph.D.) program  in Educational Psychology and Research prepares graduates for teaching and research
                        at the university level, senior administrative positions, educational research, and
                        applied research in human services. Past students from the Ph.D. program have become
                        university professors, senior university administrators, and senior researchers and
                        analysts in both public and private sectors.
                      
                     
                        Please see our Graduate Student Handbook for detailed information about our programs
                              and processes.    
                     
                      As you browse our website, feel free to  contact us  with any questions that you might have. We hope that you will also learn more about
                        our  department , our  college , and our  university .
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         EDPR Menu 
                         
                            
                               
                                About EDPR  
                                Degrees   Certificates  
                                      M.S.  
                                      Online M.S.  
                                      Ph.D.  
                                      Graduate Certificate in Qualitative Research  
                                      Graduate Certificate in Quantitative Research  
                                  
                               
                                Courses  
                                Faculty  
                                News  
                                Student Resources  
                                Admissions  
                                      Apply  
                                      M.S. Application  
                                      M.S. Online Application  
                                      Ph.D. Application  
                                      International Students  
                                      Graduate Assistantships  
                                      Graduate Catalog  
                                  
                               
                                Research  
                                      EDPR Colloquia  
                                      Childhood Study Group  
                                      QUIC  
                                      SPIRIT  
                                      Professional Associations  
                                      Statistical Consulting  
                                      Undergraduate Educational Research Team  
                                     
                                       
                                        
                                          
                                            Youth Development   Social Media Group  
                                          
                                        
                                       
                                     
                                  
                               
                                View our brochure!  
                                Like us on Facebook!  
                            
                         
                      
                      
                      
                         
                            
                                About the Department  
                                
                            
                            
                                Student Organizations in CEPR  
                                
                            
                            
                                Department News  
                                
                            
                            
                                Contact Us  
                                
                            
                         
                      
                      
                      
                      
                        	
                         
                           		
                           
                           
                           
                           		
                            
                              
                               
                                 
                                  
                                     
                                    
                                  
                                 
                                  
                                    
                                  
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     #9 
                                    
                                     BestCollege.com 
                                    
                                     Online Master's in Educational Psychology 
                                    
                                  
                                 
                                  
                                    
                                     #10 
                                    
                                     BestCollegesOnline.org 
                                    
                                     Most Affordable Online Master's in Educational Psychology 
                                    
                                  
                                 
                                  
                                    
                                     #9 
                                    
                                     BestMastersDegrees.com 
                                    
                                     Most Affordable Online Master's in Educational Psychology 
                                    
                                  
                                 									 
                                  
                                    
                                     #14 
                                    
                                     BestMastersinPsychology.com 
                                    
                                     Best Online Master's in Educational Psychology 
                                    
                                  
                                 
                               
                              
                            
                           	
                           
                         
                        
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/15/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


