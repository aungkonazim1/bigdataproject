Accessing the Import and Export tool | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Accessing the Import and Export tool | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    ePortfolio    »    Understanding the main pages of ePortfolio   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Accessing the Import and Export tool                       
        
        
       
        
     
               
		Click   ​ ePortfolio  on the navbar or from the My Settings widget.
	 
	 
		On the My Items page, click  Go to Import/Export page  from the More Actions button.
	 
      Audience:     Learner       

    
           

                   ‹ Understanding the My Items page 
        
                   up 
        
                   Understanding ePortfolio settings › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        ePortfolio  
  
      Understanding the main pages of ePortfolio    Understanding the Dashboard    Understanding the Explore page    Understanding the My Items page    Accessing the Import and Export tool    Understanding ePortfolio settings      Adding artifacts    Using reflections    Creating presentations    Creating collections    Understanding assessment types in ePortfolio    Understanding the basic concepts in sharing    Importing and exporting items    Sharing items within courses    Using form templates    Integrating ePortfolio with Content     Assessing ePortfolio content    
                  
           
         

       
     

    
    
    
   
 
   
 
