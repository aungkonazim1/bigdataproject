Strategic Renewal - Office of the President - University of Memphis    










 
 
 
     



 
    
    
    Strategic Renewal - 
      	Office of the President
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Office of the President
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Mission and Plan  
                          Communications  
                          Leadership  
                          Contact  
                      
                      
                         
                            Communications from the President's Office   
                            
                               
                                  
                                   President's Report  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Office of the President
                              	  
                          
                              	Communications
                              	  
                         Strategic Renewal 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                       Strategic Renewal as the FedEx Institute of Technology Moves into its Second Decade  
                     
                      As the FedEx Institute of Technology moves into its second decade of operations as
                        the front door to the University's research infrastructure and capabilities, strategic
                        renewal and an expansion of its mission are in the works at the University of Memphis.
                      
                     
                      President M. David Rudd's new vision for the Institute emphasizes a stronger technology
                        focus, a national profile in terms of being at the forefront of technology developments
                        and partnerships with global technology organizations. It includes serving as the
                        spark to bring the latest technology innovations to the region, stronger community
                        partnerships and an emphasis on improved opportunities for commercialization. He also
                        sees the Institute as a key mechanism to help break down the traditional silos that
                        can stifle interdisciplinary research and faculty collaborations at universities.
                      
                     
                      New key areas of focus will include the expanding biologistics sector and cyber security
                        testing, which has quickly emerged as a national security and business challenge.
                        In both cases, it will be building upon capabilities that the University has put in
                        place over the last decade. The push into biologistics will see the University's bioengineering,
                        freight transportation, bioinformatics and biomaterials groups coming together to
                        innovate in research and launch the first biologistics graduate program in the country.
                      
                     
                      None of these constituent groups was in place when the FedEx Institute of Technology
                        first opened its doors. In the case of cyber security testing, the University will
                        be building upon two core competencies in which it has national strengths: systems
                        testing and information assurance. Its Systems Testing Excellence Program is the nation's
                        largest academic research group in software testing, and since 2006 has been helping
                        the likes of FedEx and the U.S. Department of Defense to advance industry best practices
                        through research and training. Similarly, its Center for Information Assurance has
                        been one of the most active research groups at the University and enjoys designations
                        as a National Center for Academic Excellence in Information Assurance Education and
                        Research.
                      
                     
                      The FedEx Institute of Technology will also be strengthening its relationships with
                        regional chief information and technology officers. Dr. Rudd issued this call to technology
                        leaders in the community: "If you are playing such a technology leadership role in
                        your organization, please consider this your open invitation to connect with and be
                        a part of the FedEx Institute of Technology as we work together to make Memphis a
                        vibrant technology hub for the future."
                      
                     
                      The reenergizing and strategic renewal of the Institute will see the University appointing
                        Dr. Jasbir Dhaliwal, who currently oversees its portfolio of 111 graduate programs
                        (that include doctoral, masters and graduate certificate programs), stepping up to
                        lead as the U of M's new Chief Innovation Officer and Executive Director of the FIT.
                        Consistent with an effort to enhance efficiency, Dr. Dhaliwal has agreed to continue
                        to serve as interim dean of the Graduate School. As the new Chief Innovation Officer,
                        he will report directly to the president, join the President's Council, and his appointment
                        will see the University's nearly 4,000 doctoral and masters students becoming closely
                        involved with the research and outreach activities of the Institute as it builds on
                        past successes.
                      
                     
                      Technology transfer efforts, led by Dr. Kevin Boggs, and supporting entrepreneurship,
                        remain priorities which will be now be merged into the larger role of the Institute
                        on campus and in the community. The Crews Center for Entrepreneurship will be seeking
                        distinct new partnerships within the regional innovation ecosystem to boost technology
                        commercialization by students and faculty. Dr. Dhaliwal noted that, "Besides forging
                        a tighter technology relationship with FedEx, the Institute will also function as
                        an additional catalyst to further energize University efforts to grow its funded research
                        base given recent successes such as the NIH-funded Mobile Sensor Data-to-Knowledge
                        Center of Excellence in mobile health. This will involve working closely both with
                        internal research support services professionals led by Dr. Andy Meyers, the University's
                        interim vice president for Research, and with external partners such as Memphis Bioworks,
                        St. Jude Children's Research Hospital and others."
                      
                     
                      David Rudd | President 
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Communications from the President's Office 
                         
                            
                               
                                President's Report  
                            
                         
                      
                      
                      
                         
                            
                                Dashboard  
                               UofM steps towards success 
                            
                            
                                FOCUS Act  
                               Independent Governing Board Updates 
                            
                            
                                President's Blog  
                               News, stories, videos and more 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


