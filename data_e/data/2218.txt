PETE-MS - SHS - University of Memphis    










 
 
 
     



 
    
    
    PETE-MS - 
      	SHS
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			School of Health Studies
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          Students  
                          Faculty  
                          Research  
                          Centers  
                          News  
                          Camps  
                      
                      
                         
                            SHS Programs   
                            
                               
                                  
                                   Undergraduate  
                                         Dietetics  
                                         Exercise, Sport and Movement Sciences  
                                         Health Sciences  
                                         Healthcare Leadership  
                                         Physical Education Teacher Education  
                                     
                                  
                                   Graduate  
                                        
                                         Exercise, Sport and Movement Science  
                                        
                                         Health Promotion  
                                        
                                         Physical Education Teacher Education  
                                        
                                         Clinical Nutrition  
                                        
                                         Environmental Nutrition  
                                        
                                         Nutrition Science  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	SHS
                              	  
                          
                              	Programs
                              	  
                         PETE-MS 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Physical Education Teacher Education (MS) 
                     
                      The master’s degree program in Physical Education Teacher Education (PETE) provides
                        a course of study at the graduate level regarding research on teaching in physical
                        education and research on teacher education in physical education. This degree option
                        focuses on the theoretical, methodological, and research aspects of teaching and learning
                        in physical education. The program emphasizes contemporary models of instruction,
                        curriculum, assessment, and supervision in physical education. Some other key aspects
                        of the program are:
                      
                     
                      
                        
                         Is a 36 hour program, inclusive of 6 hours of faculty-directed research (thesis or
                           project).
                         
                        
                         Provides a research-centered program for students interested in pursuing advanced
                           graduate study (PhD) in physical education or physical activity in related disciplines
                           (e.g., coaching, health education, community-settings).
                         
                        
                         Provides a comprehensive overview of the PETE field for students who are interested
                           in pursuing PhD studies.
                         
                        
                         Provides a strong focus on research methods and statistics relevant to physical education,
                           promoting research understanding regardless of career goal.
                         
                        
                         Provides freedom to students to select elective courses specific to their personal
                           interests and career goals.
                         
                        
                      
                     
                       MAJOR  
                     
                       Physical Education Teacher Education  
                     
                      
                        
                         Concentration: none 
                        
                         Degree: MS 
                        
                          PETE MS Degree Sheet  
                        
                          PETE MS Suggested Program Schedule  
                        
                          TEP Application  
                        
                          Grad Catalog  
                        
                      
                     
                      Career Opportunities 
                     
                      Career opportunities include advancement in the teaching field, such as public schools,
                        colleges and universities and related education fields in the community. The M.S.
                        degree will provide an introduction to research and a knowledge base from which to
                        pursue advanced study leading to a doctorate degree in physical education. The degree
                        will assist students to become informed leaders in the field of teaching physical
                        education and health. In addition, this program of study will provide a range of research
                        skills that could provide students with the basis for research and evaluation of programs.
                        Depending on their chosen path, students will gain experience in many aspects of physical
                        education research. This may include recruitment and screening of subjects, equipment
                        operation, data collection and analysis, program evaluation, grant and manuscript
                        preparation, and oral and written presentation of research findings.
                      
                     
                      Students are also well-prepared to begin employment within the following areas: 
                     
                      Corporate Wellness Health Club Facility Management Personal Training Group Fitness Instruction Fitness and Wellness Coaching Strength and Conditioning Coaching
                      
                     
                      Physical Education Teacher Education (MAT) 
                     
                      A licensure option in physical education is currently available through the Master
                        of Arts in Teaching (M.A.T.) degree program in Instruction and Curriculum Leadership.
                        The Licensure concentration is being offered in order to meet the ever-increasing
                        demand from students entering the graduate program who wish to pursue a professional
                        career in teaching. The option provides students with a foundation in Physical Education
                        curriculum and instruction, combined with a theoretical understanding of assessment
                        and personalized learning technologies within the specific content area. The program
                        will prepare students for entry-level positions in K-12 public school districts as
                        well as private schools.
                      
                     
                      Please contact  LaRuth Lofties  in ICL with any questions regarding the MAT degree program.
                      
                     
                        
                     
                     
                     
                     	
                       
                
                
                   
                      
                         SHS Programs 
                         
                            
                               
                                Undergraduate  
                                      Dietetics  
                                      Exercise, Sport and Movement Sciences  
                                      Health Sciences  
                                      Healthcare Leadership  
                                      Physical Education Teacher Education  
                                  
                               
                                Graduate  
                                     
                                      Exercise, Sport and Movement Science  
                                     
                                      Health Promotion  
                                     
                                      Physical Education Teacher Education  
                                     
                                      Clinical Nutrition  
                                     
                                      Environmental Nutrition  
                                     
                                      Nutrition Science  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply to our Undergraduate Programs  
                               Submit your application today 
                            
                            
                                Apply to our Graduate Programs  
                               Earn your Masters Degree in one of our six programs! 
                            
                            
                                Contact Us  
                               Have questions? Call or email today! 
                            
                            
                                TIGUrS Garden  
                               Check out our on-campus urban garden 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/21/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


