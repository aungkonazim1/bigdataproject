Communications from the President's Office - Office of the President - University of Memphis  Find archived letters, presentations, Town Hall meeting notes and other communications from the President’s Office at the University of Memphis.  










 
 
 
     



 
    
    
    Communications from the President's Office - 
      	Office of the President
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Office of the President
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Mission and Plan  
                          Communications  
                          Leadership  
                          Contact  
                      
                      
                         
                            Communications from the President's Office   
                            
                               
                                  
                                   President's Report  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Office of the President
                              	  
                         
                           	Communications
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Communications from the President's Office 
                     
                       Fall 2017  
                     
                       December   
                     
                      
                        
                          Legislative Proposals Update  
                        
                      
                     
                       November  
                     
                      
                        
                          Funding Level Recommendations  
                        
                          UofM ot Collaborate with St. Jude/ALSAC to increase Course Offerings  
                        
                      
                     
                       October  
                     
                      
                        
                          Strengthening Advocacy  
                        
                          UofM Quality Indicator Score  
                        
                          Paid Parental Leave  
                        
                          University of Memphis Research Foundation Announces Student-Operated FedEx Call Center  
                        
                          Pause to Grieve  
                        
                      
                     
                       September  
                     
                      
                        
                          University of Memphis Magazine Fall 2017  
                        
                          Strategic Planning Group Named  
                        
                          Reporting Fraud, Waste or Abuse of University Resources  
                        
                          Campus Update  
                        
                          University Statement Concerning Deferred Action for Childhood Arrivals (DACA)  
                        
                      
                     
                       August  
                     
                      
                        
                          Welcome Back  
                        
                          General Faculty Meeting  
                        
                          Charlottesville  
                        
                      
                     
                       Spring - Summer 2017  
                     
                       July  
                     
                      
                        
                          Alumni and Faculty House  
                        
                          Update on Summer 2017  
                        
                          Your Work is Appreciated  
                        
                          Adjunct Faculty Parking Rates  
                        
                      
                     
                       June  
                     
                      
                        
                          Gains in Affordability at the UofM  
                        
                          Board of Trustees Meeting Update  
                        
                          Progress on Retention and Completion  
                        
                      
                     
                       May  
                     
                      
                        
                          Taylor Mayberry Nominated for Student Trustee  
                        
                          Legislative Update  
                        
                          Student Wellness and Fitness Center Working Group Announced  
                        
                      
                     
                       April  
                     
                      
                        
                          UofM Board of Trustees Committee Membership Announced  
                        
                          Salary Pool Recommendations for AY 2017-2018  
                        
                      
                     
                        
                     
                       March  
                     
                      
                        
                          Campus Update  
                        
                          Inaugural Board of Trustees Meeting  
                        
                          Immigration Executive Order  
                        
                          U.S. News   World Report Graduate Program Rankings  
                        
                      
                     
                       February  
                     
                      
                        
                          Thank You  
                        
                          Update on Campus Building Projects, Tuition and Fees  
                        
                          Helen Hardin Honors College  
                        
                          Governor's Proposed Budget  
                        
                      
                     
                       January  
                     
                      
                        
                          Recent Executive Order Regarding Immigration  
                        
                          Update on 2017 Legislative Session  
                        
                          Martin Luther King Jr. Holiday  
                        
                          UofM Global  
                        
                      
                     
                       Fall 2016   
                     
                       December  
                     
                      
                        
                          Season's Greetings  
                        
                          THEC Scorecard Update  
                        
                          UofM Global Update  
                        
                          Center for International Education Services  
                        
                          Update on Online Programs and Rankings  
                        
                      
                     
                       November  
                     
                      
                        
                          Campus Update: President's Report 2016  
                        
                          Our Campus Community  
                        
                          TIF District Update  
                        
                      
                     
                       October  
                     
                      
                        
                          Performance Funding Data  
                        
                          Governor Haslam Announces Governing Board for University of Memphis  
                        
                          Campus Response to Recent Incidents  
                        
                          Livability in Memphis  
                        
                      
                     
                       September  
                     
                      
                        
                          Thank You - Free Speech  
                        
                          Campus Update  
                        
                          University of Memphis Magazine - Veterans Stories  
                        
                      
                     
                       August  
                     
                      
                        
                          Enrollment Across Tennessee  
                        
                          A Note to Parents  
                        
                          Welcome Back!  
                        
                          UNDC and TIF  
                        
                      
                     
                       Spring-Summer 2016  
                     
                       July  
                     
                      
                        
                          UofM Diversity Council  
                        
                          Campus Response to Recent Violence  
                        
                          Campus Updates  
                        
                      
                     
                       June  
                     
                      
                        
                          Faculty and Staff Compensation Updates  
                        
                          TBR Approves Tuition Increase for 2016-17  
                        
                          Reflection and Remembrance  
                        
                          Summer Enrollment  
                        
                      
                     
                       May  
                     
                      
                        
                          Legislation Allowing Permitted Handguns on Campus  
                        
                          Spring 2016 Memphis Magazine  
                        
                          End of Semester Appreciation  
                        
                          Legislation Allowing Permitted Handguns on Campus Update  
                        
                      
                     
                       April  
                     
                      
                        
                          Advocate for the UofM  
                        
                          Community Engagement Task Force  
                        
                          Proposed Legislation Allowing Permitted Handguns on Campus  
                        
                      
                     
                       March  
                     
                      
                        
                          Transition Taskforce  
                        
                          Update on Independent Governing Board  
                        
                      
                     
                       February  
                     
                      
                        
                          Transition to Strategic Resource Investment  
                        
                          Office of Inclusion and Diversity  
                        
                          Governor's Budget Recommendations  
                        
                      
                     
                       January  
                     
                      
                        
                          Year of Service Update  
                        
                          Spring Update  
                        
                          Centennial Place Video from Dr. Rudd  
                        
                      
                     
                       Fall 2015  
                     
                        December   
                     
                      
                        
                          SACSCOC Reaffirmation  
                        
                          National Science Foundation Rankings  
                        
                          The Future of Our Great University  
                        
                      
                     
                       November  
                     
                      
                        
                          THEC Funding Recommendations for 2016-2017  
                        
                          In Memory of U.S. Senator Fred Thompson  
                        
                          Doing is Preserving  
                        
                      
                     
                        October   
                     
                      
                        
                          Doing is Mentoring  
                        
                          MD2K's First Year Update  
                        
                          Performance Funding Score  
                        
                          Tennessee's Next National Research University  
                        
                      
                     
                        September   
                     
                      
                        
                          UofM Increase in Research Awards  
                        
                      
                     
                        August   
                     
                      
                        
                          State Facilities Maintenance  
                         
                        
                      
                     
                       Spring-Summer 2015  
                     
                        July   
                     
                      
                        
                          Fundraising Update  
                        
                          Message to Campus Community  
                        
                      
                     
                        June   
                     
                      
                        
                          UofM Student Experience  
                        
                          Annual Fundraising Update  
                        
                          Enrollment Update  
                        
                          Legislation that directly impacts Tennessee Board of Regents Institutions  
                        
                      
                     
                        May     
                     
                      
                        
                          Update on Summer Initiatives  
                        
                          May 2015 Commencement  
                        
                          UofM Safety compared to SEC Schools  
                        
                          The Reason our Border County Legislation Makes a Difference  
                        
                          Legislative Update  
                        
                      
                     
                        April    
                     
                      
                        
                          Thank You  
                        
                      
                     
                        March   
                     
                      
                        
                          Strategic Renewal as the FedEx Institute of Technology Moves into its Second Decade  
                        
                          Veteran Student Enrollment  
                        
                      
                     
                        February   
                     
                      
                        
                          An update on the UofM's MD2K Center of Excellence by Dr. Santosh Kumar  
                        
                          Application Activity and 250-mile Radius Program  
                        
                      
                     
                        January   
                     
                      
                        
                          UofM Minimum Salary Increase  
                        
                          Success at the Heart of Our Mission  
                        
                      
                     
                        
                     
                       Fall 2014  
                     
                      
                        
                          Letter from President Rudd - 6/2/2014  
                        
                          M. David Rudd Named President of The University of Memphis - 5/1/2014  
                        
                      
                     
                        
                     
                       Community Presentations  
                     
                      
                        
                          Fall 2013 Town Hall Meeting  
                        
                      
                     
                         
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Communications from the President's Office 
                         
                            
                               
                                President's Report  
                            
                         
                      
                      
                      
                         
                            
                                Dashboard  
                               UofM steps towards success 
                            
                            
                                FOCUS Act  
                               Independent Governing Board Updates 
                            
                            
                                President's Blog  
                               News, stories, videos and more 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/1/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


