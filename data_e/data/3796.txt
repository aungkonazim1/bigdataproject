ILS &#8211; On Legal Grounds | Memphis Law Events &amp; Announcements    
 

 
	 
	 
	 
	 
	
	 ILS   On Legal Grounds | Memphis Law Events   Announcements 
 

 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 










 
 
  
 
 
    

 

 

 

	 
	
				
		 
			 
				
				 
					     
					 On Legal Grounds | Memphis Law Events   Announcements 									 
				
									 
						    
						   
						    Student Organizations 
 
	  AWA  
	  BLSA  
	  Bus.   Tax Law Society  
	  Christian Legal Society  
	  Federalist Society  
	  Health Law Society  
	  HLSA  
	  Honor Council  
	  ILS  
	  Law Review  
	  Memphis Law +1  
	  Moot Court Board  
	  OutLaw  
	  PAD  
	  PALS  
	  SELS  
	  SBA  
	  SGA  
	  Street Law  
	  TACDL  
 
 
  Law School Announcements 
 
	  Academic Affairs 
	 
		  Academic Affairs Calendar  
	 
 
	  Academic Success Program 
	 
		  Academic Success Program Calendar  
	 
 
	  Career Services Office 
	 
		  Career Services Office Calendar  
	 
 
	  Information Technology 
	 
		  Information Technology Calendar  
	 
 
	  Law Admissions, Recruiting,   Scholarships 
	 
		  Law Admissions, Recruiting, and Scholarships Calendar  
	 
 
	  Law School Registrar 
	 
		  Law School Registrar Calendar  
	 
 
	  Library 
	 
		  Library Calendar  
	 
 
	  Office of the Dean 
	 
		  Office of the Dean Calendar  
	 
 
	  Office of Diversity 
	 
		  Office of Diversity Calendar  
	 
 
	  Pro Bono Office 
	 
		  Pro Bono Office Calendar  
	 
 
	  Student Affairs 
	 
		  Student Affairs Calendar  
	 
 
 
 
  Blog Information  
  Writing Competitions  
  Events  
  
					  
								
			  
		  
		
	  
	
	 
		 			
			 
				 
 

	 

			   Category:  ILS  

	
  	
	 		
		
				
				
			 
				 				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Career Services Office  /  ILS  
			 24 Oct, 2017 
		  
		
		 
			  Fighting Terrorism Through Civil Litigation,   Thursday 10/26, Noon, Room 244 
		  
		
				 				
			 Memphis Law Alum Michael Elsner ( 97) uses the U.S. civil justice system to seek social change and improved protection of Americans at    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  ILS  /  Student Affairs  /  Student Organizations  
			 5 Oct, 2017 
		  
		
		 
			 Tomorrow, in rm. 127 at Noon ILS and the Study Abroad Office 
		  
		
				 				
			 Learn about how they can make this dream come true for you! 
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  ILS  /  Student Affairs  
			 20 Sep, 2017 
		  
		
		 
			 The International Law Society presents, DACA: Past, Present   Future-This FRIDAY, SEPT. 22 at NOON in WADE 
		  
		
				 				
			 The International Law Society presents the first LUNCH   LEARN, DACA: Past, Present   Future-This FRIDAY, SEPT. 22 at NOON in    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  ILS  /  Tennessee Association of Criminal Defense Lawyers  
			 19 Sep, 2017 
		  
		
		 
			 TACDL Interest Meeting This Friday Sept. 22, at Noon in Wade 
		  
		
				 				
			 TACDL will be holding our first interest meeting this Friday, the 22nd, at noon in Wade Auditorium. TACDL will describe the    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  ILS  
			 11 Sep, 2017 
		  
		
		 
			 ILSA Has Changed its Name to ILS (International Law Society) and is Now Accepting Interest Statements for Newly Created Officer Positions 
		  
		
				 				
			 September 10, 2017,  ILSA has changed its name to ILS (International Law Society) and is now accepting interest statements for newly    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  ILS  
			 30 Aug, 2017 
		  
		
		 
			 INTERNATIONAL LAW Student Association (ILSA) Interest Meeting 9/8/17 @noon in 127 
		  
		
				 				
			 The INTERNATIONAL LAW Student Association (ILSA) will have its first interest meeting on Friday, September 8, 2017, at noon, in Room    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  ILS  /  Student Organizations  
			 17 Apr, 2017 
		  
		
		 
			 ILSA ELECTION TIME!! 
		  
		
				 				
			 Hello ILSA members! We are accepting Statements of Interest for the 2017-2018 Executive Board.  Please submit a statement for each position    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  ILS  /  Student Organizations  
			 20 Sep, 2016 
		  
		
		 
			 ILSA Debate Watch 
		  
		
				 				
			 This year ILSA will host a debate watch for each of the Presidential debates in Wade. The first debate is Monday,    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  ILS  /  Student Organizations  
			 20 Sep, 2016 
		  
		
		 
			 ILSA TWEN Page and Elections 
		  
		
				 				
			 ILSA would like to thank everyone that came to last week s interest meeting. We are pleased to announce that our TWEN    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  ILS  /  Student Organizations  
			 12 Sep, 2016 
		  
		
		 
			 International Law Students  Association Interest Meeting 
		  
		
				 				
			 The International Law Students  Association (ILSA) will host their interest meeting tomorrow, Tuesday, September 13, at noon in Room 224. Please    
		  
				
	  	
  					   			  
		
			 
			 
 Page 1 of 4  1  2  3  4    
 	  
			
				
	  
	
  


	 
		
		    
		
		 
			
						 
				 Follow: 
							 
						
						
						
			   Law School Website                     		 		 Recent Posts 		 
					 
				 1L Minority Clerkship Program   Reception (Chattanooga) 
						 
					 
				 New resources! 
						 
					 
				 Community Legal Center Volunteer Opportunity 
						 
					 
				 Law School Bookstore 2 Day Sale   Dec. 6th and 7th! 
						 
					 
				 St. Jude Memphis Marathon/Downtown Street Closures 
						 
				 
		 		  Archives 		 
			  December 2017  
	  November 2017  
	  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
		 
		   Categories 		 
	  Academic Affairs 
 
	  Academic Success Program 
 
	  AWA 
 
	  BLSA 
 
	  Career Services Office 
 
	  Christian Legal Society 
 
	  Experiential Learning 
 
	  Federal Bar Association 
 
	  Federalist Society 
 
	  Health Law Society 
 
	  HLSA 
 
	  Honor Council 
 
	  ILS 
 
	  Information Technology 
 
	  Law Admissions, Recruiting,   Scholarships 
 
	  Law Review 
 
	  Law School Announcements 
 
	  Law School Registrar 
 
	  Library 
 
	  Memphis Law +1 
 
	  Mock Trial 
 
	  Moot Court Board 
 
	  National Lawyer s Guild 
 
	  Office of Diversity 
 
	  Office of the Dean 
 
	  OutLaw 
 
	  Outside Organizations 
 
	  PAD 
 
	  PALS 
 
	  Pro Bono Office 
 
	  SBA 
 
	  Sports   Entertainment Law Society 
 
	  Street Law 
 
	  Student Affairs 
 
	  Student Organizations 
 
	  Tennessee Association of Criminal Defense Lawyers 
 
	  Writing Center 
 
	  Writing Competitions 
 
		 
 			
		  
		
	  

	
 
	
	    
	
	 
		
				 
			 More 
		 
				
				
		  Search   
	 
		 
	 
    Login 					 
					 
										 
					 
					 
					  Username:  
					  
					  Password:  
					  
										  
					  Remember me  
															   
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
											  Don't have an account?  
										  Lost your password?  
					 
					
					 
					
										 
				
					 
					  Choose username:  
					  
					  Your Email:  
					  
										   
					 
					 
					 
					 
					 
					 
					 
					 
					   Have an account?   
					 
										
					 
			
					 
					  Enter your username or email:  
					  
										   
					 
					 
					 
					 
					 
					 
					 
					 
					   Back to login   
					 
					
					 
					
										  
					   
					 
					   Subscribe 	
	
 
	 
		 
		 
		 		
		
	 
		
		 
					  

		
 
		 Email Address *  
	 
  
 
		 First Name 
	 
  
 
		 Last Name 
	 
  
 
		 Middle Name 
	 
  			 
				* = required field			  
			
		 
			 
		  
	
	
				
	  
	  
  
	    Blog Feedback                     		
	  
	
  	

				  
			  			
		  
	  

	 
		
				
				
				
		 
			 
				
				    
				
				 
					
					 
						
												
						 
															 On Legal Grounds | Memphis Law Events   Announcements   2017. All Rights Reserved. 
													  
						
												 
							 Powered by  WordPress . Theme by  Alx . 
						  
												
					 
					
					 	
											 
				
				  
				
			  
		  
		
	  

  

 		
		










 
 
 