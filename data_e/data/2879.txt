Photo and Video - Marketing and Communication - University of Memphis    










 
 
 
     



 
    
    
    Photo and Video - 
      	Marketing and Communication
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Marketing and Communication
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          The Brand  
                          Our Team  
                          Resources  
                          Social Media  
                          Web  
                          Events  
                          News  
                      
                      
                         
                            Resources   
                            
                               
                                   Branded Templates  
                                         PowerPoint Slides  
                                         Stationery  
                                         Email Templates  
                                         Email Signatures  
                                         Employee Announcements  
                                         Retirement Receptions  
                                         Flyers   Posters  
                                         Cards   Certificates  
                                         Nametags  
                                         Place Cards   Table Tents  
                                         Business Cards  
                                     
                                  
                                   Photo and Video Resources  
                                   TV and Radio Appearances  
                                   Project Request Form  
                                   Affirmative Action Statements  
                                   Collateral Coding Statements  
                                   Approved Vendors  
                                   Frequently Asked Questions  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Marketing and Communication
                              	  
                          
                              	Resources
                              	  
                         Photo and Video 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Photo and Video Resources 
                     
                      Our staff photographer and videographer are available by appointment only and focus
                        on general campus photos/videos and high profile events. If your event will feature
                        our President, Provost or a local/national dignitary, it is suggested that you contact
                        the University photographer and/or videographer to ensure your event is on their calendars.
                        Their ability to shoot your event is also at the discretion of the Vice President
                        for External Relations and Director of Marketing and Communication.
                      
                     
                      For other events, our office has two digital SLR cameras that we can loan you. To
                        borrow a camera, please contact  marketing@memphis.edu  or 901.678.2843 for details and availability. These cameras are available on a first-come,
                        first-served basis and must be returned to us fully charged and in proper working
                        condition.
                      
                     
                      Schedule a Headshot 
                     
                      Headshots are for use in connection with the University of Memphis, only. Please  click here  to select a date and time to schedule your headshot. Available dates for headshots
                        will be posted two weeks out. Check back periodically to view future dates.
                      
                     
                      
                     
                      Photo and Video Archive 
                     
                      If you are looking for specific UofM videos or would like to access any of our public
                        photos for your use (in connection with the University of Memphis only), please use
                        one of the resources below.
                      
                     
                      
                        
                         YouTube:  www.youtube.com/user/uofmemphisvideos  
                        
                         Flickr:
                           
                            
                              
                               Photographs on or after October 6, 2015 -  https://www.flickr.com/photos/uofm_2  
                              
                               Photographs before October 6, 2015 -  www.flickr.com/photos/u_of_m/sets/  
                              
                            
                           
                         
                        
                      
                     
                      Photo Release Form 
                     
                      If you are taking photos at a public event, a photo release form is generally not
                        required. If you are staging photos for a project, it is a good idea to have your
                        models sign this  Photo Release form  you can keep on file.
                      
                     
                       Marketing and Communication receives many requests to film farewell, tribute and retirement
                           receptions. While every retirement celebration is a special occasion, we are a large
                           campus, and staffing limitations make it impossible to film each event. Unfortunately,
                           we can grant these requests only for University deans or members of the President's
                           Council.  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Resources 
                         
                            
                                Branded Templates  
                                      PowerPoint Slides  
                                      Stationery  
                                      Email Templates  
                                      Email Signatures  
                                      Employee Announcements  
                                      Retirement Receptions  
                                      Flyers   Posters  
                                      Cards   Certificates  
                                      Nametags  
                                      Place Cards   Table Tents  
                                      Business Cards  
                                  
                               
                                Photo and Video Resources  
                                TV and Radio Appearances  
                                Project Request Form  
                                Affirmative Action Statements  
                                Collateral Coding Statements  
                                Approved Vendors  
                                Frequently Asked Questions  
                            
                         
                      
                      
                      
                         
                            
                                Media Resources  
                               News, Publications and Upcoming Events 
                            
                            
                                Brand Standards  
                               Everything you need to know about utilizing the UofM brand 
                            
                            
                                Marketing Toolkit  
                               Download templates, find a vendor and more 
                            
                            
                                Contact Us  
                               Let us help with your marketing needs 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


