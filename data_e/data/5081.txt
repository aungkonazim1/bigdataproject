Welcome to D2L Resource Center | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 Welcome to D2L Resource Center | Resource Center 









 

 
 
 
 
 

 

 

 











 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                      
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Welcome to D2L Resource Center                       
        
        
       
          
     
           Printer-friendly version       
	 
		 Our  Brightspace Community  provides supplemental information for the Resource Center:
	 

	  
			Video tutorials
		 
		 
			Release information
		 
		 
			Help file packages available for download
		 
	  

 
	 

 
	What's new in December 2015?
 

 
	 
		New features
	 

	 
		 

		 
			Brightspace Pulse
		 

		  
				 To help new learners get started with Brightspace Pulse, introductory notifications now appear after logging in for the first time.  
			 
			 
				 To keep learners up to date with course content, Brightspace Pulse now sends notifications when course content is created in D2L Learning Environment. Learners can also preview new and updated content directly from the notification.  
			 
			
		
	  

	 

 
	 
		Changes to existing features
	 

	 
		  
	 
 

 
	What's new in November 2015?
 

 
	 
		New features
	 

	 
		   
	 
 

 
	 
		Changes to existing features
	 

	 
		 
			D2L Assignment Grader for iOS
		 

		  
				 D2L Assignment Grader for iOS now displays assignment start and end dates set in Dropbox. This feature enables instructors to view assignments by end dates and prioritize which assignments to grade. 
			 
		  
 

 
	What's new in October 2015?
 

 
	 
		New features
	 

	 
		  
	 
 

 
	 
		Changes to existing features
	 

	 
		 
	 
 

 
	What's new in September 2015?
 

 
	 
		New features
	 

	 
		  
	 
 

 
	 
		Changes to existing features
	 

	 
		
	 
 

 
	What's new in August 2015?
 

 
	 
		New features
	 

	 
		 
	 
 

 
	 
		Changes to existing features
	 

	 
		 
	 
 

 
	What's new in July 2015?
 

 
	 
		New features
	 

	 
		 
			Content
		 

		  
				 Learners can now view a list of their overdue topics in the new Overdue tab in the Course Schedule area 
			 
		 
		
	 
 

 
	 
		Changes to existing features
	 

	 
		
	 
 

 
	What's new in June 2015?
 

 
	 
		New features
	 

	 
		 
			Course Catalog
		 

		 
		 
		

		 
			Google Apps integration
		 

		 
			 
				 Users can access and search their Google documents from the Add a File area in tools in Learning Environment 
			 
		 
		

		 
			ReadSpeaker docReader
		 

		  
				 For organizations that use ReadSpeaker integration, a new media player is available in Content that provides audio playback of supported document types  
			 
		  
			Respondus LockDown Browser
		 

		  
				 Quizzes now open automatically in the Respondus LockDown Browser for users who have it installed already (if using Respondus LockDown Browser is a requirement of the quiz). If not already installed, the workflow to install and return to the quiz is improved. 
			 
			
		 
		
			 
		  
 

 
	 
		Changes to existing features
	 

	 
		 
			HTML Editor
		 

		  
				 Updated inserting tables procedure for 10.4 
			 
		 
		

		 
			LiveRoom
		 

		  
				 All references to LiveRoom have been removed due to the deprecation of the tool 
			 
		 
		
	 
 

 
	What's new in May 2015?
 

 
	 
		New features
	 

	 
		

		 
			Google Apps integration
		 

		  
				 Users can now submit documents from Google Drive to ePortfolio 
			 
			
		 
		
	 
 

 
	 
		Changes to existing features
	 

	 
		 
			Wiggio
		 

		  
				 Added Wiggio browser support information 
			 
		  
			Google Apps integration
		 

		  
				 Widget updates and improvements 
			 
		  
 

 
	What's new in April 2015?
 

 
	
	 
 

 
	 
		Changes to existing features
	 

	 
		

		 
			Google Apps integration
		 

		  
				 Updates and improvements 
			 
		  
 

 
	What's new in March 2015?
 

 
	 
		New features
	 

	 
		

		 
			Capture
		 

		  
				 You can now reply to comments in a CaptureCast presentation 
			 
		 
		
	 
 

 
	 
		Changes to existing features
	 

	 
		 
			Course Catalog
		 

		  
				 Updates to user interface and usability  
			 
		  
 

 
	 

 
	 
		 
	 
 
     Audience:    Learner        

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
       User menu 
  
      My account    Log out    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
