Hiding and showing grade items in the grade book | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Hiding and showing grade items in the grade book | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Grades    »    Managing grade items and grade book categories   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Hiding and showing grade items in the grade book                       
        
        
       
          
     
           Printer-friendly version       You can change which grade items appear in your view of the grade book using Manage Columns. 
 Hide or show grade items in the grade book 
  On the Enter Grades page, click  Hide/Show Columns  from the More Actions button. 
 Select the check box for each grade item you want to appear in your grade book. 
 Click  Save.  
  To change which grade item details (for example, weighted grade, grade scheme color) and user details (for example, username, email) appear in your view of the grade book, see  Setting grade book display options . 
 See also 
    Setting grade book display options  
      Audience:    Instructor      

    
           

                   ‹ Editing grade items or categories 
        
                   up 
        
                   Reordering grade items and categories › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Grades  
  
      Finding my grades    Creating a grade book    Creating grade items and grade book categories    Managing grade items and grade book categories    Editing grade items or categories    Hiding and showing grade items in the grade book    Reordering grade items and categories    Setting availability for grade items or categories    Setting release conditions for grade items or categories    Deleting grade items or categories    Restoring deleted grade items or categories    Associating grade items with course objects    Associating grade items or categories with learning objectives      Managing grade schemes    Managing users  grades    Managing final grades    Changing Grades settings and display options    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
