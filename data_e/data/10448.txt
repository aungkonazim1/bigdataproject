Accessing News | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Accessing News | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    News    »    News basics   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Accessing News                       
        
        
       
          
     
           Printer-friendly version       
	Do one of the following:
 

  
		Click    News  on the navbar.
	 
	 
		Click  News  in the header of the News widget.
	 
	
	 
		Click    Edit Course  on the navbar, then click    News .
	 
	
      Audience:    Learner      

    
           

                   ‹ News basics 
        
                   up 
        
                   Dismissing a news item › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        News  
  
      News basics    Accessing News    Dismissing a news item    Restoring a dismissed news item    Subscribing to a News RSS feed    Global News    Enabling notifications in News      Creating and managing news items    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
