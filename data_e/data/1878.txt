Mulroy Speaks on SCOTUS Rulings - School of Law - University of Memphis    










 
 
 
     



 
    
    
    Mulroy Speaks on SCOTUS Rulings - 
      	School of Law 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Cecil C. Humphreys School of Law
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Admissions  
                          Programs  
                          Current Students  
                          Faculty  
                          Careers  
                          Library  
                      
                      
                         
                            About   
                            
                               
                                  
                                   Administration   Staff  
                                   History  
                                   Strategic Plan  
                                   Virtual Tour  
                                   ABA Disclosures  
                                   Diversity  
                                         Diversity Home  
                                         TIP  
                                         Scholarships  
                                     
                                  
                                   Facilities  
                                         Facilities Overview  
                                         Virtual Tour  
                                         Floor Plans  
                                         Photo Gallery  
                                     
                                  
                                   News   Events  
                                         Law School News  
                                         On Legal Grounds Blog  
                                         ML-Memphis Law Magazine  
                                         Events Calendar  
                                         Communications Office  
                                     
                                  
                                   Directions  
                                   Contact  
                                         Media Resource  
                                         General Contact Info  
                                         Faculty   Staff  
                                     
                                  
                                   Life in Memphis  
                                         Memphis-The City  
                                         Neighborhoods  
                                         Housing  
                                         The Arts Scene  
                                         Sports  
                                         The Food Scene  
                                         A Music Town  
                                         Directions and Maps  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	School of Law 
                              	  
                          
                              	About
                              	  
                         Mulroy Speaks on SCOTUS Rulings 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      PROF. MULROY SPEAKS ON VARIETY OF ISSUES 
                     
                        
                     
                      University of Memphis School of Law Professor and Associate Dean of Academic Affairs,
                        Steve Mulroy spoke with several media outlets regarding a number of recent issues
                        in the news, such as the recent Supreme Court of the United States decisions regarding
                        the Affordable Healthcare Act, the Court's ruling on gay marriage equality, the removal
                        of a Confederate statue from a local park and several issues surrounding the recent
                        deaths of citizens in the Mid-South who were in police custody at the time of their
                        passing. 
                      
                     
                      Additionally, Professor Mulroy officiated the wedding of a local, same-sex couple
                        at the the Memphis Gay and Lesbian Community Center on the same day that the SCOTUS
                        ruling was announced. 
                      
                     
                      SCOTUS Decision media coverage: 
                     
                      
                        
                          Memphis Flyer  -  http://www.memphisflyer.com/MemphisGaydar/archives/2015/06/26/couples-tie-the-knot-at-tennessee-equality-project-marriage-celebration   
                        
                          Memphis Daily News  -  http://www.memphisdailynews.com/news/2015/jun/27/gay-marriage-marks-first-day-in-memphis  
                        
                          Fox 13 Memphis  -  http://www.myfoxmemphis.com/clip/11634049/law-professor-steve-mulroy-talks-about-supreme-court-ruling  
                        
                          Albany Times Union  -  http://www.timesunion.com/news/article/Same-sex-couple-marries-in-Nashville-after-ban-6351935.php  
                        
                          ABC 24  -  http://www.localmemphis.com/story/d/story/memphis-lgbt-community-celebrates-marriage-equalit/10678/XptwjdXzE0ORHUFECuWjxA  
                        
                          WREG Ch. 3  -  http://wreg.com/2015/06/26/aca-ruling/  
                        
                      
                     
                      Passenger's rights coverage: 
                     
                      
                        
                          ABC 24  -  http://www.localmemphis.com/news/local-news/knowing-the-law-when-being-pulled-over  
                        
                          Fox 13 Memphis  -  http://www.myfoxmemphis.com/story/29609910/passengers-rights-when-a-car-is-pulled-over-by-police  
                        
                      
                     
                      Removal of Confederate statue coverage: 
                     
                      
                        
                          WMC TV 5 -   http://www.wmcactionnews5.com/story/29514094/citys-fight-to-remove-nathan-bedford-forrest-monument-grave-from-park-wont-be-easy  
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         About 
                         
                            
                               
                                Administration   Staff  
                                History  
                                Strategic Plan  
                                Virtual Tour  
                                ABA Disclosures  
                                Diversity  
                                      Diversity Home  
                                      TIP  
                                      Scholarships  
                                  
                               
                                Facilities  
                                      Facilities Overview  
                                      Virtual Tour  
                                      Floor Plans  
                                      Photo Gallery  
                                  
                               
                                News   Events  
                                      Law School News  
                                      On Legal Grounds Blog  
                                      ML-Memphis Law Magazine  
                                      Events Calendar  
                                      Communications Office  
                                  
                               
                                Directions  
                                Contact  
                                      Media Resource  
                                      General Contact Info  
                                      Faculty   Staff  
                                  
                               
                                Life in Memphis  
                                      Memphis-The City  
                                      Neighborhoods  
                                      Housing  
                                      The Arts Scene  
                                      Sports  
                                      The Food Scene  
                                      A Music Town  
                                      Directions and Maps  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply to Memphis Law  
                                
                            
                            
                                News   Events  
                                
                            
                            
                                Alumni   Support  
                                
                            
                            
                                ABA Required Disclosures  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


