Viewing discussion statistics | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Viewing discussion statistics | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Discussions    »    Monitoring discussions   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Viewing discussion statistics                       
        
        
       
          
     
           Printer-friendly version       
	Statistics give you an overview of user activity in your discussion topics and forums. You can use this information to determine which topics stimulate conversation and which do not, or which students post frequently and which hang back. As the semester unfolds, you can use statistics to pinpoint topics in need of conversation-starters and to follow the participation levels of different users. You can send email reminders to users who have not been participating or ask overly vocal users to slow down and give others a chance to contribute. When the semester ends, statistics help you identify the topics that were most effective and the ones you might want to omit or modify for the next course.
 

 
	 
		 Notes 
	 

	  
			Post counts do not include unapproved and deleted posts.
		 
		 
			Only users who appear in the classlist appear when viewing statistics by user; however, totals include posts by all users, including instructors.
		 
		 
			You can sort statistics by clicking on any column heading.
		 
	  

 
	View statistics for a specific forum or topic
 

 
	Click    View Topic Statistics  or    View Forum Statistics  from the context menu of the forum or topic you want to view.
 

 
	View statistics for the entire course,
 

 
	On the Discussions List page, click  Statistics  from the tool navigation.
 
     Audience:    Instructor      

    
           

                   ‹ Editing other users  discussion posts 
        
                   up 
        
                   Exporting discussion statistics › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Discussions  
  
      Participating in discussions    Following discussions    Creating and managing discussions    Monitoring discussions    Adding post approval to a discussion forum or topic    Approving and unapproving discussion threads and posts    Editing other users  discussion posts    Viewing discussion statistics    Exporting discussion statistics    Viewing a discussion post s history      
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
