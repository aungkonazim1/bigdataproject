CERI Seismic Information - Recent Earthquakes in the Central US   
  
 
 
 
 CERI Seismic Information - Recent Earthquakes in the Central US 
 
 
 
 

 
 
 
 

 

 

 
   

 
   
   
     
     
     
     
     
     
     
     
   
   
        Graduate Studies at CERI     CERI Graduate Admissions     Graduate Student Handbook     Thesis & Dissertation Titles   
   
   
        Faculty     Staff     Students     Strategic Plan    
   
   
        Seismic Data from CERI     Recent Earthquakes the Central U.S.     Recent Helicorder Displays     New Madrid Earthquake Catalog     New Madrid Seismic Network    
   
   
        Earthquake Information Sources     Earthquake Facts and Fiction     How to Survive an Earthquake     Public Earthquake Resource Center    
   
 
   
   

   
		  Seismic Data Home  
          Recent Earthquakes in the Central U.S.  
          Earthquakes in the Mid-South  
          Recent Helicorder Displays          
      New Madrid Earthquake Catalog  
          New Madrid Seismic Network  
          Earthquake Felt and/or Damage Report  
          Earthquake Data Links  
   
   

	 
	 DISCLAIMER 
	  
       
     (Try our new  Google-based recent earthquakes map ! It only updates about every 10 minutes but has greater functionality and looks wicked cool.)
      Did you feel it?  - Please fill out a  felt report  (even if you didn't feel it). 
     For Other earthquakes:  USGS  
      Click on an earthquake on the above map for a zoomed-in view. 
            Earthquake lists:  
       Big earthquakes  
       All earthquakes  
     
     Magnitude = ? for new earthquakes until a magnitude is determined (takes 4-5 minutes). 
      Maps are updated within 1-5 minutes of an earthquake or once an hour. 
      (Smaller earthquakes are added after human processing, which may take several hours.) 
      Map need updating? Try reloading the page to your browser. 
      Brown lines are known  hazardous faults and fault zones . 
     
      Other Information:   
     How do earthquakes get on these maps?   FAQ's   Disclaimer  
      Earthquakes  elsewhere in the U.S. and around the world   
       Other sites for eq info (USGS) 
       Other earthquake sites  (USGS) 
       Credits  
     
      Data Sources:     University of Memphis, CERI   
   St Louis University   
   University of South Carolina at Columbia   
  Virginia Tech  
  University of Kentucky   
  Lamont-Doherty Earth Observatory   
  Oklahoma Geological Survey   
  Maryland Geological Survey   
  IRIS   
  EarthScope   
   U.S. Geological Survey  
	 
      
   
   
     The CERI Seismic Networks are operated under  cooperative agreement with the  U.S. Geological Survey . The USGS is a  NEHRP Agency . 
      
 Please email suggestions and comments to  Mitch Withers . 
    
  
 
  
