Viewing quiz reports | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Viewing quiz reports | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Quizzes    »    Viewing quizzes   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Viewing quiz reports                       
        
        
       
        
     
              
	You can view and export quiz statistics and reports to a CSV file for data analysis. The CSV file contains all pertinent information from the quiz so that you can query, sort, and evaluate the data without being connected to the learning management system. You must set up quiz reports from the Reports Setup tab for the appropriate quiz before you can view or export reports. See  Creating quizzes  to learn more about creating reports.
 

 
	View a quiz report
 

  
		On the Quiz List or Manage Quizzes page, click    Reports  from the context menu of the quiz you want to view.
	 
	 
		Click on the report you want to view.
	 
	 
		Select the  From  and  To  check boxes and set the calendar dates to restrict your report to a specific time frame.
	 
	 
		Click  Generate CSV Report  to save the report as a CSV file on your computer, or click  Generate HTML Report  to view the report in your browser.
	 
  
	Data exported with quiz reports
 

    
				Report Type
			 
			 
				What it includes
			 
		   
				 
					Question Statistics
				 
			 
			 
				 
					The average score on questions by points and percentage.
				 

				 
					Unlike a report created using the  Questions Stats  tab on the Quiz Stats page, you can choose to exclude the class average, score distribution, bonus questions, or the Out Of value.
				 

				 
					You can also set a release date for this report.
				 
			 
		   
				 
					Question Details
				 
			 
			 
				 
					All of the answers provided for each quiz attempt, how many users chose each possible answer, the total number of responses, and the average score on each question.
				 

				 
					Unlike a report created using the  Question Details  tab on the Quiz Stats page, you can choose to exclude the level of difficulty, text responses, or bonus questions. You can also choose to include private comments added to the report.
				 

				 
					You can also set a release date for this report.
				 
			 
		   
				 
					Users Statistics
				 
			 
			 
				 
					The class average, score distribution, and the grade of each user.
				 

				 
					Unlike a report created using the  User Stats  tab on the Quiz Stats page, you can choose to exclude the class average, score distribution, or Org Defined ID.
				 

				 
					You can also set a release date for this report.
				 
			 
		   
				 
					Attempt Details
				 
			 
			 
				 
					The Org Defined ID, username, first name, and last name of each user. Detailed information about each question. The users’ responses for each question organized by attempt, including how long each attempt took.
				 

				 
					The ability to set a start or end date for which attempts to include.
				 

				 
					You can also set a release date for this report.
				 
			 
		   
				 
					User Attempts
				 
			 
			 
				 
					The Org Defined ID, username, first name, and last name of each user. The users’ score for each question in points and percentage organized by attempt, including how long each attempt took.
				 

				 
					The ability to set a start or end date for which attempts to include.
				 

				 
					You can also set a release date for this report.
				 
			 
		    
	See also
 

  
		 Creating quizzes 
	 
      Audience:     Instructor       

    
           

                   ‹ Viewing quiz statistics 
        
                   up 
        
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Quizzes  
  
      Participating in Quizzes    Using Quizzes    Managing quiz questions and sections    Viewing quizzes    Submitting a user s quiz (impersonating a user)    Viewing quiz statistics    Viewing quiz reports      
                  
           
         

       
     

    
    
    
   
 
   
 
