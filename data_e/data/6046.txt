ACT-Online - Center for Information Assurance - University of Memphis    










 
 
 
     



 
    
    
    ACT-Online - 
      	Center for Information Assurance
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Center for Information Assurance
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Scholarships  
                          Projects  
                          CyberSummit  
                          Courses  
                          People  
                      
                      
                         
                            Projects   
                            
                               
                                  
                                   Current Projects  
                                         GenCyber Camp  
                                          
                                           
                                             
                                               GenCyber Camp 2017  
                                             
                                               GenCyber Camp 2016  
                                             
                                           
                                          
                                        
                                         Adaptive Multi-Factor Authentication  
                                         Puzzle Based Cyber Security...  
                                     
                                  
                                   Past Projects  
                                        
                                         Negative Authentication System  
                                        
                                         ACT Online  
                                        
                                         Intelligent Security Console  
                                        
                                         Cougar Based Intrusion Detection Systems  
                                        
                                         Immunity-Based Intrusion Detection Systems  
                                        
                                         Security Agents for Network Traffic Analysis  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Center for Information Assurance
                              	  
                          
                              	Projects
                              	  
                         ACT-Online 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      ACT Online 
                     
                        
                     
                      ACT Online content was developed by the University of Memphis Center for Information
                        Assurance which is recognized by the US Department of Homeland Security and the National
                        Security Agency as a National Center of Academic Excellence in Information Assurance
                        Education.
                      
                     
                      The ACT Online courseware is intelligent and adaptive, providing a rich set of tools
                        and features to aid and enhance your learning experience.  These tools include:
                      
                     
                      
                        
                         Self assessment exams that provide immediate and robust feedback 
                        
                         A powerful and versatile search feature 
                        
                         A 'notes' feature which allows you to take notes during the training for later use 
                        
                         An activities map that guides you through the phases of training 
                        
                         Detailed and dynamic post exam feedback to aid you in subsequent exam attempts 
                        
                      
                     
                      The ACT Online courseware allows you to train and take examinations at your own convenience
                        and pace.  You can exit the courseware at any time and resume where you left off,
                        even during an examination.
                      
                     
                      Courses are free and available by following this  link .
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Projects 
                         
                            
                               
                                Current Projects  
                                      GenCyber Camp  
                                       
                                        
                                          
                                            GenCyber Camp 2017  
                                          
                                            GenCyber Camp 2016  
                                          
                                        
                                       
                                     
                                      Adaptive Multi-Factor Authentication  
                                      Puzzle Based Cyber Security...  
                                  
                               
                                Past Projects  
                                     
                                      Negative Authentication System  
                                     
                                      ACT Online  
                                     
                                      Intelligent Security Console  
                                     
                                      Cougar Based Intrusion Detection Systems  
                                     
                                      Immunity-Based Intrusion Detection Systems  
                                     
                                      Security Agents for Network Traffic Analysis  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                News   Events  
                                
                            
                            
                                Student Success Stories  
                                
                            
                            
                                Community Outreach  
                                
                            
                            
                                Contact Us  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/21/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


