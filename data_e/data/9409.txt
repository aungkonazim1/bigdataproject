Earthworm Modules: dumpwave overview   
 
 
    
    
    Earthworm Modules: dumpwave overview 
 
 

 
 
Earthworm Modules: 
    Dumpwave Overview 
 

 
   (last revised 30 May 2006) 
 

 Dumpwave dumps a header summary and the contents of a  TYPE_TRACEBUF2 
  or  TYPE_TRACEBUF  waveform data file to the screen.  
 Dumpwave reads a binary file of  raw  tracebuf messages, written
  one after another with no other header info. (The file format that Dumpwave
  reads is the same format the tankplayer reads, and can be produced by trig2disk
  or waveman2disk with  DataFormat tank . ) 
 Command line arguments:  
   argument 1 = file to dump from 
  optional argument 2 = pin# to dump  
 For example: 
  dumpwave \earthworm\tanks\20060530_185505.00_MAN  
 Output will look something like the following for each tracebuf that's in the
  wave file: 
   version: 20
  s.c.n.l: NP1.HHN.ZZ.01
    pinno:     3
    nsamp:   500
 samprate: 100.0000
starttime: 1149015311.0000   2006/05/30:1855:11.00
  endtime: 1149015315.9900   2006/05/30:1855:15.99
 datatype: i4
 quality0: b   quality1: e
     pad0:         pad1: u
    7011    7122    7101    6894    7001    7063    7014    7077    7198    7114

    6943    7002    7058    6929    6937    7095    7100    6990    6978    7042

    6968    6967    7056    6956    6948    7032    6909    6858    6876    6897
...
 
 
  Module Index  
 
 
   

  
Questions? Issues? S ubscribe
to the Earthworm Google Groups List.   
 
 
