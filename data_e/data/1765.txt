blogs.memphis.edu | The University of Memphis Enterprise Blog Service   
 
 
 
      
 
 
 
 blogs.memphis.edu | The University of Memphis Enterprise Blog Service 
 
 
 

 
 
 
 
		
		
 
 
 
 
 
 
 



 
 
  
 

			

         
 



 

 
 
		 
		   Skip to Main Content 
	  
	 
		 
						 
				 
			 
					  blogs.memphis.edu  
					 The University of Memphis Enterprise Blog Service 
				  
	  

	 
		 
			 

			
					 
		 Post navigation 

	
	  
	
								
					
 
	 
			
				  ITS Launches New Wireless Access Pilot  
						 
			 By  hbusby@memphis.edu . 
		  
			  

		 
		 Information Technology Services (ITS) is launching a new wireless access network pilot with an additional layer of security. Effective Aug. 14, uofm-secure will provide wireless users enhanced security for passwords and protection of University data. ITS welcomes your participation and cooperation in helping to keep both you and the University more secure with improved encryption. Existing UofM and uofm-guest wireless networks remain in place while the new uofm-secure network is tested. The need for two older networks will be addressed during the fall semester.  To learn more visit our uofm-secure wireless page . 
			  
	
	 
		 Posted:   08.09.2017  . 			  
  
				
					
 
	 
			
				  Get Started on the Right Foot with Blogs  
						 
			 By  hbusby@memphis.edu . 
		  
			  

		 
		 If you re just getting started using UoM blogs you ll want to head to our  Support  page. There you can find Edublogs Getting Started site and other useful aides. 
 Blogs are a great tool, but having knowledge on how to use them makes this tool even more powerful.  Enjoy and happy blogging! 
			  
	
	 
		 Posted:   04.21.2017  . 			  
  
				
					
 
	 
			
				  Welcome to Blogs.Memphis.Edu  
						 
			 By  hbusby@memphis.edu . 
		  
			  

		 
		  Blogs.memphis.edu  is the University s installation of WordPress, a blogging service that allows Memphis faculty, staff, and students to collaborate around a common subject, such as an academic course, program, department, or community service. These blogs provide a place for users to share experiences, reflect on activities, and publish student creative works through written posts, pictures, videos, links, and comments.  As with other technology resources available at the University of Memphis, the Blogs.memphis.edu service is subject to the Acceptable Use of Information Technology Resources policy. (  See the link to the policy  ) 
   Please note :   By creating a blog, you agree that you are solely responsible for the content you create. Neither the content of your blog, nor the links to other web sites, are approved or endorsed by the University of Memphis. The text and other material on your blogs are the opinion of the specific author and are not official statements of advice, opinion or information of the University of Memphis.  
  To get started using your blog, click on the Log In button at the top of this page and Log In using your Memphis UUID. After you Log In you will be at your Dashboard page.  Select your blog (tiger_tom@memphis.edu, for example) and start editing your blog!  
   Also, here is a link that lists,   The 5 Reasons Students Should Blog    
  
			  
	
	 
		 Posted:   04.21.2017  . 			  
  
				
					 
		 Post navigation 

	
	  
	
			
			  
		  


				 
	        	         
	                  News/Announcements 			 Watch this space for any news you may be interested in about blogging in general, or specifically here at UofM. Happy blogging! 
		 	          
	        	
	        	  	  

	 
				
				
		 
			 Made with  WordPress     Accessible Zen . 		  
	  
	 
		   Back to the Top 
	  
  


        

        





		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 