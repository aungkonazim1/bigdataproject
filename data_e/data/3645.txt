 Q. I would like to research my family history. Can the University Libraries help me with this? - LibAnswers   
 
 
 
	 
	 
	   
	 
		 
	 
	
     
        Q. I would like to research my family history. Can the University Libraries help me with this? - LibAnswers     

	 	
    
    
    	
		
 
 
	 Skip to Main Content 
	 
		 
	 
		 
				 
				 
					  University Libraries Home Page  
					  LibAnswers   
				 
			  
				  
					 Q. I would like to research my family history. Can the University Libraries help me with this? 
					  
				  
			  
				 
					 
			    		 
							   Toggle menu visibility 
						 
						
			    	 
			    	
			    	 		
						  Ask Another Question  
			 
			
			
			   
				 
			 
				
				 Search 
			 
			 
		  
						 
							  Browse:  
							  All  
							
							 
				 Topics    
				    36  Access to Resources      2  Alumni      7  Articles      13  Campus Resources      12  Catalog      11  Circulation      6  Citations      3  Collections      8  Computers      5  Copying      28  Databases      2  Dissertations & Theses      2  Ebooks      2  Educational Support Program      2  Faculty      2  Genealogy      18  Government Resources      1  Group Study Rooms      7  Guests      6  Hours and Locations      5  Instructional Services      6  Interlibrary loan      3  IT Helpdesk      6  Journals      3  Laptops      13  Library Services & Policies      2  Microform      5  Newspapers      1  Nursing      1  Obituaries      1  Off Campus      1  Periodicals      1  Preservations & Special Collections      9  Printing      1  Reciprocal Borrowing      2  RefWorks      7  Research Guides      22  Research Help      5  Reserve Room      3  RODP      1  Scanners      6  Statistics      4  Tests & Testing      2  Textbooks      2  Tutoring      7  UUID & Password      13  Video Tutorials      21  Websites      4  Writing    
			 
						 
	
					 
				 
			   
			  
		    
			 
				
				 
		    		 
						  Toggle action bar  
					 
					 FAQ Actions 
		    	 
		    	
				  
					  
							 
								     Print 
							 
						 
						 
							 
								    Tweet 
							 
						 
						 
							 
							    Share on Facebook 
							 
						 
						
					 				
					
					  Was this helpful? 
	    	 
	    		  
	    		  
	    		 Yes 
	    	 
	    	 2  
	    	  
	    		  
	    		  
	    		 No 
	    	  
	    	 0  

				 

			 
		  
				 
					  Topics   
					   
  Access to Resources    Alumni    Articles    Campus Resources    Catalog    Circulation    Citations    Collections    Computers    Copying    Databases    Dissertations & Theses    Ebooks    Educational Support Program    Faculty    Genealogy    Government Resources    Group Study Rooms    Guests    Hours and Locations     Instructional Services    Interlibrary loan    IT Helpdesk    Journals    Laptops    Library Services & Policies    Microform    Newspapers    Nursing    Obituaries    Off Campus    Periodicals    Preservations & Special Collections    Printing    Reciprocal Borrowing    RefWorks    Research Guides    Research Help    Reserve Room    RODP    Scanners    Statistics    Tests & Testing    Textbooks    Tutoring    UUID & Password    Video Tutorials    Websites    Writing     View All Topics     
					
				 
			    
				 
					      Answered By:  Bess Robinson        Last Updated:  Nov 22, 2017          Views:  158      
					   In general, the University Libraries often do not assist with family history research. You may want to reach out to our  Preservation   Special Collections Department , the  Memphis Public Library , and/or the local historical societies to see if information may be available through those avenues.  

    
  Links & Files        Location & Hours for Preservation & Special Collections         Memphis Public Library system         Memphis Public Library: Genealogy         West Tennessee Historical Society         Jewish Historical Society of Memphis & the Mid-South         Tennessee Genealogical Society     

 
					
				 
			    
				 
					  Chat   
					      
					
				 
			  
				 
					  Other Ways to Contact Us   
					  
			                                   
                                


		  
					
				 
			    
			  
				 
					  Comments (0)   
					   
			 
				 Add a public comment to this FAQ Entry 
			  
					
				 
			  
				 
					     
					  
			      University Libraries  -  The University of Memphis  - Memphis, TN 38152 - 901-678-2205 


		  
					
				 
			  
		   
    		  Powered by   Springshare ; All rights reserved.   Report a tech support issue.     Login to LibApps    	 
	 
		 
          
 
 