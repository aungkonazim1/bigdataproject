Creating calculated grade items | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Creating calculated grade items | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Grades    »    Creating grade items and grade book categories   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Creating calculated grade items                       
        
        
       
          
     
           Printer-friendly version       
	Display a user's cumulative achievement across multiple grade items with a calculated grade item.
 

 
	 Notes 

	  
			Calculated grade items cannot belong to a category.
		 
		 
			Calculated grade items cannot contribute to the calculated final grade.
		 
	  

 
	Create a calculated grade item
 

  
		On the Manage Grades page, click  Item  from the New button.
	 
	 
		Select  Calculated .
	 
	 
		Enter a  Name  for the grade item.
	 
	 
		You can enter a  Short Name  to display in the grade book.
	 
	 
		Enter a  Description  of the grade item. If you want to make the description available to users, select  Allow users to view grade item description .
	 
	 
		If you want users’ grades to be able to exceed the total value of the item, select  Can Exceed .
	 
	 
		Select a  Grade Scheme  to associate with the item.
	 
	 
		In the  Calculation  section, select the check boxes beside the items you want to include in the calculation. Clicking a category selects all grade items in the category. Clicking the check box at the top or bottom of the list selects all items.
	 
	 
		Select a  Milestone Grade Calculation  or  Final Grade Calculation  method (these options are not available in a Points grading system).
	 
	 
		You can click  Add Rubric  to add a rubric, or click the  Create Rubric in New Window  link to create a new rubric.
	 
	 
		Select  Display Options .
	 
	 
		Click  Save and Close .
	 
      Audience:    Instructor      

    
           

                   ‹ Creating formula grade items 
        
                   up 
        
                   Creating text grade items › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Grades  
  
      Finding my grades    Creating a grade book    Creating grade items and grade book categories    Understanding grade items    Creating grade book categories    Creating numeric grade items    Creating selectbox grade items    Creating pass/fail grade items    Creating formula grade items    Creating calculated grade items    Creating text grade items      Managing grade items and grade book categories    Managing grade schemes    Managing users  grades    Managing final grades    Changing Grades settings and display options    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
