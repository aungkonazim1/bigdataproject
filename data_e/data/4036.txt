DAVID SPICELAND - ACCT - University of Memphis    










 
 
 
     



 
    
    
    DAVID SPICELAND - 
      	ACCT
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - School of Accountancy
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Welcome  
                          Programs  
                          Admissions  
                          Faculty  
                          Courses   Syllabi  
                          Publications  
                          FCBE  
                      
                      
                         
                            Faculty   
                            
                               
                                  
                                   Welcome  
                                   Programs  
                                         Programs Offered  
                                         Bachelor of Business Administration  
                                         Master of Science in Accounting  
                                         PhD in Accounting  
                                     
                                  
                                   Admissions  
                                         Getting Started  
                                         Financial Assistance  
                                         Application Form  
                                         Networking   Career Development  
                                     
                                  
                                   Faculty  
                                   Courses   Syllabi  
                                         Courses Offered  
                                         Undergraduate  
                                         Graduate  
                                         Course Syllabi  
                                     
                                  
                                   Publications  
                                   Contact Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	ACCT
                              	  
                          
                              	Faculty
                              	  
                         DAVID SPICELAND 
                      
                   
                   
                       
                       
                     
                       
                        
                          
                           
                             
                              
                              
                               
                               
                              
                             
                           
                             
                              
                               
                                 
                                 DAVID SPICELAND
                                 
                                
                              
                               
                                 
                                 Professor Emeritus,  School of Accountancy
                                 
                                
                              
                                
                                 
                                   
                                    
                                     Phone  
                                    
                                     
                                       
                                       
                                       
                                      
                                    
                                   
                                 
                                   
                                    
                                     Email  
                                    
                                     
                                       
                                       dspice@memphis.edu
                                       
                                      
                                    
                                   
                                 
                                   
                                    
                                     Fax  
                                    
                                     
                                       
                                       
                                       
                                      
                                    
                                   
                                 
                                
                              
                                
                                 
                                   
                                    
                                     Office  
                                    
                                     
                                       
                                       
                                       
                                      
                                    
                                   
                                 
                                   
                                    
                                     Office Hours  
                                    
                                     
                                       
                                       
                                       
                                      
                                    
                                   
                                 
                                
                              
                                
                                 
                                 
                                 
                                   Website  
                                  
                                 
                                
                              
                             
                           
                          
                        
                        
                        
                         Biography 
                        
                         David Spiceland is Professor Emeritus in the School of Accountancy where he taught
                           financial accounting at the undergraduate, masters, and doctoral levels for 36 years.
                           Interestingly, in 1985, his dad Jesse Spiceland, also was named Professor Emeritus
                           after 36 years teaching in the same accounting program. Professor Spiceland received
                           his B.S. degree in finance from the University of Tennessee, his MBA from Southern
                           Illinois University, and his Ph.D. in accounting from the University of Arkansas.
                           Dr. Spiceland also is a CPA.
                         
                        
                         Professor Spiceland is a textbook author whose several books include McGraw-Hill's
                           best-selling   Intermediate Accounting  , now in its 9th edition, and   Financial Accounting  , in its 4th edition. Dr. Spiceland has published articles in a variety of academic
                           and professional journals including  The Accounting Review ,  Accounting and Business Research ,  Journal of Financial Research ,  Advances in Quantitative Analysis of Finance and Accounting , and most accounting education journals:  Issues in Accounting Education ,  Journal of Accounting Education ,  Advances in Accounting Education ,  The Accounting Educators' Journal ,  Accounting Education ,  The Journal of Asynchronous Learning Networks , and  Journal of Business Education . David has received university and college awards and recognition for his teaching,
                           research, service, and technological innovations in the classroom.
                         
                        
                          Recent Publications:   
                        
                         
                           
                            
                              
                               Spiceland, Nelson, and Thomas,   Intermediate Accounting  , Ninth Edition, McGraw-Hill, Burr Ridge, Illinois, 2018, 2015, 2013, 2011, 2009,
                                 2007, 2004, 2001,1998.
                               
                              
                            
                           
                            
                              
                               Callahan, C.M., Spiceland, C.P., Spiceland, J.D. and Hairston, S., "Pilot Course:
                                 A Teaching Practicum Course as An Integral Component of An Accounting Doctoral Program,"
                                  Issues in Accounting Education , Volume 31, Issue 2, 2016
                               
                              
                            
                           
                            
                              
                               Spiceland, Thomas, and Hermann,   Financial Accounting  , Fourth Edition, McGraw-Hill, Burr Ridge, Illinois, 2016, 2014, 2011, 2009.
                               
                              
                            
                           
                            
                              
                               Spiceland, C.P., J.D. Spiceland, and P.J. Njoroge, "Linking Healthcare Quality to
                                 the Bottom Line: What Do We Know?"  International Journal of Management Accounting Research , Volume 4, Issue 2, 2015 [lead article].
                               
                              
                            
                           
                            
                              
                               Spiceland, C.P., J.D. Spiceland, and S.J. Schaeffer, "Using a Course Redesign to Address
                                 Retention and Performance Issues in Introductory Accounting,"  Journal of Accounting Education , Volume 33, Issue 1, 2015.
                               
                              
                            
                           
                            
                              
                               Spiceland, Thomas, and Hermann,   Financial Accounting: Making the Connection  , First Edition, McGraw-Hill, Burr Ridge, Illinois, 2013.
                               
                              
                            
                           
                         
                         
                        
                       
                     
                      
                   
                
                
                   
                      
                         Faculty 
                         
                            
                               
                                Welcome  
                                Programs  
                                      Programs Offered  
                                      Bachelor of Business Administration  
                                      Master of Science in Accounting  
                                      PhD in Accounting  
                                  
                               
                                Admissions  
                                      Getting Started  
                                      Financial Assistance  
                                      Application Form  
                                      Networking   Career Development  
                                  
                               
                                Faculty  
                                Courses   Syllabi  
                                      Courses Offered  
                                      Undergraduate  
                                      Graduate  
                                      Course Syllabi  
                                  
                               
                                Publications  
                                Contact Us  
                            
                         
                      
                      
                      
                         
                            
                                Networking   Career Development  
                               Learn more about two important keys to career success! 
                            
                            
                                Financial Assistance  
                               Looking for financial assistance?  Try looking here first! 
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


