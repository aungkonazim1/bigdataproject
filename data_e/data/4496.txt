Financial Aid - Department of English - University of Memphis    










 
 
 
     



 
    
    
    Financial Aid  - 
      	Department of English
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of English
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Undergraduate  
                          Graduate  
                          People  
                          Community  
                          News/Events  
                      
                      
                         
                            Graduate Students   
                            
                               
                                  
                                   Application Process  
                                   MA  
                                         Composition Studies  
                                         ESL  
                                         Linguistics  
                                         Literature  
                                         Professional Writing  
                                     
                                  
                                   MFA  
                                         Creative Writing  
                                     
                                  
                                   PhD  
                                         Applied Linguistics  
                                         Composition Studies  
                                         Professional Writing  
                                         Literary and Cultural Studies  
                                     
                                  
                                   Certificate  
                                         African American Literature  
                                         Teaching English as a Second/Foreign Language  
                                     
                                  
                                   Online Degree Programs  
                                   Advising  
                                   Financial Aid and Scholarships  
                                   Graduate Courses  
                                         Two-Year Template 2016-2018  
                                         Two-Year Template 2018-2020  
                                         Graduate Course Descriptions  
                                     
                                  
                                   Organizations  
                                         UM English Graduate Organization  
                                          The Pinch   
                                         Society for Technical Communication  
                                     
                                  
                                   Forms  
                                         UofM Graduate Online Forms  
                                         English Teaching/Graduate Assistantship Application  
                                         Graduate Advising Worksheet  
                                         Graduate Independent Study Form  
                                         Thesis Hour Registration and Prospectus Form  
                                         Dissertation Hour Registration Form  
                                     
                                  
                                   Graduate Catalog  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of English
                              	  
                          
                              	Graduate Students
                              	  
                         Financial Aid  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      English Assistantships and Financial Aid 
                     
                      Graduate students in the Department of English are encouraged to pursue the following
                        means of obtaining financial support:
                      
                     
                      
                        
                          Graduate Assistantships through our Department  - See information below regarding policies and the application process.
                         
                        
                         Assistantships, awards and fellowships through the Graduate School - Students are
                           strongly encouraged to apply for any award or fellowship for which they are eligible,
                           both as an incoming student and as a returning student.
                         
                        
                         Financial Aid 
                        
                         Department of English Awards - Each spring, students are encouraged to apply for a
                           number of scholarships and financial stipends presented at our annual Awards Day ceremony.
                           [Information forthcoming.]
                         
                        
                      
                     
                      The Department of English offers a limited number of Graduate Assistantships in either
                        departmental service, research, or teaching. These assistantships carry stipends in
                        addition to the remission of University tuition and fees. Students must have completed
                        18 hours of graduate work in English in order to qualify as Teaching Assistants (TAs).
                        TAs normally teach two sections of composition. A very limited number of Graduate
                        Assistantships (GAs) are available for those students who have not completed this
                        requirement. GAs work an average of 20 hours per week in research and teaching roles
                        for department faculty and staff.
                      
                     
                      All students must apply by  March 1  to be considered for an assistantship. Please note that this application is separate
                        from the admission application, although you may submit both to the English Graduate
                        Office at the same time if you wish.
                      
                     
                       Assistantship Application  (.pdf)
                      
                     
                      Key Policies: 
                     
                      
                        
                         Assistantships are typically awarded for one academic year and are eligible for renewal
                           upon successful completion of duties. Support time limits vary with degree program,
                           transfer credits and the availability of departmental funds.
                         
                        
                         Graduate Assistants are expected to carry a nine-hour load every semester, with the
                           exception of students writing a thesis/dissertation, who may register for six hours
                           of thesis/dissertation to be considered full-time. Students are encouraged to consult
                           with their advisors in making course selections.
                         
                        
                         In order to qualify to teach in the Department of English students must take ENGL
                           7003 Theory and Practice in Teaching Composition concurrent with or prior to their
                           first teaching assignment.
                         
                        
                         All non-native speakers of English must take the SPEAK test and earn a score of 50
                           in order to be appointed as a Teaching Assistant or Graduate Assistant with teaching
                           duties, including serving as a discussion leader or any teaching activity requiring
                           oral communication with students. See the Graduate School's SPEAK site for more information.
                         
                        
                         New Teaching Assistants are required to attend all orientation sessions offered before
                           the beginning of each semester, generally one week before classes begin.
                         
                        
                         All students who apply for Assistantships are encouraged to submit the Free Application
                           for Federal Student Aid (FAFSA) to be considered for Work Study funding.
                         
                        
                      
                     
                      Contact the Department of English Graduate Office at (901) 678-1448 or  smellis@memphis.edu  if you have any questions about the application process.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Graduate Students 
                         
                            
                               
                                Application Process  
                                MA  
                                      Composition Studies  
                                      ESL  
                                      Linguistics  
                                      Literature  
                                      Professional Writing  
                                  
                               
                                MFA  
                                      Creative Writing  
                                  
                               
                                PhD  
                                      Applied Linguistics  
                                      Composition Studies  
                                      Professional Writing  
                                      Literary and Cultural Studies  
                                  
                               
                                Certificate  
                                      African American Literature  
                                      Teaching English as a Second/Foreign Language  
                                  
                               
                                Online Degree Programs  
                                Advising  
                                Financial Aid and Scholarships  
                                Graduate Courses  
                                      Two-Year Template 2016-2018  
                                      Two-Year Template 2018-2020  
                                      Graduate Course Descriptions  
                                  
                               
                                Organizations  
                                      UM English Graduate Organization  
                                       The Pinch   
                                      Society for Technical Communication  
                                  
                               
                                Forms  
                                      UofM Graduate Online Forms  
                                      English Teaching/Graduate Assistantship Application  
                                      Graduate Advising Worksheet  
                                      Graduate Independent Study Form  
                                      Thesis Hour Registration and Prospectus Form  
                                      Dissertation Hour Registration Form  
                                  
                               
                                Graduate Catalog  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now  
                                
                            
                            
                                Alumni and Friends  
                                
                            
                            
                                Course Offerings  
                                
                            
                            
                                Contact Us  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/30/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


