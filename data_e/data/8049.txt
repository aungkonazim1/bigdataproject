The Profile of Dipankar Dasgupta   
 
 The Profile of Dipankar Dasgupta 
 
 




 
 



 


 

 
 Dr. Dipankar Dasgupta 
 

 
 * Research Sites * 
 
 
  Artificial Immune Systems (AIS)  
  Center for Information Assurance (CfIA)  
 
 
            


 
 
 
Dr. Dipankar Dasgupta 
333 Dunn Hall 
Memphis, TN 38152-3240 
phone: (901) 678-4147 
fax: (901) 678-1506 
 dasgupta@memphis.edu 
 
 
 
  Dr. Pat E. Burlison  Professor of   Computer Science  
 Director,  Center for Information Assurance  
 Director,  Intelligent Security Systems Research Laboratory  
 


       
  Elevated to IEEE Fellow(Batch of 2015) 
   Distinguished ACM Speaker  
  Recipient of 2012 Willard R. Sparks Eminent Faculty Award.  
 
 
  Advisory Board Member of  MIT in Cyber Security   
  Editorial Board of journals  


 

 Evolutionary Intelligence, Springer-Verlag 

 Evolutionary Optimization, Polish Academy of Science.  

 Recent Patents on Computer Science, online journal Bentham Science Publishers Ltd.  

 Swarm and Evolutionary Computing - Elsevier Press 
 
 
  Announcement:  
 
  Research Assistant Professor Position  (in Cyber Security) Available
  
 
 

 
 * Principal Investigator * 
 
 
  Act Online  
 
 
      


 

 
 
  Home  
  Events 
	 
	  News  
	  Professional Activities  
	  Invited Talks  
	 
 
  Research 
	 
	  Interests 
		 
		  Artificial Immune Systems  
		  Evolutionary Computation  
		  Immunological Computation  
		  Intrusion Detection  
		  Fault Detection  
		 
	 
	  Projects 
		 		
		  Current Projects  
                  Previous Projects  
		 
	 
	
	  Grants  
	  Publications  
 
	 
 
  Teaching 
	 
	  Courses  
	 
 

  Staff/Students  
  Books 
         
          AUA Book  
          IC Book  
          AIS Book  
          EC Book  
         
 
  Recognitions 
         
          Awards  
          Videos  
	 
 
  About  
  
 

 
 
Dr. Dasgupta will Organize IEEE Symposium on  Computational Intelligence in Cyber Security (CICS 2017)  at Hawaii, USA from November 27-December 1, 2017.
Program Committee Member of the 1st IEEE International Workshop on  Cyber Resiliency Economics (CRE 2016)  , Vienna, Austria, August 1-3, 2016.
Prof. Dasgupta will give an invited talk at the  Computer Science Department, University of Tennessee, Knoxville, TN, April 7, 2016
   
Prof. Dasgupta will present a research paper at 11th Annual Cyber and Information Security Research  (CISR)  Conference will be held at the conference center at Oak Ridge National Laboratory, Oak Ridge, TN, April 4 - 6, 2016.
   
Prof. Dasgupta will give invited talk at  Regional Symposium  "Graduate Education and Research in Information Security",'GERIS'16, on  March 8, 2016, at Binghamton University,Binghamton, New York.
   
Announcement for the available position in  Research Assitant Professor  (in Cyber Security)
   
Prof. Dasgupta was interviewed by a local TV Channel (FOX 13) and telecast on Feb. 19, 2016.  Click here for Video. 
   
Organized  "Cybersecurity Certificate Course"  foundational program at FedEx Institute of Technology,UofM, February 1-5, 2016.
   
Prof. Dasgupta gave an invited talk on  5th International Conference on Fuzzy and Neural Computing , FANCCO-2015, December 16-19, 2015.
   
Cluster to Advance Cyber Security & Testing (CAST)  hosted  Cybersecurity Lightning Talks  at the FedEx Institute of Technology, afternoon of December 3, 2015
   
CfIA Receives Cyber Security Training Grant from FEMA
   
UofM's CfIA Will Develop Course for  Mobile Device Security and Privacy Issues 
   
Prof. Dasgupta gave an invited talk on  Adaptive Multi-Factor Authentication  at the  Department of Electrical Engineering and Computer Science and CASE Center, Syracuse University, Syracuse, NY 13224-5040 November 18, 2015
   
Organize a Symposium on Computational Intelligence in Cyber Security (CICS) at IEEE Symposium Series on Computational Intelligence ( SSCI, ), December 7-10, 2015 at Cap Town, South Africa
   
Gave keynote speech at St. Louis at  Cyber Security workshop (STL-CyberCon) , University of Missouri-St. Louis, November 20, 2015
   
Prof. Dasgupta attended the  NIST-NICE  conference at San Diego from November 1-4, 2015
   
Prof. Dasgupta gave an invited talk at 9th  International Research Workshop  on Advances and Innovations in Systems Testing at FedEx Institute of Technology, the University of Memphis, October 20, 2015
   
Our   Cyber Security Team  got a   second position   on Cyber Defense Competition  @CANSec 2015 , held on 24th October at University of Arkansas at Little Rock
  
 

 


 GenoSAP-II 

 Team Members 

 

 Deon Garrett M.S. 

 Aishwarya Kaushal 

 Pavan Kalyan Vejandla 

 Ramjee Yerneni 

 Sukshith S Veerappajja 

 Sudip Saha 

 Soujanya Sattireddy Medapati 

 Koyel Chaudhuri 

 Archana Bhattarai 

 

   

 GenoSAP-II is a Navy-funded project to apply genetic algorithms for solving the Sailor Assignment Problem (SAP). The SAP is a complex assignment problem in which each of  n  sailors must be assigned one job drawn from a set of  m  jobs. The goal is to find a set of these assignments such that the overall desirability of the match is maximized while the cost of the match is minimized. Our studies show that the GA is able to produce good solutions with significant savings in cost comared to the Gale-Shapley algorithm. 

GenoSAP has been an ongoing project (since October 2004), for information regarding GenoSAP contact Prof. Dipankar Dasgupta at  dasgupta@memphis.edu .

 Publications 

 Dipankar Dasgupta, German Hernandez, Deon Garrett, Pavan Kalyan Vejandla, Aishwarya Kaushal, Ramjee Yerneni, James Simien.   A Comparison Of Multiobjective Evolutionary Algorithms with Informed Initialization and Kuhn-Munkres Algorithm for the Sailor Assignment Problem  . In the proceedings of The Genetic and Evolutionary Computation Conference (Late Breaking Paper). Atlanta, Georgia: July, 12-16 2008. 

 J. Vannucci, D. Garrett, D. Dasgupta.    Comparative Analysis of the Sailor Assignment Problem.   Genetic and Evolutionary Computation Conference (GECCO). Seattle, Washington: July, 8-12 2006.  

 D. Garrett, J. Vannucci, R. Silva, D. Dasgupta, J. Simien.    Genetic Algorithms for the Sailor Assignment Problem  , In the Proceedings of the  Genetic and Evolutionary Computation Conference (GECCO)  (was nominated for the Best paper award), pp 1921-1928, Washington DC, June 25-29, 2005. 

   

 Screenshots 

 

  Results  

  Parameters  

  SAPManager  

 




 

 
 






 
 
