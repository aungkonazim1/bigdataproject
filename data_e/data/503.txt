Disability Policies - Disability Resources for Students - University of Memphis    










 
 
 
     



 
    
    
    Disability Policies - 
      	Disability Resources for Students
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Disability Resources for Students
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Access  
                          Accommodations  
                          Technology  
                          Policies  
                          Career Programs  
                          Faculty  
                      
                      
                         
                            Disability Policies   
                            
                               
                                  
                                   Overview  
                                   Student Responsibilities  
                                   Student Disability Grievances  
                                   University Disability Access Policy  
                                   Attendance Policies  
                                   Reduced Course Load  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Disability Resources for Students
                              	  
                         
                           	Disability Policies
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Disability Policies 
                     
                      Under Section 504 of the Rehabilitation Act and the Americans with Disabilities Act,
                        qualified students with disabilities are entitled to equal access and opportunity
                        to participate in all University programs, services and activities. A qualified student
                        with a disability is one who has a physical or mental impairment that substantially
                        limits one or more of the major life activities and who, with or without reasonable
                        accommodation, meets the essential eligibility requirements for the programs, services
                        and activities offered by the University.
                      
                     
                      Following is a summary of the primary provisions relative to qualified students with
                        disabilities under the aforementioned federal regulations:
                      
                     
                      
                        
                         Discrimination is prohibited against qualified persons with disabilities in the areas
                           of recruitment, admission and treatment after admission.
                         
                        
                         All programs, services and activities must be available to students with disabilities
                           in the most integrated setting possible. This requirement includes academic programs,
                           field trips, practicums, internships, research, campus employment, graduate assistantships
                           and all student services and student life activities.
                         
                        
                         No student may be excluded from any course or any course of study solely on the basis
                           of disability.
                         
                        
                         When necessary, reasonable modification of course or degree requirements must be made
                           for students with disabilities unless the requirements can be demonstrated as essential
                           to the program or unless modification would fundamentally alter the nature of the
                           program.
                         
                        
                         Prohibitive rules may not be imposed on qualified students with disabilities, such
                           as banning audio recorders, service animals or other necessary equipment or aids in
                           the classroom.
                         
                        
                         The institution must provide appropriate auxiliary aids to qualified students with
                           disabilities when necessary for full educational access. Auxiliary aids include interpreters,
                           note takers, readers, books in alternate format, adaptive equipment, captioned films/videos,
                           etc.
                         
                        
                         Teaching techniques, as well as special equipment and devices used in the classroom,
                           should be adapted in individual cases, when necessary, to ensure equal access.
                         
                        
                         Educational materials must be provided in an alternate format that is effective for
                           the student, when necessary, to ensure access to educational information.
                         
                        
                         Alternate testing and evaluation methods must be used, when necessary, to ensure the
                           student's achievement is being measured rather than his or her impaired sensory, manual
                           or speaking skill, except where such skills are the factors that the test purports
                           to measure.
                         
                        
                         Classes must be relocated, when necessary, to permit access for students with mobility
                           impairments.
                         
                        
                         It is discriminatory to counsel students with disabilities toward more restrictive
                           career objectives than other students with similar interests and abilities.
                         
                        
                         Communications with persons with disabilities must be as effective as communications
                           with others and sometimes must be accomplished by the use of auxiliary aids such as
                           interpreters, telephone relay service for the deaf, the use of a computer, or alternate
                           format materials such as large print, audio recording, etext or Braille for persons
                           with visual impairments.
                         
                        
                         A student with a disability cannot be required to accept an accommodation, aid, service,
                           opportunity or benefit.
                         
                        
                         It is unlawful to retaliate, coerce, intimidate, threaten or interfere with any individual
                           who exercises his/her rights under ADA, or who aids or assists others in doing so.
                         
                        
                         Disability information is confidential and should not be disclosed without individual
                           consent.
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Disability Policies 
                         
                            
                               
                                Overview  
                                Student Responsibilities  
                                Student Disability Grievances  
                                University Disability Access Policy  
                                Attendance Policies  
                                Reduced Course Load  
                            
                         
                      
                      
                      
                         
                            
                                DRS Announcements  
                               Learn what's new at DRS and get important reminders too. 
                            
                            
                                Thanks to Our Volunteer Note-takers!  
                               See how DRS notetakers help our students. 
                            
                            
                                Student and Faculty Awards  
                               Read about the DRS Student and Faculty Awards. 
                            
                            
                                Contact DRS  
                               Information about our office hours and how to reach us. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/21/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


