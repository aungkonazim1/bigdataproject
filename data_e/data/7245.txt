Copying course components between org units | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Copying course components between org units | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Course Administration    »    Managing course components   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Copying course components between org units                       
        
        
       
          
     
           Printer-friendly version       
	The copy components utility lets you copy quizzes, content, grade items, discussion forums, dropbox folders, and nearly every other type of component from another course offering or a course offering’s parent template. Copying components saves you from having to recreate your course’s resources from scratch and can significantly reduce the amount of work required to create or re-offer a course.
 

 
	You can use the copy components feature to:
 

  
		Reuse components created in a previous course offering.
	 
	 
		Add components created by your peers into your own course offering.
	 
	 
		Set up standard components inside a course template and copy them into a new course offering each time a course is re-offered.
	 
  
	To copy components between course offerings, you must be enrolled in both offerings. (If you want to copy components from a peer’s course offering, ask that user to enroll you). You also need to be enrolled in and have access to the parent template to copy components from it into a course offering.
 

 
	You can also copy components into a course template, and you can create standard components within a template and then copy them into the template’s associated course offerings.
 

 
	Overwriting and duplicating content
 

 
	The only components you might overwrite are course files. Course files are overwritten if one of the files being copied has the same name as an existing file. No other components are overwritten.
 

 
	For example, if you have three checklists in your current course offering, and copy two checklists from another offering, you will now have a total of five checklists in the current offering. This is true even if the components are identical. If you are copying components from the same source multiple times, be careful not to copy the same items more than once or you will create duplicates.
 

 
	User data and importing course components
 

 
	User data is not copied. For example, if you copy the discussions component, only the forums and topics are copied, not the posts inside the topics. Similarly, if you copy dropbox folders, user submissions are not be copied; if you copy quizzes, user attempts are not be copied, etc.
 

 
	Links and associations between components
 

 
	 Tip  We recommend that you  Copy All Components  between courses to ensure you maintain all desired associations, and then delete any undesired material in the new course.
 

 
	If you are copying linked or associated components, you must copy all of the related components at the same time. For example, if a discussion topic has release conditions based on the results of a quiz, you must copy all three components—discussions, quizzes, and release conditions—at the same time. Similarly, if you have a quiz that is associated with a grade item, be sure to copy both the quiz and the grade item at the same time. As long as the associated components are copied together, links between them are retained.
 

 
	 Note  To copy release conditions, you must select the Release Conditions component.
 

 
	Special considerations when copying course components
 

    
				Component
			 
			 
				Notes
			 
		   
				Competencies
			 
			 
				Competencies, learning objectives, and associated activities are copied.
			 
		   
				Content
			 
			 
				Does not include Files or Content Display Settings; these must be selected as separate components. Be sure to copy all "Course Files" used in the course as well as the content topics or only the Content topic structure will be copied.
			 
		   
				Checklists
			 
			 
				Due dates for checklist items are not changed when you copy them.
			 
		   
				Groups and Sections
			 
			 
				Auto-enrollments are not run in the new course. You must enroll users yourself.
			 
		   
				Navigation Bars
			 
			 
				Tool Display Names must also be copied if you have modified the names of tools.
			 
		   
				Release Conditions
			 
			 
				The only release conditions that are copied are ones that are attached to and reference other components that are copied at the same time. For instance, if a quiz has a release condition attached that refers to a content topic, it is only copied if both the quiz and the content topic are copied at the same time. Choosing Release Conditions without selecting any other component does nothing.
			 
		    
	Copy components
 

  
		Go to the Import/Export/Copy Components page for the course offering you want to copy components into, by doing one of the following:
		  
				Click    Edit Course  on your course navbar, then click    Import/Export/Copy Components .
			 
			 
				Click  Import/Export/Copy Component  in the Admin Tools widget or from the   Admin Tools menu on the minibar.
			 
		  
	 
		Do one of the following:
		  
				Select  Copy Components from Another Org Unit , and then click  Search for offering  to find a course to copy.
			 
			 
				Select  Parent Template of Current Offering  to copy components from the course template the current course offering belongs to.
			 
		  
	 
		Do one of the following:
		  
				Click  Copy All Components  to copy all course components.
				 
					 Tip  We recommend that you  Copy All Components  between courses to ensure you maintain all desired associations, and then delete any undesired material in the new course.
				 
			 
			 
				Click  Select Components  to specify which components you want to copy, then click  Continue . Click the  Modify  link to make changes, or click  Finish .
				 
					Note the following:
				 

				  
						To see details for course components, click the  Show the current course components  link, then click the    View Detail  icon beside a component to see existing items.
					 
					 
						 For those course components that provide the option to   Include associated files   D2L recommends leaving the option turned on. Click   Continue   >   Finish  .   
						If you encounter issues with the  Include Associated Files  option and want to remove it, contact D2L Support to ask them to disable the  D2L.LE.Conversion.Features.ICopyAssociatedFilesFeature  feature toggle.
					 
				  
		  
	 
		Click  Copy Another Package  or  View Content  to see the results of your copy.
	 
  
	See also
 

  
		 Exporting course components 
	 
	 
		 Importing course components from a file 
	 
      Audience:    Instructor      

    
           

                   ‹ Managing course components 
        
                   up 
        
                   Course import compatibility › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Course Administration  
  
      Course Administration basics    Managing course components    Copying course components between org units    Course import compatibility    Importing course components from a file    Exporting course components    Exportable components    Including associated files when copying content      Understanding IMS Common Cartridge    Understanding Mobile Brand Administration    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
