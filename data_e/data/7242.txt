About Turnitin-enabled dropbox folders | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 About Turnitin-enabled dropbox folders | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   English  
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Dropbox    »    Creating dropbox folders   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          About Turnitin-enabled dropbox folders                       
        
        
       
          
     
           Printer-friendly version       
	Consider the following when creating or editing your Turnitin-enabled dropbox folder:
 

  
		When creating a new dropbox folder, the name of the folder in Turnitin truncates after 99 characters, even though the   Name   field accepts 128 characters.
	 
	 
		When you copy course components from one course to another, Turnitin settings also copy.
	 
	 
		If you want learners to see instructions on the  Submit Files  page, you must enter them into the  Instructions  rich text field in the  Properties  tab. These instructions are automatically transferred into Turnitin. Any instructions you enter in the  Properties  tab overwrite instructions entered into Turnitin.
	 
	 
		If GradeMark is activated for your org unit, in the  Turnitin  tab, the  GradeMark Available to Learners  date defaults to the current date and time, plus 7 days. In the  Restrictions  tab, if you select  Has End Date , the  GradeMark Available to Learners  date automatically sets to the end date, plus 1 day. You can change either of these dates if needed.
	 
	 
		If you want learners to see the Originality Score link to Turnitin:
		  
				Your administrator must map their role to the  Student  IMS role.
			 
			 
				In the  Properties  tab, ensure  Allow learners to see Turnitin similarity scores in their dropbox folder  is selected. This option is selected by default.
			 
			 
				In Turnitin, under  Optional Settings , ensure  Allow students to view Originality Reports?  is set to  Yes . This option is set to  Yes  by default.
			 
		  
	 
		In the  Properties  tab, if you select  Allow learners to see Turnitin similarity scores in their dropbox folder , the equivalent option in Turnitin called  Allow students to view Originality Reports?  is automatically set to  Yes .
	 
	 
		In Turnitin, under  Optional Settings ,  Allow late submissions?  defaults to  Yes . This means you don't need to change end dates to allow learners to submit late assignments.
	 
      Audience:    Instructor      
    
         
               ‹ Creating dropbox folders 
                     up 
                     About Turnitin-enabled group dropbox folders › 
           
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Dropbox  
  
      Dropbox basics    Creating and managing Dropbox    Creating dropbox categories    Creating dropbox folders    About Turnitin-enabled dropbox folders    About Turnitin-enabled group dropbox folders      Managing dropbox folder submission handling    Setting dropbox folder availability and due dates    Setting release conditions for a dropbox folder    Adding special access permissions to a dropbox folder    Editing dropbox categories and folders    Reordering dropbox categories and folders    Deleting dropbox categories and folders    Restoring deleted dropbox folders    Viewing the Dropbox event log    Associating dropbox folders with learning objectives    Previewing dropbox folders and submissions      Evaluating dropbox folder submissions    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
