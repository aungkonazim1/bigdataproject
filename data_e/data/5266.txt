Viewing the Dropbox event log | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Viewing the Dropbox event log | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Dropbox    »    Creating and managing Dropbox   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Viewing the Dropbox event log                       
        
        
       
          
     
           Printer-friendly version      	 The Dropbox event log enables you to view information on specific activity 
	 for individual dropbox folders. The event log tracks when users create, delete, and restore dropbox folders. Once an action is taken, the event log records the name and ID of the dropbox folder, the action, the user who made the change, and the date it was performed. 
	  Note Dropbox event log only tracks folder items you create, delete, and restore since Learning Environment 10.2. Dropbox folders you import from any previous Learning Environment  are not tracked in event log until you act on them (for example, delete a folder). 
 View the Dropbox event log 
	  On the Dropbox Folders page, click   Event Log  from the More Actions button. 
		 You can click on the  Created ,  Deleted , and  Restored  filters to view dropbox folder items according to the action taken on them. 
		 If you want to view all event log items, deselect all  filters and perform a blank search in the   Search   field. You can also search for a specific folder or folder ID by entering it in the  Search   field and clicking   Search . 
	   Note  You can change 
	 the sort order of results by clicking on the  Folder Name ,  Action , and  Date  column headers.      Audience:    Instructor      

    
           

                   ‹ Restoring deleted dropbox folders 
        
                   up 
        
                   Associating dropbox folders with learning objectives › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Dropbox  
  
      Dropbox basics    Creating and managing Dropbox    Creating dropbox categories    Creating dropbox folders    Managing dropbox folder submission handling    Setting dropbox folder availability and due dates    Setting release conditions for a dropbox folder    Adding special access permissions to a dropbox folder    Editing dropbox categories and folders    Reordering dropbox categories and folders    Deleting dropbox categories and folders    Restoring deleted dropbox folders    Viewing the Dropbox event log    Associating dropbox folders with learning objectives    Previewing dropbox folders and submissions      Evaluating dropbox folder submissions    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
