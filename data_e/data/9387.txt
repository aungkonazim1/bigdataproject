Earthworm Program: ewmseedarchiver overview   
   Earthworm Program: ewmseedarchiver overview   
 
 Earthworm Program: 
ewmseedarchiver Overview 
 (last revised April 21, 2015)  
 
The  ewmseedarchiver  program reads (selected) TYPE_MSEED
messages from an Earthworm transport ring and writes the miniSEED
records to the file system in a flexible file layout. The  earthworm_global.d  file should have the TYPE_MSEED packet type defined. The   tbuf2mseed  
program may be used read TRACEBUF2 messages from one ring, convert them
to MiniSEED messages, and deposit them into another ring, and then have
the  ewmseedarchiver  read them from there. 
 
 
  
   Module Index  |  ewmseedarchiver Commands 
  
 
  
 
  Questions? Issues?  Subscribe to the Earthworm Google Groups List.   
  
  