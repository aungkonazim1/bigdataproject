University College - Graduate School - University of Memphis    










 
 
 
     



 
    
    
    University College  - 
      	Graduate School 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Graduate School
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Future Students  
                          Current Students  
                          Degrees  
                          News   Events  
                          Resources  
                          Contact Us  
                      
                      
                         
                            Resources   
                            
                               
                                  
                                   Graduate School Calendar  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Graduate School 
                              	  
                          
                              	Resources
                              	  
                         University College  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      University College 
                     
                      The University bestows the designation "Graduate Faculty" upon faculty following review
                        of their credentials and recommendation by their colleagues. The University maintains
                        six levels of graduate faculty: (1) Full, (2) Associate, (3) Adjunct, (4) Adjunct
                        Research Co-Mentor, (5) Affiliate, and (6) Adjunct Teaching.
                      
                     
                      Only Full graduate faculty members may chair doctoral committees. Adjunct Research
                        Co-Mentor members may serve as co-chair of a master's or doctoral committee. Full
                        or Associate graduate faculty may chair master's committees. Only one adjunct or affiliate
                        graduate faculty member may serve as a voting member on any master's or doctoral committee.
                      
                     
                      Teaching adjunct members may not serve on graduate committees. 
                     
                      
                     
                      Full MEMBERS 
                     
                      COLIN CHAPELL, Instructor, PhD (2011), University Alabama [2023] 
                     
                      DAN L. LATTIMORE, Professor, PhD (1972), University of Wisconsin-Madison [2023] 
                     
                      HERBERT MCCREE, Instructor, EdD (1996), University of Memphis [2023] 
                     
                      
                     
                        
                     
                      ASSOCIATE MEMBERS 
                     
                      WILLIAM AKEY, Vice President in Enrollment Services, EdD (2006), University of Memphis
                        [2020]
                      
                     
                      JOY AUSTIN, Instructor,  DA (1983), University of Mississippi [2022] 
                     
                      HAL L. FREEMAN, JR., Instructor, MBA (1999), Union University [2020] 
                     
                      JOANNE GIKAS, Interim Dean, EdD (2011), University of Memphis [2020] 
                     
                      TIFFANY JOHNSON, Instructor, JD (2002), Georgetown University Law Center [2022] 
                     
                      LORETTA RUDD, Clincial Associate Professor, PhD (2003), Baylor University [2021] 
                     
                      RON SERINO, Instructor, PhD (2016), Brite Divinity School [2023] 
                     
                      
                     
                      ADJUNCT TEACHING 
                     
                      FELICIA FOWLER, EdD (2002), University of Memphis [2020] 
                     
                      RAYLEAN HENRY, EdD (2000), Tennessee State University [2017] 
                     
                      TIMOTHY LENT HOWARD, EdD (2011), University of the Pacific [2017] 
                     
                      MABEL T. HIMEL, EdD (1994), Univeristy of Memphis [2018] 
                     
                      
                     
                      AFFILIATE MEMBERS 
                     
                      LUCIEN M. GARRETT III, PhD (1998), University of Georgia [2018] 
                     
                      CYNTHIA HILL, MBA (2007), Centenary College [2020] 
                     
                      RADIE KRUEGER, EdD, Nova Southeastern University [2020] 
                     
                      THOMAS RUSSELL, PhD (1999), Vanderbilt University [2018] 
                     
                      
                     
                       NOTE:  Expiration date of graduate faculty membership is indicated in brackets.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Resources 
                         
                            
                               
                                Graduate School Calendar  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now!  
                               Take the first step toward your advanced degree. 
                            
                            
                                Degree Programs  
                               Explore our graduate catalog. 
                            
                            
                                Support Graduate Education  
                               Your gift makes a difference! 
                            
                            
                                Contact Us  
                               Questions? The Grad School Staff can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


