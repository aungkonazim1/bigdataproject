Networking Research Lab &gt; Papers   

 
   Networking Research Lab   Papers 
  
	 
	 
    
 

 

 

 
 
  
 
 
  
        
  
   
 
 

 
 
  
 
 
  
 
 

   
    
       
       
       
       
 
 

 
 
  
 
 
   
 
 

 
 
  
   
 
 

 

  DISCLAIMER:   

 This material is presented to ensure timely dissemination of scholarly and technical work. Copyright and all rights therein are retained by authors or by other copyright holders. All persons copying this information are expected to adhere to the terms and constraints invoked by each author's copyright. In most cases, these works may not be reposted without the explicit permission of the copyright holder.

 Personal use of this material is permitted. However, permission to reprint/republish this material for advertising or promotional purposes or for creating new collective works for resale or redistribution to servers or lists, or to reuse any copyrighted component of this work in other works must be obtained from the publishers.

 

 Book Chapter 
 
  Y. Liu , X. Zhao,  L. Wang , B. Zhang, "On the Aggregatability of Router Forwarding Tables",  Solutions for Sustaining Scalability in Internet Growth , IGI Global, forthcoming 
 
 
 D. Jen, M. Meisel, D. Massey,  L. Wang . B. Zhang, L. Zhang, "APT: A Practical Tunneling Architecture for Routing Scalability",  Solutions for Sustaining Scalability in Internet Growth , IGI Global, forthcoming 
 
 
  K. Malasri ,  L. Wang , "Securing Wireless Implantable Health-care Devices,"  McGraw-Hill 2011 Yearbook of Science & Technology  , pp. 296-298, 2011
 
 
 S. Kumar,  L. Wang , " Ad Hoc and Sensor Networks ,"  Wiley Encyclopedia of Computer Science and Engineering , Jan 2009
 
 

 Journal and Magazine Papers 
 
 
Y. Liu, V. Lehman,  L. Wang , "Efficient FIB Caching using Minimal Non-overlapping Prefixes,"  Computer Networks , vol. 83, pp. 85-99, June 2015
 
 
 
J. P. Abraham, Y. Liu,  L. Wang , B. Zhang, "A Flexible Quagga-based Virtual Network with FIB Aggregation,"  IEEE Network , vol. 28, no. 5, pp. 47-53, Sept. 2014
 
 
 
L. Zhang, A. Afanasyev, J. Burke, V. Jacobson, kc claffy, P. Crowley, C. Papadopoulos,  L. Wang , B. Zhang, "Named Data Networking,"  ACM SIGCOMM Computer Communication Review , vol. 44, no. 3, pp. 66-73, July 2014 
 
 
 
C. Yi, A. Afanasyev, I. Moiseenko,  L. Wang , B. Zhang, L. Zhang, "A Case for Stateful Forwarding Plane,"  Elsevier Computer Communications, Special Issue on Information-Centric Networking , vol. 36, no. 7, pp. 779-791, April 2013
 
 
 
M.  Khan, E.  McCracken, K.  Islam, S.  Bhurtel,  L.  Wang , R.  Kozma, K. M. Iftekharuddin, "Autonomous Wireless Radar Sensor Mote for Target Material Classification,"  Elsevier Digital Signal Processing , to appear
 
 
 
 Y. Liu ,  S. Amin , L. Wang, " Efficient FIB Caching using Minimal Non-overlapping Prefixes ,"  ACM SIGCOMM Computer Communication Review (CCR) , vol. 43, no. 1, pp. 15-21, Jan. 2013.
 
 
 
A. Afanasyev, C. Yi,  L. Wang , B. Zhang, L. Zhang, " Adaptive Forwarding in Named Data Networking ,"  ACM SIGCOMM Computer Communication Reviews (Editorial Note) , vol. 42, no. 3, pp. 62-67, Jul. 2012
 
 
 
R. Kozma,  L. Wang , K. M. Iftekharuddin,  E. McCracken , M. Khan, K. Islam, S. R Bhurtel, R. M. Demirer, " Multi-Modal Radar-Enabled Sensor System Integrating COTS Technology for Surveillance and Tracking ,"  Sensors , vol. 12, no. 2, pp. 1336-1351, Jan. 2012
 
 
 
V. Khare, D. Jen, X. Zhao,  Y. Liu , B. Zhang, D. Massey,  L. Wang , L. Zhang, " Evolution towards Global Routing Scalability ,"  IEEE Journal on Selected Areas in Communications, Special Issue on Internet Routing Scalability , vol. 28, no. 8, pp. 1363-1375, Oct. 2010
 
 
 
 K. Malasri ,  L. Wang , " Design and Implementation of a Secure Wireless Mote-based Medical Sensor Network ,"  Sensors , vol. 9, no. 8, pp. 6273-6297, August 2009 
 
 
 
 K. Malasri ,  L. Wang , " Securing Wireless Implantable Devices for Healthcare: Ideas and Challenges ,"  IEEE Communications , vol. 47, no. 7, pp. 74-80, July 2009
 
 
 
 L. Wang , D. Massey, L. Zhang, " Persistent Detection and Recovery of State Inconsistencies ,"  Computer Networks ,  vol. 51, no. 6, pp. 1444-1458, April 2007
 
 
 
 L. Wang , Y. Xiao, " A Survey of 
Energy-Efficient Scheduling Mechanisms in Sensor Networks ," 
  Mobile Network and Applications (MONET) , 11(5), pp. 723-740, Oct. 2006
 
 
 
 Conference, Workshop and Poster Papers 
 
 V. Lehman, A. Gawande, R. Aldecoa, D. Krioukov, B. Zhang, L. Zhang,  L. Wang  " An Experimental Investigation of Hyperbolic Routing with a Smart Forwarding Plane in NDN ", in   Proceedings of the IEEE IWQoS Symposium  , June 2016 (acceptance ratio: 20.6% = 27/131). 
 
 J. Cao, D. Pei, Z. Wu, X. Zhang, B. Zhang,  L. Wang , Y. Zhao, " Improving the Freshness of NDN Forwarding States ", in   Proceedings of IFIP Networking  , May 2016 (acceptance ratio: 29% = 58/200). 
 
 A. Afanasyev, C. Yi,  L. Wang , B. Zhang, L. Zhang, " SNAMP: Secure Namespace Mapping to Scale NDN Forwarding ", in  Proceedings of the 18th IEEE Global Internet Symposium (GI 2015) , pp. 281-286, April 2015 
 
 C. Yi, J. Abraham, A. Afanasyev,  L. Wang , B. Zhang, L. Zhang, " On the Role of Routing in Named Data Networking ", in  Proceedings of 1st ACM Conference on Information-Centric Networking , pp. 27-36, Sept. 2014 
 
 
 AKM M. Hoque ,  S. O. Amin ,  A. Alyyan , B. Zhang, L. Zhang,  L. Wang , " NLSR: Named-data Link State Routing Protocol ", to appear in  ACM SIGCOMM ICN Workshop , August 2013
 
 
 
 Y. Liu , B. Zhang,  L. Wang , " FIFA: Fast Incremental FIB Aggregation ," in  Proceedings of IEEE INFOCOM , April 2013 (acceptance ratio 17.4% = 280/1613)
 
 
 
K. M. Iftekharuddin, M. M. R. Khan,  E. McCracken ,  L. Wang , R. Kozma, " Autonomous Wireless Radar Sensor Mote Integrating a Doppler Radar into a Sensor Mote and its Application in Surveillance and Target Material Classification ," in  Proceedings of 56th SPIE Annual Meeting , Vol. 8134, 15 pages, San Diego, CA, Sept. 2011.
 
 
 
D. Massey, C. Papadopoulos,  L. Wang , B. Zhang, L. Zhang, " Teaching Network Architecture through Case Studies ,"  SIGCOMM 2011 Education workshop , August 2011
 
 
 
 Y. Liu , X. Zhao, K. Nam,  L. Wang , B. Zhang, " Incremental Forwarding Table Aggregation ," in  Proceedings of IEEE GLOBECOM Next-Generation Networking (NGN) Symposium , Dec. 2010  
 
 
 
 
R. Kozma,  L. Wang , K. Iftekharuddin,  E. McCracken , M. Khan, K. Islam,
R. M. Demirer, " A Multi-Modal Sensor System Integrating COTS Technology for
Surveillance and Tracking ," in  Proceedings of IEEE International Radar Conference , May 2010
 
 
 
X. Zhao,  Y. Liu ,  L. Wang , B. Zhang, " On the Aggregatability of Router Forwarding Tables ," in  Proceedings of IEEE INFOCOM 2010 , March 2010 (acceptance ratio 17.5% = 276/1575)
 
 
 
 L. Wang , Q. Wu,  Y. Liu , " Design and Validation of PATRICIA for the
Mitigation of Network Flooding Attacks ," in  Proceedings of the IEEE/IFIP 
International Symposium on Trusted Computing and Communications ,
Vancouver, Canada, August 29-31, 2009 (this version corrects one of the references).
 
 
 
H. Yan, D. Massey,  E. McCracken ,  L. Wang , " BGPMon and NetViews: Real-Time BGP Monitoring System ,"  IEEE INFOCOM , demo, April 2009
 
 
 
D. Jen, M. Meisel, H. Yan, D. Massey,  L. Wang , B. Zhang, L. Zhang, " Towards A New Internet Routing Architecture: Arguments for Separating Edges from Transit Core ," in  Proceedings of the Seventh ACM Workshop on Hot Topics in Networks (HotNets-VII) , Calgery, Alberta, Canada, Oct 2008 (acceptance ratio 20% = 22/110)
 
 
 
 K. Malasri ,  L. Wang , " Design and Implementation of a Secure Wireless Mote-based Medical Sensor Network ," in  Proceedings of the 10th International Conference on Ubiquitous Computing (UbiComp) , Sept. 2008 (acceptance ratio 18.6% = 42/226)
 
 
 
 L. Wang , Q. Wu, D.  D. Luong , " Engaging Edge Networks in Preventing and Mitigating Undesirable Network Traffic ," in  Proceedings of the 3rd Workshop on Secure Network Protocols (NPSEC)  in conjunction with IEEE ICNP, Beijing, China, Oct. 2007
 
 
 
D. Massey,  L. Wang , B. Zhang, L. Zhang, " A Scalable Routing System Design for Future Internet ," in  Proceedings of ACM SIGCOMM Workshop on IPv6 and the Future of the Internet , August 2007 (acceptance ratio = 29.3%)
 
 
 
 K. Malasri ,  L. Wang , " Addressing Security in Medical Sensor Networks ," in  Proceedings of ACM SIGMOBILE Workshop on Systems and Networking Support for Healthcare and Assisted Living Environments (HealthNet) , June 2007 (acceptance ratio 26% = 13/50)
 
 
 
 L. Wang , C. Ellis, W. Yin, D.  D. Luong , " Hercules: An Environment for Large-Scale Enterprise Infrastructure Testing ," in  Proceedings of the Workshop on Advances and Innovations in Systems Testing  , May 2007
 
 
 
 L. Wang ,  M. Saranu , J. Gottlieb, D. Pei, " Understanding BGP Session Failures in a Large ISP ," in  Proceedings of the IEEE INFOCOM 2007 , May 2007 (acceptance ratio = 18%).
 
 
 
B. Zhang, V. Kambhampati, D. Massey, R. Oliveira, D. Pei,  L. Wang , 
L. Zhang, " A Secure and Scalable Internet 
Routing Architecture ," 
 ACM SIGCOMM , Poster, Pisa, Italy, Sept. 2006
 
 
 
 K. Malasri ,  L. Wang , " 
SNAP: An Architecture for
Secure Medical Sensor Networks ,"  IEEE SECON , Poster, Sept. 2006
 
 
 S. Balachandran, D. Dasgupta,  L. Wang , "A Hybrid Approach for Misbehavior Detection in Wireless Ad-Hoc Networks," in  Proceedings of the Symposium on Information Assurance , June 2006
 
 
 
 L. Wang , Y. Xiao, " Energy Saving Mechanisms in Sensor Networks ," in  Proceedings of the IEEE Broadnets 2005 , Oct. 2005
 
 
 
B. Bradley,  L. Wang , " Understanding
Malicious Worms' Impact on the Internet Infrastructure through Realistic
Simulation
 ,"  USENIX NSDI (Networked System Design and Implementation) , Poster,
Boston, MA, May 2005
 
 
 

 Other Publications (Technical Reports, Internet Drafts) 
 
 
 Y. Liu , B. Zhang,  L. Wang , " FIFA: Fast Incremental FIB Aggregation ," University of Memphis Computer Science Department Technical Report No. CS-13-004, July 2013
 
 
 
 L. Wang ,  AKM M. Hoque , C. Yi,  A. Alyyan , B. Zhang, "OSPFN: An OSPF Based Routing Protocol for Named Data Networking," NDN Technical Report NDN-0003, July 2012
 
 
 
C. Yi, A. Afanasyev, I. Moiseenko,  L. Wang , B. Zhang, L. Zhang, "A Case for Stateful Forwarding Plane," NDN Technical Report NDN-0002, July 2012.
 
 
 
B. Zhang, L. Zhang,  L. Wang , "Evolution Towards Global Routing 
Scalability," Internet draft, draft-zhang-zhang-evolution-02.txt, Oct. 2009 
 
 
 
B. Zhang,  L. Wang , X. Zhao,  Y. Liu , L. Zhang, "FIB Aggregation," 
Internet Draft, draft-zhang-fibaggregation-02.txt, Oct. 2009
 
 
 
D. Jen, M. Meisel, D. Massey,  L. Wang , B. Zhang, L. Zhang, "APT: A Practical Transit Mapping Service," Internet Draft, draft-jen-apt-00.txt, July 2007
 
 
 
 L. Wang , Q. Wu, D.  D. Luong , "Engaging Edge Networks in Preventing and Mitigating Undesirable Network Traffic," U. Memphis Computer Science Dept Technical Report No. CS-07-004, May 2007
 
 
 
D. Massey,  L. Wang , B. Zhang, L. Zhang, "A Proposal for Scalable Internet Routing & Addressing," Internet Draft, http://www.ietf.org/internet-drafts/draft-wang-ietf-efit-00.txt, Feb. 2007
 
 
 
B. Zhang, D. Massey, D. Pei, L. Wang,  L. Zhang , R. Oliveira, V. 
Kambhampati, "A Secure and Scalable Internet Routing Architecture (SIRA)," 
University of Arizona Technical Report TR06-01, Apr. 2006
 
 
 
 L. Wang , Y. Xiao, "Energy Saving Mechanisms in Sensor Networks," University of Memphis Computer Science Dept Technical Report No. CS-05-003, May 2005
 
 
 
 L. Wang , D. Massey, L. Zhang, "Persistent Detection and Recovery of BGP 
Routing Inconsistencies," Technical Report No. CS-05-002, 
University of Memphis Computer Science Department, Mar. 2005. 
 
 
 

 

 
 


 
 
 
 
 home     people     projects     papers  
 

 
   University of Memphis
 

 
 


 

 

 
