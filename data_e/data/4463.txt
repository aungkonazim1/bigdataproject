Co-Op/Internship - Herff College of Engineering - University of Memphis    










 
 
 
     



 
    
    
    Co-Op/Internship  - 
      	Herff College of Engineering
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Herff College of Engineering
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Future Students  
                          Students  
                          Programs  
                          Research  
                          Alumni  
                          Events  
                      
                      
                         
                            Future Students   
                            
                               
                                  
                                   Choose Your Major  
                                   Visit the College  
                                   Apply to the UofM  
                                   Scholarships  
                                   Student Life  
                                   FAQs  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Herff College of Engineering
                              	  
                          
                              	Future Students
                              	  
                         Co-Op/Internship  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      The resume builder that changes everything:  Co-op   Internships 
                     
                      Do you want to work part-time while completing your degree? We have a position for
                        you!
                      
                     
                      Do you want to alternate between full-time work and full-time academics? We have a
                        position for you!
                      
                     
                      Do you need help to pay for your college education? We have industry partners with
                        tuition reimbursement.
                      
                     
                      Many Herff students land permanent jobs before they even graduate. That's because
                        Herff helps you build a strong résumé while you are in college. We have a national
                        reputation for providing our students with relevant engineering work experience through
                        a paid Cooperative Education/Internship Program. U.S. News   World Report ranks the UofM in the Top 10 universities with successful internship
                        programs.
                      
                     
                       How Herff Jump Starts Your Career in Engineering:  
                     
                      
                        
                         The Tennessee Society of Professional Engineers (TSPE) equates three semesters of
                           co-op coursework as equivalent to one year's worth of professional engineering work
                           experience.
                         
                        
                         Students with co-op credit or internship credit on their academic transcripts get
                           higher starting salaries.
                         
                        
                         Herff engineering co-op/internship positions are paid positions with salaries well
                           above minimum wage.
                         
                        
                         Many of our industrial partners provide on-site training for Herff student interns
                           because they hope to hire them permanently upon graduation.
                         
                        
                      
                     
                       Important Links  
                     
                       Information about the Herff Co-op   Internship program  
                     
                       Information about the university internship program  
                     
                      Cooperative Education Forms: Agency 
                     
                      
                        
                          Job Description  
                        
                          Agency Agreement  
                        
                          Agency Questionnaire   
                        
                          Student Evaluation Form  
                        
                      
                     
                      Cooperative Education Forms: Students 
                     
                      
                        
                          Student Application   
                        
                          Academic Release   Statement of Understanding  
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Future Students 
                         
                            
                               
                                Choose Your Major  
                                Visit the College  
                                Apply to the UofM  
                                Scholarships  
                                Student Life  
                                FAQs  
                            
                         
                      
                      
                      
                         
                            
                                Visit Herff  
                               See what Herff has to offer YOU! 
                            
                            
                                Apply to UofM  
                               Submit your application and enroll in the College of Engineering 
                            
                            
                                2017 E-day Results  
                               Thanks to 3,000 student participants!  
                            
                            
                                Contact Us  
                               Have Questions? We can help!  
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/29/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


