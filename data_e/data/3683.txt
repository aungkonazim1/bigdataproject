Senior Citizen Programs - Bursar's Office - University of Memphis    










 
 
 
     



 
    
    
    Senior Citizen Programs - 
      	Bursar's Office
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Bursar's Office
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Staff Directory  
                          Fees  
                          Calendars  
                          Students  
                          Parents  
                          Campus Card  
                          Bursar Areas  
                      
                      
                         
                            Fees   
                            
                               
                                  
                                   Fee Charts  
                                   Fee Payment Policies  
                                   Deadlines, Refunds   Important Dates  
                                   Fee Payment Methods  
                                   Installment Payment Plan  
                                   Explanation of Fees  
                                   Financial Assistance/ Sponsorships General Info  
                                   Fee Discounts   Waivers  
                                   Fee Adjustment (Refund) Policy  
                                   Third Party/Direct Billing  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Bursar's Office
                              	  
                          
                              	Fees
                              	  
                         Senior Citizen Programs 
                      
                   
                   
                       
                      
                     		
                     
                     
                      Senior Citizen Programs 
                     
                      Enrollment for audit or credit is limited according to space availability. To qualify
                        for the "tuition reduction" in this program, participants may not register before
                        the specific deadline for each semester. See the  Bursar's Office Calendar  for the official first day to register under these fee waiver programs.
                      
                     
                      Age Sixty (60) – Audit Only 
                     
                      Tennessee residents who will become age sixty (60) during the academic semester may
                        AUDIT courses without paying tuition charges. These charges may include the following:
                        maintenance fees, program services fees, and special course fees for Business, Fine
                        Art, Engineering, Law or Nursing Programs. Students enrolled in 12 or more hours will
                        be responsible for the mandatory  Dining Dollars .
                      
                     
                      Seniors taking courses for  AUDIT , please contact the Assistant Registrar at 901-678-3931.
                      
                     
                      Age Sixty-five (65) – Credit Course(s) 
                     
                      Tennessee residents who will become age sixty-five (65) during the academic semester
                        may enroll in courses for  CREDIT  at the cost of one-half the normal per credit hour fee, not to exceed $70 per semester.
                        These charges include maintenance fees and program services fees.  Fees not included  in this fee waiver include, but are not limited to, Fine Art course fees, Business
                        Course fees, Engineering course fees, UM online program fees, RODP online program
                        fees,  Dining Dollars , and materials fees for courses which require an additional fee.
                      
                     
                      AFTER COURSE SELECTION, participants must contact Student Accounts at 901.678.2712
                         prior to the appropriate  fee payment deadline   to avoid deletion of classes. Students who adjust their class schedule after the
                        fee waiver has been applied to their account  must  notify the Bursar's Office at 901.678.2712 of any changes to ensure proper credit
                        is applied and avoid possible deletion of classes and $100 late payment fee.
                      
                     
                     	
                      
                   
                
                
                   
                      
                         Fees 
                         
                            
                               
                                Fee Charts  
                                Fee Payment Policies  
                                Deadlines, Refunds   Important Dates  
                                Fee Payment Methods  
                                Installment Payment Plan  
                                Explanation of Fees  
                                Financial Assistance/ Sponsorships General Info  
                                Fee Discounts   Waivers  
                                Fee Adjustment (Refund) Policy  
                                Third Party/Direct Billing  
                            
                         
                      
                      
                      
                         
                            
                                TigerXpress  
                               View your statements and pay fees online in TigerXpress. 
                            
                            
                                Tuition Estimator  
                               How much will your courses cost? Get an estimate online! 
                            
                            
                                Forms  
                               All of B F's forms in one place 
                            
                            
                                Business   Finance  
                               The Division of Business   Finance at the U of M 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


