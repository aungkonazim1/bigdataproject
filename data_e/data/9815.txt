Accessing the Course Offering Information page | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Accessing the Course Offering Information page | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Course Administration    »    Course Administration basics   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Accessing the Course Offering Information page                       
        
        
       
          
     
           Printer-friendly version       
	You can edit the properties of your course offering from the Course Offering Information page.
 

 
	Access the Course Offering Information page
 

 
	Do one of the following:
 

  
		Click    Edit Course  on your course navbar, then click    Course Offering Information .
	 
	 
		In the  Course Administration  widget on your course homepage, click  Course Offering Information .
	 
      Audience:    Instructor      

    
           

                   ‹ Accessing Course Administration 
        
                   up 
        
                   Editing course colors › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Course Administration  
  
      Course Administration basics    Accessing Course Administration    Accessing the Course Offering Information page    Editing course colors    Setting up course tools    Renaming a tool in a course offering    Editing the course locale      Managing course components    Understanding IMS Common Cartridge    Understanding Mobile Brand Administration    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
