Creating a news item | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Creating a news item | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    News    »    Creating and managing news items   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Creating a news item                       
        
        
       
          
     
           Printer-friendly version        
		Do one of the following:
		  
				Click    New News Item  from the News widget menu.
			 
			 
				On the News page, click  New Item .
			 
		  
	 
		Enter the news headline in the  Headline  field.
	 
	 
		Enter the news content in the  Content  field.
	 
	 
		If you want to hide your headline's date and time, clear the  Always show start date  check box. By default, a news item's start date and time appears below its headline when displayed in the News widget.
	 
	 
		Select the posting availability date in the  Start Date  and  End Date  fields. News items publish immediately unless you specify an alternate start date from   Start Date .  
	 
		You can remove a news item on a specific date by selecting the check box  Remove news item based on end date . You and administrators can still see the news item after its end date, but it will not appear in the News widget.
	 
	 
		You can include attachments, audio recordings, and video recordings as part of your news item:
			  
				To add an attachment, click  Add a File  in the Attachments section.
			 
			 
				To record audio, click  Record Audio  in the Attachments section. Click  Flash Settings  to make adjustments to your microphone selection and volume. Click  Play  to listen to your recording. Click  Clear  to erase your recording.
			 
			 
				To record video, click  Record Video  in the Attachments section.
			 
		  
	 
		Click  Attach Existing  or  Create and Attach  to include additional release conditions. See  Release Conditions  to learn about creating and setting up release conditions.
	 
	 
		Click  Save as Draft  to continue editing the news item (students cannot see draft news items), or click  Publish  to release news item to users.
	 
  
	Creating a news item for a future date
 

 
	If you set a news item's start date for the future, the posting is not visible to users until that date.
 

 
	 Example  If you want a news item to appear on Monday morning, specify a start date of Monday at 9:00am.
 

 
	 Note  Post-dated news items do not display in the News widget even if users have permission to see future news items. They can be found within the list of news items on the News page.
 

 
	See also
 

  
		 Release Conditions 
	 
      Audience:    Instructor      

    
           

                   ‹ Creating and managing news items 
        
                   up 
        
                   Editing a news item › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        News  
  
      News basics    Creating and managing news items    Creating a news item    Editing a news item    Reordering news items    Deleting a news item    Restore a deleted News item      
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
