The University Libraries Hosts Faculty Scholarship Week, April 13 &#8211; 17 | University of Memphis Libraries   
 
 
 
 
 The University Libraries Hosts Faculty Scholarship Week, April 13   17 | University of Memphis Libraries 
 
 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 
 




 
 
  
 
 
 

 
 
 
 
			

         



 

 
 
	 Skip to content 
		 
				 
						  University of Memphis Libraries  
			 News and Events 
		 

		 
			 Menu 
			  
  Home    
		  
	  

	 
			 
			 Home  The University Libraries Hosts Faculty Scholarship Week, April 13   17 		 
		 
		 

					 
				
 
	 
					 The University Libraries Hosts Faculty Scholarship Week, April 13   17 
		
		 
						   April 14, 2015  April 14, 2015       mswrngen   
			  Events ,  Exhibition ,  Homepage ,  News  		  
	  

	 
		                 College of Communication and Fine Arts  faculty scholarship  
 Each year, the University Libraries at the University of Memphis hosts an exhibition of the faculty’s scholarship from the previous year. This year, Faculty Scholarship Week will be observed April 13-17. The event celebrates the outstanding research, writing, performance and other scholarly works of the University’s faculty. Faculty-authored and created publications and works in a variety of media from numerous U of M colleges and departments will be displayed in the Ned R. McWherter Library rotunda beginning at noon on Monday, April 13. The exhibition will close on Friday, April 17, at 4 p.m. 
 The 2015 exhibition includes scholarship from Cecil C. Humphreys School of Law, College of Arts and Sciences (including Political Science, Sociology, Mathematical Sciences, Physics, Earth Sciences, English, Foreign Languages and Literature, and History), College of Communication and Fine Arts (including Architecture, Art, Communication, Music, and Theater and Dance), College of Education, Health and Human Sciences (including Counseling, Education Psychology and Research), Fogelman College of Business and Economics (including Economics, FIR, and Management Information Systems), Herff College of Engineering (including Biomedical Engineering), Loewenberg School of Nursing, School of Communication Sciences and Disorders, and the University Libraries. 
 Faculty Scholarship Week is sponsored by the University Libraries with the support of the Friends of Libraries. 
 For more information, contact Anna Swearengen at   mswrngen@memphis.edu   or 678-2744. 

 
			  

	 
			  
  
			 

				 
		 Post navigation 

	
		      The University Libraries Celebrates Women s History Month 2015  		  Extended Library Hours During Exam Week      
	
	  
	
			
 

	
	
	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  

		
		  
	  

					 
					 		 Recent Posts 		 
					 
				 3D Printing Workshop 
						 
					 
				 NedXStudents 
						 
					 
				 Craft-n-Chat 
						 
					 
				 Bridging East and West: The First Steel Bridge of Memphis 
						 
					 
				 Banned Books Display 
						 
				 
		 		  Archives 		 
			  November 2017  
	  October 2017  
	  September 2017  
	  August 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  April 2016  
	  February 2016  
	  September 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  May 2014  
	  April 2014  
	  March 2014  
	  February 2014  
	  January 2014  
		 
		   Categories 		 
	  Alerts 
 
	  Branch Libraries 
 
	  Events 
 
	  Exhibition 
 
	  Homepage 
 
	  News 
 
	  Research and Instructional Services 
 
	  Special Collections 
 
	  Trial Resources 
 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 		  
	
	  

	 
		 
			 
								 Proudly powered by WordPress 
				  |  
				Theme: Big Brother by  WordPress.com .			  
					  
	  
  


 

        

        			 
				   Subscribe  
				 

					
						 Follow this blog 

						 
							
															 Get every new post delivered right to your inbox. 
							
							 
								 
							 
							
							 
							 
							
							  							   
						 

					
				 
			 
		







		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 