Responsibilities of a GA - Graduate School - University of Memphis    










 
 
 
     



 
    
    
    Responsibilities of a GA - 
      	Graduate School 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Graduate School
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Future Students  
                          Current Students  
                          Degrees  
                          News   Events  
                          Resources  
                          Contact Us  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Graduate School 
                              	  
                          
                              	
                              	  
                         Responsibilities of a GA 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Graduate Assistant Responsibilities 
                     
                      
                        
                         Be sure you have a copy of your contract and fully understand its terms.   
                        
                         Graduate Assistants can only work a maximum of 20 hours per week.   
                        
                         You must maintain a 3.0 cumulative grade point average to continue as a graduate assistant.   
                        
                         In order for your tuition and fees to be paid, your contract must reach the Graduate
                           School by the 4th day of classes. It is essential that you provide the department
                           with all relevant information as soon as possible.   
                        
                         When the Bursar's Office processes your contract, they pay your tuition and fees.
                           Any services (such as Parking, Financial Aid, or the Recreation Center) that are contingent
                           upon payment of fees have to wait for your contract to reach that office.   
                        
                         If you are working less than 20 hours per week, you may pay half of your tuition by
                           the appropriate fee payment deadline. For specific fee payment requirements, fee information,
                           deadlines, and penalties, go to the  Bursar's Office web site  and select "Fee Payment Information."   
                        
                         If you hold an assistantship in the Spring semester, you are eligible to pay in-state
                           tuition rates for the summer semester even if you do not hold a graduate assistantship
                           during the summer. See the Graduate School for additional information.   
                        
                         If you have financial aid, you need to notify the Financial Aid Office because the
                           amount of your loan may need to be adjusted.   
                        
                         If you are receiving federal work study as part of your stipend, be sure you fully
                           understand the implications. See the Financial Aid office or the Graduate School for
                           details.
                         
                        
                      
                     
                      If you are a non-resident alien: 
                     
                      
                        
                         You must apply for a social security number as soon as possible.   
                        
                         You must give Human Resources a copy of your social security number.   
                        
                         After obtaining your social security number, you must see Ms. Senese Duhart, Administration
                           Building 276, to complete a Form W-4 for tax purposes.   
                        
                      
                     
                      Last revised on: 30 September 2014 
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Apply Now!  
                               Take the first step toward your advanced degree. 
                            
                            
                                Degree Programs  
                               Explore our graduate catalog. 
                            
                            
                                Support Graduate Education  
                               Your gift makes a difference! 
                            
                            
                                Contact Us  
                               Questions? The Grad School Staff can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


