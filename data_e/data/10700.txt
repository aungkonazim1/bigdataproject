Faculty Services - FCBE - University of Memphis    










 
 
 
     



 
    
    
    Faculty Services - 
      	FCBE
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College of Business   Economics
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          Students  
                          Departments  
                          Faculty   Staff  
                          Research  
                          News  
                      
                      
                         
                            Faculty   Staff   
                            
                               
                                  
                                   Faculty Profiles  
                                   Faculty Services Center  
                                   Faculty Resources  
                                   FCBE Contact List  
                                   Academic Program Assessment  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	FCBE
                              	  
                          
                              	Faculty   Staff
                              	  
                         Faculty Services 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Faculty Services 
                     
                      The Faculty Services Center (FSC) located on the third floor in Room 350 is a one
                        stop service center. The services include duplication of tests, instructional handouts,
                        transparencies, portable instructional equipment check out, and mail services. We
                        also provide duplication of syllabi, but these are supposed to be online, therefore,
                        the cost is charged to the faculty's development account. The following procedures
                        have been devised to guide the delivery of the various services that Faculty Services
                        Center offers.
                      
                     
                       Faculty services staff  
                     
                      
                        
                          Claire Grant  
                        
                          Janet Hicks  
                        
                      
                     
                       Office information  
                     
                      Contact Us 
                     
                      Phone: 901.678.4292 Fax: 901.678.2685
                      
                     
                      Fogelman College of Business   Economics 3675 Central Avenue, Office 350 Memphis, TN 38152-3120
                      
                     
                      Office Hours 
                     
                      Monday - Thursday: 7:30 AM - 6:00 PM Friday: 7:30 AM - 4:30 PM
                      
                     
                      Spring Break, Fall Break, Semester Break 8:00 AM - 4:30 PM
                      
                     
                      Policies   Procedures 
                     
                      Procedure For Duplicating Instructional Material 
                     
                      A copy order form has to be filled out for each copy assignment. This form has to
                        be filled out COMPLETELY. Please count the number of pages, enter the number of copies
                        you want, one-sided, two-sided, collated/stapled, etc. This is a major time saver
                        if we have all of this information as soon as we get the order. If we have a question
                        and cannot reach the faculty member, the order could be delayed several hours.
                      
                     
                      Equipment 
                     
                      Faculty Services provides computer/projectors and TV/VCRs for classrooms that do not
                        have this technology. However, our supply is very limited and reservations must be
                        made in advance of your scheduled presentations. This equipment may be picked up and
                        returned, or FSC will deliver and pick up. If you see that you are not going to need
                        the equipment, PLEASE let FSC know as soon as possible. This not only relieves our
                        office of delivering it to the classroom, but also, it makes the equipment available
                        for someone who may need it at that time.
                      
                     
                      MAIL 
                     
                      Incoming Mail From  Outside  The College
                      
                     
                      All incoming mail from outside the college is delivered to the mailroom in the Dean's
                        Office. Mail will be sorted into the appropriate boxes as a first step in distribution.
                        Mail slots are identified for UG, Master and Ph.D. program as well as for computer
                        support and Technological Advancement functions. Once the initial sorting and distribution
                        is complete, the Faculty Services Center staff will pick up the mail to distribute
                        to individual, locked faculty mail boxes in the faculty mail room located in room
                        212. All doctoral student and part-time instructor mail will be distributed into individual
                        mail boxes also located in room 100. Faculty Service Center will deliver mail at least
                        once a day as a regular practice Monday thru Friday. Mail that is large in size and
                        does not fit into mail boxes will be kept in the Faculty services Center and the individual
                        recipient will be notified by voice mail. In cases of bulk mail or mail that is in
                        large numbers (e.g., surveys, etc.) special arrangements should be made in advance
                        with the Faculty Services Center. Accumulated mail for the faculty with extended leaves
                        will be placed in their offices once their mail boxes are filled, unless arrangements
                        for mail pick up are made by the individual faculty member.
                      
                     
                      Outgoing Mail 
                     
                      Faculty may drop outgoing mail at the Faculty Service Center, room FCB 350, the Faculty
                        Mail room, room 320, or the mail room in the Dean's Office, room FCB 430A. The outgoing
                        mail will be sorted by FSC staff. All outgoing mail must be identified by the individual
                        sender's name. Faculty Service Center staff will sort and deliver the outgoing mail
                        to the mailroom in the Dean's office to be picked up by the postal service. Large
                        quantity mailing requests must be submitted to the Faculty service Center in advance
                        so that appropriate arrangements can be made. FedEx and Priority Mail envelopes and
                        address labels are provided in FSC.
                      
                     
                      From Inside The College 
                     
                      Faculty may send material to other faculty and students either by dropping the material
                        in the designated location within the Faculty Service Center or Faculty Mail Room.
                        (FCB 320) There is a large closed mail container in the Faculty Mail Room for this
                        purpose. Materials left in this container will be distributed as part of the regular
                        mail service. Faculty, Teaching Assistants and Part-time faculty should provide full
                        information to their students and the FSC about how they can be reached by the students
                        at the beginning of each semester. Information to students should include office hours,
                        office numbers, phone numbers and information about locations for students to drop
                        off material for their instructor. All such information should be provided as part
                        of course syllabi and posted on the office door.
                      
                     
                      Procedures for Keys 
                     
                      All keys are checked out through Faculty Services. Office keys, mailbox keys and various
                        building keys will remain with you, but certain classroom keys which you will need
                        should be returned at the end of each semester. This is extremely important. If these
                        keys are not turned in, it causes a shortage for the following semester.
                      
                     
                      If you want your graduate assistant to have a key to your office, you must let FSC
                        know in advance. The key will be checked out in your name and you will be responsible
                        for it. If you do not want your GA to have a key, but want him to have access to your
                        office, you must inform FSC in advance. We will not let anyone into your office without
                        prior notice.
                      
                     
                      Lost Keys 
                     
                      If you lose your keys, they will be replaced, but please make every effort to find
                        them before requesting new ones. A yearly key audit is conducted in early Fall. You
                        must account for all keys that have been issued to you during the year.
                      
                     
                      Handouts and Other Materials 
                     
                      Faculty are encouraged to use library services to place class material on reserve.
                        Such material may include articles, various handouts, and copies of transparencies
                        used in the class. Faculty Services Center cannot duplicate copyrighted material without
                        documented proper approval. FSC will produce overhead transparencies (provided that
                        templates are ready for copying) and class handouts that require immediate distribution.
                        These requests will be handled on a first-come-first serve basis. To ensure timely
                        processing, requests should be submitted at least one (1) day in advance.
                      
                     
                      TEST MATERIALS 
                     
                      Test materials must be turned in at least 24 hours prior to the time needed. It is
                        imperative that the work request form be completed. Please fill out all information
                        required to avoid delays in processing.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Faculty   Staff 
                         
                            
                               
                                Faculty Profiles  
                                Faculty Services Center  
                                Faculty Resources  
                                FCBE Contact List  
                                Academic Program Assessment  
                            
                         
                      
                      
                      
                         
                            
                                Apply to our Undergraduate Programs    
                               Enroll at the university and engage yourself with our six different departments, student
                                 enrichment, and more. 
                               
                            
                            
                                Apply to our Masters Programs    
                               Earn your degree with one of our five MBA tracks or with one of our four specialized
                                 Masters programs!
                               
                            
                            
                                Apply to our Doctoral Programs    
                               Interested in a Ph.D.? Start the journey towards your Ph.D. here! 
                            
                            
                                Contact Us    
                               Have questions? Call or email today!  
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


