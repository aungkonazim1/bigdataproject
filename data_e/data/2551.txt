Emeriti Faculty - Department of History - University of Memphis    










 
 
 
     



 
    
    
    Emeriti Faculty - 
      	Department of History
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of History
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Why History?  
                          Undergraduate  
                          Graduate  
                          Online  
                          People  
                          HERC  
                          GAAAH  
                      
                      
                         
                            Faculty   
                            
                               
                                  
                                   Faculty  
                                   Adjunct   
                                   Emeriti  
                                   Graduate and Teaching Assistants  
                                   Faculty/Staff Resources  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of History
                              	  
                          
                              	Faculty
                              	  
                         Emeriti Faculty 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Emeriti Faculty 
                     
                      
                        
                         
                           
                            
                              
                                 
                              
                                James M. Blythe  459 Meadowcrest Circle Memphis, TN 38117 901.682.8589  jmblythe@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                                Walter R. Brown  698 Trezevant Street Memphis, TN 38112 901.324.8237  wrbrown@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                                Margaret M. Caffrey  464 Meadowcrest Circle Memphis, TN 38117 901.683.5823  mcaffrey@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                                James R. Chumney  5273 Laurie Lane Memphis, TN 38120 901.683.0141  jchumney@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                                Maurice A. Crouse  1595 Wheaton Street Memphis, TN 38117 901.683.5808  mcrouse@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                                Donald Ellis  27 Callingham Road Pittsford, NY 14534 405-819-9158  donaldwesleyellis@gmail.com  
                              
                            
                           
                            
                              
                                 
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                                Robert J. Frankle  219 N. Avalon Memphis, TN 38112 901.276.3775  rfrankle@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                                Joseph M. Hawes  1361 Carr Avenue Memphis, TN 38104 901.726.1813  jhawes@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                                F. Jack Hurley  18911 Craggy Meadows Cl Davidson, NC 28036 704.987.9185  hurleyj1@bellsouth.net  
                              
                            
                           
                            
                              
                                 
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                                Berkley Kalin  P. O. Box 7741 Bloomington, IN 47404 812-272-9373  profberkley@hotmail.com  
                              
                            
                           
                            
                              
                                 
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                                Janann Sherman  727 North Haven Road Vinalhaven, ME 04863 207.867.2000  sherman@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                                C. Edward Skeen  7280 Cedar Lane Drive Germantown, TN 38138 901.754.2994  ceskeen@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                                David Tucker  710 Sanga Creek Cordova, TN 38018 901.737.5634
                               
                              
                            
                           
                            
                              
                                 
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                                Major L. Wilson  452 Woodmere Ln Memphis, TN 38117 901.683.4802
                               
                              
                            
                           
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Faculty 
                         
                            
                               
                                Faculty  
                                Adjunct   
                                Emeriti  
                                Graduate and Teaching Assistants  
                                Faculty/Staff Resources  
                            
                         
                      
                      
                      
                         
                            
                                History Happenings  
                               Significant happenings that involve our faculty, students, staff, and alumni. 
                            
                            
                                Newsletter  
                               The department newsletter is filled with interesting articles and information about
                                 our award-winning faculty and students.
                               
                            
                            
                                Event Calendar  
                               Check here often for upcoming events hosted by the Department of History 
                            
                            
                                Contact Us  
                               Contact the Department of History at The University of Memphis. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


