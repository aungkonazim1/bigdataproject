PETER WRIGHT - MGMT - University of Memphis    










 
 
 
     



 
    
    
    PETER WRIGHT - 
      	MGMT
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - Department of Management
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Welcome  
                          Programs  
                          Courses  
                          Faculty  
                          PhD Students  
                          Research  
                          FCBE  
                      
                      
                         
                            Faculty   
                            
                               
                                  
                                   Welcome  
                                   Programs  
                                   Courses  
                                   Faculty  
                                   PhD Students  
                                   Research  
                                   Contact Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	MGMT
                              	  
                          
                              	Faculty
                              	  
                         PETER WRIGHT 
                      
                   
                   
                       
                       
                     
                       
                        
                          
                           
                             
                              
                              
                               
                               
                              
                             
                           
                             
                              
                               
                                 
                                 PETER WRIGHT
                                 
                                
                              
                               
                                 
                                 Professor,  Department of Management
                                 
                                
                              
                                
                                 
                                   
                                    
                                     Phone  
                                    
                                     
                                       
                                       901,678,2434
                                       
                                      
                                    
                                   
                                 
                                   
                                    
                                     Email  
                                    
                                     
                                       
                                       pwright@memphis.edu
                                       
                                      
                                    
                                   
                                 
                                   
                                    
                                     Fax  
                                    
                                     
                                       
                                       901.678.2685
                                       
                                      
                                    
                                   
                                 
                                
                              
                                
                                 
                                   
                                    
                                     Office  
                                    
                                     
                                       
                                       BA 318
                                       
                                      
                                    
                                   
                                 
                                   
                                    
                                     Office Hours  
                                    
                                     
                                       
                                       Before and after class and by appointment
                                       
                                      
                                    
                                   
                                 
                                
                              
                                
                                 
                                 
                                 
                                   Website  
                                  
                                 
                                
                              
                             
                           
                          
                        
                        
                        
                         Biography 
                        
                         Peter Wright holds the University of Memphis Endowed Chair of Excellence in Free Enterprise
                           Management. He received his M.B.A. and Ph.D. from the Louisiana State University.
                           He served as the owner and managing director of an industrial component firm with
                           transactions across North America, Europe, Middle East, and Far East. He has consulted
                           with Union Carbide, Eastman Kodak, Alcoa, as well as privately held firms.
                         
                        
                         Professor Wright has published management and strategic management textbooks. His
                           publications appear in leading journals, such as  Strategic Management Journal, Academy of Management Journal, Academy of Management
                              Review, Academy of Management Executive, Academy of Management Perspectives, Journal
                              of Management, Strategic Organization, Harvard Business Review, Journal of Banking
                              and Finance, Journal of the Academy of Marketing Science , among others. His writings have been published by Harvard Business School Press,
                           Allyn and Bacon, Prentice Hall, Dame Thomson, Business Publications, Penguin, Hawthorn,
                           Elsevier Science, Irwin, University of Southern California Press, Pearson, and Sage,
                           among others. His academic publications have been reported by the  CBS Television Evening News, the Washington Post, the Wall Street Journal, Business
                              Week, the Economist, the Commercial Appeal , among others.
                         
                        
                         His current research encompasses corporate governance, agency theory, tournament theory,
                           valuation of firm investments, among others.  Professor Wright is a member of the
                           Academy of Management and the Strategic Management Society.
                         
                        
                         Research 
                        
                         Representative Publications 
                        
                         Krug, J. A., Wright, P.,   Kroll, M., "Top Management Turnover Following Mergers  
                           Acquisitions:  Solid Research To Date But Much To Be Learned,"  Academy of Management Perspectives , 28, 2014, pp. 147-163. 
                         
                        
                         Wright, P., Krug, J. A.   Ma, R., "Levels of Resources and Enterprise Outcomes:  The
                           Case of Smaller Firms Based Abroad,"  Journal of Business and Entrepreneurship , 24: 2, 2013, pp. 69-78. 
                         
                        
                         Nelson, T., Singh, K., Elenkov, D., Ma, R., Krug, J., Davis, B., and Wright, P., "Theoretical
                           Contributions to Governance of Samller Firms,"  Journal of Business and Entrepreneurship , 25, 2013, pp. 21-42. 
                         
                        
                         Elenkov, D., Wright, P., Pimentel-Kutz, J., Naoumova, L.,   Nabirukhina, M., "Subcultures
                           in a Country, Work Values and Preferred Workplace Rewards:  New Evidence from Russia,"  
                            Journal of Strategic and International Studies , 13, 2013, pp. 32-43. 
                         
                        
                         Wright, P., Kroll, M., Walters, B.   Ma, R., "Intersection of Upper Echelons and Corporate
                           Governance:  Ramifications for Market Response to Acquisition Announcements,"  Journal of Managerial Issues , 24: 4, 2013, pp. 433-449. 
                         
                        
                         Bryant, P., Fabian, F., Kinnamon, E.   Wright, P., "Tailoring Entrepreneurial Education: 
                           Exploring Components of Entrepreneurship Education for Underrepresented Groups,"  Journal of Business and Entrepreneurship , 23:  2, 2012, pp. 1-24.
                         
                        
                         Jackson, W.T., Elenkov, D., Davis, B.,   Wright, P.,  "Psychological Contracts in
                           Enterprises,"  Journal of Organizational Culture, Communications and Conflict , 16: 1, 2012, pp. 133-154. 
                         
                        
                         Walters, B., Kroll, M.,   Wright, P. "The Impact of TMT Board Members Control and
                           Environment on Post - IPO Performance,"  Academy of Management Journal , 53: 3, 2010, pp. 572-595. 
                         
                        
                         Wright, P., Kroll, M., Mukherji, A.,   Pettus, M. “Do the Contingencies of External
                           Monitoring, Ownership Incentives, or Free Cash Flow Explain Opposing Firm Performance
                           Expectations?”  Journal of Management and Governance , 2009.
                         
                        
                         Kroll, M., Walters, B.,   Wright, P. “Board Vigilance, Director Experience, and Corporate
                           Outcomes,”  Strategic Management Journal , 2008, pp. 363-382.
                         
                        
                         Walters, B., Kroll, M.,   Wright, P. “CEO Ownership and Effective Boards: Impacts
                           on Firm Outcomes,”  Strategic Organization , 2008, pp. 259-283.
                         
                        
                         Wright, P., Kroll, M., Krug, J.,   Pettus, M. Influences of Top Management Team Incentives
                           on Firm Risk Taking,  Strategic Management Journal , 2007, pp. 81-89.
                         
                        
                         Lado, A., Boyd, N., Wright, P.,   Kroll, M., “Paradox and Theorizing Within the Resource-Based
                           View,”  Academy of Management Review , 2006, 31, pp. 115-131.
                         
                        
                         Wright, P., Kroll, M., Lado, A.,   Elenkov, D. "Influences of Relative Rewards of
                           Top Managers of Firm Performance,"  Strategic Organization , 3, 2005, pp. 311-335.
                         
                        
                         Elenkov, D., Judge, W.,   Wright, P. "Strategic Leadership and Executive Innovation
                           Influence: An International Multi-Cluster Comparative Study,"  Strategic Management Journal , 26, 2005, pp. 665-682.
                         
                        
                         Wright, P., Ferris, S., Vaughn, M.,   Jackson, W. "Are Competitors Advantageous or
                           Disadvantageous in Consolidated Versus Fragmented Industries?,"  Academy of Strategic Management Journal , 3, 2004, pp. 47-64.
                         
                        
                         Chung, K., Wright, P.,   Kedia, B., “Corporate Governance and Market Valuation of
                           Capital and R   D Investments,”  Review of Financial Economics , 2003, pp. 161-172.
                         
                        
                         Desai, A., Wright, P.,   Chung, K., “Impact of Changes in Strategic Investments on
                           Shareholder Returns: The Role of Growth Opportunities,”  Journal of Applied Business Research , 2003, 19, pp.41-56.
                         
                        
                         Wright, P., Kroll, M.,   Elenkov, E. “Acquisition Returns, Increase in Firm Size and
                           Chief Executive Officer Compensation: The Moderating Role of Monitoring,”  Academy of Management Journal,  2002, pp. 599-609.
                         
                        
                         Wright, P., Kroll, M., Lado, A.,   Van Ness, B. “The Structure of Ownership and Corporate
                           Acquisition Strategies,”  Strategic Management Journal , 2002, 22, pp. 41-53.
                         
                        
                         Wright, P.,   Kroll, M. “Executive Discretion and Corporate Performance as Determinants
                           of CEO Compensation, Contingent on External Monitoring Activities,”  Journal of Management and Governance , 2002, pp. 189-214.
                         
                        
                         Wright, P., Ferris, S.,   Sarin, A., “An Agency Comparison of the Keiretsu and the
                           Japanese Independent Firm,”  Academy of Strategic Management Journal , 2002, 1, pp. 1-18.
                         
                        
                         Kroll, M., Toombs, L.,   Wright, P., “Napoleon’s Tragic March Home from Moscow: Lessons
                           in Hubris,”  Academy of Management Executive,  2000, 14, pp. 117-128.
                         
                        
                         Kroll, M., Wright, P.,   Heiens, R., “The Contribution of Product Quality to Competitive
                           Advantage: Impacts on Systematic Variance and Unexplained Variance in Returns,”  Strategic Management Journal , 1999, 20, pp. 375-384.
                         
                        
                         Chung, K.H., Wright, P., Charoenwong, C., “Investment Opportunities and Market Reaction
                           to Capital Expenditure Decisions,”  Journal of Banking and Finance , 1998, 22, pp. 41-60.
                         
                        
                         Wright, P., Kroll, M.,   Parnell, J.,  Strategic Management: Concepts and Cases , Prentice Hall (4th ed.), 1998.
                         
                        
                         Wright, P.   Ferris, S., “Agency Conflict and Corporate Strategy: The Effect of Divestment
                           on Corporate Value”,  Strategic Management Journal , 1997, 18, pp. 77-83.
                         
                        
                         Kroll, M., Wright, P., Toombs, L.,   Leavell, H., “Form of Control - - A Critical
                           Determinant of Acquisition Performance and CEO Rewards”,  Strategic Management Journal , 1997, 18, pp. 85-96.
                         
                        
                         Albanese, R., Franklin, J.,   Wright, P.,  Management , Dame/Thomson Learning, 1997.
                         
                        
                         Wright, P., Ferris, S., Sarin, A.,   Awasthi, V., “The Impact of Corporate Insider,
                           Blockholder, and Institutional Equity Ownership on Firm Risk Taking”,  Academy of Management Journal , 1996, 39, 2, pp. 441-463.
                         
                        
                         Wright, P., Ferris, S., Hiller, J., Kroll, M., “Competitiveness Through Management
                           of Diversity: Effects on Stock Price Valuation,”  Academy of Management Journal , 1995, 38, 1, pp. 272-287.
                         
                        
                         Teaching 
                        
                         Teaching areas include strategic management at the Ph.D. seminar, M.B.A., and undergraduate
                           levels. 
                         
                        
                         Professor Wright has chaired the dissertation committees of fifteen Ph.D. students
                           and served on the committees of six additional Ph.D. students. 
                         
                        
                         Service 
                        
                         Service to the University, the Fogelman College of Business   Economics, and to the
                           Department of Management
                         
                        
                         
                           
                            University President's Chairs of Excellence Council 
                           
                            Chair and committee member of University College Students 
                           
                            Member of University Council for Graduate Studies 
                           
                            Faculty Senate Representative 
                           
                            Chair, College Ph.D. Program Committee 
                           
                            Member of Teaching Excellence Fellowship Selection Committee 
                           
                            Member of College of Business Teaching Excellence Award Committee 
                           
                            Member of College of Business Tenure and Promotion Committee 
                           
                            Member of College of Business Roles and Rewards Committee 
                           
                            Chair, Department of Management Tenure and Promotion Committee 
                           
                            Chair, Search Committee for Recruiting Faculty in Strategic Management 
                           
                            Member of Management Graduate Programs Committee 
                           
                         
                        
                         Service to Management Discipline 
                        
                         
                           
                            Ad hoc reviewer, Strategic Management Journal 
                           
                            Ad hoc reviewer, Academy of Management Journal 
                           
                            Ad hoc reviewer, Academy of Management Review 
                           
                            Ad hoc reviewer, Administrative Science Quarterly 
                           
                            Ad hoc reviewer, Journal of Management. 
                           
                            Ad hoc reviewer, Journal of Business Research 
                           
                            Ad hoc reviewer, Journal of Management Studies 
                           
                            Ad hoc reviewer, Southern Management Association 
                           
                            Reviewer, National Academy of Management 
                           
                            Editorial Board, American Business Review. 
                           
                            Special Issues Editor, Business Perspectives 
                           
                            Associate Editor, Journal of Management in Practice 
                           
                            Editorial Board, Journal of Business Strategies 
                           
                            Member, Strategic Management Society 
                           
                            Member, Academy of Management 
                           
                            Outside reviewer, Tenure   Promotions decisions for various universities 
                           
                         
                         
                        
                       
                     
                      
                   
                
                
                   
                      
                         Faculty 
                         
                            
                               
                                Welcome  
                                Programs  
                                Courses  
                                Faculty  
                                PhD Students  
                                Research  
                                Contact Us  
                            
                         
                      
                      
                      
                         
                            
                                MGMT News   Events  
                               Learn about current events happening in our department! 
                            
                            
                                Research Colloquia  
                               Keep up with the latest Management academic research in our upcoming seminars.  
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/1/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


