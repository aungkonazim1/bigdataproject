Why the Humanities - MOCH - University of Memphis    










 
 
 
     



 
    
    
    Why the Humanities - 
      	MOCH
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Marcus W. Orr Center for the Humanities
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Events  
                          Fellowships  
                          External Funding  
                          Links  
                          Why the Humanities  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	MOCH
                              	  
                         
                           	Why the Humanities
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Why the humanities? 
                     
                      For those who are passionate about the beauty of language and literature, the profundity
                        of the human intellect, and the exploration of the intricacies of the human experience
                        throughout the ages, the most obvious answer to that question is, "Why not?" Those
                        of us who love the humanities believe that the primary purpose of a university education
                        is learn everything we can about what it means to be human and to challenge ourselves
                        to grow into better citizens of the world—to become better human beings.
                      
                     
                      Such an admittedly idealistic answer is, of course, often met with skepticism, especially
                        in an economy where a college education is increasingly necessary to career success.
                        The common assumption is that humanities degrees have outlived their usefulness and
                        have become irrelevant in the new millennium, and that choosing a major in English,
                        World Languages, History, or Philosophy is tantamount to choosing a life of perpetual
                        underemployment. All the good jobs are going to STEM majors, right?
                      
                     
                      Wrong. While it's true that STEM skills are, indeed, valuable in an economy based
                        upon the exchange of information and upon technology itself, those are not the only
                        skills that employers find valuable. In fact, employers in a wide variety of fields
                        are actively searching for candidates with other essential skills, such as:
                      
                     
                      
                        
                         Critical thinking and analytical skills 
                        
                         Strong verbal and written communication skills 
                        
                         Creativity and innovation, especially in regard to problem solving 
                        
                         Flexibility, especially the ability to approach an issue from a variety of perspectives
                           and the ability to learn and adapt quickly in company- or industry-specific training
                           programs, even without a highly technical educational background
                         
                        
                         Greater ease in working collaboratively, especially in working with others from a
                           variety of cultural backgrounds
                         
                        
                      
                     
                      Sound familiar? The skills humanities students develop and refine in class are the
                        EXACT skills that employers are searching for in young job applicants. While your
                        future employer may never ask you to recite a Shakespeare soliloquy from memory or
                        discuss "The Allegory of the Cave" in a project meeting, your passion for the humanities
                        can be personally and intellectually fulfilling AND professionally rewarding.
                      
                     
                      Want to learn more? Click on the links below to see what the professionals are saying. 
                     
                      
                        
                         Tuajuanda C. Jordan, " Thinking Outside the Box ,"  US News and World Report , January 12, 2015.
                         
                        
                         George Anders, " That 'Useless' Liberal Arts Degree has become Tech's Hottest Ticket ,"  Forbes , August 17, 2015.
                         
                        
                         Yoni Appelbaum, " Why America's Business Majors Are in Desperate Need of a Liberal-Arts Education ,"  The Atlantic , June 28, 2016.
                         
                        
                         Abby Jackson, " Robots Threaten Jobs from Truck Driver to Wealth Manager - and It Changes How Graduates
                              Should Approach the Working World ," Business Insider, February 16, 2017.
                         
                        
                         Peter Salovey, " Why we need the humanities more than ever, by the President of Yale ," World Economic Forum, March 23, 2017.
                         
                        
                         George Anders, " The Unexpected Value of the Liberal Arts ," The Atlantic, August 1, 2017.
                         
                        
                         Jeffrey J. Selling, " Six Myths About Choosing a College Major ,"  New York Times,  November 3, 2017.
                         
                        
                      
                      
                     
                      Ready to join us? Check out our wonderful humanities programs: 
                     
                      
                        
                          English  
                        
                          History  
                        
                          Philosophy  
                        
                          World Languages and Literatures  
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Donate to MOCH  
                               Support the programs and works of the Marcus W. Orr Center for the Humanities. 
                            
                            
                                Interdisciplinary Studies  
                               Serving as the home for academic programs which transcend the boundaries of the traditional
                                 academic departments.
                               
                            
                            
                                Contact MOCH  
                               Main contact information and location 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


