Editing files in folders in Wiggio | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Editing files in folders in Wiggio | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   English  
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Wiggio    »    Creating and using folders in Wiggio   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Editing files in folders in Wiggio                       
        
        
       
        
     
               
		In the Folders tab of the group you want to edit folder files for, select the check box for the file you want to edit.
	 
	 
		Select one of the following options:
		  
				  Download 
			 
			 
				   Share 
			 
			 
				  More , then   Move ,   Rename , or   Delete 
			 
		  
	 
		Follow the prompts as they appear on the screen.
	 
      Audience:     Learner       
    
         
               ‹ Sorting files in Wiggio 
                     up 
                     Creating and using groups in Wiggio › 
           
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Wiggio  
  
      Wiggio basics    Adding resources to Wiggio    Using polls in Wiggio    Using to-do lists in Wiggio    Creating and using folders in Wiggio    Creating folders in Wiggio    Editing folders in Wiggio    Uploading files into folders in Wiggio    Sorting files in Wiggio    Editing files in folders in Wiggio      Creating and using groups in Wiggio    Managing groups in Wiggio     Managing events in Wiggio    Importing and exporting calendars in Wiggio    
                  
           
         

       
     

    
    
    
   
 
   
 
