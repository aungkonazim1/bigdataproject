Notifying students about updated course content | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Notifying students about updated course content | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Content    »    Managing and updating course content   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Notifying students about updated course content                       
        
        
       
        
     
              
	You can notify students when updating or changing a topic in Content. They receive an  Update alert  notification in their minibar; clicking the alert brings them to the updated topic.
 

 
	Notify students about updated course content
 

  
		Locate the topic on the Table of Contents page, or click on its module from the Table of Contents panel.
	 
	 
		Click on the topic you want to update.
	 
	 
		Click    Edit HTML  or     Change File  from the topic's context menu.
	 
	 
		Make your changes, then select the  Notify students that the content has changed  check box.
	 
	 
		You can  Add a summary of the changes  to indicate to students what has changed since the update.
	 
   Select the  Reset completion tracking  check box if you want to ensure students revisit the topic to complete the activity. 
	 
		Click  Update .
	 
      Audience:     Instructor       

    
           

                   ‹ Changing a module or topic status to Draft or Published in Content 
        
                   up 
        
                   Editing a module or topic s title in Content › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Content  
  
      Content basics    Creating course content    Managing and updating course content    Content display settings    Changing a module or topic status to Draft or Published in Content    Notifying students about updated course content    Editing a module or topic s title in Content    Bulk editing modules and topics in Content    Moving or reordering a module or topic in Content    Deleting modules or topics in Content    Adding or editing availability and due dates in Content    Adding or editing release conditions in Content    Adding a Learning Object to a module    Associating topics with learning objectives in Content    Adding an existing course object (activity) to Content      Using SCORM in Content    Tracking content completion and participation    
                  
           
         

       
     

    
    
    
   
 
   
 
