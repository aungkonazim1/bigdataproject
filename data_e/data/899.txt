Guide to Applying - School of Law - University of Memphis  Apply to Law School Admissions  










 
 
 
     



 
    
    
    Guide to Applying - 
      	School of Law 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Cecil C. Humphreys School of Law
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Admissions  
                          Programs  
                          Current Students  
                          Faculty  
                          Careers  
                          Library  
                      
                      
                         
                            Admissions   
                            
                               
                                  
                                   
                                   Guide to Applying  
                                        
                                         Comprehensive Application Guide  
                                        
                                         LSAT Prep  
                                        
                                         Transfer Information  
                                        
                                         TN Inst. for PreLaw (TIP)  
                                        
                                         Part-Time Program  
                                        
                                         Schedule a Visit  
                                        
                                         Academic Curriculum  
                                        
                                         Apply Now  
                                        
                                         Check Application Status  
                                     
                                  
                                   Stats, Facts and Profile  
                                         Profile and Stats Overview  
                                         Class Profile Factsheet  
                                         2017 Viewbook  
                                         Request a Viewbook  
                                         Bragging Points  
                                     
                                  
                                   Tuition, Scholarships, Financial Aid  
                                         Overview of Costs  
                                         Tuition Information  
                                         Financial Aid  
                                         Scholarships  
                                         Tuition Payment  
                                     
                                  
                                   Academic Curriculum  
                                   Experiential Learning  
                                   Life in Memphis  
                                         Memphis-The City  
                                         Neighborhoods  
                                         Housing  
                                         The Arts Scene  
                                         Sports  
                                         The Food Scene  
                                         A Music Town  
                                         Directions and Maps  
                                     
                                  
                                   Recruiting Calendar  
                                   Schedule a Visit  
                                   Frequently Asked Questions (FAQ)  
                                         FAQ List  
                                         Application Information  
                                         LSAT  
                                         Notification and Acceptance  
                                         Curriculum  
                                         City of Memphis  
                                     
                                  
                                   Admitted Student Information  
                                   Law Bookstore  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	School of Law 
                              	  
                          
                              	Admissions
                              	  
                         Guide to Applying 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                        
                     
                      GUIDE TO APPLYING 
                     
                      The University of Memphis School of Law welcomes applications for admission to its
                        2017 entering class. The application is currently open, and there is no application
                        fee. The priority application deadline is March 15th.  Apply here.   Although the March 15th priority deadline has passed, we are still accepting applications
                        at this time.
                      
                     
                      A significant factor in the admission decision process is the admission index, which
                        is based on the undergraduate grade point average and the LSAT score. The undergraduate
                        grade point average used in the admission index is the cumulative grade point average
                        found on the Credential Assembly Service (CAS) report produced by LSAC. Also considered
                        are factors deemed to be predictive of success in law school as set forth in the application,
                        personal statement, letters of recommendation, and the applicant's CAS report. Such
                        factors may include, but are not limited to overall academic record, co-curricular
                        activities, community involvement, employment, and other life experiences.
                      
                     
                      Applicants can access our application at  LSAC  (Law School Admission Council). Applicants will be sent an email indicating their
                        applications have been received. Applicants will also receive instructions on how
                        view the status of their applications. An email will be sent notifying applicants
                        their files are complete.
                      
                     
                      It is the applicant's responsibility to take the necessary steps to ensure that all
                        supporting documents required to complete the application are submitted on or before
                        the March 15th priority deadline. Failure to complete the application by the deadline
                        may reduce the applicant's chance for admission. Applicants are encouraged to begin
                        the application process and submit all the required documents as early as possible.
                         February and June LSAT scores will be reviewed for fall admission.  
                     
                      The application components are: 
                     
                       □  Application (Required)  
                     
                      The application may be accessed through LSAC. Be sure to complete all questions accurately.
                        Include your name and LSAC account number on any addenda. The priority application
                        deadline is March 15th.
                      
                     
                       □  Credential Assembly Service (CAS) (Required)  
                     
                      Applicants for the 2017 entering class must have an LSAT score that is current and
                        taken between June 2012 and February 2017. LSAC will not release a CAS report until
                        they receive an original transcript from every undergraduate institution you have
                        attended, including summer sessions and study abroad programs. It is the applicant's
                        responsibility to monitor the status of his/her LSAC account. Once your application
                        is complete, the admissions office will send a completion status e-mail. Applicants
                        can also monitor their Memphis application status on-line through LSAC.
                      
                     
                       □  Personal Statement (Required)  
                     
                      Each applicant is required to submit a personal statement. This statement provides
                        you the opportunity to describe your background and any unique experiences, characteristics
                        or circumstances you want the admissions committee to consider. You should explain
                        your desire to study law, why you believe you will be a successful law student, and
                        what you plan to do with your law degree. You are encouraged to explain your interest
                        in attending our law school and may discuss any information not otherwise apparent
                        from your application, including family members who are graduates of the University
                        of Memphis School of Law. Limit your personal statement to no more than 1,000 words.
                        NOTE: If you want to include relevant information explaining a low undergraduate grade
                        point average or low LSAT score, we encourage you to submit an addendum separate from
                        your personal statement.
                      
                     
                       □  Admonitory Action Explanation (Required if Applicable)  
                     
                      Applicants must answer questions in the Admonitory Action section of the application.
                        If an applicant answers "yes" to any of the questions, he/she should provide an addendum
                        detailing the date and location of the event and an explanation of what occurred.
                        Applicants are encouraged to include related court documents.
                      
                     
                       □  Letters of Recommendation/Evaluations (optional)  
                     
                      Applicants are encouraged to submit up to three letters of recommendation addressing
                        their potential for academic success. When possible, letters should come from professors
                        if you are currently in college or have recently graduated. The letters should be
                        sent to LSAC to be included with your CAS report. If a file is complete by March 15th,
                        file review will not be delayed if letters have not been received. For more information
                        visit the LSAC Letters of Recommendation webpage.
                      
                     
                       □ TIP Statement (Required for all TIP applicants)  
                     
                      Applicants interested in being considered for the Tennessee Institute for Pre-Law
                        Program are required to submit a TIP statement. The statement should address how they
                        have contributed to the overall diversity and/or how they might have been economically
                        disadvantaged. The TIP Program is an alternative admission summer program for Tennessee
                        and border county residents from diverse backgrounds who are not admitted through
                        the regular admissions process, but who show potential for the study of law.
                      
                     
                       □  Financial Aid and Scholarships  
                     
                      Applicants are encouraged to complete the application for Federal Student Aid (FAFSA)
                        by March 15th. Memphis Law's code is 003509. Most entering scholarships are awarded
                        based on the merits of the applicant's credentials. Some scholarships have special
                        requirements. For those scholarships, please review the scholarship list for first-year
                        students.
                      
                     
                       □  Decision Timeline (January – April)  
                     
                      Our online application is available through LSAC. Applicants applying to the University
                        of Memphis School of Law will receive an email with a link to our website and a code
                        to establish an account to check the status of their application. Hence forth, the
                        applicant can check their application status on-line. Applicants will also receive
                        an email once the application is complete. Once a decision is rendered, the online
                        status will read: decision rendered. Final decisions are mailed to the applicants' current
                        address. Most decisions are made between January and April.
                      
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Admissions 
                         
                            
                               
                                
                                Guide to Applying  
                                     
                                      Comprehensive Application Guide  
                                     
                                      LSAT Prep  
                                     
                                      Transfer Information  
                                     
                                      TN Inst. for PreLaw (TIP)  
                                     
                                      Part-Time Program  
                                     
                                      Schedule a Visit  
                                     
                                      Academic Curriculum  
                                     
                                      Apply Now  
                                     
                                      Check Application Status  
                                  
                               
                                Stats, Facts and Profile  
                                      Profile and Stats Overview  
                                      Class Profile Factsheet  
                                      2017 Viewbook  
                                      Request a Viewbook  
                                      Bragging Points  
                                  
                               
                                Tuition, Scholarships, Financial Aid  
                                      Overview of Costs  
                                      Tuition Information  
                                      Financial Aid  
                                      Scholarships  
                                      Tuition Payment  
                                  
                               
                                Academic Curriculum  
                                Experiential Learning  
                                Life in Memphis  
                                      Memphis-The City  
                                      Neighborhoods  
                                      Housing  
                                      The Arts Scene  
                                      Sports  
                                      The Food Scene  
                                      A Music Town  
                                      Directions and Maps  
                                  
                               
                                Recruiting Calendar  
                                Schedule a Visit  
                                Frequently Asked Questions (FAQ)  
                                      FAQ List  
                                      Application Information  
                                      LSAT  
                                      Notification and Acceptance  
                                      Curriculum  
                                      City of Memphis  
                                  
                               
                                Admitted Student Information  
                                Law Bookstore  
                            
                         
                      
                      
                      
                         
                            
                                Apply to Memphis Law  
                               Take the first step toward your legal education 
                            
                            
                                ABA Required Disclosures  
                               Information required by ABA Standard 509 Required Disclosures 
                            
                            
                                Alumni   Support  
                               Stay up to date with Memphis Law alumni 
                            
                            
                                Contact Us  
                               Questions about law school? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


