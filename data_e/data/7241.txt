Personal Dashboard homepage overview | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Personal Dashboard homepage overview | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   English  
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Homepages and Widgets    »    Personal Dashboard homepage and Course Setup Checklist basics   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Personal Dashboard homepage overview                       
        
        
       
          
     
           Printer-friendly version       
	  

 
	1. The   Favorite Course   tiles provide easy access to your current courses. You can edit the course image, access the Course Setup Checklist, and view alerts if the course is not visible to learners.
 

 
	2. The   All Your Courses   area lists all the courses you are enrolled in. Courses are sorted by semester, which are grouped alphabetically, and then by course name in alphabetical order. Use the  Load More  feature to view more. You can also search the list by semester, course code or course name.
 

 
	3. Select the   Favorite   check box from the   All Your Courses   area to enable the course tiles.
 

 
	4.   Classic Homepage   lets you view your organization's homepage.
 

 
	5. Use     Settings   to set your Default Homepage. You can keep the Personal Dashboard homepage as your landing page, or you can select your organization's homepage. You can also reset the Welcome Message tour of the Personal Dashboard homepage in  Settings .
 

 
	6. View and edit your   User Profile   from the Personal Dashboard homepage.
 
     Audience:    Instructor      
    
         
               ‹ Understanding the Personal Dashboard homepage and Course Setup Checklist 
                     up 
                     Course Setup Checklist overview › 
           
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Homepages and Widgets  
  
      Homepages and Widgets basics    Using system widgets    Using custom widgets    Editing widget display and placement    Personal Dashboard homepage and Course Setup Checklist basics    Understanding the Personal Dashboard homepage and Course Setup Checklist    Personal Dashboard homepage overview    Course Setup Checklist overview      How do I use the Personal Dashboard homepage?    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
