Adding artifacts using ePortfolio Mobile Application | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Adding artifacts using ePortfolio Mobile Application | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   English  
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    ePortfolio Mobile Application   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Adding artifacts using ePortfolio Mobile Application                       
        
        
       
        
     
              
	You can add four types of artifacts using ePortfolio Mobile Application: images, audio recordings, reflections, and links.
 

 
	Adding and tagging content to your ePortfolio requires the appropriate ePortfolio and tag management role permissions.
 


 
	 Note  Adding and tagging content to your ePortfolio requires the appropriate ePortfolio and tag management role permissions.
 

 
	Add an image using the camera on your mobile device
 

  
		Tap  Image  on the My Items screen.
	 
	 
		Tap  Take Photo  and capture a photo using your mobile device.
	 
	 
		Tap  Use Photo .
	 
	 
		Enter the following fields:
		  
				Title
			 
			 
				Description (optional)
			 
			 
				Comma separated tags (optional)
			 
		  
	 
		Tap  Save 
	 
  
	Add an existing image on your mobile device
 

  
		Tap  Image  on the My Items screen.
	 
	 
		Tap  Choose Photo  and select the image you want to add to your ePortfolio.
	 
	 
		Enter the following fields:
		  
				Title
			 
			 
				Description (optional)
			 
			 
				Comma separated tags (optional)
			 
		  
	 
		Tap  Save 
	 
  
	Add an audio file recorded on your mobile device
 

  
		Tap  Record  on the My Items screen.
	 
	 
		Tap  Record  to start and stop recording your audio file.
	 
	 
		Tap  Publish Artifact .
	 
	 
		Enter the following fields:
		  
				Title
			 
			 
				Description (optional)
			 
			 
				Comma separated tags (optional)
			 
		  
	 
		Tap  Save 
	 
  
	Add a reflection
 

  
		Tap  Reflection  on the My Items screen.
	 
	 
		Enter the following fields:
		  
				Title
			 
			 
				Reflection
			 
			 
				Comma separated tags
			 
		  
	 
		Tap  Save 
	 
  
	Add a linked web address
 

  
		Tap  Link  on the My Items screen.
	 
	 
		Enter the following fields:
		  
				Title
			 
			 
				Link
			 
			 
				Description (optional)
			 
			 
				Comma separated tags (optional)
			 
		  
	 
		Tap  Save 
	 
      Audience:     Learner       
    
         
               ‹ Accessing ePortfolio Mobile Application 
                     up 
                     Sharing items using ePorfolio Mobile Application › 
           
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        ePortfolio Mobile Application  
  
      Accessing ePortfolio Mobile Application    Adding artifacts using ePortfolio Mobile Application    Sharing items using ePorfolio Mobile Application    Logging out of ePortfolio Mobile Application    
                  
           
         

       
     

    
    
    
   
 
   
 
