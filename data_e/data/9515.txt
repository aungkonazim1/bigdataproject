Networking Research Lab &gt; Projects &gt; CRI: Building the Next-Generation Global Routing Monitoring Systems    

 
   Networking Research Lab   Projects   CRI: Building the Next-Generation Global Routing Monitoring Systems  
  
	 
	 
    
    
 

 

 

 
 
   
 
  
        
  
    
 

 
 
   
 
   
 

   
    
       
       
       
        
 

 
 
   
   
 

 
   
      
        
    
       
         
         
          
       
    
    
       
         
         
         
          
       
    
    
       
         
         
          
       
    
 
 
 
       
         
          NetViews: Real-Time Visualization of Internet Topology Through Peered Vantage Points  
         
         
          
       The  Internet is made up of tens of thousands interconnected self managing  networks.  The  Boarder Gateway Protocol  uses a distributed trust  system so that information can be passed between networks. The Netviews program will help network operators diagnose  possible threats and aid in traffic engineering techniques through real time  visualization of multi-level topologies. Researchers can use knowledge of  network topology in future protocol design and monitoring the growth of the  Internet.  NetViews is an incredibly  intuitive program that is easy to use and has a shallow learning curve.. In collaboration with  Colorado State University's BGPMon  system and using a new visual interpretation of BGP data operators can quickly understand and analyze real time connectivity across the globe. 
       
         
         
         
         
 
       Architecture 
       Our architecture is a  combination of client server and load distribution technology.  The Data Broker acts as a client to the  BGPMon system receiving updates and forwarding the information to clients. In  order to maintain scalability to thousands of clients, NetViews makes use of  multicasting through  overlay networking .   The DataBroker acts as a portal between BGPMon and client users.  A diagram of this interaction can be seen below.  Load is distributed across the  overlay network in order to provide updates and metrics in real time. For the  overlay network implementation we use the  Hypercast API  provided by the University  of Toronto.  Only BGP updates are injected into the  overlay network.  This is to provide as  little delay as possible.  Other services  that the DataBroker provides use direct TCP connection to clients. 
         
        Figure  1. Relationship between BGPMon and NetViews   
         
         
        Figure  2. Initial user interaction screen showing BGP updates, RIB table, and other status information.   
       
         
         
         
         
 
        Client  Visualization Model  
       The NetViews client is the main  interaction point for most users.  It  features an intuitive interface and many features.  Written entirely in Java it enables BGP table  and update capture in real time and visualizes this data on demand.  The new client visualization model increases  the efficiency and level of understanding when analyzing BGP data. Currently  our model features several ways to visualize data. By changing what type of  data is being visualized we can assign weights to each links based on the level  of prefixes being advertised through each link or the amount of update activity  each link has experienced.   These  weights can be expressed visually by thicker lines or different color  values.  Nodes can also have visual  weights associated with them based on the number of prefixes originated by that  network or the link degree of that network.  
       The visualization module visualizes  BGP tables and updates and provides a dynamic and interactive topology map.  We noticed massive cluttering when  subscribing to all prefixes so we decided to hide links and nodes for the most  part.  The user can then expand and collapse  nodes as he/she sees fit. Just as important as visualizing BGP updates was also  organizing these updates. The side panel organizes the textual portion of data.  It gives the user a wealth of information to complement the topology map.  In Figure 5 we can see the main screen of the  topology map.  There are two peers 293  and 2914 as shown by triangles.  We have  expanded out the peers to show their immediate neighbors.  On the left is information about nodes,  links, and the peers.  We can obtain  WHOIS information in this panel also.  On  the right side we have the map controls.   We can go forward or backward in time and can set the map to run in Live  mode which will set it to process updates in real time.  Also in this panel we display information  regarding each update.  This includes the  update s source, its timestamp, type, and associated prefixes and path.   
       The graphical user interface  (GUI) makes using the NetViews program intuitive and easy to pick up by the  most inexperienced of users.  A wizard  style step by step dialog window takes the guess work out of the subscription  process and ensures that all information is gathered from the user.  The user firsts enters in the prefixes that  he/she wishes to receive BGP update information for. We have also enabled the  subscription of entire AS networks so that network operators do not have to  explicitly subscribe to every prefix in the AS they are interested in.  This is also done in the first screen. Then  the user can select a subset of peers to use as vantage points and choose  whether to also download BGP tables from those peers.  Finally, a summary information screen details  the user s choices before finalizing. 
         
         
             
        Figure  3. World view with several highlighted links.  Red link is a path  specific highlight. Green Blue gradient is a prefix specific  highlight.  Gradient flows towards the prefix.  
         
        Figure  4. View of America with Side Panels.   
         
       IP Forwarding Visualization 
       Just recently we began the  initial phase of implementing an IP forwarding path visualization module to  complement our AS-level map.  This  information can be used by network operators to verify that forwarding  information corresponds to BGP routing information.  This can be helpful in traffic  engineering.  Researchers can also use  this information to study convergence time of the IP forwarding plane when a  change occurs on the BGP control plane.   In order to accomplish this we leverage looking glass servers (LG)  located at our peers and produce IP forwarding paths that should correspond to  BGP paths.  Traceroute probes are sent  periodically to the LG servers.  However,  if a change occurs in BGP along the same path it might be an indication that a  change has occurred on the router level IP forwarding plane.  We utilize this knowledge and use BGP as a  trigger to probe an address again to find new router level information. A  screenshot of the visualization prototype can be seen in Figure 5.   Here 213. 200.64.93 is a LG server located at  peer 3303.  We probed 87.237.39.196  several times and produced several paths.   Some paths are incomplete due to unresponsive routers or parsing errors.  This presents a first step towards real time  dual AS Router level topology maps. 
         
         
        Figure  5. IP Forwarding Traceroutes to  87.237.39.196 
       NetViews client is currently in Beta development and will be available for download soon!  
         
        NetViews PowerPoint of additional features(Since Oct 08)   
                 
     This material is based upon work supported by  the National Science Foundation under Grant No. 0551541.  Any opinions, findings, and conclusions or  recommendations expressed in this material are those of the author(s) and do  not necessarily reflect the views of the National Science Foundation.  
 
 
           
 
 
     
 


 
 
 
 
 home     people     projects     papers  

 
  2006 University of Memphis  
 
 

 

 
