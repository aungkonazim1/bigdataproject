Creating ordering questions | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Creating ordering questions | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Question Library    »    Question types   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Creating ordering questions                       
        
        
       
          
     
           Printer-friendly version       
	Ordering (ORD) questions require respondents to arrange a series of items into a correct sequence or order.
 

 
	Create an ordering question
 

  
		Enter a  Title . Enter a  Points  value. Select a difficulty level in the  Difficulty  drop-down list.
	 
	 
		Enter your ORD question in the  Question Text  field.
	 
	 
		Click  Add a File  to upload an image to accompany your question. You can enter a description of the image in the  Description  field.
	 
	 
		Choose a  Grading  format:
		  
				 Equally weighted  The total points value is divided equally among all possible correct order items. Users receive equally weighted points for each correct answer.
			 
			 
				 All or nothing  Users receive the total points value for correctly ordering every answer or else they receive none at all.
			 
			 
				 Right minus wrong  Users receive points equal to the number of right answers they choose minus the number of incorrect answers they choose. To determine how much each answer is worth, the system takes the total number of points assigned to the question and divides it by the total number of answer choices.
				 
					 Example  If a question is worth 10 points and has 5 answer choices, each correct answer is worth 2 points, and each incorrect answer is worth 2 points (10/5 = 2). If a user gives 3 correct answers and 2 incorrect answers, 2 is the total number of points received for the question [(3-2)*2 = 2].
				 

				 
					 Note  Users can receive a minimum of zero on a question; they cannot receive a negative mark.
				 
			 
		  
	 
		Enter an order item in each  Value  field. You can click    Add Item  to create additional ordering items. To delete an order item, click the corresponding    Remove Entry  icon.
	 
	 
		Set the correct ordering in the  Correct Order  drop-down list. The first item in the correct order should be "1".
	 
	 
		Provide comments and suggestions in the  Feedback ,  Question Hint , and  Question Feedback  fields.
	 
	 
		Click  Preview  to view your question.
	 
	 
		Click  Save  to return to the main page, click  Save and Copy  to save and create another ORD question that retains the copied properties, or click  Save and New  to continue creating new ORD questions.
	 
      Audience:    Instructor      

    
           

                   ‹ Creating matching questions 
        
                   up 
        
                   Creating arithmetic questions › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Question Library  
  
      Question Library basics    Question types    Understanding regular expressions    Creating true or false questions    Creating multiple choice questions    Creating multi-select questions    Creating long answer questions    Creating short answer questions    Creating multi-short answer questions    Creating fill in the blanks questions    Creating matching questions    Creating ordering questions    Creating arithmetic questions    Creating significant figures questions    Creating Likert questions    Creating text information    Creating image information      
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
