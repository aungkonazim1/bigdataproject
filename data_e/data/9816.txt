Setting up course tools | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Setting up course tools | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Course Administration    »    Course Administration basics   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Setting up course tools                       
        
        
       
          
     
           Printer-friendly version       
	Course tools, such as Quizzes, Content, Discussions, Chat, Grades, etc., are course components that you add to your course offering to share learning content, foster interaction, and evaluate performance. You can turn course tools on and off so that the tools available in your course are specifically tailored to your pedagogy and course materials. You can also rename tools.
 

 
	Access the Tools page
 

 
	Do one of the following:
 

  
		Click    Edit Course  on the navbar, then click    Tools .
	 
	 
		Click  Tools  in the Course Administration widget on your course homepage.
	 
  
	Activating and deactivating tools
 

 
	Before disabling a tool, be aware of the following:
 

  
		Release Conditions based on the tool are not enforced as long as the tool remains disabled.
	 
	 
		Competency activities associated with the tool are hidden, but are still associated with any learning objectives to which they are attached. Users cannot complete these learning objectives while the tool is disabled. You should detach or delete any activities associated with a tool before disabling the tool.
	 
	 
		Grade items associated with the tool remain, but you must manually update them in the grade book.
	 
	 
		Quicklinks to the tool’s items persist but a “No Resource Found” message displays when they are followed. You should remove any related Quicklinks when you deactivate a tool. You may not be able to delete others' Quicklinks.
	 
  
	Activate or deactivate a tool
 

 
	On the Tool Status page, activate a tool by toggling its Status to     On . Toggle its Status to    Off  to deactivate the tool.
 

 
	 Notes 

	  
			Some activated tools can only be accessed from the navbar or homepage widgets.
		 
		 
			Deactivating a tool does not delete any items or user data inside the tool; it simply hides the tool from your course offering. If you turn the tool back on, it restores the users' ability to access the tool.
		 
	  
     Audience:    Instructor      

    
           

                   ‹ Editing course colors 
        
                   up 
        
                   Renaming a tool in a course offering › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Course Administration  
  
      Course Administration basics    Accessing Course Administration    Accessing the Course Offering Information page    Editing course colors    Setting up course tools    Renaming a tool in a course offering    Editing the course locale      Managing course components    Understanding IMS Common Cartridge    Understanding Mobile Brand Administration    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
