Using learning objectives in ePortfolio | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Using learning objectives in ePortfolio | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    ePortfolio    »    Adding artifacts   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Using learning objectives in ePortfolio                       
        
        
       
        
     
              
	Learning objectives are a type of ePortfolio item that enable you to take ownership of your learning by allowing you to manage and track personal learning goals.
 

 
	You can:
 

  
		Associate learning objectives with artifacts, reflections, collections, and presentations that demonstrate progress toward your learning plan. If you submit a learning objective to a dropbox folder, its associated items go with it.
	 
	 
		Share learning objectives with rubrics attached to them to generate feedback and informal assessment from other ePortfolio users. (Items associated with a learning objective are not available to others unless you specifically share them.)
	 
	 
		Display learning objectives in presentations and add them to collections.
	 
	 
		Submit a learning objective to a dropbox folder for formal assessment. Associated items serve as evidence toward meeting the learning objective.
	 
	 
		Use learning objectives as an organizational tool by categorizing your ePortfolio items by learning experience.
	 
	 
		Delete learning objectives that are no longer relevant to your learning path.
	 
  
	Import learning objectives into your ePortfolio
 

 
	If you are in an independent studies or co-op program, you might have the ability to directly import learning objectives into your ePortfolio.
 

  
		On the My Items page, click  ​  Learning Objective  from the Add button.
	 
	 
		Click on the course you want to import learning objectives from.
	 
	 
		Select the check boxes beside the learning objectives you want to add.
	 
	 
		Click  Import .
	 
  
	Associate items with learning objectives
 

 
	Do one of the following:
 

  
		Click  ​  Associate Learning Objective  from the context menu of an artifact, reflection, collection, or presentation.
	 
	 
		On the Edit page for an item, click  Associate Learning Objectives .
	 
	 
		On the My Items page, select the items you want to associate with a learning objective, then click    Associate Learning Objective  from the More Actions button.
	 
      Audience:     Learner       

    
           

                   ‹ Importing course content 
        
                   up 
        
                   Using reflections › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        ePortfolio  
  
      Understanding the main pages of ePortfolio    Adding artifacts    Adding linked web addresses    Uploading files    Creating web documents    Adding audio recordings    Adding form responses    Importing course content    Using learning objectives in ePortfolio      Using reflections    Creating presentations    Creating collections    Understanding assessment types in ePortfolio    Understanding the basic concepts in sharing    Importing and exporting items    Sharing items within courses    Using form templates    Integrating ePortfolio with Content     Assessing ePortfolio content    
                  
           
         

       
     

    
    
    
   
 
   
 
