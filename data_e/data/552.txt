 First Lady - Office of the President - University of Memphis    










 
 
 
     



 
    
    
     First Lady - 
      	Office of the President
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Office of the President
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Mission and Plan  
                          Communications  
                          Leadership  
                          Contact  
                      
                      
                         
                            Learn More   
                            
                               
                                  
                                   First Lady Homepage  
                                   Dr. Loretta Rudd's Bio  
                                   Events  
                                   Women in Leadership   Philanthropy  
                                   National Coalition for Campus Children’s Centers  
                                   Urban Child Institute  
                                   Contact Dr. Loretta Rudd  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Office of the President
                              	  
                         
                           	First Lady Dr. Loretta Rudd
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      First Lady Dr. Loretta Rudd 
                     
                        
                     
                      About Dr. Loretta Rudd 
                     
                      I am honored to serve as the University of Memphis's first lady. Since coming to Memphis
                        in 2013, I've been inspired by the students and alumni, faculty and staff, donors
                        and community leaders who share a commitment to the success of our university, our
                        city and region.  Great things are happening at the University of Memphis. I'm grateful
                        to be able to contribute in a positive way to our progress.
                      
                     
                      Through my involvement with Women in Leadership and Philanthropy, I look forward to
                        creating stronger connections between the university and our alumnae and friends. 
                        The WLP gives women the opportunity to engage in the life of the university, to support
                        our students, and to give back in meaningful ways.
                      
                     
                      My deepest commitment is to early care and learning. Through the WLP and other partnerships
                        across campus and the city, I am confident we can improve child outcomes. Our community
                        is ripe for interventions that can make a difference in our children's future, which
                        will in turn make a difference in Memphis, across the state, and ultimately our nation.
                        By investing in children ages 0-3, we are preparing them for success in school that
                        will lead them to higher education.
                      
                     
                      Thank you for taking the time to visit this website. Please check periodically for
                        links to news and events on campus and in the greater community that are making our
                        city and region better.
                      
                     
                         Read more about Dr. Loretta Rudd    
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Learn More 
                         
                            
                               
                                First Lady Homepage  
                                Dr. Loretta Rudd's Bio  
                                Events  
                                Women in Leadership   Philanthropy  
                                National Coalition for Campus Children’s Centers  
                                Urban Child Institute  
                                Contact Dr. Loretta Rudd  
                            
                         
                      
                      
                      
                         
                            
                                Dashboard  
                               UofM steps towards success 
                            
                            
                                FOCUS Act  
                               Independent Governing Board Updates 
                            
                            
                                President's Blog  
                               News, stories, videos and more 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


