Creating automated live events | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Creating automated live events | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Capture    »    Capture Station   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Creating automated live events                       
        
        
       
        
     
              
	 Important Automation times are relative to the server time zone. If you need to change the default publishing time and do not have Capture automation configuration training, contact Desire2Learn Capture Support for more information.
 

 
	Create an automated live event
 

  
		Log in to the Capture Portal and click  Admin .
	 
	 
		On the Manage Live Events page, click  Create Live Event .
	 
	 
		Enter a  Title  for your live event.
	 
	 
		Enter the name of the  Presenters  and a  Description .
	 
	 
		You can add  Tags  to the live event. Tags are keywords or terms that describe the presentation and allow it to be found through Search. Press  Enter  or " , " on your keyboard after you enter each tag to ensure it is added.
	 
	 
		Enter the  Start/End Times  in the date and time fields. If you want to create an event that spans across multiple days, you can select the  Show end date  check box to modify your event end date.
	 
	 
		In the Layout Settings section, select a presentation layout.
	 
	 
		In the Access Control section, you can do the following:
		  
				If you want to create a password for an event, select the  Event Password Required  check box and enter a password in the text field.
			 
			 
				 
					 Note Password protected live events require the same password to access the presentation after it is published. You can change published presentations' passwords in the Manage Presentations area.
				 
			 
			 
				If you do not want users to access a live event's chat room, select the  Chat disabled  check box.
			 
			 
				Select the  Viewer Limit  check box and enter the maximum number of viewers in the text field.
			 
			 
				 
					 Note If the viewer limit is above 750, the live event's waiting area is disabled. If the viewer limit is above 1250, chat is disabled.
				 
			 
		  
	 
		In the Permissions area, use the search field to  Search for a User, Group, or Role  you want to add to the permissions list. See  Understanding access controls  for more information.
	 
	 
		In the Pre-roll/Post-roll section, select a video clip from the  Pre-roll clip  and  Post-roll clip  drop-down lists to add video before and after your presentation. You can select the  Users can skip pre-roll and post-roll video clips  check box if you do not require users to watch the videos.
	 
	 
		Expand the Automation section and to view the following check box options:
		  
				 Stream Event Live   Automate live event as a streamed webcast. If you do not select this check box, the event records to the station's local hard drive (offline recording) and will not broadcast live.
			 
			 
				 Publish to Web   Automate publishing to Capture Portal after webcast is finished.
			 
			 
				 Recur   Automate live event regularly (daily, weekly, monthly, yearly) until a specified end date.
			 
		  
	 
		Specify the location of the automation from the  Room  drop-down list.
	 
	 
		Click  Create .
	 
  
	See also
 

  
		 Understanding access controls 
	 
      Audience:     Instructor       

    
           

                   ‹ Capture Station 
        
                   up 
        
                   Editing in post-production › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Capture  
  
      Participating in CaptureCast presentations    Understanding Capture components    Using Capture    Capture Station    Creating automated live events      Editing in post-production    Understanding Capture Toolbox    Capture Central in Learning Environment    
                  
           
         

       
     

    
    
    
   
 
   
 
