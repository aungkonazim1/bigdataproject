Understanding cascading permissions | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Understanding cascading permissions | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    ePortfolio    »    Understanding the basic concepts in sharing   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Understanding cascading permissions                       
        
        
       
          
     
           Printer-friendly version       
	Items in collections and presentations
 

 
	When you include an item in a collection or presentation, the item inherits permissions from the collection or presentation (permissions are cascading), with the following exceptions:
 

  
		The Add Assessments permission is not inherited because it requires that a rubric be associated with each item.
	 
	 
		The Edit permission is not inherited because it would allow users to make major, possibly unwanted, changes to your ePortfolio. If you want individuals to be able to edit items in a collection or presentation, you must give them the Edit permission on each item individually.
	 
  
	Inherited permissions do not override existing permissions on items. For example, if you give an individual the view, see comments from others, and add comments permissions for a specific item, but only the view permission is set for a collection that contains the item, the individual is able to see and add comments to the item. This is the case regardless of whether it is accessed from the collection or outside the collection.
 

 
	Items associated with a reflection
 

 
	When you associate a reflection with another item, the reflection does not inherit permissions from the item, nor does the item inherit permissions from the reflection.
 

 
	Sharing a reflection associated with an item, only allows users to view the name of the associated item. However, sharing an item associated with a reflection allows users to view the reflection as part of the item.
 
     Audience:    Learner      

    
           

                   ‹ Sharing permission options 
        
                   up 
        
                   Setting up sharing groups › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        ePortfolio  
  
      ePortfolio basics    Using artifacts    Creating and editing presentations    Creating collections    Managing comments and assessments in ePortfolio    Understanding the basic concepts in sharing    Viewing items shared with you    Sharing with internal and external users    Creating quicklinks to ePortfolio items    Sharing permission options    Understanding cascading permissions    Setting up sharing groups    Removing sharing permissions    Ignoring and restoring items from users      Importing and exporting items    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
