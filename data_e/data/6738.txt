School Psychology Program - Department of Psychology - University of Memphis    










 
 
 
     



 
    
    
    School Psychology Program  - 
      	Department of Psychology
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Psychology
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Undergraduate  
                          Graduate  
                          People  
                          Centers  
                          Research  
                          Resources  
                      
                      
                         
                            Graduate   
                            
                               
                                  
                                   PhD in Clinical Psychology  
                                         About the Program  
                                         Clinical Faculty  
                                         Child Clinical  
                                         Clinical Health Psychology  
                                         Psychotherapy Research  
                                         Admission  
                                         Clinical Student Handbook  
                                         Clinical Forum Schedule  
                                         Clinical Program Announcements  
                                         Clinical Alumni News  
                                     
                                  
                                   PhD in Experimental Psychology  
                                         About the Program  
                                         Experimental Faculty  
                                         Cognitive  
                                         Behavioral Neuroscience  
                                         Required Courses  
                                         Admission  
                                         Financial Support  
                                     
                                  
                                   PhD in School Psychology  
                                        
                                         About the Programs  
                                        
                                         School Faculty  
                                        
                                         Historical Statement  
                                        
                                         Financial Support  
                                        
                                         M.S./Ph.D. Program Philosophy and Training Model  
                                        
                                         M.S./Ph.D. Program Goals and Objectives  
                                        
                                         Doctoral Handbook  
                                        
                                         School Psychology Association  
                                        
                                         Child   Family Studies Research Group  
                                     
                                  
                                   MA/Ed.S. in School Psychology  
                                        
                                         About the Programs  
                                        
                                         School Faculty  
                                        
                                         M.A./Ed.S. Program Goals  
                                        
                                         Historical Statement  
                                        
                                         Financial Support  
                                        
                                         School Psychology Association  
                                        
                                         What is a School Psychologist?  
                                        
                                         Child   Family Studies Research Group  
                                     
                                  
                                   Masters in General Psychology  
                                         About the Program  
                                         Program Goals  
                                         Required Courses  
                                         Admission  
                                     
                                  
                                   Apply to Psychology Graduate Programs  
                                         Instructions for Applying  
                                         Department Application  
                                         Graduate School Application  
                                     
                                  
                                   Graduate Student Coordinating Committee  
                                         About GSCC  
                                         GSCC Concerns/Complaints  
                                         GSCC Travel Funding  
                                     
                                  
                                   Graduate Program Forms and Resources  
                                   Clinical Student Admissions, Outcomes and Other Data  
                                   School Student Admissions, Outcomes and Other Data  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of Psychology
                              	  
                          
                              	Graduate
                              	  
                         School Psychology Program  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                         School Psychology Programs
                      
                     
                      The University of Memphis offers two degree programs in school psychology. The  Master of Arts (MA) and Educational Specialist (EdS ) degree program is designed for persons seeking nondoctoral preparation for practice
                        as a school psychologist, primarily in public school settings. Students complete the
                        MA degree of 37 semester hours and then enter the EdS degree program and complete
                        an additional 30 semester hours including an internship ( see more information in the program handbook ). The MA/EdS program usually takes three years to complete. Graduates obtain a credential
                        to practice as a school psychologist from the Tennessee State Department of Education
                        or similar agency in other states. More than 120 program graduates now serve as school
                        psychologists in the Mid-South Region and across the United States. The program is
                        approved by the National Association of School Psychologists and formally accredited
                        as part of the education unit at the University of Memphis by the Council for the
                        Accreditation of Educator Preparation. For more information about the MA/EdS program,
                        contact program director  Dr. Thomas Fagan  at  tfagan@memphis.edu .
                      
                     
                      The  Doctor of Philosophy  program is designed to prepare students for various school psychology roles in public
                        school and community settings and for academic careers. It places a strong emphasis
                        on scientific thinking, research design, and statistics; offers advanced practicum
                        experiences in school, hospital, and clinic settings; and allows students to develop
                        expertise in an area of subspecialization. Numerous clinical, supervision, and teaching
                        experiences are provided, and a year-long internship is required. A thesis and a dissertation
                        must be completed. The degree program currently requires at least 97 semester hours
                        and is usually completed over a 5-year period ( see more information in the program handbook ). Students and program graduates have been highly successful in publishing research
                        and other scholarly work in journals, books, and other outlets ( see a list of publications at this link ), and students have been highly successful in obtaining APA-accredited internships
                         (see a list of these internships at this link ). Graduates are employed in public schools, developmental disabilities and medical
                        centers, and colleges and universities in Arkansas, Hawaii, Illinois, Kentucky, Louisiana,
                        South Carolina, Tennessee, and Texas.  For more information about the program's admissions,
                        outcomes, and other data, follow this link ( School Student Admissions, Outcomes and other Data ).
                      
                     
                       As of March 28, 2014, the doctoral program is accredited by the American Psychological
                           Association (APA), and its next accreditation site visit will be held in 2021 . Information about APA accreditation can be found by contacting the Commission on
                        Accreditation of the American Psychological Association at the Office of Program Consultation
                        and Accreditation; 750 First Street, NE; Washington, DC  20002-4242; Phone: 202.336.5979;
                        and TDD/TTY: 202.336.6123. The Commission on Accreditation’s website is  http://www.apa.org/ed/accreditation/  For more information about the doctoral program, contact program director  Dr. Randy Floyd  at  rgfloyd@memphis.edu .
                      
                     
                      The University of Memphis  Graduate Catalog  has important details about admission requirements.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Graduate 
                         
                            
                               
                                PhD in Clinical Psychology  
                                      About the Program  
                                      Clinical Faculty  
                                      Child Clinical  
                                      Clinical Health Psychology  
                                      Psychotherapy Research  
                                      Admission  
                                      Clinical Student Handbook  
                                      Clinical Forum Schedule  
                                      Clinical Program Announcements  
                                      Clinical Alumni News  
                                  
                               
                                PhD in Experimental Psychology  
                                      About the Program  
                                      Experimental Faculty  
                                      Cognitive  
                                      Behavioral Neuroscience  
                                      Required Courses  
                                      Admission  
                                      Financial Support  
                                  
                               
                                PhD in School Psychology  
                                     
                                      About the Programs  
                                     
                                      School Faculty  
                                     
                                      Historical Statement  
                                     
                                      Financial Support  
                                     
                                      M.S./Ph.D. Program Philosophy and Training Model  
                                     
                                      M.S./Ph.D. Program Goals and Objectives  
                                     
                                      Doctoral Handbook  
                                     
                                      School Psychology Association  
                                     
                                      Child   Family Studies Research Group  
                                  
                               
                                MA/Ed.S. in School Psychology  
                                     
                                      About the Programs  
                                     
                                      School Faculty  
                                     
                                      M.A./Ed.S. Program Goals  
                                     
                                      Historical Statement  
                                     
                                      Financial Support  
                                     
                                      School Psychology Association  
                                     
                                      What is a School Psychologist?  
                                     
                                      Child   Family Studies Research Group  
                                  
                               
                                Masters in General Psychology  
                                      About the Program  
                                      Program Goals  
                                      Required Courses  
                                      Admission  
                                  
                               
                                Apply to Psychology Graduate Programs  
                                      Instructions for Applying  
                                      Department Application  
                                      Graduate School Application  
                                  
                               
                                Graduate Student Coordinating Committee  
                                      About GSCC  
                                      GSCC Concerns/Complaints  
                                      GSCC Travel Funding  
                                  
                               
                                Graduate Program Forms and Resources  
                                Clinical Student Admissions, Outcomes and Other Data  
                                School Student Admissions, Outcomes and Other Data  
                            
                         
                      
                      
                      
                         
                            
                                Psychology Graduate Programs Application  
                               Click on link to Apply to the Department of Psychology Graduate Program 
                            
                            
                                Academic Advising   Resource Center (AARC)  
                                The AARC provides advising to students helping them make the most of their undergraduate
                                 education at the UofM.
                               
                            
                            
                                The Psychological Services Center  
                               PSC provides general outpatient psychotherapeutic and psychological assessment services
                                 to individuals and families
                               
                            
                            
                                Teaching Take-Out  
                               This website is a resource for busy teachers who want to enrich their classes while
                                 preserving the time they need for research and other important professional activities.
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/10/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


