School Library Information Specialist Graduate Certificate - ICL - University of Memphis    










 
 
 
     



 
    
    
    School Library Information Specialist Graduate Certificate - 
      	ICL
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Instruction and Curriculum Leadership 
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Programs  
                          Students  
                          Faculty  
                          ICL Faculty Research  
                          A-Z List  
                      
                      
                         
                            ICL Programs   
                            
                               
                                  
                                   Early Childhood  
                                   Elementary Education  
                                   Instruction and Curriculum  
                                   Instructional Design   Technology  
                                   Reading  
                                   School Library Specialist  
                                   Secondary Education  
                                   Special Education  
                                   Graduate Certificates  
                                        
                                         Urban Education  
                                        
                                         IDT  
                                        
                                         Literacy, Leadership,   Coaching  
                                        
                                         Autism Studies  
                                        
                                         School Library Information Specialist  
                                        
                                         STEM Teacher Leadership  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	ICL
                              	  
                          
                              	Programs
                              	  
                         School Library Information Specialist Graduate Certificate 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                       School Library Information Specialist Graduate Certificate  
                     
                      The program of study for a graduate certificate for School Library Information Specialist
                        (SLIS) is offered 100% online. The purpose of the Graduate Certificate for SLIS is
                        to provide practical training to individuals who are presently teaching or are interested
                        in obtaining teacher licensure with a specialization as a School Library Information
                        Specialist.
                      
                     
                      Program Admission 
                     
                      Students wanting to earn the Graduate Certificate for School Library Information Specialists
                        must have an earned Master's degree and apply to be accepted in this graduate certificate
                        program. Applicants must also hold a current teaching license or be in the process
                        of obtaining teacher licensure. The courses may be completed as part of a degree program
                        with the SLIS program coordinator and graduate advisor's approval.
                      
                     
                      To be admitted students should apply to the  Graduate School  as a Master's Degree seeking applicant. Under 'Planned Course of Study' the student
                        should select 'School Library Info Specialists CERT'.
                      
                     
                      Program Requirements 
                     
                      A total of twenty-one (21) credit hours are required to complete this program. 1. ICL 7730 - Foundations of Librarianship, 3 credits 2. ICL 7731 - Introduction to Bibliography, 3 credits 3. ICL 7301 - Literature in the PreK-12 School, 3 credits 4. ICL 7132 - Cataloguing and Classification, 3 credits 5. ICL 7133 - School Library Administration, 3 credits 6. ICL 7134 - Internet in the School Library, 3 credits 7. ICL 7800 - Advanced Clinical Practicum for SLIS, 3-6 credits
                      
                     
                      
                        
                          Course Plan  
                        
                          Course Rotation  
                        
                      
                     
                      Graduation Requirements 
                     
                      1. The student must complete all seven required courses with a Grade Point Average
                        (GPA) of 3.25 or higher, for a total of 21 credit hours. 2. In the semester of the advanced clinical practicum, the student must submit a Graduate
                        Certificate Candidacy form to the COE Graduate Analyst in Ball Hall 215 (3798 Walker
                        Ave., Memphis, TN 38152) by the deadline specified by the Graduate School.
                      
                     
                      Contact Us 
                     
                      Graduate Office: 901-678-4861  icl-graduate-studies@memphis.edu  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         ICL Programs 
                         
                            
                               
                                Early Childhood  
                                Elementary Education  
                                Instruction and Curriculum  
                                Instructional Design   Technology  
                                Reading  
                                School Library Specialist  
                                Secondary Education  
                                Special Education  
                                Graduate Certificates  
                                     
                                      Urban Education  
                                     
                                      IDT  
                                     
                                      Literacy, Leadership,   Coaching  
                                     
                                      Autism Studies  
                                     
                                      School Library Information Specialist  
                                     
                                      STEM Teacher Leadership  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                ICL Degrees  
                               Broaden your career choices by gaining the confidence you to need to succeed 
                            
                            
                                Apply Now  
                               Every journey starts with a step, now take yours! 
                            
                            
                                Advising  
                               Your academic success begins with advising. 
                            
                            
                                Contact Us  
                               Questions? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


