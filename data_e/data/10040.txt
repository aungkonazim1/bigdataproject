 May 2014 Commencement - Commencement Office - University of Memphis    










 
 
 
     



 
    
    
     May 2014 Commencement - 
      	Commencement Office
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Commencement Office
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Apply to Graduate  
                          Graduates  
                          Accommodations  
                          Diplomas  
                          Guests  
                          Faculty  
                      
                      
                         
                            May 2014 Commencement   
                            
                               
                                  
                                   Overview  
                                   Mr. Marvin Ellison  
                                         About Mr. Ellison  
                                         Read Address to Graduates  
                                         Watch Address to Graduates  
                                     
                                  
                                   Mrs. Honey Helen Scheidt  
                                         About Mrs. Scheidt  
                                         Watch Award Reception  
                                     
                                  
                                   Photo Album  
                                   View Commencement Program  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Commencement Office
                              	  
                         
                           	May 2014 Commencement
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      May 2014 Commencement 
                     
                      May 6, 2014 - The University of Memphis held its spring Commencement on Saturday,
                        May 10, awarding a total of 2,512 degrees, including 57 doctorates, in two ceremonies
                        at FedExForum. Marvin Ellison, a U of M alumnus and executive vice president of U.S.
                        stores for The Home Depot, was the speaker at both ceremonies. The U of M presented
                        an honorary doctor of letters degree to community leader and philanthropist Honey
                        H. Scheidt at the afternoon ceremony.
                      
                     
                      During the 10 a.m. Commencement, the College of Arts   Sciences, the College of Communication
                        and Fine Arts and the University College conferred degrees. At 2 p.m., graduates of
                        the Fogelman College of Business   Economics, the College of Education, Health and
                        Human Sciences, the Herff College of Engineering, the Loewenberg School of Nursing,
                        the School of Communication Sciences and Disorders, and the School of Public Health
                        received their degrees.
                      
                     
                      Ellison is responsible for driving alignment and execution across the company's retail
                        divisions and overseeing operations of more than 1,972 stores in the United States,
                        Guam, Puerto Rico and the U.S. Virgin Islands. He has more than 30 years of retail
                        experience and has served in a variety of operational roles at The Home Depot since
                        joining the company in 2002. He previously was president of the company's Northern
                        Division, where he was responsible for sales and operations of more than 850 stores
                        in 21 states.
                      
                     
                      Scheidt has a long and distinguished association with the University. A generous gift
                        from Scheidt and her husband, Rudi, in 2000 transformed a growing music department
                        into the world-class Rudi E. Scheidt School of Music. Their support helped revive
                        the University's opera program and has been used to recruit and retain outstanding
                        faculty and students. Honey Scheidt was instrumental in establishing the U of M's
                        Institute for Egyptian Art and Archaeology and the Dorothy Kayser Hohenberg Chair
                        of Excellence in Art History. She has served on numerous boards, including the Germantown
                        Performing Arts Center, Boston Museum of Fine Arts, Dixon Gallery of Art, World Literacy
                        Foundation and Memphis in May. She has been recognized with many awards for service,
                        including the Humanitarian Award from the National Council of Christians and Jews
                        and the Tennessee Governor's Art Leadership Award along with her husband in 2001.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         May 2014 Commencement 
                         
                            
                               
                                Overview  
                                Mr. Marvin Ellison  
                                      About Mr. Ellison  
                                      Read Address to Graduates  
                                      Watch Address to Graduates  
                                  
                               
                                Mrs. Honey Helen Scheidt  
                                      About Mrs. Scheidt  
                                      Watch Award Reception  
                                  
                               
                                Photo Album  
                                View Commencement Program  
                            
                         
                      
                      
                      
                         
                            
                                Contact Us  
                               Got questions? Let us know how we may assist you. 
                            
                            
                                Commencement Attendance Form  
                               Will you be attending Commencement?  Inform us! 
                            
                            
                                News  
                               Learn about possible date changes, future and past Commencements, Honors Assembly
                                 and more.
                               
                            
                            
                                Commencement Live  
                               Can't attend Commencement?  Watch it live! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


