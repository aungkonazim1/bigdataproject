Restoring excluded course notifications | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Restoring excluded course notifications | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Getting Started in Learning Environment    »    Notifications   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Restoring excluded course notifications                       
        
        
       
          
     
           Printer-friendly version        
		Click  Notifications  from your personal menu on the minibar.
	 
	 
		Click the  Manage my course exclusions  link in the Exclude Some Courses section.
	 
	 
		Click the    Restart notifications  icon beside each course you want to receive notifications from, or click  Restore excluded courses  to restore all excluded courses. You can also use the Search field to find the course you want to restore.
	 
	 
		Click  Close , then click  Save .
	 
      Audience:    Learner      

    
           

                   ‹ Excluding courses from notifications 
        
                   up 
        
                   Viewing Learning Environment with role switch › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Getting Started in Learning Environment  
  
      Tasks to complete before logging in to Learning Environment    Learning Environment basics    Account Settings    Notifications    Accessing Notifications    Understanding notifications    Setting up an email contact method    Setting up a mobile contact method    Subscribing to a summary of activity    Subscribing to instant notifications    Subscribing to customized notifications    Excluding courses from notifications    Restoring excluded course notifications      Viewing Learning Environment with role switch    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
