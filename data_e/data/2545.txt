Previously Earned Credits in the Ph.D. Program - Department of History - University of Memphis    










 
 
 
     



 
    
    
    Previously Earned Credits in the Ph.D. Program - 
      	Department of History
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of History
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Why History?  
                          Undergraduate  
                          Graduate  
                          Online  
                          People  
                          HERC  
                          GAAAH  
                      
                      
                         
                            PhD Program   
                            
                               
                                  
                                  
                                    
                                      The Program  
                                    
                                  
                                  
                                    
                                      Application  
                                    
                                  
                                  
                                    
                                      Advising  
                                    
                                  
                                  
                                    
                                      Nature of Course Offerings  
                                    
                                  
                                  
                                    
                                      Fields of Study  
                                    
                                  
                                  
                                    
                                      Requirements and Restrictions  
                                    
                                  
                                  
                                    
                                      Grades  
                                    
                                  
                                  
                                    
                                      Previously Earned Credits  
                                    
                                  
                                  
                                    
                                      Time Limitation  
                                    
                                  
                                  
                                    
                                      Foreign Language Requirement  
                                    
                                  
                                  
                                    
                                      Comprehensive Examination  
                                    
                                  
                                  
                                    
                                      Reading Lists for Ph.D. Comprehensive Examination Fields  
                                    
                                  
                                  
                                    
                                      Prospectus  
                                    
                                  
                                  
                                    
                                      Dissertation  
                                    
                                  
                                  
                                    
                                      Program Overview  
                                    
                                  
                                  
                                    
                                      Forms and Applications  
                                    
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of History
                              	  
                          
                              	PhD Program
                              	  
                         Previously Earned Credits in the Ph.D. Program 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Previously Earned Credits in the Ph.D. Program 
                     
                      With the approval of your Advisory Committee, you can count up to 33 hours of course
                        work with grades of B or higher from your master's degree toward the 60 required credits.
                        This 33 credit maximum applies whether you took the courses at The University of Memphis
                        or elsewhere. The Advisory Committee will assess the suitability of previously earned
                        credits for the proposed degree in its first formal meeting; the Director of Graduate
                        Studies will approve these recommendations after confirming that the credits meet
                        departmental requirements. After your Advisory Committee has approved previously earned
                        credits, fill out the  Transfer Credit Evaluation (For Doctoral Degree Programs Only) form  (pdf). When the form asks for UofM Equivalent what it is really asking is if it is
                        the equivalent of our 6000-level courses (i.e., undergraduate students could enroll
                        in the same class) or of our 7/8000-level courses (i.e., only graduate students could
                        enroll). ). So all you need enter is 6___, 7___, or 8___. You do not need to write
                        in a specific course number. An exception is if you want to use the course as the
                        equivalent of a specific required course, like a Research Seminar, Historiography
                        course, or Philosophy and Theory of History, in which case you should enter the full
                        equivalent course number.
                      
                     
                      In addition, credits earned beyond the M.A. degree, whether from another institution
                        or The University of Memphis, can count toward the Ph.D. so long as your Advisory
                        Committee agrees and so long as you also meet the requirement that you take the final
                        30 credits at the University of Memphis. These credits, however, fall under the time
                        limit for courses (see  Time Limitation ).
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         PhD Program 
                         
                            
                               
                               
                                 
                                   The Program  
                                 
                               
                               
                                 
                                   Application  
                                 
                               
                               
                                 
                                   Advising  
                                 
                               
                               
                                 
                                   Nature of Course Offerings  
                                 
                               
                               
                                 
                                   Fields of Study  
                                 
                               
                               
                                 
                                   Requirements and Restrictions  
                                 
                               
                               
                                 
                                   Grades  
                                 
                               
                               
                                 
                                   Previously Earned Credits  
                                 
                               
                               
                                 
                                   Time Limitation  
                                 
                               
                               
                                 
                                   Foreign Language Requirement  
                                 
                               
                               
                                 
                                   Comprehensive Examination  
                                 
                               
                               
                                 
                                   Reading Lists for Ph.D. Comprehensive Examination Fields  
                                 
                               
                               
                                 
                                   Prospectus  
                                 
                               
                               
                                 
                                   Dissertation  
                                 
                               
                               
                                 
                                   Program Overview  
                                 
                               
                               
                                 
                                   Forms and Applications  
                                 
                               
                            
                         
                      
                      
                      
                         
                            
                                History Happenings  
                               Significant happenings that involve our faculty, students, staff, and alumni. 
                            
                            
                                Newsletter  
                               The department newsletter is filled with interesting articles and information about
                                 our award-winning faculty and students.
                               
                            
                            
                                Event Calendar  
                               Check here often for upcoming events hosted by the Department of History 
                            
                            
                                Contact Us  
                               Contact the Department of History at The University of Memphis. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


