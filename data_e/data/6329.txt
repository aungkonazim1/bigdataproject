Stafford, M. R., Stern, B., Stafford, T. F. and Bienstock, C. 2008. "Consumer Acceptance of Online Auctions: An Extension and Revision of the TAM ," Psychology &amp; Marketing (25:7), pp. 619-636. - BIT - University of Memphis    










 
 
 
     



 
    
    
    Stafford, M. R., Stern, B., Stafford, T. F. and Bienstock, C. 2008. "Consumer Acceptance
      of Online Auctions: An Extension and Revision of the TAM ," Psychology   Marketing
      (25:7), pp. 619-636. - 
      	BIT
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - Department of Business Information and Technology
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          BIT  
                          Programs  
                          Faculty  
                          Research  
                          Prospective Students  
                          Current Students  
                          FCBE  
                      
                      
                         
                            Research Focus   
                            
                               
                                  
                                   MIS Quarterly  
                                   Information Systems Research  
                                   Journal of AIS  
                                   Journal of MIS  
                                   European Journal of Information Systems  
                                   Information Systems Journal  
                                   MIS Quarterly Executive  
                                   Sloan Management Review  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	BIT
                              	  
                          
                              	Research
                              	  
                         Stafford, M. R., Stern, B., Stafford, T. F. and Bienstock, C. 2008. "Consumer Acceptance
                           of Online Auctions: An Extension and Revision of the TAM ," Psychology   Marketing
                           (25:7), pp. 619-636.
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     The study extends and revises the original Technology Acceptance Model (TAM) by applying
                     it in the context of online auctions and introducing three new consumer-oriented variables:
                     affinity with the computer, impulsiveness, and risk tolerance. It begins with an examination
                     of eBay, the first and most successful online auction site, to show that its business
                     is the technology that fuels growth and increased profits. The paper then addresses
                     the original cognitive TAM variables, used mostly in business contexts, and then the
                     revision designed to add emotional antecedents suitable to the consumer context. Hypotheses
                     that incorporate all of the variables and the relationships among them are tested
                     in a study of online auction consumer behavior, and support is found for the extended
                     TAM. Findings thus reveal that the TAM is stable across contexts and that additional
                     user variables help explain the acceptance of consumer technology use in personal
                     scenarios.
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Research Focus 
                         
                            
                               
                                MIS Quarterly  
                                Information Systems Research  
                                Journal of AIS  
                                Journal of MIS  
                                European Journal of Information Systems  
                                Information Systems Journal  
                                MIS Quarterly Executive  
                                Sloan Management Review  
                            
                         
                      
                      
                      
                         
                            
                                BIT News  
                               Publications, recognitions and spotlights about faculty and students of the BIT department. 
                            
                            
                                BIT Events  
                               Find out what's happening at the department and visit our weekly colloquium. 
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


