Creating group restricted dropbox folders | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Creating group restricted dropbox folders | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Groups    »    Managing groups and group categories   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Creating group restricted dropbox folders                       
        
        
       
          
     
           Printer-friendly version       
	When you create a new category with  Set up dropbox folders  selected, you are automatically taken to the Create Dropbox Folders page. Creating a dropbox folder from this page creates a folder for each of the groups in the category. See  Creating dropbox folder s for more information.
 

 
	 Note   Click  Skip  to go to the next page in the groups setup process without creating any shared folders.
 

 
	See also
 

  
		 Creating dropbox folders 
	 
      Audience:    Instructor      

    
           

                   ‹ Creating group restricted discussion areas 
        
                   up 
        
                   Creating group restricted locker areas › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Groups  
  
      Groups basics    Managing groups and group categories    Creating a group category    Creating group restricted discussion areas    Creating group restricted dropbox folders    Creating group restricted locker areas    Creating a group    Creating a new group after enrolling users    Advanced properties and additional options when setting up groups    Editing groups and group categories    Deleting groups and group categories      Managing group enrollments    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
