Royal Barks - Hypostyle - University of Memphis    










 
 
 
     



 
    
    
    Royal Barks - 
      	Hypostyle
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			The Karnak Great Hypostyle Hall Project
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About Hypostyle  
                          Meaning   Function  
                          Reliefs   Inscriptions  
                          The Project  
                      
                      
                         
                            Meaning and Function   
                            
                               
                                  
                                   Mansion of the Gods: Egyptian Temple Design  
                                   Hypostyle Halls  
                                   A Model of the Universe  
                                   Name of the Great Hypostyle Hall  
                                   Amun-Re and the Theban Triad  
                                   Mut and Khonsu  
                                   Festivals and the Royal Cult  
                                   Sacred Barks and Divine Rest Stations  
                                   Royal Barks  
                                   About the Hypostyle Hall  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Hypostyle
                              	  
                          
                              	Meaning and Function
                              	  
                         Royal Barks 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      The Royal Bark 
                     
                      Accompanying the sacred barks of the Theban Triad on these festival outings was a
                        fourth shoulder-borne model vessel consecrated to the reigning king. Pharaoh himself
                        possessed an indwelling divine spirit closely linked to Amun-Re who, it was believed,
                        had actually fathered him. Although every pharaoh was considered Amun-Re's progeny,
                        this mythology was articulated most explicitly in the "Divine Birth" inscriptions
                        of King  Amenhotep III  (ruled ca. 1390-1352 BCE) at Luxor Temple and those of Queen Hatshepsut in her memorial
                        temple at  Deir el-Bahari  in Western Thebes. As with the gods themselves, perambulation of the king's cult
                        statue in his sacred bark was integral to the Opet Festival and Valley Festival, both
                        of which celebrated the mystical connection between Amun-Re and the king. At Luxor
                        Temple during Opet, their spirits temporarily melded as one, recharging both of their
                        mystical energies for another year. 
                      
                     
                          
                     
                      
                        
                         
                           
                            
                              
                                Representation of the king's processional bark, as indicated by its aegis showing
                                    a king's head wearing a nemes head cloth and an elaborate hemhem crown. Relief from
                                    the Colonnade Hall of Luxor Temple.  
                              
                            
                           
                         
                        
                      
                     
                      Sety I and Ramesses II commanded elaborate scenes to be added to the Hypostyle Hall's
                        walls depicting the journey of the divine and royal sacred barks during these festivals.
                        Accompanying them are hieroglyphic captions describing their royal piety in carrying
                        Amun's bark on their own shoulders during processions and in constructing the Hypostyle
                        Hall as monumental infrastructure for these festivities.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Meaning and Function 
                         
                            
                               
                                Mansion of the Gods: Egyptian Temple Design  
                                Hypostyle Halls  
                                A Model of the Universe  
                                Name of the Great Hypostyle Hall  
                                Amun-Re and the Theban Triad  
                                Mut and Khonsu  
                                Festivals and the Royal Cult  
                                Sacred Barks and Divine Rest Stations  
                                Royal Barks  
                                About the Hypostyle Hall  
                            
                         
                      
                      
                      
                         
                            
                                Welcome to the Karnak Great Hypostyle Hall Project!  
                               In antiquity, Karnak was the largest religious sanctuary in Egypt's imperial capital
                                 of Thebes (modern Luxor) and was home to the god Amun-Re, king of the Egyptian pantheon.
                               
                            
                            
                                About the Architecture  
                               Who designed the Hypostyle Hall and why did they do it? Read about how Sety I and
                                 his architects built this magnificent structure between two pylon gateways which once
                                 served as the front entrance to Karnak Temple. 
                               
                            
                            
                                Tour the Hall  
                               The Hypostyle Hall is thousands of miles away -- but you can tour entire structure
                                 without leaving your seat! Click here to begin a virtual tour of the historic Hypostyle
                                 Hall. 
                               
                            
                            
                                Epigraphy Methods  
                               "Why don't you just take a photograph?" Why traditional photography is not the most
                                 scientifically reliable method of recording.
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


