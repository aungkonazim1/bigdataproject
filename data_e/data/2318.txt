 Research Focus for Department of Biological Sciences - Biological Sciences - University of Memphis    










 
 
 
     



 
    
    
     Research Focus for Department of Biological Sciences - 
      	Biological Sciences
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Biological Sciences
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Undergraduate  
                          Graduate  
                          Centers  
                          People  
                          Research  
                          Events  
                          Scholarships  
                      
                      
                         
                            Research Focus   
                            
                               
                                  
                                   About  
                                   Beetle Genome  
                                   Alzheimers  
                                   Dioxin  
                                   Bioinformatics  
                                   IMC  
                                   Herbarium  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Biological Sciences
                              	  
                         
                           	Research
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Research Focus 
                     
                         The Department of Biological Sciences emphasizes the integrative nature of the biological
                        sciences, focusing on research programs that unite three major foci: evolution/ecology,
                        physiology, and biochemistry, cell and molecular biology.
                      
                     
                      Faculty research programs reflect the interdisciplinary strength of our department,
                        while providing a firm foundation in theoretical focal areas. Recent faculty hires
                        support our rich tradition of research training in these traditional fundamental disciplines
                        while providing the specializations to foster interdisciplinary relationships.
                      
                     
                      
                     
                      Evolution/Ecology 
                     
                       Randall Bayer, Professor , (Plant Systematic   Evolution)
                      
                     
                       Melvin Beck, Professor , Interim Chair: (genetics)
                      
                     
                       Michael Kennedy, Professor : (wildlife ecology, mammal systematics)
                      
                     
                       Jennifer Mandel, Assistant Professor : (Population Genetics;Ecological and Evolutionary Genetics)
                      
                     
                       Duane McKenna , Assistant Professor (Evolutionary ecology, Systematics)
                      
                     
                       Matthew Parris, Associate Professor : (evolutionary ecology, herpetology)
                      
                     
                      Physiology 
                     
                       Michael Ferkin, Professor : (mammal behavior and physiology)
                      
                     
                       David Freeman, Associate Professor : (circadian rhythms, mammal neuroendocrinology)
                      
                     
                       Reza Pezeshki, Professor : (wetland ecology, plant physiology)
                      
                     
                      Biochemistry Cell and Molecular Biology 
                     
                       Amy Abell, Assistant Professor : (epigenetic regulation of gene expression important for EMT;signaling networks)
                      
                     
                       King-Thom Chung, Professor : (environmental science, molecular toxicology)
                      
                     
                       Judith Cole, Associate Professor : (signal transduction, endocrinology)
                      
                     
                       Lewis Coons, Professor : (parasitology)
                      
                     
                       T. Kent Gartner, Professor : (cell physiology of platelets, signal transduction)
                      
                     
                       Charles Lessman, Professor : (cell physiology, reproductive biology, development)
                      
                     
                       Carlos L. Estrano, Associate Professor : (signal transduction, protein trafficking; parasitology)
                      
                     
                       Donald Ourth, Professor : (immunology)
                      
                     
                       Steven Schwartzbach, Professor : (protein trafficking)
                      
                     
                       Omar Skalli, Director, Associate Professor : (intermediate filament protiens)
                      
                     
                       Andrew Liu , Associate Professor (biochemistry, molecular biology and genetics)
                      
                     
                       Thomas Sutter, Professor and Feinstone Chair of Functional Genomics : (cancer prevention, environmental science, signal transduction)
                      
                     
                       Barbara Taller, Associate Professor : (microbiology, plant-microbe interactions)
                      
                     
                       Tit-Yee Wong, Professor : (bacterial physiology, signal transduction)
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Research Focus 
                         
                            
                               
                                About  
                                Beetle Genome  
                                Alzheimers  
                                Dioxin  
                                Bioinformatics  
                                IMC  
                                Herbarium  
                            
                         
                      
                      
                      
                         
                            
                                Biology@Memphis Newsletter  
                               Read about graduate and undergraduate student news; awards, grants and scholarships;
                                 publications, and more...
                               
                            
                            
                                Administration  
                               Budgets, accounting, facilities management, committees, meetings, and group email
                                 accounts
                               
                            
                            
                                Facilities  
                               Not your ordinary classrooms... 
                            
                            
                                Contact Us  
                               Main office contacts, faculty and staff administration 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/28/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


