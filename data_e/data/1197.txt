Mission, Vision, and Values - umTech - Information Technology Services - University of Memphis    










 
 
 
     



 
    
    
    Mission, Vision, and Values - 
      	umTech - Information Technology Services 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			umTech - Information Technology Services 
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Service  
                          Support  
                          Teaching  
                          Smart Tech  
                          Students  
                          Solutions  
                      
                      
                         
                            Teaching   Learning   
                            
                               
                                  
                                   Mission, Vision and Values  
                                   Teaching   Learning News  
                                   Events Calendar  
                                   Service Updates   Alerts  
                                   eCourseware and Online Support  
                                         eCourseware Overview  
                                         Student Resources  
                                         Faculty Resources  
                                         Training Videos   
                                     
                                  
                                   Teaching   Learning Tools  
                                         Accessibility  
                                         Clickers (iClicker REEF)  
                                         TurnItIn (Academic Integrity)  
                                         Respondus  
                                     
                                  
                                   umMedia - Media Services   
                                         Media Services Overview  
                                         BlueJeans  
                                         Ensemble Video  
                                         iTunes U  
                                         MediaSite  
                                         umMeet (Adobe Connect)  
                                     
                                  
                                   umServices  
                                         umServices Overview  
                                         umApps  
                                         umBlog  
                                         umDrive  
                                         umSurvey  
                                         umWiki  
                                     
                                  
                                   A to Z List  
                                   CTL Contact List  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	umTech - Information Technology Services 
                              	  
                          
                              	Teaching   Learning
                              	  
                         Mission, Vision, and Values 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Mission, Vision, and Values 
                     
                      Mission 
                     
                      The mission for the Center for Teaching and Learning is to provide instructional technology
                        training in enterprise software and hardware that supports the University's mission
                        of student success. To empower the University's community through individual consultations,
                        group/department trainings, hosting emerging technology workshops, collaborating on
                        technology issues, and researching new and evaluating existing technologies with other
                        campus members.
                      
                     
                      Vision 
                     
                      Our vision is to connect the community to teaching, learning, and technology resources
                        that promote student success through engaged and collaborative learning environments.
                      
                     
                      Values 
                     
                      The Center for Teaching and Learning seeks to provide the community with: 
                     
                      
                        
                         
                           
                            Technical support that is innovative, responsive, and creative 
                           
                         
                        
                         
                           
                            Customer focus, problem solving, support, and solutions 
                           
                         
                        
                         
                           
                            Professional service through trainings, webinars, and events at all times 
                           
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Teaching   Learning 
                         
                            
                               
                                Mission, Vision and Values  
                                Teaching   Learning News  
                                Events Calendar  
                                Service Updates   Alerts  
                                eCourseware and Online Support  
                                      eCourseware Overview  
                                      Student Resources  
                                      Faculty Resources  
                                      Training Videos   
                                  
                               
                                Teaching   Learning Tools  
                                      Accessibility  
                                      Clickers (iClicker REEF)  
                                      TurnItIn (Academic Integrity)  
                                      Respondus  
                                  
                               
                                umMedia - Media Services   
                                      Media Services Overview  
                                      BlueJeans  
                                      Ensemble Video  
                                      iTunes U  
                                      MediaSite  
                                      umMeet (Adobe Connect)  
                                  
                               
                                umServices  
                                      umServices Overview  
                                      umApps  
                                      umBlog  
                                      umDrive  
                                      umSurvey  
                                      umWiki  
                                  
                               
                                A to Z List  
                                CTL Contact List  
                            
                         
                      
                      
                      
                         
                            
                                Enter a Service Request  
                               Need Help with Technology? Request technical support via umHelpdesk, or search AskTom,
                                 a collection of questions and answers to technical problems.
                               
                            
                            
                                ITS Service Catalog  
                               Browse a collection of ITS services offered, learn how to access them and what Information
                                 is needed to fulfill service requests. 
                               
                            
                            
                                Preparing for Banner 9  
                               The UofM is upgrading to Banner 9. Keep up with the latest developments. 
                            
                            
                                Contact Us  
                               Contact the ITS Service Desk at 901.678.8888. (Available 24 hours a day, excluding
                                 posted holidays) Chat Live Monday - Friday 8:00 am - 7:00 pm CDT
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


