Practicum &amp; Internship - CEPR - University of Memphis    










 
 
 
     



 
    
    
    Practicum   Internship - 
      	CEPR
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Counseling, Educational Psychology   Research
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Counseling  
                          Counseling Psychology  
                          Ed Psych   Research  
                          Faculty  
                          Research  
                      
                      
                         
                            Counseling   
                            
                               
                                  
                                   Faculty and Research  
                                   Degree Programs  
                                         Counseling-CMH (MS)  
                                         Counseling-School (MS)  
                                         Counseling-Rehab (MS)  
                                         Counseling-Clinical Rehab (MS)  
                                         Counseling (EdD)  
                                     
                                  
                                   Admissions  
                                         MS Application  
                                         EdD Application  
                                         Certificate in Disability Studies Application  
                                         Certificate in Career and College Counseling Application  
                                         Financial Assistance  
                                     
                                  
                                   Mission Statement  
                                   Current Students  
                                        
                                         Practicum   Internship  
                                        
                                         Links to Counseling Resources  
                                        
                                         General Campus Contacts  
                                        
                                          Student Wellbeing and Evaluation   
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	CEPR
                              	  
                          
                              	Counseling
                              	  
                         Practicum   Internship 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Practicum   Internship - Clinical Practice 
                     
                      Coordinator of Clinical Practice in Counseling:  Pamela Cogdal, Ph.D.  
                     
                      
                        
                          Clinical Practice Internship Manual  
                        
                          Fieldwork Forms  
                        
                          Tennessee Bureau of Investigation Background Checks  
                        
                      
                     
                      It is the responsibility of the student clinician to: follow established procedures
                        for application and enrollment in Practicum/Internship courses, to be familiar with
                        site policies for clinical placements, to know program requirements, licensure/certification
                        requirements, and complete all forms by stated deadlines. This manual is required
                        reading for all practicum and internship candidates. (In addition you must sign the
                        attached "Have Read Rights and Responsibilities" prior to practicum or internship
                        class attendance).
                      
                     
                      Additionally, all candidates must have personal professional liability insurance activated
                        at the onset and the duration of their practicum and internship experiences. Documentation
                        of the current liability insurance must be turned in for their student files. School
                        counseling students must also complete all security clearance procedures before working
                        on school sites with children. This is a College of Education, Health and Human Sciences
                        and State mandate!
                      
                     
                      Your supervised field experiences (i.e., practica and internships) are crucial developmental
                        steps in one's professional preparation. Students in CEPRs counseling programs concentrate
                        their experiences in community settings such as mental health centers, community agencies,
                        or hospitals; in city, county or private schools; or higher education settings. These
                        clinical experiences are intended to assist students in the integration of knowledge
                        learned in other academic experiences as well as to develop their clinical skills.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Counseling 
                         
                            
                               
                                Faculty and Research  
                                Degree Programs  
                                      Counseling-CMH (MS)  
                                      Counseling-School (MS)  
                                      Counseling-Rehab (MS)  
                                      Counseling-Clinical Rehab (MS)  
                                      Counseling (EdD)  
                                  
                               
                                Admissions  
                                      MS Application  
                                      EdD Application  
                                      Certificate in Disability Studies Application  
                                      Certificate in Career and College Counseling Application  
                                      Financial Assistance  
                                  
                               
                                Mission Statement  
                                Current Students  
                                     
                                      Practicum   Internship  
                                     
                                      Links to Counseling Resources  
                                     
                                      General Campus Contacts  
                                     
                                       Student Wellbeing and Evaluation   
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                About the Department  
                                
                            
                            
                                Student Organizations in CEPR  
                                
                            
                            
                                Department News  
                                
                            
                            
                                Contact Us  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


