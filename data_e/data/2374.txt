Assistantships and Awards - Communication - University of Memphis    










 
 
 
     



 
    
    
    Assistantships and Awards - 
      	Communication
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Communication
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Graduate  
                          Undergraduate  
                          Faculty/Staff  
                          Alumni  
                          Events  
                      
                      
                         
                            Graduate   
                            
                               
                                  
                                   Graduate Students  
                                   Admissions  
                                   Specialty Areas  
                                   PhD Program  
                                   MA Program  
                                         Overview  
                                         Film   Video Production  
                                         Communication Studies  
                                     
                                  
                                   Course Offerings  
                                   Assistantships   Awards  
                                   Facilities   Equipment  
                                   Living in Memphis  
                                   Policies   Documents  
                                   FAQS  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Communication
                              	  
                          
                              	Graduate
                              	  
                         Assistantships and Awards 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Assistantships and Awards 
                     
                      The most exceptional graduate students in the Department often serve as Graduate Assistants
                        (GAs). Applicants in search of an assistantship with the Department make their case
                        as part of the online  Departmental Application .     Applicants for Fall 2018 admission must have their applications completed by January
                           12, 2018 to be guaranteed consideration for a graduate assistantship.  
                     
                      As GAs, students enrich their scholarly and professional training through teaching
                        basic and advanced undergraduate courses (usually two per term, and usually COMM 2381:
                        Oral Communication), supporting faculty research and creative activity, and service
                        to the Department and University more broadly.
                      
                     
                      COMM Graduate Assistantships fund study in the Fall and Spring. They feature a waiver
                        of the student's tuition and fees as well as a monetary stipend. For Master's students,
                        assistantships are renewable for up to two (2) years; for Doctoral students, the period
                        can be up to four (4) years.
                      
                     
                      Most Master's level GAs work during their first two semesters as research or teaching
                        assistants, and then teach two (2) sections of COMM 2381 during the fall and spring
                        semesters of their second year. Doctoral level GAs typically teach two (2) sections
                        of COMM 2381 per semester throughout their assistantship, though there are sometimes
                        research assistant assignments, as well as assignments to advanced undergraduate courses.
                        GA assignments are determined by the Director of Graduate Studies, the Chair, and
                        the graduate faculty.
                      
                     
                      Doctoral students with at least three (3) semesters of experience teaching COMM 2381:
                        Oral Communication can apply to for a Graduate Assistantship with the University's
                         Center for Communication and Writing .
                      
                     
                      The Graduate School also offers a range of Doctoral and Master's  assistantships and fellowships , as well as help finding academic and nonacademic units at the University of Memphis
                        who are in need of GAs.
                      
                     
                      GA renewals in any case are not automatic, and are always subject to yearly review.
                        In addition to their regular responsibilities, GAs are required to maintain satisfactory
                        progress each term, submit a teaching portfolio each year, attend all Departmental
                        colloquia, and adhere to the University's Code of Student Rights and Responsibilities.
                      
                     
                      Travel Funding 
                     
                      Financial support for student travel to present at academic conferences is offered
                        both by the  University's Graduate Student Association  and by the  Department of Communication itself .
                      
                     
                      Awards 
                     
                      The Department recognizes its top students at the end of each year with awards for
                        excellence in teaching, research and filmmaking. Each carries a cash prize. The Graduate
                        School also offers annual awards for research and teaching. For forms and rules tied
                        to assistantships, travel funding, and awards,  click here .
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Graduate 
                         
                            
                               
                                Graduate Students  
                                Admissions  
                                Specialty Areas  
                                PhD Program  
                                MA Program  
                                      Overview  
                                      Film   Video Production  
                                      Communication Studies  
                                  
                               
                                Course Offerings  
                                Assistantships   Awards  
                                Facilities   Equipment  
                                Living in Memphis  
                                Policies   Documents  
                                FAQS  
                            
                         
                      
                      
                      
                         
                            
                                Student Film Showcase  
                               See sample films produced by BA and MA Film Students. 
                            
                            
                                Get Started in the COMM Major  
                               Declare your major and find your advisor. 
                            
                            
                                Apply to COMM Graduate Programs  
                               Programs in Film   Video Production and Communication. 
                            
                            
                                Contact Us  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


