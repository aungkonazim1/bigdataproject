PhD Finance     PhD Finance 







   



 



   Market 

Microstructure   





 



   



   

   Market 

Microstructure , a focus of the Department, is a relatively young area in 

finance, yet it has grown rapidly into one of the largest. It studies the 

process of trading, the design of markets, the behavior of traders, transaction 

costs, execution quality, and related rules and regulations. The field has had a 

great impact on both the real world and the academic profession. Insights and 

discoveries from microstructure have affected the way stock and futures 

exchanges operate and the regulation of financial markets. Much of the research 

in microstructure uses large data bases comprising every trade and quote from 

markets such as the New York Stock Exchange or the Australian Stock Exchange. 

Microstructure is an area that is equally appealing for both industry and 

academic careers.  



  