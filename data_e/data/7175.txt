Allowing certified Learning Environment artifacts | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Allowing certified Learning Environment artifacts | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    ePortfolio    »    Sharing items within courses   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Allowing certified Learning Environment artifacts                       
        
        
       
          
     
           Printer-friendly version       
	If your organization uses both ePortfolio and Learning Environment, you can allow users to import items from your course as learning artifacts to their ePortfolios. You can allow users to import learning artifacts from the following tools:
 

  
		Quizzes
	 
	 
		Dropbox
	 
	 
		Grades
	 
	 
		Competencies
	 
  
	Learning Environment artifacts differ from other artifacts because they cannot be modified. They record a submission or achievement at a specific point in time. Learning Environment artifacts do not update when items change in a course; users can only change their name, description, and tags. Users cannot modify the actual content.
 

 
	 Note  If a user imports quiz or dropbox folder results into their ePortfolio, learning objectives associated with the quiz or dropbox folder automatically import into the user's ePortfolio with an association to their corresponding artifact.
 

 
	Allow quiz artifacts
 

 
	Select the  Allow users to add this quiz to their ePortfolio  option on the appropriate New Quiz or Edit Quiz page in the Quizzes tool.
 

 
	Allow dropbox artifacts
 

 
	Select the  Allow users to add this folder to their ePortfolio  option on the appropriate New Folder or Edit Folder page in the Dropbox tool.
 

 
	Allow grade artifacts
 

 
	Select the  Allow users to add their grades to their ePortfolio  option on the Org Unit Display Options page in the Grades Setting area in the Grades tool.
 

 
	Allow competency artifacts
 

 
	Select the  Allow users to add their competencies to their ePortfolio  option on the Competency Settings page in the Competencies tool.
 
     Audience:    Instructor      

    
           

                   ‹ Pushing ePortfolio items to others 
        
                   up 
        
                   Using form templates › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        ePortfolio  
  
      ePortfolio basics    Using artifacts    Creating and editing presentations    Creating collections    Managing comments and assessments in ePortfolio    Understanding the basic concepts in sharing    Importing and exporting items    Sharing items within courses    Setting up course sharing groups    Pushing ePortfolio items to others    Allowing certified Learning Environment artifacts      Using form templates    Integrating ePortfolio with Content     Assessing ePortfolio content    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
