Accessible HTML templates | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Accessible HTML templates | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Accessibility    »    Accessible course design   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Accessible HTML templates                       
        
        
       
        
     
              
	The following template packages were designed by Desire2Learn to make it easier for course instructors to create accessible, professional looking HTML content files.
 

 
	The templates use a cascading style sheet (CSS) to format the content in HTML files. To use a template effectively, you must save the TemplateFiles folder containing the stylesheet.css file and supporting images in the same root folder as your HTML files and reference the CSS file appropriately.
 

 
	 Notes 

	  
			If you upload a template package to the Manage Files area for a course and unzip it, it will be structured properly and the sample files will reference the CSS file correctly.
		 
		 
			All of the template packages use the same folder structure and folder name "TemplateFiles." This makes it easy to swap out your CSS files for different CSS files if you want to change the appearance of your course content files. However, it also means you should only have one template package saved in Manage Files at a time.
		 
	  

 
	Table of contents
 

  
		 Available templates 
	 
	 
		 Add a template package to Manage Files 
	 
	 
		 Use a template file in the Content tool 
	 
	 
		 Swap CSS files to change the appearance of your HTML content topics 
	 
	 
		 Edit a CSS file to change the appearance of your HTML content topics 
	 
  
	  Available templates
 

  
		 Classic Contemporary 
	 
	 
		 Desk Paper 
	 
	 
		 Modern Red 
	 
	 
		 Notepaper 
	 
	 
		 Ordinary Blue 
	 
  
	     
	To the extent possible under law, Desire2Learn Incorporated has waived all copyright and related or neighboring rights to the accessibility templates listed above.
 

 
	  Add a template package to Manage Files
 

  
		Save the template package you want to use to your local machine as a zip file.
	 
	 
		Go to the Manage Files tool.
	 
	 
		At your course root, select    Upload .
	 
	 
		Select the template package zip file and click  Upload .
	 
	 
		 Unzip  the file.
	 
	 
		Use the Manage Files tool to view the HTML files available with the package.
	 
	 
		Create copies of the HTML files you want to use as templates in the Content tool.
		  
				 TemplateStyles  This file demonstrates each template style
			 
			 
				 TemplateDetails  This file provides an overview of accessibility principles related to the different template styles
			 
			 
				 SampleSyllabus  This fie contains a sample syllabus using some of the styles
			 
			 
				 SampleContentTopic  This file contains a sample content topic using most of the styles
			 
		  
			We suggest that you create copies of the sample HTML files so you always have unedited template files to refer to.
		 
	 
  
	  Use a template file in the Content tool
 

  
		Go to the Content tool.
	 
	 
		Select    New Topic .
	 
	 
		Select  Course File .
	 
	 
		Give your new topic a  Parent Module  and  Title .
	 
	 
		Select the  Browse  button beside the  Course File  field.
	 
	 
		Select the copy HTML template file you want to use as your template, and click  Select File .
	 
	 
		Update the file with your course content using the HTML Editor. If you copy, paste and overwrite content in the editor view (WYSIWYG), the style formatting should be maintained.
		 
			 Note  You cannot see all of the template styles and images in the editor. Preview the file periodically to ensure it is formatted as expected.
		 
	 
	 
		Click  Save .
	 
  
	If you want to create another content topic based on the template topic, you can copy the file you just created or create another new topic based on the copy HTML template file.
 

 
	  Swap CSS files to change the appearance of your HTML content topics
 

  
		Go to the Manage Files tool.
	 
	 
		Open the  TemplateFiles  folder.
	 
	 
		 Select  and  Delete  all of the template files.
	 
	 
		Save the Desire2Learn template package you want to use to your local machine and unzip it.
	 
	 
		 Upload  all of the files in the TemplateFiles folder of the new package to the TemplateFiles folder in Manage files.
	 
  
	  Edit a CSS file to change the appearance of your HTML content topics
 

  
		Go to the Manage Files tool.
	 
	 
		Open the  TemplateFiles  folder.
	 
	 
		Use the context menu to select     Edit File  for the stylesheet.css file.
	 
	 
		Make your changes and click  Save .
	 
      Audience:     Instructor       

    
           

                   ‹ Referring students to accessibility resources 
        
                   up 
        
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Accessibility  
  
      Using assistive technology in Desire2Learn    Accessible course design    Organizing your course accessibly    Setting release conditions and special access to support students with different needs    Meeting web content accessibility standards    Referring students to accessibility resources    Accessible HTML templates      
                  
           
         

       
     

    
    
    
   
 
   
 
