New Technology, Resources, and Programs | University of Memphis Libraries   
 
 
 
 
 New Technology, Resources, and Programs | University of Memphis Libraries 
 
 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 
 




 
 
  
 
 
 

 
 
 
 
			

         



 

 
 
	 Skip to content 
		 
				 
						  University of Memphis Libraries  
			 News and Events 
		 

		 
			 Menu 
			  
  Home    
		  
	  

	 
			 
			 Home  New Technology, Resources, and Programs 		 
		 
		 

					 
				
 
	 
					 New Technology, Resources, and Programs 
		
		 
						   October 7, 2014  October 7, 2014       jschnbel   
			  Events ,  Exhibition ,  Homepage ,  News ,  Research and Instructional Services ,  Special Collections ,  Uncategorized  		  
	  

	 
		                 Emerging Technologies Librarian Cody Behles teaches students about 3D printing    Memphis Daily News  , September 16, 2014  
 The University Libraries continues to connect our students with current technology to improve teaching and learning. We have added  GoPro cameras  (available to students for check out at Circulation) and a 3D printing lab (students can schedule a training session  here ) for the fall semester. Read more about the Libraries’ new devices in the   Memphis Daily News  . 
 Great database news! University of Memphis students and faculty can use the Wharton Research Data Services (WRDS) Databases to conduct research across multiple business-related disciplines. Click  here  for more information and to apply for a WRDS account. In addition, members the U of M community now have access to the biology module of the  Journal of Visualized Experiments (JOVE) , the world s first peer reviewed scientific video journal. 
 The University Libraries recently acquired a collection of papers from the administration of  Tennessee Governor Winfield Dunn  (1971-1975). A selection of photographs, speeches, telegrams, and other materials will be on display on the first floor of McWherter Library until  Tuesday, October 28.  Researchers can also visit the free  Internet Archive  to read digitized oral histories of the Dunn administration conducted by Dr. Charles Crawford, Department of History. 
 Join us in McWherter Library on  Friday, October 17, 2:30 p.m.  for the first presentation in our new  University Libraries’ Faculty/Staff Research Seminar  series! Historian and librarian Dr. Mark Danley will share his research on strategic thought and the history of books and reading in the eighteenth-century British army. Free and open to the public. 
 On  Tuesday, November 11, 5:30-7:00 p.m . the Friends of the University Libraries will present a special Veterans Day reception and lecture on the fourth floor of McWherter to coincide with the exhibition, “From Active Duty to Veteran: Honoring Military Service in America” (on view November 3-January 16). Dr. Mark Danley will speak on “War, Peace, History, and Memory: Military Service in the American Experience.” Free and open to the public. For additional Veterans Day events on campus, click  here . 
    Thank you to all who attended our Banned Books Week events and visited the display in the rotunda. In case you missed it, you can view our  Facebook photos  here.  A special shout-out to our friends in the Loewenberg School of Nursing for allowing Professor Bones to participate! 

 
			  

	 
			  
  
			 

				 
		 Post navigation 

	
		      Celebrate Banned Books Week with Us!  		  More Ways to Connect to Your Libraries  Resources!      
	
	  
	
			
 

	
	
	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  

		
		  
	  

					 
					 		 Recent Posts 		 
					 
				 3D Printing Workshop 
						 
					 
				 NedXStudents 
						 
					 
				 Craft-n-Chat 
						 
					 
				 Bridging East and West: The First Steel Bridge of Memphis 
						 
					 
				 Banned Books Display 
						 
				 
		 		  Archives 		 
			  November 2017  
	  October 2017  
	  September 2017  
	  August 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  April 2016  
	  February 2016  
	  September 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  May 2014  
	  April 2014  
	  March 2014  
	  February 2014  
	  January 2014  
		 
		   Categories 		 
	  Alerts 
 
	  Branch Libraries 
 
	  Events 
 
	  Exhibition 
 
	  Homepage 
 
	  News 
 
	  Research and Instructional Services 
 
	  Special Collections 
 
	  Trial Resources 
 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 		  
	
	  

	 
		 
			 
								 Proudly powered by WordPress 
				  |  
				Theme: Big Brother by  WordPress.com .			  
					  
	  
  


 

        

        			 
				   Subscribe  
				 

					
						 Follow this blog 

						 
							
															 Get every new post delivered right to your inbox. 
							
							 
								 
							 
							
							 
							 
							
							  							   
						 

					
				 
			 
		







		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 