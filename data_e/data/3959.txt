 University College Graduate Degrees - University College - University of Memphis  The University College graduate programs are designed to fulfill personal and professional development interests not found through other programs on campus.  










 
 
 
     



 
    
    
     University College Graduate Degrees - 
      	University College
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			University College
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Undergraduate  
                          Graduate  
                          Resources  
                          Online  
                          Faculty and Staff  
                      
                      
                         
                            Graduate   
                            
                               
                                  
                                   Master of Arts in Liberal Studies  
                                   Master of Professional Studies  
                                   Certificate in Strategic Leadership  
                                   Certificate in Liberal Studies  
                                   Advising Appointment  
                                   Financial Aid  
                                   Graduate Assistantship  
                                   Questions? Email Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	University College
                              	  
                         
                           	Graduate
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      University College Graduate Programs 
                     
                      The University College graduate programs are designed to fulfill personal and professional
                        development interests not found through other programs on campus. Students have the
                        option to customize a degree plan bridging two or more disciplines and may also earn
                        credit hours through work experience or professional training. All of our courses
                        are offered in formats tailored to fit the busy schedules of adult, non-traditional
                        students.
                      
                     
                      Master of arts in Liberal studies 
                     
                      The  Master of Arts in Liberal Studies  (MALS) degree at the University of Memphis allows students to customize an interdisciplinary
                        course of study. Designed for those seeking the personal enrichment provided by liberal
                        learning, this degree encourages the development of intellectual skills necessary
                        for lifelong learning—critical reading, scholarly writing, and the art of interpersonal
                        communication. 
                      
                     
                      Master of Professional Studies 
                     
                      The Master of Professional Studies (MPS) is designed for working adults who seek to
                        advance their careers through a targeted professional graduate program. The MPS degree
                        provides flexibility with courses offered online and in a hybrid format. It consists
                        of 33 hours of coursework and is available in three concentrations:
                      
                     
                      
                        
                          Strategic Leadership  prepares students to lead in today's rapidly changing professional environment. This
                           interdisciplinary approach focuses on the areas of leadership, communication, strategic
                           planning and assessment, organizational structure, and research/data analysis.
                         
                        
                          Human Resources Leadership  provides students with the necessary knowledge and skills to work successfully with
                           top management in the field of Human Resources.  This area of study will help students
                           determine appropriate policies and requirements which ensure human resource tasks
                           are accomplished according to the strategic plans and goals of the organization.
                         
                        
                          Training and Development  offers a specialized focus on the assessment, management and delivery of on-site
                           or distance training work force programs.  Students will research effective practices
                           required to evaluate programs and apply leadership theories, analyze and report data,
                           understand e-learning technologies, and design training materials.
                         
                        
                      
                     
                      Certificate in Strategic Leadership 
                     
                      The  Certificate in Strategic Leadership  is a 12-hour program (four courses) designed to provide theoretical and practical
                        knowledge to enhance leadership skills.
                      
                     
                      Certificate in Liberal Studies 
                     
                      Our  Certificate in Liberal Studies  offers a 12-hour (four-course) introduction to interdisciplinary scholarship. It
                        is ideal for students interested in mastering the foundational concepts of liberal
                        learning without necessarily completing the full Master's degree.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Graduate 
                         
                            
                               
                                Master of Arts in Liberal Studies  
                                Master of Professional Studies  
                                Certificate in Strategic Leadership  
                                Certificate in Liberal Studies  
                                Advising Appointment  
                                Financial Aid  
                                Graduate Assistantship  
                                Questions? Email Us  
                            
                         
                      
                      
                      
                         
                            
                                Find the program that's right for you  
                               Explore our degree programs 
                            
                            
                                Learn more about University College  
                               Request more program information 
                            
                            
                                Advising Appointments  
                               Academic Advising is available in-person, by phone or online 
                            
                            
                                Contact Us  
                               The University College team is here to help 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


