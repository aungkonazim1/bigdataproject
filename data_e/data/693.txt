Student Organizations - School of Law - University of Memphis  Memphis Law Student Organizations  










 
 
 
     



 
    
    
    Student Organizations - 
      	School of Law 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Cecil C. Humphreys School of Law
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Admissions  
                          Programs  
                          Current Students  
                          Faculty  
                          Careers  
                          Library  
                      
                      
                         
                            Student Affairs Office   
                            
                               
                                  
                                   Academic Support  
                                         Academic Success Program  
                                         Bar Exam Information  
                                         Disability Resources for Students  
                                         Study Abroad  
                                         Student Advising  
                                     
                                  
                                   Student Support  
                                         Counseling Services  
                                         Fitness  
                                         Housing  
                                         Individual Appointment Info  
                                         Information Technology  
                                         Lactation Room  
                                         Law School ID Cards  
                                         Locker Rentals  
                                         Parking   Transportation  
                                         Print Services  
                                         TN Lawyers Assistance Program  
                                         Health Services  
                                     
                                  
                                   Student Organizations  
                                   Orientation  
                                   Commencement  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	School of Law 
                              	  
                          
                              	Student Affairs Office
                              	  
                         Student Organizations 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                        
                     
                      STUDENT ORGANIZATIONS 
                     
                      Memphis Law has a number of active student organizations. Each organization is listed
                        below with contact information. Throughout a student's law school career many find
                        it extremely beneficial to be a part of one or a few student organizations.
                      
                     
                      Association for Women Attorneys 
                     
                      President:  Noor Obaji  
                     
                      The Association for Women Attorneys student chapter is an organization dedicated to
                        promoting the interests, education, and advancements of women attorneys. Along with
                        the AWA professional chapter, the AWA student chapter regularly coordinates scholarship
                        opportunities, speaking engagements, and meetings for members. Please contact the
                         Memphis Chapter of the AWA  or  memphisawa@yahoo.com  for further information. 
                      
                     
                       Black Law Students Association  President:  Karlyn Washington  
                     
                      Founded in 1966, the Black Law Students Association seeks to promote the professional
                        needs of African-American law students through promoting professional competence and
                        increasing awareness of the needs of the African American community. For further information,
                        please contact  The National BLSA.   
                     
                      Business and Tax Law Society 
                     
                      President:  Frederick "Freddy" Culver  
                     
                       The Business   Tax Law Society is a network of U of M law students who are pursuing,
                           or are interested in pursuing, the JD/MBA dual degree, Business Certificate, Tax Certificate,
                           and/or dual Business/Tax Certificate. The Society exists to assist in achieving your
                           business and tax law goals by providing peer-to-peer advising, scheduling tips, and
                           general feedback on life as a business/tax law student.  
                      
                     
                      Christian Legal Society President:  Dominique Winfrey  
                     
                      The Christian Legal Society is a non-denominational national organization dedicated
                        to serving Jesus Christ and committed to offering law from a balanced, Christian perspective.
                        Society activities include monthly meetings, guest speakers, and annual barbeques
                        that provide further opportunities for Christian fellowship. For further information,
                        please contact T he Christian Legal Society.   
                     
                      Federal Bar Association President:  Price Rudolph     
                     
                      The Federal Bar Association is a national organization consisting of more than 16,000
                        attorneys and 1,200 federal judges. The student organization works closely with the
                        Memphis Mid-South Chapter to foster a strong relationship between student members
                        and local federal judges and attorneys. We host several speakers on campus and provide
                        a number of networking opportunities with attorneys who practice in the federal system. 
                      
                     
                       Federalist Society for Law and Public Policy Studies  President:  Frederick "Freddy" Culver  
                     
                      The Federalist Society is a group composed of conservatives and libertarians interested
                        in promoting awareness of Federalist principles, including: that the state exists
                        to preserve freedom, that the separation of governmental powers is central to our
                        Constitution, and that it is emphatically the province and duty of the judiciary to
                        say what the law is, not what it should be. Please see The  Federalist Society  for further information. 
                      
                     
                       Health Law Society  President:  Richard Urban  
                     
                      The Health Law Society ("HLS") is dedicated to exploring the intersection between
                        medical health care and the judicial system. The HLS examines not only the traditional
                        areas of health law, but also delves deeper into local and national health policy
                        concerns. The HLS strives to enhance the experience and knowledge of its members and
                        the entire Memphis law community. 
                      
                     
                      Hispanic Law Student Association President:  Esperanza King  
                     
                       HLSA membership is not limited by race or ethnicity. We are an organization geared
                        towards helping students of all races and ethnicities excel throughout law school.
                        Our goal as an organization is to make sure that you succeed in law school academically,
                        culturally and socially. To accomplish this goal, we continually implement programming
                        that addresses the needs of our diverse student body. Whether it is providing mentoring
                        opportunities with the local minority bar association, various social outings, or
                        hosting community service projects, we always have something planned to enrich your
                        experience at the University of Memphis.
                      
                     
                       Honor Council  Chief Justice: Danny Bounds  
                     
                      The legal profession is a self-regulated profession; meaning judges and lawyers determine
                        their own professional standards and enforce them. That tradition of self-regulation
                        starts in the law school with the Honor Code and Honor Council. The Honor Code is
                        a code of professional and academic standards. The Honor Council enforces the Honor
                        Code. The Honor Council is composed of eleven law students elected by the student
                        body. The Honor Council investigates and prosecutes alleged violations of the Honor
                        Code. All first-year law students will take an oath at law school orientation to honor
                        the values reflected in the Code.
                      
                     
                      More detailed information about the Honor Code can be found in the  Academic Regulations  and on the Honor Council homepage here. 
                      
                     
                      International Law Society President:  Waleed Nasar  
                     
                      The International Law Society is committed to educating students and lawyers, from
                        all over, in the principles and purposes of international law, international organizations
                        and institutions, and comparative legal systems. We hope to achieve this by encouraging
                        communication among students and lawyers from different parts of the world, promoting
                        international understanding and cooperation, and by advancing the legal education
                        of members in general. We also strive to provide opportunities for law students and
                        lawyers to learn about other cultures and legal systems in a system of critical dialogue
                        and international cooperation. 
                      
                     
                       The University of Memphis Law Review  Editor-in-Chief: George Scoville  
                     
                      The Law Review is a student publication committed to producing a scholarly, legal
                        journal. All of the articles published in the journal are selected by students and
                        edited by students. The notes and comments selected for publication are also written
                        and edited by students. The goal is to provide a publication that will benefit practitioners,
                        judges, professors, students, and others that use this journal in their practice,
                        on the bench, in the classroom, or in their legal research. 
                      
                     
                       Memphis Law +1  
                     
                      President:  C  hristopher Burt  
                     
                      Memphis Law +1 is an organization that seeks to provide a support system for students
                        that have families; including, but not limited to, children. The definition of the
                        'traditional' student has changed dramatically in the last decade as students in law
                        school often already have families and many times are not coming directly out of undergraduate
                        programs. This presents a unique challenge from time management to setting up a schedule
                        that allows the student to network and create a resume' with greater time constraints
                        that the 'traditional' student. Our organization seeks to create a support system,
                        first and foremost, for the students by creating a network of such students that can,
                        together, make tackling law school more productive. Memphis Law +1 is sponsored by
                        the law school's family law area and specifically by Professor Lynda Black. 
                      
                     
                       Moot Court Board  
                     
                      Chief Justice:  Matt Stombaugh  
                     
                      The Moot Court Board is dedicated to recognizing, coordinating and fostering excellence
                        in both the appellate and trial advocacy. Duties of the Board include advertising,
                        organizing, and coordinating all intraschool competitions. 
                      
                     
                       National Lawyer's Guild  President:  Nathaniel Bishop  
                     
                      The National Lawyers Guild Student Chapter at Memphis Law aspires to facilitate the
                        discussion of many current issues while ensuring that multiple perspectives of a given
                        issue are heard. The NLG is dedicated to the need for basic change in the structure
                        of our political and economic system. We seek to unite the lawyers, law students,
                        legal workers and jailhouse lawyers to function as an effective force in the service
                        of the people, to the end that human rights shall be regarded as more sacred than
                        property interests. Our aim is to bring together all those who recognize the importance
                        of safeguarding and extending the rights of workers, women, LGBTQ people, farmers,
                        people with disabilities and people of color, upon whom the welfare of the entire
                        nation depends; who seek actively to eliminate racism; who work to maintain and protect
                        our civil rights and liberties in the face of persistent attacks upon them; and who
                        look upon the law as an instrument for the protection of the people, rather than for
                        their repression. 
                      
                     
                      Outlaw President:  Megan Keene  
                     
                      OUTLAW is a law student organization geared specifically towards gay, bisexual, lesbian,
                        and trans-gendered legal issues. The Gay-Straight Alliance promotes equality and civil
                        rights while maintaining visibility in the Memphis legal community as a resource for
                        the gay population. For more information, please contact the  Human Rights Campaign website  or the  Memphis Gay and Lesbian Community Center.   
                     
                      Phi Alpha Delta Law Fraternity Chapter Justice:  C  helsea Hinton  
                     
                      With over 300,000 members, Phi Alpha Delta is the nation's largest co-ed professional
                        law fraternity. It exists to promote the welfare of each member as well as the community
                        by fostering lasting relationships between teachers and students of law, promoting
                        the ideals of liberty and equal justice under the law, stimulating excellence in scholarship,
                        inspiring virtues of compassion and courage, and fostering integrity and professional
                        competence. For further information, please visit Phi Alpha Delta Law Fraternity's
                        national website. 
                      
                     
                       Public Action Law Society  President: Matthew Jehl  
                     
                      The Public Action Law Society (PALS) at the University of Memphis is a student-led
                        organization that seeks to promote volunteerism, community service, and a pattern
                        of activities that will instill in participants a desire to continue in pro bono work
                        after becoming attorneys. PALS coordinates volunteers for a number of different organizations.
                        Volunteers are connected to community service organizations that match the students'
                        interests and abilities. 
                      
                     
                      Sports   Entertainment Law Society (SELS) 
                     
                      President:  Richard Vaughan  
                     
                      SELS is a student-run organization dedicated to providing information, career support,
                        and social activity for law students interested in careers within the sports and entertainment
                        industry. We are committed to increasing student exposure to the industry. We plan
                        to arrange guest speakers to provide industry insight and examine topical issue in
                        sports and entertainment law. Through these events, our organization aims to provide
                        a realistic introduction to the entry level sports and entertainment law market for
                        today's law student. For more information, follow @MemphisLaw_SELS on Twitter. 
                      
                     
                      Street Law 
                     
                      President:  J  ordan Alex Anderson  
                     
                      Street Law is a nonprofit organization that began in 1972 at Georgetown University.
                        The program's original intent was to teach District of Columbia high school students
                        about the law and legal system. Today Street Law has active chapters in all 50 states
                        and 40 countries worldwide. The curriculum has evolved into a range of programs and
                        publications designed to teach practical law, crime prevention, conflict resolution,
                        youth advocacy, and the fundamental principles of democracy. The Memphis Law Chapter
                        currently serves two local schools, Soulsville Charter School and Memphis Central
                        High School. Students, from 1Ls to 3Ls, and professors teach at these two schools
                        on a weekly or monthly basis. Our role in the classroom is to engage the students
                        around topics of law, democracy, human rights, and local issues. Most of all, we strive
                        to bridge the gap between today's youth and the law, and to inspire students onto
                        professional careers. 
                      
                     
                       Student Bar Association  President: Hallie Flanagan  
                     
                      The Student Bar Association (SBA) is dedicated to connecting all University of Memphis
                        School of Law students into one body to foster fellowship and cooperation as well
                        as advance the aims and purposes of the School of Law. Duties of the association include
                        creating forums to resolve student issues, plan students activities, and partner with
                        other university departments for the advancement of common interests. All students
                        enrolled in the School of Law are automatically members of the SBA. 
                      
                     
                      Tennessee Association of Criminal Defense Lawyers (Student Chapter) President:   David "Hawk" Allen  
                     
                      The Tennessee Association of Criminal Defense Lawyers (TACDL) at the University of
                        Memphis is the student chapter that focuses on education and support to lawyers representing
                        citizens accused of crime. TACDL members also acts as advocates for a fair and effective
                        criminal justice in the courts, the legislature, and wherever justice demands. TACDL
                        will hold events particularly pertaining to criminal defense, private practice, and
                        indigent clients. TACDL strives to enhance the experience and knowledge of its members
                        and the entire Memphis law community.
                      
                     
                      Additional Information: 
                     
                      
                        
                         In addition to law school student organizations, law students are welcome to be a
                           part of the  University of Memphis Graduate Student Organization.  
                        
                         All law school student organizations must register through  Student Leadership and Involvement  to become a  Registered Student Organization.  
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Student Affairs Office 
                         
                            
                               
                                Academic Support  
                                      Academic Success Program  
                                      Bar Exam Information  
                                      Disability Resources for Students  
                                      Study Abroad  
                                      Student Advising  
                                  
                               
                                Student Support  
                                      Counseling Services  
                                      Fitness  
                                      Housing  
                                      Individual Appointment Info  
                                      Information Technology  
                                      Lactation Room  
                                      Law School ID Cards  
                                      Locker Rentals  
                                      Parking   Transportation  
                                      Print Services  
                                      TN Lawyers Assistance Program  
                                      Health Services  
                                  
                               
                                Student Organizations  
                                Orientation  
                                Commencement  
                            
                         
                      
                      
                      
                         
                            
                                Apply to Memphis Law  
                               Take the first step toward your legal education 
                            
                            
                                ABA Required Disclosures  
                               Information required by ABA Standard 509 Required Disclosures 
                            
                            
                                Alumni   Support  
                               Stay up to date with Memphis Law alumni 
                            
                            
                                Contact Us  
                               Questions about law school? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


