Brad Harrell - The Loewenberg College of Nursing - University of Memphis  Bradley Ray Harrell, DNP, APRN, ACNP-BC Assistant Professor  










 
 
 
     



 
    
    
    Brad Harrell - 
      	The Loewenberg College of Nursing
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			The Loewenberg College of Nursing
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About Us  
                          Programs  
                          Students  
                          Faculty and Staff  
                          Research  
                          Alumni  
                          News  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	The Loewenberg College of Nursing
                              	  
                          
                              	Faculty   Staff
                              	  
                         Brad Harrell 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Bradley Ray Harrell, DNP, APRN, ACNP-BC Assistant Professor Director, MSN and RODP Programs
                      
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  
                                    
                                     
                                       
                                        
                                           Phone:  
                                          
                                           901-678-3100 
                                          
                                        
                                       
                                        
                                           Fax:  
                                          
                                             
                                          
                                        
                                       
                                        
                                           E-mail:  
                                          
                                            bharrell@memphis.edu  
                                          
                                        
                                       
                                        
                                           Office:  
                                          
                                           3525 Community Health Building 
                                          
                                        
                                       
                                     
                                    
                                  
                                 
                               
                              
                                  
                              
                            
                           
                         
                        
                      
                     
                       Teaching:
                      
                     
                      Dr. Brad Harrell is an Assistant Professor of Nursing at Lowenberg College of Nursing.
                        His areas of teaching are related to the acute and chronic care of the teenage and
                        adult populations. Dr. Harrell has teaching experience in graduate and undergraduate
                        nursing programs, including a broad variety of didactic and clinical courses including
                        pathophysiology, pharmacology, health assessment, trends and issues in nursing, nursing
                        leadership, adult health nursing, critical care nursing, advanced practice nursing,
                        gross anatomy, and health care informatics. He has advised several doctor of nursing
                        practice students and has created and taught several masters and doctoral level nursing
                        courses.
                      
                     
                      Scholarship: 
                     
                      His scholarly work includes creation and dissemination of nursing guidelines for the
                        management of patients with abdominal compartment syndrome in a partnership with the
                        World Society of the Abdominal Compartment Syndrome (WSACS). Dr. Harrell's study on
                        abdominal compartment syndrome led to a publication in the Journal of Trauma Nursing.
                        His work with WSACS is still ongoing with an anticipated publication of international
                        nursing practice guidelines in 2016.
                      
                     
                      Service: 
                     
                      Dr. Harrell regularly speaks to local and regional groups on topics in critical care
                        nursing, adult health nursing, advanced nursing practice, and health policy. Dr. Harrell
                        has served in several capacities, including the elected office of President, at the
                        district and state levels within the Tennessee Nurses Association. Nationally, he
                        represented Tennessee at the inaugural American Nurses Association's Membership Assembly.
                        He holds an appointment to the Content Expert Panel for the American Nurses Credentialing
                        Center as one of eight Acute Care Nurse Practitioners in the nation to design, implement,
                        and author the role delineation study for acute care and gerontology nurse practitioners.
                        Lastly, Dr. Harrell's international service is an ongoing relationship with the ten
                        international nurses and Executive Committee members of the WSACS, headquartered in
                        Ghent, Belgium.
                      
                     
                      Practice: 
                     
                      Dr. Harrell practices as a nurse practitioner in a rural health clinic providing care
                        to acute and chronically ill adults. He frequently makes home visits to patients who
                        are unable to get to the clinic setting due to illness or lack of transportation.
                        He also has a business visit practice so that employees of small, rural businesses
                        will have access to high-quality health care that is convenient for them. Dr. Harrell
                        has nursing experience in trauma, critical care, cardiothoracic surgery, burn care,
                        and wound care.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Apply Now  
                               Take the first step to your new career 
                            
                            
                                RN-BSN Program  
                               Learn more about our flexible, accessible and affordable online program 
                            
                            
                                Lambuth Campus Nursing  
                               Earn your degree in Jackson, TN 
                            
                            
                                Contact Us  
                               Our team is here to help 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/12/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


