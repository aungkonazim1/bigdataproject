Referrals - CAS - University of Memphis    










 
 
 
     



 
    
    
    Referrals - 
      	CAS
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			College of Arts   Sciences
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Areas of Study  
                          Research  
                          Scholarships  
                          Students  
                          Resources  
                          News  
                      
                      
                         
                            Advising Links   
                            
                               
                                  
                                   Advising Center Staff   Contact  
                                   Guide for Advisors  
                                   Newsletter for Advisors  
                                   Departments  
                                   Finding Your Advisor  
                                   First Year Students  
                                   Graduation/Degree Certification  
                                   Major and Minor Information  
                                   Major Four Year Plans  
                                   Meeting with your Advisor  
                                   Pre-Professional Advising  
                                   Upper Division Humanities  
                                   Upper Division Social Science  
                                   Withdrawal  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	CAS
                              	  
                          
                              	Advising
                              	  
                         Referrals 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Referrals 
                     
                       Academic Counseling Center  – ACC – Wilder Tower 212, 678-2062 – advising for all new Arts and Sciences first-year
                        students
                      
                     
                       Admissions  – Wilder Tower 101 – (1) to begin the process for TCR (Transfer Credit Request) forms
                        for students planning to take transfer courses elsewhere, (2) to be readmitted:  www.memphis.edu/admissions/readmit.php , and (3) to correct an incorrectly loaded course on the transcript of a transfer
                        student
                      
                     
                       ALEKS Placement  – the math assessment, accessed online through the My Memphis portal on the Student
                        Tab, details regarding course placement and more:  www.memphis.edu/aleks  
                     
                       Center for Athletic Academic Services  – Wilder Tower 600 and 700, 678-2714 - provides academic services for student-athletes,
                         www.memphis.edu/caas  
                     
                       Counseling and Psychological Testing  – Wilder Tower 214, 678-2068 -  www.memphis.edu/cpcc/  
                     
                       Disability Resources Services  – Brister 110, 678-2880 – provides resources for students with disabilities,  www.memphis.edu/drs/  
                     
                       Educational Support Program (ESP)  -  www.memphis.edu/esp/  
                     
                      
                        
                          Academic Enhancement Center (AEC)  - 207 Mitchell Hall- (901) 678-5226
                         
                        
                          Business Learning Center (BLC)  -256 Fogelman Building- (901) 678-3912
                         
                        
                          English Learning Center (ELC)  -225 Patterson Hall- (901) 678-2059
                         
                        
                          Math Learning Center  -143 Dunn Hall- (901) 678-2704
                         
                        
                          Science Learning Center (SLC)  -207 Mitchell Hall- (901) 678-2704
                         
                        
                          Richardson Towers Learning Center (RTLC)  
                        
                          Carpenter Complex Learning Center (CCLC)  
                        
                      
                     
                       Financial Aid Office  – Wilder 103, 678-4825 - to check on specific questions regarding amounts of aid
                        or hours needed; students should check myMemphis for the deadlines to pay or if their
                        aid has paid; or refer to charts sent in a separate e-mail
                      
                     
                       General Education  –The courses that can be used for General Education requirements are listed in the
                        catalog online.
                      
                     
                      
                        
                         For transfer work that might be used for General Education, advisors can use the Transfer
                           Equivalency tables to see if a course is already approved:  http://www.memphis.edu/admissions/transfer/equivalency_table.php  
                        
                         If the course has been approved as General Education but is not pulling into UMdegree,
                           or if you are want to ask if a course can be used for General Education, enter a petition
                           in UMdegree so that it can be considered.
                         
                        
                      
                     
                       Housing  – 678-1641,  www.memphis.edu/reslife/  
                     
                       Parking  – 120 Zach Curlin Parking Garage, 678-2212,  bf.memphis.edu/parking/  
                     
                       Permits  – Refer students to individual departments for permits, but most departments do not
                        issue permits to overload class seats.
                      
                     
                       Pre-Professional Advising  – Students meet with the advisor of their major to select courses, but should also
                        meet with Jessica Kelso, the Pre-Health and Pre-Law advisor, to ensure that they are
                        on track for their chosen field, 678-5454 for appointments:
                      
                     
                      
                        
                          Pre-Health:   www.memphis.edu/cas/pre_health.php  
                        
                          Pre-Law:   www.memphis.edu/cas/pre_law.php  
                        
                      
                     
                       Registrar  – Wilder Tower 003, 678-2810, information for students and faculty/staff regarding
                        registration and related processes:  www.memphis.edu/registrar/index.php  
                     
                       Scholarships  – Wilder Tower 104, 678-3213, much information about scholarships, Hope, service
                        hours,  www.memphis.edu/scholarships/  
                     
                       Undecided Transfer Students  – transfer students who have not selected a major are advised by the Academic Counseling
                        Center in Wilder Tower 212, 901-678-2062, or email  acc@memphis.edu  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Advising Links 
                         
                            
                               
                                Advising Center Staff   Contact  
                                Guide for Advisors  
                                Newsletter for Advisors  
                                Departments  
                                Finding Your Advisor  
                                First Year Students  
                                Graduation/Degree Certification  
                                Major and Minor Information  
                                Major Four Year Plans  
                                Meeting with your Advisor  
                                Pre-Professional Advising  
                                Upper Division Humanities  
                                Upper Division Social Science  
                                Withdrawal  
                            
                         
                      
                      
                      
                         
                            
                                Apply now for an Undergraduate Degree  
                               Pursue your dream, broaden your career choices and gain the confidence you need to
                                 succeed
                               
                            
                            
                                Offering Master's and Doctoral Degrees  
                               Enhance your knowledge, skills and experience 
                            
                            
                                Community Engagement  
                               Teaching and serving the Memphis and Mid-South community through programs and events 
                            
                            
                                Contact Us  
                               Dean's Office Contacts, Advising, Department Chairs... 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


