Department of Sociology - Sociology - University of Memphis    










 
 
 
     



 
    
    
    Department of Sociology - 
      	Sociology
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Sociology
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About Us  
                          People  
                          Research  
                          Undergraduate  
                          Graduate  
                          Resources  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                         
                           	Sociology
                           	
                         
                      
                   
                   
                      
                         
                              
                             
                            
                             
                            
                             
                            
                            
                                
                            
                         
                         
                             
                         
                      
                      
                     		
                     
                     
                      Department of Sociology 
                     
                      Sociology is the study of social relations in groups, institutions and organizations,
                        communities, and entire societies. The Department of Sociology at The University of
                        Memphis offers a broad range of courses at the undergraduate and graduate levels.
                      
                     
                      We are committed to being a dynamic, learning-centered department that promotes greater
                        understanding and analysis of social relations in groups, institutions and organizations,
                        communities, and entire societies. Our contribution is made through cutting-edge research,
                        excellent undergraduate and graduate teaching, exciting applied opportunities, vital
                        service and community outreach, and preparation of students for a wide range of jobs.
                      
                     
                      Why Study Sociology? 
                     
                      One question that students in every field get asked is "What are you going to do with
                        your degree?"  For Sociology students, there are so many possible answers to this
                        question.  Sociology prepares you for a wide variety of careers and the key is to
                        recognize that sociology provides you with the tools to be successful in any career. 
                        With the focus on critical thinking, creativity, problem solving, and research skills,
                        sociology students find themselves well prepared for the workforce.
                      
                     
                      If you would like to know more about careers in sociology, the American Sociological
                        Association provides numerous career resources available online.  Here are just a
                        few:
                      
                     
                      
                        
                          Preparing for a 21st Century Job Hunt with a BA in Sociology  (article)
                         
                        
                          What are they Doing with a Bachelor's Degree in Sociology?  (research report from a 2005 survey of Sociology majors)
                         
                        
                          Video Interviews with Sociologists Explaining How They "Do Sociology"  
                        
                         Careers with an Undergraduate Degree in Sociology (profiles of professionals with
                           Sociology degrees)
                           
                            
                              
                                Emily Larson  - Senior Analyst, Government Accountability Office
                               
                              
                                Marcus Pruitt  - County Employee
                               
                              
                                Steve Ressler  - Founder and CEO, GovLoop.com
                               
                              
                                Linsay Riddle  - High School Teacher
                               
                              
                                Sushama Tilakaratne  - Research Associate
                               
                              
                            
                           
                         
                        
                      
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Community Out of Chaos  
                               "portray the idea of chaos to community in Memphis." 
                            
                            
                                Online B.A.  
                               Want to pursue your college degree and manage your busy lifestyle? Experience online
                                 classes at the UofM.
                               
                            
                            
                                Contact Us  
                               Main Office Contacts and Location 
                            
                         
                      
                      
                      
                        	
                         
                           		
                           
                           
                           
                           		
                            
                              
                               
                                 
                                  
                                     
                                    
                                  
                                 
                                  
                                    
                                  
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     #20 
                                    
                                     AffordableColleges.com 
                                    
                                     Online Bachelor's in Sociology 
                                    
                                  
                                 									 
                                  
                                    
                                     #13 
                                    
                                     BestColleges.com 
                                    
                                     Online Bachelor's in Sociology 
                                    
                                  
                                 									 
                                  
                                    
                                     #11 
                                    
                                     GreatValueColleges.net 
                                    
                                     Online Bachelor's in Sociology 
                                    
                                  
                                 									
                                  
                                    
                                     #13 
                                    
                                     TheBestSchools.org 
                                    
                                     Best Online Bachelor's in Sociology 
                                    
                                  
                                 									
                                 
                               
                              
                            
                           	
                           
                         
                        
                      
                      
                         Follow Us Online 
                         
                              Facebook   
                         
                                         
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/28/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


