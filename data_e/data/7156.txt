Adding linked web addresses | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Adding linked web addresses | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    ePortfolio    »    Using artifacts   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Adding linked web addresses                       
        
        
       
          
     
           Printer-friendly version       
	If you want to include an existing website as an artifact in your ePortfolio, you can reference the address (URL) for the site rather than uploading files.
 

  
		On the My Items page, click  Link  from the Add button.
	 
	 
		Enter the  Name  you want the URL to be stored as.
	 
	 
		Enter the  URL .
	 
	 
		Enter a  Description  of the site or its significance.
	 
	 
		Add any tags you want the artifact to have.
	 
	 
		Click  Save .
	 
      Audience:    Learner      

    
           

                   ‹ Adding reflections 
        
                   up 
        
                   Uploading files › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        ePortfolio  
  
      ePortfolio basics    Using artifacts    Adding reflections    Adding linked web addresses    Uploading files    Creating web documents    Adding audio recordings    Adding form responses    Importing course content    Associating reflections with items    Using learning objectives in ePortfolio      Creating and editing presentations    Creating collections    Managing comments and assessments in ePortfolio    Understanding the basic concepts in sharing    Importing and exporting items    Sharing items within courses    Using form templates    Integrating ePortfolio with Content     Assessing ePortfolio content    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
