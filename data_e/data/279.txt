Apply for Admission - Graduate Admissions - University of Memphis  Apply for Graduate Admission.  










 
 
 
     



 
    
    
    Apply for Admission - 
      	Graduate Admissions
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Graduate Admissions
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Future Students  
                          Readmission  
                          International  
                          School of Law  
                          Undergraduate  
                      
                      
                         
                            Future Students   
                            
                               
                                   Getting Started  
                                         Request Information  
                                         Visit Campus  
                                         Graduate School  
                                         Academic Programs  
                                         Graduate Catalog  
                                     
                                  
                                   Apply for Admission  
                                        
                                         Admission Requirements  
                                        
                                         Admission Procedures  
                                        
                                         Application Checklist  
                                        
                                         Residency Information  
                                        
                                         EVEA  
                                        
                                         Application Deadline  
                                        
                                         Border County Waiver  
                                        
                                         Assistantships  
                                        
                                         Apply Now!  
                                     
                                  
                                   What to Expect  
                                         Acknowledgement  
                                         Application Review  
                                         Admission Decision  
                                     
                                  
                                   After Acceptance  
                                         Registration  
                                         ID Card  
                                         Parking  
                                         Fee Payment  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Graduate Admissions
                              	  
                          
                              	Future Students
                              	  
                         Apply for Admission 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Apply for Graduate Admissions 
                     
                      Thank you for choosing the University of Memphis to pursue your graduate degree! We
                        are happy to review your application and contact you promptly.
                      
                     
                      To Apply 
                     
                      To begin your application, please choose the appropriate application link  (to be used by both domestic and international students)  from the following options:
                      
                     
                       New Applicants:  For those applying to graduate programs for the first time, please go to the   admissions login webpage  .
                      
                     
                       Existing Applicants:  If you are an existing applicant and your login to your existing application looks
                        like   this example  , please contact Graduate Admissions at 901.678.3685 or email  graduateadmissions@memphis.edu  (with "Decision Desk" in the subject line) for assistance with completing your application.
                        If your login does  not  look like the example noted above, please access your application through your initial
                          login page  .
                      
                     
                      The above links will facilitate your electronic application to the Graduate School.
                        The academic department of the program for which you wish to apply may have additional
                        application requirements. Please see their respective application webpages. Links
                        to these departmental webpages can be found in the   Graduate School directory  .
                      
                     
                      Official Transcripts and Test Scores: Please remember to send your official transcripts
                        and test scores directly to the Graduate School using the address below. Test scores
                        can, alternatively, be sent to the Graduate School electronically through your test
                        provider using the following school code: R1459
                      
                     
                      University of Memphis FedEx Institute of Technology Building, Suite 201 Memphis, TN 38152
                      
                     
                      FOR HELP 
                     
                      
                        
                         Your chosen major's academic department ( check the web directory ) can help you to:
                           
                            
                              
                               Obtain more information about admission requirements, test score requirements, application
                                 deadlines, etc.
                               
                              
                               Check the status of your completed application. 
                              
                               Answer questions related to the content of your application. You can also  contact Graduate Admissions .
                               
                              
                            
                           
                         
                        
                         For application navigation questions, or if you are having technical difficulty, please
                            contact Graduate Admissions .
                         
                        
                      
                     
                      NOTES 
                     
                      
                        
                         Applications are available approximately one year before the start of a given term. 
                        
                         Readmission is available to those who previously enrolled at the University of Memphis
                           and are now seeking to be a student once again. To get started, please submit an electronic
                           application to your program of choice, selecting the appropriate "Readmit" application
                           type.
                         
                        
                         Both domestic and international students begin their applications using the same program
                           links above.
                         
                        
                         No financial aid is available for the Non-Degree program. 
                        
                         If you are interested in the Intensive English for Internationals, please  review the program application page.  
                        
                      
                     
                      Message from the Provost 
                     
                       
                              
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Future Students 
                         
                            
                                Getting Started  
                                      Request Information  
                                      Visit Campus  
                                      Graduate School  
                                      Academic Programs  
                                      Graduate Catalog  
                                  
                               
                                Apply for Admission  
                                     
                                      Admission Requirements  
                                     
                                      Admission Procedures  
                                     
                                      Application Checklist  
                                     
                                      Residency Information  
                                     
                                      EVEA  
                                     
                                      Application Deadline  
                                     
                                      Border County Waiver  
                                     
                                      Assistantships  
                                     
                                      Apply Now!  
                                  
                               
                                What to Expect  
                                      Acknowledgement  
                                      Application Review  
                                      Admission Decision  
                                  
                               
                                After Acceptance  
                                      Registration  
                                      ID Card  
                                      Parking  
                                      Fee Payment  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply Now!  
                               Take the first step to your new future 
                            
                            
                                Request Information  
                               Register on the True Blue Future portal 
                            
                            
                                Graduate School  
                               Learn more about grad school programs and opportunities 
                            
                            
                                Contact Us  
                               We're here to help 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


