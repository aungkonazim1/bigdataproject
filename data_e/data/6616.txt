Glossary of Terms - Hypostyle - University of Memphis    










 
 
 
     



 
    
    
    Glossary of Terms - 
      	Hypostyle
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			The Karnak Great Hypostyle Hall Project
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About Hypostyle  
                          Meaning   Function  
                          Reliefs   Inscriptions  
                          The Project  
                      
                      
                         
                            The Project   
                            
                               
                                  
                                   Origins of the Project  
                                   Early Recording and Restoration  
                                   Endangered Monuments  
                                   Epigraphy Methods  
                                   Salvage Epigraphy  
                                   Field Reports  
                                   Glossary  
                                   Bibliography  
                                   Staff Bios  
                                   Reference Links  
                                   Contact  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Hypostyle
                              	  
                          
                              	Project
                              	  
                         Glossary of Terms 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Glossary of Terms  
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                   
                                 
                                  
                                    
                                      Abacus  
                                    
                                     Plural abaci. A square block placed between the capital of a column and an architrave.
                                       In the Hypostyle Hall, these were inscribed on all four faces with the cartouches
                                       of Seti I and Ramesses II.
                                     
                                    
                                      Amen-Ra  
                                    
                                     Originally an obscure local god, Amen rose to national prominence when successive
                                       royal dynasties came from his hometown of Thebes. His identity was merged with that
                                       of the ancient sun god Re. During the New Kingdom he became a solar god and universal
                                       creator.
                                     
                                    
                                      Amenmesse  
                                    
                                     At the death of Merenptah, a usurper took control of the throne from the legitimate
                                       successor Seti II. After a brief reign, he was overthrown by Seti II. His main accomplishments
                                       were to build a tomb for himself in the Valley of the Kings (KV 10) and to erase the
                                       name of Merenptah on monuments in Thebes.
                                     
                                    
                                      Architrave  
                                    
                                     Architraves are large stone beams supported by the columns in the Hypostyle Hall which
                                       once supported roofing slabs. The architraves are inscribed with dedicatory texts
                                       on their sides and the royal names and titles of Ramesses II and Seti I on their undersides
                                       (see under soffit).
                                     
                                    
                                      Battle of Kadesh  
                                    
                                     A battle fought by Ramesses II with the Hittite emperor for control of the strategic
                                       town of Kadesh in modern Syria. It was the last in a series of failed Egyptian offensives
                                       against Kadesh dating back to the reign of Akhenaten. Although he failed to capture
                                       Kadesh, Ramesses widely publicized the campaign as a victory by commemorating it with
                                       a series of war scene panoramas on monuments throughout Egypt. At Kadesh, he ordered
                                       scenes of the battle to be carved on the south wall of the Hypostyle Hall and the
                                       Cour de la Cachette. He soon changed his mind and moved the scenes to another part
                                       of Karnak, leaving a palimpsest of Kadesh on the Hall.
                                     
                                    
                                      Clerestory  
                                    
                                     Architectural feature typical of columned halls in ancient Egyptian temples. It consists
                                       of a series of windows above eye level, usually at the top of the columns closest
                                       to the main axis of the hall. Its purpose was to provide lighting for otherwise dark
                                       enclosed spaces.
                                     
                                    
                                      Cour de la Cachette  
                                    
                                     A large court between the Seventh Pylon and the main axis of the temple so called
                                       because of the hundreds of statues were buried there in the Late Period and discovered
                                       in the early twentieth century. The west wall adjoins the Hypostyle Hall. Ramesses
                                       II carved part of the unfinished Battle of Kadesh scenes there, but later abandoning
                                       the project. He later carved his Hittite Peace Treaty stela there. A later set of
                                       war sceens on either side of the treaty were carved later and are the work of Merenptah.
                                     
                                    
                                      Epigraphy  
                                    
                                     The scientific recording and analysis of monumental inscriptions. The Egyptian tendency
                                       to record their history on their monuments and to alter the latter for political and
                                       ideological reasons makes the study of epigraphy an important facet of Egyptology.
                                       See also under usurpation.
                                     
                                    
                                      Hittite Treaty Stela  
                                    
                                     The Egyptian-Hittite peace treaty of Ramesses II's 21st regnal year was commemorated
                                       with a large stela carved into the west exterior wall of the Cour de la Cachette.
                                     
                                    
                                      Horemheb  
                                    
                                     The last ruler of the Eighteenth Dynasty, he built the Second Pylon but only partially
                                       decorated it before his death. His reliefs were later usurped by Ramesses I and Ramesses
                                       II. With no heir of his own he designated then General Pramessu (the future Ramesses
                                       I) to succeed him.
                                     
                                    
                                      Hypostyle  
                                    
                                     A Greek term for a building with a roof supported by columns. 
                                    
                                      Karnak  
                                    
                                     El-Karnak (the castle) is the Arabic term for the temples of Karnak which was once
                                       thought to be a royal palace by local Egyptians since the Middle Ages. The ancient
                                       name for the temple complex was Ipet-Sewt, "the Most Select Place."
                                     
                                    
                                      Khonsu  
                                    
                                     A moon god who was the son of Amen-Re and Mut. He is often shown as a mummiform figure
                                       with a combination crescent moon and moon disk on his head. His other form is as a
                                       hawk-headed man with the same headdress.
                                     
                                    
                                      Merenptah  
                                    
                                     The son and successor of Ramesses II. He commemorated his exploits on the battlefield
                                       with a series of war scenes on the west wall of the Cour de la Cachette. After his
                                       death, a rival claimant to the throne named Amenmesse denied the rightful heir Seti
                                       II the throne for a few years and erased Merenptah's name on the monuments. Once Seti
                                       II deposed the interloper, he replased the erased name of his father Merenptah on
                                       the monuments with his own.
                                     
                                    
                                      Mut  
                                    
                                     Pronounced Moot, she was the wife of Amen-Re and the mother of Khonsu. She is most
                                       often seen in human form with a vulture cap surmounted by the Egyptian double crown.
                                       She was often seen in the guise of other goddesses including the lion-headed godess
                                       Sakhmet.
                                     
                                    
                                      Palimpsest  
                                    
                                     A term used by papyrologists and Medievalists to describe a document (papyrus or parchment)
                                       which has been recycled by washing off the original text for reuse, but leaving traces
                                       of the original behind. Egyptologists use the term to refer to the erasure of one
                                       set of inscriptions and its replacement by a new one. On the south wall of the Hypostyle
                                       Hall, the erasure of the Battle of Kadesh scenes was finished in plaster leaving many
                                       traces of after the new reliefs were carved over them.
                                     
                                    
                                      Ramesses I  
                                    
                                     Founder of the Nineteenth Dynasty, Ramesses I ruled less than two years. His main
                                       accomplishment was to complete the decoration of the Second Pylon before the construction
                                       of the Hypostyle Hall. He also usurped reliefs of Horemheb on the pylon which must
                                       have been constructed before Ramesses I came to the throne. His name, in turn, was
                                       eventually usurped by his grandson Ramesses II.
                                     
                                    
                                      Ramesses II  
                                    
                                     One of the most famous of Egyptian pharaohs, Ramesses II gave his name to the Ramesside
                                       age and to ten of his successors. Reigning for 67 years into his eighties or nineties,
                                       he built many monuments in Egypt. At the death of his father Seti I, construction
                                       of the Hypostyle Hall was complete, but its reliefs and inscriptions were only partly
                                       finished. Ramesses II completed the decoration and later added his name to part of
                                       the Hall that Seti I had decorated, even replacing Seti's name with his own in some
                                       cases.
                                     
                                    
                                      Seti I  
                                    
                                     As the second king of the Nineteenth Dynasty, Seti launched a series of ambitious
                                       building programs and equally daring wars in the Levant. Like most of his other great
                                       monuments, the Hyposytle Hall was incomplete when the king died after an 11 year reign,
                                       leaving Ramesses II to complete its decoration. Seti completed the decoration of the
                                       north wing of the Hall, including a magnificent panorama of war scenes on the north
                                       exterior wall, but his sculptors had barely started work in the south wing before
                                       he died.
                                     
                                    
                                      Seti II  
                                    
                                     The legitimate successor of his father Merenptah, Seti's rule was challenged by a
                                       usurper king Amenmesse who may have ruled in the south of the country, including Thebes,
                                       for a few years before Seti regained total control of the country. Where Amenmesse
                                       had erased his father's cartouches, Seti II replaced them with his own.
                                     
                                    
                                      Soffit  
                                    
                                     A soffit is the inscribed underside of an architrave. The soffits in the Hypostyle
                                       Hall often retain their original briliant paint having been shielded from sun, wind
                                       and rain for over 3300 years. They are inscribed with the names and titles of Ramesses
                                       II and Seti I.
                                     
                                    
                                      Usurpation  
                                    
                                     The tendency of pharaohs in the New Kingdom to erase the names of their predecessors
                                       on monuments and replace them with their own. Among the unfortunate targets of this
                                       practice are monuments of Queen Hatshepsut and Tutankhamen, both of whom were defamed
                                       in later reigns. In other cases, however, the Ramesside kings usurped the monuments
                                       of their recent predecessors, even their own fathers and grandfathers but with no
                                       sense of malice towards them. So Ramesses I usurped the cartouches of his predecessor
                                       Horemheb on the Second Pylon only to have them usurped in turn by his grandson Ramesses
                                       II who also usurped the name of his father Seti I in some parts of the Hall. There
                                       is no evidence of animosity in these cases.
                                     
                                    
                                  
                                 
                               
                              
                            
                           
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         The Project 
                         
                            
                               
                                Origins of the Project  
                                Early Recording and Restoration  
                                Endangered Monuments  
                                Epigraphy Methods  
                                Salvage Epigraphy  
                                Field Reports  
                                Glossary  
                                Bibliography  
                                Staff Bios  
                                Reference Links  
                                Contact  
                            
                         
                      
                      
                      
                         
                            
                                Welcome to the Karnak Great Hypostyle Hall Project!  
                               In antiquity, Karnak was the largest religious sanctuary in Egypt's imperial capital
                                 of Thebes (modern Luxor) and was home to the god Amun-Re, king of the Egyptian pantheon.
                               
                            
                            
                                About the Architecture  
                               Who designed the Hypostyle Hall and why did they do it? Read about how Sety I and
                                 his architects built this magnificent structure between two pylon gateways which once
                                 served as the front entrance to Karnak Temple. 
                               
                            
                            
                                Tour the Hall  
                               The Hypostyle Hall is thousands of miles away -- but you can tour entire structure
                                 without leaving your seat! Click here to begin a virtual tour of the historic Hypostyle
                                 Hall. 
                               
                            
                            
                                Epigraphy Methods  
                               "Why don't you just take a photograph?" Why traditional photography is not the most
                                 scientifically reliable method of recording.
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


