New Enrollment Services Student Support Center Reaching Out to Students &#8211; The University of Memphis President&#039;s Blog   


 

 
 
 
 


 
 
 
 

 



 New Enrollment Services Student Support Center Reaching Out to Students   The University of Memphis President s Blog 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 



 
 
  
 
 
 

 
 
 
 
			

         
 		
		



 
 
 
 

 

 


  Skip to content  




 

	
	 

		
		 

			
			 

				
						  The University of Memphis President s Blog  

					
			  

		  

		
		 

			 Primary Menu 
			 Menu 

			
			 
				 
					 
				 
					 Search for: 
					 
				 
				 
			 				 
			 

			 

			  
  Home    About the President  
  

		  

		 

		
		
			
				 

			
		
		 

		
	  

	
	 
	 

		
		 

			
			
	
	 

		
		
		 

			
			 New Enrollment Services Student Support Center Reaching Out to Students 
			
			 

				   Author   Jeanine Hornish Rakow     Published on    December 23, 2014     Leave a comment  
			  

			
		  

		
		 

			 The U of M Enrollment Services Student Center is a great example of innovation, efficiency and effectiveness.  Our new Center serves as a central hub for getting answers to prospective and current students concerning admissions, financial aid, scholarship and registration needs. With a small but highly energetic staff of four, headed by John Rhodes, the Center will limit the need for students to travel to multiple university offices to find the answers they need. The Center has already proven highly effective at handling the majority of walk-in traffic and phone activity, freeing up other offices and resources to devote to processing student information.  The Center streamlines the enrollment process, increasing its efficiency and making it more student-friendly.  Initial feedback has been great. The Center, located in 103 Wilder Tower, is open daily and has recently established a web site located at: 
  http://www.memphis.edu/enrollment/  
 Though they just opened this Fall, they are already proving a valuable addition to the Enrollment Services division. 
 “On their own initiative, they compiled a list of all students enrolled for Fall 2014 who have outstanding balances for this term,” reports Betty Huff, Special Assistant for Enrollment Management. “They are contacting the students and attempting to assist them with their financial holds to enable them to be able to register for Spring 2015 prior to leaving for the holidays.  They understand that committed students are more likely to return than students not registered.”  John Rhodes estimates the number of students in this financial position at 311. He and his team will continue to put in place initiatives that will help remove barriers to students completing their degrees. 
 The Center is an important part of making the University of Memphis a friendlier place for students and removing barriers in, what can sometimes appear, an imposing process.  Their initiative deserves our congratulations and a big Thank You! Keep up the great work! 
 Go Tigers! 
  M. David Rudd |   President  

		  

		
		 

			  Published on    December 23, 2014      Author   Jeanine Hornish Rakow   
			
		  

		
	  

	
				
	 
		 Post navigation 
		    Previous article:  Celebrating Success      Next article:  Success at the Heart of Our Mission    
	 
				

 

	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  


			
			
		  

		
	  


	
		
		
		 

		 Main Sidebar 

			
			  
				 
					 Search for: 
					 
				 
				 
			  		 		 Recent Posts 		 
					 
				 UofM Quality Indicator Score 
						 
					 
				 University of Memphis Research Foundation Announces Grand Opening of Student-Operated FedEx Call Center 
						 
					 
				 Pause to Grieve 
						 
					 
				 University of Memphis Magazine: Fall 2017 
						 
					 
				 Campus Update 
						 
				 
		 		  Recent Comments      Archives 		 
			  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  July 2014  
	  June 2014  
	  May 2014  
	  April 2014  
		 
		   Categories 		 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 
			
		  

		
		  

	
	
	 

		
		 Footer Content 

		 
		
			
				
				
								
			
		  
	
		 

			
			
			Using  Tiny Framework     
			
			   Log in  

		  
		
		 

			
			

 

	 Social Links Menu 

	      i class= fa fa-twitter   /i    
  
  

			
		  

		
	  

	
  


        

        









		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 
 