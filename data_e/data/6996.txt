Staff - Marketing and Communication - University of Memphis    










 
 
 
     



 
    
    
    Staff - 
      	Marketing and Communication
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Marketing and Communication
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          The Brand  
                          Our Team  
                          Resources  
                          Social Media  
                          Web  
                          Events  
                          News  
                      
                      
                         
                            Our Team   
                            
                               
                                  
                                   What We Do  
                                   Working Together  
                                   Marketing Manager Assignments  
                                   Project Timelines  
                                   Project Request Form  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Marketing and Communication
                              	  
                          
                              	Our Team
                              	  
                         Staff 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Working Together 
                     
                      The earlier you can involve us in your project, the more we can help. If you have
                        a good idea of what you need but don't have any ideas on how to get there, we can
                        work with you from concept development until the finished product is delivered. If
                        you already know what you need, we still encourage you to work with us with an open
                        mind and/or take advantage of all the resources available in our  Marketing Toolkit .
                      
                     
                      Some crucial elements to think through before beginning any project are as follows: 
                     
                      
                        
                          Goals:  Is the tactic absolutely necessary to achieve your goal? Does it justify the use
                           of resources (yours and ours)?
                         
                        
                          Timing:  In order to best help you, we need the proper time to be able to generate the work.
                            Click here  for planning timelines.
                         
                        
                          Audience:  Identify your target audience/s and how to best reach each group.
                         
                        
                          Materials:  Determine quantities of materials that will be needed.
                         
                        
                          Strategy:  Define what sets your program or offering apart from the competitor.
                         
                        
                          Call to Action:  What action do you want the audience to take?
                         
                        
                          Content:  Do you have the copy/content you intend to use and has it been signed off on by your
                           Dean/Department head?
                         
                        
                          Design Elements:  Do you know what photos, if any you want to use?
                         
                        
                          Budget:  Allocate funds for any production costs if applicable.
                         
                        
                          Effectiveness:  Upon completion, how will you consider the overall effectiveness of your strategy
                           and/or tactics?
                         
                        
                      
                     
                      From this base of information, we can work together to accomplish your goals. During
                        an initial meeting, we'll work closely on building out a production schedule that
                        will consist of the many stages of production, such as delivery of final and approved
                        content, editing, design, estimate requests, delivery of concepts/proofs, pre-press
                        work, and the proposed date.
                      
                     
                      Next Step 
                     
                      Contact your  marketing manager  and complete a  project request form .
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Our Team 
                         
                            
                               
                                What We Do  
                                Working Together  
                                Marketing Manager Assignments  
                                Project Timelines  
                                Project Request Form  
                            
                         
                      
                      
                      
                         
                            
                                Media Resources  
                               News, Publications and Upcoming Events 
                            
                            
                                Brand Standards  
                               Everything you need to know about utilizing the UofM brand 
                            
                            
                                Marketing Toolkit  
                               Download templates, find a vendor and more 
                            
                            
                                Contact Us  
                               Let us help with your marketing needs 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


