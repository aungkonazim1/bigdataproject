Circulation Homepage - Libraries - University of Memphis    










 
 
 
     



 
    
    
    Circulation Homepage  - 
      	Libraries
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			University Libraries
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Services  
                          Resources  
                          Instruction  
                          Special Collections  
                          Technology  
                      
                      
                         
                            Circulation   
                            
                               
                                  
                                   Staff  
                                   Check-out Policies  
                                   Department Hours  
                                   Search/Recall/Hold Form  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Libraries
                              	  
                         
                           	Circulation
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      
                        





  
 
 
 QuickSearch 
 Journal Titles 
 Databases 
 
 
  
 Search for books, articles, media, and more: 
  
     
     
  
  Advanced QuickSearch  |  Classic Catalog  |  Research Guides  |  WorldCat  
 

  
 Find Journals, Newspapers, and Magazines by Title or ISSN: 
	 
	 
	 
	 
	 
	 
	  
	 
	  
	 
	 
 
  Browse Journals  |  Find Full Text from Citation  |  Research Guides  
 
	
   Find a Database Using Title or Keywords:  
   
   
   
   
   
   
       
  
   
 
 
 All Databases  | 
 Library Passwords  | 
 Which Database?  | 
 Access Problems? 
   
	 Most Used Databases:  
 
 
   
 Business Source Premier 
 CINAHL Complete 
 Comm. and Mass Media 
 CQ Press Electronic Library 
 EBSCO Discovery Service (EDS)  Education Full Text 
 JSTOR 
 LexisNexis Academic 
 OmniFile Full Text Mega 
 Opposing Viewpoints 
 Project MUSE 
 PsycINFO 
 
 

 
 
 
 
 
 


                        
                      
                       
                     
                      About 
                     
                      The McWherter Libraries Circulation Department is located on the first floor of the
                        McWherter Library on the north side of the atrium. In addition to answering general
                        information questions, Circulation is your location to check out or return library
                        material---including books, laptop computers, headphones, whiteboard markers, GoPro
                        cameras, Pocket Projectors, calculators, phone chargers, Reserved Material, Government
                        Publications and DVDs. Circulation is also the checkout and return area for Interlibrary
                        Loan materials. A valid U of Memphis ID card is required to pick up or check out any
                        material.
                      
                     
                        
                     
                      U of Memphis undergraduate students have a checkout limit of 35 items 30 day checkout
                        for books from the stacks. U of Memphis graduate students and staff have semester
                        checkout privileges with a limit of 50 items. U of Memphis faculty have an academic
                        year checkout period with a limit of 100 items. Books from the stacks are renewable
                        2 times online with fines assessed at $.25 per day per book for all patrons.
                      
                     
                       Laptop computers with chargers, headphones and markers are checked out to U of Memphis
                        personnel only for 4 hours and may not be renewed online, but may be brought back
                        to the checkout desk to be returned and checked out again. Fines are $11.00 per hour
                        or part of an hour for ($10) laptop and ($1) charger checkout. Laptop and charger
                        checked out together, no exception.
                      
                     
                       GoPro Camera kits may be checked out to U of Memphis personnel for 3 days. Overdue
                        fines are $1.00 an hour or part of an hour. The kits cannot be checked out again upon
                        return, but student can check out a GoPro again after a 24 hour waiting period.
                      
                     
                       The following items may be checked out to U of Memphis personnel for 4 hour periods,
                        are not renewable online and have overdue fines of $1.00 an hour or part of an hour:
                      
                     
                        
                     
                      
                        
                         Firefly Touchscreen kits 
                        
                         Phone chargers, wall adaptors, Apple 30 pins, Apple Lightning cords, 30 pins to HDMI,
                           Apple Lightning to VGA adaptors and Samsung Micro USB to HDMI adaptors
                         
                        
                         Pocket Projectors *now on 3-day check out period* 
                        
                      
                     
                        
                     
                      Memphis residents, U of Memphis Alumni and faculty or students from local universities
                        (hereafter Reciprocal borrowers) may apply for borrowing privileges which allow a
                        30 day checkout period for up to 10 books. Circulation will create a borrowing card
                        for residents and alumni, but registered Reciprocal borrowers may use their university
                        ID to be logged on to a public computer or to check out books. The initial card is
                        complimentary, a replacement for the card is $10.00. Renewal of items are either in
                        person with the books or by telephone (901-678-2205). To renew by telephone, the patron
                        will be asked for each book bar code. Books may be renewed 2 times by telephone but
                        must be brought in to renew thereafter.
                      
                     
                       Memphis high school students may apply to borrow books. If the student is under 18
                        years of age, the application must be signed by the student's parent or legal guardian.
                        Up to 5 books at a time may be checked out for a two week period with two renewals
                        either in person with the borrowed books or by telephone. 
                      
                     
                        
                     
                       The Circulation Department is open from:
                      
                     
                        
                     
                      7:30 a.m. until 11:45 p.m. Monday through Thursday, 7:30 a.m. until 5:45 p.m. on Friday, 10 a.m. until 5:45 p.m. on Saturday 1 p.m. until 9:45 p.m. on Sunday.
                      
                     
                        
                     
                      The Circulation public desk number is (901) 678-2205. The library hours line is (901)
                        678-8240.
                      
                     
                        
                     
                        
                     
                         
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Circulation 
                         
                            
                               
                                Staff  
                                Check-out Policies  
                                Department Hours  
                                Search/Recall/Hold Form  
                            
                         
                      
                      
                      
                         
                            
                                Ask-a-Librarian  
                               Got a question? Got a problem? Ask Us! 
                            
                            
                                Interlibrary Loan  
                               Request books, articles, and other research materials from other libraries. 
                            
                            
                                Reserve space  
                               Group study and presentation spaces available in the library. 
                            
                            
                                My Library Account  
                               Review your library account for due dates and more. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


