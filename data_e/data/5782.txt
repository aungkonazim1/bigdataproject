Frequently Asked Questions - Financial Aid - University of Memphis    










 
 
 
     



 
    
    
    Frequently Asked Questions  - 
      	Financial Aid
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Financial Aid
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Undergraduate  
                          Graduate  
                          Law  
                          Scholarships  
                          Student Employment  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Financial Aid
                              	  
                         Frequently Asked Questions  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Frequently Asked Questions 
                        How do I apply for Financial Aid? 
                     
                       What is the school code for the University of Memphis?   When do I apply?   Why do I have to apply every year?   Who may apply for aid?   How long does it take to be notified if I am awarded?   I'm a Freshman. Will my financial aid cover my tuition and dorm?   When will I get my refund? Will I get all my monies in one semester? Can I get my
                           aid early?   What if I applied for aid but did not submit all my documents on time?   What happens to my financial aid if I drop a class or withdraw completely?   Can I transfer my aid to another school?   Can I get financial aid during the summer?   I don't live with my parents, nor do they claim me as a tax exemption. Why am I not
                           considered independent?   If my parents are divorced, whose income should be reported?   What is the difference between subsidized and unsubsidized loans?   Where do I get my loan deferment form filled out?   When does interest on PLUS loans begin to accrue? When does a PLUS Loan go into repayment?   Can I make payments, even if I'm in school, in a grace period, have a deferment or
                           have a forbearance?   My question wasn't answered - what next?  
                     
                           How do I apply for financial aid?  
                     
                      Complete the  Free Application for Federal Student Aid (FAFSA)  or the Renewal Application electronically using FAFSA on the WEB.
                      
                     
                      Submit all documents as requested by the Student Financial Aid Office (i.e. tax returns,
                        verification forms, etc.).
                      
                     
                      If you have already been admitted, you can use your University account access to check
                          myMemphis   for the most up-to-date information on your financial aid application. Other students
                        should follow up with the Student Financial Aid Office periodically to make sure no
                        additional information is needed.
                      
                     
                      If you listed the University of Memphis on your FAFSA but have decided to attend another
                        school, notify us in order to avoid delays in getting your aid processed elsewhere.
                      
                     
                          What is the school code for the University of Memphis?  
                     
                       003509  is the school code for the University of Memphis.
                      
                     
                           When do I apply for financial aid?  
                     
                      YOU MUST APPLY EVERY YEAR (not every semester). The financial aid year begins in the
                        fall and ends in the summer. Typically you would apply after January 1 for the following
                        fall semester. For priority deadlines, review  WHEN TO APPLY .
                      
                     
                           Why do I have to apply every year?  
                     
                      Financial aid is not automatically renewed. Federal requirements and/or your family's
                        financial situation may change. So you must reapply for aid each year.    Back to Top    
                     
                           Who may apply for financial aid?  
                     
                      Any undergraduate, graduate or law student currently enrolled or who applies for admission
                        to The University of Memphis may apply for financial aid. However, students must be
                        accepted into a degree program with a declared major, be a U.S. citizen or eligible
                        non-citizen; be making satisfactory academic progress and be enrolled at least half-time
                        (in most cases) to receive aid. The number of credit hours required for eligibility
                        is as follows:
                      
                     
                      Half-time - 3/4 time - Full-time 
                     
                      Undergraduate 6 - 9 - 12 (undergraduate credit hours) 
                     
                      Graduate* 5 ---- 9 (graduate credit hours) 
                     
                      Law** 6 ---- 12 
                     
                      *Graduate assistants are required to be enrolled in 9 graduate credit hours to be
                        considered full-time.
                      
                     
                      **For federal aid purposes, half-time is defined as a minimum of 6 law credit hours
                        for the fall and spring semesters. For law students, half-time during the summer is
                        defined as a minimum of 3 law credit hours.
                      
                     
                           How long does it take to be notified if I am awarded?  
                     
                      It really depends on the time of year. If you are submitting documents for fall in
                        January, February or March, award information is usually posted on  myMemphis  beginning in April once all the system upgrades have been put in place. If you are
                        applying for aid when classes are already in session, the award information is usually
                        posted within a week of submitting all your required documents.
                      
                     
                           I'm a Freshman. Will my financial aid cover my tuition and dorm?  
                     
                      Excluding scholarships, the maximum amount of Direct Stafford Loan funds that a Freshman
                        (dependent student) can receive is $5500 per year (3500 subsidized; 2000 unsubsidized).
                        That amount may be above any Federal Pell Grant, state grant or other assistance the
                        student may have received. If that amount is not enough to cover expenses, the student
                        and family are responsible for the difference. The dependent student's parent may
                        apply for a Parent PLUS Loan to offset added expenses. If the parent is denied the
                        PLUS Loan, the parent may request that the student borrow additional unsubsidized
                        Stafford to offset costs (up to the amount the student is eligible for depending on
                        grade level and other aid received).    Back to Top         When will I receive my financial aid refund? Will I get all my financial aid (monies)
                           in one semester? Can I get my aid early?  
                     
                      All financial aid refunds are disbursed through the Bursar's Office. For a disbursement
                        schedule of excess financial aid, log on to the  eBursar  website. Aid will not be released early.
                      
                     
                      Most financial aid is processed based on an academic year (i.e. fall and spring).
                        Aid awards are therefore usually disbursed evenly between the fall and spring semesters.
                        If you plan to graduate in the fall or request that aid be processed for one semester
                        only, then our office will process your award for half of an academic year's eligibility.
                      
                     
                      Federal regulations dictate when your monies can be disbursed. You may check the  eBursar  website for more detailed information regarding the disbursement of financial aid
                        refunds. 
                      
                     
                           What if I applied for financial aid but did not submit all my documents on time?  
                     
                      If you do not have all your financial aid documents in before the fee payment deadline
                        you may apply for the installment plan (see Fee Payment section under the Bursar's
                        website for specific requirements). Please note that even if you applied for financial
                        aid, there is no guarantee that you will be eligible to receive aid once all your
                        documents are in. NOTE: The installment plan is NOT available in the summer.  AVOID LATE FEES!  Students dependent on Financial Aid to pay their fees must adhere to the missing
                        requirement submission deadlines: August 1st - Fall, December 1st - Spring, May 1st
                        - Summer.
                      
                     
                           What happens if I drop a class or withdraw completely?  
                     
                      If you completely withdraw or drop below half-time prior to receiving your aid, or
                        prior to the beginning of classes, you are not eligible for your scheduled aid for
                        that semester. If you drop a class or completely withdraw after receiving your disbursement,
                        any balance of aid for that semester will be canceled. If you have a student loan,
                        any refund due to complete withdrawal or drop in hours will be returned to the appropriate
                        account and credited to your student loan. NOTE: Totally withdrawing prior to attending
                        60% of the academic term results in a calculation of "unearned financial aid" that
                        MUST be repaid. 100% of financial aid is not earned until the student has attended
                        at least 60% of the academic term.    Back to Top    
                     
                           Can I transfer my financial aid to another school?  
                     
                      If you transfer from one institution to another within an academic year, you should
                        be able to receive aid at your new institution. Due to availability and other factors
                        such as cost, you may not be able to receive the same amount(s) and type(s) of aid.
                        Even though you do NOT need to fill out the FAFSA again, you do need to contact their
                        Central Processing Center (i.e. 1-800-433-3243) to have the results sent to your new
                        institution. You may need to supply other documents to the new institution, so be
                        sure to contact their Financial Aid Office.
                      
                     
                           Can I get financial aid during the Summer?  
                     
                      It depends on what aid you already received during the academic year. If you transfer
                        from another institution and received aid there during the academic year (i.e. fall
                        and/or spring), we would need to consider what aid you already received there. If
                        you are unsure what your summer aid eligibility may be, you can submit a Summer Update
                        Form, which is available on the  Financial Aid Forms  page in March each year.
                      
                     
                           I don't live with my parents, nor do they claim me as a tax exemption. Why am I not
                           considered independent?  
                     
                      The federal guidelines determine dependency status. When you complete the FAFSA, you
                        will be asked specific questions which determine your dependency status.    Back to Top    
                     
                           If my parents are divorced, whose income should be reported?  
                     
                      Household and income information of the parent (and stepparent if any) whom you resided
                        with more than half of the time during the last 12 months should be reported on the
                        FAFSA.
                      
                     
                           What is the difference between Subsidized   Unsubsidized Loans?  
                     
                      The federal government does not charge interest on a Subsidized Loan while the borrower
                        is enrolled at least half-time, during a six-month grace period, or during authorized
                        periods of deferment. However, interest is charged throughout the life of an Unsubsidized
                        Loan, and it starts to accrue on the date of disbursement.
                      
                     
                           Where do I get my loan deferment form filled out?  
                     
                      All loan deferment forms should be taken directly to the Registrar's Office in 003
                        Wilder Tower. This office is responsible for enrollment certification.    Back to Top    
                     
                           When does interest on PLUS Loans begin to accrue? When does a PLUS Loan go into repayment?  
                     
                      Because a PLUS Loan is a type of Unsubsidized Loan, interest on a PLUS Loan starts
                        to accrue the day the loan is disbursed. Even though the account may not be in repayment,
                        interest is still accruing.
                      
                     
                      PLUS Loans enter repayment on the date of the last disbursement on the loan, and the
                        first payment is due within the next 60 days. PLUS Loans do not have a grace period.
                      
                     
                           Can I make payments, even if I’m in school, in grace, have a deferment or have a
                           forbearance?  
                     
                      Payments can be made at ANYTIME. If you are in school, or your loan is in a grace
                        period, deferment, or forbearance, you will not receive monthly bills, but checks
                        or money orders can be made payable to the U.S. Department of Education. Please include
                        the borrower’s Social Security Number on the check or money order and mail it to:
                      
                     
                      U.S. Department of Education  Direct Loan Payment Center  P.O. Box 746000  Atlanta, GA 30374-6000
                      
                     
                           My question wasn't answered - what next?  
                     
                      Go to  AskTOM  for a wide range of questions and answers relating to financial aid.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Financial Aid Forms  
                               Manage your forms in myMemphis 
                            
                            
                                Financial Aid Checklist  
                               Step-by-step guide to receiving financial aid 
                            
                            
                                Frequently Asked Questions  
                               Answers to the most common questions 
                            
                            
                                Contact Us  
                               Phone: 901.678.4825  |  financialaid@memphis.edu 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


