North Wall East Half - Hypostyle - University of Memphis    










 
 
 
     



 
    
    
    North Wall East Half - 
      	Hypostyle
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			The Karnak Great Hypostyle Hall Project
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About Hypostyle  
                          Meaning   Function  
                          Reliefs   Inscriptions  
                          The Project  
                      
                      
                         
                            Interior Scenes Galleries   
                            
                               
                                  
                                   Reliefs and Inscriptions  
                                   West Wall South Half  
                                         Plates 1-10  
                                         Plates 11-20  
                                         Plates 21-30  
                                         Plates 31-41  
                                     
                                  
                                   South Wall West Half  
                                         Plates 42-53  
                                     
                                  
                                   South Gate Interior  
                                         Plates 54-87  
                                     
                                  
                                   South Wall East Half  
                                         Plates 62-72  
                                         Plates 73-75  
                                         Plate 76  
                                         Plates 77-86  
                                     
                                  
                                   East Wall South Half  
                                         Plates 88-99  
                                         Plates 100-109  
                                     
                                  
                                   Vestibule North Wing  
                                         Plates 233-243  
                                         Plates 244-254  
                                         Plates 255-257, 265  
                                     
                                  
                                   Vestibule South Wing  
                                         Plates 110-118  
                                         Plates 120-129  
                                     
                                  
                                   East Wall North Half  
                                         Plates 202-210  
                                         Plates 211-220  
                                         Plates 221-232  
                                     
                                  
                                   North Wall East Half  
                                   North Wall Gateway  
                                         Plates 183-187  
                                     
                                  
                                   North Wall West Half  
                                         Plates 171-175  
                                         Plates 176-180  
                                     
                                  
                                   West Wall North Half  
                                         Plates 131-140  
                                         Plates 141-150  
                                         Plates 151-160  
                                         Plates 161-170  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Hypostyle
                              	  
                          
                              	Interior Scenes
                              	  
                         North Wall East Half 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      North Wall East Half  
                     
                      
                        
                         
                           
                            
                              
                                
                              
                               
                                 
                                  Plan of the North Wall East Half 
                                 
                                  Note: in the headings below, B + number refers to the location number in  Harold H. Nelson,  Key Plans Showing Locations of Theban Temple Decorations  , (Chicago: Oriental Institute, 1941), plate 4, figure 10.
                                  
                                 
                                  Plate + number refers to the plate numbers in:  Harold H. Nelson, William J. Murnane (editor),  The Great Hypostyle Hall at Karnak , Volume I, Part I,  The Wall Reliefs , Oriental Institute Publications volume 106 , (Chicago: Oriental Institute, 1981).
                                  
                                 
                                  General Views 
                                 
                                  
                                    
                                     
                                       
                                        
                                          
                                               
                                          
                                               
                                          
                                               
                                          
                                        
                                       
                                        
                                          
                                            Plate 189 (B282)  
                                          
                                            Plate 190 (B283)  
                                          
                                            Plate 191 (B284)  
                                          
                                        
                                       
                                        
                                          
                                               
                                          
                                               
                                          
                                               
                                          
                                        
                                       
                                        
                                          
                                            Plate 192 (B285-286)  
                                          
                                            Plate 193 (B287)  
                                          
                                            Plate 194 (B288)  
                                          
                                        
                                       
                                        
                                          
                                               
                                          
                                               
                                          
                                               
                                          
                                        
                                       
                                        
                                          
                                            Plate 195 (289)  
                                          
                                            Plate 196 (B290)  
                                          
                                            Plate 197 (B291,292)  
                                          
                                        
                                       
                                        
                                          
                                               
                                          
                                               
                                          
                                               
                                          
                                        
                                       
                                        
                                          
                                            Plate 198 (B293)  
                                          
                                            Plate 199 (B294)  
                                          
                                            Plate 200 (B295)  
                                          
                                        
                                       
                                        
                                          
                                               
                                          
                                             
                                          
                                             
                                          
                                        
                                       
                                        
                                          
                                            Plate 201 (B296-297)  
                                          
                                             
                                          
                                             
                                          
                                        
                                       
                                     
                                    
                                  
                                 
                                   Back to top.  
                                 
                               
                              
                            
                           
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Interior Scenes Galleries 
                         
                            
                               
                                Reliefs and Inscriptions  
                                West Wall South Half  
                                      Plates 1-10  
                                      Plates 11-20  
                                      Plates 21-30  
                                      Plates 31-41  
                                  
                               
                                South Wall West Half  
                                      Plates 42-53  
                                  
                               
                                South Gate Interior  
                                      Plates 54-87  
                                  
                               
                                South Wall East Half  
                                      Plates 62-72  
                                      Plates 73-75  
                                      Plate 76  
                                      Plates 77-86  
                                  
                               
                                East Wall South Half  
                                      Plates 88-99  
                                      Plates 100-109  
                                  
                               
                                Vestibule North Wing  
                                      Plates 233-243  
                                      Plates 244-254  
                                      Plates 255-257, 265  
                                  
                               
                                Vestibule South Wing  
                                      Plates 110-118  
                                      Plates 120-129  
                                  
                               
                                East Wall North Half  
                                      Plates 202-210  
                                      Plates 211-220  
                                      Plates 221-232  
                                  
                               
                                North Wall East Half  
                                North Wall Gateway  
                                      Plates 183-187  
                                  
                               
                                North Wall West Half  
                                      Plates 171-175  
                                      Plates 176-180  
                                  
                               
                                West Wall North Half  
                                      Plates 131-140  
                                      Plates 141-150  
                                      Plates 151-160  
                                      Plates 161-170  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Welcome to the Karnak Great Hypostyle Hall Project!  
                               In antiquity, Karnak was the largest religious sanctuary in Egypt's imperial capital
                                 of Thebes (modern Luxor) and was home to the god Amun-Re, king of the Egyptian pantheon.
                               
                            
                            
                                About the Architecture  
                               Who designed the Hypostyle Hall and why did they do it? Read about how Sety I and
                                 his architects built this magnificent structure between two pylon gateways which once
                                 served as the front entrance to Karnak Temple. 
                               
                            
                            
                                Tour the Hall  
                               The Hypostyle Hall is thousands of miles away -- but you can tour entire structure
                                 without leaving your seat! Click here to begin a virtual tour of the historic Hypostyle
                                 Hall. 
                               
                            
                            
                                Epigraphy Methods  
                               "Why don't you just take a photograph?" Why traditional photography is not the most
                                 scientifically reliable method of recording.
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


