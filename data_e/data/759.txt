Contact Directory - UofM Lambuth - University of Memphis    










 
 
 
     



 
    
    
    Contact Directory - 
      	UofM Lambuth
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			University of Memphis Lambuth
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Admissions  
                          Programs  
                          Campus Life  
                          Resources  
                          News  
                          Contact Us  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	UofM Lambuth
                              	  
                          
                              	Contact Us
                              	  
                         Contact Directory 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Directory 
                     
                       A    B    C    D    E    F    G    H   I   J    K    L    M   N   O    P   Q   R    S    T   U  V   W   X  Y  Z
                      
                     
                       Sheila Arnold , Writing Center    731.425.1929, 901.678.1929 - Student Union, 004-J
                      
                     
                       Dr. Joy Austin , Faculty 731.425.1928, 901.678.1928 - Varnell-Jones 207-E
                      
                     
                       Dr. Karen Baker , Faculty    731.425.1926, 901.678.1926 - Varnell-Jones, 201-A
                      
                     
                       Dr. Michelle Baldwin , Faculty 731.425.1975, 901.678.1975 - Hyde Hall, 321-B
                      
                     
                       Bookstore  731.425.1958, 901.678.1958 - Student Union, Main Floor
                      
                     
                       Dr. Cheryl Bowers , Faculty 731.425.1925, 901.678.1925 - Varnell-Jones, 201-B
                      
                     
                       Dr. Forrest Brem , Faculty 731.425.1970, 901.678.1970 - Hyde Hall, 121-A
                      
                     
                       Dr. Linda Brice , Faculty 731.425.1902, 901.678.1902 - Varnell-Jones, 121
                      
                     
                       Business Service Center  731.425.7317, 901.678.7317 - Varnell-Jones, 109
                      
                     
                       Dr. Diane Butler , Faculty 731.425.1975, 901.678.1975 - Hyde Hall, 117-B
                      
                     
                       Cafeteria      731.425.1976, 901.678.1976 - Student Union Cafe
                      
                     
                       Campus Security  731.425.1942, 731.412.1934 - Carney Johnston Hall, 8
                      
                     
                      Carney-Johnston Hall 731.425.7347, 901.678.7347 - Front Desk  Career Services 731.425.7398, 901.678.7398 - Student Union, 004-H
                      
                     
                       Ann Childs , Vice Provost's Office 731.425.1988, 901.678.1988 - Varnell-Jones, 101
                      
                     
                       Tori Cliff , Faculty 731.425.1908, 901.678.1908 - Varnell-Jones, 206
                      
                     
                       Katie Cochran , Residence Life 731.425.7349, 901.678.7349 - Student Union, 002-B
                      
                     
                       Dr. Rebecca Cook , Faculty 731.425.1920, 901.678.1920 - Hyde Hall, 112-A
                      
                     
                       Dr. Annette Cornelius , Faculty 731.425.7993, 901.678.5093 - Varnell-Jones, 221
                      
                     
                       Julie Cupples , Faculty 731.425.1975, 901.678.1975 - Hyde Hall, 115-A
                      
                     
                       Dorcas Davis , Library     731.425.1983, 901.678.1983 - Gobbell Library, Main Floor
                      
                     
                       Candy Donald , Vice Provost's Office 731.425.1936, 901.678.1936 - Varnell-Jones, 100-A
                      
                     
                       Jeff Ealy , Police Services     731.425.7336, 901.678.7336 - Carney Johnston, 012-A
                      
                     
                       Dawn Englert , Faculty 731.425.1966, 901.678.1966 - Varnell-Jones 126-E
                      
                     
                       Cynthia Evans , LCON 731.425.1975, 901.678.1975 - Hyde Hall 115
                      
                     
                      Fax for Administration Suite     731.425.1916, 901.678.1916 - Varnell-Jones 104-B
                      
                     
                      Fax for Business Service Center 731.425.7989, 901.678.5089 - Varnell-Jones, 109
                      
                     
                      Fax for Career Services 731.425.7382, 901.678.7382 - Student Union, 004-H
                      
                     
                      Fax for Nursing Program (LCON) 731.425.1995, 901.678.1955 - Hyde Hall, 115-117
                      
                     
                      Fax for PBIS Grant 731.425.1956, 901.678.1956 - Varnell-Jones, 217
                      
                     
                      Fax for Police Services 731.425.7352, 901.678.7352 - Carney Johnston, LL
                      
                     
                      Fax for Student/Enrollment Services 731.425.1917, 901.678.1917 - Varnell-Jones, LL
                      
                     
                      Fitness Studio 731.425.1979, 901.678.1979 - WHFB, 104
                      
                     
                       Dr. Nela Florendo , Fulbright Scholar Faculty 731.425.1931, 901.678.1931 - Varnell-Jones, 304
                      
                     
                       Hal Freeman , Faculty 731.425.1907, 901.678.1907 - Varnell-Jones, 313
                      
                     
                       Kathy Gardner , Business Service Center     731.425.7317, 901.678.7317 - Varnell-Jones, 109
                      
                     
                       Lt. Col. Billy Garrett , Police Services 731.425.1991, 901.678.1991 - Carney Johnston, 002
                      
                     
                       Dr. Manning Garrett , Faculty 731.425.1935, 901.678.1935 - Varnell-Jones, 319
                      
                     
                      GCA Services,  Cleaning Service 731.425.1943, 901.678.1943 - Spangler Hall, Lobby
                      
                     
                       Jennie Gibson , ICL Grants 731.425.1993, 901.678.1993 - Varnell-Jones, 219-A
                      
                     
                       Audrey Glenn , Bookstore 731.425.1958, 901.678.1958 - Student Union, Main Floor
                      
                     
                       Dr. Denis Grimes , Faculty 731.425.1952, 901.678.1952 - Varnell-Jones, 214-B
                      
                     
                       Lauren Halliburton , Admissions     731.425.1950, 901.678.1950 - Varnell-Jones, LL006
                      
                     
                       Jamilah Harris , Business Service Center 731.425.1901, 901.678.1901 - Varnell-Jones, 109-A
                      
                     
                       Cyndi Hill , Faculty 731.425.1905, 901.678.1905 - Varnell-Jones, 309
                      
                     
                       Dr. Gray Hilmerson , Faculty 731.425.1924, 901.678.1924 - Varnell-Jones, 207-B
                      
                     
                       Dr. Donald Hopper , Faculty 731.425.1967, 901.678.1967 - Varnell-Jones, 320-A
                      
                     
                       Adam Johnson , Admissions     731.425.1964, 901.678.1964 - Varnell-Jones, LL-004
                      
                     
                       Dr. Jermaine Johnson , Faculty 731.425.7312, 901.678.7312 - Hyde Hall, 219-A
                      
                     
                       Amber Jones , Receptionist 731.427.4725, 901.678.5087 - Varnell-Jones, Lobby
                      
                     
                       Dr. Torre Kelley , Faculty     731.425.7967, 901.678.5067 - Varnell-Jones, 219
                      
                     
                       Dr. Charles Kubicek , Faculty 731.425.7316, 901.678.7316 - Hyde Hall, 212-A
                      
                     
                      Library     731.425.1918, 901.678.1918 - Library Main Floor
                      
                     
                      Loewenberg College of Nursing, Receptionist 731.425.1975, 901.678.1975 - Hyde, 115-117
                      
                     
                       Dr. Joseph Londino , Faculty 731.425.7370, 901.678.7370 - Hyde Hall, 211-A
                      
                     
                       Joe Marrow , Police Services     731.425.1974, 901.678.1974 - Carney Johnston, 001
                      
                     
                       Laurie Matlock , Aramark 731.425.1976, 901.678.1976 - Student Union, Cafe
                      
                     
                       Pam McCarty , Faculty 731.425.1930, 901.678.1930 - Varnell-Jones, 220
                      
                     
                       Tammy McCoy , Advising   Student Services 731.425.1906, 901.678.1906 - Varnell-Jones, 126-A
                      
                     
                       Dr. Rosemary McLaughlin , Faculty 731.425.1975, 901.678.1975 - Hyde Hall, 318
                      
                     
                       George Megelsh , Faculty 731.425.1922, 901.678.1922 - Varnell-Jones, 200-D
                      
                     
                       Dr. Paul Mego , Faculty 731.425.1923, 901.678.1923 - Varnell-Jones, 200-C
                      
                     
                       Dr. Kelly Mollica ,  Faculty 731.425.7960, 901.678.5010 - Varnell-Jones, 302
                      
                     
                       Renee Morris , Faculty 731.425.1975, 901.678.1975 - Hyde Hall, 117-A
                      
                     
                       Veronica Morrow , Faculty 731.425.7376, 901.678.7376 - Varnell-Jones, 210
                      
                     
                       Dr. Christina Moss , Faculty 731.425.1932, 901.678.1932 - Varnell-Jones, 310
                      
                     
                       Jonathan Orr , Faculty     731.425.1921, 901.678.1921 - Varnell-Jones, 200-A
                      
                     
                       Dr. Linda Page , Faculty     731.425.7994, 901.678.5094 - Varnell-Jones, 222
                      
                     
                       Tony Pearson , IT 731.425.1939, 901.678.1939 - Hyde Hall, 320
                      
                     
                      Police Services 731.425.1974, 731.412.9413 - Carney Johnston
                      
                     
                      Police Services Lobby 731.425.1946, 901.678.1946 - Carney Johnston
                      
                     
                      Police Services Conference Room 731.425.1909, 901.678.1909 - Carney Johnston, 005
                      
                     
                      Police Services Squad Room 731.425.1974, 901.678.1974 - Carney Johnston, 001
                      
                     
                      Police Services Chief/Deputy Chief 731.425.7341, 901.678.7341 - Carney Johnston, 004
                      
                     
                       Dr. Robin Rash , Faculty     731.425.1934, 901.678.1934 - Varnell-Jones, 312
                      
                     
                       Michelle Reddick , Career Services 731.425.7398, 901.678.7398 - Student Union, 4-H
                      
                     
                       Dr. Niles Reddick , Vice Provost 731.425.1936, 901.678.1936 - Varnell-Jones, 100
                      
                     
                       Dr. Martha Robinson , Faculty 731.425.7961, 901.678.5211 - Varnell-Jones, 307
                      
                     
                       Christy Schrotberger , Faculty     731.425.1969, 901.678.1969 - Hyde Hall, 115-B
                      
                     
                       ArQuetta Shirley , Enrollment Office 731.425.1904, 901.678.1904 - Varnell-Jones, LL Lobby
                      
                     
                       Beth Ann Simpson , Student Life   Activities 731.425.1951, 901.678.1951 - Student Union, 002-A
                      
                     
                       Anna Marie Smith , Enrollment 731.425.7982, 901.678.5082 - Varnell-Jones, 002
                      
                     
                       Antoinette Smith , Testing 731.425.1910, 901.678.1910 - Hyde Hall, 314
                      
                     
                       Jeremy Smith , Police Services 731.425.1974, 901.678.1974 - Carney Johnston, 001
                      
                     
                       Tommy Smith , Police Services 731.425.1974, 901.678.1974 - Carney Johnston, 001
                      
                     
                       Carolyn Stark , Lambuth Linx Grants 731.425.1955, 901.678.1955 - Varnell-Jones, 217
                      
                     
                       Debbie Sudduth , Faculty 731.425.1975, 901.678.1975 - Hyde Hall, 312-A
                      
                     
                      Student Worker Desk, Student Services 731.425.7350, 901.678.7350 - Student Union, Lobby
                      
                     
                      Switchboard/Receptionist 731.427.4725, 901.678.5087 - Varnell-Jones, Lobby
                      
                     
                      TAF Lab     731.425.1963, 901.678.1963 - Student Union, 006
                      
                     
                      Testing Center 731.425.1910, 901.678.1910 - Hyde Hall, 314
                      
                     
                       Dr. Jeremy Tubbs , Faculty 731.425.1919, 901.678.1919 - HPAC
                      
                     
                       Lisa Warmath , Vice Provost's Office     731.425.1903, 901.678.1903 - Varnell-Jones, 108-B
                      
                     
                       Wellness, Health,   Fitness Building Information Desk  731.425.7329, 901.678.7329 - WHFB 104
                      
                     
                      Wellness, Health,   Fitness Building Pool Deck 731.425.1948, 901.678.1948 - WHFB
                      
                     
                       Rex West , Library 731.425.7351, 901.678.7351 - Gobbell Library, Main Floor
                      
                     
                       Dr. Elizabeth Weston , Faculty 731.425.1927, 901.678.1927 - Varnell-Jones, 207-C
                      
                     
                       James Wilkinson , Recreation 731.425.7328, 901.678.7328 - WHFB, 201-B
                      
                     
                       Debbie Williams , ICL Grants 731.425.1955, 901.678.1955 - Varnell-Jones, 217
                      
                     
                       Gary Williams , Physical Plant 731.425.1912, 901.678.1912 - Physical Plant Bldg
                      
                     
                       Holley Wood , Vice Provost's Office 731.425.7368, 901.678.7368 - Varnell-Jones, 108-A
                      
                     
                       Jackie Wood , Library 731.425.1957, 901.678.1957 - Gobbell Library, Main Floor
                      
                     
                       Dr. Kathy O'Connor Wray , Faculty 731.425.1975, 901.678.1975 - Hyde Hall, 321-A
                      
                     
                       Jacoby Wright , Police Services 731.425.1974, 901.678.1974 - Carney Johnston, 001
                      
                     
                      Writing Lab 731.425.1929, 901.678.1929 - Student Union, 004-J
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Apply Now  
                               Take the first step toward your new future 
                            
                            
                                Lambuth Academy  
                               High School Students: Learn more about dual enrollment 
                            
                            
                                News   Events  
                               See what's happening at Lambuth 
                            
                            
                                Contact Us  
                               Questions? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/27/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


