Exportable components | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Exportable components | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Course Administration    »    Managing course components   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Exportable components                       
        
        
       
        
     
             
 
	The following components can be exported:
 

  
		Dropbox
	 
	 
		Calendar
	 
	 
		Checklists
	 
	 
		Competencies
	 
	 
		Content
	 
	 
		Course files
	 
	 
		Discussions
	 
	 
		External links
	 
	 
		FAQ
	 
	 
		Glossary
	 
	 
		Grades (except calculated grade items)
	 
	 
		Navbar templates (not navbars)
	 
	 
		News
	 
	 
		Question library
	 
	 
		Quizzes
	 
	 
		Rubrics
	 
	 
		Self assessments
	 
	 
		Surveys
	 
      Audience:     Instructor       

    
           

                   ‹ Exporting course components 
        
                   up 
        
                   Understanding IMS Common Cartridge › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Course Administration  
  
      Course Administration basics    Managing course components    Copying course components between org units    Course import compatibility    Importing course components from a file    Exporting course components    Exportable components      Understanding IMS Common Cartridge    Understanding Mobile Brand Administration    
                  
           
         

       
     

    
    
    
   
 
   
 
