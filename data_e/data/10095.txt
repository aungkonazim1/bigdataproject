The University Libraries Celebrates Women&#8217;s History Month 2015 | University of Memphis Libraries   
 
 
 
 
 The University Libraries Celebrates Women s History Month 2015 | University of Memphis Libraries 
 
 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 
 




 
 
  
 
 
 

 
 
 
 
			

         



 

 
 
	 Skip to content 
		 
				 
						  University of Memphis Libraries  
			 News and Events 
		 

		 
			 Menu 
			  
  Home    
		  
	  

	 
			 
			 Home  The University Libraries Celebrates Women s History Month 2015 		 
		 
		 

					 
				
 
	 
					 The University Libraries Celebrates Women s History Month 2015 
		
		 
						   March 2, 2015       jschnbel   
			  Events ,  Exhibition ,  Homepage ,  News ,  Special Collections ,  Uncategorized  		  
	  

	 
		               
 The University Libraries is pleased to participate in the U of M’s campus-wide celebration of Women’s History Month. See the  full calendar of events  here and visit the Libraries   Women s and Gender Studies research guide  for books, articles, web sites, videos, primary documents, and other resources about women s history and feminist issues. In addition, we have created a  special research guide featuring Tennessee women  who dedicated their lives to activism and service and participated in national movements for equality. 
 In honor of our new exhibition, “Woven Into Words: Tennessee Women Making History,” we will host a reception on  Tuesday, March 3, 5:30-7:00 p.m.  on the fourth floor of McWherter Library; Dr. Christine Eisel, Department of History, will share “Lessons Learned in the Archives.” Guests can explore several display cases which illustrate the impact of women like Roberta Church, Elizabeth Meriwether, Sister Hughetta Snowden, Cornelia Crenshaw, and Maxine Smith and highlight government documents relating to women’s suffrage and political history. Presented with the support of the Friends of the University Libraries. 
 Then, on  Wednesday, March 18, 11:30 a.m. , Jazmin Miller will present her original one-woman show, “The Journey of Truth,” about the life of abolitionist and activist Sojourner Truth in the rotunda of McWherter Library. Free and open to the public. 
 In addition to attending programs, you can participate in Women s History Month by responding to a writing prompt and/or  recognizing a woman who has helped you weave your own story  (submission sheets are also available in rotunda of McWherter Library). 
     

 
			  

	 
			  
  
			 

				 
		 Post navigation 

	
		      New at the Libraries!  		  The University Libraries Hosts Faculty Scholarship Week, April 13   17      
	
	  
	
			
 

	
	
	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  

		
		  
	  

					 
					 		 Recent Posts 		 
					 
				 3D Printing Workshop 
						 
					 
				 NedXStudents 
						 
					 
				 Craft-n-Chat 
						 
					 
				 Bridging East and West: The First Steel Bridge of Memphis 
						 
					 
				 Banned Books Display 
						 
				 
		 		  Archives 		 
			  November 2017  
	  October 2017  
	  September 2017  
	  August 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  April 2016  
	  February 2016  
	  September 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  May 2014  
	  April 2014  
	  March 2014  
	  February 2014  
	  January 2014  
		 
		   Categories 		 
	  Alerts 
 
	  Branch Libraries 
 
	  Events 
 
	  Exhibition 
 
	  Homepage 
 
	  News 
 
	  Research and Instructional Services 
 
	  Special Collections 
 
	  Trial Resources 
 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 		  
	
	  

	 
		 
			 
								 Proudly powered by WordPress 
				  |  
				Theme: Big Brother by  WordPress.com .			  
					  
	  
  


 

        

        			 
				   Subscribe  
				 

					
						 Follow this blog 

						 
							
															 Get every new post delivered right to your inbox. 
							
							 
								 
							 
							
							 
							 
							
							  							   
						 

					
				 
			 
		







		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 