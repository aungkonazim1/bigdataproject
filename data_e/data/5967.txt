Learning Support - ACC - University of Memphis    










 
 
 
     



 
    
    
    Learning Support - 
      	ACC
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Academic Counseling Center
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Staff  
                          Prepare for Advising  
                          Pre-Nursing  
                          Tips  
                          Learning Support  
                          ALEKS  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	ACC
                              	  
                         
                           	Learning Support
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Learning Support 
                     
                      Learning Support at The University of Memphis permits students to enroll in prescribed
                        courses to develop the academic competencies necessary for success in college level
                        courses. Major components of the program include testing and placement in appropriate
                        level courses.
                      
                     
                      Placement 
                     
                      Students admitted to The University of Memphis as beginning freshmen will be placed
                        in appropriate English, math, and reading, courses based on ACT subscores that are
                        less than 5 years old.
                      
                     
                      
                        
                         Students with ACT reading subscores below 19 are required to enroll in ACAD 1100 
                        
                         Students with ACT English subscores below 18 are required to enroll in prescribed
                           English courses
                         
                        
                      
                     
                      If students feel they have been misplaced by their ACT subscores in English and reading,
                        they may take the  ACCUPLACER  test to attempt to place in a higher level course.
                      
                     
                      
                        
                         All students, regardless of their ACT math subscore, are required to take the  ALEKS placement assessment  for math.
                         
                        
                      
                     
                      Students without ACT scores are required to take the ACCUPLACER, a computer-adaptive
                        assessment, to determine the best course placement for them. The ACCUPLACER assessment
                        provides students and their advisors with important information about educational
                        preparation and is a good beginning toward the achievement of academic goals.
                      
                     
                      ACCUPLACER tests are given many times during the course of the year. The Testing Center
                        has a list of dates and a Test Guide for your convenience. The ACCUPLACER test should
                        be taken before seeing your advisor or attending New Student Orientation.
                      
                     
                      Note the following requirements regarding placement tests and course enrollment: 
                     
                      
                        
                         ACCUPLACER testing is required of students whose ACT (SAT) scores are more than 5
                           years old, transfer students with no college level credit in English, non-degree seeking
                           students before enrollment in English, and students with no ACT/SAT scores.
                         
                        
                         ALEKS testing is required of transfer students with no college level credit in math,
                           and non-degree seeking students before enrollment in a math course.
                         
                        
                         Enrollment in any prescribed course is restricted to students who are placed by their
                           ACT subscores or who have taken the appropriate placement assessment. Self-placement
                           without testing is not permitted.
                         
                        
                      
                     
                      Courses 
                     
                      Students with ACT reading subscores below 19 are required to enroll in  ACAD 1100 .
                      
                     
                      Students with ACT English subscores below 18 are required to enroll in prescribed
                        English courses. Students must check with their advisor to determine which English
                        course is appropriate for them.
                      
                     
                      Students will be placed in a math course based on the results of the  ALEKS placement  assessment. Students must check with their advisor to determine which college level
                        math course is appropriate for them.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Available Hours  
                               See when we are available during the week for advising. 
                            
                            
                                ACAD 1100  
                               Everything new students need to know to be successful. 
                            
                            
                                Frequently Asked Questions  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


