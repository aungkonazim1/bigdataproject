Neighborhood Preservation Fellow - School of Law - University of Memphis  Neighborhood Preservation Fellow  










 
 
 
     



 
    
    
    Neighborhood Preservation Fellow - 
      	School of Law 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Cecil C. Humphreys School of Law
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Admissions  
                          Programs  
                          Current Students  
                          Faculty  
                          Careers  
                          Library  
                      
                      
                         
                            About   
                            
                               
                                  
                                   Administration   Staff  
                                   History  
                                   Strategic Plan  
                                   Virtual Tour  
                                   ABA Disclosures  
                                   Diversity  
                                         Diversity Home  
                                         TIP  
                                         Scholarships  
                                     
                                  
                                   Facilities  
                                         Facilities Overview  
                                         Virtual Tour  
                                         Floor Plans  
                                         Photo Gallery  
                                     
                                  
                                   News   Events  
                                         Law School News  
                                         On Legal Grounds Blog  
                                         ML-Memphis Law Magazine  
                                         Events Calendar  
                                         Communications Office  
                                     
                                  
                                   Directions  
                                   Contact  
                                         Media Resource  
                                         General Contact Info  
                                         Faculty   Staff  
                                     
                                  
                                   Life in Memphis  
                                         Memphis-The City  
                                         Neighborhoods  
                                         Housing  
                                         The Arts Scene  
                                         Sports  
                                         The Food Scene  
                                         A Music Town  
                                         Directions and Maps  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	School of Law 
                              	  
                          
                              	About
                              	  
                         Neighborhood Preservation Fellow 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      BRITTANY WILLIAMS NAMED NPC FELLOW 
                     
                        
                     
                      The University of Memphis Cecil C. Humphreys School of Law and the City of Memphis
                        have hired Brittany Williams as the first "City of Memphis Neighborhood Preservation
                        Fellow."
                      
                     
                      As a Fellow, Williams will assist in case handling and management for Neighborhood
                        Preservation Act cases and other Environmental Court cases filed by the City. She
                        will also draft and file lawsuits in Environmental Court, as well as communicate outside
                        of court with defendants and attorneys for defendants regarding the status of all
                        cases filed with Environmental Court or otherwise managed by the Neighborhood Preservation
                        Clinic on behalf of the City. She will represent the City in litigation against owners
                        of blighted property that violates City codes and will provide assistance to students
                        enrolled in the University of Memphis Neighborhood Preservation Clinic in regards
                        to case filing and management.
                      
                     
                      The City of Memphis provided the law school with $150,000 of funding to support this
                        unique position through December 31, 2017, with proceeds from its Vacant Property
                        Registry used to fund the unique partnership. As part of the agreement, the "Neighborhood
                        Preservation Fellow" must be a Memphis Law graduate, with preference given to a graduate
                        who has successfully completed a semester in the law school's Neighborhood Preservation
                        Clinic, or participated in the anti-blight litigation externship with the City of
                        Memphis Law Division.
                      
                     
                      Brittany Williams, a Class of 2015 Memphis Law graduate, was officially named the
                        first Neighborhood Preservation Fellow in Summer 2016, only one year after successfully
                        graduating from law school. Prior to her appointment as Fellow, Brittany worked for
                        the law firm of Brewer   Barlow, PLC as a law clerk and in March 2016 was named the
                        Interim Neighborhood Preservation Fellow before her official appointment in June of
                        2016.
                      
                     
                      "Our efforts with the City of Memphis to fight blight allow us to not only help the
                        city itself, but also give our current students, and now our graduates like Brittany
                        Williams, the ability to play a front-line role in this critical fight," said Dean
                        Peter V. Letsou. "We are training our students to tackle a problem that lacked a focused
                        solution before this partnership and, in doing so, we are helping to provide Memphis
                        with specially trained attorneys to alleviate this blight epidemic."
                      
                     
                      The fellowship adds yet another component to the law school's and the City's dedicated
                        fight against blight in the Mid-South. The law school and the City of Memphis last
                        year announced the creation and launch of the Neighborhood Preservation Clinic, a
                        partnership between the law school and the City's legal division that allows law students
                        to earn real-world experience for law students in issues of blight, property abandonment,
                        and neglect through the handling of cases in the Shelby County Environmental Court.
                        The clinic is co-directed by local attorney and Memphis Law adjunct faculty member
                        Steve Barlow and Assistant Professor of Law and Director of Experiential Learning
                        Daniel Schaffzin. Over the past year, law students in the clinic have studied the
                        laws concerning real property abandonment and neglect in Memphis, and worked with
                        code enforcement officers to prosecute civil lawsuits filed pursuant to the Tennessee
                        Neighborhood Preservation Act. The clinic and the City celebrated the filing of the
                        City's 1,000th lawsuit against blighted property owners during the fall 2015 semester.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         About 
                         
                            
                               
                                Administration   Staff  
                                History  
                                Strategic Plan  
                                Virtual Tour  
                                ABA Disclosures  
                                Diversity  
                                      Diversity Home  
                                      TIP  
                                      Scholarships  
                                  
                               
                                Facilities  
                                      Facilities Overview  
                                      Virtual Tour  
                                      Floor Plans  
                                      Photo Gallery  
                                  
                               
                                News   Events  
                                      Law School News  
                                      On Legal Grounds Blog  
                                      ML-Memphis Law Magazine  
                                      Events Calendar  
                                      Communications Office  
                                  
                               
                                Directions  
                                Contact  
                                      Media Resource  
                                      General Contact Info  
                                      Faculty   Staff  
                                  
                               
                                Life in Memphis  
                                      Memphis-The City  
                                      Neighborhoods  
                                      Housing  
                                      The Arts Scene  
                                      Sports  
                                      The Food Scene  
                                      A Music Town  
                                      Directions and Maps  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply to Memphis Law  
                               Take the first step toward your legal education 
                            
                            
                                ABA Required Disclosures  
                               Information required by ABA Standard 509 Required Disclosures 
                            
                            
                                Alumni   Support  
                               Stay up to date with Memphis Law alumni 
                            
                            
                                Contact Us  
                               Questions about law school? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


