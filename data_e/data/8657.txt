Navigate a document | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Navigate a document | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   English  
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Binder    »    Binder for iPad    »    Managing documents in Binder for iPad   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Navigate a document                       
        
        
       
          
     
           Printer-friendly version       
	You can view the structure and contents of documents from the   Book  icon on the navbar.
 

 
	Tap the   Book  icon, then:
 

  
		Tap  Outline  to view the table of contents of a document. Tap the headings listed to navigate directly to the section of the document.
	 
	 
		Tap  Annotations  to view a list of annotations in the document. Tap an annotation in the list to navigate directly to the page.
	 
	 
		Tap  Bookmarks  to view a list of all pages in the document. Tap a bookmarked page in the list to navigate directly to the page.
	 
      Audience:    Learner      
    
         
               ‹ Bookmark a page in a document 
                     up 
                     Search for a term within a document › 
           
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Binder  
  
      Binder basics    Binder Web    Binder for iPad    Basics of Binder for iPad    Managing connections in Binder for iPad    Managing documents in Binder for iPad    Add a document    Create a document collection    Move a document or collection    Delete a document    Bookmark a page in a document    Navigate a document    Search for a term within a document      Managing annotations in Binder for iPad      Binder for Android    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
