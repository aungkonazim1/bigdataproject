Center for Information Assurance - Center for Information Assurance - University of Memphis    










 
 
 
     



 
    
    
    Center for Information Assurance - 
      	Center for Information Assurance
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Center for Information Assurance
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Scholarships  
                          Projects  
                          CyberSummit  
                          Courses  
                          People  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                         
                           	Center for Information Assurance
                           	
                         
                      
                   
                   
                      
                         
                              
                             
                            
                             
                            
                            
                                
                            
                         
                         
                             
                         
                      
                      
                     		
                     
                     
                      
                        
                         
                           
                            CfIA News 
                           
                              CfIA Successfully concludes 10th Annual Cyber Security Summit   The Center for Information Assurance (CfIA) successfully hosted its 10th annual Cybersecurity
                              Summit on October 12, 2017 at the FedEx Institute of Technology on the University
                              of Memphis campus. Dr. Dipankar Dasgupta and Dr. Judith Simon, Co-Directors, and Dr.
                              Kan Yang, Associate Director of the CfIA, were involved in hosting this year's summit
                              and welcomed students and professionals from multiple areas in the Cybersecurity industry.
                              The CfIA was honored to have some of the guest speakers present at past Cybersecurity
                              Summits.   Read More      
                           
                              CfIA Student Team won 2017 Cyber Defense Competition   A team of undergraduate students at the Center for Information Assurance (CfIA), recently
                              formed a student Organization (RSO) open to all U of M students. Nu11t3st3r (pronounced
                              "Nulltester") won 1st place in the CANSec Student Cyber-Defense Competition on October
                              29, 2017 at the Missouri University of Science and Technology in Rolla, Missouri.   Read More      
                           
                              Dr. Judith C. Simon selected as a member of NCC-CSP   Dr. Judith C. Simon has been selected as a member of the National CyberWatch Center's
                              Curriculum Standards Panel (NCC-CSP) for the Cybersecurity Foundation Series, and
                              specifically for the Information Security Fundamentals (ISF) course. The panel will
                              develop standards for instructional materials, assessments, hands-on labs or other
                              learning programs that will produce the nation's first competency-based, mastery learning
                              curricula that aligns with national competency frameworks.
                            
                           
                              Cyber Security Virtual Career Fair   The National Centers of Academic Excellence is hosting a Virtual Career Fair, sponsored
                              by CyberWatch West and the National Science Foundation, on October 13, 2017, from
                              9:00am-2:00pm (PST).   Read More      
                           
                              Proctored Cybersecurity Training Session at Nashville   The CfIA will be offering multiple individuals the opportunity to attend a proctored
                              Cybersecurity training session at the Tennessee Emergency Management Agency (TEMA)
                              in Nashville, TN on September 22, 2017. Trainees will learn about the security and
                              privacy issues present in mobile devices and the preventative measures that will help
                              prevent them from becoming cyber-attack victims. Please see  flyer  for more details.
                            
                           
                             Download the CfIA annual report - 2017  
                           
                             Download the CfIA annual report - 2015-16  
                           
                             See more news  
                           
                         
                        
                         
                           
                            Events 
                           
                            
                              
                               
                                 
                                  
                                    
                                     
                                       
                                        Fri Dec 15
                                        
                                       
                                     
                                    
                                       
                                    
                                       A Genetic Algorithm Approach to Optimize Microgrid Energy System    By: Sajib Sen  3:00 pm - 3:30 pm, FIT 324 "This approach will maintain demand response of a particular duration by keeping the
                                       generation cost minimum"
                                     
                                    
                                  
                                 
                                  
                                    
                                       
                                    
                                       
                                    
                                       
                                    
                                  
                                 
                                  
                                    
                                     
                                       
                                        Fri Dec 15
                                        
                                       
                                     
                                    
                                       
                                    
                                       Minimizing Traceability using Genetic Algorithm    By: Senjuti Dutta  3:30 pm - 4:00 pm, FIT 324 "This approach will minimize tracking of a user based on the configuration presented
                                       on the websites"
                                     
                                    
                                  
                                 
                                  
                                    
                                       
                                    
                                       
                                    
                                       
                                    
                                  
                                 
                               
                              
                            
                           
                             See more events  
                           
                              
                           
                              
                           
                         
                        
                      
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                News   Events  
                                
                            
                            
                                Student Success Stories  
                                
                            
                            
                                Community Outreach  
                                
                            
                            
                                Contact Us  
                                
                            
                         
                      
                      
                       
 
    
            
       
          
              
          
           GenCyber Boot Camp 2016 
       
    
                        
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


