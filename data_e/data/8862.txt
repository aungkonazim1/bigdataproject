Adding a message to Wiggio | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Adding a message to Wiggio | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   English  
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Wiggio    »    Adding resources to Wiggio   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Adding a message to Wiggio                       
        
        
       
        
     
               Click   Message  in the Start a conversation area. 
	 Select whether you want to post a  Text  or  Video  message. 
	 Do one of the following:
		  For a text message, enter your message in the  Type your message here...  field. 
			 For a video message, click  Allow  if an Adobe Flash Player Settings message displays. Click   Record  to record your message and   Stop  to stop. To redo your message, click   Redo . 
		  
 
		Do one of the following:
		  
				Select a group of participants from the  Participants  drop-down list.
			 
			 
				Add participants by entering their name or email into the  Add by name or email...  field and pressing Enter.
			 
			 
				Click the   Import from email  icon to import participants from your email contact list.
			 
		  
			 Tip  Expand and review your list of participants by clicking the  Show participants  link. Delete  participants by clicking the   Remove  icon by their name.
		 
	 
	 Click  Post .	
       Audience:     Learner       
    
         
               ‹ Adding a poll to Wiggio 
                     up 
                     Adding an event to Wiggio › 
           
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Wiggio  
  
      Wiggio basics    Adding resources to Wiggio    Adding files to Wiggio    Adding a link to Wiggio    Adding a meeting to Wiggio    Adding a to-do list to Wiggio    Adding a poll to Wiggio    Adding a message to Wiggio    Adding an event to Wiggio      Using polls in Wiggio    Using to-do lists in Wiggio    Creating and using folders in Wiggio    Creating and using groups in Wiggio    Managing groups in Wiggio     Managing events in Wiggio    Importing and exporting calendars in Wiggio    
                  
           
         

       
     

    
    
    
   
 
   
 
