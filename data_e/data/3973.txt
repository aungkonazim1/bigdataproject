james mitchel - LEAD - University of Memphis    










 
 
 
     



 
    
    
    james mitchel - 
      	LEAD
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Leadership
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Higher Ed/Adult Ed  
                          Leadership   PS  
                          Faculty   Staff  
                          News  
                      
                      
                         
                            Faculty   Staff   
                            
                               
                                  
                                   Faculty   Staff  
                                   HIAD Faculty  
                                   LDPS Faculty  
                                   Affiliated Faculty  
                                   CSHE Management  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	LEAD
                              	  
                          
                              	Faculty Profiles
                              	  
                         james mitchel 
                      
                   
                   
                       
                      
                     
                     	
                     
                     		  
                      
                        
                        
                         
                           
                           
                                                                
                              
                              
                               
                              
                              
                            
                           
                           
                            
                              
                               
                                 
                                 James Mitchell 
                                 
                               
                              
                              
                               
                                 
                                 Affiliate Research Professor 
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Phone 
                                    
                                     
                                       
                                       901.678.5430
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Email 
                                    
                                     
                                       
                                       jmtchll2@memphis.edu
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Fax 
                                    
                                     
                                       
                                       901.678.0505
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Office 
                                    
                                     
                                       
                                       Ball Hall 123D
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Office Hours 
                                    
                                     
                                       
                                       By appointment 
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 										
                                 
                                 
                                                                         
                                 
                               
                              
                            
                           
                           
                         
                        			 
                        			  
                        
                        
                         About Dr. Mitchell 
                        
                         James B. (Jim) Mitchell, Jr. received his elementary and secondary school education
                           in the public schools of Memphis. He graduated from Memphis Central High School in
                           1964, where he served as president of the 450 member senior class. Jim was awarded
                           a Leadership Scholarship and entered Rhodes College in September, 1964. He was awarded
                           the Bachelor of Arts degree from Rhodes College in June, 1968. Jim Mitchell began
                           his educational career in the Shelby County Schools in Memphis, TN.  He taught social
                           sciences at the middle school level for seven years from 1968-1975.
                         
                        
                         In 1974, Jim was awarded the Master of Education degree from The University of Memphis
                           with a major in Educational Administration. He began his administrative duties in
                           1975 as Certificated Personnel Supervisor in the Shelby County Schools Central Administrative
                           Office. His eight years of personnel experience centered upon recruitment, interviewing,
                           and initial employment recommendation for K-12 certificated personnel. In July, 1983, Jim
                           Mitchell began his experience in secondary school leadership as Vice-Principal of
                           Instruction at Germantown High School, one of Tennessee's largest comprehensive high
                           schools.  In July, 1987 he was appointed Principal of Millington Central High School.
                           During his two years in this leadership role, he was responsible for all facets of
                           administration in a comprehensive secondary school of 125 employees and 1,600 students.
                         
                        
                         In May, 1989, Dr. Mitchell was awarded the Doctor of Education degree from The University
                           of Memphis with a major in Educational Administration and Supervision (EDAS). He received
                           The University of Memphis 1989 EDAS Outstanding Graduate Student Award.  In May, 1991,
                           he was appointed Assistant Superintendent, Operations Division. He coordinated the
                           completion of over $131 million dollars of capital projects involving the construction
                           of thirteen (13) new schools plus additions and renovations in numerous other school
                           facilities.
                         
                        
                         Dr. Mitchell was appointed as the 21st Superintendent of Shelby County Schools in
                           1997, where he provided leadership to a rapidly growing school district with 46 schools,
                           45,000 students, 5,000 employees, and an annual $250 million dollar operating budget.
                            He retired as Superintendent of Shelby County Schools in April, 2002.
                         
                        
                         In September, 2002, Dr. Mitchell was appointed to serve as School Leader in Residence
                           for the Department of Leadership in the College of Education at The University of
                           Memphis.  He serves as a graduate school instructor in the Department of Leadership. 
                           Dr. Mitchell also assists in establishing partnerships with school districts in the
                           region.
                         
                        
                        
                      
                     								
                     
                     
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Faculty   Staff 
                         
                            
                               
                                Faculty   Staff  
                                HIAD Faculty  
                                LDPS Faculty  
                                Affiliated Faculty  
                                CSHE Management  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now   
                               Start an application and view the Graduate Catalog 
                            
                            
                                About Us  
                               Learn more about the Department of Leadership and what we do 
                            
                            
                                Need Help? Contact Us!   
                               Questions or comments? Get in touch with our support staff! 
                            
                            
                                Online Resources   
                               A list of useful links, forms, and documents for current and prospective Leadership
                                 students 
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/26/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


