Business Economics Major - ECON - University of Memphis    










 
 
 
     



 
    
    
    Business Economics Major   - 
      	ECON
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - Department of Economics
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Welcome  
                          Programs  
                          Faculty  
                          Research  
                          FCBE  
                      
                      
                         
                               
                            
                               
                                  
                                   About  
                                   Programs  
                                   Faculty  
                                   Research  
                                   Contact Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	ECON
                              	  
                          
                              	Programs
                              	  
                         Business Economics Major   
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Business Economics Major 
                     
                       Business Economics Major (B.B.A.)     
                     
                      A. University General Education Program (41 hours) 
                     
                      See  Graduation from the University  for the University General Education Program requirements. Note that MATH 1830 or
                        1910 (Mathematics requirement) with a minimum grade of "C",  ECON 2010 and 2020 (Social/Behavioral
                        Sciences requirement) are specified for the B.B.A. degree.
                      
                     
                      B. College and Degree (B.B.A.) Requirements (42 hours) 
                     
                      Lower Division Business Core Curriculum (12 hours) 
                     
                      No grade below “C” and 2.25 GPA (2.5 for accounting majors) in ECON 2010, ECON 2020
                        and the following lower division courses
                      
                     
                      
                        
                         ACCT 2010 Fundamentals of Accounting I (3) 
                        
                         ACCT 2020 Fundamentals of Accounting II (3) 
                        
                         ISDS 2710 Business Statistics (3) or MATH 1530 Prob/Statistics/Non Calculus (3) 
                        
                         MIS 2749 Introduction to Business Microcomputer Applications (3) 
                        
                      
                     
                      Upper Division Business Core Curriculum (30 hours) 
                     
                      No grade below "C" and 2.25 GPA in the following courses: 
                     
                      
                        
                         ACCT 3130 Legal, Social, and Political Environment of Business (3) 
                        
                         FIR 3410 Business Finance (3) 
                        
                         MGMT 3110 Organization and Management (3) 
                        
                         MGMT 4710 Strategic Management (3) (Enrollment usually limited to graduating seniors) 
                        
                         MKTG 3010 Principles of Marketing (3) 
                        
                         MGMT 3510 Business Communication (3) 
                        
                         ISDS 3510 Production and Operations Management (3) 
                        
                         ISDS 3711 Analytical Tools for Business Decisions (3) 
                        
                         MIS 3210 Critical Thinking in Project Management for Business (3) 
                        
                      
                     
                      Choose One: 
                     
                      ACCT 4625 International Accounting: IFRS vs US GAAP (3); MGMT 4810 International Management
                        (3); MKTG 4530 International Marketing (3); ECON 4350 International Economics (3);
                        FIR 4550 International Finance (3); MGMT 4510 International Business Communication
                        and Negotiation (3); HPRM 4400 International Hospitality (3); MIS 4310 Global Information
                        Technology (3); ECON 4351 International Monetary Economics: Theory and Policy (3);
                        one course from FIR 4110-4119 (3) when the topic pertains to international business
                        and has been approved in advance by the department chair.
                      
                     
                      Transfer students are referred to the section Transfer Credit in the College of Business
                        and Economics description.
                      
                     
                      C. The Major (24 hours) 
                     
                      No grade below "C" and a minimum 2.25 GPA in the major. 
                     
                      A minimum of 24 upper-division semester hours as follows: 
                     
                      For students choosing the Business Economics major with no concentration: ECON 3310,
                        3320, and six upper-division economics courses (18 hours) selected with the approval
                        of the department advisor.
                      
                     
                      D. Electives (13 hours) 
                     
                      Lower division or upper division courses to bring the total of hours earned to 120. 
                     
                      E. Honors Program 
                     
                      All students who receive a minimum grade of B (3.0) in ECON 2010 or 2020 honors sections
                        are eligible for the program. Entry to the program can also be obtained through the
                        recommendation of a member of the faculty to the Department of Economics Honors Committee.
                        A student must maintain a grade point average of at least 3.25 in economics to continue
                        in the program and must have a minimum grade point average of 3.5 in economics to
                        graduate with honors in economics. Honor students write a senior research paper. At
                        commencement they are awarded the special distinction "With Honors in Economics.”
                        Details of the program are available at the department office.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                          
                         
                            
                               
                                About  
                                Programs  
                                Faculty  
                                Research  
                                Contact Us  
                            
                         
                      
                      
                      
                         
                            
                                Undergraduate Programs  
                               Learn about our undergraduate offerings! 
                            
                            
                                Seminars  
                               Keep up with the latest Economic academic research in our upcoming seminars.  
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


