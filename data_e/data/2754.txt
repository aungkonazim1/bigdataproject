BARBARA DAVIS - MGMT - University of Memphis    










 
 
 
     



 
    
    
    BARBARA DAVIS - 
      	MGMT
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - Department of Management
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Welcome  
                          Programs  
                          Courses  
                          Faculty  
                          PhD Students  
                          Research  
                          FCBE  
                      
                      
                         
                            Faculty   
                            
                               
                                  
                                   Welcome  
                                   Programs  
                                   Courses  
                                   Faculty  
                                   PhD Students  
                                   Research  
                                   Contact Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	MGMT
                              	  
                          
                              	Faculty
                              	  
                         BARBARA DAVIS 
                      
                   
                   
                       
                       
                     
                       
                        
                          
                           
                             
                              
                              
                               
                               
                              
                             
                           
                             
                              
                               
                                 
                                 BARBARA DAVIS
                                 
                                
                              
                               
                                 
                                 Associate Professor,  Department of Management
                                 
                                
                              
                                
                                 
                                   
                                    
                                     Phone  
                                    
                                     
                                       
                                       901-678-3932
                                       
                                      
                                    
                                   
                                 
                                   
                                    
                                     Email  
                                    
                                     
                                       
                                       bddavis@memphis.edu
                                       
                                      
                                    
                                   
                                 
                                   
                                    
                                     Fax  
                                    
                                     
                                       
                                       901-678-4990
                                       
                                      
                                    
                                   
                                 
                                
                              
                                
                                 
                                   
                                    
                                     Office  
                                    
                                     
                                       
                                       BA 328
                                       
                                      
                                    
                                   
                                 
                                   
                                    
                                     Office Hours  
                                    
                                     
                                       
                                       TR 7:30-11:15am   by appointment
                                       
                                      
                                    
                                   
                                 
                                
                              
                                
                                 
                                 
                                 
                                   Website  
                                  
                                 
                                
                              
                             
                           
                          
                        
                        
                        
                         Biography 
                        
                          Dr. Barbara Davis  joined the Management department in Fogelman College of Business   Economics in 1994.
                           She received her B.S. from Knoxville College, M.A. from The University of Kentucky,
                           and doctorate from The University of Memphis (then Memphis State University). She
                           worked in numerous manufacturing and service positions including Quality Control Manager
                           and Supervisor prior to entering academia. Her research interests are employment and
                           intercultural communication, workplace skills, and diversity. She teaches business,
                           intercultural, and executive communications. Dr. Davis was nominated for the University
                           of Memphis Alumni Association Distinguished Teaching Award for the 2011 as well as
                           the 2012 academic year.
                         
                        
                         Research 
                        
                         
                           
                            Current Publications: 
                           
                            Martin, J. S.,  Davis, B. D.,  Krapels, R. H.(2012). ”A Comparison of the Top Six Journals Selected as Top Journals
                              for Publication by Business Communication Educators”   Journal of Business Communication.    
                           
                            Jackson, W. T., Elenkov, D.,  Davis, B. D.,    Wright, P. (2012). “Psychological Contracts in Enterprises.”  Journal of Organizational Culture, Communications and Conflict.  
                           
                         
                        
                         Teaching 
                        
                         Dr. Davis teaches undergraduate and graduate courses and has served on dissertation
                           committees in both the business and education college.
                         
                        
                         Courses taught include MGMT 3510, Business Communication and Honors Business Communication;
                           MGMT 4510, International Business Communication and Negotiations; and MGMT 7173, Executive
                           Business Communication
                         
                        
                         Service 
                        
                         
                           
                            Association for Business Communication Southeast Region Clarice Brantley Service Award,
                              2012.
                            
                           
                            Former, 2003-2004 Suzanne Downs Palmer Professorship Award for Service 
                           
                         
                        
                         University 
                        
                         
                           
                            Member, Behavior Standards Committee 
                           
                            Member, TBR 1st Generation Ph.D. 
                           
                            Member, Faculty Athletic Advisory Committee 
                           
                         
                        
                         College 
                        
                         
                           
                            Coordinator, Business Dining Etiquette Program 
                           
                            Coordinator, 4C’s Initiative 
                           
                            Member, Continuous Improvement Committee 
                           
                         
                        
                         Department 
                        
                         
                           
                            Member, Tenure and Promotion Committee 
                           
                            Member, Undergraduate Studies Committee 
                           
                         
                        
                         Profession 
                        
                         
                           
                            Member, Association for Business Communication (ABC) 
                           
                            Member, Southeast Region Association for Business 
                           
                            Communication 
                           
                            Former Vice-President, Southeast Region Association for Business Communication 
                           
                            Member, Delta Pi Epsilon, Inc. 
                           
                            Member, Consulting Interest Group (ABC) 
                           
                            Editorial Board,  Journal of Business Communication  
                           
                         
                        
                         Community 
                        
                         
                           
                            Board Member, Memphis Cancer Research Foundation 
                           
                            Volunteer, American Cancer Society, Reach to Recovery Program 
                           
                            Trainer, American Cancer Society, Community Action Team of Shelby County 
                           
                            Volunteer, Memphis MidSouth Affiliate Susan G. Komen for the Cure 
                           
                            Steering Committee, Memphis MidSouth Affiliate Komen Luncheon 
                           
                            Coordinator, Race for the Cure, New Bethel M. B. Church 
                           
                            Career Day Participant, Oakhaven Elementary School (Memphis City School) 
                           
                            Juror for Exhibition of student Work, Booker T. Washington High School (Memphis City
                              School)
                            
                           
                            Volunteer, Memphis In May 
                           
                         
                         
                        
                       
                     
                      
                   
                
                
                   
                      
                         Faculty 
                         
                            
                               
                                Welcome  
                                Programs  
                                Courses  
                                Faculty  
                                PhD Students  
                                Research  
                                Contact Us  
                            
                         
                      
                      
                      
                         
                            
                                MGMT News   Events  
                               Learn about current events happening in our department! 
                            
                            
                                Research Colloquia  
                               Keep up with the latest Management academic research in our upcoming seminars.  
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/1/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


