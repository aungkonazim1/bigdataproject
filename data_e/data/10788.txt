Health Statistics on the Web - Nursing - LibGuides at University of Memphis Libraries  	 
 
	 
         
         
         
        
		 Health Statistics on the Web - Nursing - LibGuides at University of Memphis Libraries 
		 
		 
 
 
 
 
 
 
 
 
 
 
 

		 
		 
		 
		 
		 
		 
         
         
				 
				 
             
         
        
        
            
		
		 
		
		 
		 
		
		 
		
		
	   
		         
        	 
		 
		 Skip to main content                  
                 
         
         
             
                
			 
				
					 
						 University of Memphis Libraries
						 
					 
					 
						 LibGuides
						 
					 
					 
						 Nursing
						 
					 
					 Health Statistics on the Web
					 
			              
			 
				 
					
                 
                     
                         
                             
                             Search this Guide  
                             
                                 Search 
                             
                         
                     
                 				 
							 
             
                 Nursing: Health Statistics on the Web 
                 
                     Welcome! This guide will direct you to some resources that will help you in your studies at the Loewenberg School of Nursing. 
                 
             
         
         
         
             
                 
                    
                 
                     
                         Welcome 
                        
                     
                 
                 
                     
                         Find Articles 
                        
                     
                 
                 
                     
                         Find Journals 
                        
                     
                 
                 
                     
                         Find Books 
                        
                     
                 
                 
                     
                         Web Sites 
                        
                     
                 
                 
                     
                         Health Statistics on the Web 
                        
                     
                 
                     
                         
                             RefWorks 
                            
                         
                         
                              
                             Toggle Dropdown 
                         
                         
                             
                                 RefWorks
                                    
                                 
                              
                                      
             
         
          
         
         
            			
				 
     
         
         
     
				
 
				 
     
         
							 
					 
						 
							 State & Local Data Sets & Statistics
                                 
							 
								 
									 
                          
                        
        			 
        				 State Health Facts Online (Kaiser Family Foundation)  
        			  
                        
                             
                              
                          
                        
			   Snap Shots of State Population Data  
				 
			  
                        
                             
                              
                          
                        
        			 
        				 America's Health Rankings (United Health Foundation)  
        			  
                         
                        
        			 
        				 Community Health Status Indicators  
        			  
                         
                        
        			 
        				 County Health Rankings (Robert Wood Johnson Foundation)  
        			  
                        
                             
                             
								 
								
							 
						 
					 
							 
							 
					 
						 
							 International Statistics
                                 
							 
								 
									 
                          
                        
        			 
        				 Global Health Facts.org  
        			  
                         
                        
        			 
        				 World Health Organization--Statistical Information System  
        			  
                        
                             
                              
                          
                        
			   Pan American Health Organization  
				 
			  
                        
                             
                             
								 
								
							 
						 
					 
							 
							 
					 
						 
							 Health Statists Portals/Gateways
                                 
							 
								 
									 
                          
                        
			   Partners in Information Access for the Public Health Workforce  
				  Health Data Tools and Statistics 
			  
                        
                             
                              
                          
                        
        			 
        				 National Center for Health Statistics (NCHS)   FastStats 
        			  
                        
                             
                             
								 
								
							 
						 
					 
							 
							 
					 
						 
							 National Data Sets and Statistics
                                 
							 
								 
									 
                          
                        
        			 
        				 Behavioral Risk Factor Surveillance System  
        			  
                        
                             
                              
                          
                        
			   CDC WONDER--Wide-ranging Online Data for Epidemiologic Research  
				 
			  
                         
                        
			   National Health and Nutrition Examination Survey  
				 
			  
                        
                             
                              
                          
                        
        			 
        				 Web-based Injury Statistics Query and Reporting System  
        			  
                        
                             
                              
                          
                        
			   Substance Abuse & Mental Health Services Administration  
				 
			  
                        
                             
                              
                          
                        
        			 
        				 Center for Medicare and Medicaid Services  
        			  
                        
                             
                              
                          
                        
			   Agency for Health Care Research and Quality  
				 
			  
                        
                             
                              
                          
                        
        			 
        				 FedStats  
        			  
                         
                        
        			 
        				 United States Census Bureau  
        			  
                        
                             
                             
								 
								
							 
						 
					 
							 
         
     
     
         
         
     
     
         
         
     
				
 
				 
     
         
         
     
				
             
                 
                     
                         Previous:  Web Sites 
                     
                     
                      Next:  RefWorks    
                     
                          
         
         
         
             
                 
                     
                         Last Updated:   Oct 30, 2017 2:58 PM                      
                     
                         URL:   http://libguides.memphis.edu/Nursing                      
                     
                            Print Page                      
                 
                 
                     Login to LibApps                  
             
             
                 
                     Report a problem.  
                 
                 
                    
                     Subjects:  
                      Nursing  
                                     
                 
                    
                     Tags:  
                      medical information ,  subject guide  
                                     
             
         
         
        
			 
				 
					 
					    
					    
					 
				 
			          
                              
                                
                 
                

		 
  	 
	 
	