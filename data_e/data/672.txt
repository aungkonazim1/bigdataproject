Strategic Plan - Libraries - University of Memphis    










 
 
 
     



 
    
    
    Strategic Plan - 
      	Libraries
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			University Libraries
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Services  
                          Resources  
                          Instruction  
                          Special Collections  
                          Technology  
                      
                      
                         
                            Administration   
                            
                               
                                  
                                   About Us  
                                         Staff  
                                         Hours and Location  
                                     
                                  
                                   Library Faculty and Staff Portal  
                                   Libraries Standing Committees  
                                   Libraries Organizational Chart  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Libraries
                              	  
                          
                              	Administration
                              	  
                         Strategic Plan 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                       University Libraries, University of Memphis  
                     
                       Strategic Plan 2013-2018    Read as PDF      
                     
                       MISSION STATEMENT  The University Libraries is a leader in managing and providing access to information
                        services and resources that support teaching, learning, and research for the University
                        of Memphis community.
                      
                     
                       VISION STATEMENT   The University Libraries will be the information and research destination of choice
                        for the University of Memphis community, committed to developing lifelong learning
                        and research skills in a welcoming, resource-rich, innovative, and stimulating environment,
                        embracing collaborative opportunities and cultivating a technologically-enhanced,
                        user-centered setting for the discovery, creation, organization, preservation, and
                        communication of knowledge.
                      
                     
                        STATEMENT OF VALUES  Providing the varied services, resources, and programs of the University Libraries
                        requires the involvement of many people. While each individual utilizes his or her
                        own particular expertise and background and adopts his or her unique way to accomplish
                        each task, the overall effort is guided by a common set of values that binds the whole
                        together with a common purpose. As they go about meeting the Libraries' established
                        objectives, University Libraries' faculty and staff are committed to the following
                        values:
                      
                     
                        Service  The University Libraries seeks excellence in providing assistance to all Libraries
                        users while ensuring that access to information resources is provided in an open,
                        receptive, and courteous manner, with a commitment to freedom of information and equity
                        of access.
                      
                     
                       Quality  The University Libraries strives to deliver effective user services, resources, and
                        programs, using the highest feasible standards of management, assessment, organization,
                        and delivery. We are simultaneously committed to the development of a knowledgeable,
                        versatile, and skilled faculty and staff.
                      
                     
                       Integrity  The University Libraries affirms the principles of academic freedom and provides all
                        services, programs, and operations with honesty, openness, and accountability.
                      
                     
                       Diversity  The University Libraries values and appreciates the differences, among our users,
                        our collections, and our faculty and staff. We endeavor to provide a climate of acceptance
                        and respect for all points of view and for all individuals, whether members of the
                        university community or the community-at-large, without regard to race, ethnicity,
                        gender, age, sexual orientation, or physical abilities.
                      
                     
                       Collaboration  The University Libraries partners with the departments and colleges to provide resources
                        and services to support the academic programs of the University. We join with other
                        academic communities and libraries throughout Memphis, Tennessee, the Mid-South region,
                        and the nation to enhance access to information resources beyond the University Campus.
                      
                     
                       Innovation  The University Libraries identifies, investigates, evaluates, and implements new and
                        emerging methods for obtaining appropriate resources and providing relevant services
                        to meet known and/or anticipated user needs.
                      
                     
                       GOALS 2013 - 2018  To accomplish the overall mission of the University Libraries, resources and energy
                        will be focused on achieving the following goals.  1. Build, preserve, and support collections that meet the needs of present and future
                        users.  2. Develop, explore, and implement new information technologies and resources.  3. Develop and implement strategies that maximize the effectiveness of the integrated
                        library system (ILS).  4. Develop, promote, and deliver instructional services and resources to meet changing
                        user needs.  5. Invest in Libraries' personnel to enhance their abilities to provide library services
                        and resources.  6. Collaborate with other units of the University, the urban community, the professional
                        community, the region, or the nation to improve access to information resources.  7. Create and implement marketing and development plans that will enhance the visibility,
                        enhance the image, and expand the resource base of the University Libraries.
                      
                     
                       GOALS AND OBJECTIVES:      Read as PDF       
                     
                        
                     
                        
                     
                        
                     
                      Updated 9/13 
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Administration 
                         
                            
                               
                                About Us  
                                      Staff  
                                      Hours and Location  
                                  
                               
                                Library Faculty and Staff Portal  
                                Libraries Standing Committees  
                                Libraries Organizational Chart  
                            
                         
                      
                      
                      
                         
                            
                                Ask-a-Librarian  
                               Got a question? Got a problem? Ask Us! 
                            
                            
                                Interlibrary Loan  
                               Request books, articles, and other research materials from other libraries. 
                            
                            
                                Reserve space  
                               Group study and presentation spaces available in the library. 
                            
                            
                                My Library Account  
                               Review your library account for due dates and more. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


