Youngsang Kwon - Earth Sciences - University of Memphis    










 
 
 
     



 
    
    
    Youngsang Kwon - 
      	Earth Sciences
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Earth Sciences
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          Faculty  
                          Research  
                          Student Resources  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Earth Sciences
                              	  
                          
                              	Faculty
                              	  
                         Youngsang Kwon 
                      
                   
                   
                       
                      
                     
                     	
                     
                     		  
                      
                        
                        
                         
                           
                           
                                                                
                              
                              
                               
                              
                              
                            
                           
                           
                            
                              
                               
                                 
                                 Youngsang Kwon
                                 
                               
                              
                              
                               
                                 
                                 Assistant Professor
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Phone 
                                    
                                     
                                       
                                       901.678.2979
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Email 
                                    
                                     
                                       
                                       ykwon@memphis.edu
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Fax 
                                    
                                     
                                       
                                       901.678.2178
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Office 
                                    
                                     
                                       
                                       JN223
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Office Hours 
                                    
                                     
                                       
                                       Call for Hours
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 										
                                 
                                 
                                                                         
                                 
                               
                              
                            
                           
                           
                         
                        			 
                        			  
                        
                        
                         Research Interests 
                        
                         Forest Dynamics, Terrestrial Carbon Cycling, GIS, Remote Sensing, Spatial Statistics,
                           Climate Change
                         
                        
                         Education 
                        
                         
                           
                            2012 Ph.D. Department of Geography. State University of New York at Buffalo, Buffalo,
                              NY  Dissertation: A multi-scale assessment of forest carbon cycling across the eastern
                                 USA using Forest Inventory Analysis and MODIS data  
                           
                            2004 M.S. Department of Environment Studies. Seoul National University (SNU), Seoul,
                              Korea Dissertation: Estimating the air temperature cooling effect of the Cheonggyechun
                              stream restoration project of Seoul using Landsat ETM+
                            
                           
                            2001 B.S. Department of Forest Resources and Environmental Science, Korea University,
                              Seoul, Korea
                            
                           
                         
                        
                         Selected publications 
                        
                         
                           
                             Kwon, Youngsang  and Bradley Baker (2016) Area-based fuzzy membership forest cover comparison between
                              MODIS NPP and Forest Inventory and Analysis (FIA) across eastern U.S. forest. Environmental
                              Monitoring and Assessment. 189(1), 1-15. doi: 10.1007/210661-016-5745-x
                            
                           
                            Trout Fryxell RT, Moore JE, Collins MD,  Kwon, Youngsang , Jean-Philippe, Schaeffer SM, Odoi A, Kennedy M, Houston AE. (in press) Habitat and
                              vegetation variables are not enough when predicting tick populations in the southeastern
                              United States. PLoS ONE. PONE-D-15-37688.
                            
                           
                            Sungwook Choung, Oh J, Han W S, Chon C,  Kwon, Youngsang , Kim D, Shin W, (2015) Comparison of Physicochemical Properties Between Fine (PM2.5)
                              and Coarse Airborne Particles at Cold Season in Korea. Science of the Total Environment.
                              STOTEN-D-15-03810.
                            
                           
                             Kwon, Youngsang  and Larsen, Chris (2013) Effects of forest type and environmental factors on forest
                              carbon use efficiency (CUE) using MODIS and FIA data across the eastern USA.  International Journal of Remote Sensing . 34(23): 8425-8488.
                            
                           
                             Kwon, Youngsang  and Larsen, Chris (2013) An assessment of the optimal spatial scale for monitoring
                              of MODIS NPP and FIA NPP across the eastern USA.  Environmental Monitoring and Assessment . 185(9): 7263-7277DOI: 10.1007/s10661-013-3099-1
                            
                           
                             Kwon, Youngsang  and Larsen, Chris. (2012). Use of pixel- and plot-scale screening variables to validate
                              MODIS GPP predictions with Forest Inventory and Analysis NPP measures across the eastern
                              USA.  International Journal of Remote Sensing . 33(19)
                            
                           
                             Kwon, Youngsang  and Song, Inju. (2006). The effect of adjacent landuse type on temperature of landscape
                              patches in Seoul, Korea.  Journal of Korea Planners Association . 41, 235-246. (English)
                            
                           
                            Park, Jonghwa and  Kwon, Youngsang  (2004). Estimating the air temperature cooling effect of the Cheonggyechun stream
                              restoration project of Seoul, Korea.  Journal of the Korean Institute of Landscape Architecture . 2, 120~129. (English)
                            
                           
                         
                        
                        
                      
                     								
                     
                     
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Apply Now!  
                               Interested in a degree in Earth Sciences? Find out how to apply 
                            
                            
                                News and Colloquium  
                               Learn about faculty and student research, how to join Earth Sciences Club, see the
                                 department colloquium schedule, and more...
                               
                            
                            
                                Field Activities  
                               Find out about admissions, prerequisites, location and more 
                            
                            
                                Contact Us  
                               Main office location and numbers, undergraduate and graduate advising 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


