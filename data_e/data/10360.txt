Eunseo Choi@CERI   
 
 
 

   
   
   Eunseo Choi@CERI 
   
   

   
   

   
   
   

   
   
   
   
   

   
  
  
  
  
   
   
  


   
   

 
 

   
   
     
       Eunseo Choi 
     

      
     
       
         
           
               Contact 
           
           
               CV 
           
           
               Research 
           
           
             Publications 
             
               
                 
                   Journal Articles 
                 
                 
                   Talks 
                 
                 
                   Posters 
                 
               
             
           
           
           
             External Links 
             
               
                 
                   CERI Geodynamics Group 
                 
                 
                   CERI Faculty Page 
                 
                 
                   CERI 
                 
                 
                   The U of M 
                 
               
             
             
         
       
     

     
     
       Contact 
       Assistant Professor 
       Center for Earthquake Research and Information 3890 Central Ave. Memphis, TN 38152 USA 
       email: echoi2 at memphis.edu 
     

     
     
       CV 
		  Download a PDF version here . 
		 
           Education 
		   
             Ph.D. in Geophysics, 2008, California Institute of Technology. Advisor:
               Michael Gurnis  
		         B.Sc. in Geology, 1999, Seoul National University. Advisor: Moonsup Cho 
		   
           Positions 
		   
             Assistant professor 2013.01-present, CERI, the Univ. of Memphis, Memphis, TN. 
             Post-doctoral researcher 2012.01-2012.12, Institute for Geophysics, U. Texas, Austin. Advisor:
               Luc Lavier . 
		     Post-doctoral researcher 2008.10-2011.12. Lamont-Dohearty Earth Observatory. Advisor:
           W. Roger Buck . 
		   

		   Grants and Computational Resources 
		   
             2017.03, Fully three-dimensional numerical models for along-axis variations in magmatic and
tectonic processes at slow-spreading mid-ocean ridges (PI: Choi), NSF. 
             2016.10, Earth Cube Building Blocks: Collaborative Proposal: GeoTrust: Improving Sharing and Reproducibility
of Geoscience Applications (PI: Malik), NSF. 
             2013.09, Earth System Bridge: Spanning Scientific Communities with Interoperable Modeling Frameworks (PI: S. Peckham), NSF. 
             2012.09, Landslide dynamics from seismic wave inversion, satellite remote sensing, and numerical modeling (PI: G. Ekstrom), EAR 12-27083. 
		     2010.06, TeraGrid Research Allocation (EAR100019): 1.8 MSU. 
		     2009.06, 3D Models of Faulting during Oblique Continental Extension (PI: W. R. Buck), NSF EAR-0911565. 
		   
		   
         
       

     
     
       Research 
          Research Interests  
         
           Long-term lithospheric deformations in various
              tectonic environments including mid-ocean ridges and
              continental rifts. 
           Advanced numerical techniques for modeling
              large- and multi-scale deformations in geodynamic
              problems. 
         

           Current projects   
         
             Faults formation and evolution associated with plate boundary processes. 
             3D structures at mid-ocean ridges including oceanic core complexes. 
             Seismo-tectonics of intra-plate seismic zones. 
             Enhancing reproducibility in computational tectonic models using GeoTrust 
              For more about the on-going projects,
                visit  my profile on osf.io .  
         
           Completed projects   
         
             Fully-coupled thermomechanical equations and impact on crustal deformation (2014-) 
             Multiple discontinuous decollements and thrust styles (2013-2014) 
             Coupling a tectonic modeling code, SNAC, to a surface modeling code, CHILD (2011-). 
             Landslide modeling (2012-). 
             A novel modeling code development (DynEarthSol2D) (2011-2012). 
             Sequential rider block formation (2011-2012). 
             3D Basin formation at bends of strike-slip faults (2010) 
             Constraints on shallow mantle viscosity from ridge axial morphology and deformation (2009-2010) 
             Geometry of the ridge-transform fault intersection (2005-2007). 
             Spacing of fracture zones in oceanic lithosphere (2006-2007). 
             SNAC development. Co-working with  VPAC  in Australia a Lavier at U. Texas (2003-2006). 
             Geoframework: Developing a framework to couple component codes
                and applying it to modeling crust-mantle interactions (2003-2007). 
             Detecting surface deformation in Iceland using the InSAR
                technique. Advisor: Prof. Mark Simons at California Institute of
                Technology. 
             Modeling the extension of the Basin and Range province. 
         
       

     
     
       Journal Articles 
       

       
       
       
         Tian, X., and E. Choi (2017).   Effects of axially variable diking rates on faulting at slow spreading midocean
          ridges  ,  Earth Planet. Sci. Lett. , 458, 14-21, doi: 10.1016/j.epsl.2016.10.033 . ( PDF ) 
         Logan, L. C., L. L. Lavier, E. Choi, E. Tan, and G. A. Catania (2017).   
          Semi-brittle rheology and ice dynamics in DynEarthSol3D  ,  The Cryosphere , 11, 117-132,
          doi: 10.5194/tc-11-117-2017 .
         Hong, T.-K., E. Choi, S. Park, and J. S. Shin (2016).   Prediction of ground motion and
          dynamic stress change in Baekdusan (Changbaishan) volcano caused by a North Korean nuclear
          explosion  ,  Sci. Rep. , 6, 21477,
          doi: 10.1038/srep21477 .
        ( PDF ) 

         Choi, E., and K. D. Petersen (2015).   Making Coulomb angle-oriented shear bands in numerical
          tectonic models  ,  Tectonophysics , 657, 94–101,
          doi: 10.1016/j.tecto.2015.06.026 
          ( PDF ) 

         Wu, G., L. Lavier and E. Choi (2015).  Modes of continental extension in a crustal wedge ,
           Earth Planet. Sci. Lett. , 421, 89-97.
          doi: 10.1016/j.epsl.2015.04.005 
          ( PDF ) 

         Ta, T., K. Choo, E. Tan, B. Jang and E. Choi (2015).   Accelerating DynEarthSol3D on
          Tightly Coupled CPU-GPU Heterogeneous Processors  ,  Comp. & Geosci. , 79, 27-37.
          doi: 10.1016/j.cageo.2015.03.003 
          ( PDF ) 

         Feng, L., E. Choi and M. J. Bartholomew (2015).  Spatial arrangement of décollements as a control on development of thrust fault systems ,  J. Struct. Geol. , 75, 49-59. doi: 10.1016/j.jsg.2015.03.002  ( PDF ) 

         Choi, E., W. R. Buck, L. L. Lavier, and K. D. Petersen (2013),  Using core complex
            geometry to constrain fault strength ,  Geophys. Res. Lett. , 40,
           doi:10.1002/grl.50732 
          ( PDF ). 

         Logan, L., Catania, G., Lavier, L., and Choi, E. (2013).  A novel method for predicting
            fracture in floating ice .  , 59(216), 750-758.
           doi:10.3189/2013JoG12J210 
          ( PDF ). 

         Choi, E., Tan, E., Lavier, L. L., and Calo, V. M. (2013).  DynEarthSol2D: An efficient
            unstructured finite element method to study long-term tectonic deformation. 
           Journal of Geophysical Research: Solid Earth , 116.
           doi:10.1002/jgrb.50148 
          ( PDF ). 

         

         Choi, E. and W. R. Buck (2012).  Constraints on the Strength of Faults from the Geometry of Rider Blocks
		    in Continental and Oceanic Core Complexes ,  J. Geophys. Res. , 117, B04410,
		   doi:10.1029/2011JB008741 
          ( PDF ). 

         Choi, E. et al. (2011).  One-sided basins at "inverted curtains" along strike-slip
		    faults: Implications for the tectonics at releasing bends ,  Tectonics , 30, TC6006,
		   doi:10.1029/2011TC002943 
          ( PDF ). 

         Choi, E., W. R. Buck (2010).  Constraints on shallow mantle viscosity from morphology
		    and deformation of fast-spreading ridges ,  Geophys. Res. Lett. , 37, L16302,
		   doi:10.1029/2010GL043681 
          ( PDF ). 

         Choi, E., L. Lavier, and M. Gurnis (2008).  Thermomechanics of mid-ocean ridge
		    segmentation ,  Phys. Earth Planet. Int. ,
		   doi:10.1016/j.pepi.2008.08.010 , 171, 374-386
          ( PDF ). 

         Choi, E. and M. Gurnis (2008).  Thermally induced brittle deformation in oceanic lithosphere and
		    the spacing of fracture zones ,  Earth Planet. Sci. Lett. ,
		   doi:10.1016/j.epsl.2008.02.025 , 269 (1-2), 259-270
          ( PDF ). 

         Tan, E., E. Choi, and others (2006).  GeoFramework: Coupling multiple models of
		    mantle convection within a computational framework ,  Geochem. Geophys. Geosyst. , 7,
		  Q06001,  doi:10.1029/2005GC001155 
          ( PDF ). 

         Choi, E. and M. Gurnis (2003).  Deformation in transcurrent and extensional
		    environments with widely spaced weak zones ,  Geophys. Res. Lett. , 30 (2), 1076,
		   doi:10.1029/2002GL016129 
          ( PDF ). 
       
     

     
       Talks  (*: invited)  
       
          *Improving Reproducibility of Numerical Tectonic Models . Workshop
             on Analog Modeling of Tectonic Processes, Austin TX, May 17-19, 2017. 

          *3D numerical models for variable modes of faulting along slow
             spreading mid-ocean ridges . Dept. of Earth Sciences, Texas A&&M, Feb. 3, 2017. 

          *Quantity make Quality: Advances enabled by HPC in Geodynamics .
             Dept. of Earth Sciences, Undergraduate and Graduate class, Texas A\&M, Feb. 2, 2017. 

          Effects of axially variable diking rates on faulting at slow spreading
             mid-ocean ridges . E. Choi and X. Tian. AGU Fall Meeting T32A-07,
             Dec. 12-16, 2016, San Francisco, CA. 

          Coupled Flow and Geomechanical Study of Intraplate Seismicity
            in the New Madrid Seismic Zone . R. Asaithambi, B. Jha and E. Choi.
            AGU Fall Meeting T54B-07, Dec. 12-16, 2016, San Francisco, CA. 

          *3D numerical models for variable modes of faulting along slow spreading mid-ocean ridges  
           Dept. Geology, Southern Illinois University, Nov. 3, 2016. 

          *3D numerical models for variable modes of faulting along slow spreading mid-ocean ridges  
          Wednesday Luncheon Seminar, School of Earth and Environmental Sciences, Seoul National University,
          South Korea. June 8, 2016. 

          *Along-axis variations in diking rates and faulting styles at slow-spreading mid-ocean ridges  
          Seminar series in the Department of Geological and Environmental Sciences,
          Chonnam National University, South Korea. June 1, 2016. 

          *Strong ground motions and dynamic stress changes around Baekdu (Changbai) volcano induced by nuclear
      explosions  
          Department of Earth Sciences Colloquium, the University of Memphis, February 8, 2016. 

          Strong ground motions and dynamic stress changes around Baekdu (Changbai) volcano induced by nuclear
      explosions  
          ES-SSA meeting September 6-7, 2015 in Memphis, TN. 

          Magma explains low estimates of lithospheric strength based on flexure of ocean island loads  
          by W. Roger Buck et al., EGU Annual Meeting 2015. 

          Making SNAC, StGermain and Pyre interoperable through BMI} at EarthCube workshop:
          Numerical Model Metadata for Solid Earth Sciences  
          April 30 - May 2, 2015. Portland State University, Portland, OR. 

          *Quantity makes quality: Advances in geodynamics enabled by large-scale parallel computing  
          Guest lecture in an undergrad earth science class for the civil engineering department
          at Michigan State University. April 14, 2015. 

          A new set of focal mechanisms and a geodynamic model for the Eastern Tennessee
          Seismic Zone  
          by M. Cooley et al., GSA Southeastern Section-64th Annual Meeting (19-20 March 2015) Paper No. 6-10,
          Chattanooga, TN. 

          Development of Core Complex Domes Due to Along-Axis Variation in Diking  
          Buck,W. R., E. Choi and X. Tian. Abstract T53D-05 presented at 2014, Fall Meeting, AGU, San Francisco, CA, 15-19 December. 

          Linking Tectonics and Surface Processes through SNAC-CHILD Coupling  
          CERI Colloquium, Aug. 29, 2014. 

          DynEarthSol3D: An Efficient and Flexible Unstructured Finite Element Method to Study Long-Term Tectonic Deformation  
          E. Tan, E. Choi, L. Lavier and V. Calo, AOGS 11th Annual Meeting, Sapporo, Japan. July 28 - August 1, 2014. 

          *DynEarthSol3D: An Unstructured-Mesh Finite Element Solver for Long-Term Tectonic Deformations Involving Strain Localization  
          2014 CIG Crustal Deformation Modeling Workshop,
          Li Ka Shing Conference Center, Stanford University, 2014.06.23-27. 

          *SNAC Clinic  
          CSDMS Annual Meeting 2014,
          Boulder, Colorado. 2014.05.20-22. 

          *Fault strength and the formation of rider blocks and domes in continental and oceanic core complexes  
          Presented by W. Roger Buck (LDEO)
          EGU General Assembly 2014, Vienna, Austria, 2014.04.27-05.02. 

          *Constraining Normal Fault Strengths Using Rider Blocks  
          Dept. Earth and Env. Sci., Tulane Univ, 2013.01.25. 

          Bridging Surface and Tectonic Processes with SNAC and CHILD  
          Frontiers in Computational Physics: Modeling the Earth System, Boulder, Colorado, 2012.12.20. 

          Finite Element Analysis of Lithospheric Deformation: Introduction to DynEarthSol2D  
          Frontiers in Computational Physics: Modeling the Earth System, Boulder, Colorado, 2012.12.19. 

          *Constraining Normal Fault Strengths from the Geometry of Rider Blocks  
          CIG Workshop on Mantle and Lithospheric Dynamics, Davis, California, USA, 2012.07.30-08.01. 

          *Bridging surface dynamics and tectonic modeling with SNAC  
          CSDMS 2011 ANNUAL Meeting: Impact of time and process scales, Boulder, Colorado, USA, 2011.10.28. 

          Diking as an integral part of rifting process  
          2011 SIAM Conference on Mathematical & Computational Issues in the Geosciences, Long Beah, CA, 2011.3.24. 

          1. Axial morphology and shallow mantle viscosity at fast-spreading ridges. 2. Inverted Curtains: Why some continental transform basins are asymmetric?  
          Geochemistry and Geodynamics Friday Seminar, Woods Hole Oceanographic Institute, 2010.10.8. 

          *SNAC Tutorial  
          GLADE 2010: From grains to global tectonics, Scripps Institution of Oceanography, La Jolla, CA, 2010.07.29.   pdf   

          A numerical approach to localized deformations in oceanic lithosphere  
          Joint MG&G and SG&T Seminar, Lamont-Doherty Earth Observatory, 2009.1.09. 
       
     

     
       Posters 
       
          Calving Geometry of Thwaites Glacier Linked to Semi-brittle Ice Dynamics .
            Liz C. Logan et al. AGU Fall Meeting P31A-2085, Dec. 12-16, 2016, San Francisco, CA. 
          Oedometer test as a benchmark for geodynamic models involving
            strain-weakening plasticity . C. Lee and E. Choi. AGU Fall Meeting T23C-2952,
            Dec. 12-16, 2016, San Francisco, CA. 
          Possible Triggering Of Volcanic Eruption In Baekdusan (Changbaishan)
            Volcano By A North Korea Underground Nuclear Explosion Test? 
             T.-K. HONG et al. AOGS 13th Annual Meeting SE17-A008, July 31-Aug. 5,
             2016, Beijing, China. 
          Improved Thermo-Mechanical theory in long-term tectonic modeling .
            Md. S. Ahamed and E. Choi. CIG All Hands Meeting, June 20-22, 2016, Davis, CA. 
          Coupling long-term tectonic loading with short-term earthquake slip .
            Md. S. Ahamed and E. Choi. CIG All Hands Meeting, June 20-22, 2016, Davis, CA. 
          3D Numerical Models of the Effect of Diking on the Faulting Pattern at Incipient Continental Rifts and Steady-State Spreading Centers  
          X. Tian et al., AGU Fall Meeting 2015, T51E-2945. 
          Making Coulomb angle-oriented shear bands in numerical tectonic models  
          E. Choi and K.D. Petersen, at the 14th International Workshop on Modeling of Mantle and Lithospheric Dynamics, Oleron, France, Aug. 31-Sep. 5, 2015.
         
          EarthCube - Earth System Bridge: Spanning Scientific Communities with Interoperable Modeling Frameworks  
          Peckham, S., C. DeLuca, D. Gochis, J. Arrigo, A. Kelbert, E. Choi, R. Dunlap. Abstract IN31D- 3754 presented at 2014, Fall Meeting, AGU, San Francisco, CA, 15-19 December. 

          Linking Tectonics and Surface Processes through SNAC-CHILD Coupling: Preliminary Results Towards Interoperable Modeling Frameworks  
          Choi, E., A. Kelbert and S. Peckham. Abstract T33B-4683 presented at 2014, Fall Meeting, AGU, San Francisco, CA, 15-19 December. 

          Modes of continental extension in a lithospheric wedge  
          Wu, G., L. Lavier and E. Choi. Abstract T13A- 4613 presented at 2014, Fall Meeting, AGU, San Francisco, CA, 15-19 December. 

          3D Numerical Models for Along-axis Variations in Diking  
          Tian, X. and E. Choi. Abstract T43A-4708 presented at 2014, Fall Meeting, AGU, San Francisco, CA, 15-19 December. 

          A New Set of Focal Mechanisms and a Geodynamic Model for the Eastern Tennessee Seismic Zone  
          Cooley, M., C. Powell, and E. Choi. Abstract T13B-4635 presented at 2014, Fall Meeting, AGU, San Francisco, CA, 15-19 December. 

          Incorporating elastic and plastic work rates into energy balance for long-term tectonic modeling  
          Ahamed, Md S. and E. Choi. Abstract T41A-4592 presented at 2014, Fall Meeting, AGU, San Francisco, CA, 15-19 December. 

          DynEarthSol3D: numerical studies of basal crevasses and calving blocks  
          Logan, E., L. Lavier, E. Choi, E. Tan, and G. Catania. Abstract C33A-0367 presented at 2014, Fall Meeting, AGU, San Francisco, CA, 15-19 December. 

          DynEarthSol3D: An Efficient and Flexible Unstructured Finite Element Method to Study Long-Term Tectonic Deformation  
          E. Tan, E. Choi, L. Lavier and V. Calo, AOGS 11th Annual Meeting, Sapporo, Japan. July 28 - August 1, 2014. 

          Incorporating elastic and plastic work rates into energy balance for long-term tectonic modeling  
          Md. S. Ahamed and E. Choi. CIG Mantle and Lithospheric Dynamics Workshop, Joint with the Canadian Geophysical Union 2014, Banff, Canada, May 4-8, 2014. 

          Modeling the evolution of a thrust system: a geological application of DynEarthSol2D  
          L. Feng, E. Choi, M. Bartholomew. CSDMS Annual Meeting 2014, Boulder, Colorado. May 20-22, 2014. 

          DynEarthSol3D: An Efficient and Flexible Unstructured Finite Element Method to Study Long-Term Tectonic Deformation  
          E. Tan, E. Choi, L. Lavier and V. Calo. CIG-EarthScope Institute for Lithospheric Dynamics, Tempe, AZ. 2014. Feb. 3-4. 

          Modeling the evolution of a ramp-flat-ramp thrust system: A geological application of DynEarthSol2D  
          L. Feng, E. Choi and M. J. Bartholomew. Abstract T31C-2530 presented at AGU Fall Meeting, San Francisco, CA, 9-13 December, 2013. 

          Numerical investigation of the morphological transition of submarine lava flow due to slope change  
          E. Choi, M. Tominaga, M. G. Baker, D. May, E. Fujita and T. Kozono. Abstract V51D-2704 presented at AGU Fall Meeting, San Francisco, CA, 9-13 December, 2013. 

          DynEarthSol3D: An Efficient and Flexible Unstructured Finite Element Method to Study Long-Term Tectonic Deformation  
          E. Tan, E. Choi, L. L. Lavier and V. M. Calo. Abstract DI31A-2197 presented at AGU Fall Meeting, San Francisco, CA, 9-13 December, 2013. 

          Basement-surface coupling of northwest Bengal Basin  
          Md S. Ahamed, D. Hossain and E. Choi. Abstract T41B-2584 presented at AGU Fall Meeting, San Francisco, CA, 9-13 December, 2013. 

          Modes of Extension in Collapsing Orogens  
          G. Wu, L. L. Lavier and E. Choi. GSA Annual Meeting 2013, Denver, CO, Oct. 27-30. 
          Geological Society of America Abstracts with Programs. Vol. 45, No. 7, p.80. 

          Effects of Flat Length on Deformation around a Ramp-Flat-Ramp Thrust System: A Numerical Approach  
          L. Feng, E. Choi and M. J. Bartholomew. GSA Annual Meeting 2013, Denver, CO, Oct. 27-30. 
          Geological Society of America Abstracts with Programs. Vol. 45, No. 7, p.826. 

          DynEarthSol3D: An Efficient and Flexible Unstructured Finite Element Method to Study Long-Term Tectonic Deformation  
          E. Tan, E. Choi, L. Lavier and V. Calo. AOGS Annual Meeting Abstract SE29-A021. 2013. 

          Bounds on fault strength based on geometry of rider blocks , E. Choi, W. R. Buck, L. Lavier and K. D. Petersen,
          AGU Fall Meeting. San Francisco, CA, 2012.12.   pdf   

          3D Numerical Models for Faulting Patterns in Oblique Rifts , E. Choi and W. R. Buck,
          AGU Fall Meeting. San Francisco, CA, 2011.12.   pdf   

          Landslide Rupture and Length-Depth Scaling , Stark, C. P., E. Choi, and M. Convertino, CSDMS 2011 ANNUAL Meeting: Impact of time and process scales,
          Boulder, Colorado, USA, 2011.10.28-30.   pdf    

          Evolution of Fault Populations in 2- and 3-Dimensions , E. Choi and W. R. Buck, AGU Fall Meeting. San Francisco, CA, 2010.12. 

          Asymmetric Basins at "Inverted Curtains" along Strike-Slip Faults , E. Choi and L. Seeber, GLADE 2010: From grains to global tectonics. Scripps Institution of
          Oceanography, La Jolla, CA, 2010.7.26-29. 

          3D Numerical Models for Continental and Oceanic Core Complexes , Choi, E. and W. R. Buck,
          AGU Chapman Conference on Detachments in Oceanic Lithosphere: Deformation, Magmatism, Fluid Flow, and Ecosystems. Agros, Cyprus, 2010.5.8-16.   pdf   

          Can ridge axial deformations constrain mantle viscosity? , AGU Fall Meeting, San Francisco, CA, 2009.12.   pdf    

          Can ridge axial deformations constrain mantle viscosity? , Choi, E. and W. R. Buck, Ridge2000 Integration and Synthesis Workshop: Developing a holistic view of
          oceanic spreading center processes. 2009.10.1-3, St. Louis, MO.  

          Applications of Rate-Dependent Plasticity in Tectonic Modeling , Choi, E. and W. R. Buck, MARGINS Workshop: Rupturing Continental Lithosphere: Synthesis and
          New Perspectives. Charleston, SC, 2009.4.30-5.2. 

          Thermally-induced Brittle Deformation in Oceanic Lithosphere and the Spacing of Fracture Zones , Choi, E. and M. Gurnis, AGU Fall Meeting. San Francisco, CA, 2007.12.
            pdf    

          Introduction to SNAC , CIG CFEM Conference, Golden, CO., 2007.   pdf   

          Factors Controlling the Variations in the Mid-Ocean Ridge Segmentation , Choi, E., L. Lavier, and M. Gurnis, AGU Fall Meeting. San Francisco, CA, 2006.12.
            pdf    

          Coupling Codes through GeoFramework , Choi, E. et al., AGU Fall Meeting. San Francisco, CA, 2004.12. 
       
     
     
     

     
 

    
