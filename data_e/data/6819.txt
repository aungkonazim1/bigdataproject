 Programs - Study Abroad - University of Memphis    










 
 
 
     



 
    
    
     Programs - 
      	Study Abroad
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Study Abroad
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Programs  
                          Students  
                          J-1 Students  
                          Faculty  
                          Funding  
                          Resources  
                          News  
                          About  
                      
                      
                         
                            Programs   
                            
                               
                                  
                                   Deadlines  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Study Abroad
                              	  
                         
                           	Programs
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Programs 
                     
                      Study Abroad can be a life-altering experience from which you can broaden your global
                        perspective. It is not enough to simply get a college degree and have a good GPA to
                        be competitive in the job market after graduation. In today's globalized society potential
                        employers are looking for people with international experience, cross-cultural skills,
                        flexibility, and adaptability that can all be gained on a study abroad experience.
                      
                     
                      The University of Memphis' Study Abroad offers more than 200 semester, academic year
                        and summer programs in over 60 countries to meet the diverse needs of the UofM student
                        population.    Bilateral Exchanges   UofM bilateral exchange programs are programs that offer students the opportunity
                        to study at one of our partner schools abroad for a semester, academic year or summer.
                        Students participating in an exchange program pay UofM tuition and the application
                        fee. Students are responsible for paying their housing fees directly to the host institution.    Faculty–Led Programs   UofM faculty and academic departments organize UM study abroad programs for students.
                        Students participating on a faculty-led program earn UofM credit. Our faculty-led
                        programs are offered during winter and summer sessions as well as fall and spring
                        break. Some faculty-led programs are designed as residency programs while others are
                        travel seminars that provide an in-depth introduction to the country. Since these
                        programs are led by UofM faculty, they provide a wonderful opportunity for students
                        to interact with faculty and share an international experience.
                      
                     
                        Internships Abroad   Participation in an internship overseas can give you the professional advantage you
                        need to succeed by giving you in demand global skills. Placements are available in
                        English at various sites across the world.    Service Learning Abroad   Service learning programs provide an opportunity to engage in community service at
                        non-profit organizations, health clinics, schools, and local government offices abroad.
                      
                     
                        UofM Sponsored Programs   To enable us provide more options for study abroad locations the UofM has affiliations
                        with some study abroad program providers. Students may apply to study abroad on any
                        of our affiliated programs. Program costs, application fees and requirements for sponsored
                        programs vary and students should contact the study abroad office for information
                        on the individual sponsors.
                      
                     
                      
                        
                          Alliance for Global Education  
                        
                          Academic Programs International (API)  
                        
                          CAPA International Education  
                        
                          CEA Study Abroad  
                        
                          College Consortium for International Studies (CCIS)  
                        
                          Institute for Study Abroad (IFSA) - Butler University  
                        
                          International Studies Abroad (ISA)  
                        
                          International Student Exchange Program (ISEP)  
                        
                          Tennessee Consortium for International Studies (TNCIS)  
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Programs 
                         
                            
                               
                                Deadlines  
                            
                         
                      
                      
                      
                         
                            
                                Log-In  
                               Log-In to your application 
                            
                            
                                Search Programs  
                               Search Study Abroad programs 
                            
                            
                                Scholarships  
                               Search scholarship opportunities for Study Abroad 
                            
                            
                                Contact Us  
                               Email, call or schedule appointments 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


