History - UofM Global - University of Memphis    










 
 
 
     



 
    
    
    History - 
      	UofM Global
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






 

   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 

   
   
   
    
      
      
       
 
	
	
	
 
 

 


 
 

 
  
 
	 
		 
			 
				 
					     
				 
				     
				 
					 
						 
							 
								 
									   UofM Home  
										  myMemphis  
										  eCourseware  
										  Webmail  
										  Contact  
								     
								 
								 
									 Search 
									 
									 
									    
									 Site Index 
								 																
								  
							 
						 
					 
					 
						  
							 
								   About UofM Global      
									 
										  UofM Global  
										  Ranked Online Degrees  	
										  The UofM Global Experience  
										  Our Faculty  
										  Tuition   Fees  	
										  Accreditations  
									 
								 
								   Online Degrees      
									 
										  Online Degrees  
										  Undergraduate Degrees  
										  Graduate Degrees  
										  Graduate Certificates  
										  Online Admissions  
										  Contact Us  
									 
								 
								   Online Admissions      
									 
										  Online Admissions  
										  Apply Now   
										  Smart Start  
										  Financial Aid  
										  All Online Services  
									 
								 
								   Current Students      
									 
										  Student Services and Support  
										  Online Advising  
										  Course Registration  
										  Alternative Degree Pathways  
										  Learning Support  
										  Technology Support  
										  Library Services  
										  Career Services  
										  Additional Services  
										  Graduation  
									 
								 
								  Request Info  
							 
						 
					 
				 
			 
		 
		 
			 
				 
					  Online Degrees  
				 
			 
		 
	 
 
 
      
      
      
    
    
       
          
             
                
                   
                       
                           			UofM Global
                           	  
                   
                
             
          
       
       
          
             
                
                   
                      
                         
                             Home  
                             
                                 	UofM Global
                                 	  
                             
                                 	Degree Programs
                                 	  
                            History 
                         
                      
                      
                         
                            Degree Programs   
                            
                               
                                  
                                   Online Degrees  
                                   Undergraduate Degrees  
                                         All Undergraduate Degrees  
                                         Business  
                                         Health Professions  
                                         Humanities  
                                         Social Science  
                                     
                                  
                                   Graduate Degrees  
                                         All Graduate Degrees  
                                         Business  
                                         Education  
                                         Engineering  
                                         Health Professions  
                                         Humanities  
                                         Social Science  
                                     
                                  
                                   Graduate Certificates  
                                   Online Admissions  
                                   Contact Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                     
                     		
                     
                     
                      History 
                     
                      Online B.A. and M.A. program in History, courses taught by dedicated online instructors
                        and tenure/tenure-track faculty, ranging from ancient to modern content, global in
                        scope. Full range of courses are available to B.A. students, including internships,
                        directed readings courses, and most of the courses currently offered in the undergraduate
                        catalog. M.A. offerings broad in scope and scale, but currently the Egyptology concentration
                        is not available online. All students receive comprehensive advising, including career
                        counseling, graduate school workshops, and exposure to other professional development
                        opportunities.
                      
                     
                      The BA in History requires a minimum of 120 hours to graduate. You may complete this
                        degree entirely online through UofM Global or in combination with previously completed
                        college level learning from other sources. For additional information, please see
                        the  History Undergraduate Catalog .
                      
                     
                      
                        
                            Contact Us  
                        
                            Apply Now  
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Degree Programs 
                         
                            
                               
                                Online Degrees  
                                Undergraduate Degrees  
                                      All Undergraduate Degrees  
                                      Business  
                                      Health Professions  
                                      Humanities  
                                      Social Science  
                                  
                               
                                Graduate Degrees  
                                      All Graduate Degrees  
                                      Business  
                                      Education  
                                      Engineering  
                                      Health Professions  
                                      Humanities  
                                      Social Science  
                                  
                               
                                Graduate Certificates  
                                Online Admissions  
                                Contact Us  
                            
                         
                      
                      
                     
                     
                      
                                 Need Help? 
                                 
                                     
                                         
											     Call 844-302-3886 
                                         
                                     
                                     
                                         
                                                 Email Us 
                                         
                                     
                                 
                             
                     
                     
                     
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
 
 
 
 
  Full sitemap  
 
 
 
 
  About UofM Global  
 
 
  UofM Global  
  Ranked Online Degrees  	
  The UofM Global Experience  
  Our Faculty  
  Tuition   Fees  	
  Accreditations  
 
  
 
  Online Degrees  
 
 
  Online Degrees  
  Undergraduate Degrees  
  Graduate Degrees  
  Graduate Certificates  
  Online Admissions  
  Contact Us  
 
  
 
 
 
  Online Admissions  
 
 
  Online Admissions  
  Apply Now   
  Smart Start  
  Financial Aid  
  All Online Services  
 
  
  Contact Us  
 
  Tuition  
  Financial Aid  
  
 
	  Current Students  
 
 
  Student Services and Support  
  Online Advising  
  Course Registration  
  Alternative Degree Pathways  
  Learning Support  
  Technology Support  
  Library Services  
  Career Services  
  Additional Services  
  Graduation  
 
  
 
 
 
 
 
   
 
 
   
 
 
   
 
 
 
 
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/29/17 
                                  
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


