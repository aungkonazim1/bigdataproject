Viewing a CaptureCast presentation | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Viewing a CaptureCast presentation | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Capture    »    Participating in CaptureCast presentations   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Viewing a CaptureCast presentation                       
        
        
       
        
     
              
	  

 
	A CaptureCast presentation viewer's interface
 

 
	You can view live and on-demand CaptureCast presentations from the following locations:
 

  
		Capture Portal
	 
	
	 
		Capture Central
	 
	
	 
		Embedded as course content in Learning Environment
	 
  
	Toggle views
 

 
	The following toggle view icons allow you to manage how slides, screen captures, and video appear in the viewer:
 

  
		  Side-by-side 
	 
	 
		  Fit to screen 
	 
	 
		  Picture in picture 
	 
	 
		  Swap video and slides 
	 
  
	Search and navigate a presentation
 

 
	Click the   Search & Navigation  icon to open the chapters and slides menu. The menu lists all chapters and slides and the time they appear during the presentation. In the menu, you can hover over the   Thumbnail  icon to view a thumbnail of the slide. Click on a listed title to navigate to it in the presentation. Enter a search query in the text box at the bottom of the menu to search within the presentation for specific chapters and slides.
 

 
	Toggle closed captions
 

 
	If a presentation also contains closed captions, you can toggle closed captions by clicking the   Turn on/off subtitles  icon.
 

 
	Participating in a live event chat
 

 
	Send a message to all participants
 

  
		In the live event's Chat tab, click the   Send a regular message  icon.
	 
	 
		 Enter your message  in the text box and press Enter on your keyboard.
	 
  
	  

 
	Enter a chat message
 

 
	Ask a general question
 

  
		In the Chat tab, click the   Ask a question  icon.
	 
	 
		 Enter your message  in the text box and press Enter on your keyboard. Questions are reviewed and answered at the discretion of the presenter.
	 
  
	  

 
	Enter a general question
 

 
	Private message a participant
 

  
		Double-click on a participant's name in the chat attendance list to open a private chat tab.
	 
	 
		 Enter your message  in the text box and press Enter on your keyboard. Messages entered in private chat are only visible to the person you are communicating with.
	 
      Audience:     Learner       

    
           

                   ‹ Accessing Capture Portal 
        
                   up 
        
                   Viewing CaptureCast presentations offline and in mobile playback › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Capture  
  
      Participating in CaptureCast presentations    Accessing Capture Portal    Viewing a CaptureCast presentation    Viewing CaptureCast presentations offline and in mobile playback      Understanding Capture components    Using Capture    Capture Station    Editing in post-production    Understanding Capture Toolbox    Capture Central in Learning Environment    
                  
           
         

       
     

    
    
    
   
 
   
 
