2016 Student Research Forum Winners - SRF - University of Memphis    










 
 
 
     



 
    
    
    2016 Student Research Forum Winners - 
      	SRF
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Student Research Forum
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Preparation  
                          Schedule  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	SRF
                              	  
                         2016 Student Research Forum Winners 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      2016 Student Research Forum Winners 
                     
                      To see pictures of the winners, please view the album on the   Graduate School facebook page  .
                      
                     
                       graduate student winners  
                     
                       Business  1st: Toluwalope Ayangbayi, Department of Economics 2nd: Farrah Zeba, Department of Economics
                      
                     
                       Education  1st: Daniel Taylor, Department of Instruction and Curriculum Leadership and Tegan
                        Jemma Reeves, Department of Counseling, Educational Psychology, and Research 2nd: Kenneth Barideaux Jr, Department of Psychology   Engineering  1st: Ahmed Abuhussein, Department of Electrical Engineering 2nd: Joel Berretta, Department of Biomedical Engineering
                      
                     
                       Liberal and Fine Arts  1st: Jessica Johnson, Department of Art 2nd: Kevin Ryan, Department of Philosophy
                      
                     
                       Life and Health Sciences  1st: Caitlin Nelms, School of Communication Sciences and Disorders 2nd: Lisa Dale, Department of Communication
                      
                     
                       Math and Computer Sciences  1st: Ali Dogan, Department of Mathematics 2nd: Abhijit Nag, Department of Computer Science   Physical and Applied Sciences  1st: Seyed Mostafa Mousavi, Department of Earth Sciences 2nd: Alexander Olinger, Department of Biomedical Engineering   Social and Behavioral Science  1st: Breya Walker, Department of Psychology 2nd: Stephanie McMillian, School of Communication Sciences and Disorder and Lisa Mintz,
                        Department of Psychology
                      
                     
                       undergraduate student winners  
                     
                       Business:  1st: Shihui Shen, Hospitality and Resort Management
                      
                     
                       Engineering:  1st: Emmanuel Lim, Meg Homeyer and Michael Hollandsworth, Department of Biomedical
                        Engineering 2nd: Allen Mamaril, Department of Biomedical Engineering
                      
                     
                       Liberal and Fine Arts:  1st: Hunter Rhodes, Department of English
                      
                     
                       Life and Health Sciences:  1st: Emily Adamic, Health and Human Performance 2nd: Kendall Major, Department of Biology
                      
                     
                       Physical and Applied Sciences:  1st: Katherine Mitchell, Department of Physics 2nd: Elizabeth Lee, Department of Earth Sciences
                      
                     
                       Social and Behavioral Sciences:  1st: Maurice Pian, Department of Psychology 2nd: Eric Mitten, Department of Psychology
                      
                     
                       Math and Computer Sciences:  1st: Ashlesh Gawande and Muktadir Chowdhury, Department of Computer Science 2nd: Kevin Townsend, Department of Computer Science
                      
                     
                       Education:  1st: Anqi Wang, Hospitality and Resort Management
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Register for 2017 Student Research Forum  
                                
                            
                            
                                2016 Student Research Forum Winners  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


