Collateral Coding System - Marketing and Communication - University of Memphis    










 
 
 
     



 
    
    
    Collateral Coding System - 
      	Marketing and Communication
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Marketing and Communication
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          The Brand  
                          Our Team  
                          Resources  
                          Social Media  
                          Web  
                          Events  
                          News  
                      
                      
                         
                            Resources   
                            
                               
                                   Branded Templates  
                                         PowerPoint Slides  
                                         Stationery  
                                         Email Templates  
                                         Email Signatures  
                                         Employee Announcements  
                                         Retirement Receptions  
                                         Flyers   Posters  
                                         Cards   Certificates  
                                         Nametags  
                                         Place Cards   Table Tents  
                                         Business Cards  
                                     
                                  
                                   Photo and Video Resources  
                                   TV and Radio Appearances  
                                   Project Request Form  
                                   Affirmative Action Statements  
                                   Collateral Coding Statements  
                                   Approved Vendors  
                                   Frequently Asked Questions  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Marketing and Communication
                              	  
                          
                              	Resources
                              	  
                         Collateral Coding System 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Collateral Coding Standards 
                     
                       Certain University collateral material  is required to carry a production code and an affirmative action statement. The production
                        code consists of a series of numerals and letters denoting that the piece is a University
                        of Memphis project, and that project's number, the quantity of pieces printed and
                        the identity of the printer. The preferred placement of this code is .125" to .25"
                        from the bottom right edge of the back cover, and it should be typeset in 5 point
                        Formata Bold Condensed.
                      
                     
                      What is considered printed material for tracking and cost submitting purposes is defined
                        below:
                      
                     
                      Printed Materials used for communicating university related messages and paid for
                        with State funds including:
                      
                     
                      
                        
                         Magazines 
                        
                         Booklets/Reports 
                        
                         Newsletters 
                        
                         Brochures / pamphlets when page count is greater than one 
                        
                         Posters 
                        
                         Any items that use a 4 color (or full color) process in bulk orders (200+) including:
                           
                            
                              
                               Postcards 
                              
                               Flyers 
                              
                               Promotional giveaway items printed on paper products 
                              
                            
                           
                         
                        
                      
                     
                       What is not included is:
                      
                     
                      
                        
                         Any item not paid for with State funds 
                        
                         Stationery 
                        
                         Academic-/Course-/Instruction-related materials 
                        
                         Promotional giveaway items not printed on paper (e.g. T-shirts, plastic items, bags) 
                        
                         Electronic productions (e.g. PDF files, web pages, electronic forms, electronic brochures,
                           electronic newsletters, etc.)
                         
                        
                         The cost of shipping and handling is not to be included 
                        
                      
                     
                        
                     
                      Additionally, all collateral is required to carry an affirmative action statement.
                        Placement within the publication is discretionary. The complete text and proper usage
                        criteria of the four approved affirmative action statements are on the  affirmative action statements  page.
                      
                     
                       Sample Affirmative Action Statement and Collateral Code  
                     
                        The University of Memphis offers equal opportunity to all persons without regard to
                              race, religion, sex, creed, color, national origin or disability. The University does
                              not discriminate on these bases in recruitment and admission of students or in the
                              operation of its programs and activities, as specified by federal laws and regulations.
                              Designated coordinators for University compliance with Section 504 of the Rehabilitation
                              Act of 1973 and the Americans with Disabilities Act of 1990 are the Vice President
                              for Student Affairs and the Equal Opportunity Compliance Officer. Information in this
                              document will be provided in alternate format upon request. The University of Memphis
                              is an Equal Opportunity/Affirmative Action University. It is committed to education
                              of a non-racially identifiable student body.   
                     
                        UOM169-FY1516/1.5M STARR-TOOF PRINTING 670 S COOPER ST MEMPHIS TN 38104   
                     
                         
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Resources 
                         
                            
                                Branded Templates  
                                      PowerPoint Slides  
                                      Stationery  
                                      Email Templates  
                                      Email Signatures  
                                      Employee Announcements  
                                      Retirement Receptions  
                                      Flyers   Posters  
                                      Cards   Certificates  
                                      Nametags  
                                      Place Cards   Table Tents  
                                      Business Cards  
                                  
                               
                                Photo and Video Resources  
                                TV and Radio Appearances  
                                Project Request Form  
                                Affirmative Action Statements  
                                Collateral Coding Statements  
                                Approved Vendors  
                                Frequently Asked Questions  
                            
                         
                      
                      
                      
                         
                            
                                Media Resources  
                               News, Publications and Upcoming Events 
                            
                            
                                Brand Standards  
                               Everything you need to know about utilizing the UofM brand 
                            
                            
                                Marketing Toolkit  
                               Download templates, find a vendor and more 
                            
                            
                                Contact Us  
                               Let us help with your marketing needs 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


