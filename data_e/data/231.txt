Hooks Grant - UofM Media Room - University of Memphis    










 
 
 
     



 
    
    
    Hooks Grant - 
      	UofM Media Room
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			UofM Media Room
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Publications  
                          News Releases  
                          Events  
                          Resources  
                          Contact Us  
                      
                      
                         
                            News Releases   
                            
                               
                                  
                                   Awards  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	UofM Media Room
                              	  
                          
                              	News Releases
                              	  
                         Hooks Grant 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Benjamin L. Hooks Institute for Social Change Receives $10,000 Grant From Ingersoll
                        Rand
                      
                     
                      October 26, 2017 - The Benjamin L. Hooks Institute at the University of Memphis has
                        received a $10,000 grant from the Ingersoll Rand Foundation to support the Hooks African-American
                        Male Initiative (HAAMI). In 2015, the Hooks Institute launched HAAMI to increase the
                        retention and graduation rates of African-American male students at the UofM.
                      
                     
                      Current data shows that for first-time, full-time students who graduated within a
                        four-year period, African-American males have demonstrated an average graduation rate
                        of 10.6 percent, significantly less than all other groups. The Hooks Institute's efforts
                        with HAAMI are intended to help create prosperity for African-American males, which
                        in turn helps to collectively benefit the lives of all Memphians and to create positive
                        social change for the Mid-South and the nation.
                      
                     
                      "Trane is a proud partner with the University of Memphis along with our commitment
                        to the Memphis community," said Jim Crone, West Tennessee business development director
                        at Trane Mid-South, a brand of Ingersoll Rand. "We feel our support of HAAMI is a
                        contribution to the long-term sustainability of HAAMI's mission to increase the graduation
                        rate of African-American males, thus contributing to the overall success of diversity
                        in our community."
                      
                     
                      The Ingersoll Rand Foundation grant will provide general operating support for the
                        HAAMI program, support for HAAMI sessions and cultural outings, equipment, technology
                        and support for a HAAMI resource center and HAAMI promotion. Funding will support
                        HAAMI students as they work towards graduation.
                      
                     
                      "The Ingersoll Rand Foundation has been a great University of Memphis partner, particularly
                        with its generous support of the Hooks African-American Male Initiative," said Rorie
                        Trammel, assistant director and HAAMI coordinator. "We are grateful for the financial
                        support, which enables us to offer an enriching program for University of Memphis
                        students, as well as for the direct engagement of Trane employees in HAAMI program
                        activities."
                      
                     
                       About the Ingersoll Rand Foundation  The Ingersoll Rand Foundation is committed to advancing the quality of life through
                        charitable partnerships to help build safe, comfortable, efficient and educated communities.
                        Ingersoll Rand is committed to good corporate citizenship and believes that advancing
                        the quality of life requires taking an active role in addressing the social issues
                        impacting our company and communities.
                      
                     
                       About the Benjamin L. Hooks Institute for Social Change  The Benjamin L. Hooks Institute implements its mission of teaching, studying and promoting
                        civil rights and social change through research, education and direct intervention
                        programs. For 20 years, we have addressed disparities related to education, diversity
                        and inclusion, economic mobility and civic engagement. To learn more about our current
                        initiatives, visit our website at  www.memphis.edu/benhooks .
                      
                     
                      FOR MORE INFORMATION Nathaniel C. Ball 901.678.3655
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         News Releases 
                         
                            
                               
                                Awards  
                            
                         
                      
                      
                      
                         
                            
                                Music Performance Calendar  
                               Don't miss a School of Music performance 
                            
                            
                                Art Museum of Memphis  
                               Learn about the latest installations 
                            
                            
                                The Martha and Robert Fogelman Galleries of Contemporary Art  
                               View upcoming Department of Art exhibits 
                            
                            
                                Contact Us  
                               Have a story to share or questions about UofM News? 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


