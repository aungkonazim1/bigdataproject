Steve Horton   
 
 
   
   Steve Horton 
 
 
 
 
   
     
          Steve Horton's    
        
     
     
       
                     
                      
                  
                   
       
     
     
          Research Overview:   
I study earthquakes especially their source characteristics and their
effects on the built environment. Quantifying earthquake hazard 
and understanding the occurrence of earthquakes particularly in the
Central US is an important research focus. 
       
        Outreach Overview:   
I am invovled in a number of community outreach activities. I hold
Earthquake Town Meetings in at risk communities in the central US. I am
working to  
formalize plans with emergency management
agenciesin the Central US to open information clearinghouses following
significant
earthquakes. 
          Current and Recent Research
Projects       
Seismotectonics of the New Madrid Seismic Zone: 
            
       
           Earthquake Location     
          Earthquake Focal Mechanisms     
             
           Aftershock
studies        :  
          
            
       Explanation:    A tea hut near Anjar,
India. Following the Mw=7.7 Bhuj Earthquake of 26 January 2001, CERI
deployed a temporary seismic network to record aftershocks. See
Aftershock link for details.   
          
              
             Mount
St. Helens   
          
                      
                  Explanation:    Mount
St. Helens from south west following steam eruption. A network of eight
broadband seismometers was installed at Mount St. Helens in October
2004 to record seismic activity associated with the dome-building
eruption that began on September 23. The Cascades Volcano Observatory
installed two broadband seismometers northwest of the Mount St. Helens
crater in October 2004. CERI installed six temporary stations at
complimentary azimuths to provide better coverage of the eruption at
about the same time. CERI removed the temporary stations in May 2005.
See Mount St. Helens link for details. 
           
             Ambient
Ground Motion Vibration   
           
                 
          
           
          Explanation: 
Observations of the Horizontal to Vertical
power spectral ratio of ambient ground motion from 3 Basins. The H/V
peaks are correlated with sediment thickness in each basin. All 3 show
similar trends suggesting that the process that produces the peak in
the ratio is similar in each basin. And that the seismic proerties of
the sediments in each basin are also similar. We hope to include more
observations from basins worldwide. See Ambient link for details. 
         
         
        
     
     
         CERI Home   
     
   
 
 
 
 
