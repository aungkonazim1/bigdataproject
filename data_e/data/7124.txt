Creating live events | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Creating live events | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Capture    »    Using Capture   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Creating live events                       
        
        
       
          
     
           Printer-friendly version       
	Before viewers can watch a live webcast from Capture Portal or Learning Environment, you must create and schedule the live event. Once you create a live event, its information and access link appear on Capture Portal's Calendar page and in the Live Event Schedule widget on the Capture Portal Home page. If you embed a live event in Learning Environment, the event pauses in the Capture viewer until the presenter begins the live webcast session. The Capture viewer automatically loads the live webcast once the presentation begins.
 

 
	 Note If you are creating a live event for a Capture Station recording, see  Creating automated live events  for more applicable procedures.
 

 
	Create a live event in Capture Portal or Capture Central
 

  
		Do one of the following:
		  
				Log in to Capture Portal and click  Admin .
			 
			 
				In Learning Environment, click    Capture Central  on your course navbar.
			 
		  
	 
		On the Manage Live Events page, click  Create Live Event .
	 
	 
		Enter a  Title  for your live event.
	 
	 
		Enter the name of the  Presenters  and a  Description  of the event.
	 
	 
		You can add  Tags  to the live event. Tags are keywords or terms that describe the presentation and allow it to be found through Search. Tags are keywords or terms that describe the presentation and allow it to be found through Search. Press Enter or comma ( , ) on your keyboard after you enter a tag to add it.
	 
	 
		Enter the  Start/End Times  in the date and time fields. If you want to create an event that spans across multiple days, select the  Show end date  check box to modify your event end date.
	 
	 
		In the Layout Settings section, select a presentation layout.
		 
			 Note Presenters can still modify the layout of the presentation before they start presenting with their Capture Software if they want to change what you set.
		 
	 
	 
		In the Access Control section, you can do the following:
		  
				If you want to create a password for an event, select the  Event Password Required  check box and enter a password in the text field. Password protected events require viewers to enter the password to access the event.
				 
					 Note Password protected live events require the same password to access the presentation after it is published. You can change published presentations' passwords in the Manage Presentations area.
				 
			 
			 
				If you do not want users to access a live event's chat room, select the  Chat Disabled  check box.
			 
			 
				Select the  Viewer Limit  check box and enter the maximum number of viewers in the text field.
			 
			 
				 
					 Note If the viewer limit is above 750, the live event's waiting area is disabled. If the viewer limit is above 1250, chat is disabled.
				 
			 
		  
	 
		In the Permissions area, enter your search criteria in the search field to find a user, group, or role you want to add to the permissions list to indicate what specific users can do for this presentation. You can set the following permissions from the drop-down list:
		  
				 Can View   Can view presentation only.
			 
			 
				 Can View and Manage   Can view, manage, and edit presentation. Can embed presentation into Learning Environment course content. For example, a user with Admin access rights can create an event, search for multiple instructors' names and specify that they can view and manage the presentation so they can post it to their Learning Environment.
			 
		  
	 
		In the Pre-roll/Post-roll section, you can select a clip from the  Pre-roll clip  and  Post-roll clip  drop-down lists to add video before and after your presentation. You can select the  Users can skip pre-roll and post-roll video clips  check box to make viewing optional.
		 
			
		 
	 
	 
		Click  Create .
	 
  
	
 
     Audience:    Instructor      

    
           

                   ‹ Publishing a CaptureCast presentation 
        
                   up 
        
                   Managing live events › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Capture  
  
      Participating in CaptureCast presentations    Understanding Capture components    Using Capture    Webcasting a live CaptureCast presentation    Recording an offline CaptureCast presentation    Publishing a CaptureCast presentation    Creating live events    Managing live events    Managing published CaptureCast presentations and playlists    Managing folders    Presenting and publishing with Web Capture    Managing participants in live event chat    Understanding access controls    Uploading non-Capture videos    Splitting long CaptureCast presentations into playlists    Creating playlists from published presentations    Purging deleted CaptureCast presentations      Capture Station    Editing in post-production    Capture Central in Learning Environment    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
