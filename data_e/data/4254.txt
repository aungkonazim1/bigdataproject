King Thom Chung - Biological Sciences - University of Memphis    










 
 
 
     



 
    
    
    King Thom Chung - 
      	Biological Sciences
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Biological Sciences
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Undergraduate  
                          Graduate  
                          Centers  
                          People  
                          Research  
                          Events  
                          Scholarships  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Biological Sciences
                              	  
                          
                              	People
                              	  
                         King Thom Chung 
                      
                   
                   
                       
                      
                     
                     	
                     
                     		  
                      
                        
                        
                         
                           
                           
                                                                
                              
                              
                               
                              
                              
                            
                           
                           
                            
                              
                               
                                 
                                 King Thom Chung
                                 
                               
                              
                              
                               
                                 
                                 Professor
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Phone 
                                    
                                     
                                       
                                       901.678.4458
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Email 
                                    
                                     
                                       
                                        kchung@memphis.edu 
                                        
                                    
                                  
                                 
                                  
                                    
                                     Fax 
                                    
                                     
                                       
                                       901.678.4457
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Office 
                                    
                                     
                                       
                                       Life Science 529
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Office Hours 
                                    
                                     
                                       
                                       M-F 10:30 am to 6:00 pm
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 										
                                 
                                 
                                  I am an anaerobic microbiologist ,with special interest in the microbial functions
                                    in the  gastrointestinal tracts of human and animals , particularly their roles in
                                    cancer formation.  I am a member of American Society for microbiology, Institute of
                                    food Technologists,  Environmental Mutagenesis and Genomics Society.  I constantlyreview papers for   Environmental and Molecular Mutagenesis  ,   Food and Chemical Toxicology,  Mutation Research,   Toxicological In Vitro, etc.    Since 2009 I have been the managing editor f or the special issue of Arylamine Induced Carcinogenesis of Fronitiers in Biosciences.
                                         
                                 
                                  Currently I am teaching  Introduction to Microbiology  (Biol 1230), Public Health Microbiology (Biol 7080/8080) and Food and Industrial
                                    Toxicology (Biol 7555/8550).
                                  
                                                                         
                                 
                               
                              
                            
                           
                           
                         
                        			 
                        			  
                        
                        
                           
                        
                         Education 
                        
                         I came to the University of Memphis in 1988. I have a B.S. degree (1965) from National
                           Taiwan University (Taipei) major in Agricultural Chemistry; M. A. degree (1967) major
                           in Biology from University of California, Santa Cruz;  and Ph. D. (1972) major in
                           microbiology from University of California.  Previously, I served as a visiting scientist
                           at the USDA Roman, ARD USDA, ARS, Roman L. Hruska U.S. Meat Animal Research Center,
                           Clay Center, Nebraska; Associate Professor in Tunghai University (Taichung, Taiwan); Professor in Soochow University (Taipei); Scientist
                           in Frederic Cancer Research Facility in Frederick, Maryland.   
                         
                        
                           
                        
                         Recent Publications 
                        
                         Tsay, J.-G., K.-T. Chung, W.-L. Chen, C.-H. Chen, M. H.-C. Lin, F.-J. Lu  and C.-H.
                           Chen. Calvatia lilacina protein-extract induces apoptosis through glutathione depletion
                           in human colorectal carcinoma cells.  J. Agri. Food Chem., 57: 1579-1588, 2009
                         
                        
                         Makena, P. S. and K.-T. Chung.  Comparative mutagenic effects of structurally similar
                           flavonoids, quercetin and taxifolin on the tester strains Salmonella typhimurium TA102
                           and  Escherichia coli WP-2.  Environ.   Mol. Mutagenesis, 50(6): 451-459, 2009.
                         
                        
                         Chen, Ching-Hsein, Yu-Jia Chang, Maurice S.B. Ku,  King-Thom Chung, and Jen-Tsung
                           Yang.  Enhancement of temozolomide- induced apoptosis by valproic acid in human glioma
                           cell lines through redox regulation.  J. Molecular Medicine (Berlin, German), 89(3):303-315, 
                           2011. 2011.DOI 10.1007/s00109-019-0707-1.
                         
                        
                         Chen, S.-C., Y.-C Hseu, J.Sung, C.-H. Chen, L.-C. Chen and K.-T. Chung.  Induction
                           of DNA damage signaling genes in benzidine-treated HepG2 cells. Environ.Mol. Mutagenesis
                           52: 664-672, 2011,  2011. (DOI: 10.1002/em.20669.)Wu, Y. -I., C. -H. Chen, Wen-Huei
                           Chang, K.-T. Chung, F. -J. Lu, Y.-W. Liu  and C. -H.
                         
                        
                         Anti-cancer effects of the protein extractions from Calvatia lilacina, Pleurotus ostreatus
                           and Volvaria volvaceae . Evidence Based on Complementary and Alternative Medicine,
                           Vol. 2011 (article ID 982368):1-10, 2011. (Doi:10.1093/ecam/neq057.)
                         
                        
                         Godapudi, G. and K.-T. Chung.  Comparative genotoxicity of 3-hydroxyanthranilic acid
                           and anthranilic acid in the presence of metal cofactor Cu(II) in vitro. Mutat. Res.,26:200-208,
                           2011. (doi:10.16/j.mrgentox.2011.09.012.)
                         
                        
                         Chen, C-Y, Z-L.  Li, K.-T. Chung, F. -J. Lu and C. –H. Chen. 2014. Liriodenine enhances
                           the apoptosis effect of valproic acid in human colon cancer cells through  oxidative
                           stress upregulation and Akt inhibition.  Process Biochemistry, 49:1990-2000.
                         
                        
                         Chung, K.-T.2013. The etiology of bladder cancer and its prevention. J. Cancer Science
                           and Therapy. 5(10):346-361. Doi:10.4172/1948-5956.1000226.Chung, K.-T.  2015. Occurrence,
                           uses, and carcinogenicity of arylamines.  Frontiers in Bioscience, Elite: 367-393Chung,
                           K.-T. 2015. Carcinogenicity, allergenicity, and lupus-inducibility of arylamines  Frontiers
                           in Biosciences? In press, 2015.
                         
                        
                           
                        
                        
                      
                     								
                     
                     
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Biology@Memphis Newsletter  
                               Read about graduate and undergraduate student news; awards, grants and scholarships;
                                 publications, and more...
                               
                            
                            
                                Administration  
                               Budgets, accounting, facilities management, committees, meetings, and group email
                                 accounts
                               
                            
                            
                                Facilities  
                               Not your ordinary classrooms... 
                            
                            
                                Contact Us  
                               Main office contacts, faculty and staff administration 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/28/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


