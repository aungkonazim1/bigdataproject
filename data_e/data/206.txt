JD/MPH - School of Law - University of Memphis    










 
 
 
     



 
    
    
    JD/MPH - 
      	School of Law 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Cecil C. Humphreys School of Law
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Admissions  
                          Programs  
                          Current Students  
                          Faculty  
                          Careers  
                          Library  
                      
                      
                         
                            Programs   
                            
                               
                                  
                                   Degree Programs  
                                        
                                         JD  
                                        
                                         JD/MBA  
                                        
                                         JD/MA in Pol. Science  
                                        
                                         JD/MPH  
                                        
                                         MPH Fast-Track  
                                        
                                         Part-time Program  
                                     
                                  
                                   Certificate Programs  
                                         Advocacy  
                                         Business Law  
                                         Health Law  
                                         Tax Law  
                                     
                                  
                                   Experiential Learning  
                                         Experiential Learning Home  
                                         Legal Clinics  
                                         Externship Program  
                                     
                                  
                                   Law Review  
                                         Law Review Home  
                                         Editorial Board  
                                         Editor-in-Chief  
                                         Joining Law Review  
                                         Virtual Tour of Law Review Suite  
                                         Symposium  
                                         Subscriptions  
                                         Archives  
                                         Current Volume  
                                         Connect with Us  
                                     
                                  
                                   Advocacy Program  
                                         Moot Court Board  
                                         Joining Moot Court Board  
                                         In-School Competitions  
                                         Travel Team Competitions  
                                         Past Champions  
                                     
                                  
                                   Inst. for Health Law   Policy  
                                         Institute Home   
                                         Advisory Committee  
                                         Symposium  
                                         Pro Bono Opportunities  
                                         Health Law Certificate  
                                         Health Law Society  
                                         MBA Health Law Section  
                                     
                                  
                                   International Law Programs  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	School of Law 
                              	  
                          
                              	Programs
                              	  
                         JD/MPH 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      JD/MPH PROGRAM 
                     
                      Public health, public policy and issues of health disparities are at the forefront
                        of many of America's unresolved social problems. Rising health care costs pose a growing
                        threat to the economy, while for some access to even the most basic health care is
                        tenous. Lawyers play a central role in society's approach to these public health issues.
                        However, their effectiveness is frequently limited by an inadequate knowledge of public
                        health matters. To close this educational gap, the University of Memphis has created
                        the JD/MPH dual degree program. The program offers students the opportunity to pursue
                        the JD (juris doctor) and MPH (master of public health) degrees simultaneously. Students
                        will have the ability to complete both degrees in four years of full-time study, a
                        year less than it would take to complete each degree separately.
                      
                     
                      The goal of the JD/MPH program is to prepare and train students to apply law and policy
                        to benefit public health. The dual degree allows law students to combine in-depth
                        advocacy and policy skills learned in the UofM's JD program with the population health
                        perspective gained in the MPH program to address policy and advocacy needs at the
                        community level.
                      
                     
                       JD/MPH ADMISSIONS AND PROGRAM REQUIREMENTS  
                     
                      Candidates must meet the entrance requirements for both the Juris Doctor and Master
                        of Public Health degree programs, and must notify both programs at the time of application
                        that s/he intends to pursue the dual JD/MPH degree. Students will be admitted into
                        each program separately, and completion of one degree is not contingent upon completion
                        of both.
                      
                     
                      The MPH is a 42-credit hour degree program, and the JD/MPH program will allow up to
                        nine (9) credit hours of law courses for shared credit. Students must have a grade
                        of C or higher for the law school courses that are to be credited toward the MPH degree;
                        grades for the shared courses will not be counted in the GPA for the MPH degree. For
                        a complete listing of law courses offered with shared credit, please click  here .
                      
                     
                       JD/MPH APPLICATION  
                     
                      Applicants to the JD/MPH Program must be admitted to the Cecil C. Humphreys School
                        of Law, the University of Memphis Graduate School, and the MPH Program. Please click
                         here  for the application form for the JD/MPH dual degree program.
                      
                     
                      Questions about the JD/MPH Program? 
                     
                      Marian Levy, DrPH, RD, FAND Assistant Dean of Students and Public Health Practice 901.678.4514  mlevy@memphis.edu  
                     
                      Questions about the JD/MPH application process? 
                     
                      Shirl Sharpe, MS, Academic Services Coordinator II 114 Robison Hall 901.678.1710  Shirl Sharpe  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Programs 
                         
                            
                               
                                Degree Programs  
                                     
                                      JD  
                                     
                                      JD/MBA  
                                     
                                      JD/MA in Pol. Science  
                                     
                                      JD/MPH  
                                     
                                      MPH Fast-Track  
                                     
                                      Part-time Program  
                                  
                               
                                Certificate Programs  
                                      Advocacy  
                                      Business Law  
                                      Health Law  
                                      Tax Law  
                                  
                               
                                Experiential Learning  
                                      Experiential Learning Home  
                                      Legal Clinics  
                                      Externship Program  
                                  
                               
                                Law Review  
                                      Law Review Home  
                                      Editorial Board  
                                      Editor-in-Chief  
                                      Joining Law Review  
                                      Virtual Tour of Law Review Suite  
                                      Symposium  
                                      Subscriptions  
                                      Archives  
                                      Current Volume  
                                      Connect with Us  
                                  
                               
                                Advocacy Program  
                                      Moot Court Board  
                                      Joining Moot Court Board  
                                      In-School Competitions  
                                      Travel Team Competitions  
                                      Past Champions  
                                  
                               
                                Inst. for Health Law   Policy  
                                      Institute Home   
                                      Advisory Committee  
                                      Symposium  
                                      Pro Bono Opportunities  
                                      Health Law Certificate  
                                      Health Law Society  
                                      MBA Health Law Section  
                                  
                               
                                International Law Programs  
                            
                         
                      
                      
                      
                         
                            
                                Apply to Memphis Law  
                                
                            
                            
                                News   Events  
                                
                            
                            
                                Alumni   Support  
                                
                            
                            
                                ABA Required Disclosures  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


