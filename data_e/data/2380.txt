Film &amp; Video Production Alumni - Communication - University of Memphis    










 
 
 
     



 
    
    
    Film   Video Production Alumni - 
      	Communication
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Communication
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Graduate  
                          Undergraduate  
                          Faculty/Staff  
                          Alumni  
                          Events  
                      
                      
                         
                            Alumni   
                            
                               
                                  
                                   BA/MA  
                                   Film   Video Production  
                                   Doctoral  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Communication
                              	  
                          
                              	Alumni
                              	  
                         Film   Video Production Alumni 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Film   Video Production Alumni 
                     
                      Our FVP alumni are independent filmmakers and industry professionals in the Mid-South
                        and across the country.
                      
                     
                       Let us know what you're doing now , and we'll add or update your information here!
                      
                     
                      Jason Alexander - Photographer/Editor, WBBJ-TV, Jackson, TN. 
                     
                      Patrick Artus - Production assistant on "America's Next Top Model." "Dancing With
                        the Stars." (L.A.)
                      
                     
                      Andy Babbin - Gaffer (On various independent films) – "18 Fingers of Death", "God's
                        Waiting List", "Something's Wrong in Kansas", "Detective"
                      
                     
                      Barry Barclay - Production Coordinator (features)   Production Manager (Reality TV)
                        – "America's Next Top Model", "The Break Up", Cursed", "Identity", "The Sweetest Thing",
                        "Scream 3", "Charlie's Angels." (L.A.)
                      
                     
                      Matt Beickert - Owner/operator of TV II, a multimedia communications firm, Memphis. 
                     
                      Sean Bloemer - Owner/operator of Sean Bloemer Productions. Specializing in commercials,
                        music videos, and still photography. (Memphis)
                      
                     
                      Amy Brown - Owner and Operator of a Documentary film company. (North Carolina). 
                     
                      Camilla Buoni - Director of Videography for Snap Happy Productions, Memphis. 
                     
                      Ronnie Carney - Senior Communications Specialist, Fed Ex Services, Memphis. 
                     
                      Mandy Carson-Ingram - After four years working in production sound in England, Mandy
                        became a camerawoman and editor in the news division of the BBC.
                      
                     
                      Jim Casey - Managing Editor, True North Custom Publishing, Chattanooga, TN. 
                     
                      Brian Churchill - Independent producer and director. Owner of Churchill Studios, Inc.,
                        Memphis. Co-founder of Grade One Entertainment. (Memphis)
                      
                     
                      John Paul Clark - Director of Photography on features and music videos, including
                        "The Grace Card." Owner of Firefly Grip and Electric. (Memphis)
                      
                     
                      Chuck Cooper - Director/production designer. (N.Y.) 
                     
                      Michael Cruikshank - Camera Assistant (on various TV shows and Movies) – "Deadwood",
                        "Southland Tales", "Petomane: Parti avec le vent, Le"
                      
                     
                      Liz Daggett - Independent filmmaker and Coordinator for CODA (Center for Outreach
                        in the Development of the Arts). Liz is a faculty member in the Art Department at
                        Rhodes College, Memphis.
                      
                     
                      Andy Dean - Owner, Dean Film and Video, Memphis. 
                     
                      Tom Dean - Gaffer. Wilmington, N.C. 
                     
                      Sam Dlugach - Colorist. Credits include the TV series "Frazier," and many features,
                        including "The Conscientious Objector," "Family," and "Dopamine." (L.A.)
                      
                     
                      Jeremy Donaldson - Director of Photography, Mabus Agency Tallahassee, Fl. 
                     
                      Brad Ellis - Independent filmmaker. Co-founder of Old School Films. Directing credits
                        include "Act One," which won the Hometown Award at the Indie Memphis Film Festival.
                        (Memphis)
                      
                     
                      Sarah Fleming - Independent producer/director. Co-founder of "Live From Memphis,"
                        a grassroots organization representing Memphis music, film and the arts.
                      
                     
                      Dana Shea Gabrion – Supervising Producer CBS Television, Co-Executive Producer - "America's
                        Next Top Model"; Supervising Producer - "Who's Your Daddy?" , "T.H.E.M."; Exec. In
                        Charge of Production - "Kitchen Trends", Line Producer - "The Skateboard Show", "Dreamchasers",
                        "Ultimate Reality"; Reality Production Consultant for the WB Network   Turner Television.
                        The University Alumni Association named Dana "Outstanding Young Alumna" in 2011. (L.A.)
                      
                     
                      Olivia Hickox-Cowdry - Production Coordinator, NZK Productions, L.A. 
                     
                      Zach Glover - Videographer, Memphis Bar Association. 
                     
                      Geo Holmes - Director/Director of Photography. Owner/operator of Beale Street Studios.
                        Clients include the PGA, NCAA and CBS Sports. (Memphis)
                      
                     
                      Eric Huber - "Video Scholar" at the Church Health Center and Assistant Professor at
                        Rhodes College, Memphis.
                      
                     
                      Matt Hunter - Feature and Television Intern at The Chernin Group, L.A. 
                     
                      Hayden Jackson - Location Sound Mixer for Discovery, Harpo, VH1, ESPN, HBO, etc. (Chicago) 
                     
                      Justin Jaggers - Photojournalist, WMC-TV, Memphis. 
                     
                      Anwar Jamison - Digital Media Lead Instructor, Arkansas State University Mid-South. 
                     
                      Michelle Jarvis - Principal Creative Director. 68 Pictures, Pasadena, CA. 
                     
                      Chet Leonard - Location sound. Credits include "Waking Up in Reno," "A Time to Kill,"
                        "In Good Company," and "Lost Highway." (L.A.)
                      
                     
                      Shannon McIntosh - Senior Executive VP at Miramax. Credits include: Producer on "The
                        Hateful Eight;" Executive Producer on "Django Unchained;" the rap documentary, "Rhyme
                        and Reason;" and "Four Friends and a Movie" (the making of "Four Rooms"). (L.A.)
                      
                     
                      Bart Mallard - Actor, director. (Memphis) 
                     
                      Brian Manis - Producer and Development head for Davis Entertainment – "Garfield's
                        A Tale of Two Kitties", "Dr. Dolittle 3", "Garfield." (L.A.)
                      
                     
                      Calvin Miller - Video producer/editor/media consultant at calvinmillervideos, Memphis. 
                     
                      Andrew Neninger (a.k.a. Kentucker Audley) - Named one of Filmmaker Magazine's "25
                        New Faces of Independent Film" in 2007. Films include "Team Picture" and "Family Tree."
                        (Memphis)
                      
                     
                      Mark Norris - Producer. Co-founder of Old School Pictures. Credits include "Act One,"
                        which won the Hometown Award at the Indie Memphis Film Festival.
                      
                     
                      Jim O'Donnel - Director. Credits include the documentaries, "Ray for the NBA" and
                        "The Party Ain't Stopped Yet." Investigative Producer at ABC7, San Francisco, CA.
                      
                     
                      Leo O'Sullivan - Writer (with one script optioned to Al Pacino about "Napoleon" w/
                        Michael Radford set to direct AND another MOW for ABC about the making of the movie
                        "JAWS". (L.A.)
                      
                     
                      John Olivio - Independent filmmaker. Credits include "Stranded in Canton." (Memphis) 
                     
                      Ryan Parker - Director of Photography. Credits include "Feral," "Only Child," and
                        "Sweet, Sweet, Lonely Girl." (New York / Memphis)
                      
                     
                      Marius Penczner - Producer/writer/director/editor. Features - "I WasA Zombie For the
                        F.B.I.;" Music Videos - ZZ Top, the Everly Brothers, Travis Tritt; Commercials; Political
                        Media Campaigns, including Clinton-Gore, 1996. (Washington, D.C.)
                      
                     
                      Sue Lynn Perry - Production Manager for Running Pony Productions. Sue Lynn received
                        a 2005 Mid South Regional Emmy for her commercial television work. (Memphis)
                      
                     
                      David Perry - Editor. API Photography; Thompson and Company Marketing Communications.
                        (Memphis).
                      
                     
                      Miller Pipkin - MFA candidate at Emerson University (Boston). 
                     
                      Juli S. Pitzer - Video Production Manager at The University of Kansas (Lawrence, Kansas). 
                     
                      Wes Pollard - Promotion Director, WREG, TV, Memphis. 
                     
                      John Pouncy - Camera Assistant (on various TV shows and Movies) – "The District",
                        "Strange Wilderness", "The Third Nail", "9/Tenths", "You Got Served", "Joy Ride"
                      
                     
                      Richard Rawling - Editor at Elephant Films, Memphis. 
                     
                      Andy Romaine - Digital Effects Artist – "Fantastic Four", "Sin City", "Elektra", "Taxi",
                        "Exorcist: The Beginning", "The Last Samurai" (L.A.)
                      
                     
                      Cynthia Runions - Independent producer. Credits include "Dummy" (Atlanta) 
                     
                      Jay Russell - Writer/Director - Credits include many television documentaries and
                        the features, "End of the Line," "My Dog Skip," and " Ladder 49." (L.A.)
                      
                     
                      Joe Sanford - Producer/director/editor/videographer; owner of Pelican Pictures, New
                        Orleans.
                      
                     
                      John Saunders - Assistant Director on many films and TV episodes including, "The Hunger
                        Games," "John Wick" and "Vice Principles."
                      
                     
                      Drew Smith - Director of Strategic Communications at Lausanne Collegiate School (Memphis).
                        Drew was previously a producer at Running Pony Productions. 
                      
                     
                      Steven Snyder - MFA candidate at Chapman University (L.A.) 
                     
                      Lynn Sitler - Executive Director of the Memphis and Shelby County Film Commission. 
                     
                      Paul Sloan - Vice President/Creative Services Director, at KSAZ-TV, Phoenix, Arizona. 
                     
                      Lisa Stanley - Thompson and Company Marketing Communications. (Memphis). 
                     
                      Matt Stevens - Screenwriter (Obscenity, Psycho Bitch), script analyst, film reviewer
                        for E! Online. Matt's short film "The Making of Killer Kite," received the student
                        Emmy for Best Comedy and he was an assistant professor at Florida State University.
                        He is author of "Script Partners: What Makes Film and TV Writing Teams Work" with
                        Claudia Johnson. (L.A.)
                      
                     
                      David Stotts - After being Promotion Manager for WHBQ-TV, Memphis, and winning many
                        Regional Emmy Awards for audio, direction and editing, David is now with the NBA San
                        Antonio Spurs.
                      
                     
                      Dot Stovall - Promotions Director. WISH-TV, Indianapolis, IN. 
                     
                      Chris Sutherland - Chief videographer for WREG-TV, Memphis. 
                     
                      Jonathon Thomason - Creative Director, Master Video Productions, Memphis. 
                     
                      Mark Volzer - Managing Partner, Bravura Productions, Washington, D.C. 
                     
                      Mellissa Waldron - Radio Producer; Winner of the James Beard Award for "The Leonard
                        Lopate Show." (N.Y.)
                      
                     
                      Julian Williams - Independent filmmaker and DP whose works have been screened in venues
                        across the country, including the IndieMemphis, Nashville Independent, Carolina Film
                        and Video, Atlanta Underground, Athens and Rochester Film Festivals. Julian is an
                        instructor at Watkins Film School in Nashville.
                      
                     
                      Gina Wright - Radio and Television Producer/Director for eight years; fourteen years
                        experience as an editor on network TV shows for CMT and TNN, award winning music videos,
                        commercials, corporate videos and features. 1998 Mid South Emmy Award nominee and
                        Gold Medal Award at Worldfest Houston Film Festival for documentary, Joe Ely. Instructor
                        at Watkins Film School, Nashville.
                      
                     
                      Z. Eric Yang - Winner of the 2008 Student Academy Award, Eric is now a Creative Executive
                        at Alibaba Pictures.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Alumni 
                         
                            
                               
                                BA/MA  
                                Film   Video Production  
                                Doctoral  
                            
                         
                      
                      
                      
                         
                            
                                Student Film Showcase  
                               See sample films produced by BA and MA Film Students. 
                            
                            
                                Get Started in the COMM Major  
                               Declare your major and find your advisor. 
                            
                            
                                Apply to COMM Graduate Programs  
                               Programs in Film   Video Production and Communication. 
                            
                            
                                Contact Us  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


