Tenure and Promotion - CAS - University of Memphis    










 
 
 
     



 
    
    
    Tenure and Promotion - 
      	CAS
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			College of Arts   Sciences
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Areas of Study  
                          Research  
                          Scholarships  
                          Students  
                          Resources  
                          News  
                      
                      
                         
                            CAS Chair Handbook   
                            
                               
                                  
                                   Part I  
                                         Preface  
                                         Roles and Responsibilities  
                                         Survival Strategies  
                                         Departmental Culture   Chair Leadership Style  
                                         Length of Term and Transition to a New Chair  
                                     
                                  
                                   Part II  
                                         Management Introduction  
                                         Major Event Timeline  
                                         Attendance Expectations   Absence Reporting  
                                         Department Meetings  
                                         Budget Responsibilities 
                                          
                                           
                                             
                                               Understanding Your Budget  
                                             
                                               Viewing Your Budgets in Self-Service Banner  
                                             
                                               Monitoring Payrolls and Budgets in e~Print  
                                             
                                           
                                          
                                        
                                         Staff Management  
                                         Time and Leave  
                                         Annual Evaluations (Faculty and Staff)  
                                         Dealing with Complaints (Student, Faculty, and Staff)  
                                         Development (Donor Relations)  
                                     
                                  
                                   Part III  
                                        
                                         Faculty Recruitment  
                                        
                                         Joint Appointments  
                                        
                                         Tenure   Promotion  
                                        
                                         Teaching Load Guidelines  
                                        
                                         Tenure Track Faculty  
                                        
                                         Non-tenure Track Full-Time Faculty  
                                        
                                         Tenured Faculty  
                                        
                                         Part-Time Faculty  
                                        
                                         Retirement and Post-Retirement  
                                        
                                         Emeritus status for retired faculty members  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	CAS
                              	  
                          
                              	CAS Chair Handbook
                              	  
                         Tenure and Promotion 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Tenure and Promotion 
                     
                      You are already well aware of the tremendous importance of tenure and promotion decisions.
                        It's only the future of your department hanging in the balance—no pressure!
                      
                     
                      Below are links to official T P guidelines and procedures. And we have a few tips
                        you won't find in the rule books:
                      
                     
                      
                        
                         Begin preparing tenure track faculty for the tenure application process as soon as
                           they arrive on campus. Decisions about the tenure track faculty member's work load
                           should be made with the eventual tenure application in mind. You and other senior
                           faculty should be nurturing the new faculty and guiding them toward tenure from day
                           one. It's way too late to get worried at the end of year five.  
                         
                        
                         Know your department's tenure and promotion guidelines and follow them religiously.
                           Be especially mindful of deadlines. Missed deadlines cause needless heartache and
                           stomach ulcers. Every year some department misses a key deadline—make sure it's not
                           your department.  
                         
                        
                         Be proactive about collecting the external review letters for each applicant. Your
                           departmental T P guidelines specify who is responsible for selecting the external
                           reviewers and requesting the letters. Stay on top of this. Nothing creates more havoc
                           with the review committees than not having a sufficient number of external letters.
                           Tip: The minimum is four.  
                         
                        
                          Tenure and Promotion Guides and Forms (provost's website)  
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         CAS Chair Handbook 
                         
                            
                               
                                Part I  
                                      Preface  
                                      Roles and Responsibilities  
                                      Survival Strategies  
                                      Departmental Culture   Chair Leadership Style  
                                      Length of Term and Transition to a New Chair  
                                  
                               
                                Part II  
                                      Management Introduction  
                                      Major Event Timeline  
                                      Attendance Expectations   Absence Reporting  
                                      Department Meetings  
                                      Budget Responsibilities 
                                       
                                        
                                          
                                            Understanding Your Budget  
                                          
                                            Viewing Your Budgets in Self-Service Banner  
                                          
                                            Monitoring Payrolls and Budgets in e~Print  
                                          
                                        
                                       
                                     
                                      Staff Management  
                                      Time and Leave  
                                      Annual Evaluations (Faculty and Staff)  
                                      Dealing with Complaints (Student, Faculty, and Staff)  
                                      Development (Donor Relations)  
                                  
                               
                                Part III  
                                     
                                      Faculty Recruitment  
                                     
                                      Joint Appointments  
                                     
                                      Tenure   Promotion  
                                     
                                      Teaching Load Guidelines  
                                     
                                      Tenure Track Faculty  
                                     
                                      Non-tenure Track Full-Time Faculty  
                                     
                                      Tenured Faculty  
                                     
                                      Part-Time Faculty  
                                     
                                      Retirement and Post-Retirement  
                                     
                                      Emeritus status for retired faculty members  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply now for an Undergraduate Degree  
                               Pursue your dream, broaden your career choices and gain the confidence you need to
                                 succeed
                               
                            
                            
                                Offering Master's and Doctoral Degrees  
                               Enhance your knowledge, skills and experience 
                            
                            
                                Community Engagement  
                               Teaching and serving the Memphis and Mid-South community through programs and events 
                            
                            
                                Contact Us  
                               Dean's Office Contacts, Advising, Department Chairs... 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


