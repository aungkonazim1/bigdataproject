Content | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 Content | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Content                       
        
        
       
          
     
           Printer-friendly version       
	Learner topics
 

 
	Use the Content tool to access course materials, complete required activities, and monitor your completion progress on topics contained within each course module.
 

 
	 
		What are the basics of Content?
	 

	  
			 Accessing Content 
		 
		 
			 Content user interface 
		 
		 
			 Searching for a module or topic in Content 
		 
		 
			 Viewing content 
		 
		 
			 Enabling notifications in Content 
		 
		 
			 Printing from Content 
		 
		 
			 Submitting feedback in Content  
		 
		 
			 Downloading content 
		 
		 
			 Sending content to a mobile device via Desire2Learn Binder 
		 
	  

 
	
 

 
	
 

 
	 
 
     Audience:    Learner      

       Content basics   
           

        
        
                   Content basics › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Content  
  
      Content basics    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
