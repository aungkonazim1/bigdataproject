Robert A. Neimeyer, Ph.D. - Department of Psychology - University of Memphis    










 
 
 
     



 
    
    
    Robert A. Neimeyer, Ph.D. - 
      	Department of Psychology
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Psychology
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Undergraduate  
                          Graduate  
                          People  
                          Centers  
                          Research  
                          Resources  
                      
                      
                         
                            People   
                            
                               
                                  
                                   Program Contacts  
                                   Faculty  
                                        
                                         Frank Andrasik  
                                        
                                         Karen Linville Baker  
                                        
                                         J. Gayle Beck  
                                        
                                         Kristoffer S. Berlin  
                                        
                                         Jeffrey S. Berman  
                                        
                                         Cheryl Bowers  
                                        
                                         Jason L. G. Braasch  
                                        
                                         Gina Caucci  
                                        
                                         Robert Cohen  
                                        
                                         Melloni N. Cook  
                                        
                                         Thomas K. Fagan  
                                        
                                         Randy G. Floyd  
                                        
                                         Arthur C. Graesser  
                                        
                                         David A. Houston  
                                        
                                         Kathryn H. Howell  
                                        
                                         Xiangen Hu  
                                        
                                         Stephanie Huette  
                                        
                                         Xu Jiang  
                                        
                                         Roger J. Kreuz  
                                        
                                         Deranda Lester  
                                        
                                         Brook A. Marcks  
                                        
                                         Meghan McDevitt-Murphy  
                                        
                                         Elizabeth B. Meisinger  
                                        
                                         Andrew W. Meyers  
                                        
                                         James G. Murphy  
                                        
                                         Robert A. Neimeyer  
                                        
                                         Andrew M. Olney  
                                        
                                         Philip I. Pavlik Jr.  
                                        
                                         Leslie A. Robinson  
                                        
                                         M. David Rudd  
                                        
                                         Helen J. K. Sable  
                                        
                                         Nicholas W. Simon  
                                        
                                         Idia B. Thurston  
                                        
                                         James P. Whelan  
                                        
                                         William H. Zachry  
                                        
                                         Jia Wei Zhang  
                                     
                                  
                                   Staff  
                                         Staff Personnel  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of Psychology
                              	  
                          
                              	People
                              	  
                         Robert A. Neimeyer, Ph.D. 
                      
                   
                   
                       
                      
                      
                        
                        	
                        
                        		  
                         
                           
                           
                            
                              
                              
                                                                   
                                 
                                 
                                  
                                 
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                    Robert A. Neimeyer, Ph.D.
                                    
                                  
                                 
                                 
                                  
                                    
                                    Professor, Clinical (Psychotherapy)
                                    
                                  
                                 
                                 
                                  
                                    
                                     
                                       
                                        Phone 
                                       
                                        
                                          
                                          901.494.1806
                                          
                                        
                                       
                                     
                                    
                                     
                                       
                                        Email 
                                       
                                        
                                          
                                           neimeyer@memphis.edu 
                                           
                                       
                                     
                                    
                                     
                                       
                                        Fax 
                                       
                                        
                                          
                                          901.678.2579
                                          
                                        
                                       
                                     
                                    
                                  
                                 
                                 
                                  
                                    
                                     
                                       
                                        Office 
                                       
                                        
                                          
                                          Psychology Building, Room 347
                                          
                                        
                                       
                                     
                                    
                                     
                                       
                                        Office Hours 
                                       
                                        
                                          
                                          Contact
                                          
                                        
                                       
                                     
                                    
                                  
                                 
                                 
                                  
                                    										
                                    
                                    
                                      Website  
                                    
                                      CV  
                                                                            
                                    
                                  
                                 
                               
                              
                              
                            
                           			 
                           			  
                           
                            Education   Ph.D., University of Nebraska B.A., University of Florida   Research Interests   
                           
                            
                              
                               Psychotherapy Process and Outcome 
                              
                               Psychology of Grief and Loss 
                              
                               Constructivist Psychology 
                              
                            
                           
                             Representative Publications   
                           
                            
                              
                               Neimeyer, R. A. (Ed.) (2015).  Techniques of grief therapy: Assessment and intervention.  New York: Routledge.
                               
                              
                               Thompson, B. E.   Neimeyer, R. A. (Eds.) (2014).  Grief and the expressive arts: Practices for creating meaning.  New York: Routledge.
                               
                              
                               Neimeyer, R. A. (2016). Complicated grief: Assessment and intervention. In J. Cook,
                                 S. Gold   C. Dalenberg (Eds.).  APA handbook of trauma psychology.  Washington, DC: American Psychological Association.
                               
                              
                               Neimeyer, R. A.,   Holland, J. M. (2014). Bereavement in later life: Theory, assessment
                                 and intervention. In B. T. Mast   P. A. Lichtenberg (Eds.),  Handbook of clinical geropsychology.  Washington, DC: American Psychological Association.
                               
                              
                               Neimeyer, R. A., Woodward, M., Pickover, A.   Smigelsky, M. (2015). Questioning our
                                 questions: A constructivist technique for clinical supervision.  Journal of Constructivist Psychology,  DOI: 10.1080/10720537.2015.1038406.
                               
                              
                               Neimeyer, R. A.   Young-Eisendrath, P. (2015). Assessing a Buddhist treatment for
                                 bereavement and loss: The Mustard Seed Project.  Death Studies, 39,  263-273, DOI: 10.1080/07481187.2014.937973.
                               
                              
                               Piazza-Bonin, E., Neimeyer, R. A., Burke, L. A., Young, A.   McDevitt-Murphy, M. (2014).
                                 Disenfranchised grief following African American homicide loss: An inductive case
                                 study.  Omega, 70,  369-392.
                               
                              
                               Gillies, J. M., Neimeyer, R. A.   Milman, E. (2014). The Grief and Meaning Reconstruction
                                 Inventory (GMRI): Initial validation of a new measure.  Death Studies, 38,  207-216, DOI: 10.1080/07481187.2014.907089
                               
                              
                               Burke, L. A.   Neimeyer, R. A. (2014). Spiritual distress in bereavement: Evolution
                                 of a research program.  Religions, 5,  1087-1115, doi:10.3390/rel5041087.
                               
                              
                               Neimeyer, R. A., Klass, D.   Dennis, M. R. (2014). A social constructionist account
                                 of grief: Loss and the narration of meaning.  Death Studies, 38,  485-498.
                               
                              
                               Cerel, J., McIntosh, J. L., Neimeyer, R. A., Maple, M.   Marshall, D. (2014). The
                                 continuum of "survivorship": Definitional issues in the aftermath of suicide.  Suicide and Life-Threatening Behavior, 44,  591-600. DOI: 10.1111/sltb.12093.
                               
                              
                               Neimeyer, R. A.   Soderlund, J. (2014). Good grief: A contemporary orientation to
                                 bereavement counseling.  New Therapist, 90,  6-14.    
                              
                            
                           
                           
                         
                        								
                        
                        
                        
                        
                        	
                      
                      
                   
                
                
                   
                      
                         People 
                         
                            
                               
                                Program Contacts  
                                Faculty  
                                     
                                      Frank Andrasik  
                                     
                                      Karen Linville Baker  
                                     
                                      J. Gayle Beck  
                                     
                                      Kristoffer S. Berlin  
                                     
                                      Jeffrey S. Berman  
                                     
                                      Cheryl Bowers  
                                     
                                      Jason L. G. Braasch  
                                     
                                      Gina Caucci  
                                     
                                      Robert Cohen  
                                     
                                      Melloni N. Cook  
                                     
                                      Thomas K. Fagan  
                                     
                                      Randy G. Floyd  
                                     
                                      Arthur C. Graesser  
                                     
                                      David A. Houston  
                                     
                                      Kathryn H. Howell  
                                     
                                      Xiangen Hu  
                                     
                                      Stephanie Huette  
                                     
                                      Xu Jiang  
                                     
                                      Roger J. Kreuz  
                                     
                                      Deranda Lester  
                                     
                                      Brook A. Marcks  
                                     
                                      Meghan McDevitt-Murphy  
                                     
                                      Elizabeth B. Meisinger  
                                     
                                      Andrew W. Meyers  
                                     
                                      James G. Murphy  
                                     
                                      Robert A. Neimeyer  
                                     
                                      Andrew M. Olney  
                                     
                                      Philip I. Pavlik Jr.  
                                     
                                      Leslie A. Robinson  
                                     
                                      M. David Rudd  
                                     
                                      Helen J. K. Sable  
                                     
                                      Nicholas W. Simon  
                                     
                                      Idia B. Thurston  
                                     
                                      James P. Whelan  
                                     
                                      William H. Zachry  
                                     
                                      Jia Wei Zhang  
                                  
                               
                                Staff  
                                      Staff Personnel  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Psychology Graduate Programs Application  
                               Click on link to Apply to the Department of Psychology Graduate Program 
                            
                            
                                Academic Advising   Resource Center (AARC)  
                                The AARC provides advising to students helping them make the most of their undergraduate
                                 education at the UofM.
                               
                            
                            
                                The Psychological Services Center  
                               PSC provides general outpatient psychotherapeutic and psychological assessment services
                                 to individuals and families
                               
                            
                            
                                Teaching Take-Out  
                               This website is a resource for busy teachers who want to enrich their classes while
                                 preserving the time they need for research and other important professional activities.
                               
                            
                         
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/17/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


