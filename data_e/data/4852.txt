 Entrepreneurship Minor - MGMT - University of Memphis    










 
 
 
     



 
    
    
     Entrepreneurship Minor - 
      	MGMT
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - Department of Management
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Welcome  
                          Programs  
                          Courses  
                          Faculty  
                          PhD Students  
                          Research  
                          FCBE  
                      
                      
                         
                            Programs   
                            
                               
                                  
                                   Welcome  
                                   Programs  
                                   Courses  
                                   Faculty  
                                   PhD Students  
                                   Research  
                                   Contact Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	MGMT
                              	  
                          
                              	Programs
                              	  
                          Entrepreneurship Minor 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Entrepreneurship Minor 
                     
                      "Entrepreneurship is the process of starting a business or other organization. The
                        entrepreneur develops a business model, acquires the human and other required resources,
                        and is fully responsible for its success or failure. Entrepreneurship operates within
                        an entrepreneurship ecosystem." ( wikipedia.org/wiki/Entrepreneurship )
                      
                     
                      The Entrepreneurship Minor is for undergraduate students with *any* major at the University
                        of Memphis (18 hours). Course requirements: UNIV 2555, MGMT 2820, elective, MGMT 4921
                        or UNIV 4110, MGMT 4920, MGMT 4820.
                      
                     
                      Entrepreneurship Minor Course Sequence: 
                     
                      1. UNIV 2555 - Intro to Creativity, Innovation, and Design (3 credits) 2. MGMT 2820 - Intro to Entrepreneurship (3 credits) 3. Elective (3 credits)
                      
                     
                      Courses that satisfy a requirement for the student in his or her major may NOT be
                        used. The student is expected to propose an elective course, in the entrepreneurial
                        spirit , that will enhance and support related learning and success of the entrepreneurial
                        activity, project, or business that is the focus of the minor. For example, the elective
                        course could be in a functional area of knowledge critical to the entrepreneurial
                        project implementation (e.g., management, marketing, finance, economics, non-profit
                        administration, healthcare administration). Another alternative could be a technical
                        course critical to the project idea development (e.g., in the sciences, computers,
                        engineering, technology, biology, architecture, arts).
                      
                     
                      4. MGMT 4921 or UNIV 4110 - Internship in Entrepreneurship (3 credits) 5. MGMT 4920 - Independent Study/Management Problems (students participate in entrepreneurship
                        competition coordinated by  Crews Center for Entrepreneurship ) (3 credits) 6. MGMT 4820 - Entrepreneurship (3 credits; capstone course). Prerequisites: MGMT
                        3110 and MGMT 3510 *or* MGMT 2820.
                      
                     
                      For information about Entrepreneurship Minor, please contact: 
                     
                      Undergraduate Student Services Fogelman College of Business and Economics University of Memphis 901.678.2855  fcbeadvising@memphis.edu  
                     
                       Entrepreneurship Collaborative  
                     
                      The Department of Management's strategy and entrepreneurship faculty collaborate with
                        the  Crews Center for Entrepreneurship  to coordinate the Entrepreneurship Minor and conduct entrepreneurship research.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Programs 
                         
                            
                               
                                Welcome  
                                Programs  
                                Courses  
                                Faculty  
                                PhD Students  
                                Research  
                                Contact Us  
                            
                         
                      
                      
                      
                         
                            
                                MGMT News   Events  
                               Learn about current events happening in our department! 
                            
                            
                                Research Colloquia  
                               Keep up with the latest Management academic research in our upcoming seminars.  
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


