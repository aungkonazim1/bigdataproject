Tiger Funds - Campus Card - University of Memphis  Tiger Funds  










 
 
 
     



 
    
    
    Tiger Funds - 
      	Campus Card
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Campus Card
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Your Campus Card  
                          Dining Dollars  
                          Tiger Funds  
                          Meal Plans/Flex Bucks  
                          Student Giving  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Campus Card
                              	  
                         Tiger Funds 
                      
                   
                   
                       
                      
                     		
                     
                     
                      Tiger Funds 
                     
                      A Tiger Funds account works like a debit card. It is a unique money management system
                        activated by your Campus Card that is available to all students, faculty, and staff.
                        Log in to your account (click Your Campus Card above) to see specifics about your
                        account, including balance and transaction history, meal plan status (if applicable),
                        and Tiger Funds information.
                      
                     
                      Once you have made a deposit into your personal Tiger Funds account, you simply present
                        your Campus Card to pay for meals at campus dining locations or to purchase books,
                        supplies, and personal items at the University Store.
                      
                     
                      You can also use your Campus Card in copier machines and for services at the Health
                        Center and the Bursar's Office.
                      
                     
                      How does it work? 
                     
                      It is simple to activate your Tiger Funds account. Your Campus Card is already encoded
                        to provide access to University events and activities. When you make a minimum deposit
                        of $10 to your personal Tiger Funds account, your Campus Card becomes your Tiger Funds
                        card as well. Your daily transaction limit cannot exceed $500. Tiger Funds may not
                        be used to purchase gift cards.
                      
                     
                      Each time you make a purchase, your Campus Card is swiped through a card reader and
                        the amount of the purchase is automatically deducted from the balance in your Tiger
                        Funds account. Your remaining balance is indicated on your receipt after each transaction
                        so you will always know how much money remains in your account.
                      
                     
                      You can deposit money to your Tiger Funds account in the following ways: 
                     
                      
                        
                         Online at Campus Card Online (credit card - Visa, MasterCard, Discover, American Express). 
                        
                         Through Blackboard Card Management Centers (cash only) located in the McWherter Library,
                           Law Library, Wilder Tower, Jones Hall Tiger Den, UC Union Food Court, V. Lane Rawlins
                           Service Court and Bookstore, the Speech and Hearing Center, and the Gobbel Library
                           at the Lambuth campus.
                         
                        
                         In person at the Bursar's Office customer service windows, 1st floor, Wilder Tower
                           (cash, check or credit card).
                         
                        
                      
                     
                      Tiger Funds are maintained separately from Dining Dollars, meal plan accounts and
                        Flex Bucks, which are also accessed by your Campus Card. Meal plan accounts are administered
                        by the University's food service vendor. The policies and procedures of the meal plan
                        accounts are those of the vendor and not those of the University of Memphis.
                      
                     
                       What are the advantages of Tiger Funds?  
                     
                      It gives you ready access to all food services locations that offer a variety of food     
                        choices, from early morning breakfasts to late night snacks. It offers you purchase
                        privileges without cash, for the great selection of books, supplies, gifts, and personal
                        items at the University Store. Use your Campus Card to make copies at the McWherter
                        Library, Law Library, the Speech and Hearing Center, and the Lambuth campus Library.
                        Account balances remain available as long as you have an active relationship with
                        the      University. Once you establish your Tiger Funds account, there is little
                        or need to carry cash or to write checks for University meals, supplies, or services.
                      
                     
                       How do I request a refund of the remaining balance on my Tiger Funds account?  
                     
                      Refund requests from the Tiger Funds account must be submitted in person to the Bursar's
                        Office, 115 Wilder Tower. Lambuth students may request a refund at the Business Service
                        Center on the Lambuth campus in 109 Varnell-Jones Hall.
                      
                     
                      
                        
                         A $5 processing fee will be deducted from the available balance in the account prior
                           to the refund.
                         
                        
                         Any amounts that are owed to the University will be deducted from the available balance
                           in the account prior to the processing of a refund.
                         
                        
                         The refund will be processed to the eRefund account through TigerXpress. 
                        
                         If no payment profile exists in TigerXpress, a check will be mailed to the permanent
                           address on file with the University.
                         
                        
                      
                     
                      Tiger Funds accounts that remain inactive for an extended period of time are subject
                        to automatic refund processing and the previous conditions apply.
                      
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Frequently Asked Questions  
                               Campus Card and Dining Dollars FAQs 
                            
                            
                                Dining Locations  
                               Dining locations across campus and more 
                            
                            
                                Vending Locations  
                               Vending machine locations across campus 
                            
                            
                                Contact Us  
                               Our team is here to help 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/10/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


