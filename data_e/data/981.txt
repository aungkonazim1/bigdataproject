What's Happening in the Department of English? - Department of English - University of Memphis    










 
 
 
     



 
    
    
    What's Happening in the Department of English? - 
      	Department of English
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of English
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Undergraduate  
                          Graduate  
                          People  
                          Community  
                          News/Events  
                      
                      
                         
                            News and Events   
                            
                               
                                  
                                   Recent Faculty Articles, Essays, Poems, and Short Stories  
                                   What's Happening?  
                                   Naseeb Shaheen Memorial Lecture Series  
                                   River City Writers' Series  
                                         About  
                                         Current Series  
                                         Past Speakers  
                                         Make a Gift  
                                         Contact RCWS  
                                         MFA Creative Writing Program  
                                     
                                  
                                   The Pinch  
                                   Newsletter  
                                   Student Recognition  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of English
                              	  
                          
                              	News and Events
                              	  
                         What's Happening in the Department of English? 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      What's Happening in the Department of English? 
                     
                      Get the most up-to-date information on English Department events through our  facebook page .
                      
                     
                      Fall 2017 
                     
                      
                        
                         
                           
                            
                              
                                 
                               
                              
                               
                                 
                                  On September 25th and 26th, the River City Writers' Series welcomed nonfiction author
                                    Bob Cowser for a reading and an interview. Bob Cowser's most recent book  Green Fields: Crime, Punishment, and a Boyhood Between  won "Best Memoir 2010" from the Adirondack Center for Writers, and an excerpt was
                                    cited in the  Best American Essays 2012.  His other works include  Dream Season ,  Scrorekeeping , and the edited collection  Why We're Here: New York Essayists on Living Upstate . Cowser is a Professor of English at St. Lawrence University, where he teaches courses
                                    in nonfiction writing and American Literature and was named the 2012 Owen D. Young
                                    Outstanding Faculty Member.
                                  
                                 
                                    
                                 
                               
                              
                                 
                               
                              
                            
                           
                            
                              
                                 
                              
                               
                                 
                                  On October 24th. poet and scholar Melisa Cahnmann-Taylor gave a reading from her new
                                    book  Imperfect Tense  as part of the River City Writers Series.  Melisa Cahnmann-Taylor is Professor of Language and Literacy Education at the University
                                    of Georgia. She is the author of  Imperfect Tense  (2016), and co-author of three books on education. She has received NEA "Big Read"
                                    Grants, the Beckman award for "Professors Who Inspire Social Change," and a Fulbright
                                    for nine-month study of adult Spanish language acquisition in Oaxaca Mexico. She is
                                    poetry editor for  Anthropology   Humanism .
                                  
                                 
                                    
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                                  
                              
                               
                                 
                                  On October 26th, the Fourth Annual Shaheen Symposium called Conversations Across Concentration
                                    featured Profs. Kathy Lou Schultz (Literature and Cultural Studies, African American
                                    Literature), Lyn Wright (Applied Linguistics), Marcus Wicker (Creative Writing), and,
                                    visiting from the University of Georgia, Melisa Cahnmann-Taylor (Educational Linguistics).
                                    These professors discussed representative poems by Lucille Clifton ("4/30/92 for rodney
                                    king"), Carolyn Rodgers ("How I Got OVAH"), and Pauli Murray ("Prophecy"), placing
                                    an emphasis on how they would go about teaching and contextualizing these works from
                                    their own disciplinary or methodological perspectives.
                                  
                                 
                                    
                                 
                               
                              
                                  
                              
                            
                           
                            
                              
                                  
                              
                               
                                 
                                  On November 2nd, The Department of English and the Marcus Orr Center for the Humanities
                                    welcomed Dr. Yolanda Pierce, Professor and Dean of the Howard University School of
                                    Divinity in Washington, DC. Her talk was entitled "i have shaken rivers out of my
                                    eyes: Black Poetry and Prophetic Rage." Dr. Pierce's most recent book ( Hell Without Fires: Slavery, Christianity   the African American Spiritual Narrative ) and forthcoming book ( Religious Ecstasy   African American Cultural Expression ) focus on the historical and contemporary significance of the African American religious
                                    tradition.
                                  
                                 
                                    
                                 
                               
                              
                                  
                              
                            
                           
                         
                        
                      
                     
                        
                     
                      Spring 2017 
                     
                      
                        
                         
                           
                            
                              
                                 
                              
                               
                                 
                                   Visiting Professor and fiction/nonfiction author Sarai Walker gave a reading, Q  
                                       A session, and book signing of her novel  Dietland  On January 25th.  
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                               
                                 
                                  On February 28th and March 1st, the River City Writers Series presented a reading
                                    and interview by Caitlin Horrocks, author of the  New York Time s Book Review Editor Choice winning and Barnes   Noble Discover Great New Writers
                                    selected short story collection  This Is Not Your City .
                                  
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                               
                                 
                                  In celebration of Women's History Month, undergraduate and graduate students read
                                    their creative works at Women in Words: An Invitational Student Reading on March 16th.
                                    Featured student readers included Briana Brakhage, Madison Cardinez, Devan DelConte,
                                    Marisa Manuel, Jeanna Paden, Kendra Vanderlip, and Christine Wunrow. MFA student Amanda
                                    Muir served as Master of Ceremonies.
                                  
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                               
                                 
                                  On March 30th-31st, The River City Writers series presented a reading and interview
                                    with Shara McCallum, a Jamaican-born award-winning poet and essayist. Shara's books
                                    of poetry include  Madwoman, The Face of Water: New and Selected Poems, This Strange Land, Song of Thieves , and  The Water Between Us .
                                  
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                               
                                 
                                  In conjunction with Rhodes College, the Department of English presented the Baldwin
                                    Now Symposium on March 30th. The roundtable discussion was led by acclaimed Baldwin
                                    scholars Quentin Miller, Soyica Colbert, and Magdalena Zaborowska.
                                  
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                               
                                 
                                  On Saturday, April 29th, the African American Literature concentration invited Shelby
                                    County elementary, middle, and high school teachers to a teaching workshop called
                                    Explosive Topics: Teaching Race in Literature. The workshop was led by African American
                                    literature faculty members and graduate students.
                                  
                                 
                               
                              
                                 
                              
                            
                           
                         
                        
                      
                     
                      Fall 2016 
                     
                      
                        
                         
                           
                            
                              
                                 
                              
                               
                                 
                                  The U of M English Department was pleased to welcome Prof. William J. Maxwell of Washington
                                    University in St. Louis, who will deliver the Seventh Annual Naseeb Shaheen Memorial
                                    Lecture on October 20, 2016. Prof. Maxwell's talk is entitled: "Born-Again, Seen-Again
                                    James Baldwin: State Surveillance, Afro-Pessimism, and the Literary History of Black
                                    Lives Matter." The Lecture Series also included a symposium on the topic of "Reading
                                    Resistance," featuring readings by graduate students, professors, and instructors,
                                    on October 14th.  Learn more about the Shaheen Lecture Series and Symposium .
                                  
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                               
                                 
                                  Thanks to generous funding and support from SEA, the English Department, and the Lambuth
                                    Foundation, River City Writers Series at the University of Memphis welcomed poet David
                                    Kirby for two readings on October 21st and 22nd. David Kirby's collection  The House on Boulevard St.: New and Selected Poems  was a finalist for the National Book Award in 2007. Kirby's honors include fellowships
                                    from the National Endowment of the Arts and the Guggenheim Foundation. His latest
                                    poetry collection is  Get Up, Please.  
                                  
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                               
                                 
                                  Students gathered in Patterson Hall to learn about all the English Department has
                                    to offer at the English Undergraduate Orientation in September.
                                  
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                               
                                 
                                  The Department of English won third place at Discover Your Major Day for its carnival-themed
                                    booth! Congratulations to Dale Williams and thanks to all our volunteers.  See more of the fun in this slideshow!  
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                               
                                 
                                  More photos of Discover Your Major Day, courtesy of Curt Hart. 
                                 
                               
                              
                                 
                              
                            
                           
                         
                        
                      
                      
                     
                      Spring 2016 
                     
                      
                        
                         
                           
                            
                              
                                 
                              
                               
                                 
                                  On May 9-19, 2016, thirty-three students and three Department of English faculty members
                                    (Cathy Dice, Tammy Jones, and Susan Popham) from the UofM Honors Program traveled
                                    to London and Edinburgh for the Study Abroad course, Rediscovering Harry Potter: Sources,
                                    Landscapes, and Legacies.  Visit the English Department Study Abroad page to learn mor e.
                                  
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                               
                                 
                                   As part of Women's History Month in March, the English department sponsored Women
                                    in Words, a reading, panel discussion, and reception showcasing the writings of women
                                    from the English Department. CW Faculty member Sonja Livingston read from her book,  Ladies Night in Dreamland , and flash readings were presented by students Breanne Hager, Deven Lyle, Nikki McQuire,
                                    Kat Moore, and Amanda Muir. A panel discussion on publishing featured Sonja Livingston,
                                    Brandy Wilson, and Kat Moore. The event was moderated by Ashley Roach. 
                                  
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                                 
                               
                              
                               
                                 
                                  On Wednesday, February 17th, the River City Writers' Series welcomed author Nickole
                                    Brown for a reading and craft talk. Brown's works include Fanny Says, an "unleashed
                                    love song" to her late grandmother; her debut, Sister, a novel in poems; and Air Fare,
                                    which she co-edited with Judith Taylor. 
                                  
                                 
                               
                              
                                  
                              
                            
                           
                            
                              
                                 
                              
                               
                                 
                                  On February 29th, UofM instructors Cathy Dice and Tammy Jones held the annual WordSmith
                                    Writing Olympics, a writing competition for Memphis-area middle and high-schoolers.
                                     Learn more about WordSmith . 2016's WordSmith competition included a children's book-drive to benefit  Porter-Leath , which helps promote literacy and health for underprivileged families in the Memphis
                                    community.
                                  
                                 
                               
                              
                                  
                              
                            
                           
                            
                              
                                  
                              
                               
                                 
                                   On March 2nd, Dr. Darryl Domingo presented "Advertising, Amusement, and the Arts
                                    of Persuasion in Eighteenth-Century London Newspapers" as part of Marcus W. Orr Center
                                    for Humanities Brown Bag series, which allows faculty to present and get feedback
                                    on current research. 
                                  
                                 
                               
                              
                                 
                              
                            
                           
                         
                        
                      
                     
                        
                     
                      Fall 2015 
                      
                     
                      
                        
                         
                           
                            
                              
                                 
                              
                               
                                 
                                  The English Department hosted Dr. Alexis De Veaux, who gave a talk and a reading from
                                    her novel  Yabo  on September 29th.
                                  
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                                 
                              
                               
                                 
                                  Dale Williams created a mesmerizing display for Discover your Major Day on October
                                    1st! The English Department won "Honorable Mention" for their fortune teller themed
                                    display.
                                  
                                 
                               
                              
                                 
                              
                            
                           
                         
                        
                      
                     
                        
                     
                       
                     
                       
                     
                       
                     
                       
                     
                          
                     
                      
                        
                           
                        
                           
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         News and Events 
                         
                            
                               
                                Recent Faculty Articles, Essays, Poems, and Short Stories  
                                What's Happening?  
                                Naseeb Shaheen Memorial Lecture Series  
                                River City Writers' Series  
                                      About  
                                      Current Series  
                                      Past Speakers  
                                      Make a Gift  
                                      Contact RCWS  
                                      MFA Creative Writing Program  
                                  
                               
                                The Pinch  
                                Newsletter  
                                Student Recognition  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now  
                                
                            
                            
                                Alumni and Friends  
                                
                            
                            
                                Course Offerings  
                                
                            
                            
                                Contact Us  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/30/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


