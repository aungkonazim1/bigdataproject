Student Life - Herff College of Engineering - University of Memphis    










 
 
 
     



 
    
    
    Student Life  - 
      	Herff College of Engineering
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Herff College of Engineering
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Future Students  
                          Students  
                          Programs  
                          Research  
                          Alumni  
                          Events  
                      
                      
                         
                            Future Students   
                            
                               
                                  
                                   Choose Your Major  
                                   Visit the College  
                                   Apply to the UofM  
                                   Scholarships  
                                   Student Life  
                                   FAQs  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Herff College of Engineering
                              	  
                          
                              	Future Students
                              	  
                         Student Life  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Get Involved While at Herff! 
                     
                      Engineering Live and Learn Community 
                     
                      The Engineering LLC (ELLC) is a place where first and second year students enrolled
                        in the Herff College of Engineering live together in the new Centennial Hall Dormitory
                        while participating in activities throughout the year. The activities last year included
                        industry tours, Cookies and Calculus tutoring, and seminars with faculty about degree
                        completion and research. Space is limited so there is a separate application. The
                        deadline to apply for priority consideration in the ELLC is February 1, 2018. 
                      
                     
                        Click here to review the ELLC application and program requirements       
                     
                        Click here to learn more about Tiger Living Communities on campus     
                     
                      Get Involved Across Campus 
                     
                      The creative drive that comes naturally to engineering students extends over into
                        the rest of their college life. We see Herff students getting involved all over campus
                        and throughout the city, in meaningful ways.
                      
                     
                      There are over 200 campus groups to consider, and you can even form your own unique
                        group or event—like Up 'Til Dawn, an outreach event that was innovated right here
                        by UofM students, but now spans the nation. Universities all across America work together
                        on this Memphis-founded event to raise funds for St. Jude Children's Research Hospital.
                        There are tons of other groups waiting to have your creative brain on board, including
                        intramural sports, service organizations, student government, arts and culture groups,
                        Greek life, religious groups and so much more.
                      
                     
                        Click here to learn more about opportunities across campus    
                     
                      The following opportunities are specific to engineering and will greatly enhance your
                        experience at the UofM both academically and socially.
                      
                     
                      Get Involved in Engineering Student Organizations 
                     
                      
                        
                         
                           
                            
                              
                                 
                              
                               
                                 
                                   National and On-Campus Links (if available)   
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                      
                                 
                               
                              
                               
                                 
                                   AIAA American Institute for Aeronautics   Astronautics  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                      
                                 
                               
                              
                               
                                 
                                   ASCE American Society of Civil Engineers ,  UofM ASCE  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                    
                                 
                               
                              
                               
                                 
                                   ASME American Society of Mechanical Engineers  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                    
                                 
                               
                              
                               
                                 
                                   BMES Biomedical Engineering Society  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                      
                                 
                               
                              
                               
                                 
                                  EERI Earthquake Engineering Research Institute,    UofM EERI    
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                      
                                 
                               
                              
                               
                                 
                                   EWB Engineers without Borders ,  UofM EWB  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                    
                                 
                               
                              
                               
                                 
                                   IEEE Institute of Electrical and Electronics Engineers  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                    
                                 
                               
                              
                               
                                 
                                   NSBE National Society of Black Engineers  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                      
                                 
                               
                              
                               
                                 
                                   SAE Society of Automotive Engineers ,  @MemphisTigerBaja  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                    
                                 
                               
                              
                               
                                 
                                   SFB Society for Biomaterials  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                    
                                 
                               
                              
                               
                                 
                                   SME Society of Manufacturing Engineers  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                      
                                 
                               
                              
                               
                                 
                                   Tau Beta Pi – Engineering Honor Society  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                    
                                 
                               
                              
                               
                                 
                                   Alpha Eta Mu Beta - Biomedical Engineering Honor Society  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                    
                                 
                               
                              
                               
                                 
                                   IEEE-Eta Kappa Nu  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                    
                                 
                               
                              
                               
                                 
                                   SME Society of Manufacturing Engineers  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                    
                                 
                               
                              
                               
                                 
                                   SWE Society of Women Engineers ,  UofM SWE facebook  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                    
                                 
                               
                              
                               
                                 
                                   Tau Alpha Pi - Engineering Technology Honor Society  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                    
                                 
                               
                              
                               
                                 
                                   WTS Women in Transportation  
                                 
                               
                              
                            
                           
                         
                        
                      
                     
                          
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Future Students 
                         
                            
                               
                                Choose Your Major  
                                Visit the College  
                                Apply to the UofM  
                                Scholarships  
                                Student Life  
                                FAQs  
                            
                         
                      
                      
                      
                         
                            
                                Visit Herff  
                               See what Herff has to offer YOU! 
                            
                            
                                Apply to UofM  
                               Submit your application and enroll in the College of Engineering 
                            
                            
                                2017 E-day Results  
                               Thanks to 3,000 student participants!  
                            
                            
                                Contact Us  
                               Have Questions? We can help!  
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


