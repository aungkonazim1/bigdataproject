Keyboard-only navigation accessibility features | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Keyboard-only navigation accessibility features | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Accessibility    »    Using assistive technology in Desire2Learn   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Keyboard-only navigation accessibility features                       
        
        
       
          
     
           Printer-friendly version       
	The Desire2Learn Learning Suite includes a number of features aimed at improving the usability of our systems for people with disabilities. The following list outlines some of the design decisions that benefit people that navigate our products by keyboard or using an assistive technology that emulates a keyboard:
 

  
		Standard page designs. Similar functionality is located in the same place and accessed in the same way across tools.
	 
	 
		Full keyboard accessibility. The tab order is logical and tab focus visually indicated.
	 
	 
		Keyboard alternatives for drag-and-drop functionality and other dynamic features.
	 
	 
		Simplified pages that divide content across tabs and hide secondary content in expandable/collapsible sections.
	 
	 
		Unique link, button and field names for easy searching.
	 
	 
		Keyboard accessible HTML Editor (WYSIWYG).
	 
	 
		WAI-ARIA markup is used for tabs, context menus, error and confirmation messages, and page landmarks to help improve navigation.
	 
      Audience:    Learner      

    
           

                   ‹ Screen reader tips 
        
                   up 
        
                   Keyboard-only navigation tips › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Accessibility  
  
      Using assistive technology in Desire2Learn    ReadSpeaker docReader    Screen reader accessibility features    Screen reader tips    Keyboard-only navigation accessibility features    Keyboard-only navigation tips    Screen magnifiers, zooming, and color contrast accessibility features    Screen magnifiers, zooming, and color contrast tips    Getting additional support      
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
