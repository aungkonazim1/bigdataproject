Colleges and Schools - Development - University of Memphis    










 
 
 
     



 
    
    
    Colleges and Schools - 
      	Development
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Development
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Donate Now  
                          Giving Options  
                          Ways to Give  
                          Donor Recognition  
                          About Us  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Development
                              	  
                          
                              	Giving Options
                              	  
                         Colleges and Schools 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                        
                     
                      Colleges and Schools 
                     
                       Cecil C. Humphreys School of Law - The Cecil C. Humphrey's School of Law, recently named one of "America's Best Values"
                        by National Jurist magazine, relocated in 2010 to a $48 million, 160,000 square foot,
                        state-of-the art facility in a fully-renovated historic building in downtown Memphis.
                        Consider supporting a graduating 3L with bar application support here ( Law School - One Student, One Bar Exam, $1,00 0) or you can  Support School of Law ...
                      
                     
                       For more information on options, please contact the  Director of Development  for the Cecil C. Humphreys School of Law.  
                     
                       College of Arts and Sciences  - The College of Arts and Sciences is the largest college with 24 academic units,
                        ROTC, and multiple research centers, including two Centers of Excellence. It awards
                        nearly 500 undergraduate degrees per year and offers a comprehensive liberal arts
                        curriculum to equip its students in pursuing rich personal and professional lives.
                         Support CAS ...
                      
                     
                       For more information on options, please contact the  Director of Development  for the College of Arts and Sciences.  
                     
                       College of Communication and Fine Arts  - Established in 1977 and offering both undergraduate and graduate degrees, the College
                        of Communication and Fine Arts houses the Art Museum, the Institute of Egyptian Art
                        and Archaeology, the Rudi E. Scheidt School of Music, and the departments of Art,
                        Architecture, Communication, Journalism and Theatre   Dance.  Support CCFA ...
                      
                     
                       For more information on options, please contact the  Director of Development  for the College of Communication and Fine Arts.  
                     
                       College of Education  - The founding program of the University of Memphis, the College dates back to 1912,
                        when West Tennessee State Normal School first opened its doors to students. The College
                        is home to 11 nationally recognized teacher education programs. It is also home to
                        the Barbara K. Lipman Early Childhood School and Research Institute and the Campus
                        Elementary School, which are among fewer than 100 cutting-edge university laboratory
                        schools in the U.S.  Support COE ...
                      
                     
                       For more information on options, please contact the  Director of Development  for the College of Education.  
                     
                       Fogelman College of Business   Economics  – The Fogelman College of Business   Economics is proud to be fully accredited by
                        the American Assembly of Collegiate Schools of Business (AACSB), an honor earned by
                        less than 5% of business school world-wide. It is composed of six departments and
                        provides a unique Complete Professional Program that holistically prepares undergraduate
                        students with the tools to achieve both personal and professional excellence. Through
                        the Avron B. Fogelman Professional Development Center, FCBE provides training on ethics,
                        healthy living habits, business etiquette, resume building, professionalism, philanthropic
                        activities, and more. The Fogelman College offers a comprehensive portfolio of graduate
                        programs including six different tracks to earn a MBA. Fogelman boasts the largest
                        number of MBA graduates in Tennessee. Furthermore, the College works very closely
                        with Memphis-area companies, many of them Fortune 500, to offer students invaluable
                        hands-on learning experiences from professionals in these industries.  Support FCBE ...
                      
                     
                       For more information on options, please contact the  Director of Development  for the Fogelman College of Business   Economics.  
                     
                       Herff College of Engineering  - The Herff College of Engineering offers both undergraduate and graduate programs
                        and serves the educational and research needs of the industrial community, the metropolitan
                        area, the state and the nation.  Support Herff ...
                      
                     
                       For more information on options, please contact the  Director of Development  for the Herff College of Engineering.  
                     
                       Kemmons Wilson School of Hospitality and Resort Management  - The Kemmons Wilson School of Hospitality and Resort Management at the University
                        of Memphis is a prominent program providing a contemporary hospitality education,
                        with an emphasis on lodging and resort operations.  Support Kemmons Wilson School ...
                      
                     
                       For more information on options, please contact the  Director of Development  for the Kemmons Wilson School of Hospitality and Resort Management.  
                     
                       University Libraries  - The University Libraries include the award-winning McWherter Library and five branch
                        libraries. In addition to serving the University of Memphis, the libraries serve students
                        and faculty at other educational institutions, local and regional corporations, governmental
                        agencies, and citizens of the community.  Support the Libraries ...
                      
                     
                       For more information on options, please contact the  Director of Development  for the University Libraries.  
                     
                       Loewenberg School of Nursing  - The Loewenberg School of Nursing offers both the Bachelor and Master of Science
                        in Nursing degrees as well as the Executive Master of Science in Nursing Administration.
                        Both graduate and undergraduate students benefit from exceptional learning opportunities
                        at healthcare agencies in the Memphis area, including 10 major hospitals.  Support the School of Nursing ...
                      
                     
                       For more information on options, please contact the  Director of Development  for the Loewenberg School of Nursing.  
                     
                       The Rudi E. Scheidt School of Music  of The University of Memphis is staffed with an outstanding faculty of artists and
                        Pedagogues. It is the largest music program in the state of Tennessee and one of the
                        largest in the southeastern United States. Currently we have over 500 majors with
                        forty-five full-time faculty and more than twenty adjunct instructors. The School
                        of Music is the only institution in the state that offers a doctoral degree in music.
                        Activities abound in our department with more than 300 concerts, recitals, and other
                        special events each year.  Support the Rudi E. Scheidt School of Music ....
                      
                     
                       For more information on options, please contact the  Director of Development  for the Rudi E. Scheidt School of Music.  
                     
                       School of Communication Sciences and Disorders  – The School of Communication Sciences and Disorders became part of the University
                        in 1967. U.S. News   World Report ranked the U of M's audiology program eighth in
                        the nation; the speech pathology program is ranked thirteenth nationwide. The school
                        offers graduate programs leading to Ph.D., M.A. and Au.D. degrees with concentrations
                        in Audiology and Speech-Language Pathology.  Support the School of Communication Sciences and Disorders ...
                      
                     
                       For more information on options, please contact the  Director of Development  for the School of Communication Sciences and Disorders.  
                     
                       School of Public Health  – Established in June 2009, the School of Public Health serves a unique mission to
                        educate the next generation of leaders in public health with a focus on urban health
                        and health equity. Within our urban-serving institution, University students and faculty
                        seek to translate their research into life-changing discoveries and interventions
                        that affect the full scope of society.  Support SPH ...
                      
                     
                       For more information on options, please contact the  Director of Development  for the School of Public Health.  
                     
                       School of Health Studies  - Our seven academic disciplines (Dietetics and Clinical Nutrition, Environmental
                        Nutrition, Exercise Science, Healthcare Leadership, Health Sciences/Health Promotion,
                        Nutrition Science, and Physical Education Teacher Education) are home to approximately
                        900 undergraduate and graduate students. We strongly encourage students to go beyond
                        the typical educational requirements and get involved in extracurricular activities
                        that will help to support their learning and overall development, such as internships,
                        majors clubs, special projects, and participation in faculty directed research studies.
                      
                     
                       For more information on options, please contact the  Director of Development  for the School of Heath Studies.  
                     
                       Student Affairs  - The Division of Student Affairs provides services, programs, and activities that
                        support all schools and colleges by building on the university's academic base. The
                        division strives to consider all aspects of a student's life at the university, always
                        trying to be mindful of the "whole person" and the role the university can play in
                        a student's development as an individual, working and living responsibly within a
                        community.  Support Student Affairs  
                     
                       For more information on options, please contact the  Director of Development  for the Student Affairs.  
                     
                       University College  - The University College offers individualized and interdisciplinary degree programs,
                        enabling University of Memphis students to pursue a wide range of studies and shape
                        degree programs that build upon prior experience and knowledge. The College has two
                        undergraduate degrees, the Bachelor of Professional Studies and the Bachelor of Liberal
                        Studies, which offer more than 20 degree concentrations.  Support University College ...
                      
                     
                       For more information on options, please contact the  Director of Development  for the University College.  
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Staff Directory  
                               Meet the Development Staff 
                            
                            
                                University of Memphis Foundation  
                               Learn about the University of Memphis Foundation 
                            
                            
                                Alumni Association  
                               Learn about the Alumni Association 
                            
                            
                                Glossary  
                               Click here for a Glossary of Terms 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/26/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


