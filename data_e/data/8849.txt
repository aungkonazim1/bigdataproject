Starting a conversation in Wiggio | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Starting a conversation in Wiggio | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   English  
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Wiggio    »    Wiggio basics   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Starting a conversation in Wiggio                       
        
        
       
        
     
               
		In the  Start a conversation  field, enter your text.
	 
	 
		You can click on the following icons to add resources to your conversation:
		  
				     Files  
			 
			 
				     Link  
			 
			 
				     Meeting  
			 
			 
				     To-do  
			 
			 
				     Poll  
			 
			 
				     Message  
			 
			 
				     Event  
			 
		  
	 
		Do one of the following:


		  
				Select a group of participants from the  Participants  drop-down list.
			 
			 
				Add participants by entering their name or email into the  Add by name or email...  field and pressing Enter on your keyboard.
			 
			 
				Click the   Import from email  icon to add participants from your email contact list.
			 
		  
			 Tip  Expand and review your list of participants by clicking the  Show participants  link. Delete participants by clicking the   Remove  icon by their name.
		 
	 
	 
		Click  Post .
	 
  
	See also
 

  
		 Adding files to Wiggio 
	 
	 
		 Adding a link to Wiggio 
	 
	 
		 Adding a meeting to Wiggio 
	 
	 
		 Adding a to-do list to Wiggio 
	 
	 
		 Adding a poll to Wiggio 
	 
	 
		 Adding a message to Wiggio 
	 
	 
		 Adding an event to Wiggio 
	 
      Audience:     Learner       
    
         
               ‹ Accessing Wiggio 
                     up 
                     Replying to a conversation in Wiggio › 
           
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Wiggio  
  
      Wiggio basics    Accessing Wiggio    Starting a conversation in Wiggio    Replying to a conversation in Wiggio    Deleting a conversation in Wiggio    Updating your profile in Wiggio    Adding contacts to Wiggio    Communicating privately with contacts in Wiggio    Deleting contacts in Wiggio    Wiggio notifications      Adding resources to Wiggio    Using polls in Wiggio    Using to-do lists in Wiggio    Creating and using folders in Wiggio    Creating and using groups in Wiggio    Managing groups in Wiggio     Managing events in Wiggio    Importing and exporting calendars in Wiggio    
                  
           
         

       
     

    
    
    
   
 
   
 
