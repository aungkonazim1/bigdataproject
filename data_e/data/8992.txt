Setting your notifications | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Setting your notifications | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Getting Started in Learning Environment    »    Notifications   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Setting your notifications                       
        
        
       
        
     
              
	The Notifications tool enables you to:
 

  
		Receive instant notifications about course activity, such as grades, new discussion posts, upcoming quizzes, dropboxes, and news items. You can also choose to receive instant notifications about ePortfolio feedback and subscription activity.
	 
	 
		Subscribe to a summary of activity for each course and receive a daily email.
	 
	 
		Specify your preferred email address and mobile number for instant notifications. You can choose a different email address than your system email address.
	 
  
	Table of contents
 

  
		 Set up an email contact method 
	 
	 
		 Set up a mobile contact method 
	 
	 
		 Subscribe to a summary of activity 
	 
	 
		 Subscribe to instant notifications 
	 
	 
		 Exclude courses from notifications 
	 
	 
		 Restore excluded course notifications 
	 
  
	  Set up an email contact method
 

  
		Click  Enable email notifications  on the Notifications page.
	 
	 
		Select your system email or custom email. If you select  Use custom email , enter your email address in the text field.
	 
	 
		Click  Save .
	 
  
	  Set up a mobile contact method
 

  
		Click  Register your mobile  on the Notifications page.
	 
	 
		Select your  Country , your  Mobile Carrier , and enter your  Mobile Number  in the form.
	 
	 
		Click  Save .
	 
	 
		After you receive a registration confirmation code on your mobile device, enter it in the  Confirmation Code  field.
	 
	 
		Click  Confirm .
	 
  
	  Subscribe to a summary of activity
 

 
	 Note  You must set up your email contact method on the Notifications page to subscribe to a summary of activity.
 

  
		On the Notifications page, select the delivery frequency you want from the  How often?  drop-down list.
	 
	 
		Select when you want to receive your summary from the  At what time?  drop-down list.
	 
	 
		Click  Save .
	 
  
	  Subscribe to instant notifications
 

  
		On the Notifications page, enable instant notifications for specific course activity and updates by doing any of the following:
		  
				Select the  Email  check box to enable email notifications.
			 
			 
				Select the  SMS  check box to enable SMS notifications.
			 
			 
				Select the  Campus Life  check box to enable Campus Life notifications.
			 
		  
	 
		Click  Save .
	 
  
	 Notes 

	  
			Customization options are available if you want to include your grade value in Grades notifications, and if you want to allow past, future, and inactive course enrollments to send you notifications. Go to the Customize Notifications section on the Notifications page and select the check boxes beside the options you want to enable.
		 
		 
			Users with cascading enrollments will only receive notifications for the org unit they are initially enrolled in (i.e., the source enrollment) and will not receive notifications from any org units they are enrolled in as an indirect result of their cascading role. This prevents these users from being flooded with notifications from potentially thousands of courses.
		 
	  

 
	  Exclude courses from notifications
 

  
		Click  Manage my course exclusions  in the Exclude Some Courses section on the Notifications page.
	 
	 
		Click the    Exclude  icon beside each course you want to exclude, or click  Exclude All Courses  to stop all course notifications. You can also use the Search field to find the course you want to exclude.
	 
	 
		Click  Close .
	 
	 
		Click  Save .
	 
  
	  Restore excluded course notifications
 

  
		Click the  Manage my course exclusions  link in the Exclude Some Courses section on the Notifications page.
	 
	 
		Click the    Restart notifications  icon beside each course you want to receive notifications from, or click  Restore excluded courses  to restore all excluded courses. You can also use the Search field to find the course you want to restore.
	 
	 
		Click  Close .
	 
	 
		Click  Save .
	 
      Audience:     Learner       

    
           

                   ‹ Accessing Notifications 
        
                   up 
        
                   Viewing Learning Environment with role switch › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Getting Started in Learning Environment  
  
      Tasks to complete before logging in to Learning Environment    Learning Environment basics    Account Settings    Notifications    Accessing Notifications    Setting your notifications      Viewing Learning Environment with role switch    
                  
           
         

       
     

    
    
    
   
 
   
 
