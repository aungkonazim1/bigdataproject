Workers Compensation - University Benefits Administration - University of Memphis    










 
 
 
     



 
    
    
    Workers Compensation - 
      	University Benefits Administration
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			University Benefits Administration
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Staff Directory  
                          Benefits Information  
                          Related Resources   Links  
                          Human Resources  
                      
                      
                         
                            Benefits Information   
                            
                               
                                   Prospective Employees  
                                   New Faculty  
                                   New Staff  
                                   Current Employees  
                                   Departing Employees  
                                   Retiring Employees  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	University Benefits Administration
                              	  
                          
                              	Benefits Information
                              	  
                         Workers Compensation 
                      
                   
                   
                       
                      
                     		
                     
                     
                      Workers Compensation 
                     
                      During the course of employment at the University of Memphis, we hope you are never
                        injured on the job, or suffer from an occupational illness. If you do have an on-the-job
                        injury, proper reporting of the situation will expedite the filing of a claim for
                        medical expenses and compensation if necessary.
                      
                     
                       What should I do if I am injured on the job?  
                     
                      
                        
                         If it is an emergency, seek treatment at the nearest emergency room. Then contact
                           your supervisor and the University Benefits Administration Office as soon as possible
                           to start the claim process.
                         
                        
                         In a non-emergency, notify your supervisor immediately and follow the steps below.
                           
                            
                              
                               Tell your supervisor what happened, how it happened, who saw what happened, and if
                                 you were injured as a result of the accident.
                               
                              
                               You and your supervisor should then call the Workplace Injury and First Notice of
                                 Loss Call Center at 1.866.245.8588.
                               
                              
                               Select option #1 to speak with a registered nurse who will evaluate the nature of
                                 your injury and determine your immediate care or treatment options. Your supervisor
                                 will only verify that you are reporting a work related injury to the registered nurse.
                               
                              
                               If no medical treatment is recommended, the registered nurse will document the call
                                 for you and your supervisor and enter an incident report in the reporting system.
                               
                              
                               If medical treatment is recommended, the nurse will direct you to the nearest State
                                 approved medical provider.
                               
                              
                            
                           
                         
                        
                         You and your supervisor will need to complete the First Report of Injury or Illness
                           form found online at  First Report of Injury or Illness . This form should be returned to the Environmental Health and Safety Office.
                         
                        
                      
                     
                       What to do when seeking medical treatment:  
                     
                      
                        
                         Upon arrival, you should notify the medical facility personnel that you were injured
                           while on the job.
                         
                        
                         Follow-up doctor and/or specialist appointments must be arranged by CorVel and NOT
                           by the injured employee or supervisor.
                         
                        
                         You may contact CorVel to discuss existing claims at 1.888.226.7835. 
                        
                      
                     
                       What to do after seeking medical treatment:  
                     
                      
                        
                         It is the employee's responsibility to keep the University Benefits Administration
                           Office notified regarding their work status.
                         
                        
                         All medical documentation must be forwarded and provided to the University Benefits
                           Administration Office only – not to your supervisor. Due to HIPAA privacy compliance,
                           ONLY the University Benefits Administration Office will store medical records related
                           to an employee's on the job injury.
                         
                        
                      
                     
                       For Accidents Which Occur After Hours  The Workplace Injury and First Notice of Loss Call Center at 1.866.245.8588 operates
                        24/7. In non-emergency situations, the employee and supervisor should call the Call
                        Center and follow the steps above.
                      
                     
                       First Report of Injury or Illness  
                     
                       Near Miss Form  
                     
                       Important Contacts  
                     
                      
                        
                         CorVel's Workplace Injury and First Notice of Loss Call Center: 1.866.245.8588 
                        
                          Wallet Card  
                        
                         University Benefits Administration: 901.678.3573 
                        
                         Environmental Health   Safety: 901.678.5700 
                        
                          State of Tennessee Workers Compensation Division  
                        
                      
                     
                     	
                      
                   
                
                
                   
                      
                         Benefits Information 
                         
                            
                                Prospective Employees  
                                New Faculty  
                                New Staff  
                                Current Employees  
                                Departing Employees  
                                Retiring Employees  
                            
                         
                      
                      
                      
                         
                            
                                Tiger Perks  
                               Exclusive discounts offered to UofM employees 
                            
                            
                                Affordable Care Act  
                               Important information related to the Affordable Care Act 
                            
                            
                                Forms  
                               All of B F's forms in one place 
                            
                            
                                Business   Finance  
                               The Division of Business   Finance at the UofM 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


