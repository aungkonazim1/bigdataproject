Updating your profile in Wiggio | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Updating your profile in Wiggio | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Integrations    »    Wiggio    »    Wiggio basics   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Updating your profile in Wiggio                       
        
        
       
          
     
           Printer-friendly version       
			 Note Any changes you make to your Wiggio profile will be visible in all groups you are a member of.   Click on your name in the Wiggio navbar, then select  Your Profile . 
	 Make updates to your  Name ,  Email , or  Password  by clicking   Edit   beside the field you want to change. 
	 Click  Select Image  to upload a profile picture, or  Delete  to remove an  image. 
	 To add AIM, Skype, Facebook, Twitter, or a personal blog to your profile, click  add  beside the appropriate application name. 
	 Enter your cell phone number in the  Cell Phone Number  field and select your carrier information from the Cellular Carrier drop-down list to receive text messages from other group members. 
	 Select  Hide email address  or  Hide phone number  to prevent other group members viewing your email address or phone number. 
	 Click  Save . 
      Audience:    Learner      

    
           

                   ‹ Filtering and unfiltering group feeds in Wiggio 
        
                   up 
        
                   Adding contacts to Wiggio › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Wiggio  
  
      Wiggio basics    Accessing Wiggio    Wiggio browser support    Using Wiggio without belonging to a group    Wiggio terminology    Starting a conversation in Wiggio    Replying to a conversation in Wiggio    Deleting a conversation in Wiggio    Filtering and unfiltering group feeds in Wiggio    Updating your profile in Wiggio    Adding contacts to Wiggio    Communicating privately with contacts in Wiggio    Deleting contacts in Wiggio    Wiggio notifications      Creating and using groups in Wiggio    Managing groups in Wiggio     Adding resources to Wiggio    Creating and using folders in Wiggio    Using virtual meetings in Wiggio    Using polls in Wiggio    Using to-do lists in Wiggio    Managing events in Wiggio    Importing and exporting calendars in Wiggio    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
