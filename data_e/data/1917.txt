Career Services Office - School of Law - University of Memphis  The Career Services Office (CSO) is committed to assisting students and graduates of Memphis Law with job search and professional development needs.  










 
 
 
     



 
    
    
    Career Services Office - 
      	School of Law 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Cecil C. Humphreys School of Law
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Admissions  
                          Programs  
                          Current Students  
                          Faculty  
                          Careers  
                          Library  
                      
                      
                         
                            Career Services   
                            
                               
                                  
                                   Student Resources   Services  
                                         Services  
                                         Symplicity Guide  
                                         Career Pathways  
                                         Clerkships  
                                         Pro Bono  
                                         On Campus Interviews  
                                         1L CSO Orientation  
                                         Job Fairs  
                                         Job Postings  
                                     
                                  
                                   Alumni Resources   Services  
                                         Services  
                                         Reciprocity  
                                         Symplicity  
                                         Get Involved  
                                     
                                  
                                   Employer Resources  
                                         Posting a Job  
                                         On Campus Recruiting  
                                         Resume Collection Program  
                                         Increasing Your Visibility  
                                     
                                  
                                   Employment Statistics  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	School of Law 
                              	  
                         
                           	Career Services
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                        
                     
                      Career Services Office 
                     
                      The Career Services Office (CSO) is committed to assisting students and graduates
                        of Memphis Law with job search and professional development needs.
                      
                     
                      Our staff strives to maximize employment by marketing to employers and promoting placement
                        opportunities. Our office does not guarantee jobs or placement services, but works
                        with students and alumni to ensure that they possess the skills necessary to conduct
                        a successful job search at any point in their career.
                      
                     
                      ABA Employment Summaries 
                     
                      
                        
                          Class of 2014  
                        
                          Class of 2015  
                        
                          Class of 2016  
                        
                      
                     
                      NALP Reports 
                     
                      
                        
                          Class of 2013  
                        
                          Class of 2014  
                        
                          Class of 2015  
                        
                          Class of 2016  
                        
                      
                     
                      Information regarding race, gender, and ethnicity has been redacted from the above
                        NALP reports.
                      
                     
                      Career Services Office Cecil C. Humphreys School of Law 1 N. Front Street, Room 236 Memphis, TN 38103 Telephone: (901) 678-3217 Fax: (901) 678-4107 E-mail:  lawcareerservices@memphis.edu  Office Hours: Monday – Friday, 8:30 a.m. – 5 p.m.
                      
                     
                       It is the policy of the University of Memphis Cecil C. Humphreys School of Law that
                           no citizen of the United States or any other person within the jurisdiction thereof
                           shall, on the grounds of race, color, religion, creed, national origin, sex, sexual
                           orientation, gender identity/expression, disability, age, status as a protected veteran,
                           genetic information, or any other legally protected class, be excluded from participation
                           in or be denied the benefits of, or be subjected to discrimination under any program
                           or activity of the University. Employers who advertise positions through the Career
                           Services Office or participate in CSO recruitment programs must read the non-discrimination
                           policy and undertake to observe it.  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Career Services 
                         
                            
                               
                                Student Resources   Services  
                                      Services  
                                      Symplicity Guide  
                                      Career Pathways  
                                      Clerkships  
                                      Pro Bono  
                                      On Campus Interviews  
                                      1L CSO Orientation  
                                      Job Fairs  
                                      Job Postings  
                                  
                               
                                Alumni Resources   Services  
                                      Services  
                                      Reciprocity  
                                      Symplicity  
                                      Get Involved  
                                  
                               
                                Employer Resources  
                                      Posting a Job  
                                      On Campus Recruiting  
                                      Resume Collection Program  
                                      Increasing Your Visibility  
                                  
                               
                                Employment Statistics  
                            
                         
                      
                      
                      
                         
                            
                                Apply to Memphis Law  
                                
                            
                            
                                News   Events  
                                
                            
                            
                                Alumni   Support  
                                
                            
                            
                                ABA Required Disclosures  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


