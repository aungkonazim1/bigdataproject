New to the Libraries | University of Memphis Libraries   
 
 
 
 
 New to the Libraries | University of Memphis Libraries 
 
 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 
 




 
 
  
 
 
 

 
 
 
 
			

         



 

 
 
	 Skip to content 
		 
				 
						  University of Memphis Libraries  
			 News and Events 
		 

		 
			 Menu 
			  
  Home    
		  
	  

	 
			 
			 Home  New to the Libraries 		 
		 
		 

					 
				
 
	 
					 New to the Libraries 
		
		 
						   September 14, 2015  September 15, 2015       mswrngen   
			  Uncategorized  		  
	  

	 
		                 
 The new Health Sciences Library (HSL), or the Baptist Memorial Health Care Library, is on the second floor of the Community Health building on the Park Avenue Campus. The hours are 8:00 am – 8:00 pm on Monday – Thursday, 8:00 am – 4:30 pm on Friday, and closed on Saturday and Sunday. The HSL staff are John Swearengen, relocating from Communication Sciences, and Rose Moore, relocating from Math. The library has a three-story high ceiling and windows which look out onto an outdoor patio, and includes four large computer/study tables, twelve study alcoves, and three group study rooms with multimedia capability which can be reserved online at  www.memphis.edu/libraries/reservations/hs_space.php . HSL’s collection includes what was Communication Science’s collection as well as the last eight years of nursing monographs and all the nursing journals from McWherter Library. 
    Newly available to check out at McWherter Library is Oculus Rift. The Rift is a virtual reality head-mounted display developed by Oculus VR. The Librares currently has the Oculus Rift Development Kit, which affords more flexibility and encourages creativity in the development of virtual reality environments. The Rift is only available to graduate students and faculty with a stated research interest. For more info, visit the Rift libguide:    http://libguides.memphis.edu/oculus . 
 Other technology available to check out at the Libraries includes touch screen kits, calculators, digital cameras, STEM tech (such as soldering irons and multimeters), projectors, and HD audio recorders. Available in the Technology Sandbox, located in the McWherter Library First Floor Commons Room, are GIS, design, and data science programs, which students can access for free. Also available in the Sandbox is a 3D printing lab where students can print and scan in 3D. 
 Emerging technology training is also available at McWherter Library. Training sessions now available include a 3D printing session, a session on GoPro cameras, a session on our circulating technology, and a session on HTML/CSS, web design for beginners. To register for free, visit:  http://www.memphis.edu/libraries/technology/training.php . 

 
			  

	 
			  
  
			 

				 
		 Post navigation 

	
		      Maymester at the Libraries  		  What s New in 2016?      
	
	  
	
			
 

	
	
	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  

		
		  
	  

					 
					 		 Recent Posts 		 
					 
				 3D Printing Workshop 
						 
					 
				 NedXStudents 
						 
					 
				 Craft-n-Chat 
						 
					 
				 Bridging East and West: The First Steel Bridge of Memphis 
						 
					 
				 Banned Books Display 
						 
				 
		 		  Archives 		 
			  November 2017  
	  October 2017  
	  September 2017  
	  August 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  April 2016  
	  February 2016  
	  September 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  May 2014  
	  April 2014  
	  March 2014  
	  February 2014  
	  January 2014  
		 
		   Categories 		 
	  Alerts 
 
	  Branch Libraries 
 
	  Events 
 
	  Exhibition 
 
	  Homepage 
 
	  News 
 
	  Research and Instructional Services 
 
	  Special Collections 
 
	  Trial Resources 
 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 		  
	
	  

	 
		 
			 
								 Proudly powered by WordPress 
				  |  
				Theme: Big Brother by  WordPress.com .			  
					  
	  
  


 

        

        			 
				   Subscribe  
				 

					
						 Follow this blog 

						 
							
															 Get every new post delivered right to your inbox. 
							
							 
								 
							 
							
							 
							 
							
							  							   
						 

					
				 
			 
		







		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 