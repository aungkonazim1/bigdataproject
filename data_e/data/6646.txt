Interns - Kemmons Wilson School of Hospitality &amp; Resort Management - University of Memphis    










 
 
 
     



 
    
    
    Interns - 
      	Kemmons Wilson School of Hospitality   Resort Management
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Kemmons Wilson School of Hospitality   Resort Management 
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Hospitality  
                          Sport   Leisure  
                          Students  
                          Careers  
                          Faculty  
                          Contact Us  
                      
                      
                         
                            Careers   
                            
                               
                                  
                                   Hospitality and Resort Management  
                                         Careers  
                                         Success Stories  
                                         Current Opportunities  
                                     
                                  
                                   Sport and Leisure Management  
                                         Careers  
                                         Internships  
                                         Success Stories  
                                     
                                  
                                   Employers  
                                   Events  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Kemmons Wilson School of Hospitality   Resort Management
                              	  
                          
                              	Careers
                              	  
                         Interns 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Interns in Action 
                     
                       Dantavius Swift, SLM '17     Senior Capstone Experience– Memphis Grizzlies  
                     
                        "For my final semester as an undergraduate student, I have been interning with the
                        Memphis Grizzlies. My experience has been everything and more that I have ever dreamed
                        of! Entering college I always had the dream of working within a professional sports
                        organization, so this was literally a dream turned into a reality. I am currently
                        interning in the department of Basketball Operations. My primary responsibilities
                        consist of helping the head equipment manager with practices and games, being a point
                        of contact for visiting teams and player personnel, as well as being a liaison between
                        players, coaches, and media correspondents.
                      
                     
                      The Sport and Leisure Management program at the University of Memphis prepared me
                        for internship with the Grizzlies in various facets. Having to enroll in courses dealing
                        with event management, cultural perspectives, public relations, and sport governance
                        gave me a "leg up" on what to expect when working for a professional organization.
                        Being awarded the opportunity of being taught by professors, such as Dr. Berg and
                        Dr. Fuller, who have invaluable amounts of experience also provided me with the resources
                        and tools to become more aware of how to perform at the highest standard.
                      
                     
                      I thoroughly enjoy my position and I look forward to what will come for me in the
                        future! GO GRIZZ!"
                      
                     
                       Amber Clanton, SLM '15     Senior Capstone Experience– Texas Rangers, Ballpark Entertainment   Promotions  
                     
                        “My experience with the Texas Rangers has been nothing short of amazing! It has been
                        an huge        aspiration of mine for the longest time to pursue a career in professional
                        sports, and being able to    intern with an organization that is doing so well in
                        the season is a phenomenal feeling. I work within    the Ballpark Entertainment and
                        Promotions department, and my job is to assist with interpreting and  executing promotions
                        on concourses and on-field during games, as well as in the Dallas-Fort Worth  community.
                      
                     
                       The Sport   Leisure Management program has helped me prepare for my position with
                        the Texas  Rangers in many ways. Courses like SLS 4135 Sport/Culture Global Perspective
                        helped me broaden  my horizons to opportunities outside of the Memphis-area, while
                        SLS 4205 Legal/Ethical Sport    Leisure Studies helped me to understand and be more
                        cautious of possible liability issues, antitrust  laws, and crowd management at a
                        huge facility such as Globe Life Park in Arlington. SLS 3650 Sport  Info/Public Relations
                        was my favorite because it sums up my duties within the promotions  department. This
                        course basically gave me a "heads up" of what to expect when being the liaison  between
                        the Rangers organization, sponsors, and fans!
                      
                     
                       I love what I do and look forward to coming back for a second season with the Texas
                        Rangers in  2016! GO RANGERS!”
                      
                     
                         
                     
                       Ofelia Carmichael, SLM '16     Summer Internship Experience – Specialized Marketing Group   
                     
                       “This summer I got the opportunity to intern with The Specialized Marketing Group
                        (TSMGI) in Skokie, IL. I got to assist with the various divisions of the company which
                        included Global Sports Marketing, Motorsports, Promotions, and Creative.
                      
                     
                      Research and presentation skills prepared me best for this internship. The great thing
                        about this major is the amount of practice we get with researching for papers and
                        presentations. This practice allowed me to effectively work with my co-workers and
                        effectively communicate my ideas. There were numerous times when I was tasked with
                        researching a potential market with the other interns and we had to put together a
                        presentation within a couple days. It was fast paced and constant; it’s an extremely
                        fun environment to work in.
                      
                     
                      My favorite part of the internship was the events we got to work for a couple of our
                        clients. My favorite experience was at Hot Import Nights where I represented Valvoline.
                        Hot Import Nights is the largest consumer car show in the world. It shows off the
                        hottest cars, models, music, gaming, technology, and innovation. One of TSMGI’s clients
                        is Valvoline and Valvoline wanted to use this event as an activation to improve their
                        customer base. What was so unique about this experience was getting the opportunity
                        to spend time with Formula One Drift driver, Ryan Tuerck. Valvoline brought in this
                        driver as a way to provide some Star Power to the tent and get people over to see
                        the Valvoline cars and products. Usually drivers that show up for these events only
                        stay for an hour to sign autographs for fans, but Ryan stayed for the entire event.
                        That was extremely impressive to me and was a testament to Ryan’s loyalty to Valvoline
                        and his fans. It was great to see the process of the discussion and planning of how
                        Valvoline would put forth their activation to the actual set-up of the event to the
                        event itself. It gave me a new appreciation for the amount of time and planning that
                        goes into just one event.”
                      
                     
                         
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Careers 
                         
                            
                               
                                Hospitality and Resort Management  
                                      Careers  
                                      Success Stories  
                                      Current Opportunities  
                                  
                               
                                Sport and Leisure Management  
                                      Careers  
                                      Internships  
                                      Success Stories  
                                  
                               
                                Employers  
                                Events  
                            
                         
                      
                      
                      
                         
                            
                                Apply to the KWS Program  
                               Choose the degree program that's right for you 
                            
                            
                                Academic Advising  
                               Schedule an appointment with your advisor 
                            
                            
                                Research Highlights  
                               KWS faculty are leaders in their fields of study 
                            
                            
                                Contact Us  
                               Questions? Our team can help. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/21/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


