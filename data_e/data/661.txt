Online Communication - UofM Global - University of Memphis  Online Communication  










 
 
 
     



 
    
    
    Online Communication - 
      	UofM Global
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






 

   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 

   
   
   
    
      
      
       
 
	
	
	
 
 

 


 
 

 
  
 
	 
		 
			 
				 
					     
				 
				     
				 
					 
						 
							 
								 
									   UofM Home  
										  myMemphis  
										  eCourseware  
										  Webmail  
										  Contact  
								     
								 
								 
									 Search 
									 
									 
									    
									 Site Index 
								 																
								  
							 
						 
					 
					 
						  
							 
								   About UofM Global      
									 
										  UofM Global  
										  Ranked Online Degrees  	
										  The UofM Global Experience  
										  Our Faculty  
										  Tuition   Fees  	
										  Accreditations  
									 
								 
								   Online Degrees      
									 
										  Online Degrees  
										  Undergraduate Degrees  
										  Graduate Degrees  
										  Graduate Certificates  
										  Online Admissions  
										  Contact Us  
									 
								 
								   Online Admissions      
									 
										  Online Admissions  
										  Apply Now   
										  Smart Start  
										  Financial Aid  
										  All Online Services  
									 
								 
								   Current Students      
									 
										  Student Services and Support  
										  Online Advising  
										  Course Registration  
										  Alternative Degree Pathways  
										  Learning Support  
										  Technology Support  
										  Library Services  
										  Career Services  
										  Additional Services  
										  Graduation  
									 
								 
								  Request Info  
							 
						 
					 
				 
			 
		 
		 
			 
				 
					  Online Degrees  
				 
			 
		 
	 
 
 
      
      
      
    
    
       
          
             
                
                   
                       
                           			UofM Global
                           	  
                   
                
             
          
       
       
          
             
                
                   
                      
                         
                             Home  
                             
                                 	UofM Global
                                 	  
                             
                                 	Student Services
                                 	  
                            Online Communication 
                         
                      
                      
                         
                            Student Services and Support   
                            
                               
                                  
                                   Student Services and Support  
                                   Online Advising Menu  
                                         Overview  
                                         Degree Planning  
                                         Find My Advisor  
                                     
                                  
                                   Course Registration  
                                         Overview  
                                         Online Course Search  
                                         Undergraduate Catalog  
                                         Graduate Catalog  
                                         Dates and Deadlines  
                                         Tuition   Fees  
                                         Scholarships  
                                     
                                  
                                   Alternative Degree Pathways  
                                         Overview  
                                         Finish Line Program  
                                         Prior Learning Assessment  
                                     
                                  
                                   Learning Support  
                                        
                                         Overview  
                                        
                                         Readiness Assessment  
                                        
                                         Success Strategies  
                                        
                                         Get Connected  
                                        
                                         Online Tutoring  
                                     
                                  
                                   Technology Support  
                                         Overview  
                                         Technology Requirements  
                                         Additional Services  
                                         Online Course Preview  
                                     
                                  
                                   Library Services  
                                         Overview  
                                         Ask-a-Librarian  
                                         Electronic Delivery  
                                         Research Consultations  
                                     
                                  
                                   Career Services  
                                   Additional Services  
                                         Overview  
                                         Adult and Veteran Students  
                                         Disability Resources  
                                     
                                  
                                   Graduation  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                     		
                     
                     
                        
                     
                      Get Connected 
                     
                      As a UofM Global student, you are provided a variety of opportunities to engage online
                        with other students and interact with your instructor in a fully online environment.
                        The following tools are available to you to help you get connected with your online
                        classes and the broader University of Memphis Community.
                      
                     
                      Email 
                     
                      Email is a great way to stay in touch with other students as well as your online instructors.
                        We recommend you use University of Memphis email (umMail) for all communication regarding
                        your online courses. To be successful, ensure you  check your email on a daily basis , as this may be the primary way your online instructors provide important updates
                        about your courses.
                      
                     
                        Log in to your email account      
                     
                      Discussion Boards 
                     
                      Many of your courses will require you to use discussion boards to  interact with your instructors and other students . These discussions provide meaningful opportunities to demonstrate what you have
                        learned and dig deeper into course content. Pay close attention to specific instructions
                        provided by your course instructors within the course syllabus regarding discussion
                        board requirements, as they may vary from course to course.
                      
                     
                      Blogging 
                     
                      Blogging is another method for  demonstrating knowledge and skills in a fully online environment . The University of Memphis provides a robust, easy-to-use tool to students who wish
                        to journal, blog, or create a  professional portfolio . Visit   blogs.memphis.edu   to learn more.   
                     
                        ONLINE TUTORING      
                     
                     	
                      
                   
                
                
                   
                      
                         Student Services and Support 
                         
                            
                               
                                Student Services and Support  
                                Online Advising Menu  
                                      Overview  
                                      Degree Planning  
                                      Find My Advisor  
                                  
                               
                                Course Registration  
                                      Overview  
                                      Online Course Search  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Dates and Deadlines  
                                      Tuition   Fees  
                                      Scholarships  
                                  
                               
                                Alternative Degree Pathways  
                                      Overview  
                                      Finish Line Program  
                                      Prior Learning Assessment  
                                  
                               
                                Learning Support  
                                     
                                      Overview  
                                     
                                      Readiness Assessment  
                                     
                                      Success Strategies  
                                     
                                      Get Connected  
                                     
                                      Online Tutoring  
                                  
                               
                                Technology Support  
                                      Overview  
                                      Technology Requirements  
                                      Additional Services  
                                      Online Course Preview  
                                  
                               
                                Library Services  
                                      Overview  
                                      Ask-a-Librarian  
                                      Electronic Delivery  
                                      Research Consultations  
                                  
                               
                                Career Services  
                                Additional Services  
                                      Overview  
                                      Adult and Veteran Students  
                                      Disability Resources  
                                  
                               
                                Graduation  
                            
                         
                      
                      
                     
                     
                      
                                 Need Help? 
                                 
                                     
                                         
											     Call 844-302-3886 
                                         
                                     
                                     
                                         
                                                 Email Us 
                                         
                                     
                                 
                             
                     
                     
                     
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
 
 
 
 
  Full sitemap  
 
 
 
 
  About UofM Global  
 
 
  UofM Global  
  Ranked Online Degrees  	
  The UofM Global Experience  
  Our Faculty  
  Tuition   Fees  	
  Accreditations  
 
  
 
  Online Degrees  
 
 
  Online Degrees  
  Undergraduate Degrees  
  Graduate Degrees  
  Graduate Certificates  
  Online Admissions  
  Contact Us  
 
  
 
 
 
  Online Admissions  
 
 
  Online Admissions  
  Apply Now   
  Smart Start  
  Financial Aid  
  All Online Services  
 
  
  Contact Us  
 
  Tuition  
  Financial Aid  
  
 
	  Current Students  
 
 
  Student Services and Support  
  Online Advising  
  Course Registration  
  Alternative Degree Pathways  
  Learning Support  
  Technology Support  
  Library Services  
  Career Services  
  Additional Services  
  Graduation  
 
  
 
 
 
 
 
   
 
 
   
 
 
   
 
 
 
 
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


