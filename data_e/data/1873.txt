CLEA Award 2016 - School of Law - University of Memphis    










 
 
 
     



 
    
    
    CLEA Award 2016 - 
      	School of Law 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Cecil C. Humphreys School of Law
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Admissions  
                          Programs  
                          Current Students  
                          Faculty  
                          Careers  
                          Library  
                      
                      
                         
                            About   
                            
                               
                                  
                                   Administration   Staff  
                                   History  
                                   Strategic Plan  
                                   Virtual Tour  
                                   ABA Disclosures  
                                   Diversity  
                                         Diversity Home  
                                         TIP  
                                         Scholarships  
                                     
                                  
                                   Facilities  
                                         Facilities Overview  
                                         Virtual Tour  
                                         Floor Plans  
                                         Photo Gallery  
                                     
                                  
                                   News   Events  
                                         Law School News  
                                         On Legal Grounds Blog  
                                         ML-Memphis Law Magazine  
                                         Events Calendar  
                                         Communications Office  
                                     
                                  
                                   Directions  
                                   Contact  
                                         Media Resource  
                                         General Contact Info  
                                         Faculty   Staff  
                                     
                                  
                                   Life in Memphis  
                                         Memphis-The City  
                                         Neighborhoods  
                                         Housing  
                                         The Arts Scene  
                                         Sports  
                                         The Food Scene  
                                         A Music Town  
                                         Directions and Maps  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	School of Law 
                              	  
                          
                              	About
                              	  
                         CLEA Award 2016 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                        
                     
                      2016 CLEA AWARD 
                     
                      University of Memphis Cecil C. Humphreys School of Law recent graduate Jeffrey T.
                        Slack (JD '16) was recently awarded the Clinical Legal Education Association (CLEA)
                        Outstanding Student Award. Jeff was a student in both the Neighborhood Preservation
                        Clinic during the Fall 2015 semester and the Elder Law Clinic in the Spring 2016 semester.
                      
                     
                      CLEA recognizes law students who have excelled in clinical fieldwork in law school
                        by providing high quality representation to clients, and who have engaged in exceptionally
                        thoughtful, self-reflective participation in an accompanying clinical seminar. The
                        award is based on excellence in case work and the quality and extent of the student's
                        contribution to the clinical community at his law school. Bill was lauded for, among
                        other things, his ability to set the pace and the standard for his colleagues in his
                        diligence, preparation, thoughtfulness, and motivation to be the best lawyer he can
                        be.
                      
                     
                      Jeff's instructors noted that he treated all of his clients with the utmost respect
                        and regard for their personal dignity and needs as vulnerable elders. He excelled
                        in the seminar component of his clinical courses and made the most of the educational
                        opportunities that were offered. Additionally, the faculty noted that Jeff distinguished
                        himself throughout his clinical course by providing to his clients the kind of personal,
                        caring service that exemplifies the "kinder, gentler" type of practice that elder
                        lawyers are known for.
                      
                     
                      In their nomination letter for Mr. Slack, the Memphis Law clinical faculty noted several
                        things about Jeff that make him the ideal candidate for this award. They states that
                        as a student attorney enrolled in the Elder Law Clinic, Jeff treated all of his clients
                        with the utmost respect and regard for their personal dignity and needs as vulnerable
                        elders. He has been vigilant concerning ethical issues and understands that his role
                        as an attorney is to advance the cause of social justice. He excelled in the seminar
                        component of the course, producing exceptional reflection papers and taking all assignments
                        seriously, making the most of the educational opportunities that were being offered.
                        Jeff was always willing to go the extra mile, whether it be by coming in to the clinic
                        office over the weekend to review a 200 page packet of discovery obtained from the
                        opposing party in a case defending a client against collection of credit card debt,
                        or whether it be by driving out to meet with a client in the community to file documents
                        with the county Register of Deeds, rather than requiring the client to make the trip
                        downtown. Jeff distinguished himself throughout by providing to his clients the kind
                        of personal, caring service that exemplifies the "kinder, gentler" type of practice
                        that elder lawyers are known for.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         About 
                         
                            
                               
                                Administration   Staff  
                                History  
                                Strategic Plan  
                                Virtual Tour  
                                ABA Disclosures  
                                Diversity  
                                      Diversity Home  
                                      TIP  
                                      Scholarships  
                                  
                               
                                Facilities  
                                      Facilities Overview  
                                      Virtual Tour  
                                      Floor Plans  
                                      Photo Gallery  
                                  
                               
                                News   Events  
                                      Law School News  
                                      On Legal Grounds Blog  
                                      ML-Memphis Law Magazine  
                                      Events Calendar  
                                      Communications Office  
                                  
                               
                                Directions  
                                Contact  
                                      Media Resource  
                                      General Contact Info  
                                      Faculty   Staff  
                                  
                               
                                Life in Memphis  
                                      Memphis-The City  
                                      Neighborhoods  
                                      Housing  
                                      The Arts Scene  
                                      Sports  
                                      The Food Scene  
                                      A Music Town  
                                      Directions and Maps  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply to Memphis Law  
                                
                            
                            
                                News   Events  
                                
                            
                            
                                Alumni   Support  
                                
                            
                            
                                ABA Required Disclosures  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


