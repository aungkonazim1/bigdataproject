Director, Procurement and Contract Services   
 
   
       
       Director, Procurement and Contract Services 
 
 
   
   
   
   
   
   
   

 
 
 
 
 
 
 
 
 

     

   
   
       
       
         
           Skip to Main Content 
         
         
           
             
   
     
       
         
           
         
       
       
         
          University of Memphis Career Opportunities  
       
     
   
 

           
           
             
   
     
       
         Toggle navigation 
          
          
          
       
     
     
       
    Home  
    Search Jobs  
      Create Account  
      Log In  
    Help  
 

       
             
               University Benefits 
             
             
               Equal Employment 
             
             
               Annual Security Report 
             
             
               Campus Map 
             
             
               About UofM 
             
       
     
   
 

             
               
                
                   Director, Procurement and Contract Services 
  
  
   
       Bookmark this Posting  |
     Print Preview  |
         Apply for this Job 
   


   
     
       Posting Details 
        
       
            

               Posting Information 
                
               
                     
                       Posting Number 
                         HMAE1416 
                     
                     
                       Advertised Title 
                         Director, Procurement and Contract Services 
                     
                     
                       Campus Location 
                         Main Campus (Memphis, TN) 
                     
                     
                       Position Number 
                         016797 
                     
                     
                       Category 
                         Staff (Hourly/Monthly) 
                     
                     
                       Department 
                         Procurement and Contract Services 
                     
                     
                       Minimum Position Qualifications 
                          Bachelor s degree in relevant area and ten (10) years of appropriate experience. Relevant experience may substitute for required education.  
                     
                     
                       Special Conditions 
                          The department is especially interested in candidates with the following: 
Knowledge of eprocurement technologies (SciQuest) 
Experience with Banner 
Strong communication  
Supervisory skills  
                     
                     
                       Work Schedule 
                          Monday – Friday 
8:00 a.m. – 4:30 p.m.  
                     
                     
                       Posting Date 
                         10/03/2017 
                     
                     
                       Closing Date 
                          
                     
                     
                       Open Until Screening Begins 
                         Yes 
                     
                     
                       Hiring Range 
                         $80,000 - $95,000 per year 
                     
                     
                       Full-Time/Part-Time 
                         Full-Time: Benefits Eligible 
                     
                     
                       Working Conditions 
                          While performing the duties of this job, the employee is regularly required to sit, use hands to handle or feel, and talk or hear. The employee frequently is required to walk. The employee is occasionally required to stand, reach with hands and arms, and stoop, kneel, or crouch. The employee must occasionally lift and/or move up to 10 pounds. Specific vision abilities required by this job include close vision.  
                     
                     
                       Additional Working Conditions 
                          
                     
                     
                       Special Instructions to Applicants 
                          All applications must be submitted online at workforum.memphis.edu. 
 Applicants must complete all applicable sections of the online application to be considered for a position. Please upload a cover letter, resume, and reference list after completing your application, if required by the system. Required work experience is based on full time hours. Part time work experience will be prorated as listed. 
 Candidates who are called for an interview must notify the Department of Human Resources in writing of any reasonable accommodation needed prior to the date of the interview.  
                     
                     
                       Is this posting for UofM employees only? 
                         No 
                     
                     
                       Positions Supervised 
                          Assistant Directors, Procurement Specialists, Jr. Procurement Specialists, Procurement Assistants, Procurement Compliance Officer and Office Coordinator.  
                     
                     
                       Knowledge, Skills, and Abilities 
                          
                     
                     
                       Additional Position Information 
                          
                     
                 
            
            

               Job Duties 
               The duties and responsibilities listed are intended to describe the general nature and level of work to be performed in this position and are not to be construed as an exhaustive list of the requirements of this job. 
                 
                     
                       Duties   Responsibilities 
                        Directs and leads the overall operations of Procurement and Contract Services; communicates with all levels of internal and external customers, stakeholders, and suppliers to ensure sound, ethical and legal business practices; keeps current with developments and changes in Federal, State, and University policies, regulations, and laws related to sourcing, contracting, negotiating, and strategic procurement practices.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Executes (signs and approves) expenditure and revenue contracts by the authority and delegation of the University President to contractually bind the University in legal agreements, in accordance with University Policy UM1571.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Develops, edits, and oversees expenditure and revenue contracts to ensure compliance with business and legal issues, including contract administration, internal controls, monitoring, tracking, and contract repository; confers with legal counsel and  AVP  Business Services as needed.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Oversees and administers the eprocurement systems (SciQuest   Ionware), works within the system of record (Banner), and the interface between the three systems; leads on-going maintenance, monitoring, quarterly upgrades and subsequent testing/implementatiion, fiscal year-end activities, and documentation of processes; oversees system administration issues with  BFSS  and  ITD .  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Oversees personnel; hires and trains employees; assigns work and evaluates performance; provides continuous staff development to ensure staff possesses proper training and current skill levels.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Provides strategic planning and policy development for areas of responsibility and ensures compliance with State, Federal and University policies and procedures; communicates changes in policies/procedures to internal and external customers.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Conducts research, analysis, administrative oversight, and submission of quarterly Contract, Diversity, Minority, Disabled, and Woman-Owned reports; monthly contract report to President; annual Senate Finance, Ways and Means Committee report, and other reports and metrics as required.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Oversees training and instruction to University personnel and suppliers related to contracts, legal issues, eprocurement systems, purchasing cards, and disposal of surplus.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Oversees the purchasing card program, including internal controls and process improvements.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Prepares and monitors expenditure and revenue budget for Procurement and Contract Services on an annual basis; analyzes and identifies future budgetary requirements required to support the University s mission.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Oversees the disposal of University surplus property program.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Other related duties as assigned.  
                     
                 
            
       
     
  
     Supplemental Questions 
       
            Required fields are indicated with an asterisk (*).  
 
       *  
  Describe your management experience.
     (Open Ended Question) 
 


     *  
  Describe your experience in Procurement and Contract services.
     (Open Ended Question) 
 



 

       
     Applicant Documents 
       
           Required Documents 
     
     Resume 
     Cover Letter 
     References List 
 

 Optional Documents 
     
     Unofficial Transcript 
 



       


 


                  
               
                
             
           
           
             
   
     University of Memphis — Human Resources 
165 Administration Building, Memphis, TN 38152 
901.678.3573  workforce@memphis.edu  
 Follow us on Twitter  
      
      ©University of Memphis. All Rights Reserved. The University of Memphis is an Equal Opportunity/Affirmative Action university.  
   
 

           
         
       
         
 
        
  

  


    
      
    
     
   
 
