Sharing with internal and external users | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Sharing with internal and external users | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    ePortfolio    »    Understanding the basic concepts in sharing   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Sharing with internal and external users                       
        
        
       
          
     
           Printer-friendly version       
	Use permissions to share ePortfolio items with other users. You can set up separate permissions options for each artifact, collection, reflection, presentation, and learning objective in your ePortfolio so you can pick and choose what content you share with others. You can assign permissions to individual users, sharing groups, groups of users based on course or department enrollment, or the general public through a URL (presentations only). You can choose whether others can see the item, see comments, see assessments, add comments, add assessments, and/or edit the item.
 

 
	Sharing items with internal users
 

 
	You can share items with other users in your organization by selecting individual users, all users in a course or group you are enrolled in, or a previously saved sharing group.
 

 
	For information on saving permission settings using a sharing group, see  Setting up sharing groups .
 

 
	Assign permissions for a user or group of users
 

  
		Click  ​  Share  from the context menu of the item you want to make available to others.
	 
	 
		Select when you want users to see the item in the Visibility section of the Sharing Setting dialog.
	 
	 
		Click  Add Users and Groups .
	 
	 
		Browse for the users or sharing groups you want to add. Use the  Search for  field to narrow your browsing results. Users with cascading roles must perform a search to display results.
	 
	 
		Click on users or sharing groups in the browse listing to add them to the Selected Users list. Click the  ​  Remove  icon beside users or sharing groups you want to remove from the Selected Users list.
	 
	 
		Select the permissions you want the selected users to have and click  Add . View more detail about  permission options .
		 
			 Note  You can adjust permissions for individual users by clicking the  ​  Edit Permissions  icon beside their name in the Sharing List grid. You can adjust permissions for sharing groups you've created by clicking the  Edit Sharing Group  link beside their name in the Sharing List grid.
		 
	 
	 
		If you want to send an invite to specific users in the Sharing List, select the check boxes beside their names and click  Send Invite .
	 
	 
		Click  Close .
	 
  
	Sharing presentations with external users
 

 
	You can share ePortfolio presentations with people who are not part of your organization such as friends, parents, or potential employers.
 

 
	You can share presentations with external users in two ways:
 

  
		You can make the presentation publicly available to anyone through a URL. When you share a presentation this way people can only view the presentation. They cannot add comments to, add assessments to, or edit the presentation.
	 
	 
		You can send a personal invite to view your presentation via an email and the recipient can follow the attached link to view the presentation. The recipient must set up a username and password, and then they can see and add comments or assessments based on the permissions you gave them. External users cannot edit presentations.
	 
  
	Make a presentation publicly available through a URL
 

 
	 Note  If you make a presentation public via URL, the system includes social media icons (Twitter, Google+, and Facebook) at the top of your presentation to enable further sharing.
 

  
		Click  ​  Share  from the context menu of the presentation you want to make available by URL.
	 
	 
		Select  Anyone with the URL below can access this item  from the presentation's Sharing Settings and click  Close . Share the provided URL with others.
	 
  
	Make a presentation externally available to specific people and assign them permissions
 

  
		Click  ​  Share  from the context menu of the presentation you want to make available to external users.
	 
	 
		Click  Add Users and Groups  from a presentation's Sharing Settings.
	 
	 
		Click  Click to share the presentation with an external user .
	 
	 
		Enter the  External Email Address  of the external user you want to send an invite to and click  Add .
	 
	 
		Select the permissions you want the external users to have and click  Add . The system automatically notifies external users of their granted permissions. View more detail about  permission options .
	 
	 
		Click  Close .
		 
			 Note  The recipient must follow the link in the invite and set up a username and password to view the presentation. The invite expires after three days (or another amount of time set by your organization).
		 
	 
  
	  Sending invites
 

 
	When you send an invite to internal users you are sharing an item with, you have the opportunity to leave them a message about why you are sharing the item and what you’d like them to do with it. Depending on how the user’s preferences are set up, they will receive the message in their email, the Invites area of their dashboard, or an RSS Reader. Recent invites also appear in the Message alerts on the minibar.
 

 
	Send an invite
 

  
		In the Sharing Settings dialog for an item, send invites to specific users in the Sharing List by selecting the check boxes beside their names, then click  Send Invite .
	 
	 
		Complete the invite and click  Send .
	 
  
	 Important  If you send an invite to a sharing group that includes all users at your organization or all users in a particular course or department, all of the users in the group will receive the invite. This may be bothersome to users who do not know you.
 

 
	See also
 

  
		 Setting up sharing groups 
	 
	 
		 Sharing permission options 
	 
      Audience:    Learner      

    
           

                   ‹ Viewing items shared with you 
        
                   up 
        
                   Creating quicklinks to ePortfolio items › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        ePortfolio  
  
      ePortfolio basics    Using artifacts    Creating and editing presentations    Creating collections    Managing comments and assessments in ePortfolio    Understanding the basic concepts in sharing    Viewing items shared with you    Sharing with internal and external users    Creating quicklinks to ePortfolio items    Sharing permission options    Understanding cascading permissions    Setting up sharing groups    Removing sharing permissions    Ignoring and restoring items from users      Importing and exporting items    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
