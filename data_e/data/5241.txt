Adding an existing course object (activity) to Content | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Adding an existing course object (activity) to Content | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Content    »    Managing and updating course content   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Adding an existing course object (activity) to Content                       
        
        
       
          
     
           Printer-friendly version        
		Click on the module you want to add a course object to from the Table of Contents.
	 
	 
		Select one of the following from the  Add Existing Activities  button:
		  
				   Chat 
			 
			 
				   Checklist 
			 
			 
				   Discussions 
			 
			 
				   Dropbox 
			 
			 
				   ePortfolio item 
			 
			 
				   External Learning Tools 
			 
			 
				   Forms 
			 
			 
				   LiveRoom 
			 
			 
				   Online Rooms  (only available at the Organization level)
			 
			 
				  Quizzes 
			 
			 
				   Self Assessments 
			 
			 
				   Surveys 
			 
		  
	 
		Click on the existing activity you want to insert from the drop-down list, or if you create a new activity, click  Create and Insert .
	 
      Audience:    Instructor      

    
           

                   ‹ Associating topics with learning objectives in Content 
        
                   up 
        
                   Evaluating Dropbox submissions from Content › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Content  
  
      Content basics    Creating course content    Managing and updating course content    Content display settings    Changing a module or topic status to Draft or Published in Content    Notifying students about updated course content    Editing a module or topic s title in Content    Bulk editing modules and topics in Content    Moving or reordering a module or topic in Content    Deleting modules or topics in Content    Adding or editing availability and due dates in Content    Adding or editing release conditions in Content    Adding a Learning Object to a module    Associating topics with learning objectives in Content    Adding an existing course object (activity) to Content    Evaluating Dropbox submissions from Content      Using SCORM in Content    Tracking content completion and participation    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
