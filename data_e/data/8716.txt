Editing a Draft or In Review competency&#039;s details | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Editing a Draft or In Review competency s details | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Competencies    »    Creating and managing competency structure elements   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Editing a Draft or In Review competency s details                       
        
        
       
          
     
           Printer-friendly version      

  
		On the Competency Home page, click the competency you want to edit.
	 
	 
		Make your changes.
	 
	 
		Click  Save .
	 
  
	 Note  It is possible to edit Archived competencies by setting their status to Draft, but you can copy these competencies and modify the copies instead of making changes to already archived elements.
 


     Audience:    Instructor      

    
           

                   ‹ Creating competencies 
        
                   up 
        
                   Hiding or showing an Approved competency › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Competencies  
  
      Competency structure basics    Automating competency structure evaluation    Creating and managing competency structure elements    Understanding competency status settings    Creating competencies    Editing a Draft or In Review competency s details    Hiding or showing an Approved competency    Modifying an Approved competency    Copying a competency    Archiving a competency    Deleting competencies    Creating learning objectives    Editing a learning objective s details    Copying a learning objective    Deleting learning objectives    Creating activities    Adding associations between competency structure elements    Removing associations between competency structure elements    Sharing competency structures    Tracking competency versions    Viewing competency structure results    Overriding competency structure results    Managing independent learning objectives and independent activities      Evaluating competency structure activities    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
