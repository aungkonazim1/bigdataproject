The Profile of Dipankar Dasgupta   
 
 The Profile of Dipankar Dasgupta 
 
 




 
 



 


 

 
 Dr. Dipankar Dasgupta 
 

 
 * Research Sites * 
 
 
  Artificial Immune Systems (AIS)  
  Center for Information Assurance (CfIA)  
 
 
            


 
 
 
Dr. Dipankar Dasgupta 
333 Dunn Hall 
Memphis, TN 38152-3240 
phone: (901) 678-4147 
fax: (901) 678-1506 
 dasgupta@memphis.edu 
 
 
 
  Dr. Pat E. Burlison  Professor of   Computer Science  
 Director,  Center for Information Assurance  
 Director,  Intelligent Security Systems Research Laboratory  
 


       
  Elevated to IEEE Fellow(Batch of 2015) 
   Distinguished ACM Speaker  
  Recipient of 2012 Willard R. Sparks Eminent Faculty Award.  
 
 
  Advisory Board Member of  MIT in Cyber Security   
  Editorial Board of journals  


 

 Evolutionary Intelligence, Springer-Verlag 

 Evolutionary Optimization, Polish Academy of Science.  

 Recent Patents on Computer Science, online journal Bentham Science Publishers Ltd.  

 Swarm and Evolutionary Computing - Elsevier Press 
 
 
  Announcement:  
 
  Research Assistant Professor Position  (in Cyber Security) Available
  
 
 

 
 * Principal Investigator * 
 
 
  Act Online  
 
 
      


 

 
 
  Home  
  Events 
	 
	  News  
	  Professional Activities  
	  Invited Talks  
	 
 
  Research 
	 
	  Interests 
		 
		  Artificial Immune Systems  
		  Evolutionary Computation  
		  Immunological Computation  
		  Intrusion Detection  
		  Fault Detection  
		 
	 
	  Projects 
		 		
		  Current Projects  
                  Previous Projects  
		 
	 
	
	  Grants  
	  Publications  
 
	 
 
  Teaching 
	 
	  Courses  
	 
 

  Staff/Students  
  Books 
         
          AUA Book  
          IC Book  
          AIS Book  
          EC Book  
         
 
  Recognitions 
         
          Awards  
          Videos  
	 
 
  About  
  
 

 
 
Dr. Dasgupta will Organize IEEE Symposium on  Computational Intelligence in Cyber Security (CICS 2017)  at Hawaii, USA from November 27-December 1, 2017.
Program Committee Member of the 1st IEEE International Workshop on  Cyber Resiliency Economics (CRE 2016)  , Vienna, Austria, August 1-3, 2016.
Prof. Dasgupta will give an invited talk at the  Computer Science Department, University of Tennessee, Knoxville, TN, April 7, 2016
   
Prof. Dasgupta will present a research paper at 11th Annual Cyber and Information Security Research  (CISR)  Conference will be held at the conference center at Oak Ridge National Laboratory, Oak Ridge, TN, April 4 - 6, 2016.
   
Prof. Dasgupta will give invited talk at  Regional Symposium  "Graduate Education and Research in Information Security",'GERIS'16, on  March 8, 2016, at Binghamton University,Binghamton, New York.
   
Announcement for the available position in  Research Assitant Professor  (in Cyber Security)
   
Prof. Dasgupta was interviewed by a local TV Channel (FOX 13) and telecast on Feb. 19, 2016.  Click here for Video. 
   
Organized  "Cybersecurity Certificate Course"  foundational program at FedEx Institute of Technology,UofM, February 1-5, 2016.
   
Prof. Dasgupta gave an invited talk on  5th International Conference on Fuzzy and Neural Computing , FANCCO-2015, December 16-19, 2015.
   
Cluster to Advance Cyber Security & Testing (CAST)  hosted  Cybersecurity Lightning Talks  at the FedEx Institute of Technology, afternoon of December 3, 2015
   
CfIA Receives Cyber Security Training Grant from FEMA
   
UofM's CfIA Will Develop Course for  Mobile Device Security and Privacy Issues 
   
Prof. Dasgupta gave an invited talk on  Adaptive Multi-Factor Authentication  at the  Department of Electrical Engineering and Computer Science and CASE Center, Syracuse University, Syracuse, NY 13224-5040 November 18, 2015
   
Organize a Symposium on Computational Intelligence in Cyber Security (CICS) at IEEE Symposium Series on Computational Intelligence ( SSCI, ), December 7-10, 2015 at Cap Town, South Africa
   
Gave keynote speech at St. Louis at  Cyber Security workshop (STL-CyberCon) , University of Missouri-St. Louis, November 20, 2015
   
Prof. Dasgupta attended the  NIST-NICE  conference at San Diego from November 1-4, 2015
   
Prof. Dasgupta gave an invited talk at 9th  International Research Workshop  on Advances and Innovations in Systems Testing at FedEx Institute of Technology, the University of Memphis, October 20, 2015
   
Our   Cyber Security Team  got a   second position   on Cyber Defense Competition  @CANSec 2015 , held on 24th October at University of Arkansas at Little Rock
  
 

 

  Grants 
  Received a $473,218 (Multi-University grant of $3.0M) grant from the FEMA for Developing " Cyber Security Competitive Training" , January  15,2016.
  
 Received a $240,907  grant from the NSA for   "An Adaptive Continuous Multi-Factor Authentication Framework" ,August 15 ,2015
  
 Received a $364,864 (with Jackson State University)with total funding of $850,000)grant from the NSF for   "Puzzle-Based Cyber security Learning to Enhance Defensive Skills of Front-Line Technicians" , September 1, 2014.
  
 Received a $325K (Multi-University grant of $2.3M) grant from the FEMA/DHS  for Developing "A Cyber Security Competitive Training”, October 1st,2014.
  
 Received a $207K (Multi-University grant of $800K) grant from the FEMA for Developing   "ACT Online Courseware Update" , October 1st,2013.
  
 Prof. Dipankar Dasgupta share with NCPC a  $2.3 million grant  from the Federal Emergency Management Agency (FEMA), October 1, 2014.
  
 Received a  $365k grant  from the NSF for "Collaborative Project: Puzzle-Based Cybersecurity Learning to Enhance Defensive Skills of Front-Line Technicians", September 1, 2014.
  
  $800,000 Grant  from FEMA Establishes National Cybersecurity Preparedness Consortium, November 26, 2013.
  
 A project  "Negative Authentication System"  funded by Intelligence Advanced Research Projects Activity ( IARPA ) in collaboration with Massachusetts Institute of Technology (MIT), Period( August 2012- April 2014)
  
 Received a research grant from the Office of Naval Research (ONR) to conduct research on   "Game Theoretic Approaches to Protect Cyberspace" , Started April, 2009 to December, 2009 with Dr. Sajjan Shiva as the PI and Dr. Dipankar Dasgupta and Dr. Qishi Wu as co-PIs.
  
 NAVY (NPRST) Grant for researh on  Task-Based Sailor Assignment problem (TSAP) (GenoSAP-III)  , Started September 12, 2007 to August, 2009.
  
 Received $4M DHS Grant for developing Adaptive Cyber-Security Training (ACT), in collaboration with Vanderbilt University & SPARTA, October, 2006 to March, 2010.
  
 DoD Scholarship program Capacity Building, funded for One year started September, 2007.
  
 Received (jointly with Prof. J. Simon)Excellence in Learning (IEL) Grant of $12,000 from the U of M in September 2006 for developing Multidisciplinary Information Security Training Lab.
  
 NAVY (NPRST) Grant for research on  Genetic Algorithm Approach to the Sailor Assignment Problem,  Started October 2004 - December 2005.
  
 DARPA Grant for research on  Immunity-Based Intrusion Detection Systems/Intelligent M & R Security Console,  April 2000 - March 2005.
  
 NSF SGER grant on  Immunity-Based Computational Techniques,  March 2001 - February 2002.
  
  NSF Research Instrumentation Grant (Jointly) , June 1999 - May 2001.
  
 ONR Grant for research on  Anomaly Detection using a Technique Inspired by the Immune System , May 1999 - August 2001.
  
 Academic Enrichment Funds for Importance of Computer and Internet Security Education, The University of Memphis, 2001-2002.
  
 Technology Access Funding (TAF) grant, The University of Memphis, May 2000- April 2001.
  
 Technology Access Funding (TAF) grant, The University of Memphis, 1998.
  
 
 

 

 
 






 
 
