Overwriting learning objects and files | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Overwriting learning objects and files | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Repository    »    Publishing to Learning Repository   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Overwriting learning objects and files                       
        
        
       
        
     
              
	The process of overwriting replaces, but does not delete, an existing learning object or file in a repository. In effect, overwriting creates a new version of the learning object or file.
 

 
	The new object must have the same structure as the object it replaces. You can overwrite one module with another module, but you cannot overwrite a topic with a module. Desire2Learn recommends that you only overwrite a file or a topic with a file of the same type (e.g., image, shockwave, etc.).
 

 
	 Note    If you rearrange topics ABC within a module to resemble ACB, topic C will inherit topic B's metadata when you overwrite the original with the rearranged version of the learning object. You may want to select  Overwrite the existing object's metadata  and enter a new set of metadata if you have rearranged topics in your learning object.
 

 
	The new object appears in all searches. The overwritten object no longer appears in searches. All existing dynamic links point to the new object. However, locked links continue to point to the old object.
 

 
	 Important   Do not overwrite a learning object or file with a link to itself, as this may result in a circular reference.
 

 
	  
     Audience:     Instructor       

    
           

                   ‹ Publishing learning objects with thumbnails 
        
                   up 
        
                   Managing learning objects › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Learning Repository  
  
      Main Learning Repository concepts    Searching for and using learning resources    Publishing to Learning Repository    Publishing from Content    Publishing from course files    Publishing from your PC    Publishing quizzes, quiz sections, and questions    Checking a learning object s background publishing status    Formatting metadata for publishing packages    Publishing learning objects with thumbnails    Overwriting learning objects and files      Managing learning objects    
                  
           
         

       
     

    
    
    
   
 
   
 
