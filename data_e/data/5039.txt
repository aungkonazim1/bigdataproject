Director, Planning and Assessment for Business &amp; Finance   
 
   
       
       Director, Planning and Assessment for Business   Finance 
 
 
   
   
   
   
   
   
   

 
 
 
 
 
 
 
 
 

     

   
   
       
       
         
           Skip to Main Content 
         
         
           
             
   
     
       
         
           
         
       
       
         
          University of Memphis Career Opportunities  
       
     
   
 

           
           
             
   
     
       
         Toggle navigation 
          
          
          
       
     
     
       
    Home  
    Search Jobs  
      Create Account  
      Log In  
    Help  
 

       
             
               University Benefits 
             
             
               Equal Employment 
             
             
               Annual Security Report 
             
             
               Campus Map 
             
             
               About UofM 
             
       
     
   
 

             
               
                
                   Director, Planning and Assessment for Business & Finance 
  
  
   
       Bookmark this Posting  |
     Print Preview  |
         Apply for this Job 
   


   
     
       Posting Details 
        
       
            

               Posting Information 
                
               
                     
                       Posting Number 
                         HMAE1410 
                     
                     
                       Advertised Title 
                         Director, Planning and Assessment for Business & Finance 
                     
                     
                       Campus Location 
                         Main Campus (Memphis, TN) 
                     
                     
                       Position Number 
                         020395 
                     
                     
                       Category 
                         Staff (Hourly/Monthly) 
                     
                     
                       Department 
                         VP Business and Finance 
                     
                     
                       Minimum Position Qualifications 
                          Bachelor s degree in a relevant area and ten(10) years of appropriate experience, as well as a proven record of success in senior leadership positions.  
                     
                     
                       Special Conditions 
                          
                     
                     
                       Work Schedule 
                          Monday   Friday 8:00 a.m.   4:30 p.m. May need to work/travel beyond normal work schedule.  
                     
                     
                       Posting Date 
                         09/28/2017 
                     
                     
                       Closing Date 
                          
                     
                     
                       Open Until Screening Begins 
                         Yes 
                     
                     
                       Hiring Range 
                         Commensurate with experience 
                     
                     
                       Full-Time/Part-Time 
                         Full-Time: Benefits Eligible 
                     
                     
                       Working Conditions 
                          While performing the duties of this job, the employee is regularly required to sit, use hands to handle or feel, and talk or hear. The employee frequently is required to walk. The employee is occasionally required to stand, reach with hands and arms, and stoop, kneel, or crouch. The employee must occasionally lift and/or move up to 10 pounds. Specific vision abilities required by this job include close vision.  
                     
                     
                       Additional Working Conditions 
                          
                     
                     
                       Special Instructions to Applicants 
                          All applications must be submitted online at workforum.memphis.edu. 
 Applicants must complete all applicable sections of the online application to be considered for a position. Please upload a cover letter, resume, and reference list after completing your application, if required by the system. Required work experience is based on full time hours. Part time work experience will be prorated as listed. 
 Candidates who are called for an interview must notify the Department of Human Resources in writing of any reasonable accommodation needed prior to the date of the interview.  
                     
                     
                       Is this posting for UofM employees only? 
                         No 
                     
                     
                       Positions Supervised 
                          
                     
                     
                       Knowledge, Skills, and Abilities 
                          
                     
                     
                       Additional Position Information 
                          
                     
                 
            
            

               Job Duties 
               The duties and responsibilities listed are intended to describe the general nature and level of work to be performed in this position and are not to be construed as an exhaustive list of the requirements of this job. 
                 
                     
                       Duties   Responsibilities 
                        Leads the development and management of the Division s strategic plan and continuous improvement process. Works collaboratively with the Business   Finance AVP s to translate university directions into concrete goals and accomplishments. Coordinates and leads the development of goals and accomplishment efforts for the Division, performs routine assessments to determine their progress, prepares and submits required documents on behalf of the Vice President.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Initiates projects and programs and coordinates their implementation, in collaboration with appropriate leaders; works collaboratively with the University community to ensure integration with University goals and strategic initiatives.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Leads the preparation and oversight of the Division s budget. Prepares budget presentation and presents on behalf of the Vice President, as required.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Plans and coordinates Business and Finance materials for the governing Board and Committee meetings. Prepares data and materials, presentations and speaking points for the Vice President for all governing Board and committee meetings.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Represents the VP for Business and Finance on University committees where issues and activities impact the University s operations, as needed. Coordinates with key leads on special projects to facilitate the flow of information and review process.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Provides analysis, advice and recommendations to the VP for Business   Finance regarding the formulation of strategy and project implementation, as appropriate, and assists with solutions to problems by recommending approaches to be taken and methodologies to be used.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Serves as the  SACS  representative for the Division and assists in the preparation and submission of required documents.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Prepares and disseminates internal and external communications that create an environment of transparency and trust among University students and employees.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Oversees special projects and serves as an advocate for University initiatives.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Provides guidance and oversight to the University s Process Improvement initiatives.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Interfaces with the Chief Operations Officer and the Office of Board Services on governing Board matters related to Business and Finance.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Provides overall leadership on divisional best practices and customer service initiatives. Performs other duties as assigned.  
                     
                 
            
       
     
  
     Supplemental Questions 
       
            Required fields are indicated with an asterisk (*).  
 
       *  
  Describe your experience in linking a strategic plan to an operating budget request.
     (Open Ended Question) 
 


     *  
  Describe your experience in conducting assessments on the effectiveness of a functional area or unit.
     (Open Ended Question) 
 


     *  
  Describe any experiences you have had in providing data to or for a governing board.
     (Open Ended Question) 
 



 

       
     Applicant Documents 
       
           Required Documents 
     
     Resume 
     Cover Letter 
     References List 
 

 Optional Documents 
     
     Unofficial Transcript 
 



       


 


                  
               
                
             
           
           
             
   
     University of Memphis — Human Resources 
165 Administration Building, Memphis, TN 38152 
901.678.3573  workforce@memphis.edu  
 Follow us on Twitter  
      
      ©University of Memphis. All Rights Reserved. The University of Memphis is an Equal Opportunity/Affirmative Action university.  
   
 

           
         
       
         
 
        
  

  


    
      
    
     
   
 
