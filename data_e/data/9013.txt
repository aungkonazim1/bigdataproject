Viewing reviews of a learning object or file | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Viewing reviews of a learning object or file | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Repository    »    Searching for and using learning resources   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Viewing reviews of a learning object or file                       
        
        
       
        
     
              
	Users of learning objects can review, rate, and comment on them. The review also includes the reviewer’s name and the review date.
 

 
	On this page you can view the list of reviews for the learning object or file. The Overall Rating shows the average of all the ratings that were given to this learning object or file.
 

 
	View the learning object's reviews
 

  
		From any LOR Results page, click     Open  from the learning object's context menu.
	 
	 
		Click    Reviews  from the More Actions button.
	 
      Audience:     Instructor       

    
           

                   ‹ Adding reviews to a learning object or file 
        
                   up 
        
                   Retrieving learning objects with navigation › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Learning Repository  
  
      Main Learning Repository concepts    Searching for and using learning resources    Searching Learning Repository    Browsing Learning Repository s content    Working with Learning Repository search results    Viewing files in a learning object    Adding reviews to a learning object or file    Viewing reviews of a learning object or file    Retrieving learning objects with navigation    Retrieving learning objects without navigation    Retrieving a learning object as a new topic    Importing a learning object into a quiz or a question library    Searching for and retrieving a collection    Saving a learning object to your PC    Using RSS feeds in Learning Repository      Publishing to Learning Repository    Managing learning objects    
                  
           
         

       
     

    
    
    
   
 
   
 
