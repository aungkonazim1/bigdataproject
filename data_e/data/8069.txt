RODP Online Courses - University College - University of Memphis    










 
 
 
     



 
    
    
    RODP Online Courses - 
      	University College
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			University College
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Undergraduate  
                          Graduate  
                          Resources  
                          Online  
                          Faculty and Staff  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	University College
                              	  
                          
                              	Online
                              	  
                         RODP Online Courses 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      TN eCampus Online Courses 
                     
                      TN eCampus is a collaborative partnership that allows institutions to share resources
                        and personnel needed to deliver quality online instruction. When you enroll in TN
                        eCampus courses, you will remain a University of Memphis student and your courses
                        will appear on your transcript as University of Memphis courses. Your class may be
                        instructed by a professor from one of the other partner institutions and you may have
                        classmates enrolled at other partner institutions.
                      
                     
                      Several undergraduate and graduate degree programs are offered through TN eCampus.
                        These fully accredited programs are designed with asynchronous classes so you can
                        complete course work based on deadlines, not assigned class meetings. You may choose
                        to complete your degree entirely online or, with your academic advisor's approval,
                        you may complete some of your courses on-campus.
                      
                     
                      
                     
                      
                        
                           How do I register for a TN eCampus online course?   
                        
                           Do I need a permit for a TN eCampus online course?   
                        
                           I registered for a TN eCampus online course. Where do I get textbooks?   
                        
                           Where and when do I access the TN eCampus online course site?   
                        
                           My TN eCampus online course exams require a proctor.  How do I take a proctored exam?   
                        
                           I have an issue with a TN eCampus online course. What should I do?   
                        
                           I would like to dispute/appeal my TN eCampus online course final grade. What should
                                 I do?   
                        
                      
                     
                      
                     
                             How do I register for a TN eCampus online course?   
                     
                      The University of Memphis offers over 500 online courses which can be easily identified
                        by the section number and instructional method in the course schedule. Registration
                        for all online courses is completed through the same process as any other course.
                        Access your  myMemphis portal  and use the student page to complete  registration functions .
                      
                     
                      TN eCampus courses can be identified by sections R01, R25, and R50. Ex. BIOL 2010-R01 
                      
                     
                            Do I need a permit for a TN eCampus online course?   
                     
                       First, attempt to register for your courses.   You should o  nly submit a permit request if you have already tried to register for a TN eCampus
                              online course and received an error message.   
                     
                       Our office issues permits for TN eCampus online courses   except for   graduate level Nursing (NURS), and Education (ASTL, TEAE, or TELC) courses. For contact
                           information to request permits for those courses, please go to the  registrar's website.    
                     
                          - Request a permit for a TN eCampus course.   
                     
                          - Request a permit for COMM 7110 or a PRST course.   
                     
                              I registered for a TN eCampus online course. Where do I get textbooks?    
                     
                       TN eCampus books can only be purchased through the  TN eCampus Virtual Bookstore.  At the website, place your cursor over the "Books" link and c lick on the Textbooks   Course Materials link. Then use the   the various drop down menus   to find your courses and view the required and suggested textbooks. Please be sure
                              to read the return policy and other important notices.   The University of Memphis bookstore does not sell TN eCampus books.       
                     
                           - Order TN eCampus Books   
                     
                              Where and when do I access the TN eCampus online course site?    
                     
                      
                        
                         
                           
                             New students should read and complete all sections of the TN eCampus  Orientation for Online Students.  Then try our  TN eCampus Online Readiness Course  by entering Username: guest and Password: Guest2017.   
                            
                           
                         
                        
                      
                     
                      
                        
                         
                           
                             You will not be able to access courses until the first day of the semester, and be
                                 aware some delays may occur.    
                           
                         
                        
                         
                           
                             Students adding courses during the late registration period will not be able to access
                                 their courses for an additional 24-48 hours after their registration is complete.  
                           
                         
                        
                         
                           
                              To access TN eCampus courses (course sections R01, R25, and R50) go to  gotoclass.tnecampus.org   and f  ollow the instructions to form your user name and password.     You will not use the same username and password that you use at the University of
                                    Memphis!    
                           
                         
                        
                         
                           
                             If you have technical difficulties accessing your course (logging in, timing out,
                                 using your course tools) please call the TN eCampus Support Services at 1-866-895-8429
                                 or go to their website at  https://tnecampus.helpspot.com   (Note: You must be a a registered TN eCampus student to utilize this service.)   
                           
                         
                        
                          You may learn more about our course management software by viewing our video tutorials
                              and other resources linked below. You will need to use your UofM user name and password
                              to access the videos.  
                        
                         Student Training Resources: 1. A brief video introducing students to eCourseware can be found at  Student Introduction to eCourseware.  (Mac users may need to open this link in Safari.) 2. A more detailed video, explaining how to use the basic tools of eCourseware, can
                           be found at  eCourseware Tools to Know . (Mac users may need to open this link in Safari.)
                         
                        
                      
                     
                            My TN eCampus online course exams require a proctor.  How do I take a proctored exam?   
                     
                       Some TN eCampus courses require your midterm and final exam be given to you by a proctor.
                           (someone appointed to supervise students during exams) You are responsible for finding an
                           appropriate proctor, setting an exam appointment, and following all policies at your
                           chosen test location.  Only your instructor can approve your choice of proctor.  
                     
                       If your  TN eCampus course indicates that you will have a proctored midterm and/or final exam,
                                 please read our entire proctor policy.      
                     
                              I have an issue with a TN eCampus online course. What should I do?    
                     
                       You should first attempt to resolve course issues by communicating with the instructor. 
                           If you are not able to reach a resolution, you can contact us for assistance.  Please
                            send an email  that includes your full name, U-Number, the course ID (ex. BIOL 1010-R50) the instructors
                           name, and details of the problem you are having. Include information about your attempts
                           to resolve the situation yourself. We will contact you as soon as we have information
                           to share with you.  
                     
                            I would like to dispute/appeal my final grade in my TN eCampus online course. What
                              should I do?    
                     
                       If you wish to dispute a TN eCampus course final grade, first discuss your concerns
                           with the instructor.  If you have unsuccessfully tried to contact the instructor,
                           please  send an email  that includes your full name, U-Number, the course ID (ex. BIOL 1010-R50) the instructors
                           name, and your reason for contacting the instructor.    
                     
                       If you are not able to reach a resolution, and wish to pursue a grade appeal, please
                           review grade appeal procedures for  undergraduate students  or for  graduate students .    Appeals must be based on evidence that you performed at a level sufficient to warrant
                                 a different grade, not circumstances that would grant a different grade.    If you wish to file an appeal and your petition meets the requirements stated in
                           the UofM appeal policies linked above; please  send an email  that includes your full name, U-Number, the course ID (ex. BIOL 1010-R50) the instructors
                           name, and your formal grade appeal statement. We will contact you as soon as we have
                           information to share with you.  
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Find the program that's right for you  
                               Explore our degree programs 
                            
                            
                                Learn more about University College  
                               Request more program information 
                            
                            
                                Advising Appointments  
                               Academic Advising is available in-person, by phone or online 
                            
                            
                                Contact Us  
                               The University College team is here to help 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


