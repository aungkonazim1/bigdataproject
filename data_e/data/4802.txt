Library - LibGuides at University of Memphis Law Library  
 
 
	 
		
		 
		 
		 
				
         Library - LibGuides at University of Memphis Law Library 
		
		
        
        
        
		
		
			 
	
	 
		
		
	
		   
		  Library 
			 
				 
					 
						 
							 
								 
									  Guide Information  
								 
								 
									 Last Updated: 
									 Nov 29, 2017 
								 
								 
									 Guide URL: 
									  http://libguides.law.memphis.edu/libinfo  
								   Description:  general information and policies    Tags:   library ,  library privileges ,  library resources ,  library services     RSS:   Subscribe to Updates via RSS     
						 
							  Guide Index   Hours   Contact Us   Check-out Privileges   Floor Maps   
					 
				 
			   Floor Maps 
					 
						  Three dimensions   Basement  http://www.memphis.edu/law/images/level0-3d.jpg  First Floor  http://www.memphis.edu/law/images/level1-3d.jpg  Second Floor  http://www.memphis.edu/law/images/level2-3d.jpg  Third Floor  http://www.memphis.edu/law/images/level3-3d.jpg  Fourth Floor  http://www.memphis.edu/law/images/level4-3d.jpg   
						  
					 
					 
						  Two dimensions   Basement  http://libguides.law.memphis.edu/content.php?pid=672901&sid=5573188&preview=aea39eeb16812a9620f871c5f86f0617  First Floor  http://www.memphis.edu/law/images/level1-2d.jpg  Second Floor  http://www.memphis.edu/law/images/level2-2d.jpg  Third Floor  http://www.memphis.edu/law/images/level3-2d.jpg  Fourth Floor  http://www.memphis.edu/law/images/level4-2d.jpg   
						  
					   Back to Top   
 
	 
    	Powered by  Springshare ; All rights reserved. 
    	 
        	 Report a tech support issue .      	 
  	 
	
	 

  
	  		
	 
 