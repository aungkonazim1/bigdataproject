Four FCBE faculty recognized by the National Society of Leadership &amp; Success - FCBE - University of Memphis    










 
 
 
     



 
    
    
    Four FCBE faculty recognized by the National Society of Leadership   Success - 
      	FCBE
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College of Business   Economics
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          Students  
                          Departments  
                          Faculty   Staff  
                          Research  
                          News  
                      
                      
                         
                            News   
                            
                               
                                  
                                   News  
                                         Current News  
                                         News Archive  
                                     
                                  
                                   Events  
                                   FCBE Media Room  
                                         College Publications  
                                         Fogelman Focus  
                                         Fogelman Flash  
                                         Memphis Memo  
                                         Fogelman Features  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	FCBE
                              	  
                          
                              	News
                              	  
                         Four FCBE faculty recognized by the National Society of Leadership   Success 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Four FCBE faculty recognized by the National Society of Leadership   Success 
                     
                      For release:  March 8, 2017  The National Society of Leadership and Success chapter, at the University of Memphis,
                        recently recognized four Fogelman College of Business   Economics faculty members.
                        Dr. William T. Smith, professor and chair of the Department of Economics, and Dr.
                        Irvin Tankersley, associate professor of Accountancy, were recognized for their Excellence
                        in Teaching. Dr. Kathy Tuberville, director of the Avron B. Fogelman Professional
                        Development Center and instructor in the Department of Management, was recognized
                        for her Excellence in Service to Students. The Honorary Faculty Membership award was
                        presented to Dr. Marla Stafford, Great Oaks Foundation Professor of Marketing and
                        chair of the Department of Marketing   Supply Chain Management. The Honorary Membership
                        award is presented to a faculty, staff, or administrator who embodies the mission
                        of The National Society of Leadership and Success, demonstrates leadership qualities
                        in a personal and professional capacity, and has made an impact on campus and on students'
                        everyday lives.
                      
                     
                      The Society is the nation's largest leadership honor society. Students are selected
                        by their college for membership based on either academic standing or leadership potential.
                        Candidacy is a nationally recognized achievement of honorable distinction. There are
                        currently 545 chapters, with 725,662 members nationwide. The Society has a primary
                        objective of developing members as leaders who can make a positive impact on their
                        communities through volunteerism, personal growth, and strong leadership. This is
                        accomplished by providing an active support network for members that includes access
                        to live and recorded interviews with guest speakers of the society, some of which
                        have included Carly Fiorina, Jesse Eisenberg, and Leigh Anne Tuohy. They also provide
                        over $250,000 in various scholarship funds, Study Abroad Opportunities, and an interactive
                        support network to help members achieve their goals and grow and develop as leaders.
                      
                     
                        
                     
                      (From left to right) Dr. Bill Smith, Dr. Irvin Tankersley, Dr. Kathy Tuberville, and
                        Dr. Marla Royne Stafford National Society of Leadership and Success faculty recognition recipients.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         News 
                         
                            
                               
                                News  
                                      Current News  
                                      News Archive  
                                  
                               
                                Events  
                                FCBE Media Room  
                                      College Publications  
                                      Fogelman Focus  
                                      Fogelman Flash  
                                      Memphis Memo  
                                      Fogelman Features  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply to our Undergraduate Programs    
                               Enroll at the university and engage yourself with our six different departments, student
                                 enrichment, and more. 
                               
                            
                            
                                Apply to our Masters Programs    
                               Earn your degree with one of our five MBA tracks or with one of our four specialized
                                 Masters programs!
                               
                            
                            
                                Apply to our Doctoral Programs    
                               Interested in a Ph.D.? Start the journey towards your Ph.D. here! 
                            
                            
                                Contact Us    
                               Have questions? Call or email today!  
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


