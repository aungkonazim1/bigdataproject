1L Survival Guide - LibGuides Mobile LibGuides. 1L Survival Guide.  
 
	 
         
         
         
                     
                     
                     
                     
                    
                     
                              
         
         
          
        
		 
		 
		 
		                
        
        
        
        
        
         1L Survival Guide - LibGuides Mobile 
        
        
        
        	 
	 
		 
			This is the "1L Survival Guide" guide. 
			 Alternate Page for Screenreader Users  
			 Skip to Page Content  
		 
		 
                              LibGuides Mobile   
                         
                          
                 
                     
                     
                     
                     
                     
                
                     1L Survival Guide     
                      Last update:   Aug 31, 2017  
                      URL:   http://libguides.law.memphis.edu/content_mobile.php?pid=672901  
                    
                    
                     
                                         
                                             
                                              
                                            Year One
                                             
                                             
                                                 
                                                  
	 
		
		 
			
			   
			 Academic Success (in Library)
			 
		 
			
		           1L of a Ride   - Andrew McClurg   Call Number: KF287 .M38 2017           The Companion Text to Law School   - Andrew J. McClurg   Call Number: KF283 .M38 2012           ''One L of a Year''   - Leah M. Christensen   Call Number: KF283 .C485 2012           Acing Your First Year of Law School   - Shana Connell Noyes; Henry S. Noyes   Call Number: KF283 .N69 2008           Law School Without Fear - Strategies for Success   - Helene S. Shapo; Marshall S. Shapo   Call Number: KF386 .S44 2009           Law School Survival Manual   - Nancy Rapoport; Jeffrey D. Van Neil   Call Number: KF283 .R37 2010           Succeeding in Law School   - Herbert N. Ramy   Call Number: KF283 .R36 2010           Expert Learning for Law Students   - Michael Hunter Schwartz   Call Number: KF283 .S354 2008           Bridging the Gap Between College and Law School   - Ruta K. Stropus; Charlotte D. Taylor   Call Number: KF283 .S77 2009           Strategies Tactics First Year Law Student (Maximize Your Grades)   - Steven Emanuel   Call Number: KF283 .W35 2010           Understanding law school : an introduction to the LexisNexis understanding series and tips on how to succeed in law school   - Various   Call Number: KF283 .U53 2004  ISBN: 9780820561936     
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 CALI Lessons
			 
		 
			
		  To register for CALI (Computer Assisted Legal Instruction), ask for the  CALI authorization code  at the Library Circulation and Reference Desk.      Lessons in Legal Concepts and Skills  Interactive lessons allow you to test your understanding and sometimes teach you even more if you get the wrong answer.       
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 Academic Success (in Library, Continued)
			 
		 
			
		           Starting off Right in Law School   - Carolyn J. Nygren   Call Number: KF273 .N97 2011           Coming to Law School   - Ian Gallacher   Call Number: KF283 .G35 2010           Your Guide to Law School Success   - Donna Gerson   Call Number: KF283 .G47 2008 v.1           Get a Running Start   - Donald Gifford; Mark Graber; David Gray; William Richman; Michael Van Alstine   Call Number: KF283 .G739 2016           Navigating the First Year of Law School   - G. Nicholas Herman; Melissa A. Essary; Zachary C. Bolitho   Call Number: KF283 .H47 2016           What the L?   - Kelsey May; Samantha Roberts; Elizabeth Shelton   Call Number: KF283 .M388 2010           The Law School Trip   - Andrew McClurg   Call Number: PN6231.L4 M32 2001           A Short and Happy Guide to Being a Law Student   - Paula A. Franzese   Call Number: KF283 .F735 2014     
		 
				 Comments (0) 
		 
		  
		 
	   
                                                 
                                                 close 
                                             
                                         
                                     
                                     
                                         
                                             
                                              
                                            Civil Procedure
                                             
                                             
                                                 
                                                  
	 
		
		 
			
			   
			 Academic Success (in Library)
			 
		 
			
		           Civil Procedure   - Joseph W. Glannon   Call Number: KF8841 .G58 2013           The Glannon Guide to Civil Procedure   - Joseph W. Glannon   Call Number: KF8841 .G59 2013           Civil Procedure   - Mary Kay Kane   Call Number: KF8841 .K36 2013           Understanding Civil Procedure   - Gene R. Shreve, et al.   Call Number: KF8840 .S484 2013           Siegel's Civil Procedure   - Brian N. Siegel; Lazar Emanuel   Call Number: KF8841 .S54 2012           Questions and Answers: Civil Procedure   - William V. Dorsaneo; Elizabeth G. Thornburg   Call Number: KF8841 .D67 2015     
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 CALI Lessons
			 
		 
			
		  To register for CALI (Computer Assisted Legal Instruction), ask for the  CALI authorization code  at the Library Circulation and Reference Desk.      Lessons on Civil Procedure  Interactive lessons allow you to test your understanding and sometimes teach you even more if you get the wrong answer.         
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 Treatises and Hornbooks (in Library or eBooks)
			 
		 
			
		   
		 
				 Comments (0) 
		 
		  
		 
	   
                                                 
                                                 close 
                                             
                                         
                                     
                                     
                                         
                                             
                                              
                                            Contracts
                                             
                                             
                                                 
                                                  
	 
		
		 
			
			   
			 Academic Success (in Library)
			 
		 
			
		           Examples and Explanations for Contracts   - Brian A. Blum   Call Number: KF901 .B58 2017           Contracts in a Nutshell   - Claude Rohwer; Anthony Skrocki; Michael Malloy   Call Number: KF801.Z9 R62 2017           Questions and Answers: Contracts   - Scott J. Burnham; Keith A. Rowley   Call Number: KF801.Z9 R69 2014           Siegel's Contracts   - Brian N. Siegel; Lazar Emanuel; Siegel   Call Number: KF801.Z9 S54 2012           Starting off Right in Contracts   - Carolyn J. Nygren; Howard E. Katz   Call Number: KF801.Z9 N94 2014           Understanding Contracts   - Jeffrey Thomas Ferriell   Call Number: KF801.Z9 F46 2014     
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 CALI Lessons
			 
		 
			
		  To register for CALI (Computer Assisted Legal Instruction), ask for the  CALI authorization code  at the Library Circulation and Reference Desk.      Lessons on Contracts  Interactive lessons allow you to test your understanding and sometimes teach you even more if you get the wrong answer.       
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 Treatises and Hornbooks (in Library or eBooks)
			 
		 
			
		   
		 
				 Comments (0) 
		 
		  
		 
	   
                                                 
                                                 close 
                                             
                                         
                                     
                                     
                                         
                                             
                                              
                                            Criminal Law
                                             
                                             
                                                 
                                                  
	 
		
		 
			
			   
			 Academic Success (in Library)
			 
		 
			
		           Criminal Law   - Richard G. Singer; John Q. La Fond   Call Number: KF8841 .G58 2013           Understanding Criminal Law   - Joshua Dressler   Call Number: KF9219 .D74 2015           Loewy's Criminal Law in a Nutshell, 5th   - Arnold H. Loewy   Call Number: KF9219.85 .L64 2009           The Glannon Guide to Criminal Law   - Laurie L. Levenson   Call Number: KF9219.85 .L477 2012           Questions and Answers   - Emily Marcus Levine; Paul Marcus   Call Number: KF9219.85 .L48 2012     
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 CALI Lessons
			 
		 
			
		  To register for CALI (Computer Assisted Legal Instruction), ask for the  CALI authorization code  at the Library Circulation and Reference Desk.      Lessons on Criminal Law  Interactive lessons allow you to test your understanding and sometimes teach you even more if you get the wrong answer.         
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 Treatises and Hornbooks (in Library or eBooks)
			 
		 
			
		   
		 
				 Comments (0) 
		 
		  
		 
	   
                                                 
                                                 close 
                                             
                                         
                                     
                                     
                                         
                                             
                                              
                                            Property
                                             
                                             
                                                 
                                                  
	 
		
		 
			
			   
			 Academic Success (in Library)
			 
		 
			
		           The Glannon Guide to Property   - James Charles Smith   Call Number: KF561 .S627 2015           Understanding Property Law   - John G. Sprankling   Call Number: KF561.S67 2017           Property   - Burke   Call Number: KF560 .B87 2016           Questions and Answers   - John Copeland Nagle   Call Number: KF570 .N34 2014           Real Property in a Nutshell   - Roger Bernhardt; Ann Burkhart   Call Number: KF570 .B47 2016     
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 CALI Lessons
			 
		 
			
		  To register for CALI (Computer Assisted Legal Instruction), ask for the  CALI authorization code  at the Library Circulation and Reference Desk.      Lessons on Property Law  Interactive lessons allow you to test your understanding and sometimes teach you even more if you get the wrong answer.       
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 Treatises and Hornbooks (in Library or eBooks)
			 
		 
			
		         
		 
				 Comments (0) 
		 
		  
		 
	   
                                                 
                                                 close 
                                             
                                         
                                     
                                     
                                         
                                             
                                              
                                            Torts
                                             
                                             
                                                 
                                                  
	 
		
		 
			
			   
			 Academic Success (in Library)
			 
		 
			
		           The Law of Torts   - Joseph W. Glannon   Call Number: KF1250.Z9 G58 2015           Torts in a Nutshell   - Edward Kionka   Call Number: KF1250.Z9 K53 2015           Understanding Torts   - John L. Diamond; Lawrence C. Levine; Anita Bernstein   Call Number: KF1250 .D5 2013           Principles of Tort Law   - Marshall Shapo   Call Number: KF1250 .S46 2016           Questions and Answers   - Anita Bernstein   Call Number: KF1250.Z9 B47 2014           Mastering Tort Law   - Russell L. Weaver; Edward C. Martin; Andrew R. Klein; Paul J. Zwier; John H. Bauman   Call Number: KF1250 .M326 2016           Siegel's Torts   - Brian N. Siegel; Lazar Emanuel   Call Number: KF1250.Z9 S535 2012           Starting off Right in Torts   - Carolyn J. Nygren; Howard E. Katz   Call Number: KF1250 .N94 2012     
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 CALI Lessons
			 
		 
			
		  To register for CALI (Computer Assisted Legal Instruction), ask for the  CALI authorization code  at the Library Circulation and Reference Desk.      Lessons on Torts  Interactive lessons allow you to test your understanding and sometimes teach you even more if you get the wrong answer.       
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 Treatises and Hornbooks (in Library or eBooks)
			 
		 
			
		   
		 
				 Comments (0) 
		 
		  
		 
	   
                                                 
                                                 close 
                                             
                                         
                                     
                                     
                                         
                                             
                                              
                                            Research/Writing
                                             
                                             
                                                 
                                                  
	 
		
		 
			
			   
			 Academic Success (in Library)
			 
		 
			
		           Legal Research in a Nutshell   - Kent Olson; Morris Cohen   Call Number: KF240 .C54 2016           Legal Writing   - Terrill Pollman; Judith M. Stinson; Elizabeth Pollman   Call Number: KF250 .L398 2014           Legal Writing in a Nutshell, 4th   - Lynn B. Bahrych; Marjorie Dick Rombauer   Call Number: KF250 .S68 2009           Legal Writing by Design   - Teresa J. Reid Rambo; Leanne J. Pflaum   Call Number: KF250 .R35 2013           Legal Writing in Plain English   - Bryan A. Garner   Call Number: KF250 .G373 2013           Legal Writing and Analysis in a Nutshell   Call Number: KF250 .B34 2017           Cupples and Temple-Smith's Grammar, Punctuation and Style   - Deborah Cupples; Margaret Temple-Smith   Call Number: KF250 .C73 2013           Essay Success Express   - Marsha Graham; Elizabeth Weishaar   Call Number: KF250 .G73 2008           Legal Drafting in a Nutshell   - George Kuney; Donna Looper   Call Number: KF250 .K86 2016     
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 CALI Lessons
			 
		 
			
		  To register for CALI (Computer Assisted Legal Instruction), ask for the  CALI authorization code  at the Library Circulation and Reference Desk.      Lessons in Legal Research      Lessons in Legal Writing        
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 Treatises and Hornbooks (in Library or eBooks)
			 
		 
			
		   
		 
				 Comments (0) 
		 
		  
		 
	   
                                                 
                                                 close 
                                             
                                         
                                     
                                     
                                         
                                             
                                              
                                            Constitutional Law
                                             
                                             
                                                 
                                                  
	 
		
		 
			
			   
			 Academic Success (in Library)
			 
		 
			
		           Constitutional Law - National Power and Federalism   - Christopher N. May; Christopher N. Ides   Call Number: KF4550 .M29 2012           Constitutional Law--Individual Rights   - Allan Ides; Christopher N. May   Call Number: KF4749 I34 2013           Understanding Constitutional Law   - John Attanasio; Joel K. Goldstein; Norman Redlich   Call Number: KF4550.Z9 R43 2012           Constitutional Law in a Nutshell   - Jerome Barron   Call Number: KF4550 .B368 2017           Questions and Answers   - Paul E. McGreal; Linda S. Eads; Charles W. Rhodes   Call Number: KF4550.Z9 M4 2017     
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 CALI Lessons
			 
		 
			
		  To register for CALI (Computer Assisted Legal Instruction), ask for the  CALI authorization code  at the Library Circulation and Reference Desk.      Lessons in Constitutional Law  Interactive lessons allow you to test your understanding and sometimes teach you even more if you get the wrong answer.       
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 Treatises and Hornbooks (in Library or eBooks)
			 
		 
			
		   
		 
				 Comments (0) 
		 
		  
		 
	   
                                                 
                                                 close 
                                             
                                         
                                     
                                     
                                         
                                             
                                              
                                            Exams
                                             
                                             
                                                 
                                                  
	 
		
		 
			
			   
			 Academic Success (in Library)
			 
		 
			
		           Getting to Maybe   - Richard Michael Fischl; Jeremy Paul   Call Number: KF283 .F47 1999           Mastering the Law School Exam   - Suzanne Darrow-Kleinhaus   Call Number: KF283 .D37 2007           Writing Essay Exams to Succeed in Law School   - Dernbach   Call Number: KF283 .D47 2014           Law School Secrets   Call Number: KF283 .B38 2012           Law School Exams   - Charles R. Calleros   Call Number: KF283 .C35 2013           Open Book   - Barry Friedman; John C. P. Goldberg   Call Number: KF283 .F75 2011     
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 CALI Lessons
			 
		 
			
		  To register for CALI (Computer Assisted Legal Instruction), ask for the  CALI authorization code  at the Library Circulation and Reference Desk.      Exam Taking Skills, Outlines, and Advice for Law Students  Series of three podcasts (see left column for 2 and 3) to help you prepare.      Tips for Multiple Choice Exams in Law School Podcast      Top 10 Tips for Successfully Writing a Law School Essay      Writing Better Law School Exams: The Importance of Structure       
		 
				 Comments (0) 
		 
		  
		 
	   
                                                 
                                                 close 
                                             
                                         
                                     
                                             
             
                 Description       Loading content... please wait 
             
         
         
         
             
                         
                  More Information        Loading content... please wait  
                  Close window   
             
         
           
         
                          ^ Top     |     Home Page     |     Full Site  
                     
                    
                     
                         
                             Powered by  Springshare  and iWebKit. 
                             All rights reserved. 
                              Report a tech support issue.  
                             
                     
                    	 
 