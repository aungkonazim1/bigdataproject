Plan for Grad School - Career Services - University of Memphis    










 
 
 
     



 
    
    
    Plan for Grad School - 
      	Career Services
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Career Services
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Students/Alumni  
                          Employers  
                          Faculty/Staff  
                          Student Success  
                      
                      
                         
                            Students   Alumni   
                            
                               
                                  
                                   Schedule an Appointment  
                                   Explore Majors   Careers [FOCUS 2]  
                                   ACAD Career Unit  
                                   Resumes and Cover Letters  
                                   Interview Tips  
                                   Internships  
                                   Search Job   Internship Listings  
                                   Connect with Employers  
                                         Career Fairs  
                                         On-Campus Interviews  
                                     
                                  
                                   Suit Yourself Program  
                                         Overview  
                                         Eligibility and Criteria  
                                         Application  
                                         Selected Workshops  
                                         Faculty and Staff Recommendation  
                                     
                                  
                                   Memphis Career Preparation Academy  
                                         Overview  
                                         Benefits and Commitment  
                                         Workshops  
                                         Eligibility  
                                         Application  
                                     
                                  
                                   Professional Development Series  
                                   Plan for Graduate School  
                                   Resources  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Career Services
                              	  
                          
                              	Student   Alumni
                              	  
                         Plan for Grad School 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Thinking about Graduate or Professional School? 
                     
                      When it comes to continuing your education into graduate or professional school, you
                        must be clear on what field you intend to pursue. You must also determine whether
                        or not continuing your education will provide you with a significant advantage in
                        your career field. Here are some key resources to help you determine whether graduate
                        or professional school is right for you.
                      
                     
                      Personal Statement 
                     
                      Required by many schools. To write a statement that will help you win a seat in the
                        graduate program of your choice, you need to think like a member of the admissions
                        committee. You are welcome to schedule an appointment to meet with a career advisor
                        to start the process, help with ideas or receive a critique of your draft.
                      
                     
                      Admission Tests: 
                     
                      A short list of the different admission tests that are required by most Graduate Schools.
                        These tests are considered a means of predicting one's potential for success in graduate
                        or professional programs.
                      
                     
                      
                        
                          Graduate Record Examination (GRE)  
                        
                          Graduate Management Admission Test (GMAT )
                         
                        
                          Law School Admission Test (LSAT)  
                        
                          Medical College Admission Test (MCAT)  
                        
                          Dental Admission Test (DAT)  
                        
                          Pharmacy College Admission Test (PCAT)  
                        
                          Miller Analogies Test (MAT)  
                        
                      
                     
                        
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Students   Alumni 
                         
                            
                               
                                Schedule an Appointment  
                                Explore Majors   Careers [FOCUS 2]  
                                ACAD Career Unit  
                                Resumes and Cover Letters  
                                Interview Tips  
                                Internships  
                                Search Job   Internship Listings  
                                Connect with Employers  
                                      Career Fairs  
                                      On-Campus Interviews  
                                  
                               
                                Suit Yourself Program  
                                      Overview  
                                      Eligibility and Criteria  
                                      Application  
                                      Selected Workshops  
                                      Faculty and Staff Recommendation  
                                  
                               
                                Memphis Career Preparation Academy  
                                      Overview  
                                      Benefits and Commitment  
                                      Workshops  
                                      Eligibility  
                                      Application  
                                  
                               
                                Professional Development Series  
                                Plan for Graduate School  
                                Resources  
                            
                         
                      
                      
                      
                         
                            
                                TigerLink  
                               Linking Tigers to Careers 
                            
                            
                                Hire Tiger Talent  
                               Employer Recruiting Info 
                            
                            
                                Events  
                               Career fairs and more 
                            
                            
                                Contact Us  
                               Office location and information 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


