November 2016 &#8211; On Legal Grounds | Memphis Law Events &amp; Announcements    
 

 
	 
	 
	 
	 
	
	 November 2016   On Legal Grounds | Memphis Law Events   Announcements 
 

 
 
 
 
		
		
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 










 
 
  
 
 
    

 

 

 

	 
	
				
		 
			 
				
				 
					     
					 On Legal Grounds | Memphis Law Events   Announcements 									 
				
									 
						    
						   
						    Student Organizations 
 
	  AWA  
	  BLSA  
	  Bus.   Tax Law Society  
	  Christian Legal Society  
	  Federalist Society  
	  Health Law Society  
	  HLSA  
	  Honor Council  
	  ILS  
	  Law Review  
	  Memphis Law +1  
	  Moot Court Board  
	  OutLaw  
	  PAD  
	  PALS  
	  SELS  
	  SBA  
	  SGA  
	  Street Law  
	  TACDL  
 
 
  Law School Announcements 
 
	  Academic Affairs 
	 
		  Academic Affairs Calendar  
	 
 
	  Academic Success Program 
	 
		  Academic Success Program Calendar  
	 
 
	  Career Services Office 
	 
		  Career Services Office Calendar  
	 
 
	  Information Technology 
	 
		  Information Technology Calendar  
	 
 
	  Law Admissions, Recruiting,   Scholarships 
	 
		  Law Admissions, Recruiting, and Scholarships Calendar  
	 
 
	  Law School Registrar 
	 
		  Law School Registrar Calendar  
	 
 
	  Library 
	 
		  Library Calendar  
	 
 
	  Office of the Dean 
	 
		  Office of the Dean Calendar  
	 
 
	  Office of Diversity 
	 
		  Office of Diversity Calendar  
	 
 
	  Pro Bono Office 
	 
		  Pro Bono Office Calendar  
	 
 
	  Student Affairs 
	 
		  Student Affairs Calendar  
	 
 
 
 
  Blog Information  
  Writing Competitions  
  Events  
  
					  
								
			  
		  
		
	  
	
	 
		 			
			 
				 
 

	 

			   Monthly Archive:  November 2016  
			
	
  	
	 		
		
				
				
			 
				 				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Law Admissions, Recruiting,   Scholarships  
			 28 Nov, 2016 
		  
		
		 
			 Deadline this Friday for Horne   Wells Scholarship 
		  
		
				 				
			 Horne   Wells Scholarship Opportunity and Deadline The University of Memphis School of Law is pleased to announce the law firm    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  BLSA  
			 23 Nov, 2016 
		  
		
		 
			 BLSA Appeal! 
		  
		
				 				
			 I am pleased to bring you the final issue of the BLSA Appeal for the Fall semester! Good luck on exams!    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Student Affairs  
			 21 Nov, 2016 
		  
		
		 
			 DUBERSTEIN AND WAGNER RESULTS 
		  
		
				 				
			 Congratulations to the members of the Wagner and Duberstein Spring Moot Court Travel Teams: Wagner: Rebecca Holden, Regan Sherwood, and David    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Law School Announcements  /  Student Affairs  
			 21 Nov, 2016 
		  
		
		 
			 Massage on the Go   Wed., Nov. 23rd 
		  
		
				 				
			 On Wednesday, November 23rd, we will have FREE chair massages from Massage on the Go in the Student Lounge from 12:00    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Law School Announcements  /  Student Affairs  
			 21 Nov, 2016 
		  
		
		 
			 Mid-South Therapy Dogs   Tues., Nov. 22nd 
		  
		
				 				
			 We will host the Mid-South Therapy Dogs on Tuesday, November 22nd in the Student Lounge from 12:00   2:00 p.m. As    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Law School Registrar  
			 21 Nov, 2016 
		  
		
		 
			 FINAL EXAM ID 
		  
		
				 				
			 Students: You should receive an email from Cheryl Edwards (cedwrds2@memphis.edu) this evening that contains instructions for downloading the exam software, as    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Library  
			 21 Nov, 2016 
		  
		
		 
			 Free Study Help   CALI lessons 
		  
		
				 				
			 Don t forget the hundreds of CALI lessons available to assist you with prep for your final exams. Here’s the link to    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Law Admissions, Recruiting,   Scholarships  
			 21 Nov, 2016 
		  
		
		 
			 Horne   Wells Scholarship Opportunity and Deadline 
		  
		
				 				
			 The University of Memphis School of Law is pleased to announce the law firm of Horne   Wells, PLLC Scholarship will    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  PAD  
			 17 Nov, 2016 
		  
		
		 
			 P.A.D. thanks all!!!! 
		  
		
				 				
			 Phi Alpha Delta would like to take this time to thank all of the students and teachers who made the Canned    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Law School Announcements  
			 16 Nov, 2016 
		  
		
		 
			 MBA Bar Meet @ Lafayette s 
		  
		
				 				
			 The Memphis Bar Association would like to invite law students to attend their next  Bar Meet  this Thursday night at Lafayette s    
		  
				
	  	
  					   			  
		
			 
			 
 Page 1 of 5  1  2  3  4  5    
 	  
			
				
	  
	
  


	 
		
		    
		
		 
			
						 
				 Follow: 
							 
						
						
						
			   Law School Website                     		 		 Recent Posts 		 
					 
				 1L Minority Clerkship Program   Reception (Chattanooga) 
						 
					 
				 New resources! 
						 
					 
				 Community Legal Center Volunteer Opportunity 
						 
					 
				 Law School Bookstore 2 Day Sale   Dec. 6th and 7th! 
						 
					 
				 St. Jude Memphis Marathon/Downtown Street Closures 
						 
				 
		 		  Archives 		 
			  December 2017  
	  November 2017  
	  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
		 
		   Categories 		 
	  Academic Affairs 
 
	  Academic Success Program 
 
	  AWA 
 
	  BLSA 
 
	  Career Services Office 
 
	  Christian Legal Society 
 
	  Experiential Learning 
 
	  Federal Bar Association 
 
	  Federalist Society 
 
	  Health Law Society 
 
	  HLSA 
 
	  Honor Council 
 
	  ILS 
 
	  Information Technology 
 
	  Law Admissions, Recruiting,   Scholarships 
 
	  Law Review 
 
	  Law School Announcements 
 
	  Law School Registrar 
 
	  Library 
 
	  Memphis Law +1 
 
	  Mock Trial 
 
	  Moot Court Board 
 
	  National Lawyer s Guild 
 
	  Office of Diversity 
 
	  Office of the Dean 
 
	  OutLaw 
 
	  Outside Organizations 
 
	  PAD 
 
	  PALS 
 
	  Pro Bono Office 
 
	  SBA 
 
	  Sports   Entertainment Law Society 
 
	  Street Law 
 
	  Student Affairs 
 
	  Student Organizations 
 
	  Tennessee Association of Criminal Defense Lawyers 
 
	  Writing Center 
 
	  Writing Competitions 
 
		 
 			
		  
		
	  

	
 
	
	    
	
	 
		
				 
			 More 
		 
				
				
		  Search   
	 
		 
	 
    Login 					 
					 
										 
					 
					 
					  Username:  
					  
					  Password:  
					  
										  
					  Remember me  
															   
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
											  Don't have an account?  
										  Lost your password?  
					 
					
					 
					
										 
				
					 
					  Choose username:  
					  
					  Your Email:  
					  
										   
					 
					 
					 
					 
					 
					 
					 
					 
					   Have an account?   
					 
										
					 
			
					 
					  Enter your username or email:  
					  
										   
					 
					 
					 
					 
					 
					 
					 
					 
					   Back to login   
					 
					
					 
					
										  
					   
					 
					   Subscribe 	
	
 
	 
		 
		 
		 		
		
	 
		
		 
					  

		
 
		 Email Address *  
	 
  
 
		 First Name 
	 
  
 
		 Last Name 
	 
  
 
		 Middle Name 
	 
  			 
				* = required field			  
			
		 
			 
		  
	
	
				
	  
	  
  
	    Blog Feedback                     		
	  
	
  	

				  
			  			
		  
	  

	 
		
				
				
				
		 
			 
				
				    
				
				 
					
					 
						
												
						 
															 On Legal Grounds | Memphis Law Events   Announcements   2017. All Rights Reserved. 
													  
						
												 
							 Powered by  WordPress . Theme by  Alx . 
						  
												
					 
					
					 	
											 
				
				  
				
			  
		  
		
	  

  

 		
		










 
 
 