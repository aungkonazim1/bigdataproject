Peter I. Neathery Scholarship/Fellowship - Computer Science - University of Memphis    










 
 
 
     



 
    
    
    Peter I. Neathery Scholarship/Fellowship  - 
      	Computer Science
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Computer Science
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          News  
                          Future Students  
                          Current Students  
                          Research  
                          People  
                          Outreach  
                      
                      
                         
                            Financial Aid   
                            
                               
                                  
                                   Undergraduate Scholarships  
                                   Fellowships   Assistantships  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Computer Science
                              	  
                          
                              	Financial Aid
                              	  
                         Peter I. Neathery Scholarship/Fellowship  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Peter I. Neathery Scholarship/Fellowship 
                     
                      The Peter I. Neathery Scholarships and Fellowships in Computer Science were made possible
                        by a generous gift from the eponymous donor. These merit-based awards are given to
                        both undergraduate and graduate Computer Science students at the University of Memphis.
                        Up to four undergraduate and four graduate students each year may receive the award.
                      
                     
                      Undergraduate Scholarships 
                     
                      
                        
                         If sufficient funding is available, and at the discretion of the Computer Science
                           faculty, up to four (4) $1,000.00 Neathery Scholarships may be competitively awarded
                           each year to undergraduate students who are enrolled full-time at the University of
                           Memphis. Recipients may be current students or incoming freshmen.
                         
                        
                         Incoming freshmen must have Computer Science as their declared major with an ACT score
                           of 28 or higher (26 or higher for students who have scored 5 on the AP Computer Science
                           A exam).
                         
                        
                         Current students must be majoring in Computer Science, have a GPA of 3.5 or higher,
                           and be making satisfactory progress towards their Computer Science degree.
                         
                        
                         The scholarship is renewable for a total of three (3) consecutive years; therefore,
                           a student may be awarded this scholarship for a total of four (4) years.
                         
                        
                         Awardees are selected by the Computer Science faculty or designated committee. 
                        
                      
                     
                      Graduate Fellowships 
                     
                      
                        
                         If sufficient funding is available, and at the discretion of the Computer Science
                           faculty, up to four (4) Neathery Fellowships for $3,000.00 per student may be awarded.
                         
                        
                         This fellowship is for one year of funding. 
                        
                         Recipients must be full-time current or incoming graduate students majoring in Computer
                           Science.
                         
                        
                         Students must have a BS degree in computer science or closely related area, and an
                           undergraduate GPA of 3.5 or above is preferred.
                         
                        
                         Current students must be in the graduate program less than one year, and the most
                           recent GPA should also be at least 3.0.
                         
                        
                         GRE scores of at least 308 (V+Q) preferred. 
                        
                         Students must be nominated by at least one Computer Science faculty member. 
                        
                      
                     
                      Application 
                     
                      All applications should be submitted through the University's centralized Tiger Scholarship
                        Manager system. Please refer to the University's   Scholarships   page for details.
                      
                     
                      All recipients of the Neathery award are required to: 
                     
                      
                        
                         Have their photo made 
                        
                         Provide a brief biography 
                        
                         Write a thank you note that can be included in the donor's packet 
                        
                      
                     
                      Past Recipients 
                     
                      
                        
                         Jason Buck (undergraduate, 2008-09) 
                        
                         Xiaoshan Cai (undergraduate, 2008-09, 2007-08, 2006-07) 
                        
                         Tim Doan (undergraduate, 2014-15) 
                        
                         Laqin Fan (graduate, 2016-17) 
                        
                         Eric Franks (undergraduate, 2008-09) 
                        
                         Brian Hanson (undergraduate, 2006-07) 
                        
                         Sean Higgins (undergraduate, 2013-14) 
                        
                         Michael Irick (undergraduate, 2008-09) 
                        
                         Jon Cullyn Moore (undergraduate, 2016-17) 
                        
                         Austin Nabors (undergraduate, 2014-15) 
                        
                         Julia Nordstad (undergraduate, 2005-06) 
                        
                         Austin Thompson (undergraduate, 2004-05) 
                        
                         Quang Tran (graduate, 2014-15) 
                        
                         Kazi Zaman (graduate, 2016-17) 
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Financial Aid 
                         
                            
                               
                                Undergraduate Scholarships  
                                Fellowships   Assistantships  
                            
                         
                      
                      
                      
                         
                            
                                Career Opportunities  
                               Internship information and job postings for students 
                            
                            
                                Degree Programs  
                               Information on our degree and certificate programs 
                            
                            
                                Courses  
                               Recent syllabi for our courses 
                            
                            
                                Contact Us  
                               Whom to contact about what 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/21/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


