Master of Arts in Liberal Studies - University College - University of Memphis    










 
 
 
     



 
    
    
    Master of Arts in Liberal Studies   - 
      	University College
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			University College
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Undergraduate  
                          Graduate  
                          Resources  
                          Online  
                          Faculty and Staff  
                      
                      
                         
                            Graduate   
                            
                               
                                  
                                   Master of Arts in Liberal Studies  
                                   Master of Professional Studies  
                                   Certificate in Strategic Leadership  
                                   Certificate in Liberal Studies  
                                   Advising Appointment  
                                   Financial Aid  
                                   Graduate Assistantship  
                                   Questions? Email Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	University College
                              	  
                          
                              	Graduate
                              	  
                         Master of Arts in Liberal Studies   
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Master of Arts in Liberal Studies (MALS) 
                     
                      The Master of Arts in Liberal Studies (MALS) program at the University of Memphis
                        allows students to customize an interdisciplinary course of study. The program features
                        a flexible, adaptable curriculum that students can tailor for the broadest possible
                        application and personal enrichment. It is designed to sharpen the intellectual skills
                        necessary for lifelong learning—critical reading, scholarly writing, and the art of
                        interpersonal communication.
                      
                     
                      Program Details 
                     
                      Because each MALS student follows a unique course of study, students work with academic
                        advisors to develop a curriculum proposal that satisfies both the interdisciplinary
                        intent of the MALS program and the Graduate School's requirements for graduation.
                      
                     
                      Every MALS student is required to complete and submit a  Coordinated Study grid  before being admitted to the program. The Coordinated Study grid is a planning document
                        that students use to identify the classes that will constitute their degree. Students
                        may combine courses from any two or more disciplines with guidance and approval from
                        an academic advisor. Review this example of a Coordinated Study grid completed by
                        a student in the field of  Hospitality Management .
                      
                     
                      The Coordinated Study is comprised of twenty-one (21) credit hours selected by the
                        student from two or more disciplines. No more than 12 credit hours may come from any
                        single department. At least 12 hours of the Coordinated Study must be 7000-level courses.
                        Each student completes a Coordinated Study grid before starting the program, and that
                        grid serves as a working agreement between the student and University College. The
                        Coordinated Study grid must also identify at least three alternative courses that
                        could be taken in the event that any of the student's primary course selections are
                        not offered during the term of the student's enrollment.
                      
                     
                      At the time of admission to the MALS program, the Graduate Coordinator approves the
                        student's course of study as reflected on the Coordinated Study Grid that accompanies
                        the application essay. Students then enroll each semester in available courses from
                        the approved plan in a  sequence of study . Within two weeks after the start of the final semester, students will submit a final
                        program proposal that reiterates the student's academic goals (if necessary, reflecting
                        any changes that have developed since admission) and identifies the Coordinated Study
                        courses actually completed. Major deviations from the original approved plan require
                        department pre-approval and will be addressed at the time of registration each semester.
                      
                     
                      Students enrolled in the MALS degree will complete at least 33 credit hours, successfully
                        present a Special Project (UNIV 7997), and pass an oral comprehensive examination.
                        The 33 hours include two segments: the MALS Core and the Coordinated Study.
                      
                     
                      The MALS Core is required of all students, and consists of twelve (12) pre-selected
                        credit hours:
                      
                     
                      
                        
                          UNIV 7000 Foundations of Liberal Studies  (3 hours)
                         
                        
                          UNIV 7100 Research in Interdisciplinary Studies  (3 hours)
                         
                        
                          UNIV 7200 Liberal Studies Seminar  (3 hours)
                         
                        
                          UNIV 7997 Special Project  (3 hours)
                         
                        
                      
                     
                      Students wishing to take UNIV 7997 must submit a proposal to the Graduate Coordinator
                        in the semester prior to taking the course to receive approval. Access our  Proposal Approval Form  and review deadlines and additional details.
                      
                     
                      Applying for Admission to the Program 
                     
                      We are currently accepting applications for the Spring 2018 semester, with a submission
                        deadline of December 8, 2017.
                      
                     
                      All applicants must have completed an undergraduate degree with a cumulative grade
                        point average of 2.75 on a 4.0 scale from an accredited college or university. Applicants
                        are selected on a competitive basis, so admission is not guaranteed to all applicants
                        who meet only the minimum requirements. Since all University College Graduate Programs
                        aim to produce lifelong learners and competitive members of the workforce, applicants'
                        submitted materials are scrutinized not only for content and but also for presentation,
                        organization, professionalism, and compliance with the program's specific application
                        instructions, which are listed below.
                      
                     
                      Step 1: Application for Admission to the Graduate School 
                     
                      All students seeking to enter any of the University College graduate programs must
                        first  apply to the Graduate School  at the University of Memphis. The Graduate School accepts applications via electronic
                        submission. Please review their site for specific instructions and deadlines. The
                        MALS program does not require the GRE for admission. However, if you have taken the
                        GRE or any similar entrance exam, please submit your scores to the Graduate School.
                      
                     
                      Step 2: Application to the University College MALS Program 
                     
                      As soon as possible after applying to the Graduate School, students should submit
                        the following to the University College Graduate Programs office:
                      
                     
                      
                        
                          Resumé: The applicant's resumé should include the applicant's employment history, academic
                           history, current contact information, and any additional relevant information, such
                           as professional skills, training, presentations, publications, etc.
                         
                        
                          Recommendation Letter:  Applicants must submit at least one  official letter of recommendation , preferably from a previous or current instructor who can attest to the student's
                           capacity for graduate work. All recommendation letters must meet University College
                           requirements or they will not be accepted.
                         
                        
                          Coordinated Study Grid:  Applicants must submit a completed  Coordinated Study Grid  detailing the intended course of study.
                         
                        
                          Personal Essay:  Applicants must submit a brief, original essay (approximately 500-600 words) that
                           addresses the following points:
                           
                            
                              
                               Why is the MALS program the best choice for pursuing your personal and intellectual
                                 goals?
                               
                              
                               How have your academic and professional experiences prepared you for this program? 
                              
                               What is the theme of the program of study you want to pursue, and how will your Coordinated
                                 Study courses develop that theme?
                               
                              
                               Why do you think you are a good candidate for the MALS program? 
                              
                            
                           
                         
                        
                      
                     
                      To expedite processing, applicants are strongly advised to submit their materials
                        all at once rather than sending each document separately.  Each document must have
                        the applicant's name and submission date, and may be either emailed to  ucgrad@memphis.edu  or mailed to the following address:
                      
                     
                      
                        
                         Graduate Programs Coordinator University College University of Memphis 201 Brister Hall Memphis, TN 38152-3440
                         
                        
                      
                     
                      Step 3: The Interview 
                     
                      One of the University College Graduate Program Coordinators may also require an interview
                        with the applicant. If deemed necessary, the interview will be scheduled as a component
                        of the application process prior to the acceptance. Interviews are conducted in person
                        or by video conference.
                      
                     
                      
                     
                      Only after all of these steps are completed will a final decision be made on the application.
                        Please note that the University College only has the authority to recommend or deny
                        admission. The Graduate School makes all final admission decisions and will notify
                        applicants once a decision has been reached.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Graduate 
                         
                            
                               
                                Master of Arts in Liberal Studies  
                                Master of Professional Studies  
                                Certificate in Strategic Leadership  
                                Certificate in Liberal Studies  
                                Advising Appointment  
                                Financial Aid  
                                Graduate Assistantship  
                                Questions? Email Us  
                            
                         
                      
                      
                      
                         
                            
                                Find the program that's right for you  
                               Explore our degree programs 
                            
                            
                                Learn more about University College  
                               Request more program information 
                            
                            
                                Advising Appointments  
                               Academic Advising is available in-person, by phone or online 
                            
                            
                                Contact Us  
                               The University College team is here to help 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


