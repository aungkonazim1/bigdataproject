Dr. Kristen Jones invited to serve on the editorial board of the Journal of Management - FCBE - University of Memphis    










 
 
 
     



 
    
    
    Dr. Kristen Jones invited to serve on the editorial board of the Journal of Management - 
      	FCBE
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College of Business   Economics
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          Students  
                          Departments  
                          Faculty   Staff  
                          Research  
                          News  
                      
                      
                         
                            News   
                            
                               
                                  
                                   News  
                                         Current News  
                                         News Archive  
                                     
                                  
                                   Events  
                                   FCBE Media Room  
                                         College Publications  
                                         Fogelman Focus  
                                         Fogelman Flash  
                                         Memphis Memo  
                                         Fogelman Features  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	FCBE
                              	  
                          
                              	News
                              	  
                         Dr. Kristen Jones invited to serve on the editorial board of the Journal of Management 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Dr. Kristen Jones invited to serve on the editorial board of the  Journal of Management  
                     
                      For release:  May 30, 2017  Dr. Kristen Jones, assistant professor in the Department of Management, was recently
                        invited to serve on the editorial board of the  Journal of Management  ( JOM ).  JOM  is committed to publishing scholarly empirical and theoretical research articles
                        that have a high impact on the management field as a whole.  JOM  covers domains such as business strategy and policy, entrepreneurship, human resource
                        management, organizational behavior, organizational theory, and research methods.
                        It is peer-reviewed and published bi-monthly. The journal is ranked 4/192 in Management,
                        3/120 in Business, and 1/79 in Applied Psychology.  JOM  is the official Journal of the Southern Management Association.
                      
                     
                      "The Department of Management is thrilled that Dr. Jones has been invited to serve
                        on the editorial board of the Journal of Management," said Dr. Chuck Pierce, chair
                        of the Department of Management. " JOM  is a top-tier, preeminent journal in the field of management. Dr. Jones' invitation
                        to serve on  JOM's  editorial board is a reflection of her excellent research publication record."
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         News 
                         
                            
                               
                                News  
                                      Current News  
                                      News Archive  
                                  
                               
                                Events  
                                FCBE Media Room  
                                      College Publications  
                                      Fogelman Focus  
                                      Fogelman Flash  
                                      Memphis Memo  
                                      Fogelman Features  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply to our Undergraduate Programs    
                               Enroll at the university and engage yourself with our six different departments, student
                                 enrichment, and more. 
                               
                            
                            
                                Apply to our Masters Programs    
                               Earn your degree with one of our five MBA tracks or with one of our four specialized
                                 Masters programs!
                               
                            
                            
                                Apply to our Doctoral Programs    
                               Interested in a Ph.D.? Start the journey towards your Ph.D. here! 
                            
                            
                                Contact Us    
                               Have questions? Call or email today!  
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


