Troubleshooting metadata conflicts | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Troubleshooting metadata conflicts | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Metadata    »    Metadata basics   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Troubleshooting metadata conflicts                       
        
        
       
        
     
              
	Conflicts occur when a resource’s metadata violates restrictions set by the current view. You might encounter a conflict when you:
 

  
		Switch between views.
	 
	 
		Switch from the advanced interface to the basic interface.
	 
	 
		Apply a template.
	 
	 
		Import metadata.
	 
     
				Conflict
			 
			 
				Reason
			 
			 
				Result
			 
			 
				Resolution
			 
		   
				Character Limit
			 
			 
				The value of the data exceeds the character limit set for the field.
			 
			 
				The value truncates.
			 
			 
				Use a shorter value or cancel your changes and use the advanced interface or an alternate view.
			 
		   
				Vocabulary
			 
			 
				The value of the data does not belong to the restricted vocabulary set for the field.
			 
			 
				The value clears.
			 
			 
				Select a value from the field’s restricted vocabulary or cancel your changes and use the advanced interface or an alternate view.
			 
		   
				Multiplicity
			 
			 
				A field contains more values than the view allows.
			 
			 
				Additional values save but do not display.
			 
			 
				Use the advanced interface or an alternate view if you want to see or edit the additional values.
			 
		   
				Language
			 
			 
				Occurs when you import data in a language not used by the Metadata tool in your organization.
			 
			 
				The language changes to “unspecified.”
			 
			 
				Select an alternate language, leave the language unspecified, or ask site administration to add the language.
			 
		        Audience:     Instructor       

    
           

                   ‹ Changing your metadata interface 
        
                   up 
        
                   Importing a vCard › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Metadata  
  
      Metadata basics    Accessing Metadata    Metadata interfaces    Setting metadata interface preferences    Changing your metadata interface    Troubleshooting metadata conflicts    Importing a vCard      Adding and removing metadata    
                  
           
         

       
     

    
    
    
   
 
   
 
