Creating activities | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Creating activities | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Rubrics    »    Using Rubrics in Competencies   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Creating activities                       
        
        
       
        
     
              
	 

  
      
	Activities are created inside a course offering and are the only competency structure elements that can be assessed. You can set up flexible competency structures and associate multiple learning objectives with an activity, or associate multiple activities to a learning objective.
 

 
	The following are ways you can associate activities to learning objectives:
 

  
		 Association only (no assessment)  You can assign this type of association to activities that are part of a learning objective's coverage, but do not require any formal assessment. This association type has no impact on competency structure completion and does not affect competency or learning objective evaluations.
	 
	 
		 
			 Tip  If you associate an activity with a learning objective without adding an assessment method or achievement threshold, provide users with clear and specific performance benchmarks that create transparency about expectations.
		 
	 
	 
		 Association with rubric assessment  You can assign this type of association to activities if you want to assess users, but you do not want assessment to affect competency or learning objective evaluations within a competency structure. This is useful if you are creating diagnostic or formative assessments.
	 
	 
		 Association with rubric assessment and threshold criterion  You can assign this type of association to activities if you want to assess users and make the assessment a requirement for competency structure completion. The threshold is the minimum required rubric level or percentage score a user must achieve in the assessment to complete the associated learning objective.
	 
  
	Activity types
 

  
		 Quiz activity  Quiz activities are assessed by a user’s quiz score, the score of specific quiz questions, or a rubric.
	 
	 
		 Survey activity  Survey activities are assessed by a rubric. You cannot associate anonymous survey activities with learning objectives.
	 
	 
		 Dropbox activity  Dropbox activities are assessed by a rubric.
	 
	 
		 Discussion activity  Discussion activities are assessed by a rubric.
	 
	 
		 Grade Item activity  Grade item activities are assessed by a numeric score or a rubric.
	 
	 
		 Content activity  You can associate modules and topics in the Content tool with learning objectives, but you cannot assess them with a numeric score or rubric.
	 
	 
		 Manual Assessment activity  Manual assessment activities are not created with Learning Environment course tools. They can be activities such as a live presentation, a musical recital, community service hours, and extracurricular activities. Manual Assessment activities are assessed by a rubric.
	 
     


 
     Audience:     Instructor       

    
           

                   ‹ Using Rubrics in Competencies 
        
                   up 
        
                   Using Rubrics in ePortfolio › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Rubrics  
  
      Rubrics Basics    Using Rubrics in Competencies    Creating activities      Using Rubrics in ePortfolio    
                  
           
         

       
     

    
    
    
   
 
   
 
