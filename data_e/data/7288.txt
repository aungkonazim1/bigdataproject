Creating text information | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Creating text information | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Question Library    »    Question types   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Creating text information                       
        
        
       
          
     
           Printer-friendly version       
	You can create text information if you want to provide supplementary information that relates to several questions. It is useful to create text information if you have several questions that need to reference the same passage and you want to avoid repeatedly writing the same text for each question.
 

 
	For example, perhaps you have a case study you want to base several questions on. Instead of inserting the case study into each question, you can simply create a text information item and have your related questions appear directly underneath it.
 

 
	Create a text information item in the Question Library
 

  
		Enter a  Title .
	 
	 
		Enter your text information in the  Question Text  field.
	 
	 
		Click  Preview  to view your text information.
	 
	 
		Click  Save  to return to the main page, click  Save and Copy  to save and create another text information item that retains the copied properties, or click  Save and New  to continue creating new text information.
	 
      Audience:    Instructor      

    
           

                   ‹ Creating Likert questions 
        
                   up 
        
                   Creating image information › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Question Library  
  
      Question Library basics    Question types    Understanding regular expressions    Creating true or false questions    Creating multiple choice questions    Creating multi-select questions    Creating long answer questions    Creating short answer questions    Creating multi-short answer questions    Creating fill in the blanks questions    Creating matching questions    Creating ordering questions    Creating arithmetic questions    Creating significant figures questions    Creating Likert questions    Creating text information    Creating image information      
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
