webswave overview   
   webswave overview   
 
 webswave Overview 
 (last revised April 23, 2015)  
 
The  webswave  program is a display/diagnostic standalone
tool for plotting traces and picks in real time using a web browser and
the recent websockets standard in HTML5. The tool has an embedded
webserver (Mongoose 3) that feeds traces and picks to any browser
client without the need for additional software. Currently for Linux
and Mac OSX only, requiring websocket-compatible web browsers such as
Firefox 16+, Chrome 23+ or Safari 6+. It also works with iOS6 Safari,
allowing real-time traces on a smartphone. 
 
 


   Module Index 
  
 
  
 
  Questions? Issues?  Subscribe to the Earthworm Google Groups List.   
  
  