Masters - Department of Chemistry - University of Memphis    










 
 
 
     



 
    
    
    Masters  - 
      	Department of Chemistry
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Chemistry
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          People  
                          Research  
                          Resources  
                      
                      
                         
                            Programs   
                            
                               
                                  
                                   Undergraduate  
                                         Chemistry Major (B. S.) - Chemistry Concentration  
                                         Chemistry Major (B. S.) - Biochemistry Concentration  
                                         Chemistry Major (ACS Certified B. S.)  
                                         Chemistry Minor  
                                         Courses and Information  
                                         Advising  
                                         Permit Requests  
                                         Scholarships   Awards  
                                         Research Opportunities  
                                         Undergraduate Research Conference  
                                         NSF REU Program  
                                         Student Members of ACS (SMACS)  
                                         Career Options  
                                     
                                  
                                   Graduate  
                                        
                                         Ph. D. Chemistry  
                                        
                                         Master of Science in Chemistry  
                                        
                                         Courses  
                                        
                                         Advising  
                                        
                                         Assistantships  
                                        
                                         Grad Student Association  
                                        
                                         Apply!  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of Chemistry
                              	  
                          
                              	Programs
                              	  
                         Masters  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Master of Science (Chemistry Major) 
                     
                      Concentrations are available in Analytical, Computational, Inorganic, Organic, and
                        Physical Chemistry. The M.S. degree requires 30 hours approved by the student's advisory
                        committee:
                      
                     
                      
                        
                         Up to 9 hours in 6000 level courses (see diagnostic requirements) 
                        
                         At least 9 hours in courses numbered 7100 - 7899, with at least two areas of chemistry
                           represented
                         
                        
                         Presentation (CHEM 7911) 
                        
                         Up to three hours of seminar (CHEM 7913) 
                        
                         Up to 9 hours in research and independent study (CHEM 7001, 7910, 7996) 
                        
                         Comprehensive examinations, including written cumulative examinations and research
                           prospectus
                         
                        
                      
                     
                      Further details may be found in the  Graduate Catalog .
                      
                     
                      Information for after you earn your degree: 
                     
                      
                        
                          American Chemical Society Careers  
                        
                          UofM Career Services  
                        
                          What can you do with a degree in Chemistry?  
                        
                          Links for careers in Chemistry  
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Programs 
                         
                            
                               
                                Undergraduate  
                                      Chemistry Major (B. S.) - Chemistry Concentration  
                                      Chemistry Major (B. S.) - Biochemistry Concentration  
                                      Chemistry Major (ACS Certified B. S.)  
                                      Chemistry Minor  
                                      Courses and Information  
                                      Advising  
                                      Permit Requests  
                                      Scholarships   Awards  
                                      Research Opportunities  
                                      Undergraduate Research Conference  
                                      NSF REU Program  
                                      Student Members of ACS (SMACS)  
                                      Career Options  
                                  
                               
                                Graduate  
                                     
                                      Ph. D. Chemistry  
                                     
                                      Master of Science in Chemistry  
                                     
                                      Courses  
                                     
                                      Advising  
                                     
                                      Assistantships  
                                     
                                      Grad Student Association  
                                     
                                      Apply!  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply to the Department of Chemistry  
                               Now accepting applications for graduate school 
                            
                            
                                Contact us!  
                               Call us (901.678.2621) or stop by the Departmental office at Smith Chemistry Rm 210 
                            
                            
                                Seminar Schedule  
                               Click here for the current seminar schedule 
                            
                            
                                What's new?  
                               Click here to see what's new in our Department 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


