Requesting Class Permits - Registrar - University of Memphis    










 
 
 
     



 
    
    
    Requesting Class Permits - 
      	Registrar
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Registrar
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Register  
                          Students  
                          Veterans  
                          Faculty   Staff  
                          Forms  
                      
                      
                         
                            Register   
                            
                               
                                  
                                   Register  
                                   Quick Guide  
                                   Schedule Builder  
                                   Errors/Problems  
                                   Holds  
                                   Permits  
                                   Credit by Exam  
                                   Auditing  
                                   Complete Self Service Guides  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Registrar
                              	  
                          
                              	Register
                              	  
                         Requesting Class Permits 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Requesting Class Permits 
                     
                      A permit is an electronic "permission slip" that allows you to register for a course
                        section that is closed to you because it is full or has been labeled "restricted"
                        or "permit required" by the academic department.
                      
                     
                      If a section you want to register for carries the note "Restricted" or "Permit Required,"
                        you must receive an electronic permit before you can register for the course.
                      
                     
                      For almost all subjects, this means you must request a permit from the academic department
                        offering the subject. However, there are some exceptions. Click on the appropriate
                         Contact Info  link (in the right-hand column or bottom of this page) to determine which office
                        you should contact for a permit.
                      
                     
                      If the department/unit agrees to allow you to register for the course, it will issue
                        an electronic "permit" in the University's registration system. Until it issues this
                        permit, the system will not allow you to register for the class.
                      
                     
                      To see if a department has issued you a permit, check your "Registration Status" screen
                        in Student Self Service.
                      
                     
                      Important Points to Remember About Permits 
                     
                      
                        
                         
                           
                            The Registrar's Office  CANNOT  issue permits: only academic departments or advising units can.
                            
                           
                         
                        
                         
                           
                            A permit gives you permission to register for a class: you, yourself, must actually
                              register for the class through the online registration system.
                            
                           
                         
                        
                         
                           
                            If the online registration system will not allow you to register for the class, even
                              though the department/unit agreed to issue a permit, contact the office again and
                              explain the problem. The office will need to determine if it issued the permit correctly
                              or if it needs to issue an additional permit or permits.
                            
                           
                         
                        
                         
                           
                            If you decide you do not want to take the class after all and drop the permit, you
                              will no longer be able to register for the class. So be sure you really do not want
                              the class before dropping the permit. Otherwise, you will have to request the permit
                              again.
                            
                           
                         
                        
                      
                     
                      Contact Information for Permits 
                     
                      Select the college offering your course to determine the office to contact: 
                     
                      
                        
                           Subjects Offered by the Fogelman College of Business   Economics   (excluding Online Programs)
                         
                        
                           Subjects Offered by ALL Other Colleges   (excluding Online Programs)
                         
                        
                           Subjects Offered by Online Programs   (TN eCampus and UofM)
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Register 
                         
                            
                               
                                Register  
                                Quick Guide  
                                Schedule Builder  
                                Errors/Problems  
                                Holds  
                                Permits  
                                Credit by Exam  
                                Auditing  
                                Complete Self Service Guides  
                            
                         
                      
                      
                      
                         
                            
                                Calendars  
                               View Academic Year calendars; semester dates/deadlines; final exam schedules. 
                            
                            
                                Transcripts  
                               Request copies of your official transcript. 
                            
                            
                                Notice!  
                                Check for alerts and reminders. 
                            
                            
                                Contact Us   
                               Need additional assistance or information? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


