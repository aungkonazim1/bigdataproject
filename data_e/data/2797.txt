TN Institute for Prelaw - School of Law - University of Memphis  TN Institute for Prelaw  










 
 
 
     



 
    
    
    TN Institute for Prelaw - 
      	School of Law 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Cecil C. Humphreys School of Law
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Admissions  
                          Programs  
                          Current Students  
                          Faculty  
                          Careers  
                          Library  
                      
                      
                         
                            About   
                            
                               
                                  
                                   Administration   Staff  
                                   History  
                                   Strategic Plan  
                                   Virtual Tour  
                                   ABA Disclosures  
                                   Diversity  
                                        
                                         Diversity Home  
                                        
                                         TIP  
                                        
                                         Scholarships  
                                     
                                  
                                   Facilities  
                                         Facilities Overview  
                                         Virtual Tour  
                                         Floor Plans  
                                         Photo Gallery  
                                     
                                  
                                   News   Events  
                                         Law School News  
                                         On Legal Grounds Blog  
                                         ML-Memphis Law Magazine  
                                         Events Calendar  
                                         Communications Office  
                                     
                                  
                                   Directions  
                                   Contact  
                                         Media Resource  
                                         General Contact Info  
                                         Faculty   Staff  
                                     
                                  
                                   Life in Memphis  
                                         Memphis-The City  
                                         Neighborhoods  
                                         Housing  
                                         The Arts Scene  
                                         Sports  
                                         The Food Scene  
                                         A Music Town  
                                         Directions and Maps  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	School of Law 
                              	  
                          
                              	About
                              	  
                         TN Institute for Prelaw 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                        
                     
                      TN INSTITUTE FOR PRE-LAW 
                     
                      The Tennessee Institute for Pre-Law (TIP) program is an "admission by performance"
                        program for Tennessee and border county residents who are not admitted through the
                        regular admissions process, but who show potential for academic success in the study
                        of law and bring diversity to the class. TIP seeks to matriculate students from diverse
                        backgrounds and circumstances who are capable of successfully participating in law
                        school curricular and co-curricular programs.
                      
                     
                      The TIP program is a five-week program of classroom instruction that simulates the
                        first-year law school curriculum. The program is held at the University of Memphis
                        School of Law beginning in mid-June and ending in late July. Please note: If you are
                        taking the June 2017 LSAT, you cannot be considered for the Summer 2017 TIP class. 
                        TIP classes begin prior to the release of June test results.
                      
                     
                      Applicants who are admitted into TIP and successfully complete the summer program
                        will be eligible for Fall 2017 admission to the University of Memphis School of Law.
                      
                     
                       TIP ELIGIBILITY:  
                     
                      The Applicant must be: 
                     
                      
                        
                         A Tennessee or Border County Resident (Crittenden County in Arkansas and De Soto,
                           Marshall, Tate   Tunica Counties in Mississippi);
                         
                        
                         A graduating college senior or applicant who will have obtained a baccalaureate degree
                           prior to program commencement; and
                         
                        
                         Able to demonstrate diversity. 
                        
                      
                     
                      Interested Applicants must submit the following by  March 15th :
                      
                     
                      
                        
                         University of Memphis School of Law Application for Admission; 
                        
                         The TIP portion of the Application for Admission; and 
                        
                         The required TIP supplemental statement. 
                        
                      
                     
                      The TIP portion of the Application for Admission requests whether an applicant is: 
                     
                      
                        
                         A first-generation college graduate; 
                        
                         Foreign-born, or a first-generation U.S. citizen; 
                        
                         A graduate of a historically minority undergraduate institution; 
                        
                         Economically disadvantaged; 
                        
                         Disabled; or 
                        
                         Racially or ethnically underrepresented. 
                        
                      
                     
                      The required  TIP Supplemental Statement  asks the applicant to discuss any circumstances (financial, personal, family, health-related,
                        etc.) that have impacted his or her educational opportunities. Applicants may also
                        discuss the way in which he or she might contribute to diversity in the law school.
                      
                     
                      Application to TIP is optional, and admission to TIP is competitive. Only a limited
                        number of seats are available.
                      
                     
                       FINANCIAL ASSISTANCE  
                     
                      There is no fee to participate in this program. Outside employment and concurrent
                        enrollment in an academic program is prohibited. Therefore, program participants are
                        provided the following for the duration of the program:
                      
                     
                      
                        
                         A $1,000 stipend. 
                        
                         Law school textbooks. 
                        
                         Free parking. 
                        
                         Free WIFI, printing, and e-mail access. 
                        
                         Main Campus housing for non-Shelby County residents. 
                        
                      
                     
                      Notification of the TIP decision will be given in mid to late April. Please contact
                        Alena Allen, Associate Professor and Director of Law School Diversity, at (901) 678-3227
                        or via  email  if you have any questions.
                      
                     
                      To view a comprehensive brochure about TIP,  please click here.  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         About 
                         
                            
                               
                                Administration   Staff  
                                History  
                                Strategic Plan  
                                Virtual Tour  
                                ABA Disclosures  
                                Diversity  
                                     
                                      Diversity Home  
                                     
                                      TIP  
                                     
                                      Scholarships  
                                  
                               
                                Facilities  
                                      Facilities Overview  
                                      Virtual Tour  
                                      Floor Plans  
                                      Photo Gallery  
                                  
                               
                                News   Events  
                                      Law School News  
                                      On Legal Grounds Blog  
                                      ML-Memphis Law Magazine  
                                      Events Calendar  
                                      Communications Office  
                                  
                               
                                Directions  
                                Contact  
                                      Media Resource  
                                      General Contact Info  
                                      Faculty   Staff  
                                  
                               
                                Life in Memphis  
                                      Memphis-The City  
                                      Neighborhoods  
                                      Housing  
                                      The Arts Scene  
                                      Sports  
                                      The Food Scene  
                                      A Music Town  
                                      Directions and Maps  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply to Memphis Law  
                               Take the first step toward your legal education 
                            
                            
                                ABA Required Disclosures  
                               Information required by ABA Standard 509 Required Disclosures 
                            
                            
                                Alumni   Support  
                               Stay up to date with Memphis Law alumni 
                            
                            
                                Contact Us  
                               Questions about law school? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


