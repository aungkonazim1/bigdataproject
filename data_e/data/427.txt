Staff Senate - Staff Senate - University of Memphis    










 
 
 
     



 
    
    
    Staff Senate - 
      	Staff Senate
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Staff Senate
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Senators  
                          Clusters  
                          Meetings  
                          Events  
                          Topics of Discussion  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                         
                           	Staff Senate
                           	
                         
                      
                   
                   
                      
                         
                              
                            
                                
                            
                         
                         
                             
                         
                      
                      
                     		
                     
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  Staff Senate 
                                 
                               
                              
                               
                                 
                                   News 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  Staff Senators are elected by U of M staff members to serve as a representative within
                                    the university and college units. The Staff Senate serves as an advisory body to promote
                                    the general welfare of the University, as well as act as a communication vehicle for
                                    University staff members.
                                  
                                 
                                  Staff Senate meetings are held the third Thursday of every month in the Senate Chambers
                                    in the University Center.  Who is YOUR Staff Senator ??
                                  
                                 
                                  Board of Trustees Information 
                                 
                                  Stay informed on what's happening with the new Board of Trustees. The first board
                                    meeting is Friday, March 17. Visit their website at  http://www.memphis.edu/bot/  for more information.
                                  
                                 
                                  Staff Salary Survey Results 
                                 
                                  President Rudd requested recommendations on the proposed three percent (3%) salary
                                    pool distribution. Staff Senate prepared a survey and sent to all staff in March.
                                    View the  results.  
                                 
                                  Career Milestone Awards 
                                 
                                  The Career Milestone Awards Event and Reception was held on November 2. The event
                                    recognizes employees for their years of service to the university.  Video  |  Pictures from Event and Reception  
                                 
                                    
                                 
                               
                              
                               
                                 
                                  
                                    
                                     Staff Appreciation Day Photos 
                                    
                                     Enjoy the photos taken at the UofM SAD event in May and some past events.  Photo Gallery  
                                    
                                     We Did it! 
                                    
                                     We raised over $2,520 - well over our goal of $2000! Thank you to all of the Donors
                                       who put us over the TOP! (we have a couple of donations pending deposit!)
                                     
                                    
                                       
                                    
                                  
                                 
                               
                              
                            
                           
                         
                        
                      
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Bylaws  
                               Revised October 2014 
                            
                            
                                Committees  
                               And other representation… 
                            
                            
                                Make a Suggestion  
                               Or voice a concern through the suggestion box… 
                            
                            
                                President Rudd’s Blog  
                               Stay up to date on important university news… 
                            
                         
                      
                      
                                         
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


