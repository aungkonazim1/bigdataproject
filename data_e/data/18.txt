Current UofM Students - Current Students - University of Memphis  At University of Memphis, we take pride in putting our students first. Important resources to help you be successful and make the most of your college experience.  










 
 
 
     



 
    
    
    Current UofM Students - 
      	Current Students
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			UofM Current Students
                           	  
                   
                
             
          
       
       
          
             
                
                   
                      
                         
                             Home  
                            
                              	Current Students
                              	
                            
                         
                      
                      
                         
                            Current Students   
                            
                               
                                   Enrollment and Registration  
                                   Campus Life and Student Services  
                                   Student Organizations  
                                   Student Services  
                                   Majors and Minors  
                                   Helpful Links  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                        
                        
                         
 
      Register  
      Get Involved  
                         
                        
                        
                      
                      
                     		
                     
                     
                      Current UofM Students 
                     
                      At The University of Memphis, we take pride in putting our students first. You will
                        find important resources here to help you be successful and make the most of your
                        college experience.
                      
                     
                          Enrollment and Registration
                      
                     
                      
                        
                          Class Listings : Time and place class meets for each semester
                         
                        
                          Registrar's Office : Registration, Transcripts, Records, Veteran Services
                         
                        
                          Registration Center : Enrollment and registration information
                         
                        
                          Summer School : Info on summer courses, admissions and registration
                         
                        
                      
                     
                          Campus Life and Student Services
                      
                     
                      
                        
                          Adult and Commuter Students  
                        
                          Bookstore  
                        
                          Calendar of Events  
                        
                          Campus Map  
                        
                          Campus Safety  
                        
                          Career Counseling : Career exploration, planning, testing, assessment
                         
                        
                          Career Services : Resume writing, interview skills, job listings and internships
                         
                        
                          Daily Helmsman: Campus newspaper  
                        
                          Daycare  
                        
                          Disability Resources for Students  
                        
                          Eating on Campus  
                        
                          Health Center : Medical staff, vaccinations, health and wellness info
                         
                        
                         International Programs:  Study Abroad ,  International Student Office  
                        
                          Multicultural Affairs  
                        
                          Parking  
                        
                          Psychological Counseling  
                        
                          Recreation, Fitness, and Intramural Sports  
                        
                          Residence Life (ResLife) :  Housing 101 ,  Res Halls (Dorms)  
                        
                          Testing Center : Student testing, assessment, placement; credit by exam
                         
                        
                          Tiger Copy and Graphics  
                        
                          University Center  
                        
                      
                     
                          Student Organizations
                      
                     
                      
                        
                          Registered Student Organizations  
                        
                          Common Cents : Student Giving Program
                         
                        
                          Fraternities and Sororities  
                        
                          Leadership and Involvement : development, community service, events
                         
                        
                          Future Alumni of Memphis (FAM): Student Alumni Association  
                        
                          Graduate Student Association  
                        
                          Student Activities Council (SAC)  
                        
                          Student Government Association (SGA)  
                        
                      
                     
                          Majors and Minors
                      
                     
                      
                        
                          Undergraduate - College and Degree Programs  
                        
                          Graduate - Academic Degree Programs  
                        
                          Online Education: UofM Global  
                        
                          TRiO Student Success Programs (SSP) : Academic, financial, and counseling support for those in the federally-funded TRiO
                           program
                         
                        
                      
                     
                          Helpful Links
                      
                     
                      
                        
                          Ask TOM: Electronic storehouse of questions and answers.  
                        
                          Key Administrators  
                        
                          Right-to-Know Info  
                        
                          Student Handbook: Code of Student Rights and Responsibilities  
                        
                          Contact the Dean of Students  
                        
                      
                     
                     	
                      
                   
                
                
                   
                      
                         Current Students 
                         
                            
                                Enrollment and Registration  
                                Campus Life and Student Services  
                                Student Organizations  
                                Student Services  
                                Majors and Minors  
                                Helpful Links  
                            
                         
                      
                      
                        
                        
                                                
                        
                        
                      
                      
                        
                        
                                                
                        
                        
                      
                      
                        
                        
                                                
                        
                        
                      
                   
                
             
          
       
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/7/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


