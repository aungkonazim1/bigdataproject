James G. Murphy, Ph.D. - Department of Psychology - University of Memphis    










 
 
 
     



 
    
    
    James G. Murphy, Ph.D. - 
      	Department of Psychology
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Psychology
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Undergraduate  
                          Graduate  
                          People  
                          Centers  
                          Research  
                          Resources  
                      
                      
                         
                            People   
                            
                               
                                  
                                   Program Contacts  
                                   Faculty  
                                        
                                         Frank Andrasik  
                                        
                                         Karen Linville Baker  
                                        
                                         J. Gayle Beck  
                                        
                                         Kristoffer S. Berlin  
                                        
                                         Jeffrey S. Berman  
                                        
                                         Cheryl Bowers  
                                        
                                         Jason L. G. Braasch  
                                        
                                         Gina Caucci  
                                        
                                         Robert Cohen  
                                        
                                         Melloni N. Cook  
                                        
                                         Thomas K. Fagan  
                                        
                                         Randy G. Floyd  
                                        
                                         Arthur C. Graesser  
                                        
                                         David A. Houston  
                                        
                                         Kathryn H. Howell  
                                        
                                         Xiangen Hu  
                                        
                                         Stephanie Huette  
                                        
                                         Xu Jiang  
                                        
                                         Roger J. Kreuz  
                                        
                                         Deranda Lester  
                                        
                                         Brook A. Marcks  
                                        
                                         Meghan McDevitt-Murphy  
                                        
                                         Elizabeth B. Meisinger  
                                        
                                         Andrew W. Meyers  
                                        
                                         James G. Murphy  
                                        
                                         Robert A. Neimeyer  
                                        
                                         Andrew M. Olney  
                                        
                                         Philip I. Pavlik Jr.  
                                        
                                         Leslie A. Robinson  
                                        
                                         M. David Rudd  
                                        
                                         Helen J. K. Sable  
                                        
                                         Nicholas W. Simon  
                                        
                                         Idia B. Thurston  
                                        
                                         James P. Whelan  
                                        
                                         William H. Zachry  
                                        
                                         Jia Wei Zhang  
                                     
                                  
                                   Staff  
                                         Staff Personnel  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of Psychology
                              	  
                          
                              	People
                              	  
                         James G. Murphy, Ph.D. 
                      
                   
                   
                       
                      
                      
                        
                        	
                        
                        		  
                         
                           
                           
                            
                              
                              
                                                                   
                                 
                                 
                                  
                                 
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                    James G. Murphy, Ph.D.
                                    
                                  
                                 
                                 
                                  
                                    
                                    Professor, Director of Clinical Training
                                    
                                  
                                 
                                 
                                  
                                    
                                     
                                       
                                        Phone 
                                       
                                        
                                          
                                          901.678.2630
                                          
                                        
                                       
                                     
                                    
                                     
                                       
                                        Email 
                                       
                                        
                                          
                                           jgmurphy@memphis.edu 
                                           
                                       
                                     
                                    
                                     
                                       
                                        Fax 
                                       
                                        
                                          
                                          901.678.2579
                                          
                                        
                                       
                                     
                                    
                                  
                                 
                                 
                                  
                                    
                                     
                                       
                                        Office 
                                       
                                        
                                          
                                          Psychology Building, Room 348
                                          
                                        
                                       
                                     
                                    
                                     
                                       
                                        Office Hours 
                                       
                                        
                                          
                                          Contact
                                          
                                        
                                       
                                     
                                    
                                  
                                 
                                 
                                  
                                    										
                                    
                                    
                                      Website  
                                    
                                      CV  
                                    
                                      Google Scholar  
                                                                            
                                    
                                  
                                 
                               
                              
                              
                            
                           			 
                           			  
                           
                           
                             Education   Ph.D., Auburn University M.S., Auburn University B.A., Seton Hall University   Research Interests  
                           
                            
                              
                               Addictive and Health Risk Behaviors 
                              
                               Behavioral Economic and Decision Making Processes Related to Addiction, and Addictive
                                 Behavior Change  
                               
                              
                               Brief Interventions with Young Adult Populations including College Students and Military
                                 Veterans
                               
                              
                               Mechanisms of Health and Addictive Behavior Change 
                              
                            
                            Selected Recent Publications (* denotes a mentored student author) 
                           
                            
                              
                               Murphy, J. G., *Dennhardt, A. A., *Skidmore, J. R., Borsari, B., Barnett, N. P., Colby,
                                 S. M.   Martens, M. P. (2012). A randomized controlled trial of a behavioral economic
                                 supplement to brief motivational interventions for college drinking.  Journal of Consulting and Clinical Psychology, 80,  876-886. doi:10.1037/a0028763
                               
                              
                               *Dennhardt, A. A.,   Murphy, J. G. (2013). Prevention and treatment of college student
                                 drug use: A review of the literature.  Addictive Behaviors, 38,  2607-2618.
                               
                              
                               Bickel, W., Johnson, M., Koffarnus, M. MacKillop, J., Murphy, J. G. (2014). The behavioral
                                 economics of substance use disorders: Reinforcement pathologies and their repair.
                                  Annual Review of Clinical Psychology, 10,  20.1-20.37. doi:10.1146/annurev-clinpsy-032813-153724
                               
                              
                               McDevitt-Murphy, M. E., Murphy, J. G., *Williams, J. L., *Monahan, C. J., *Bracken-Minor,
                                 K. L.,   *Fields, J. A. (2014). Randomized controlled trial of two brief alcohol interventions
                                 for oef/oif veterans.  Journal Of Consulting And Clinical Psychology,  doi:10.1037/a0036714
                               
                              
                               *Buscemi, J., Murphy, J. G., Berlin, K.S.,   Raynor, H.A. (2014). A behavioral economic
                                 analysis of changes in food-related and food-free reinforcement during weight loss
                                 treatment.  Journal of Consulting and Clinical Psychology, 82  (4). 659-669 doi:10.1037/a0036376
                               
                              
                               *Skidmore, J. R., Murphy, J. G.   Martens, M. P. (2014). Behavioral economic measures
                                 of alcohol reward value as problem severity indicators in college students.  Experimental and Clinical Psychopharmacology, 22  (3), 198-210. doi: 10.1037/a0036490
                               
                              
                               *Teeters, J. B., *Pickover, A., M., *Dennhardt, A. A., Martens, M. P,   Murphy, J.
                                 G. (2014). Alcohol demand is associated with driving after drinking among college
                                 student binge drinkers.  Alcoholism: Clinical and Experimental Research, 38  (7), 2066-2072. doi:10.1111/acer.12448
                               
                              
                               *Meshesha, L. Z., *Dennhardt, A. D.,   Murphy, J. G. (2015). Polysubstance Use is
                                 Associated with Deficits in Substance-Free Reinforcement in College Students.  Journal of Studies on Alcohol and Drugs, 76 (1), 106-116. doi:10.15288/jsad.76.1.106
                               
                              
                               *Dennhardt, A. D. *Yurasek, A, A,,   Murphy, J. G. (2015). Change in delay discounting
                                 and substance reward value following a brief alcohol and drug use intervention.  Journal of the Experimental Analysis of Behavior, 103 (1), 125-140. doi:10.1002/jeab.121
                               
                              
                               *Teeters, J. B.   Murphy, J. G. (2015). The behavioral economics of driving after
                                 drinking among college drinkers.  Alcoholism: Clinical and Experimental Research, 39,  896-904. doi:10.1111/acer.12695
                               
                              
                               *Teeters, J. B., Borsari, B., Martens, M. P., Murphy, J. G. (in press). Brief motivational
                                 interventions are associated with reductions in alcohol-impaired driving among college
                                 drinkers.  Journal of Studies on Alcohol and Drugs.  
                              
                               *Tripp, J., Meshesha, L. Z., Pickover, A. M., Teeters, J., McDevitt-Murphy,   Murphy,
                                 J. G. (in press). PTSD Status Predicts Alcohol Related Consequences, Alcohol Demand,
                                 and Drug Use in College Students.  Experimental and Clinical Psychopharmacology.  
                              
                               Murphy, J. G., *Dennhardt, A. D., *Yurasek, A. M., *Skidmore, J. R., Martens, M. P.,
                                 MacKillop, J.   McDevitt-Murphy, M. E. (2015). Behavioral economic predictors of brief
                                 alcohol intervention outcomes.  Journal of Consulting and Clinical Psychology.  http://dx.doi.org/10.1037/ccp0000032
                               
                              
                               *Yurasek, A.M., *Dennhardt, A. A.,   Murphy, J. G. (in press). A randomized controlled
                                 trial of a behavioral economic intervention for alcohol and marijuana use.  Experimental and Clinical Psychopharmacology.  
                              
                               *Pickover, A. M., Correia, C., Messina, B., Garza, K. B.   Murphy, J. G. A behavioral
                                 economic measure of prescription drug use severity among college substance users.
                                 (accepted for publication pending revisions).  Experimental and Clinical Psychopharmacology.  
                              
                            
                           
                           
                         
                        								
                        
                        
                        
                        
                        	
                      
                      
                   
                
                
                   
                      
                         People 
                         
                            
                               
                                Program Contacts  
                                Faculty  
                                     
                                      Frank Andrasik  
                                     
                                      Karen Linville Baker  
                                     
                                      J. Gayle Beck  
                                     
                                      Kristoffer S. Berlin  
                                     
                                      Jeffrey S. Berman  
                                     
                                      Cheryl Bowers  
                                     
                                      Jason L. G. Braasch  
                                     
                                      Gina Caucci  
                                     
                                      Robert Cohen  
                                     
                                      Melloni N. Cook  
                                     
                                      Thomas K. Fagan  
                                     
                                      Randy G. Floyd  
                                     
                                      Arthur C. Graesser  
                                     
                                      David A. Houston  
                                     
                                      Kathryn H. Howell  
                                     
                                      Xiangen Hu  
                                     
                                      Stephanie Huette  
                                     
                                      Xu Jiang  
                                     
                                      Roger J. Kreuz  
                                     
                                      Deranda Lester  
                                     
                                      Brook A. Marcks  
                                     
                                      Meghan McDevitt-Murphy  
                                     
                                      Elizabeth B. Meisinger  
                                     
                                      Andrew W. Meyers  
                                     
                                      James G. Murphy  
                                     
                                      Robert A. Neimeyer  
                                     
                                      Andrew M. Olney  
                                     
                                      Philip I. Pavlik Jr.  
                                     
                                      Leslie A. Robinson  
                                     
                                      M. David Rudd  
                                     
                                      Helen J. K. Sable  
                                     
                                      Nicholas W. Simon  
                                     
                                      Idia B. Thurston  
                                     
                                      James P. Whelan  
                                     
                                      William H. Zachry  
                                     
                                      Jia Wei Zhang  
                                  
                               
                                Staff  
                                      Staff Personnel  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Psychology Graduate Programs Application  
                               Click on link to Apply to the Department of Psychology Graduate Program 
                            
                            
                                Academic Advising   Resource Center (AARC)  
                                The AARC provides advising to students helping them make the most of their undergraduate
                                 education at the UofM.
                               
                            
                            
                                The Psychological Services Center  
                               PSC provides general outpatient psychotherapeutic and psychological assessment services
                                 to individuals and families
                               
                            
                            
                                Teaching Take-Out  
                               This website is a resource for busy teachers who want to enrich their classes while
                                 preserving the time they need for research and other important professional activities.
                               
                            
                         
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/17/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


