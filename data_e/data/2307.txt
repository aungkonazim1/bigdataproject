Undergraduate Program Degree Requirements - Department of Anthropology - University of Memphis    










 
 
 
     



 
    
    
    Undergraduate Program Degree Requirements - 
      	Department of Anthropology
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Anthropology
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Undergraduate  
                          Graduate  
                          People  
                          Research  
                          Resources  
                          News   Events  
                      
                      
                         
                            Undergraduate Resources   
                            
                               
                                  
                                   Undergraduate Catalog  
                                   Undergraduate Admissions  
                                   Degree Requirements and Course Descriptions  
                                   Courses Required for B.A. Degree  
                                   Registration Services  
                                   Supervised Course Agreement  
                                   University Calendars  
                                   Declare Anthropology Major  
                                   Undergraduate Alumni  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of Anthropology
                              	  
                          
                              	Undergraduate
                              	  
                         Undergraduate Program Degree Requirements 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Undergraduate Program Degree Requirements and Course Description 
                     
                      Anthropology Major (B.A.) 
                     
                      A. University General Education Program (41 hours) 
                     
                      See the  Undergraduate Catalog  for the University General Education Program requirements.
                      
                     
                      B. College and Degree (B.A.) Requirements (18-19 hours) 
                     
                      The  College and Bachelor of Arts requirements  are in addition to the University General Education Program requirements and are
                        listed in the Undergraduate Catalog.
                      
                     
                      C. The Major (30 hours) 
                     
                      Completion of 30 semester hours in anthropology courses including ANTH 1100, 1200,
                        3200, 4065; and 18 additional semester hours, at least 6 of which must be at the 4000
                        level.
                      
                     
                      D. Electives 
                     
                      Electives may be chosen to bring the total number of hours to 120. 
                     
                      E. Honors Program 
                     
                      The Department of Anthropology offers an honors program in anthropology to the superior
                        anthropology student who desires a more intensive approach to and knowledge of the
                        discipline, a deeper understanding of research skills, and an opportunity for original
                        criticism and analysis. To be eligible a student must (a) have the approval of the
                        Departmental Honors Committee, (b) be an anthropology major, (c) have attained at
                        least junior standing, (d) have completed successfully ANTH 1100, 1200, and one upper-division
                        anthropology course, (e) maintained a minimum cumulative grade point average of 3.00
                        in overall studies, (f) maintained a cumulative grade point average of at least 3.25
                        in anthropology, (g) completion of four honors courses in anthropology (at least one
                        of which must be an independent study culminating in a research paper/thesis or research
                        report) and a minimum of six hours in honors courses outside anthropology. Details
                        of the program are available at the Department of Anthropology office.
                      
                     
                      Anthropology Minor 
                     
                      Completion of 18 semester hours in anthropology courses, including ANTH 1100, 1200,
                        3200, and 4065.
                      
                     
                      Course Descriptions 
                     
                      Click  here  for more information.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Undergraduate Resources 
                         
                            
                               
                                Undergraduate Catalog  
                                Undergraduate Admissions  
                                Degree Requirements and Course Descriptions  
                                Courses Required for B.A. Degree  
                                Registration Services  
                                Supervised Course Agreement  
                                University Calendars  
                                Declare Anthropology Major  
                                Undergraduate Alumni  
                            
                         
                      
                      
                      
                         
                            
                                Undergrads:   
                               Declare Anthropology your major! 
                            
                            
                                Join the Graduate Program  
                               Learn about applied and engaged anthropology! 
                            
                            
                                Make a Gift  
                               Support the programs and research of the Department of Anthropology 
                            
                            
                                Contact Us  
                               Main office location and numbers, undergraduate and graduate advising 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/15/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


