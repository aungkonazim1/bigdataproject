Deleting discussion forums, topics, threads, and posts | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Deleting discussion forums, topics, threads, and posts | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Discussions    »    Creating and managing discussions   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Deleting discussion forums, topics, threads, and posts                       
        
        
       
        
     
              
	  Delete forums or topics
 

  
		On the Discussions List page, click    Delete  from the context menu of the forum or topic you want to delete.
	 
	 
		Click  Yes .
	 
  
	  Delete a thread from a topic
 

 
	Click    Delete Thread  from the context menu of the thread you want to delete.
 

 
	  Delete a post from a thread
 

 
	 Important  When you delete a post, the system also deletes any replies to that post.
 

 
	Click    Delete Post  from the context menu of the post you want to delete.
 
     Audience:     Instructor       

    
           

                   ‹ Copying a discussion forum, topic, thread, or post 
        
                   up 
        
                   Reordering discussion forums and topics › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Discussions  
  
      Participating in discussions    Following discussions    Creating and managing discussions    Creating discussion forums and topics    Discussion forum and topic restrictions    Topic assessment    Topic objectives    Editing a discussion forum or topic    Copying a discussion forum, topic, thread, or post    Deleting discussion forums, topics, threads, and posts    Reordering discussion forums and topics    Restoring a deleted discussion forum, topic, thread, or post      Monitoring discussions    
                  
           
         

       
     

    
    
    
   
 
   
 
