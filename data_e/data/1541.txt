Graduate Student Rights and Responsibilities - GSA - University of Memphis    










 
 
 
     



 
    
    
    Graduate Student Rights and Responsibilities - 
      	GSA
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Graduate Student Association
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Members  
                          Committees  
                          Calendar  
                          Contact Us  
                          Graduate School  
                      
                      
                         
                            About    
                            
                               
                                  
                                   Graduate Student Rights and Responsibilities  
                                   Graduate Student Association Constitution  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	GSA
                              	  
                          
                              	About
                              	  
                         Graduate Student Rights and Responsibilities 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Graduate Student Rights and Responsibilities 
                     
                      PREAMBLE 
                     
                      Graduate students play an integral part in the ability of the university campus to
                        provide the breadth and quality of educational experiences expected of a Carnegie
                        RU/HA (Research University with Higher Levels of Research Activity) institution. Graduate
                        student supplement and complement the teaching and research activities of the faculty,
                        and in doing so allow the faculty to engage more students in individualized instructional
                        opportunities. They also provide the institution with an ability to more rapidly adjust
                        the educational opportunities to meet student needs and preferences that can be accommodated
                        for by the faculty alone. It is important that the campus community recognize and
                        support the important role played by graduate students in enabling the campus to address
                        its research, teaching, and outreach mission.
                      
                     
                      A major purpose of graduate education at The University of Memphis is to instill in
                        each student an understanding of and capacity for scholarship, independent judgment,
                        academic rigor, and intellectual honesty. Graduate education is an opportunity for
                        the student to develop into a professional. Graduate research and teaching assistantships
                        offer an "apprenticeship" experience in the academic profession as well as financial
                        support. It is the joint responsibility of faculty and graduate students to work together
                        to foster these ends through relationships that encourage freedom of inquiry, mentoring,
                        demonstrate personal and professional integrity, and foster mutual respect. This shared
                        responsibility with faculty extends to all of the endeavors of graduate students,
                        as students and as members of the larger academic community.
                      
                     
                      Each right of an individual places a reciprocal duty upon others, the duty to permit
                        the individual to exercise the right. The graduate student, as a member of the academic
                        community, has both rights and duties. Within that community, the graduate student's
                        most essential right is the right to learn. The University of Memphis has a duty to
                        provide for the graduate student those privileges, opportunities, and protections
                        which best promote the learning process in all its aspects. The graduate student also
                        has duties to other members of the academic community, the most important of which
                        is to refrain from interference with those rights of others which are equally essential
                        to the purposes and processes of the University. While this document is not legally
                        binding, violations of the standards contained within may serve as grounds for filing
                        grievances through the existing University procedures. It is also acknowledged that
                        situations may arise which this document is not able to foresee or prevent, and in
                        such cases this document should be viewed as a guideline and as a set of minimum standards.
                        Some of the standards contained within are already specified as rights or responsibilities
                        of students in existing official university literature and are restated here to ensure
                        that graduate students are aware of such standards.
                      
                     
                      The following principles illustrate what graduate students should expect from their
                        programs and what programs should expect from their graduate students to help achieve
                        this excellence:
                      
                     
                      I. Graduate Students Have The Right: 
                     
                      
                        
                         To be respected as individuals and as developing professionals. 
                        
                         To matriculate through the degree programs in a timely fashion. 
                        
                         To an advisor. 
                        
                         To be provided with clear degree requirements. 
                        
                         To have progress towards their degree objectively evaluated by the appropriate faculty
                           members.
                         
                        
                         To reasonable confidentiality in communications with professors, defined as:
                           
                            
                              
                               When the graduate student had an expectation of confidentiality AND 
                              
                               When a reasonable person in the same situation would have an expectation of confidentiality. 
                              
                            
                           
                         
                        
                         To only perform tasks that are related to their professional/academic development
                           or within the confines of a contractual obligation or relevant assistantship duties.
                         
                        
                         Not be discriminated against on the basis of race, gender, disability, religion, socioeconomic
                           status, age, ethnicity, or sexual orientation.
                         
                        
                         To due process regarding grade appeals, grievances, or issues before Judicial Affairs. 
                        
                         To student representation in decisions regarding curriculum and program development. 
                        
                         To be appropriately recognized for significant contributions to the research process
                           and publications.
                         
                        
                         To sufficient supervision when appointed as a Graduate Teaching Assistant or Graduate
                           Research Assistant.
                         
                        
                         To petition for a change in advisor/major professor or thesis/doctoral committee membership
                           at any point in their academic career without incurring any undue future negative
                           academic/social consequences as a result of this change, with the exception of academic
                           scheduling.
                         
                        
                         To be informed in a timely fashion about a change in their advisor or committee members'
                           status as it relates to their thesis/dissertation completion or research agenda.
                         
                        
                         To expect professional interaction from faculty, staff, and peers. 
                        
                      
                     
                      II. Graduate Students Have The Responsibility: 
                     
                      
                        
                         To read pertinent catalog/website information and college/departmental handbooks regarding
                           successful completion of courses, forms (e.g., intent to graduate and candidacy),
                           composition of masters and doctoral committees, comprehensive and qualifying examinations,
                           and theses/dissertation defenses.
                         
                        
                         To acknowledge the contributions of faculty members in their scholarly presentations
                           and publications.
                         
                        
                         To conduct themselves in a manner befitting their professional area of study 
                        
                         To matriculate with integrity through the degree in a timely fashion. 
                        
                         To take the initiative to ask questions and access information about degree requirements,
                           program completion, and financial arrangements.
                         
                        
                         To inform appropriate faculty members in a timely fashion about any changes in program
                           status, advisor, or committee membership.
                         
                        
                         To familiarize themselves with the university and college level student codes of conduct. 
                        
                         To follow all University policies and procedures when conducting research, including
                           those specified by the Institutional Review Board and the Institutional Animal Care
                           and Use Committee.
                         
                        
                         To interact with faculty in a professional and civil manner. 
                        
                         To behave in a professional and appropriate manner in class. 
                        
                      
                     
                      Approved 03/04/05 
                     
                     
                     	
                      
                   
                
                
                   
                      
                         About  
                         
                            
                               
                                Graduate Student Rights and Responsibilities  
                                Graduate Student Association Constitution  
                            
                         
                      
                      
                      
                         
                            
                                Travel Funding  
                               Request funds for travel with the Travel Fund application. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


