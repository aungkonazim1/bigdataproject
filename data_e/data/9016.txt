Importing a learning object into a quiz or a question library | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Importing a learning object into a quiz or a question library | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Repository    »    Searching for and using learning resources   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Importing a learning object into a quiz or a question library                       
        
        
       
        
     
              
	 Note    Linking to quizzes is not available. You can only import quizzes and quiz questions from Learning Repository.
 

 
	When you want to import a learning object as a quiz, quiz section, or question from an LOR you must start in the course that you want the quiz, quiz section, or question to exist in.
 

  
		On the course homepage, click     Quizzes  on the navbar.
	 
	 
		Start from one of the following locations:
		  
				On the Manage Quizzes page, click     Add Learning Object  from the More Actions button.
			 
			 
				On the Question Library page, click  Import , and then select   Learning Repository (LOR)  from the Import Source drop-down list. Click  Add Learning Object .
			 
		  
	 
		Click  Search  to find the learning object you want.
	 
	 
		Choose the learning object from the search results page. Click  Next .
	 
      Audience:     Instructor       

    
           

                   ‹ Retrieving a learning object as a new topic 
        
                   up 
        
                   Searching for and retrieving a collection › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Learning Repository  
  
      Main Learning Repository concepts    Searching for and using learning resources    Searching Learning Repository    Browsing Learning Repository s content    Working with Learning Repository search results    Viewing files in a learning object    Adding reviews to a learning object or file    Viewing reviews of a learning object or file    Retrieving learning objects with navigation    Retrieving learning objects without navigation    Retrieving a learning object as a new topic    Importing a learning object into a quiz or a question library    Searching for and retrieving a collection    Saving a learning object to your PC    Using RSS feeds in Learning Repository      Publishing to Learning Repository    Managing learning objects    
                  
           
         

       
     

    
    
    
   
 
   
 
