Competencies reports | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Competencies reports | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Insights    »    Standard reports   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Competencies reports                       
        
        
       
        
     
              
	Competency reports provide a comprehensive analysis of the structure of competencies and achievement rates over time. You can filter the results to show specific results for Competencies, Learning Objectives, and Activities for a specific Org Unit, Semester, or Department.
 

  
		 Competency Achievements 
	 
	 
		 Individual Progress on Competencies Over Time 
	 
	 
		 Progress on Competencies Across Courses 
	 
	 
		 Progress on Competencies by Course 
	 
  
	  Competency Achievements
 

 
	Displays Competency/Learning Objective/Activity Achievement results for a selected org unit. This report is available in the Insights Portal and the Insights Console.
 

 
	Drill-down and hyperlinks
 

 
	Competency Achievements reports allow you to drill up to the root level parent Competency associated with a specific Competency Object when viewing the reports in the Insights Console.
 

 
	  

 
	  Individual Progress on Competencies Over Time
 

 
	Displays individual learner progress on competency achievement over time. Data is grouped by Course, followed by Competency, Learning Objective, and Assessment Activity. This report is available in the Insights Portal and the Insights Console.
 

 
	Use the Individual Progress on Competencies Over Time report to address the question "How do student achievement rates vary across courses?" This report enables educators to compare student performance against common competencies across courses.
 

 
	  

 
	  Progress on Competencies Across Courses
 

 
	Displays progress for one or more competencies across all courses that utilize the competency. Data is grouped by Competency, Learning Objective, and Assessment Activity, and shows a summary of progress for each course. This report is available in the Insights Portal and the Insights Console.
 

 
	Use the Progress on Competencies Across Courses report to address the question "How do student achievement rates vary across courses?" This report enables educators to compare student performance against common competencies across courses.
 

 
	  

 
	  Progress on Competencies by Course
 

 
	Displays overall progress on Competencies, Learning Objectives, and Assessment Activities within one or more courses. Data is grouped by course, followed by Competency, Learning Objective, and Assessment Activity. This report is available in the Insights Portal and the Insights Console.
 

 
	Use the Progress on Competencies by Course report to address the question "How are students within a course offering progressing towards the achievement of expected competencies?" The report enables educators to compare achievements against set targets and identify specific areas where learners might be having difficulties.
 

 
	  
     Audience:     Instructor       

    
           

                   ‹ Attendance reports 
        
                   up 
        
                   Corporate reports › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Insights   
  
      Using Insights Portal    Standard reports    Standard reports    Accreditation reports    Attendance reports    Competencies reports    Corporate reports    Engagement reports    Enrollments reports    Quizzing reports    RiskAnalysis reports    Rubrics reports    Surveys reports    Tool Access reports    User Login reports      Data Mining reports    Understanding the Student Success System    
                  
           
         

       
     

    
    
    
   
 
   
 
