Creating a group in Wiggio | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Creating a group in Wiggio | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Integrations    »    Wiggio    »    Creating and using groups in Wiggio   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Creating a group in Wiggio                       
        
        
       
          
     
           Printer-friendly version       
	 Note  When you create a group, you are automatically the group's administrator and owner.
 

  
		Click the   Create a group  icon.
	 
	 
		On the Create a group page, enter a  Name  and  Password .
	 
	 
		Select how you want group members to receive emails:
		  
				 Each post by email 
			 
			 
				 Each post by SMS or email 
			 
			 
				 Daily summary of posts by email 
			 
			 
				 No mail 
			 
		  
	 
		Click  Create .
	 
  
	Once you have created a group, you can further customize it's settings. See  Editing settings for a group in Wiggio  for more information
 

 
	See also
 

  
		 Editing settings for a group in Wiggio 
	 
      Audience:    Learner      

    
           

                   ‹ Creating and using groups in Wiggio 
        
                   up 
        
                   Adding members to a group in Wiggio › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Wiggio  
  
      Wiggio basics    Creating and using groups in Wiggio    Creating a group in Wiggio    Adding members to a group in Wiggio    Creating a subgroup in Wiggio     Adding a subgroup to a conversation in Wiggio    Finding your group email address in Wiggio    Finding your group password in Wiggio    Finding your group administrator in Wiggio    Sharing a group in Wiggio    Joining a group in Wiggio    Leaving a group in Wiggio    Updating group email notification settings in Wiggio      Managing groups in Wiggio     Adding resources to Wiggio    Creating and using folders in Wiggio    Using virtual meetings in Wiggio    Using polls in Wiggio    Using to-do lists in Wiggio    Managing events in Wiggio    Importing and exporting calendars in Wiggio    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
