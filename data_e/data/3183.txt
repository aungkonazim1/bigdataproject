Salary Redistribution Request Form University of Memphis Salary Redistribution Request Form  
 
 Salary Redistribution Request Form 
 
 
 




 
 
 
   
       
      Salary Redistribution Request 
        General Online Help  
       Use this form for retroactive redistribution of salary only. 
      Submit Completed Grant Form to Grants & Contracts Accounting, Administration Bldg., Room 263. 
      Submit Completed Non-Grant Form to Financial Reporting, Administration Bldg., Room 275.       
     
   
 

 
 
	 
         
        	 
            	 EMPLOYEE INFORMATION 
                                 	
                     Date 
                     
                 
             
        	 
            	 
                     Full Name 
                     
                 
                 
                     Pay ID (select one) 
                     MN
                     BW
                  
               
               
                  
                     Banner ID 
                     
                  
                  
                     Position Number 
                     
                   
               
            
     
     
    	 
        	 CURRENT 
         
         
        	 Index 
        	 Fund 
        	 Orgn 
        	 Account 
        	 Program 
        	 Activity 
        	 Percent 
        	 Amount 
        	 Pay Period * 
        	 Earnings Code ** 
                                                                                                                                              
     
     
         CHANGE TO 
     
     
        	 Index 
        	 Fund 
        	 Orgn 
        	 Account 
        	 Program 
        	 Activity 
        	 Percent 
        	 Amount 
        	 Pay Period * 
        	 Earnings Code ** 
                                                                                                                                              
 
 
	 
         * Pay Period Numbers range from  1-26 for Biweekly and 1-12 for Monthly.  
          Go to  http://www.memphis.edu/payroll/schedules.php  for biweekly and monthly payroll schedules. 
         ** Earnings Codes can be found at  http://bf.memphis.edu/spectrum/hr/earnings.php . 
          If labor distribution reports reflect multiple account code entries for one pay period for an individual, enter each on a separate line. 
        Request for salary redistribution must be within 90 calendar days of initial charge, and occur in current Fiscal Year. 
        If redistribution affects another account you do not have signature authority on, you must obtain approval from all financial managers affected prior to submitting request.  
	 
	 
    	 Explain why salary was charged incorrectly.  Provide reason for salary redistribution, and how costs are allowable and allocable to the project:  
          
         
	 
    	  Approvals:  
         Signature of Chair  required when redistribution decreases Ledger 5 and increases Ledger 2, due to changes in recovery. 
		 Signature of Chair and Dean  required when redistribution exceeds 90 calendar days of initial charge. 
         Signature of Principal Investigator and Grants   Contracts Accounting  required when salary redistribution request affects restricted Ledger 5 accounts.  
     
	 
         
        	 
            	 
                	 Department Chair: 
                	 
                 
                 
                	 Dean: 
                	 
                 
           
              
             	  Signature  
                  Date  
                  
                  Signature  
                  Date  
                  
              
              
                 
                	 Principal Investigator: 
                	 
                 
                 
                	 Grants   Contracts Accounting: 
                	 
                 
             
              
             	  Signature  
                  Date  
                  
                  Signature  
                  Date  
                  
              
         
     By signing above, the Principal Investigator certifies the cost transferred is an appropriate expenditure for the sponsored agreement charged, and the expenditure complies with the terms and restrictions governing the sponsored agreement. 
     
 
 
 
   An Equal Opportunity/Affirmative Action University  
 
 
