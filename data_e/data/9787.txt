Jennifer P. Taylor, Au.D., CCC-A - School of Communication Sciences and Disorders - University of Memphis    










 
 
 
     



 
    
    
    Jennifer P. Taylor, Au.D., CCC-A  - 
      	School of Communication Sciences and Disorders
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			School of Communication Sciences and Disorders
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          People  
                          Research Areas  
                          Student Orgs  
                          Resources  
                          News  
                      
                      
                         
                            People   
                            
                               
                                  
                                   Staff  
                                   PhD Students  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	School of Communication Sciences and Disorders
                              	  
                          
                              	People
                              	  
                         Jennifer P. Taylor, Au.D., CCC-A  
                      
                   
                   
                       
                      
                     
                     	
                     
                     		  
                      
                        
                        
                         
                           
                           
                                                                
                              
                              
                               
                              
                              
                            
                           
                           
                            
                              
                               
                                 
                                 Jennifer P. Taylor, Au.D., CCC-A 
                                 
                               
                              
                              
                               
                                 
                                 Clinical Associate Professor, School of Communication Sciences and Disorders 
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Phone 
                                    
                                     
                                       
                                       901.678.5800
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Email 
                                    
                                     
                                       
                                       jptaylr2@memphis.edu
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Fax 
                                    
                                     
                                       
                                       901.525.1282
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Office 
                                    
                                     
                                       
                                       4055 N. Park Loop, Room 3017F
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Office Hours 
                                    
                                     
                                       
                                       By appointment
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 										
                                 
                                 
                                   Website  
                                 
                                   CVS  
                                                                         
                                 
                               
                              
                            
                           
                           
                         
                        			 
                        			  
                        
                        
                         About Jennifer Taylor 
                        
                         Dr. Taylor joined the clinical faculty in 2003.  Dr. Taylor is the Director of Clinical
                           Services in Audiology and a Clinical Associate Professor for the School of Communication
                           Sciences and Disorders and provides audiological services and supervision at the University
                           of Tennessee Boling Center for Developmental Disabilities. Her clinical interests
                           include pediatric diagnostics and rehabilitation. She is a project associate for a
                           Department of Education personnel preparation grant, "Working with Interpreters" focusing
                           on training students to work with interpreters providing clinical services. Dr. Taylor
                           serves as co-advisor for the NSSLHA and SAA chapters at Memphis. She is also active
                           in volunteer organizations in the Memphis area.
                         
                        
                         Education 
                        
                         
                           
                            Au.D., 2003, University of Florida 
                           
                            M.S., Audiology, 2000, University of Mississippi 
                           
                            B.A., Communicative Disorders, 1998, University of Mississippi 
                           
                         
                        
                         Experience 
                        
                         Professional 
                        
                         Director, Audiology Clinical Services - The University of Memphis 
                        
                         Honors and Awards 
                        
                         Award of Appreciation - Council of Academic Programs in Communication Sciences and
                           Disorders
                         
                        
                         Clinical and Scholarly Activities 
                        
                         Clinical Interests 
                        
                         Diagnostics, Pediatric Testing/ Hearing Screenings, Pediatric Aural Rehabilitation/
                           Audiologic Rehabilitation, Adult Aural Rehabilitation/ Audiologic Rehabilitation
                         
                        
                         Teaching Interests 
                        
                         Clinical Concepts 
                        
                         Public Service 
                        
                         President, Junior League of Memphis 
                        
                         Campus Organizations 
                        Co-Advisor of National Student Speech-Language-Hearing Association- University of
                        Memphis Chapter Co-Advisor of Student Academy of Audiology-University of Memphis Chapter 
                        
                         Resources 
                        
                         Advising 
                        
                           
                        
                         Service 
                        
                         
                           
                            HIPAA Privacy Officer - June 2013-present 
                           
                            Conference Planning Committee Member-at-Large, Council of Academic Programs in Communication
                              Sciences and Disorders
                            
                           
                         
                        
                        
                      
                     								
                     
                     
                     
                     
                     	
                      
                   
                
                
                   
                      
                         People 
                         
                            
                               
                                Staff  
                                PhD Students  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now  
                               Want to pursue a degree in Communication Sciences and Disorders?  Find out how to
                                 apply.
                               
                            
                            
                                Support the School  
                               Help support the School with the funding of various initiatives. 
                            
                            
                                Memphis Speech and Hearing Center  
                               Learn about our in-house clinical training facility. 
                            
                            
                                Contact Us  
                               Need more information about our School? 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


