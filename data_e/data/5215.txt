Integrations | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 Integrations | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Integrations                       
        
        
       
          
     
           Printer-friendly version       
	Desire2Learn partners with third-party service providers to create integrations that address client needs. This page provides help for Desire2Learn's existing integration products.
 

 
	
	 
		Assignment Grader
	 

	  
			 D2L Assignment Grader for iOS Instructor Guide 
		 
		 
 Brightspace Assignment Grader for Android Instructor Guide
		  
	 
	

	 
		Banner Grades
	 
	
		 
		 
			 Banner Grades Instructor Guide 1.0 
		 
	 
	

	 
		Binder
	 

	  
			 Binder 
		 
	 
	  

	 
		Course Catalog
	 

	  
			 Course Catalog 
		 
	 
	

	 
		Digital Media Services
	 

	 
		 
			 Digital Media Services User Guide 3.0 
		 
	 
	

	 
		Google Apps
	 

	 
		 
			 Google Apps 1.3 - Learner Guide User 
		 
	 
	
 

 
	

	 
		Integration Pack for Synchronous Communication Tools (Online Rooms)
	 

	  
			 Online Rooms User Guide 2.2 
		 
		 
			

	 
		ReadSpeaker docReader
	 

	  
			 ReadSpeaker docReader Administrator Guide 
		 
	 
	 

	 
		Wiggio
	 

	  
			 Wiggio 
		 
	  

 
	 
 
     Audience:    Learner        

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                        
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
