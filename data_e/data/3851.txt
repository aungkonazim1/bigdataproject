December 2016 &#8211; On Legal Grounds | Memphis Law Events &amp; Announcements    
 

 
	 
	 
	 
	 
	
	 December 2016   On Legal Grounds | Memphis Law Events   Announcements 
 

 
 
 
 
		
		
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 










 
 
  
 
 
    

 

 

 

	 
	
				
		 
			 
				
				 
					     
					 On Legal Grounds | Memphis Law Events   Announcements 									 
				
									 
						    
						   
						    Student Organizations 
 
	  AWA  
	  BLSA  
	  Bus.   Tax Law Society  
	  Christian Legal Society  
	  Federalist Society  
	  Health Law Society  
	  HLSA  
	  Honor Council  
	  ILS  
	  Law Review  
	  Memphis Law +1  
	  Moot Court Board  
	  OutLaw  
	  PAD  
	  PALS  
	  SELS  
	  SBA  
	  SGA  
	  Street Law  
	  TACDL  
 
 
  Law School Announcements 
 
	  Academic Affairs 
	 
		  Academic Affairs Calendar  
	 
 
	  Academic Success Program 
	 
		  Academic Success Program Calendar  
	 
 
	  Career Services Office 
	 
		  Career Services Office Calendar  
	 
 
	  Information Technology 
	 
		  Information Technology Calendar  
	 
 
	  Law Admissions, Recruiting,   Scholarships 
	 
		  Law Admissions, Recruiting, and Scholarships Calendar  
	 
 
	  Law School Registrar 
	 
		  Law School Registrar Calendar  
	 
 
	  Library 
	 
		  Library Calendar  
	 
 
	  Office of the Dean 
	 
		  Office of the Dean Calendar  
	 
 
	  Office of Diversity 
	 
		  Office of Diversity Calendar  
	 
 
	  Pro Bono Office 
	 
		  Pro Bono Office Calendar  
	 
 
	  Student Affairs 
	 
		  Student Affairs Calendar  
	 
 
 
 
  Blog Information  
  Writing Competitions  
  Events  
  
					  
								
			  
		  
		
	  
	
	 
		 			
			 
				 
 

	 

			   Monthly Archive:  December 2016  
			
	
  	
	 		
		
				
				
			 
				 				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Academic Affairs  
			 20 Dec, 2016 
		  
		
		 
			 Exam Review 
		  
		
				 				
			 FROM DEAN MULROY: Students:  Exam review period in the spring will be from Monday, January 23 through Friday February 9, 2017.     
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Office of Diversity  
			 19 Dec, 2016 
		  
		
		 
			 JOIN THE LAW STUDENT DIVERSITY COMMITTEE! 
		  
		
				 				
			 The Student Affairs Office of Diversity is now accepting applications for the Spring 2017 Law Student Diversity Committee To be eligible,    
		  
				
	  	
  					  				 	
	 
		
		 
			 
									 																			 
					  
		
		 
			  Law School Announcements  
			 19 Dec, 2016 
		  
		
		 
			 ML   Issue #6 available now 
		  
		
				 				
			 The newest issue of ML, the law school s magazine, is available now. You can read the full issue online by clicking    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Academic Affairs  /  Experiential Learning  /  Law School Registrar  
			 19 Dec, 2016 
		  
		
		 
			 Spring 2017 Externship Course   Additional Enrollment Opportunity   Field Placements Available! 
		  
		
				 				
			 Dear 2L and 3L Students: Enrollment in the Law School s Externship Course is an incredible way to earn valuable legal experience    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Academic Affairs  /  Law School Registrar  
			 19 Dec, 2016 
		  
		
		 
			 SPRING 2017 CHILDREN S DEFENSE CLINIC   OPEN SEATS REMAIN   APPLY NOW! 
		  
		
				 				
			 Open seats remain in the Spring 2017 Children s Defense Clinic Course.  Just launched this Fall, the Children s Defense Clinic has already represented nearly 30    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  PALS  
			 16 Dec, 2016 
		  
		
		 
			 Alternative Spring Break 2017 Application Due Jan. 18, 2017 
		  
		
				 				
			 Looking for a great way to spend spring break?  Need to complete your pro bono hour requirement for graduation? Then apply    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Career Services Office  
			 16 Dec, 2016 
		  
		
		 
			 3Ls: Law Clerk position with possible post-grad employment 
		  
		
				 				
			 Immediate opening: A large personal injury law firm with 33 attorneys is seeking a 3rd year law student who is in    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Career Services Office  
			 16 Dec, 2016 
		  
		
		 
			 DAMALI BOOKER 1L MINORITY JOB FAIR (Nashville) Apply by Jan 9 
		  
		
				 				
			 The NBA s annual Damali Booker 1L Minority Job Fair will be held on Saturday, January 28, at Waller Lansden Law Firm    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Office of Diversity  
			 15 Dec, 2016 
		  
		
		 
			 2017 LSAC Diversity Initiative Internship 
		  
		
				 				
			 LSAC is accepting applications for the 2017 LSAC Diversity Initiatives Summer Internship. This is a great opportunity for a current law    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Academic Affairs  
			 9 Dec, 2016 
		  
		
		 
			 Government Relations   Lobbying Canceled 
		  
		
				 				
			 FROM DEAN MULROY Students:  Due to instructor health issues, we will not be able to offer Government Relations   Lobbying this    
		  
				
	  	
  					   			  
		
			 
			 
 Page 1 of 2  1  2    
 	  
			
				
	  
	
  


	 
		
		    
		
		 
			
						 
				 Follow: 
							 
						
						
						
			   Law School Website                     		 		 Recent Posts 		 
					 
				 1L Minority Clerkship Program   Reception (Chattanooga) 
						 
					 
				 New resources! 
						 
					 
				 Community Legal Center Volunteer Opportunity 
						 
					 
				 Law School Bookstore 2 Day Sale   Dec. 6th and 7th! 
						 
					 
				 St. Jude Memphis Marathon/Downtown Street Closures 
						 
				 
		 		  Archives 		 
			  December 2017  
	  November 2017  
	  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
		 
		   Categories 		 
	  Academic Affairs 
 
	  Academic Success Program 
 
	  AWA 
 
	  BLSA 
 
	  Career Services Office 
 
	  Christian Legal Society 
 
	  Experiential Learning 
 
	  Federal Bar Association 
 
	  Federalist Society 
 
	  Health Law Society 
 
	  HLSA 
 
	  Honor Council 
 
	  ILS 
 
	  Information Technology 
 
	  Law Admissions, Recruiting,   Scholarships 
 
	  Law Review 
 
	  Law School Announcements 
 
	  Law School Registrar 
 
	  Library 
 
	  Memphis Law +1 
 
	  Mock Trial 
 
	  Moot Court Board 
 
	  National Lawyer s Guild 
 
	  Office of Diversity 
 
	  Office of the Dean 
 
	  OutLaw 
 
	  Outside Organizations 
 
	  PAD 
 
	  PALS 
 
	  Pro Bono Office 
 
	  SBA 
 
	  Sports   Entertainment Law Society 
 
	  Street Law 
 
	  Student Affairs 
 
	  Student Organizations 
 
	  Tennessee Association of Criminal Defense Lawyers 
 
	  Writing Center 
 
	  Writing Competitions 
 
		 
 			
		  
		
	  

	
 
	
	    
	
	 
		
				 
			 More 
		 
				
				
		  Search   
	 
		 
	 
    Login 					 
					 
										 
					 
					 
					  Username:  
					  
					  Password:  
					  
										  
					  Remember me  
															   
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
											  Don't have an account?  
										  Lost your password?  
					 
					
					 
					
										 
				
					 
					  Choose username:  
					  
					  Your Email:  
					  
										   
					 
					 
					 
					 
					 
					 
					 
					 
					   Have an account?   
					 
										
					 
			
					 
					  Enter your username or email:  
					  
										   
					 
					 
					 
					 
					 
					 
					 
					 
					   Back to login   
					 
					
					 
					
										  
					   
					 
					   Subscribe 	
	
 
	 
		 
		 
		 		
		
	 
		
		 
					  

		
 
		 Email Address *  
	 
  
 
		 First Name 
	 
  
 
		 Last Name 
	 
  
 
		 Middle Name 
	 
  			 
				* = required field			  
			
		 
			 
		  
	
	
				
	  
	  
  
	    Blog Feedback                     		
	  
	
  	

				  
			  			
		  
	  

	 
		
				
				
				
		 
			 
				
				    
				
				 
					
					 
						
												
						 
															 On Legal Grounds | Memphis Law Events   Announcements   2017. All Rights Reserved. 
													  
						
												 
							 Powered by  WordPress . Theme by  Alx . 
						  
												
					 
					
					 	
											 
				
				  
				
			  
		  
		
	  

  

 		
		










 
 
 