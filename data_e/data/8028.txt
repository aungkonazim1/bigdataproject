ISSRL    
 
 
 ISSRL 
 


 
 
 

 
 Intelligent Security Systems Research Laboratory 
 

 
 
 
  Main Page  
  Home  
  Acclaim  
  Lab Projects  
  Students  
  Publications  
  Research  
  Seminars  
  Other Sites  
 
 
 

 
  
  
 
 
Dr. Dipankar Dasgupta 
333 Dunn Hall 
Memphis, TN 38152-3240 
 
phone: (901) 678-4147 
fax: (901) 678-2480 
 dasgupta@memphis.edu 
 
 Professor,  Department of Computer Science ,  University of Memphis  
 Director,  Center for Information Assurance  
 Director,  Intelligent Security Systems Research Laboratory  
 
You can read more about  Dr. Dipankar Dasgupta , his publications, and his research interests at his  profile .
 
 

 


 

 Immunity-based agents roam around the machines (nodes or routers) and monitor the situation in the network (i.e., look for changes such as malfunctions, faults, abnormalities, misuse, deviations, intrusions, etc.). These agents can mutually recognize each other's activities and can take appropriate actions according to the underlying security policies. Such an agent can learn and adapt to its environment dynamically and can detect both known and unknown intrusions.  

-- Dr. Dipankar Dasgupta

 

 

 

 



 

 
 
 
