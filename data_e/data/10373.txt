Films - The Civil Rights Act of 1964: How Far Have We Come? - LibGuides at University of Memphis Libraries   
 
	 
         
         
         
        
		 Films - The Civil Rights Act of 1964: How Far Have We Come? - LibGuides at University of Memphis Libraries 
		 
		 
 
 
 
 
 
 
 
 
 
 
 

		 
		 
		 
		 
		 
		 
         
         
				 
				 
             
         
        
        
            
		
		 
		
		 
		 
		
		 
		
		
	   
		         
        	 
     
	 
		 Skip to main content                  
                 
         
  		 
             
                
			 
				
					 
						 University of Memphis Libraries
						 
					 
					 
						 LibGuides
						 
					 
					 
						 The Civil Rights Act of 1964: How Far Have We Come?
						 
					 
					 Films
					 
			              
			 
				 
					
                 
                     
                         
                             
                             Search this Guide  
                             
                                 Search 
                             
                         
                     
                 				 
							 
             
                 The Civil Rights Act of 1964: How Far Have We Come? 
                 
                      
                 
             
         
         
          
         
             
                 
                     
                         
                             
                 
                     
                         Welcome 
                        
                     
                 
                 
                     
                         Government Publications 
                        
                     
                 
                 
                     
                         Books 
                        
                     
                 
                 
                     
                         Newspapers 
                        
                     
                 
                 
                     
                         Films 
                        
                     
                 
                 
                     
                         Additional Resources 
                        
                     
                 
                 
                     
                         Inspiring Women of the 1960s 
                        
                     
                  
                            
                 
                     
                         
                        
							 
					 
						 
							 MLK:  The Assassination Tapes
                                 
							 
								 
									
			 
				   

    The U of M s Ned R. McWherter Library s Preservations   
  Special   Collections lauded in the   Memphis Daily News   for 
  helping MLK documentary.  

     Tom Jennings, director of   MLK: The Assassination     Tapes ,  
   and his colleagues heaped praises on the   Special Collections 
   staff at the University of Memphis,   whose efforts helped 
   the documentary about the   assassination of  Dr. Martin Luther King Jr. 
   win the   prestigious Peabody Award.  

  The documentary, being shown on the Smithsonian   Channel, followed the 1968 sanitation workers strike   here and other events leading up to King s murder.  

   MLK: The Assassination   Tapes  uses multiple formats of material showcasing the sanitation   workers strike from the U of M Libraries  Special Collections department s holdings.   Copies of the documentary are on order.  
		    
								 
								
							 
						 
					 
							 
							 
					 
						 
							 Citizen King:  a film
                                 
							 
								 
									
			 
				     
     Citizen King      (PBS)   pushes past the myths that have obscured King's story to reclaim the history of a people's leader. Using the personal recollections, diaries, letters, and eyewitness accounts of friends, family, journalists, law enforcement officers and historians, this film brings fresh insights to King's difficult journey, his charismatic -- if at times flawed -- leadership, and his truly remarkable impact.  
  Call Number:  E185.97.K5 C58 2004 Video (  Lambuth Campus Library - Main Level - Media Collection)     
		    
								 
								
							 
						 
					 
							 
                         
                     
                                          
                     
                 
                 
                     
                         
                             
     
                  
					 
						 
							 Civil Rights Films at McWherter Library
                                 
							 
								 
									 
                          
                        
			 
				 
						 
					 
					 
					 Birth struggle of the 1964 Civil Rights Act
					   by   produced by CBS News
					   Call Number: electronic resource  Publication Date: [Distributed by] FMG on Demand, 1964. 
						  This documentary from the CBS News Archives examines the stormy passage of civil rights bill H.R. 7152 through the House of Representatives. The program, filmed in 1964, begins with a report on the controversial bill's history, from its introduction by John F. Kennedy to the eve of its debate on the Senate floor. Following that report, Eric Sevareid moderates as Senators Hubert Humphrey and Strom Thurmond engage in a live television debate on the bill's merits. 
Footage of John and Robert Kennedy, Justice Department officials Nicholas Katzenbach and Burke Marshall, President Lyndon Johnson, and the racial clashes of the early 1960s captures the tension and drama surrounding the most comprehensive civil rights law since Reconstruction." -- description provided by Films Media Group. 
						  
			  
                        
                             
                             
			 
				   

        Eyes on the prize:  America s  civil rights             years - 1954-1965      / Blackside, Inc. ; produced and directed by Judith Vecchione ; executive producer, Harry Hampton ; series writer, Steve Fayer - 7 discs  
 E185.615 .E983 2006  McWherter Permanent Reserve Collection   
		     
                          
                        
			 
				 
						 
					 
					 
					 A Civil Rights Journey
					   by   Sonnie W. Hereford
					   Call Number: F334.H9 C58 1999b  Publication Date: 1999 
						  A history of Huntsville's civil rights movement as seen through the eyes of Sonnie Hereford, III, M.D. 
Location:  McWherter Library - 4th Floor - Special Collections 
						  
			  
                         
                        
			 
				 
						 
					 
					 
					 A Place of Rage
					   by   Pratibha Parmar
					   Call Number: E185.86 P55 1991  Publication Date: 1991 
						  Prominent black women comment on experiences of Afro-American women, on racial discrimination and its effects on the American culture and make suggestions which they hope will improve the future. Includes historical footage of civil rights movement in the 1960's. Producer & Director, Pratibha Parmar. 
						  
			  
                        
                             
                             
								 
								
							 
						 
					   
					 
						 
							 Documentary  "I Am a Man: From Memphis: a Lesson in Life"
                                 
							 
								 
									
			 
				   
 
  National Guard bayonets block Memphis  famed Beale Street March 29, 1968 as marchers supporting striking sanitation workers pass through downtown. A march led by Dr. Martin Luther King Jr. the day before erupted in violence. UPI/ Commercial Appeal  
University of Memphis Libraries Preservation and Special Collections Department.    

      I   Am a Man: From Memphis: A Lesson in Life     is a documentary featuring Mr. Elmore Nickleberry, who was one of the original Sanitation workers who went on strike in 1968 to protest the unfair, racist practices of the Memphis Sanitation Department. At the time of the filming and its debut (2009), Mr. Nickleberry, at 77, was still working for the Memphis Sanitation Department, proud to have remained an active employee who helped keep the city clean.  

  1 DVD copy being processed for McWherter Library - 4th Floor - Special Collections  

  *Complementary  print  and  eBook ,  I am a Man! Race, Manhood, and the Civil Rights Movement , also available--includes special chapter on the Memphis Sanitation Strike.  
		    
								 
								
							 
						 
					   
					 
						 
							 At the River I Stand: Documentary
                                 
							 
								 
									
			 
				   
      At the river I stand       (A film by David Appleby, Allison Graham, and Steven John Ross; a production of Memphis State University, Department of Theatre and Communication Arts.)   Memphis, Spring 1968 marked the dramatic climax of the Civil Rights movement.    At the River I Stand    skillfully reconstructs the two eventful months that transformed a strike by Memphis sanitation worker into a national conflagration, and disentangles the complex historical forces that came together with the inevitability of tragedy at the death of Dr. Martin Luther King, Jr.     2 DVD copies ordered for McWherter Library - 2nd Floor;VHS currently available on 2nd floor (McWherter Library):   HD5325.S2572 1968 M46 1993       
		    
								 
								
							 
						 
					  
         
     
                          
                     
                    
                 
                     
                         Previous:  Newspapers 
                     
                     
                      Next:  Additional Resources    
                     
                                  
             
         
         
         
             
                 
                     
                         Last Updated:   Aug 18, 2016 4:14 PM                      
                     
                         URL:   http://libguides.memphis.edu/civilrightsact1964                      
                     
                    	    Print Page                      	
                     
                 
                     Login to LibApps                  
             
             
                 
                     Report a problem  
                 
                 
                    
                     Subjects:  
                      Special Collections  
                                     
                 
                    
                     Tags:  
                      civil rights ,  communications ,  education ,  event ,  exhibition ,  film ,  government publications ,  government resources ,  history ,  minorities ,  motion pictures ,  race ,  research ,  sociology ,  special collections  
                                     
             
         
         
        
			 
				 
					 
					    
					    
					 
				 
			          
                              
                                
                 
                

		 
  	 
 
