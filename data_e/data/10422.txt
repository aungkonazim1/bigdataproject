Performance Management Assessment Reference Guide - Employee Relations &amp; Engagement - University of Memphis    










 
 
 
     



 
    
    
    Performance Management Assessment Reference Guide - 
      	Employee Relations   Engagement
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Employee Relations   Engagement
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Staff Directory  
                          Performance Management  
                          Awards  
                          HR  
                          Nursing Mothers  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Employee Relations   Engagement
                              	  
                         Performance Management Assessment Reference Guide 
                      
                   
                   
                       
                      
                     		
                     
                     
                      Performance Management Assessment Reference Guide 
                     
                      An Equal Opportunity/Affirmative Action University    Copyright 2000, 2015 The University of Memphis, Department of Human Resources. All
                        other rights are reserved.      THE PERFORMANCE MANAGEMENT CYCLE   Effective Performance Management Assessments follow a Performance Management Cycle:
                          1) Set Performance Standards  2) Communicate Standards and Expectations  3) Monitor Employee Performance All Year  4) Give Ongoing Feedback  5) Write and Conduct the Performance Management Assessment
                      
                     
                      At the beginning of the period for which you will appraise the employee (usually 12
                        months prior), discuss with the employee the importance of each appraisal factor on
                        the form as it relates to their job responsibilities. You may include a job-related
                        Optional Factor that is not preprinted. You must rate a minimum of 8 performance appraisal
                        factors as a 1 or a 2. Use the Importance Rating Scale below to set the rating number.
                      
                     
                      2 = Highly Important to Position  1 = Important to Position  0 = Not Applicable to Position
                      
                     
                      Once you and the employee agree on the importance of each factor, write on the  performance management worksheet  the appropriate rating number for each performance factor. Save the worksheet in
                        the employee's file and update when a change in expectations occurs, or when a new
                        incumbent is hired. To access the performance management worksheet, click  here .
                      
                     
                       Instructions for Completing the Performance Management Assessment Form   1) At the end of the period for which you are appraising the employee, retrieve the
                        performance management assessment form that you marked with agreed upon Importance
                        Ratings of appraisal factors.    2) Use the Performance Rating Scale of 1-5 to rate the employee for each performance
                        appraisal factor that has an Importance Rating of more than 0. Highlight the rating
                        by clicking on the down arrow key next to Performance Rating. Ratings are available
                        in tenths of a point. (Example 3.3 or 3.4)    3) Provide supporting comments for all ratings. Comments should be as specific as
                        possible. Note actual examples of job performance that support your rating. You may
                        attach additional documents, if necessary.    4) After completing the performance management assessment form, ratings will be automatically
                        calculated to get the Overall Performance Rating Score.     Manual Calculations   a) Transfer the Job Importance Ratings for each factor to Column 1 on the chart at
                        the end of this guide, add these ratings, and write the total in the Total Importance
                        Rating (TIR) space provided at the bottom of Column 1.    b) Go to column 2, Importance Ratio. For each factor divide the Job Importance Rating
                        in Column 1 by the number you calculated as the TIR. Multiply this number by 10 and
                        round to the nearest tenth place. Write these numbers in Column 2. Example: If the
                        Job Importance Rating (I) is a 1 and Total Importance Rating (TIR) is 12, then the
                        Importance Ratio (IR) is 1/12 or .083 X 10 = .83. Then round to 8.    c) Add Column 2. The total IR's should approximate 10, but due to rounding it will
                        not be exact.    d) Now transfer each Performance Rating to Column 3.    e) Multiply each Importance Ratio by the Performance Rating, and write the result
                        in Column 4. Add these Weighted Ratings, and write the total at the bottom of Column
                        4.    f) To obtain the Overall Performance Rating Score, multiply the Total Rating Score
                        (TRS) by 2. Write the result in the space for Overall Performance Rating Score.
                      
                     
                       IMPORTANT POINTS   You may use the Special Recognition section of the Staff Performance Management Assessment
                        form to note significant employee achievement. This documentation will not affect
                        the Overall Performance Rating Score.    The Affirmative Action Section must also be completed for staff that makes hiring
                        and promotional decisions/recommendations. This rating will not affect the Overall
                        Performance Rating Score.     Performance Appraisal Factors are listed in alphabetical order, not in order of performance.
                             PERFORMANCE APPRAISAL FACTOR EXAMPLES   I. Flexibility   Job Importance Rating: 1   *Adapts to changing work demands and priorities  *Learns and/or assumes new tasks  *Accepts changes and can integrate new knowledge and skills     Performance Rating: 3       Supporting Comments:    Normally accepts new and different tasks with little difficulty and impact on workflow
                        processes. The employee required the standard length of time to adapt to the new telephone
                        system.    II. Quality of Work   Job Importance Rating: 1   *Effectively performs the job through timeliness, accuracy and thoroughness  *Maintains high quality work relative to established standards  *Consistently follows University policies and procedures     Performance Rating: 4       Supporting Comments:  Projects are always completed on time with few errors. Once the employee did not
                        know if all University documents needed to have the University logo and date, but
                        she/he contacted the Media Relations department for advice before printing the document.
                      
                     
                      Example: 
                     
                      
                        
                         
                           
                            
                                 
                               Column 1 
                               Column 2 
                               Column 3 
                               Column 4 
                            
                           
                            
                              
                                Performance  Appraisal  Factors  
                              
                               Job Importance Rating
                                 
                                  ( I ) 
                                 
                                  From Pages 1-3 
                                 
                               
                              
                               Importance Ratio
                                 
                                  ( IR ) 
                                 
                                  I/TIR X 10 
                                 
                               
                              
                               Performance  Rating
                                 
                                  ( PR ) 
                                 
                                  From Pages 1 - 3 
                                 
                               
                              
                               Weight Rating
                                 
                                  ( WR ) 
                                 
                                  IR X PR 
                                 
                               
                              
                            
                           
                            
                              
                                Flexibility  
                              
                               1 
                              
                               1/3 X 10 = 3 
                              
                               3 
                              
                               9 
                              
                            
                           
                            
                              
                                Quality of Work  
                              
                               2 
                              
                               2/3 X 10 = 7 
                              
                               4 
                              
                               28 
                              
                            
                           
                            
                              
                                Totals  
                              
                               3
                                 
                                  Total Importance Rating (TIR) 
                                 
                               
                              
                               Total Importance Ratio 
                              
                                 
                              
                               37
                                 
                                  Total Rating Score  (TRS)
                                  
                                 
                               
                              
                            
                           
                         
                        
                      
                     
                      Total Rating Score (TRS)  37  multipled by 2 equals  74  = Overall Performance Rating Score        Overall Performance Score Rankings:    (Circle appropriate level)
                      
                     
                      1 - 35      Unsatisfactory performance   36 - 59    Needs some improvement to meet position requirements   60 - 75    Meets position requirements   76 - 95    Frequently exceeds position requirements   96 - 100  Distinguished performance
                      
                     
                       Electronic Performance Management Assessment   Copyright 2000, 2007, 2011, 2013, 2015 The University of Memphis, Department of Human
                        Resources. All other rights are reserved. The Guide and Form program may not be used
                        or copied by third parties without appropriate written permission.      Electronic Appraisal   
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Performance Appraisal  
                               The online performance management system for staff evaluations 
                            
                            
                                Recognition   Engagement Spotlight  
                               Prestigious employee awards and engagement functions hosted by HR 
                            
                            
                                Forms  
                               All of B F's forms in one place 
                            
                            
                                Contact Us  
                               Our team is here to help 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


