Adding associations between competency structure elements | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Adding associations between competency structure elements | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Competencies    »    Creating and managing competency structure elements   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Adding associations between competency structure elements                       
        
        
       
          
     
           Printer-friendly version       
	The most fundamental structure must contain at least one competency, one learning objective, and one activity. Add associations between these elements to create your competency structure.
 

 
	Associate a competency structure element with another existing element
 

  
		On the Competency Home page, click on the element you want to associate with existing elements.
	 
	 
		Click    Structure .
	 
	 
		In the Edit Structure tab do one of the following:
		  
				Click  Add Parent .
			 
			 
				Click  Add Child .
			 
		  
	 
		Select    Existing Competency ,    Existing Learning Objective , or    Incorporate Activities .
	 
	 
		Select the check box beside each element you want to associate and click  Add / Add Selected .
	 
  
	  Associate a learning objective with an activity
 

 
	See  Creating activities  to learn more about the activity types you can associate with learning objectives.
 

  
		On the Competency Home page, click on the learning objective you want to associate with an activity.
	 
	 
		Click    Structure .
	 
	 
		In the Edit Structure tab, click  Add Child .
	 
	 
		Click    Incorporate Activities .
	 
	 
		Click on the activity type you want to add and select an existing activity item from the drop-down list. If you select a Manual Assessment Activity, enter a  Name  for the activity and select a rubric. If you select a Content Activity, proceed to Step 8.
	 
	 
		Select  Assess Objective  to associate a rubric or numeric assessment.
	 
	 
		Once you choose an Assessment Type, select its related Assessment Method or Criteria.
	 
	 
		If you want to set an assessment Threshold that users must achieve, select the check box  The activity is required to complete learning objective  and set a  Threshold .
	 
	 
		Click  Add .
	 
  
	See also
 

  
		 Creating activities 
	 
      Audience:    Instructor      

    
           

                   ‹ Creating activities 
        
                   up 
        
                   Removing associations between competency structure elements › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Competencies  
  
      Competency structure basics    Automating competency structure evaluation    Creating and managing competency structure elements    Understanding competency status settings    Creating competencies    Editing a Draft or In Review competency s details    Hiding or showing an Approved competency    Modifying an Approved competency    Copying a competency    Archiving a competency    Deleting competencies    Creating learning objectives    Editing a learning objective s details    Copying a learning objective    Deleting learning objectives    Creating activities    Adding associations between competency structure elements    Removing associations between competency structure elements    Sharing competency structures    Tracking competency versions    Viewing competency structure results    Overriding competency structure results    Managing independent learning objectives and independent activities      Evaluating competency structure activities    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
