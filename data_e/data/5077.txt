Accessibility | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 Accessibility | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home   
                      
                      
                      
                      
                       
                           
  
   
   

    
              
     
           Printer-friendly version       Accessibility 


 
	 
		Web accessibility refers to how easily people with disabilities can navigate and interact with websites. Disabilities may be physical (such as blindness, low vision, deafness, or fine motor skills difficulty), or cognitive (such as dyslexia or attention deficit disorder). People with disabilities often use assistive technologies to help them navigate the web. An assistive technology is any device that helps a person with a disability. Common web assistive technologies include modified mice and keyboards, screen readers and screen magnifiers.
	 

	 
		Web accessibility occurs when websites support web accessibility standards, are compatible with assistive technologies, and are easy for people to navigate and understand.
	 

	 
		At Desire2Learn we follow web accessibility standards closely and work with interested clients to test the usability of our products for people with disabilities. We believe that instructional practices should focus on helping people learn, and should not be limited by the learning management system the material is delivered in.
	 

	 
		Accessibility topics for learning
	 

	 
		Many features in the Desire2Learn Learning Suite can be adjusted to improve access for individuals with disabilities. We recommend that individuals who use screen readers, screen magnifiers, or navigate primarily by keyboard, read the accessibility topics to help ensure that the features and settings they use best support their needs.
	 
 

 
	 
		How do I use assistive technology in Desire2Learn?
	 
 

 
	  
			 Screen reader accessibility features 
		 
		 
			 Screen reader tips 
		 
		 
			 Keyboard-only navigation accessibility features 
		 
		 
			 Keyboard-only navigation tips 
		 
	  

 
	  
			 Screen magnifiers, zooming, and color contrast accessibility features 
		 
		 
			 Screen magnifiers, zooming, and color contrast tips 
		 
		 
			 Getting additional support 
		 
	  

 
	
 

 
	 
 
     Audience:    Learner      

       Using assistive technology in Desire2Learn   
           

        
        
                   Using assistive technology in Desire2Learn › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Accessibility  
  
      Using assistive technology in Desire2Learn    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
