Previewing dropbox folders and submissions | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Previewing dropbox folders and submissions | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Dropbox    »    Creating and managing Dropbox   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Previewing dropbox folders and submissions                       
        
        
       
        
     
              
	The preview option enables you to navigate the steps that users go through to submit files, review their submissions, and view submission history.
 

 
	You can make a preview submission visible on the Submissions page, but you cannot give feedback, grades, or rubric scores to it because it is not tied to an actual user.
 

 
	Preview file submission and submission history
 

  
		Click    Preview  from the More Actions button on the Dropbox Folders page.
	 
	 
		Click on the dropbox folder you want to submit a file to.
	 
	 
		Click  Add a File  to browse for the file you want to submit. You can attach files from your local computer or storage device, a personal locker, a group locker, or ePortfolio.
	 
	 
		You can  Add a File ,  Record Video , or  Record Audio  as feedback. When you finish a recording, click  Add .
	 
	 
		Enter any  Comments  you want to submit with the file.
	 
	 
		Select the  Allow this preview submission to be available in the dropbox folder  option if you want to search for and view the file on the Folder Submissions page after you leave the Preview area.
	 
	 
		Click  Submit .
	 
	 
		Click  View History .
	 
	 
		Select the  Folder  you want to view from the drop-down list.
	 
	 
		Click on your submission to preview.
	 
      Audience:     Instructor       

    
           

                   ‹ Associating dropbox folders with learning objectives 
        
                   up 
        
                   Evaluating dropbox folder submissions › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Dropbox  
  
      Dropbox basics    Creating and managing Dropbox    Creating dropbox categories    Creating dropbox folders    Managing dropbox folder submission handling    Setting dropbox folder availability and due dates    Setting release conditions for a dropbox folder    Adding special access permissions to a dropbox folder    Editing dropbox categories and folders    Reordering dropbox categories and folders    Deleting dropbox categories and folders    Restoring deleted dropbox folders    Viewing the Dropbox event log    Associating dropbox folders with learning objectives    Previewing dropbox folders and submissions      Evaluating dropbox folder submissions    
                  
           
         

       
     

    
    
    
   
 
   
 
