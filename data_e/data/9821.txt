Creating content in the design view of the HTML Editor | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Creating content in the design view of the HTML Editor | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    HTML Editor    »    Creating content in the HTML Editor   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Creating content in the design view of the HTML Editor                       
        
        
       
          
     
           Printer-friendly version        
		Navigate to the location in the system where you want to insert or edit your content.
	 
	 
		Click    Edit  from the context menu or click an applicable text field to display the HTML Editor interface.
	 
	 
		Enter your content into the HTML Editor.
	 
	 
		To insert tables, images, links, audio/video/animation files, special characters, and equations, use the  Insert options  and  Table options .
	 
	 
		Save your changes.
	 
  
	HTML Editor options
 

    
				Icon
			 
			 
				Name
			 
			 
				Function/Description
			 
		   
				Editing options
			 
		   
				  
			 
				 Cut 
			 
			 
				Copies and deletes the selected content to the clipboard.
			 
		   
				  
			 
				 Copy 
			 
			 
				Copies the selected content to the clipboard.
			 
		   
				  
			 
				 Paste 
			 
			 
				Pastes content from the clipboard.
			 
		   
				  
			 
				 Undo 
			 
			 
				Undoes the last action.
			 
		   
				  
			 
				 Redo 
			 
			 
				Redoes the last action.
			 
		   
				  
			 
				 Paste as Plain Text 
			 
			 
				Pastes content from the clipboard in plain text.
			 
		   
				  
			 
				 Paste from Word 
			 
			 
				Pastes content from the clipboard with Word formatting.
			 
		   
				Text formatting options
			 
		   
				  
			 
				 Format 
			 
			 
				Applies a preset style to selected text.
			 
		   
				  
			 
				 Font Family 
			 
			 
				Changes the font of selected text.
			 
		   
				  
			 
				 Size 
			 
			 
				Changes the size of selected font.
			 
		   
				  
			 
				 Apply Color 
			 
			 
				Changes the color of selected text.
			 
		   
				  
			 
				 Bold 
			 
			 
				Makes the selected text bold.
			 
		   
				  
			 
				 Italic 
			 
			 
				Makes the selected text italic.
			 
		   
				  
			 
				 Underline 
			 
			 
				Underlines the selected text.
			 
		   
				  
			 
				 Strikethrough 
			 
			 
				Puts a line through the selected text (as if it has been crossed out).
			 
		   
				  
			 
				 Subscript 
			 
			 
				Makes the selected text subscript.
			 
		   
				  
			 
				 Superscript 
			 
			 
				Makes the selected text superscript.
			 
		   
				Paragraph formatting options
			 
		   
				  
			 
				 Indent 
			 
			 
				Moves the margin of the current paragraph to the right.
			 
		   
				  
			 
				 Outdent 
			 
			 
				Moves the margin of the current paragraph to the left.
			 
		   
				  
			 
				 Unordered List 
			 
			 
				Inserts a bulleted list.
			 
		   
				  
			 
				 Ordered List 
			 
			 
				Inserts a numbered list.
			 
		   
				  
			 
				 Align Left 
			 
			 
				Aligns the selected paragraph to the left.
			 
		   
				  
			 
				 Align Right 
			 
			 
				Aligns the selected paragraph to the right.
			 
		   
				  
			 
				 Align Center 
			 
			 
				Aligns the selected paragraph in the center of the page.
			 
		   
				  
			 
				 Align Full 
			 
			 
				Fully aligns the selected paragraph (creating flush margins on both sides).
			 
		   
				  
			 
				 Left to Right 
			 
			 
				Tags the selected paragraph's text direction as left-to-right. This ensures that users' browsers render the text appropriately. Useful when the text direction of the paragraph differs from the system locale or other content in the HTML Editor.
			 
		   
				  
			 
				 Right to Left 
			 
			 
				Tags the selected paragraph's text direction as right-to-left. This ensures that users' browsers render the text appropriately. Useful when the text direction of the paragraph differs from the system locale or other content in the HTML Editor.
			 
		   
				  Insert options
			 
		   
				  
			 
				 Insert Stuff 
			 
			 
				Enables you to insert media (including audio and video) files from a variety of sources.
			 
		   
				  
			 
				 Insert Image 
			 
			 
				Inserts an image at the current location of your cursor.
			 
		   
				  
			 
				 Insert Quicklink 
			 
			 
				Enables you to insert a quicklink to a resource inside Learning Environment.
			 
		   
				  
			 
				 Graphical equation 
			 
			 
				Launches the  Equation Editor , allowing you to insert graphical equations.
			 
		   
				  
			 
				 MathML equation 
			 
			 
				Launches the  Equation Editor , allowing you to insert MathMl equations.
			 
		   
				  
			 
				 LaTeX equation 
			 
			 
				Launches the  Equation Editor , allowing you to insert LaTeX equations.
			 
		   
				  
			 
				 Insert Symbol 
			 
			 
				Enables you to insert symbols and other special characters.
			 
		   
				  
			 
				 Insert Line 
			 
			 
				Inserts a horizontal line separating paragraphs.
			 
		   
				  
			 
				 Insert Attributes 
			 
			 
				Enables you to add Title, ID, Class, Style, and Text Direction attributes to selected text without opening the Source Editor.
			 
		   
				  Table options
			 
		   
				  
			 
				 Insert Table 
			 
			 
				Inserts a table at the current location of your cursor and lets you choose number of rows and columns.
			 
		   
				  
			 
				 Table Properties 
			 
			 
				Enables you to change table properties such as cell padding, cell spacing, alignment, height, width, border properties, background colors, and whether the table has a caption.
			 
		   
				  
			 
				 Cell Properties 
			 
			 
				Enables you to change the properties of the cell your cursor is located in.
			 
		   
				  
			 
				 Row Properties 
			 
			 
				Enables you to change the properties of the row your cursor is located in.
			 
		   
				  
			 
				 Remove Column 
			 
			 
				Deletes the column your cursor is located in.
			 
		   
				  
			 
				 Remove Row 
			 
			 
				Deletes the row your cursor is located in.
			 
		   
				  
			 
				 Delete Table 
			 
			 
				Deletes the table your cursor is located in.
			 
		   
				  
			 
				 Insert Column Before 
			 
			 
				Adds a blank column before the column your cursor is located in.
			 
		   
				  
			 
				 Insert Column After 
			 
			 
				Adds a blank column after the column your cursor is located in.
			 
		   
				  
			 
				 Insert Row Before 
			 
			 
				Adds a blank row before the row your cursor is located in.
			 
		   
				  
			 
				 Insert Row After 
			 
			 
				Adds a blank row after the row your cursor is located in.
			 
		   
				  
			 
				 Cut Row 
			 
			 
				Copies and deletes the row your cursor is located in.
			 
		   
				  
			 
				 Copy Row 
			 
			 
				Copies the row your cursor is located in.
			 
		   
				  
			 
				 Paste Row Before 
			 
			 
				Pastes a previously copied or cut row before the row your cursor is located in.
			 
		   
				  
			 
				 Paste Row After 
			 
			 
				Pastes a previously copied or cut row after the row your cursor is located in.
			 
		   
				  
			 
				 Merge Table Cells 
			 
			 
				Joins selected cells together. Existing content within the cells merges together.
			 
		   
				  
			 
				 Split Table Cells 
			 
			 
				Splits the selected cell into two cells. Existing content inside the cell remains in the first cell; the second cell is empty.
			 
		        Audience:    Instructor      

    
           

                   ‹ Creating content in the HTML Editor 
        
                   up 
        
                   Pasting Content into the HTML Editor › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        HTML Editor  
  
      HTML Editor basics    Creating content in the HTML Editor    Creating content in the design view of the HTML Editor    Pasting Content into the HTML Editor    Inserting images in the HTML Editor    Inserting media files in the HTML Editor      Creating tables in the HTML Editor    Creating quicklinks in the HTML Editor    Creating equations in the HTML Editor    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
