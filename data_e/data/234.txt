Chinese University Collaboration - UofM Media Room - University of Memphis    










 
 
 
     



 
    
    
    Chinese University Collaboration - 
      	UofM Media Room
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			UofM Media Room
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Publications  
                          News Releases  
                          Events  
                          Resources  
                          Contact Us  
                      
                      
                         
                            News Releases   
                            
                               
                                  
                                   Awards  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	UofM Media Room
                              	  
                          
                              	News Releases
                              	  
                         Chinese University Collaboration 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      UofM Deans Expand Collaboration with Chinese University 
                     
                      October 17, 2017 - Drs. Lin Zhan and Richard Bloomer traveled to China recently as
                        featured speakers at the second International Forum on Health Education and Promotion
                        held at Sanda University in Shanghai. Zhan, dean of the Loewenberg College of Nursing,
                        delivered the talk "Health Promotion: Concepts   Practice in Nursing." She focused
                        her presentation on building a culture of health for all and used the ACES Project
                        and Let's Talk Health project as examples. Bloomer, dean of the School of Health Studies,
                        shared recent research in his talk "Impact of Dietary Intake on Cardiovascular and
                        Metabolic Disease."
                      
                     
                      As China experiences a rise in obesity and type II diabetes, Bloomer discussed the
                        role of nutrition in preventing the development of the disease.
                      
                     
                      Both talks highlighted the role that clinicians play in sharing health-specific information
                        with patients and clients in an attempt to improve health outcomes. The forum was
                        held as part of Sanda's 25th anniversary celebration and was attended by several Chinese
                        dignitaries and other VIPs from around the world. Zhan and Bloomer worked with Sanda's
                        chief administrators to solidify collaboration opportunities to include graduate student
                        enrollment, as well as faculty exchange and study abroad programs for faculty and
                        students from both universities. The initial exchange is scheduled to begin in 2018.
                      
                     
                      "Dean Bloomer and I are both excited about the opportunities this collaboration brings
                        to UofM students and faculty," said Zhan.
                      
                     
                      FOR MORE INFORMATION Gabrielle Maxey 901.678.2135  gmaxey@memphis.edu  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         News Releases 
                         
                            
                               
                                Awards  
                            
                         
                      
                      
                      
                         
                            
                                Music Performance Calendar  
                               Don't miss a School of Music performance 
                            
                            
                                Art Museum of Memphis  
                               Learn about the latest installations 
                            
                            
                                The Martha and Robert Fogelman Galleries of Contemporary Art  
                               View upcoming Department of Art exhibits 
                            
                            
                                Contact Us  
                               Have a story to share or questions about UofM News? 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


