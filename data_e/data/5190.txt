Joining a teleconference virtual meeting in Wiggio | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Joining a teleconference virtual meeting in Wiggio | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   English  
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Integrations    »    Wiggio    »    Using virtual meetings in Wiggio   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Joining a teleconference virtual meeting in Wiggio                       
        
        
       
          
     
           Printer-friendly version       
			 Note  You can join a teleconference virtual meeting in Wiggio using a phone or  a computer. If using a phone, turn off the computer phone settings to eliminate background noise and static.
         Click the  Start  or  Join  link in the virtual meeting post on the feed to enter the virtual meeting. 
	 Dial the phone number that displays in the  Teleconference pop-up dialog. 
	 
		Do one of the following:
		    
		    If you are a host, dial the  Moderator code .  
		   
		    If you are a participant, dial the  Access code . 
	          Audience:    Learner      
    
         
               ‹ Attending a virtual meeting in Wiggio 
                     up 
                     Sharing your desktop in a Wiggio virtual meeting › 
           
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Wiggio  
  
      Wiggio basics    Creating and using groups in Wiggio    Managing groups in Wiggio     Adding resources to Wiggio    Creating and using folders in Wiggio    Using virtual meetings in Wiggio    Understanding virtual meetings    Adding a virtual meeting to your personal calendar    Attending a virtual meeting in Wiggio    Joining a teleconference virtual meeting in Wiggio    Sharing your desktop in a Wiggio virtual meeting      Using polls in Wiggio    Using to-do lists in Wiggio    Managing events in Wiggio    Importing and exporting calendars in Wiggio    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
