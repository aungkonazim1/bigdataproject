Networking Research Lab &gt; People   

 
   Networking Research Lab   People 
  
	 
	 
 

 

 

 
 
  
 
 
  
        
  
   
 
 

 
 
  
 
 
  
 
 

   
    
       
       
       
       
 
 

 
 
  
 
 
   
 
 

 
 
  
   
 
 

 

 Faculty 

 
  Lan Wang 
 

 Members 
 
 Muktadir Chowdhury (PhD student)
 Laqin Fan (PhD student)
 Minsheng Zhang (PhD student)
 Pi Lei (MS student)
 Ashlesh Gawande (Undergraduate student)
 Nicholas Gordon (Undergraduate student)
 


 Alumni 
 
 Dzung Dinh Luong (Postdoc)
 Kriangsiri ("Top") Malasri (MS)
 Malleswari Saranu (MS)
 Oksana Koziaryk (BS, graduated in 2012)
 Stephen G. Smith (BS, graduated in 2010)
 Ernest A. McCracken (BS, graduated in 2009, MS, graduated in 2012) 
 David DeBaecke (BS, graduated in 2007)
 Roman Birg (Undergraduate student) 
 Gus Sanders (BS, graduated in 2012)
 Daniel Albright (Undergraduate student)
 Andrew Hood (Undergraduate student)
  Yaoqing Liu (PhD, graduated in 2013) 
 Adam Alyyan (BS, graduated in 2013)  
 A K M Mahmudul Hoque (MS, graduated in 2014)                                                                                                    
 Nic Smith (BS, graduated in 2014)
  Syed Obaid Amin   (Postdoctoral Fellow, 2012-2014), Futurewei Technologies
 Marc Badrian, MS student, University of Memphis
 Vince Lehman (BS, 2014), Software Engineer, Retrans Inc.
 Benjamin Murphy (Undergraduate student)
 

 

 
 


 
 
 

 
 home     people     projects     papers  
 

 
   University of Memphis
 

 
 


 

 

 
