Grading - Registrar - University of Memphis  Grading  










 
 
 
     



 
    
    
    Grading - 
      	Registrar
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Registrar
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Register  
                          Students  
                          Veterans  
                          Faculty   Staff  
                          Forms  
                      
                      
                         
                            Faculty   Staff   
                            
                               
                                  
                                   Faculty   Staff  
                                         Authorized Signatures  
                                         FERPA Compliance  
                                         Scheduling   Term Planning  
                                         Self Service   Online Aids  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Registrar
                              	  
                          
                              	Faculty   Staff
                              	  
                         Grading 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Grading 
                     
                      Faculty Self Service allows you to submit final course grades online. 
                     
                      Login to the portal to submit final grades. 
                     
                      
                        
                         Select the  Faculty  page (in the menu bar at the top of the screen).
                         
                        
                         Expand the  Banner Self-Service  and  Faculty and Advisors  folders; click on the  Final Grades  link.
                         
                        
                         Select the desired term and click the [Submit] button. 
                        
                         Select the course (title and CRN) from the drop-down list and click [Submit].      
                        
                      
                     
                      Final Grades Screen 
                     
                      You should now be on the Final Grades screen for the selected class. Record your grades
                        and, if necessary, any stopped-attending dates.    
                     
                      Enter Grades 
                     
                      Use the drop-down box to select a grade for each student still enrolled in your class. 
                     
                       Remember :
                      
                     
                      
                        
                         Do not enter anything in the "Attend Hours 0-999.99" column. This information is not
                           used at UofM.
                         
                        
                         If the grade options do not seem appropriate for the section, or if no options are
                           listed, contact the Registrar's Office: there could be a problem with the grade mode.
                         
                        
                         You cannot assign a grade for a student who has a "W" in the "Rolled" column. 
                        
                         It is possible for a student to withdraw from a course  during  the grading period (approved late withdrawal). The student's record will have a "Withdrawn"
                           notice; however, the grading drop-down box will still be active since late "W" grades
                           have not been rolled.  DO NOT CHANGE THE "W" GRADE.  The student will receive a "W" grade once grades become official.
                         
                        
                         Check for students who have Stopped Attending dates. This warning message will appear
                           under the student's record:  The student has not withdrawn from the class . The student has not officially withdrawn and must, therefore, be assigned a grade.
                         
                        
                         If a student is to be assigned an F or U  and non-attendance is a factor , make sure you enter a Last Attend Date for this student if one is not already there.
                         
                        
                         Enter "F" as the official grade for any student who has  never attended  or  stopped attending  but never officially withdrew.
                         
                        
                      
                     
                      Record Stopped-Attending Dates 
                     
                      Record a stopped-attending date if the student has stopped attending since you last
                        reported attendance. You do not have to go to the Attendance Reporting screen.
                      
                     
                      If the student is to receive a final grade of F or U, you  SHOULD  enter a stopped-attending date if the grade is a result of the student's having stopped
                        attending.
                      
                     
                      
                        
                         Enter your date in this format: MM/DD/YYYY. You must enter the slashes, and you must
                           enter leading zeros for the day and month when needed (ex: "02/28/2015").
                         
                        
                         You cannot enter a date in advance of the current day's date. 
                        
                         You should not enter a date prior to the beginning of the part of term  UNLESS  you now want to indicate that a student "Never Attended." Enter such a date in the
                           Last Attend Date column and click [Submit]; the status in the "Reported Attend" column
                           will change to "Never."
                         
                        
                      
                     
                      When you have finished recording grades, click the [Submit] button at the bottom of
                        the screen. Your class is not considered graded until each student on each page of
                        the class grade roster has a grade.
                      
                     
                      Warning Messages 
                     
                      Warning messages may appear anywhere on the page, including under the student's record.
                        They are "FYI" and do not have to be "corrected," and they do not disappear when you
                        submit changes.
                      
                     
                      
                        
                          Grades cannot be entered for this section. The Attendance Hours and Last Date of Attendance
                              may be entered and saved. Grades entered will NOT be saved.    EXCH courses and 0-hour labs are not gradable. Also, the message is misleading: you
                           can enter Last Attend Date, but you should not enter Attend Hours. We do not use the
                           Attend Hours column at the U of M.   
                        
                          Please submit grades often. There is a 60 minute time limit starting at [time] on
                              [date].    Once you login to the portal and reach the Final Grades screen, you have 60 minutes
                           in which to enter grades. (Login again if you need more time).   
                        
                          The student has not withdrawn from the class.    This message appears to let you know that even though the student has stopped attending,
                           he or she has not officially withdrawn from the course. You must assign this student
                           an "F" or "U."   
                        
                      
                     
                      Error Messages 
                     
                      When you click [Submit], you will receive either a message telling you that your entries
                        have been accepted or this message telling you that you have errors to correct: At
                        least one student record is in error. Please review errors below.
                      
                     
                      A record in error is marked with an asterisk (*) to the right of the e-mail icon.
                        The error message is listed below the record in error. Correct any errors and [Submit]
                        again. Once corrected, the error will not reappear.
                      
                     
                      
                        
                          A date prior to the Class Start Date is not permitted. Enter the day before Term Start
                              Date to indicate that student Never Attended.   Enter a date prior to the part-of-term's first day of classes.   
                        
                          Future dates are not permitted. Please enter a Last Date of Attendance within term,
                              no later than today.   Enter a stopped-attending date that is equal to today's date or an earlier term date.   
                        
                          You entered a date in an invalid date format.   A stopped-attending date must be entered in this format: MM/DD/YYYY. MM = 2 digits;
                           DD = 2 digits; YYYY = 4 digits. You must enter the slashes.   
                        
                          Section is NOT gradable. The grades you entered were not saved.   EXCH courses and 0-hour labs are not gradable.   
                        
                          The Final Grade Worksheet is unavailable.   Grading is not open for the term in which the course (CRN) is being offered. You cannot
                           submit grades before grading opens. If you have questions, contact the Registrar.
                           If you need to submit a grade after the grading session has closed, submit a   Request for Grade Change   form to the Registrar.   
                        
                          Your grade submissions have been rejected. A Last Date of Attendance is not permitted
                              for grade code(s):  A . Please REMOVE the Last Date of Attendance before resubmitting your grades.  (In this example, the instructor tried to assign an  A  to a student who has stopped attending.)  You cannot assign a passing grade and enter a Last Attend Date for a student in the
                           same submission. If you do, neither the grade nor the date will be accepted. To correct
                           the error, you must either enter a passing grade with no date, or enter a failing
                           grade with an acceptable date.  You cannot assign a passing grade to a student who has an existing Last Attend Date:
                           the grade is rejected but the existing date remains. To correct the problem, you must
                           either space out the date, select the passing grade, and then click [Submit], or leave
                           the date, select a failing grade, and click [Submit].
                         
                        
                      
                     
                      Corrections 
                     
                      You may revisit the Final Grades screen to change grades you have entered  so long as the Web grading period for the term is still open . Refer to the appropriate   Dates   Deadlines calendar   for the grade entry deadline.  If the grading session has closed, you will have to
                        submit a   Request for Grade Change   form to the Registrar in order to change any grades.
                      
                     
                      When you are through grading a section, select another CRN, navigate to another service,
                        or logout.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Faculty   Staff 
                         
                            
                               
                                Faculty   Staff  
                                      Authorized Signatures  
                                      FERPA Compliance  
                                      Scheduling   Term Planning  
                                      Self Service   Online Aids  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Calendars  
                               View Academic Year calendars; semester dates/deadlines; final exam schedules. 
                            
                            
                                Transcripts  
                               Request copies of your official transcript. 
                            
                            
                                Notice!  
                                Check for alerts and reminders. 
                            
                            
                                Contact Us   
                               Need additional assistance or information? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/10/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


