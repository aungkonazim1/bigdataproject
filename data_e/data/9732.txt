Roles and Responsibilities - CAS - University of Memphis    










 
 
 
     



 
    
    
    Roles and Responsibilities - 
      	CAS
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			College of Arts   Sciences
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Areas of Study  
                          Research  
                          Scholarships  
                          Students  
                          Resources  
                          News  
                      
                      
                         
                            CAS Chair Handbook   
                            
                               
                                  
                                   Part I  
                                        
                                         Preface  
                                        
                                         Roles and Responsibilities  
                                        
                                         Survival Strategies  
                                        
                                         Departmental Culture   Chair Leadership Style  
                                        
                                         Length of Term and Transition to a New Chair  
                                     
                                  
                                   Part II  
                                         Management Introduction  
                                         Major Event Timeline  
                                         Attendance Expectations   Absence Reporting  
                                         Department Meetings  
                                         Budget Responsibilities 
                                          
                                           
                                             
                                               Understanding Your Budget  
                                             
                                               Viewing Your Budgets in Self-Service Banner  
                                             
                                               Monitoring Payrolls and Budgets in e~Print  
                                             
                                           
                                          
                                        
                                         Staff Management  
                                         Time and Leave  
                                         Annual Evaluations (Faculty and Staff)  
                                         Dealing with Complaints (Student, Faculty, and Staff)  
                                         Development (Donor Relations)  
                                     
                                  
                                   Part III  
                                         Faculty Recruitment  
                                         Joint Appointments  
                                         Tenure   Promotion  
                                         Teaching Load Guidelines  
                                         Tenure Track Faculty  
                                         Non-tenure Track Full-Time Faculty  
                                         Tenured Faculty  
                                         Part-Time Faculty  
                                         Retirement and Post-Retirement  
                                         Emeritus status for retired faculty members  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	CAS
                              	  
                          
                              	CAS Chair Handbook
                              	  
                         Roles and Responsibilities 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Chair Roles 
                     
                      Without doubt, the department chair has one of the most demanding jobs on campus.
                        You are a gate-keeper, a role model, a mentor, a liaison, and a visionary. Oh, and
                        you are also expected to excel at teaching and maintain an active research program.
                        Tired yet? The passages below from the University of Memphis Faculty Handbook delineate
                        a chair's many responsibilities.
                      
                     
                      After you read the 'official' list of duties, we'll address some chair survival strategies. 
                     
                      University of Memphis 2011 Faculty Handbook, Chapter 2 
                     
                      Some of the chair's specific administrative responsibilities include: 
                     
                      
                        
                         
                           
                            Organizing the department in such a way that the total program will be served effectively 
                           
                         
                        
                         
                           
                            Developing a set of department policies, supplementary to and consistent with those
                              of the college and University
                            
                           
                         
                        
                         
                           
                            Providing leadership in departmental planning 
                           
                         
                        
                         
                           
                            Assisting in developing a continuing education program 
                           
                         
                        
                         
                           
                            Developing and administering the department's budget 
                           
                         
                        
                         
                           
                            Supervising the department's secretarial and support staff 
                           
                         
                        
                         
                           
                            Preparing class schedules and teaching assignments 
                           
                         
                        
                         
                           
                            Supervising and managing the physical facilities under the department's jurisdiction 
                           
                         
                        
                         
                           
                            Promoting positive interpersonal relations 
                           
                         
                        
                         
                           
                            Serving as a link between the department and the university administration 
                           
                         
                        
                      
                     
                      The chair's faculty responsibilities include: 
                     
                      
                        
                         
                           
                            Recruiting capable faculty members (in concerted effort with the faculty and with
                              the concurrence of administrative officers)
                            
                           
                         
                        
                         
                           
                            Encouraging and rewarding excellence in teaching, research and creative endeavors,
                              and service
                            
                           
                         
                        
                         
                           
                            Assisting and encouraging faculty in securing outside funding for research 
                           
                         
                        
                         
                           
                            Evaluating faculty performance annually 
                           
                         
                        
                         
                           
                            Recommending faculty members for tenure, promotion, salary adjustments, continuing
                              education assignments, and graduate faculty memberships
                            
                           
                         
                        
                         
                           
                            Serving as a link between faculty and University administration 
                           
                         
                        
                      
                     
                      The chair's student responsibilities include: 
                     
                      
                        
                         
                           
                            Establishing an effective advising system 
                           
                         
                        
                         
                           
                            Ensuring that students meet degree requirements 
                           
                         
                        
                         
                           
                            Administering a graduate assistantship program 
                           
                         
                        
                         
                           
                            Conferring with students about policies and practices of the department, the college,
                              and the university
                            
                           
                         
                        
                         
                           
                            Assisting students in solving problems relating to academic matters 
                           
                         
                        
                         
                           
                            Assisting in student recruitment 
                           
                         
                        
                      
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         CAS Chair Handbook 
                         
                            
                               
                                Part I  
                                     
                                      Preface  
                                     
                                      Roles and Responsibilities  
                                     
                                      Survival Strategies  
                                     
                                      Departmental Culture   Chair Leadership Style  
                                     
                                      Length of Term and Transition to a New Chair  
                                  
                               
                                Part II  
                                      Management Introduction  
                                      Major Event Timeline  
                                      Attendance Expectations   Absence Reporting  
                                      Department Meetings  
                                      Budget Responsibilities 
                                       
                                        
                                          
                                            Understanding Your Budget  
                                          
                                            Viewing Your Budgets in Self-Service Banner  
                                          
                                            Monitoring Payrolls and Budgets in e~Print  
                                          
                                        
                                       
                                     
                                      Staff Management  
                                      Time and Leave  
                                      Annual Evaluations (Faculty and Staff)  
                                      Dealing with Complaints (Student, Faculty, and Staff)  
                                      Development (Donor Relations)  
                                  
                               
                                Part III  
                                      Faculty Recruitment  
                                      Joint Appointments  
                                      Tenure   Promotion  
                                      Teaching Load Guidelines  
                                      Tenure Track Faculty  
                                      Non-tenure Track Full-Time Faculty  
                                      Tenured Faculty  
                                      Part-Time Faculty  
                                      Retirement and Post-Retirement  
                                      Emeritus status for retired faculty members  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply now for an Undergraduate Degree  
                               Pursue your dream, broaden your career choices and gain the confidence you need to
                                 succeed
                               
                            
                            
                                Offering Master's and Doctoral Degrees  
                               Enhance your knowledge, skills and experience 
                            
                            
                                Community Engagement  
                               Teaching and serving the Memphis and Mid-South community through programs and events 
                            
                            
                                Contact Us  
                               Dean's Office Contacts, Advising, Department Chairs... 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


