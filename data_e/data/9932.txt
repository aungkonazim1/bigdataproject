Creating analytic rubrics | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Creating analytic rubrics | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Rubrics    »    Rubrics Basics   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Creating analytic rubrics                       
        
        
       
        
     
              
	Analytic rubrics allow you to assess a Competencies activity or ePortfolio item based on more than one criterion in a single rubric. With analytic rubrics, levels of achievement are displayed in columns and your assessment criteria are displayed in rows.
 

 
	Analytic rubrics may use a points, custom points or text only scoring method. Points and custom points analytic rubrics may use both text and points to assess performance; with custom points, each criterion may be worth a different amount of points. For both points and custom points an overall score is provided based on the total number of points achieved. The overall score determines whether the activity is achieved.
 

 
	Create an analytic rubric
 

  
		On the Rubrics page, click  New Rubric .
	 
	 
		Enter a  Name  and  Description .
	 
	 
		Select a  Rubric Status  from the drop-down list.
	 
	 
		Select "Analytic" from the  Rubric Type  drop-down list.
	 
	 
		Select how many achievement levels you want the rubric to have in the  Initial # of Levels  field.
	 
	 
		Select how many criteria you want to break your evaluation down by in the  Initial # of Criteria  field.
	 
	 
		Select a  Scoring Method .
	 
	 
		Select whether you want the rubric to be associated with  Competencies  and/or  ePortfolio .
	 
	 
		If you are creating a rubric from an org unit above a course in your organization's hierarchy, such as a department or from the organization level, and want to share it with child org units, click  Add Org Units  and select the org units to share with.
	 
	 
		Click  Save .
	 
  
	Define analytic rubric achievement levels and criteria
 

  
		On the Rubrics page, click    Edit Levels and Criteria  from the context menu of the analytic rubric you want to define achievement levels and criteria for.
	 
	 
		Click    Edit Criterion  from the context menu of a criterion you want to modify.
	 
	 
		Enter a  Criterion Name .
	 
	 
		Click  Save .
	 
	 
		Repeat Steps 2-4 for other criteria.
		 
			 Note  Click  Add Criteria Group  if you want to create a criteria made of subsections of existing criteria.
		 
	 
	 
		Click    Edit Level  from the context menu of an achievement level you want to modify. We recommend that you start with the lowest achievement level and work your way up.
	 
	 
		Enter a  Level Name .
	 
	 
		If you are creating an analytic rubric that uses a points scoring method, enter a  Score (points)  for the level. For custom points, you may enter a different score (points) for each criterion.
	 
	 
		Enter a  Description  of what is required to achieve the level for each criterion. Achievement level descriptions help evaluators determine which level best reflects a user's achievement. The more detailed your descriptions are, the more consistent evaluations will be.
	 
	 
		Enter a standard  Feedback  for each level. Standard feedback appears to users who achieve the level, and it is an easy way to communicate a rubric's evaluation methodology.
	 
	 
		Click  Save .
	 
	 
		Repeat Steps 6-11 for other levels.
	 
      Audience:     Instructor       

    
           

                   ‹ Creating holistic rubrics 
        
                   up 
        
                   Adding rubric achievement levels , criteria, or criteria groups › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Rubrics  
  
      Rubrics Basics    Understanding Rubrics    Accessing Rubrics    Creating holistic rubrics    Creating analytic rubrics    Adding rubric achievement levels , criteria, or criteria groups    Editing rubrics, achievement levels, criteria, or criteria groups     Managing rubric sharing properties    Managing rubric status settings    Copying rubrics    Reordering rubric achievement levels or criteria    Deleting rubrics    Deleting rubric achievement levels, criteria, or criteria groups    Viewing rubric statistics      Using Rubrics in Competencies    Using Rubrics in ePortfolio    
                  
           
         

       
     

    
    
    
   
 
   
 
