Enrollment Update for Campus &#8211; The University of Memphis President&#039;s Blog   


 

 
 
 
 


 
 
 
 

 



 Enrollment Update for Campus   The University of Memphis President s Blog 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 



 
 
  
 
 
 

 
 
 
 
			

         
 		
		



 
 
 
 

 

 


  Skip to content  




 

	
	 

		
		 

			
			 

				
						  The University of Memphis President s Blog  

					
			  

		  

		
		 

			 Primary Menu 
			 Menu 

			
			 
				 
					 
				 
					 Search for: 
					 
				 
				 
			 				 
			 

			 

			  
  Home    About the President  
  

		  

		 

		
		
			
				 

			
		
		 

		
	  

	
	 
	 

		
		 

			
			
	
	 

		
		
		 

			
			 Enrollment Update for Campus 
			
			 

				   Author   Jeanine Hornish Rakow     Published on    September 2, 2014  September 2, 2014     Leave a comment  
			  

			
		  

		
		 

			 Dear Colleagues: 
 Welcome back to a new academic year.  I hope that the summer months were productive and enjoyable.  A win over APSU was a nice way to start the week, despite the rain.  As I’m sure you’ve read, we’ve had some good news over the summer months across a number of fronts; including, faculty activities/awards, grant funding, and significant gifts to the U of M.  I’ll continue to share details related to several gifts over the next few weeks, including the launch of a targeted capital campaign for our Lambuth campus.  The news is promising on the enrollment side.  Given the importance of enrollment to our overall budgeting process, it is hoped that these updates will prove educational.  We also hope to transition to an enrollment strategy that clearly differentiates budget neutral (or budget negative) categories like dual enrollment (we have about 900 dual enrollment students who do not pay full tuition).  Last year we had dual enrollment of 815 high school students.  This year we have 900 in classes, but because of a new application/registration procedure have only processed about 345 to date. We’ll have the other 555 in the system by week’s end. 
 We have completed the second “purge” (identifying students who have not paid for the semester), so these enrollment numbers are likely to be reasonably stable. As you know, total enrollment fluctuates a bit from week to week depending on a host of variables.  As mentioned above, the numbers summarized below are promising for several reasons and it is critical to put these numbers in context.  Over the course of the last two years, the U of M has increased the number of graduates by 392 students over the record highs of previous years, with last year’s total being a record.  These historic graduation numbers, coupled with several years of low enrollment, resulted in a total decline in enrollment of 1,245 students from Fall 2011 to Fall 2013.  The net impact of higher graduation rates, coupled with two years of notable enrollment declines, reduces the total number of available continuing students. It’s important to recall that 62 cents of every dollar in our budget is generated from tuition and fee revenue.  An added complication is that the formula funding model was not fully funded by the Tennessee Legislature this year, compounding the problem (with a net loss to the U of M of approximately $2.6 million).  The intersection of these issues certainly challenges us to retain existing students at remarkably higher rates and recruit larger numbers of bright, capable incoming students. 
 As of late last week, following is a summary of current enrollment (Note: The categories referenced below do not include all enrollment categories, such as: audit students, non-degree students, readmits, transient students, certificate students, special status students, etc.): 
 
 
 
 First-Time Freshman: 
 2,368, an increase of 220 students over the fall enrollment for 2013-14 
 
 
 Total Undergraduates 
 17,258 (after all dual enrollment students have registered), a decrease of 34 students from last year 
 
 
 Total Graduate Students 
 3,769 (a decrease of 227 over last year.  Our first year doctoral students increased.  Some of the decline in graduate numbers are a function of the increased graduation numbers referenced above, along with a shift of some funds to targeted doctoral programs to enhance our research infrastructure and related productivity) 
 
 
 Total Law Students 
 337 (a decrease of 25 over last year. The first year law class grew to 121 this year. Also recall that last year’s graduating class was among the largest in recent history at 127) 
 
 
 Total Enrollment Across All Categories 
 21,356 ( a decrease of 292 from last year) 
 
 
 
 In addition to the summary numbers listed above, let me share arguably the most critical statistic from TBR.    After the 3 rd  day of classes our total Full Time Equivalent undergraduate students stood at 16,367, a net increase of .3%.    Please remember this is our full-time equivalent (not just total headcount). We fully expected to see a noticeable drop in our FTE given our significant increases in graduation numbers, along with significant reductions in total enrollment over the last two years. In short, the increase in undergraduate FTE is a good thing.  It is critical for us to start to move away from just reporting total headcount since this includes dual enrollment numbers (which are budget neutral or slightly negative) and part-time students which pay reduced tuition, both of which do not translate in meaningful ways for our overall budget picture. We will transition to reporting that more accurately reflects our budget model, including: Total Undergraduate Full-time Equivalents (FTE) and Total Graduate Students.  Additionally, we will introduce a mechanism that recognizes tuition paying graduate students versus those receiving waivers and stipend support. 
 The increase in overall undergraduate FTE suggests an increase in retention efforts, certainly a very good sign.  These numbers reflect that the hard work by many is paying off.  As we sort through implications for our overall budget I’ll share details as they emerge. 
 Thank you for your hard work. I look forward to an exciting and enjoyable year.  Go Tigers. 
 M. David Rudd, President 

		  

		
		 

			  Published on    September 2, 2014  September 2, 2014      Author   Jeanine Hornish Rakow   
			
		  

		
	  

	
				
	 
		 Post navigation 
		    Previous article:  Two Philosophy Professors Win Independent Prestigious Templeton Foundation Awards      Next article:  U of M Students Make Big Impression at the Edinburgh International Fringe Festival    
	 
				

 

	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  


			
			
		  

		
	  


	
		
		
		 

		 Main Sidebar 

			
			  
				 
					 Search for: 
					 
				 
				 
			  		 		 Recent Posts 		 
					 
				 UofM Quality Indicator Score 
						 
					 
				 University of Memphis Research Foundation Announces Grand Opening of Student-Operated FedEx Call Center 
						 
					 
				 Pause to Grieve 
						 
					 
				 University of Memphis Magazine: Fall 2017 
						 
					 
				 Campus Update 
						 
				 
		 		  Recent Comments      Archives 		 
			  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  July 2014  
	  June 2014  
	  May 2014  
	  April 2014  
		 
		   Categories 		 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 
			
		  

		
		  

	
	
	 

		
		 Footer Content 

		 
		
			
				
				
								
			
		  
	
		 

			
			
			Using  Tiny Framework     
			
			   Log in  

		  
		
		 

			
			

 

	 Social Links Menu 

	      i class= fa fa-twitter   /i    
  
  

			
		  

		
	  

	
  


        

        









		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 
 