Study Carrel Policy - Libraries - University of Memphis  Study Carrel  










 
 
 
     



 
    
    
    Study Carrel Policy - 
      	Libraries
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			University Libraries
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Services  
                          Resources  
                          Instruction  
                          Special Collections  
                          Technology  
                      
                      
                         
                            About University Libraries   
                            
                               
                                  
                                   Hours  
                                   News   Events  
                                   Jobs  
                                   Staff Directory  
                                   Organization Chart  
                                   Library Faculty and Staff Portal  
                                   Alternate Use of Library Space Request  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Libraries
                              	  
                          
                              	About University Libraries
                              	  
                         Study Carrel Policy 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Study Carrel Policy 
                     
                      
                        
                          Priority of Assignment  
                        
                          User Responsibilities  
                        
                          Libraries' Responsibilities  
                        
                          Application Procedures  
                        
                          Libraries' Material Policies  
                        
                          Carrel Inspections  
                        
                          Carrel Keys  
                        
                      
                     
                      Study carrels on the  third  and  fourth  floors of the Ned R. McWherter Library are available for individual assignment to
                        support study and research by eligible library users. Study carrels are equipped with
                        electrical outlets, wireless Internet access, desks, and cabinets. Carrels are not
                        to be used for office or conference space. Applications for carrels are available
                        from the Check-Out Desk and  online . The use of study carrels is governed by the following policies:
                      
                     
                      
                        
                               Priority of Assignment     Carrel assignments will be made according to the following priorities on a first-come,
                           first-served basis: Check-out periods for study carrels range from one semester to
                           one academic year. 
                           
                            
                              
                               Graduate students 
                              
                               Faculty 
                              
                               Administrators 
                              
                               Visiting scholars 
                              
                               Retired faculty 
                              
                               Others with special needs   
                              
                            
                           
                         
                        
                               User Responsibilities    A carrel is to be used exclusively by the person to whom it has been assigned and
                           cannot be loaned or transferred to another party.  Nothing is to be permanently posted or attached in any way to walls or furniture.
                           The inside and outside of the carrel door glass should not be blocked by posters,
                           papers, a filing cabinet, etc.  Personal equipment such as computers, iPads, Kindles, iPods, calculators, study lamps,
                           or desk clocks is permitted. Electrical appliances such as microwave ovens, space
                           heaters, coffee pots and personal furniture are prohibited.  Use of tobacco products, illegal drugs, or alcoholic beverages is not allowed in study
                           carrels. (University Policy:  UM1614 Rev. 1 ;  UM1563 Rev. 1 )  Possession of any weapon in the study carrels is strictly prohibited (University Policy:
                            UM1505 ).  Personal items of value left in the carrel should be locked in the cabinets provided
                           for this purpose. The Libraries will not be responsible for damage or loss of any
                           personal property or other items left in the carrel.   
                        
                               Libraries' Responsibilities       Assignment of study carrels is intended to accommodate the need for a place where
                           faculty or students can conduct uninterrupted research. Carrels not in regular use
                           should be surrendered.  Carrel assignments are confidential.  Carrels may only be used during McWherter Library’s operating  hours . Carrels may not be used as a mailing address location. Mail addressed to a carrel
                           will be returned to the sender. Messages will not be delivered to carrels, nor will
                           carrel holders be paged.   
                        
                               Application Procedures    Library users who are eligible may apply for an individual library carrel. An application/registrationform
                           may be picked up at and returned to the Check Out Desk. Applications are also available
                            online here .      
                        
                               Libraries' Material Policies    All circulating library materials in the carrel must be properly checked out to the
                           assigned occupant. All items will be subject to regular loan policies and overdue
                           fines and fees.  Periodicals, Reference books, and other non-circulating materials should not be left
                           in the study carrel.   
                        
                               Carrel Inspections    Libraries staff will periodically inspect carrels. All library-owned materials will
                           be removed if not properly checked out or if overdue. Fines and fees will be assessed
                           on overdue materials. Any prohibited materials will be removed and held at the Check-Out
                           Desk.   
                        
                               Carrel Keys    Carrel keys may not be duplicated or loaned to other individuals. Keys must be returned
                           to the Check-Out Desk at the end of the loan period. If keys are not returned, a fee
                           of $25 per key will be charged accordingly.  Lost keys should be reported immediately to the Check-Out Desk. Replacement keys will
                           be made available after payment of required charges.  Send all inquiries to Sharon Tucker, via email at  stucker@memphis.edu  or for additional assistance, call (901) 678-8248.
                         
                        
                      
                     
                        Revised, Spring 2011  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         About University Libraries 
                         
                            
                               
                                Hours  
                                News   Events  
                                Jobs  
                                Staff Directory  
                                Organization Chart  
                                Library Faculty and Staff Portal  
                                Alternate Use of Library Space Request  
                            
                         
                      
                      
                      
                         
                            
                                Ask-a-Librarian  
                               Got a question? Got a problem? Ask Us! 
                            
                            
                                Interlibrary Loan  
                               Request books, articles, and other research materials from other libraries. 
                            
                            
                                Reserve space  
                               Group study and presentation spaces available in the library. 
                            
                            
                                My Library Account  
                               Review your library account for due dates and more. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


