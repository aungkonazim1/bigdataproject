ICL Faculty Resources - ICL - University of Memphis    










 
 
 
     



 
    
    
    ICL Faculty Resources - 
      	ICL
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Instruction and Curriculum Leadership 
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Programs  
                          Students  
                          Faculty  
                          ICL Faculty Research  
                          A-Z List  
                      
                      
                         
                            ICL Faculty   
                            
                               
                                  
                                   Faculty Resources  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	ICL
                              	  
                          
                              	Faculty
                              	  
                         ICL Faculty Resources 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      ICL Faculty Resources 
                     
                      Faculty Evaluations 
                     
                      
                        
                         
                           
                             Faculty Evaluation - Academic Affairs  
                           
                         
                        
                      
                     
                      Faculty Mentoring Program 
                     
                      
                        
                          Mentoring Guide  
                        
                          Mentoring Checklist  
                        
                      
                     
                      Faculty Forms 
                     
                      
                        
                          Cash Reimbursement  
                        
                          Extra Compensation  
                        
                          International Travel Form  
                        
                          Overtime Outside Department  
                        
                          Peer Observation Measure  
                        
                          Planned Absence  
                        
                          Summer Compensation Cheat Sheet  
                        
                          Summer Compensation Quick Guide  
                        
                          Travel Claim Expense  
                        
                          Travel Reimbursement Process  
                        
                      
                     
                      Graduate Documents 
                     
                      
                        
                          Documents  
                        
                          Faculty Guide to Course Change Revision  
                        
                          Faculty Guide to EdD Graduate Students Process  
                        
                          Faculty Guide to EdS Graduate Students Process  
                        
                      
                     
                      Grants   Contracts 
                     
                      
                        
                          Budgeting Basics  
                        
                      
                     
                      Information about the University of Memphis and the Memphis Metro Area 
                     
                      
                        
                          President's Strategic Plan  
                        
                          City of Memphis  
                        
                          Shelby County Government  
                        
                          Shelby County Schools  
                        
                      
                     
                      Information   Policies 
                     
                      
                        
                          Office of the Provost  
                        
                          U of M Faculty Handbook  
                        
                          Policies and Procedures  
                        
                          Semester Calendars  
                        
                          Student Communication Form  
                        
                      
                     
                      Online Training Guides 
                     
                      
                        
                          Banner Guide  
                        
                          Live Text Faculty Guide  (assistance with Live Text, please contact  Dr. Mary Ransdell )
                         
                        
                          Dual Services Instructions  
                        
                          Spectrum Finance Program Guide  
                        
                          Spectrum HR Program Guide  
                        
                          Tigerbuy Program Guide  
                        
                      
                     
                      Research   Funding 
                     
                      
                        
                          Research Support Services  
                        
                      
                     
                      Syllabus Resources 
                     
                      
                        
                          COE Generic Course Syllabus Format  
                        
                          COE Teacher Education Course Syllabus Format  
                        
                          Course Example  
                        
                          Meaningful Assignments 1  
                        
                          Meaningful Assignments 2  
                        
                          Assessment Example  
                        
                          Academic Policies  
                        
                          Dispositions Example  
                        
                          Norms Example  
                        
                      
                     
                      Tenure   Promotion 
                     
                      
                        
                          T   P Forms  
                        
                          T   P Calendar  
                        
                          T P Instructions  
                        
                          Instruction and Curriculum Leadership T   P Guide  
                        
                      
                     
                      Training Opportunities 
                     
                      
                        
                          Learning Opportunities for University of Memphis Faculty and Staff  
                        
                          Human Resources Professional Development  
                        
                          University of Memphis Professional and Continuing Education  
                        
                      
                     
                      Other Resources 
                     
                      
                        
                          Alphabetical Listing of Forms  
                        
                          Campus Map  
                        
                          Help Desk/Workorders  
                        
                      
                     
                       NOTE : Some documents attached to the links are pdf files. You'll need Adobe Reader to
                        view these documents. If you don't have Adobe Reader installed on your computer, please
                        click the "Adobe Reader" icon below to download the latest FREE version.
                      
                     
                       Get Acrobat Reader  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         ICL Faculty 
                         
                            
                               
                                Faculty Resources  
                            
                         
                      
                      
                      
                         
                            
                                ICL Degrees  
                               Broaden your career choices by gaining the confidence you to need to succeed 
                            
                            
                                Apply Now  
                               Every journey starts with a step, now take yours! 
                            
                            
                                Advising  
                               Your academic success begins with advising. 
                            
                            
                                Contact Us  
                               Questions? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


