Undergraduate Colloquium - Computer Science - University of Memphis    










 
 
 
     



 
    
    
    Undergraduate Colloquium - 
      	Computer Science
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Computer Science
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          News  
                          Future Students  
                          Current Students  
                          Research  
                          People  
                          Outreach  
                      
                      
                         
                            News   Events   
                            
                               
                                  
                                   Department News  
                                   Upcoming Events  
                                   Colloquium  
                                   Undergraduate Colloquium  
                                   Newsletters  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Computer Science
                              	  
                          
                              	News   Events
                              	  
                         Undergraduate Colloquium 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Undergraduate Colloquium 
                     
                      The Undergraduate Colloquium is held during the Spring and Fall semesters.  Topics
                        include relevant technical skills as well as career advice.
                      
                     
                      Spring 2017 
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  M O N Feb. 6
                                  
                                 
                               
                              
                               
                                 
                                  Pizza Party and Career Opportunities 
                                  Pete Mason and Erin Vickers, Sedgwick 10:20-11:15 am, Dunn Hall 233
                               
                              
                            
                           
                         
                        
                      
                     
                      Fall 2016 
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  M O N Oct. 17
                                  
                                 
                               
                              
                               
                                 
                                  Open MCT: Open-Source, Web-Based Mission Control 
                                  Victor Woeltjen, MORI Associates 11:30 am-12:25 pm, Dunn Hall 103
                               
                              
                            
                           
                         
                        
                      
                     
                      Spring 2016 
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  M O N Apr. 25
                                  
                                 
                               
                              
                               
                                 
                                  On the Crossroads of Cyber Security and Big Data Analytics 
                                  Lu Wang, Manager of Information Security, FedEx Services 11:30 am, FIT 225
                               
                              
                            
                           
                            
                              
                               
                                 
                                  M O N Mar. 28
                                  
                                 
                               
                              
                               
                                 
                                  The Real Curriculum: What You Should Actually Be Learning in College 
                                  Mark Cooper, Ph.D., Technical Fellow, FedEx Services 11:30 am, FIT 225
                               
                              
                            
                           
                         
                        
                      
                     
                      Fall 2015 
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  W E D Nov. 18
                                  
                                 
                               
                              
                               
                                 
                                  Application Security: Building Security In 
                                  Thomas Davis, Manager, Information Security, The ServiceMaster Company 11:30 am-12:15 pm, Dunn Hall 123   Video (part 1)  |  Video (part 2)  
                              
                            
                           
                            
                              
                               
                                 
                                  W E D Nov. 11
                                  
                                 
                               
                              
                               
                                 
                                  The Information Security Workforce 
                                  Juan Soto, First Tennessee 11:30 am-12:15 pm, Dunn Hall 123   Video (part 1) |   Video (part 2)  
                              
                            
                           
                            
                              
                               
                                 
                                  W E D Oct. 21
                                  
                                 
                               
                              
                               
                                 
                                  Cyber Security: Governance, Risk, and Compliance 
                                  Brian Burns, CISSP, CISA, AutoZone 11:30 am-12:15 pm, Dunn Hall 123   Video (part 1)  |  Video (part 2)  
                              
                            
                           
                            
                              
                               
                                 
                                  W E D Oct. 14
                                  
                                 
                               
                              
                               
                                 
                                  Getting the Most Out of Your Integrated Development Environment 
                                  Ernest McCracken, IBM / University of Memphis 11:30 am-12:15 pm, Dunn Hall 123   Video (part 1)  |  Video (part 2)   
                              
                            
                           
                            
                              
                               
                                 
                                  W E D Sept. 16
                                  
                                 
                               
                              
                               
                                 
                                  How to Become a Medical Bioinformaticist 
                                  Dr. Fatih Sen, Le Bonheur Children's Hospital 11:30 am-12:15 pm, Dunn Hall 123   Video (part 1)  |  Video (part 2)   
                              
                            
                           
                            
                              
                               
                                 
                                  T H U R S Sept. 10
                                  
                                 
                               
                              
                               
                                 
                                  Cyber Security: Explaining the Issues to Executive Management 
                                  Eric Spiegel, Dixon Hughes Goodman 1:00 pm, FIT 405  Slides  |  Video (part 1)  |  Video (part 2)  
                              
                            
                           
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         News   Events 
                         
                            
                               
                                Department News  
                                Upcoming Events  
                                Colloquium  
                                Undergraduate Colloquium  
                                Newsletters  
                            
                         
                      
                      
                      
                         
                            
                                Career Paths  
                               Internship information and job postings for students 
                            
                            
                                Degree Programs  
                               Information on our degree and certificate programs 
                            
                            
                                Courses  
                               Recent syllabi for our courses 
                            
                            
                                Contact Us  
                               Whom to contact about what 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


