Editing an email folder | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Editing an email folder | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Email    »    Understanding email folders   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Editing an email folder                       
        
        
       
          
     
           Printer-friendly version       
	The amount of information you can edit depends on the type of folder you are editing. While you can change the name, the parent folder, and delete folders you have created, you can only change the name of system-created folders. You cannot delete system-created folders (Trash, Draft, Inbox, Address Book), nor can you change their parent folder.
 
 Edit an email folder 
  
		On the Folder Management page, click    Edit  from the context menu of the folder you want to edit.
	 
	 
		Update the folder.
	 
	 
		Click  Save .
	 
      Audience:    Learner      

    
           

                   ‹ Adding an email folder 
        
                   up 
        
                   Reordering the email folder list › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Email  
  
      Email basics    Understanding email folders    Email folders    Accessing email folders    Accessing the folder management area    Adding an email folder    Editing an email folder    Reordering the email folder list    Moving an email message to a folder    Deleting an email folder      Understanding the address book    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
