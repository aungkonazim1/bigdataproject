Internships - Kemmons Wilson School of Hospitality &amp; Resort Management - University of Memphis    










 
 
 
     



 
    
    
    Internships - 
      	Kemmons Wilson School of Hospitality   Resort Management
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Kemmons Wilson School of Hospitality   Resort Management 
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Hospitality  
                          Sport   Leisure  
                          Students  
                          Careers  
                          Faculty  
                          Contact Us  
                      
                      
                         
                            Careers   
                            
                               
                                  
                                   Hospitality and Resort Management  
                                         Careers  
                                         Success Stories  
                                         Current Opportunities  
                                     
                                  
                                   Sport and Leisure Management  
                                        
                                         Careers  
                                        
                                         Internships  
                                        
                                         Success Stories  
                                     
                                  
                                   Employers  
                                   Events  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Kemmons Wilson School of Hospitality   Resort Management
                              	  
                          
                              	Careers
                              	  
                         Internships 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Internships 
                     
                        
                     
                        Interns in Action         Senior Capstone Experience  Handbook        Experiential Learning       Earn Class Credit for Real-Life Experience(SLS 2605)             
                     
                       Driven by Doing:  
                     
                      Senior Capstone Experience 
                     
                      So you want to work in sports? 
                     
                      The business of sport is incredibly alluring, but extremely competitive. Among the
                        most valuable facets of the Sport and Leisure Management program is its connection
                        with the sport industry locally in the 901 and across the country. These ties impact
                        the students in numerous ways, most notably with experiential learning and their Senior
                        Capstone Experience. As a part of their degree requirements, Sport Management students
                        are required to complete a 12-credit Capstone providing an opportunity to gain valuable,
                        hands-on experience in the sports industry before graduation.
                      
                     
                      The Senior Capstone Experience is the culmination of one's academic experience as
                        a Sport and Leisure Management student at the University of Memphis. The planning
                        process begins as soon as the student begins the program as a freshman and concludes
                        with a full-semester experience with a specific sport entity.
                      
                     
                      The Senior Capstone Experience requires the student to work with a sport-related organization
                        for a minimum of 400 hours over the course of one semester, gaining experience in
                        areas such as sales, marketing, finance, event management, administration, communication,
                        law, youth sport, community sport, and more. The student will gain valuable hands-on
                        experience in a variety of areas under the supervision of qualified sport industry
                        practitioners and executives.
                      
                     
                      The Senior Capstone Experience process begins as soon as you are declared a Sport
                        and Leisure Management student and continues through the completion of your Senior
                        Capstone Experience and your final presentation. An outline of the preliminary process
                        is as follows:
                      
                     
                      Senior Capstone Experience Eligibility: 
                     
                      
                        
                         Complete all core courses as outlined on the Progress Checklist in the Senior Capstone
                           Experience Handbook
                         
                        
                         Attain senior academic standing by completing a minimum of 90 credit hours toward
                           graduation
                         
                        
                         Have no more than 18 credits remaining before graduation (excluding the 12 Senior
                           Capstone Experience credits)
                         
                        
                         Complete SLS 4605 during one of the final two semesters 
                        
                         It is encouraged and recommended that you complete at least 2 internships/experiential opportunities
                           outside of classroom prior to the Senior Capstone Experience
                         
                        
                      
                     
                        
                     
                      Freshman Year: 
                     
                      
                        
                         Attend a Senior Capstone Experience Presentation (recommended) 
                        
                         Develop a resume and cover letter 
                        
                         Develop your personal portfolio 
                        
                         Explore interest and general internship sites 
                        
                         Network with University of Memphis alumni in the sport industry 
                        
                         Obtain a summer internship or volunteer at sport events 
                        
                      
                     
                        
                     
                      Sophomore Year: 
                     
                      
                        
                         Complete SLS 2105 – Foundations of Sport and Leisure Management 
                        
                         Update your resume, your cover letter, and enhance your program portfolio 
                        
                         Expand interests and participate in sport- or leisure-related internships on campus
                           or in the Memphis community during the academic year
                         
                        
                         Obtain a summer internship or volunteer at sports events 
                        
                      
                     
                        
                     
                      Junior Year: 
                     
                      
                        
                         Attend a Senior Capstone Experience Information Session 
                        
                         Meet with the Senior Capstone Experience Coordinator 
                        
                         Enhance your personal portfolio 
                        
                         Work as an intern on-campus or in the Memphis community during the academic year 
                        
                         Obtain a summer internship or volunteer at sport events 
                        
                         Attend career services drop-in hours for resume and sample cover letter review, have
                           Career Services complete Career Services SLS Verification Form
                         
                        
                         Submit Pre-Senior Capstone Experience Application Packet by April 1 
                        
                      
                     
                        
                     
                                                   SEMESTER PRIOR TO YOUR SENIOR CAPSTONE EXPERIENCE 
                     
                      
                        
                         Research, find, and obtain a Senior Capstone Experience that matches your career goals.  
                        
                         Attend a mandatory Senior Capstone Experience meeting. Senior Capstone Experience procedures,
                           requirements, logistics, and other miscellaneous details will be discussed. Failure to
                           attend a mandatory Senior Capstone Experience meeting will jeopardize your eligibility
                           for Senior Capstone Experience enrollment.
                         
                        
                         Request a Confirmation Letter from your Site Supervisor on company letterhead. The
                           letter must specify the Senior Capstone Experience offer, start and end dates, approximate
                           start and end times each day, a position description, general responsibilities and
                           contact information for your Site Supervisor. This letter can be emailed to Senior
                           Capstone Experience Coordinator. This letter must be received prior to beginning your
                           Senior Capstone Experience.
                         
                        
                         Review and sign the Senior Capstone Experience Learning Agreement form.  
                        
                         Submit each of the following Senior Capstone Experience forms via email to Senior
                           Capstone Experience Coordinator. Each form must be complete and can be returned electronically (see
                           website for forms):
                         
                        
                      
                     
                                   - Confirmation Letter from the organization on organization letterhead 
                     
                                   - Senior Capstone Experience Learning Agreements 
                     
                                   - Student Information form 
                     
                                   - Draft SMART Goals 
                     
                      Once you have completed all the steps listed above, and the forms have been approved
                        you will be given permission for SLS 4605 – Senior Capstone Experience.
                      
                     
                      NOTE: No student will be eligible to register for SLS 4605 after the deadline (one
                        week after classes begin).
                      
                     
                                                PRE-SENIOR CAPSTONE EXPERIENCE APPLICATION PACKET 
                     
                       Eligibility Requirements – Part A : A description of the prerequisite criteria and the registration prerequisites for
                        SLS 4605. This form must be signed and dated indicating your understanding of the
                        criteria and prerequisites.
                      
                     
                       Eligibility Requirements – Part B:  Your contact information, a checklist of courses completes, identification of semester
                        in which you plan to take SLS 4605 – Senior Capstone Experience. This form is completes
                        by you and must be signed by the student's Faculty Advisor.
                      
                     
                       Resume:  An updated resume with all previous internship, industry, and activity/program experiences leading
                        to the Senior Capstone Experience reviewed by Career Services.
                      
                     
                       Cover Letter:  Sample cover letter written to an individual within an organization of interest in
                        a specific geographical location and specific spot sector (e.g., marketing, communications,
                        sales).
                      
                     
                       Career Services Verification Form:  Confirming that they have reviewed your resume/cover letter.
                      
                     
                       Bio Sheet:  Providing a brief summary of your background and interests.
                      
                     
                       
                     
                      NOTE: During the Spring Semester of the junior year, an administrative registration
                        hold will be in effect until the Pre-Senior Capstone Experience Application Packet
                        has been received and approved.
                      
                       
                     
                      
                        
                          Eligibility Requirements – Part A  
                        
                          Eligibility Requirements – Part B  
                        
                          Learning Agreement  
                        
                          Progress Checklist  
                        
                          Student Information  
                        
                          Bio Sheet  
                        
                          Site Supervisor Evaluation  
                        
                          Student Self Evaluation  
                        
                          Grade Breakdown  
                        
                          Timesheet  
                        
                          Weekly Logs  
                        
                          Monthly Logs  
                        
                          Final Portfolio Evaluation  
                        
                          Final Presentation Evaluation  
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Careers 
                         
                            
                               
                                Hospitality and Resort Management  
                                      Careers  
                                      Success Stories  
                                      Current Opportunities  
                                  
                               
                                Sport and Leisure Management  
                                     
                                      Careers  
                                     
                                      Internships  
                                     
                                      Success Stories  
                                  
                               
                                Employers  
                                Events  
                            
                         
                      
                      
                      
                         
                            
                                Apply to the KWS Program  
                               Choose the degree program that's right for you 
                            
                            
                                Academic Advising  
                               Schedule an appointment with your advisor 
                            
                            
                                Research Highlights  
                               KWS faculty are leaders in their fields of study 
                            
                            
                                Contact Us  
                               Questions? Our team can help. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/21/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


