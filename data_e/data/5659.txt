Priority Permit Parking - Parking &amp; Transportation Services - University of Memphis    










 
 
 
     



 
    
    
    Priority Permit Parking - 
      	Parking   Transportation Services
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Parking and Transportation Services
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Directory  
                          Permit/Other Parking  
                          Lambuth/Millington Parking  
                          Resources  
                      
                      
                         
                            Permit and Other Parking   
                            
                               
                                  
                                   General Permit Parking  
                                   Motorcycle Parking  
                                   Priority Permit Parking  
                                   Resident Permit Parking  
                                   Park Avenue Campus  
                                   University Department Parking  
                                   Visitor Parking  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Parking   Transportation Services
                              	  
                          
                              	Permit and Other Parking
                              	  
                         Priority Permit Parking 
                      
                   
                   
                       
                      
                     		
                     
                     
                      Priority Permit Parking 
                                          
                       Don’t wait in Line.  Go Online !  
                     
                      Priority Gate Access Parking is available each semester on a first come, first serve
                        basis. Availability of parking lots differs each semester. Check with the Parking
                          Transportation Services Office for information on space availability. Faculty/staff
                        may purchase priority permits year round on a monthly, semester, or yearly basis depending
                        on employment status. Students may only purchase into a Priority lot for a given semester
                        during the permit sales window. Evening only access is also available for faculty/staff
                        or students to purchase. Evening access begins at 4:30 p.m.
                      
                     
                       Employee Priority Point Guidelines  
                     
                       Priority Lots  :  Lot 6 ,  Lot 7 ,  Lot 9 ,  Lot 10 ,  Lot 16 ,  Lot 17 ,  Lot 18 ,  Lot 19 ,  Lot 40 ,  Lot 43 ,  Lot 45 ,  Lot 52 , and  Lot 53  
                     
                       Parking Garages:   Lot 3 , and  Lot 40  
                     
                      
                        
                         
                           
                            
                               Parking Lots 
                               Spaces 
                            
                           
                            
                              
                                    Priority Gated Access Lot 3 (ZC Garage)  
                              
                               894 Parking Spaces   38 Accessible Parking Spaces (first level only)
                               
                              
                            
                           
                            
                              
                                    Priority Access Lot 6  
                              
                               20 Priority Parking Spaces  6 Visitor Metered Spaces  1 Accessible Parking Space
                               
                              
                            
                           
                            
                              
                                    Priority Gated Access Lot 7  
                              
                               49 Priority Parking Spaces  2 Accessible Parking Spaces  4 Loading Spaces
                               
                              
                            
                           
                            
                              
                                    Priority Gated Access Lot 9  
                              
                               11 Accessible Parking Spaces  4 Loading Zone Spaces
                               
                              
                            
                           
                            
                              
                                    Priority Gated Access Lot 10  
                              
                               27 Priority Parking Spaces  3 Accessible Parking Spaces   3 Visitor Metered Spaces
                               
                              
                            
                           
                            
                              
                                    Priority Permit Access Lot 16  
                              
                               40 Priority Parking Spaces 
                              
                            
                           
                            
                              
                                    Priority Gated Access Lot 17  
                              
                               92 Priority Parking Spaces 
                              
                            
                           
                            
                              
                                    Priority Gated Access Lot 18  
                              
                               51 Priority Parking Spaces  5 Accessible Parking Spaces
                               
                              
                            
                           
                            
                              
                                    Priority Gated Access Lot 19  
                              
                               179 Priority Parking Spaces  1 Reserved Parking Space
                               
                              
                            
                           
                            
                              
                                    Priority Gate Access Lot 40 (Innovation Garage)  
                              
                               838 Priority Parking Spaces  12 Accessible Parking Spaces
                               
                              
                            
                           
                            
                              
                                    Priority Gate Access Lot 43  
                              
                               24 Priority Parking Spaces  5 Accessible Parking Spaces  3 Loading Zone Spaces  1 Reserved Parking Space
                               
                              
                            
                           
                            
                              
                                    Priority Gate Access Lot 45  
                              
                               
                                 
                                  240 Priority Parking Spaces  5 Accessible Parking Spaces  1 Reserved Parking Space  10 Loading Zone Spaces
                                  
                                 
                               
                              
                            
                           
                            
                              
                                    Priority Gate Access Lot 52  
                              
                               44 Priority Parking Spaces  10 Accessible Parking Spaces  3 Loading Zone Spaces  1 Reserved Parking Spaces (Health Center)
                               
                              
                            
                           
                            
                              
                                    Priority Gate Access Lot 53  
                              
                               189 Priority Parking Spaces  3 Accessible Parking Spaces
                               
                              
                            
                           
                         
                        
                      
                     
                        
                     
                       Priority Gate Access Parking Fees:   
                     
                      
                        
                         
                           
                            
                                 
                               Faculty/Staff 
                               Students 
                            
                           
                            
                              
                                24 hrs Access  
                              
                               $43.70 per month (current) 
                              
                               $95.00 per semester 
                              
                            
                           
                            
                              
                                Evening Only Access    (after 4:30 p.m. )    
                              
                               $28.40 per month (current) 
                              
                               $40.00 per semester 
                              
                            
                           
                         
                        
                      
                     
                     	
                      
                   
                
                
                   
                      
                         Permit and Other Parking 
                         
                            
                               
                                General Permit Parking  
                                Motorcycle Parking  
                                Priority Permit Parking  
                                Resident Permit Parking  
                                Park Avenue Campus  
                                University Department Parking  
                                Visitor Parking  
                            
                         
                      
                      
                      
                         
                            
                                TigerPark  
                               Don’t wait in line…Go online! 
                            
                            
                                The Blue Line  
                               The Blue Line shuttle at the University of Memphis. 
                            
                            
                                Forms  
                               All of B F's forms in one place. 
                            
                            
                                Business   Finance  
                               The Division of Business   Finance at the UofM. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


