Women's Leadership Initiatives - Student Leadership &amp; Involvement - University of Memphis    










 
 
 
     



 
    
    
    Women's Leadership Initiatives - 
      	Student Leadership   Involvement
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Student Leadership   Involvement
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Student Engagement  
                          Leadership   Service  
                          Community Engagement  
                      
                      
                         
                            Leadership   Service   
                            
                               
                                  
                                   Emerging Leaders  
                                         Overview  
                                         Apply  
                                         FAQ  
                                         Current EL Information  
                                     
                                  
                                   Social Change Scholars  
                                         Overview  
                                         Apply  
                                         FAQ  
                                     
                                  
                                   Leadership Training  
                                         Tiger Leadership Institute  
                                         Leader to Leader  
                                     
                                  
                                   Community Service  
                                         Service on Saturday  
                                         Days of Service  
                                         Volunteer Odyssey  
                                     
                                  
                                   Conference and Retreats  
                                         LeaderShape Institute  
                                         LEAD Conference  
                                         Topical Conferences  
                                     
                                  
                                   Alternative Breaks  
                                     
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Student Leadership   Involvement
                              	  
                          
                              	Leadership   Service
                              	  
                         Women's Leadership Initiatives 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Women's Leadership Initiatives 
                     
                      Leadership   Service is proud to provide intentional opportunities to support the
                        leadership development of women on campus through a variety of events. These programs
                        seek to expose students to women leaders, encourage mentorship, and support the developmental
                        growth of a community of women leaders in Memphis.
                      
                     
                      Upcoming Events 
                     
                       Women in Leadership: Speed Networking Luncheon  October 20, 2017  |  12:00 pm - 2:00 pm  |  UC River Room Sponsored with UofM Alumni Association and Women in Leadership   Philanthropy
                      
                     
                       Conversational Intelligence  Oct. 27, 2017  |  11:30 am - 1:00 pm  |  UC Senate Chamber
                      
                     
                       Women's Issues in the Workplace  Nov. 10, 2017  |  12:30 pm - 2:00 pm  |  UC River Room Sponsored with UofM Alumni Association and Women in Leadership   Philanthropy
                      
                     
                       RSVP HERE  
                     
                      Note: Please RSVP using the above link for each program you are interested in attending. 
                     
                      Resources 
                     
                      We've compiled a list of online resources for women's leadership - check it out  here !
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Leadership   Service 
                         
                            
                               
                                Emerging Leaders  
                                      Overview  
                                      Apply  
                                      FAQ  
                                      Current EL Information  
                                  
                               
                                Social Change Scholars  
                                      Overview  
                                      Apply  
                                      FAQ  
                                  
                               
                                Leadership Training  
                                      Tiger Leadership Institute  
                                      Leader to Leader  
                                  
                               
                                Community Service  
                                      Service on Saturday  
                                      Days of Service  
                                      Volunteer Odyssey  
                                  
                               
                                Conference and Retreats  
                                      LeaderShape Institute  
                                      LEAD Conference  
                                      Topical Conferences  
                                  
                               
                                Alternative Breaks  
                                  
                            
                         
                      
                      
                      
                         
                            
                                Our Mission  
                               Learn about the key elements we use to enhance your college experience. 
                            
                            
                                Fraternity   Sorority Affairs  
                               Enhance your college experience by joining a fraternity/sorority! 
                            
                            
                                Tiger Zone  
                               Learn about student organizations, events   involvement opportunities. 
                            
                            
                                Contact Us  
                               Questions? Our team is here to help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/27/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


