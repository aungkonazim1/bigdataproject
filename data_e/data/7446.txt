Inserting CaptureCast presentations into Learning Environment | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Inserting CaptureCast presentations into Learning Environment | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Capture    »    Capture Central in Learning Environment   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Inserting CaptureCast presentations into Learning Environment                       
        
        
       
        
     
             

 
	 Tip If you want to include a CaptureCast presentation as a topic in Content, create a    New Document  in the Content tool and enter a title that indicates it is a CaptureCast presentation topic.
 

  
		In the HTML Editor where you want to insert your CaptureCast presentation, click    Insert Stuff .
	 
	 
		Click    Capture  in the sidebar.
	 
	 
		Click  Search  to view a list of all presentations you can insert into the course. If available, you will see the presentation name, its published date or presentation date and time, the presenter's name, and the tags added to the presentation.
		 
			 Note Contact your administrator if you do not see your presentation in the list of presentations available.
		 
	 
	 
		Select the presentation you want to insert, and click  Next .
	 
	 
		Click the    Play  icon to preview the presentation. Select the  Auto Start  check box if you want the video to play automatically after the page loads for users.
	 
	 
		Click  Insert .
	 
	 
		Save your changes.
	 
      Audience:     Instructor       

    
           

                   ‹ Configuring CaptureCast presentations with Learning Environment 
        
                   up 
        
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Capture  
  
      Participating in CaptureCast presentations    Understanding Capture components    Using Capture    Capture Station    Editing in post-production    Understanding Capture Toolbox    Capture Central in Learning Environment    Accessing Capture Central    Sharing CaptureCast presentations from Capture Portal or Capture Central    Configuring CaptureCast presentations with Learning Environment    Inserting CaptureCast presentations into Learning Environment      
                  
           
         

       
     

    
    
    
   
 
   
 
