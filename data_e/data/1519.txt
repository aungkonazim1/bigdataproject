What is Hazing? - Student Leadership &amp; Involvement - University of Memphis    










 
 
 
     



 
    
    
    What is Hazing? - 
      	Student Leadership   Involvement
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Student Leadership   Involvement
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Student Engagement  
                          Leadership   Service  
                          Community Engagement  
                      
                      
                         
                            Hazing Information   
                            
                               
                                  
                                   Overview  
                                   About Hazing  
                                        
                                         Alcohol   Hazing  
                                        
                                         Alternatives to Hazing  
                                        
                                         Dangers of Hazing  
                                        
                                         Hazing Prevention  
                                        
                                         Sanction Guidelines  
                                     
                                  
                                   What You Can Do  
                                         Take Action  
                                         Report Hazing  
                                         Request A Presentation  
                                         What Happens Next  
                                     
                                  
                                   Organizations  
                                         Overview  
                                         Band  
                                         Greek  
                                         NCAA Athletic  
                                         ROTC  
                                         Sports Clubs  
                                         Other  
                                     
                                  
                                   Resources  
                                         Faculty/Advisors  
                                         Parents  
                                         Students  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Student Leadership   Involvement
                              	  
                          
                              	Hazing
                              	  
                         What is Hazing? 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Dangers of Hazing 
                     
                      Starting out in college can be scary even under the best of circumstances—new people,
                        new professors, and new course material make college a particularly daunting phase
                        in education. But when other students go out of their way to make starting out at
                        college even more intimidating, things can begin to escalate much too quickly. While
                        instances of hazing have always occurred, dating back to the earliest days of college
                        history, in recent years hazing tactics seem to have become more violent, aggressive,
                        and sometimes, deadly. While hazing tends to be most common among members of the Greek
                        system, or those on an athletic team, hazing can and does occur to a wide variety
                        of new students. And while some hazing can seem friendly and harmless, some hazing
                        tactics can be terrifying. Even though many colleges have begun to enforce strict
                        anti-hazing rules, huge numbers of students still report either witnessing, partaking
                        in, or being the victim of a variety of hazing rituals. Among extreme cases of hazing,
                        the effects can range from psychologically damaging to physically harmful. The following
                        infographic takes a look at some of the stats and facts around hazing, as well as
                        some of the surprising details on just how dangerous it can be.
                      
                     
                          
                     
                       Source:  http://www.educationnews.org/higher-education/dangers-of-hazing   
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Hazing Information 
                         
                            
                               
                                Overview  
                                About Hazing  
                                     
                                      Alcohol   Hazing  
                                     
                                      Alternatives to Hazing  
                                     
                                      Dangers of Hazing  
                                     
                                      Hazing Prevention  
                                     
                                      Sanction Guidelines  
                                  
                               
                                What You Can Do  
                                      Take Action  
                                      Report Hazing  
                                      Request A Presentation  
                                      What Happens Next  
                                  
                               
                                Organizations  
                                      Overview  
                                      Band  
                                      Greek  
                                      NCAA Athletic  
                                      ROTC  
                                      Sports Clubs  
                                      Other  
                                  
                               
                                Resources  
                                      Faculty/Advisors  
                                      Parents  
                                      Students  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Our Mission  
                               Learn about the key elements we use to enhance your college experience. 
                            
                            
                                Fraternity   Sorority Affairs  
                               Enhance your college experience by joining a fraternity/sorority! 
                            
                            
                                Tiger Zone  
                               Learn about student organizations, events   involvement opportunities. 
                            
                            
                                Contact Us  
                               Questions? Our team is here to help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/27/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


