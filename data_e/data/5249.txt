Viewing class and user progress in Content | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 Viewing class and user progress in Content | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Content    »    Tracking content completion and participation   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Viewing class and user progress in Content                       
        
        
       
          
     
           Printer-friendly version       
	You can also view general information about completion tracking for all users from the Class Progress page, or the details of a user's completion tracking from the User Progress page, both located in the User Progress tool. See  User Progress  for more information.
 

 
	If you click  Content  in the Reports area on the User Progress page, you can track how many visits a user makes to the Content tool, how much time is spent viewing course material, and how many topics were visited. Expand the report details under each module to find a further breakdown of a user's actions. The expanded details show all topics and sub-modules within the module, how many visits the user made for each topic, and how much time was spent in each topic. Clicking on the module or topic's name brings you to that module or topic in the Content tool.
 

 
	Access a user's progress summary from Content
 

  
		In a topic's Completion Summary, hover over a user's profile display or click on the user's profile picture.
	 
	 
		Click the    User Progress  link in the user's profile badge.
	 
  
	 Note  The Progress Summary page displays overall user progress results for the current course, not just the topic.
 

 
	See also
 

  
		 User Progress 
	 
      Audience:    Instructor      

    
           

                   ‹ Course content feedback  
        
                   up 
        
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Content  
  
      Content basics    Creating course content    Managing and updating course content    Using SCORM in Content    Tracking content completion and participation    Using completion tracking in Content    Course content statistics    Course content feedback     Viewing class and user progress in Content      
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
