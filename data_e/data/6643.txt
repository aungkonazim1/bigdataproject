M.A. in Egyptian Art - Prospective Students - Institute of Egyptian Art &amp; Archaeology - University of Memphis    










 
 
 
     



 
    
    
    M.A. in Egyptian Art - Prospective Students - 
      	Institute of Egyptian Art   Archaeology
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Institute of Egyptian Art and Archaeology
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Location  
                          Teaching  
                          Staff  
                          Research  
                          Exhibition  
                          Events  
                          Resources  
                      
                      
                         
                            Degree Programs in Egyptology   
                            
                               
                                  
                                   Prospective Students  
                                        
                                         Graduate  
                                        
                                         Undergraduate  
                                     
                                  
                                   Current M.A. Students  
                                   Alumni  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Institute of Egyptian Art   Archaeology
                              	  
                          
                              	Teaching
                              	  
                         M.A. in Egyptian Art - Prospective Students 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      M.A. in Egyptian Art - Prospective Students 
                     
                      This web page contains information about the  Master of Arts  degree in   Art History   with a  concentration in Egyptian Art and Archaeology  which is offered by the Department of Art.  Click here for information about the graduate Egyptology program in the    History Department .  
                     
                      To ensure that your computer displays the latest update, click on the 'Refresh' or
                        'Reload' button at the top of your screen.
                      
                     
                        
                     
                       What can our program do for you?  
                     
                      The  Master's degree program  in  Art History  with a concentration in  Egyptian Art and Archaeology  is designed to prepare the student either to continue on to a higher degree in Egyptology
                        or to enter an alternate career path. Whichever the student's choice, the program
                        will assist the student in developing strong skills in analytical thinking, research
                        and writing.
                      
                     
                      Egyptology students with the M.A. degree in Art History from the University of Memphis
                        have continued on to graduate programs in ancient Egyptian art, history, archaeology,
                        and language at such distinguished institutions as New York University, the University
                        of Pennsylvania, the University of Chicago, UCLA, Emory University, the University
                        of Toronto (Canada) and University College, London. Other of our students have enrolled
                        in programs in related fields such as religious studies, ancient history and  museum studies.  Graduates have also found internships or employment in secondary education, field
                        archaeology, and museums.
                      
                     
                      For information about past students of the M.A. program in Egyptian Art, see the  Alumni page.  
                     
                        
                     
                       Description of the Master's Program in Art History  
                     
                      The Master of Arts in Art History with a concentration in Egyptian Art and Archaeology
                        is a rigorous two year program. The student must maintain a minimum 3.0 GPA ("B" average)
                        to remain in good standing in the program.
                      
                     
                      At the University of Memphis, students who register for 9 or more hours per semester
                        in the academic year are considered full-time students. University-funded graduate
                        assistants must register for no fewer than 9 hours credit per semester (or 6 thesis/dissertation
                        hours).
                      
                     
                      Students are required to  meet with the Program Advisor  before  registering  for each semester's coursework. The Program Advisor must clear each student prior
                        to registration.
                      
                     
                       Dr. Lorelei H. Corcoran  is the program advisor.
                      
                     
                        
                     
                         Art History Program Requirements   
                     
                      The two-year Masters program in Art History includes the following requirements - 
                     
                      
                        
                          30 hours  of completed coursework
                         
                        
                         A minimum of  21 hours  (7 classes) must be in  7000 level (M.A.)  courses  Only  three (3) * of these hours may be  thesis credits  (ARTH 7996).
                         
                        
                         Six (6) credit-hours may be from outside the major department (ARTH). 
                        
                          Eighteen (18) hours  (6 classes) must be in the major area (ARTH),  but  outside of the required courses (ARTH 6123, ARTH 7130, and ARTH 7996).
                         
                        
                         The student must maintain a minimum 3.0 GPA ("B" average) to remain in good standing
                           in the program.
                         
                        
                         Fulfill all University of Memphis mandated course requirements and regulations. 
                        
                         Read the official wording in the  Graduate Catalog.  
                        
                      
                     
                        
                     
                       Additional Requirements of the Masters Program in Art History - Egyptian Art Concentration  
                     
                      Successful completion of the program requires that the student fulfill a number of
                        requirements. These include:
                      
                     
                      
                        
                         Completion of two (2) departmental core courses -  ARTH* 6123 - Greek Art  ARTH 7130 - Art History Methods and Professional Practice
                         
                        
                         Satisfactory completion of at least two semesters of ancient Egyptian hieroglyphs 
                        
                         Passing a competency exam in Middle Egyptian hieroglyphs before the start of the second
                           semester
                         
                        
                         Passing a slide exam on ancient Egyptian art before the start of the second semester 
                        
                         One semester of either ARTH 6111 (Art and Archaeology of Ancient Egypt), ARTH 6112
                           (Art and Archaeology of the Old and Middle Kingdoms) or ARTH 6113 (Art and Archaeology
                           of the New Kingdom through Later Periods)
                         
                        
                         Passing a reading competency exam in either German or French by the end of the first
                           year.
                         
                        
                         Fulfill all University of Memphis mandated course requirements 
                        
                         Select a Thesis Committee Chair by the end of the first year of study 
                        
                         Successful completion of the Master's Thesis 
                        
                         Passing a comprehensive examination after submission of the M.A. thesis to the Thesis
                           Committee
                         
                        
                         Successful defense of the comprehensive exam and the M.A. thesis 
                        
                           * ARTH = Art History
                         
                        
                      
                     
                      With the guidance of the program advisor,  Dr. Lorelei Corcoran , students are responsible for meeting these requirements in a timely manner.    For a more detailed explanation of this list, see the  M.A. Requirements  page.
                      
                     
                        
                     
                         Egyptology or Degree-Related Classes Offered at The University of Memphis    
                     
                      Primarily in the programs in Art History and History, the University of Memphis offers
                        over two dozen  courses relevant to the programs in Egyptology.  Some of these courses may be taken by upper division undergraduate students, while
                        others are reserved for graduate study. These listings were taken from the on-line
                        undergraduate and graduate bulletins.
                      
                     
                        
                     
                         Sample Two Year Schedule    
                     
                      The degree in Egyptian Art and Archaeology affords the student the opportunity to
                        tailor their program to their specific interests in Egyptology. The sample  semester-by-semester schedule  for the two year Master's Egyptology Program in Art History will help the student
                        plan their course of instruction.
                      
                     
                      The individual student's specific schedule will vary. 
                     
                        
                     
                         Applying to Graduate School and the Program in Art History in the Department of Art
                              at the University of Memphi s  
                     
                      
                        
                           Main Page for the University of Memphis Graduate School   
                        
                           Main Page for the 2016-2017 Graduate Catalog   
                        
                           Graduate Admissions Web Page   
                        
                           Admissions Regulations for the Graduate School   
                        
                           Other Admission Regulations   
                        
                           Links to General Information, Applications, Financial Aid and Admissions Categories   For future students
                         
                        
                          
                        
                           Graduate School Admissions Procedures    How to Apply    
                        
                          
                        
                           Graduate Catalog entry for the Art Department Master's Programs,  including the M.A. in Art History  Departmental M.A. requirements for admission and course descriptions
                         
                        
                          
                        
                           Art Department web page for Master's Program in Art History   Concise information on how to apply to the Masters program in Art History Graduate Assistantship Form is available as a PDF on this page
                         
                        
                          
                        
                           History Department - Egyptology program   Introduction to the Egyptology MA and PhD programs in the History Department
                         
                        
                          
                        
                           History Department - Forms and Applications web page   Forms and guidelines on applying to the Graduate School at the University of Memphis
                         
                        
                      
                     
                        
                     
                         Links for Out-of-State and International Applicants   
                     
                      
                        
                           Academic Common Market (for residents of the Southeastern region)   Students from nearby states that do not have Egyptology programs may be eligible for
                           in-state tuition.
                         
                        
                           Residency Classification   
                        
                           Other Information  (vaccinations, entrance exams, etc.)  
                        
                           International Students   
                        
                      
                     
                             Links to Tuition, Fees and Financial Aid Resources at the University of Memphis   
                     
                      
                        
                           University Fee Charts and Payment   
                        
                           Tuition Estimator   
                        
                           Financial Aid Home Page   
                        
                           Graduate Student Assistantship Information   
                        
                           Art History Department Graduate Assistantship Form   
                        
                           Fellowships   
                        
                           Federal Funding    Includes information regarding federal student aid, loans, work study, etc.
                         
                        
                           Applying for Federal Work-Study funding    (FAFSA)   All incoming graduate students are  strongly encouraged  to apply for Federal funding. You do not have to apply for Federal loans, but please
                           check the  box for Work-Study  on your University of Memphis financial aid application.
                         
                        
                      
                     
                        
                     
                         Calendars and Deadlines   
                     
                      
                        
                           University of Memphis Academic Calendar 2017-2018 (Office of the Registrar)   
                        
                           University-wide Semester Calendar (Office of the Registrar)   
                        
                           Application Deadlines for University of Memphis Graduate School   Application deadlines are listed at the top of the web page.   Note:    Financial Aid deadlines are earlier.  
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Degree Programs in Egyptology 
                         
                            
                               
                                Prospective Students  
                                     
                                      Graduate  
                                     
                                      Undergraduate  
                                  
                               
                                Current M.A. Students  
                                Alumni  
                            
                         
                      
                      
                      
                         
                            
                                Upcoming Events  
                               Schedule of events and exhibits 
                            
                            
                                Egyptian Art and Archaeology Major  
                               Earn your degree with a concentration in Ancient Egypt 
                            
                            
                                Research Projects  
                               Learn more about current IEAA research initiatives  
                            
                            
                                Contact Us  
                               Questions? Our staff can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


