 ADA - OIE - University of Memphis    










 
 
 
     



 
    
    
     ADA - 
      	OIE
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Office for Institutional Equity
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          EOAA  
                          Harassment  
                          Title IX  
                          ADA  
                          Policies  
                          Resources  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	OIE
                              	  
                         
                           	ADA
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Americans with Disabilities 
                     
                       The University of Memphis is committed to creating a welcoming and inclusive environment
                           for students, faculty, staff and visitors with disabilities. In compliance with the
                           Americans with Disabilities Act (ADA) and Section 504 of the Rehabilitation Act of
                           1973 (Section 504), no qualified individual with a disability shall be discriminated
                           against or excluded from consideration for employment or participation in or the benefits
                           of the services, programs or activities of the University of Memphis.  
                     
                      Under the ADA and Section 504, a person is considered to have a disability if (1)
                        he or she has a physical or mental impairment that substantially limits one or more
                        major life activities (such as hearing, seeing, speaking, breathing, performing manual
                        tasks, walking, caring for oneself, learning, or concentrating); (2) has a record
                        of having such an impairment; or (3) is regarded as having such an impairment class.
                        The University provides reasonable and appropriate accommodations to students and
                        employees with disabilities.
                      
                     
                      ADA Coordinator 
                     
                      The Director of the Office for Institutional Equity serves as the ADA Coordinator
                        for the University. The ADA Coordinator is responsible for:
                      
                     
                      
                        
                         Coordinating University programs, policies, and procedures relating to University
                           compliance and the promotion of University opportunities for persons with disabilities.
                         
                        
                         Ensuring that appropriate processes are in place to provide for the prompt and equitable
                           resolution of complaints and inquiries from University employees and students, as
                           well as the public regarding compliance with the ADA and other applicable federal
                           and state laws regarding discrimination on the basis of disability.
                         
                        
                         Providing ADA program and facility interpretation and advice on compliance to all
                           sectors of the University community.
                         
                        
                      
                     
                        
                     
                       More information about accessibility    
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Training  
                               OIE sponsors several online training modules, available in Learning Curve. 
                            
                            
                                Contact Us  
                               OIE provides a range of services that support equal opportunity and nondiscrimination. 
                            
                            
                                How to File a Complaint  
                               File a complaint of discrimination, harassment, sexual misconduct or retaliation. 
                            
                            
                                Sexual Misconduct  
                               General information and resources 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


