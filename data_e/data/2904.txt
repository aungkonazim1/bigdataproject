Instructional Technology Trainer   
 
   
       
       Instructional Technology Trainer 
 
 
   
   
   
   
   
   
   

 
 
 
 
 
 
 
 
 

     

   
   
       
       
         
           Skip to Main Content 
         
         
           
             
   
     
       
         
           
         
       
       
         
          University of Memphis Career Opportunities  
       
     
   
 

           
           
             
   
     
       
         Toggle navigation 
          
          
          
       
     
     
       
    Home  
    Search Jobs  
      Create Account  
      Log In  
    Help  
 

       
             
               University Benefits 
             
             
               Equal Employment 
             
             
               Annual Security Report 
             
             
               Campus Map 
             
             
               About UofM 
             
       
     
   
 

             
               
                
                   Instructional Technology Trainer 
  
  
   
       Bookmark this Posting  |
     Print Preview  |
         Apply for this Job 
   


   
     
       Posting Details 
        
       
            

               Posting Information 
                
               
                     
                       Posting Number 
                         HMAE1479 
                     
                     
                       Advertised Title 
                         Instructional Technology Trainer 
                     
                     
                       Campus Location 
                         Main Campus (Memphis, TN) 
                     
                     
                       Position Number 
                         002931 
                     
                     
                       Category 
                         Staff (Hourly/Monthly) 
                     
                     
                       Department 
                         ITS Service Desk 
                     
                     
                       Minimum Position Qualifications 
                          Bachelor’s degree in teaching, training, information technology or related discipline and two years of appropriate experience. Relevant experience may substitute for required education.  
                     
                     
                       Special Conditions 
                          The department is especially interested in applicants with training experience in higher education. 
 Candidate must possess excellent communication skills, both written and verbal, and must be able to interact with many levels of University faculty, staff, and students as required.  
                     
                     
                       Work Schedule 
                          Monday   Friday 
8:00 a.m. – 4:30 p.m.  
                     
                     
                       Posting Date 
                         12/07/2017 
                     
                     
                       Closing Date 
                         12/20/2017 
                     
                     
                       Open Until Screening Begins 
                         No 
                     
                     
                       Hiring Range 
                         $51,500 per year 
                     
                     
                       Full-Time/Part-Time 
                         Full-Time: Benefits Eligible 
                     
                     
                       Working Conditions 
                          While performing the duties of this job, the employee is regularly required to sit; use hands to handle, or feel; and talk or hear. The employee frequently is required to stand, walk, and reach with hands and arms. The employee is occasionally required to stoop, kneel, or crouch. The employee must occasionally lift and/or move up to 10 pounds. Specific vision abilities required by this job include close vision.  
                     
                     
                       Additional Working Conditions 
                          
                     
                     
                       Special Instructions to Applicants 
                          All applications must be submitted online at workforum.memphis.edu 
 Applicants must complete all applicable sections of the online application to be considered for a position. Please upload a cover letter, resume, and reference list after completing your application, if required by the system. Required work experience is based on full time hours. Part time work experience will be prorated as listed. 
Candidates who are called for an interview must notify the Department of Human Resources in writing of any reasonable accommodation needed prior to the date of the interview.  
                     
                     
                       Is this posting for UofM employees only? 
                         No 
                     
                     
                       Positions Supervised 
                          None  
                     
                     
                       Knowledge, Skills, and Abilities 
                          Skills in planning instructional design. 
 Content development, presentation and writing skills. 
 Strong project management skills.  
                     
                     
                       Additional Position Information 
                          
                     
                 
            
            

               Job Duties 
               The duties and responsibilities listed are intended to describe the general nature and level of work to be performed in this position and are not to be construed as an exhaustive list of the requirements of this job. 
                 
                     
                       Duties   Responsibilities 
                        Prepares and conducts technical training programs and classes using in-person, on-demand and virtual class settings.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Plans and directs classroom training, electronic learning, multimedia programs and other computer-aided instructional technologies, simulators, conferences and workshops  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Develops training materials and procedures and/or train faculty/staff/students in the use of University supported software and hardware  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Assesses training needs through surveys, consultation, focus groups and  evaluation teams  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Organizes and develops or obtain training procedure manuals and guides and course materials such as handouts, visual and electronic materials.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Other duties as assigned.  
                     
                 
            
       
     
  
     Supplemental Questions 
       
            Required fields are indicated with an asterisk (*).  
 
       *  
  Describe your instructional technical training experience.
     (Open Ended Question) 
 



 

       
     Applicant Documents 
       
           Required Documents 
     
     Resume 
     Cover Letter 
     References List 
 

 Optional Documents 
     
     Unofficial Transcript 
 



       


 


                  
               
                
             
           
           
             
   
     University of Memphis — Human Resources 
165 Administration Building, Memphis, TN 38152 
901.678.3573  workforce@memphis.edu  
 Follow us on Twitter  
      
      ©University of Memphis. All Rights Reserved. The University of Memphis is an Equal Opportunity/Affirmative Action university.  
   
 

           
         
       
         
 
        
  

  


    
      
    
     
   
 
