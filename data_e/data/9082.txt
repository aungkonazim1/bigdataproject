Bridging East and West: The First Steel Bridge of Memphis | University of Memphis Libraries   
 
 
 
 
 Bridging East and West: The First Steel Bridge of Memphis | University of Memphis Libraries 
 
 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 
 




 
 
  
 
 
 

 
 
 
 
			

         



 

 
 
	 Skip to content 
		 
				 
						  University of Memphis Libraries  
			 News and Events 
		 

		 
			 Menu 
			  
  Home    
		  
	  

	 
			 
			 Home  Bridging East and West: The First Steel Bridge of Memphis 		 
		 
		 

					 
				
 
	 
					 Bridging East and West: The First Steel Bridge of Memphis 
		
		 
						   October 17, 2017       khggerty   
			  Uncategorized  		  
	  

	 
		               
 The University Libraries would like to invite the campus community to visit the Fall 2017 exhibit, “Bridging East and West: The First Steel Bridge of Memphis.” This exhibit takes you through challenges the railroad industry faced with securing land to build the tracks needed to connect the east and west coasts of our nation, and highlights the technology to move rail cars over geographical obstacles like the Mississippi River. Come and transport yourself back to the end of the 19th century to see original maps, historic photographs, and learn about how Memphis celebrated the opening of the third largest bridge of its kind in the world. On display through Spring 2018. 

 
			  

	 
			  
  
			 

				 
		 Post navigation 

	
		      Banned Books Display  		  Craft-n-Chat      
	
	  
	
			
 

	
	
	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  

		
		  
	  

					 
					 		 Recent Posts 		 
					 
				 3D Printing Workshop 
						 
					 
				 NedXStudents 
						 
					 
				 Craft-n-Chat 
						 
					 
				 Bridging East and West: The First Steel Bridge of Memphis 
						 
					 
				 Banned Books Display 
						 
				 
		 		  Archives 		 
			  November 2017  
	  October 2017  
	  September 2017  
	  August 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  April 2016  
	  February 2016  
	  September 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  May 2014  
	  April 2014  
	  March 2014  
	  February 2014  
	  January 2014  
		 
		   Categories 		 
	  Alerts 
 
	  Branch Libraries 
 
	  Events 
 
	  Exhibition 
 
	  Homepage 
 
	  News 
 
	  Research and Instructional Services 
 
	  Special Collections 
 
	  Trial Resources 
 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 		  
	
	  

	 
		 
			 
								 Proudly powered by WordPress 
				  |  
				Theme: Big Brother by  WordPress.com .			  
					  
	  
  


 

        

        			 
				   Subscribe  
				 

					
						 Follow this blog 

						 
							
															 Get every new post delivered right to your inbox. 
							
							 
								 
							 
							
							 
							 
							
							  							   
						 

					
				 
			 
		







		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 