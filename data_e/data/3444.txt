Centennial Place Video &#8211; The University of Memphis President&#039;s Blog   


 

 
 
 
 


 
 
 
 

 



 Centennial Place Video   The University of Memphis President s Blog 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 



 
 
  
 
 
 

 
 
 
 
			

         
 		
		



 
 
 
 

 

 


  Skip to content  




 

	
	 

		
		 

			
			 

				
						  The University of Memphis President s Blog  

					
			  

		  

		
		 

			 Primary Menu 
			 Menu 

			
			 
				 
					 
				 
					 Search for: 
					 
				 
				 
			 				 
			 

			 

			  
  Home    About the President  
  

		  

		 

		
		
			
				 

			
		
		 

		
	  

	
	 
	 

		
		 

			
			
	
	 

		
		
		 

			
			 Centennial Place Video 
			
			 

				   Author   Jeanine Hornish Rakow     Published on    January 27, 2016     Leave a comment  
			  

			
		  

		
		 

			 Campus Community: 
 What a start to the spring semester at the University of Memphis. Last week we officially opened our newest residence hall, Centennial Place. This unique, state of the art residence hall is 290,000 square feet and will house 780 students. This facility , which is arguably one of the best in the country, features incredible living spaces, tremendous collaborative learning areas and will make us more competitive from an enrollment standpoint.  I think you will find Centennial Place to be a great point of pride for our campus as we continue to grow the University of Memphis. 
 If you missed the grand opening of Centennial Place, we have put together a short video highlighting the event and the remarkable facility.  Click here  to view it. I also encourage you to stop by and take a look at this impressive residence hall for yourself. 
 Go Tigers! 
 M. David Rudd | President 

		  

		
		 

			  Published on    January 27, 2016      Author   Jeanine Hornish Rakow   
			
		  

		
	  

	
				
	 
		 Post navigation 
		    Previous article:  Spring Update      Next article:  Governor s Budget Recommendations    
	 
				

 

	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  


			
			
		  

		
	  


	
		
		
		 

		 Main Sidebar 

			
			  
				 
					 Search for: 
					 
				 
				 
			  		 		 Recent Posts 		 
					 
				 UofM Quality Indicator Score 
						 
					 
				 University of Memphis Research Foundation Announces Grand Opening of Student-Operated FedEx Call Center 
						 
					 
				 Pause to Grieve 
						 
					 
				 University of Memphis Magazine: Fall 2017 
						 
					 
				 Campus Update 
						 
				 
		 		  Recent Comments      Archives 		 
			  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  July 2014  
	  June 2014  
	  May 2014  
	  April 2014  
		 
		   Categories 		 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 
			
		  

		
		  

	
	
	 

		
		 Footer Content 

		 
		
			
				
				
								
			
		  
	
		 

			
			
			Using  Tiny Framework     
			
			   Log in  

		  
		
		 

			
			

 

	 Social Links Menu 

	      i class= fa fa-twitter   /i    
  
  

			
		  

		
	  

	
  


        

        









		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 
 