Reordering the email folder list | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Reordering the email folder list | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Email    »    Understanding email folders   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Reordering the email folder list                       
        
        
       
          
     
           Printer-friendly version        
		On the Folder Management page, click    Reorder  from the context menu of any folder.
	 
	 
		Select the folder you want to move, then click    Up  or    Down  to move the folder through the order.
	 
	 
		Once you have the folders ordered the way you want, click  Save .
	 
  
	 Note  The Reorder Folders page only lists folders that are organized at the same level (i.e. they are either top-level folders or share a parent folder). To change the nesting structure of folders at different levels (i.e. remove or add a parent association) you must edit the  Parent Folder  field on the Edit Folder page.
 
     Audience:    Learner      

    
           

                   ‹ Editing an email folder 
        
                   up 
        
                   Moving an email message to a folder › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Email  
  
      Email basics    Understanding email folders    Email folders    Accessing email folders    Accessing the folder management area    Adding an email folder    Editing an email folder    Reordering the email folder list    Moving an email message to a folder    Deleting an email folder      Understanding the address book    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
