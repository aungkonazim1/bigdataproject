Viewing grade statistics | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Viewing grade statistics | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Grades    »    Managing final grades   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Viewing grade statistics                       
        
        
       
          
     
           Printer-friendly version       
	Viewing final grade statistics
 

 
	Final grade statistics lets you view details about a class, group, or section’s overall grades for a course. Statistics include the average, median, mode, standard deviation for a population, minimum grade, maximum grade, and a graph showing the distribution of grades. You can also view users’ individual final grades.
 

 
	View final grade statistics
 

 
	Do one of the following:
 

  
		On the Manage Grades page, click    View Statistics  from the action menu beside the Final Calculated or Final Adjusted grade category.
	 
	 
		On the Enter Grades page, click    Statistics  from the action menu beside the Final Calculated or Final Adjusted grade category.
	 
  
	Viewing grade category statistics
 

 
	Category statistics lets you view details about a class, group, or section’s overall performance in a category of grade items. Statistics include the average, median, mode, standard deviation for a population, minimum grade, maximum grade, and a graph showing the distribution of grades. You can also view users’ individual grades in the category.
 

 
	View category statistics
 

 
	On the Manage Grades page, click    View Statistics  from the action menu beside the category you want to view statistics for.
 
     Audience:    Instructor      

    
           

                   ‹ Setting release conditions for the final grade 
        
                   up 
        
                   Changing Grades settings and display options › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Grades  
  
      Finding my grades    Creating a grade book    Creating grade items and grade book categories    Managing grade items and grade book categories    Managing grade schemes    Managing users  grades    Managing final grades    Accessing the Final Grades page    Calculating final grades    Additional options on the Final Grades page    Editing the calculated or adjusted final grade    Excluding grade items from the final grade    Releasing a final grade    Setting final grade display options for users    Setting display options for your view of the grade book    Setting release conditions for the final grade    Viewing grade statistics      Changing Grades settings and display options    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
