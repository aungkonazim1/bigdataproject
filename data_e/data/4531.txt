Creating Personal Web pages &mdash; Department of History, The University of Memphis Getting started with writing Web pages  
 
 
 Creating Personal Web pages   Department of History, The University of Memphis 
 
 
 
 
 
 
 
 Creating personal Web pages 
  1. Getting started  |  2. Creating your pages  |  3. Publishing your pages  |
 4. Following up, and learning more  
 
 Web sites at The University of Memphis are expected to comply with the 
 University guidelines
about Web sites . Those that do not comply are subject to removal. Be sure to read the guidance document carefully
before you begin constructing your site. 



 1. Getting started 
 There are three different approaches to setting up Web pages within the 
Department of History at The University of Memphis. 
 
  UMdrive  
  College of Arts and Sciences Web server  
  eCourseware  
 

 UMdrive  
 
 Although it was probably designed primarily for other file-sharing purposes, you 
may use UMdrive for your Web pages. UMdrive is easier to use than any of the 
other approaches, so if you are new to Web publishing you should give it first 
consideration. 
 This approach is Web-based and requires very little special learning. If you 
can use a Web browser, you already know most of what you need to know about 
UMdrive. 
 Access to pages kept on UMdrive depends on the directory in
which you store the files. If you store them in a public directory,
anyone may access them. If you store them in a non-public
directory, access is restricted to you, systems personnel, and the
persons to whom you specifically grant permission. 
 You will probably find when you log in for the first time that the system has 
 already created a public directory with the name   public   for you.
If so, and if you are content to use that directory for your files, you won t need the material
that follows 
-- you could move on to  Section 2: Creating your pages .
Before you leave this section, however, you should consider two things: 
 
 As you will see in the  remarks below about naming directories and files , there
are certain advantages to creating a directory named   www  , so you
might not want to put your files in a directory named   public  . (Don t
worry about names   any directory can either be made available to the public or denied to the
public regardless of what it is named. Strange though it may seem, a directory named   public  
can be closed to the public and one named   absolutely_no_admission  
can be open to everyone.) 
 You should verify that the existing public directory actually allows sharing of files with
the public. If not, you will need to allow sharing ( directions for doing this are found below )
before you move on.
 
 
 To create a public directory, log in
to the  UMdrive server ,
using your UUID and ph-password. (These are what you use to access
your University of Memphis e-mail account.) 
 
 From your main or root directory, which will be named the same as
your UUID, create a new directory (folder) by clicking on the   New
Folder   button.
 
 
 
The directory can be named anything you like, but if you are using it
for Web pages, for the reasons given in the  remarks below about naming directories and files 
it makes sense to name it   www  
(the example uses   whatever  , though,
because the choice is up to you). In the new window, type in
the name of the new public directory and then click
  Next  . 
 
  The revised directory structure will then be displayed.  Find the
row for the new directory and look at the column labeled   Shared with  .
If you see that the notation is  Everyone  the directory is already shared with the public
and you don t need to do anything. 
  
 But if you should see any other notation, it means that the directory is not shared with everyone yet and
you will need to change the setting. Click on the directory name to select it and then click on the   Share   icon. 
 Click on  Verify Permissions  (the diagram suggests you can click on the dot below the label,
but you must click on the wording).  
 
 The new window shows who can read, write, delete, and administer files in the 
new directory. As the creator of the directory, you always have these privileges (notice that the   Full Access   column is checked). To allow others to read the files, you must set privileges for them.
(Here you see several others have been granted full privilege already because of their positions within the university.)
In the row which is labeled   Public   click in the 
column named   Viewer (read-only)      select ONLY  Viewer (read-only); don t change any
other permissions because you don t want to allow the public to
change your files in any way . There is probably a mark in the radio button
for the option  Apply the changed permissions to this folder as well as 
its sub-folders and files. 
If not, click on the radio butter to select that option. Then click on
  Finish   to apply the change. 
 
 When you return to the root directory, you will find that the
  Share   column indicates that the directory is now shared with Everyone.  
  If you follow the directions given in this document, the URL for your home
page will be
  https://umdrive.memphis.edu/uuid/dirname/filename  
(where   uuid   is your assigned
user name,   dirname  
is the name of the directory, and   filename   is the name of your home page
  usually   index.html  ).
If you are creating a page for a group, such as the Department of History, there is a small peculiarity that you must be aware of   there is a
  g-   prefix in the UUID, so the URL for the Web page of the Department of History is
  https://umdrive.memphis.edu/g-history/www/index.html  .
 The   https://   prefix of the 
URL indicates that it will request a secure connection   the padlock icon 
on your browser will appear locked
 
when you reach UMdrive to indicate that secure connection. Actually, whether you supply the prefix for an unsecured connection
(  http://  ), for a secured connection (  https://  , or no
prefix at all, the connection will always be made securely. 
 
 Restricting Access: 
 On occasion, you may have material for which you wish to restrict access to only certain users, such as other individuals or the members of 
the class that you are currently teaching. You may restrict access to an entire directory or to individual files. 
You begin by clicking on the directory name or file name and then clicking on the   Share   icon. 
 You will see the same screen that was illustrated earlier for setting privileges. This time you do not need to click on a choice   the system will assume
that you wish to select the first entry, Choose Users. The instructions are quite explicit. Note if you want to grant access to anyone, you may use either
the person s user name (such as mcrouse) or the full e-mail address (such as mcrouse@memphis.edu). If it is not someone at the university,
you must give the full e-mail address. You might not realize it, but every existing class
at the university has a username. Suppose that you are scheduled to teach History 
2010, section 101, in the First Summer Session 2013, and you wish to allow only members
of that class to access the information.
The proper format for entering the name is 
  SubjectareaCourseSection.YearTerm   (you might recognize that this format 
is the same one that is used in  sending e-mail
messages to everyone in a class  without having to list 
each member of the class as an addressee.) Following that format, you would enter
  hist2010104.2013u   and click on   Next  . 
You will notice that as you start typing the user name, the system will supply possible choices, listing all classes since the system started if they begin with
the characters you have already entered, and you do not have to type the entire user name
if you see it appear in the list. You can simply click on the name in the list to select it before clicking   Next  .
You may have noticed that either upper- or lower-case entries will work, although the results appear in upper-case. You can ignore the box
labeled  Search for Users  because it will produce a list that is identical to the one just described. 
  
 The new screen should show the class with the box for   Viewer (read-only)   checked. If the 
box isn t checked, check it.  Make sure none of the other boxes are checked. 
Since you probably want your class to access all files or sub-directories in the restricted 
directory, you should leave the radio button marked for  Apply the changed 
permissions 
to this folder as well as its sub-folders and files  and then click on   Finish  . 
 
  
 You will then see a screen that reports that the permissions have indeed been 
changed. The directory or the file will report that it is shared by  Some  now. 
 Because the directory is not open to the public, anyone who tries to 
access it will see a screen asking for authentication. (The appearance of the 
screen may vary considerably, depending on the browser that you are using, but 
it will ask for the same information.) 
 
 
  
 
  
 
  
 
 
 When the person enters his or her UUID and password, UMdrive will check to 
see if the UUID and password match any of those who are authorized 
access. (UMdrive  knows,  by linking to registration records in the
Student Information System, who is enrolled 
in every class.) If there is a match, the directory will open; if not, the server
will issue a stern   and somewhat cryptic   rebuke. 
   
 In this example, 
only you as the owner of the directory and persons enrolled in History 2010, section 
101, Summer Semester 2013, will be allowed access; all others will be denied. 
(You may, of course, allow access to more classes or individuals any time you 
wish, and you may also delete any of those who currently have access.) 
 

 College of Arts and Sciences Web Server
 
 
 Some persons in the Department of History put their Web pages on the Web server
of the College of Arts and Sciences (CAS, in computer  lingo ).
If you wish to do so, you will need to go to the CAS Computer
Support office in 118 Scates Hall or contact M. J. Garrett, who is in charge
of that office, at 901.678.1617 or  
garrett@memphis.edu  to get an account. 

 If you use the CAS server, CAS Computer Support will create the necessary Web directory for you. The
URL for your home page will be
  http://cassian.memphis.edu/history/uuid/   (where
  uuid   is your assigned user name). 
 You should be aware that if you use the CAS server, it is quite easy to set it up as a network drive if your computer is on the campus network,
but you cannot access your files on the server from off-campus without special software being installed
on the computer you are using. The other servers, UMdrive and eCourseware, are Web-based and files may be accessed off-campus using an ordinary browser.
This could be an important consideration in the choice you make of the server for your files. 
 

 eCourseware 
 
 If all of your Web pages consist of material intended only for a specific course or specific courses and not for the general public,
an alternative to  regular  Web pages is eCourseware. eCourseware is
the generic term that The University of Memphis uses for course-management software. Currently it is the software
provided by  Desire2Learn , which
completely replaced WebCT throughout the Tennessee Board of Regents system in Fall 2007.
It is often abbreviated as D2L. 

 Desire2Learn is somewhat more complicated than  regular  Web pages,
but it works well, and in addition to being used for courses at The University of Memphis,
it is the format that the Tennessee Board of Regents requires for courses that are in the 
 
Regents Online Degree Programs .

 At The University of Memphis, whether you use eCourseware or not, every semester the system automatically creates
a new  shell  for every course for which you are listed as the instructor, leaving it to you to provide the content for it.
When you log in to eCourseware, you will find links to those courses.
For courses at The University of Memphis you may either  log-in directly to the local eLearn server 
or log-in to the  myMemphis portal  and click on the  eCampus Resources 
tab, then on the  UofM eCourseware  icon in the  eCourseware  channel. 
 The  shell  is a kind of template into which you insert the content that you want the course to have. How
you go about providing that content is well beyond the scope of this document. Many faculty members   not all   use eCourseware and may
be willing to give you basic instruction. To master eCourseware you probably should
enroll in one of the courses taught periodically in the 
 
Advanced Learning Center s Face to Face Sessions . The Advanced Learning Center will also conduct
consultations with groups or departments upon request   you must make a  request through the
Help Desk for these consultations .
If you want to learn eCourseware on your own, these online resources are available: 
 
  
  Quick start tutorial  
  
Desire2Learn Help  
  Student Introduction to eCourseware  
  

 Access to material on eCourseware is much more restricted than with
normal Web pages. Students have access only to the course
or courses in which they are currently enrolled. Instructors (and their graduate assistants, if there are any) have
access to their own courses but not necessarily to those of other persons. There are, however, ways of
letting other people access your courses, and they may let you access theirs.
Systems personnel naturally have access to all material. 

 There is no direct URL for your pages on eCourseware. Anyone who is authorized to access them
will find links on his or her   My Home   page after logging in
to eCourseware. 

   
 
  

 

 
 
 
 
 
 
 
 
 
 Copyright 2013 by  The University of Memphis  |
 Important Notice  |
Maintained by  Maurice Crouse  |
Last modified 10 August 2013 
   
 
 
  
 
 
