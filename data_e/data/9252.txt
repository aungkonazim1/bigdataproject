Earthworm Modules: srusb2ew commands   
 
 Earthworm Modules: srusb2ew commands 
 

 

  srusb2ew Configuration File Commands 
 (last revised 05 March, 2010)  

Page Index: 
1.   Functional command listing  
2.   Alphabetic command listing & description  
3.   Sample Configuration File  
 
 
On startup, srusb2ew reads the configuration file named on the
command line. Commands in this file set all the parameters used for
configuring the Earthworm srusb2ew module. In the control file,
lines may begin with a valid srusb2ew command (listed below) or
with one of 2 special characters:

  
#  marks the line as a comment (example: # This is a comment).    
@  allows control files to be nested; one control file can be 
   accessed from another with the command "@" followed by 
   a string representing the path name of the next control file 
   (example: @model.d).
  
Command names must be typed in the control file exactly as shown in this
document (upper/lower case matters!). Blank lines are also permitted in the 
control file.
 
 
 1.  FUNCTIONAL COMMAND LISTING 
   
 
Below are the commands recognized by srusb2ew, grouped by the
function they influence.  Some of the commands are required, as noted.
They may be specified in any order in the control file.

 
 
 
   Earthworm system setup:
                 ModuleId                 required
                 RingName                 required
                 LogFile                  required
                 HeartbeatInterval        required
                 OutputMsgType            optional
                 Debug                    optional

   USBxCH data acquisition parameters:
                 AtodDriverName           required
                 AtodModelName            required
                 SamplingRate             required

   USBxCH GPS timing parameters:
                 GpsModelName             required

   Channel SCNL parameters:
                 EwChannelScnl            optional

   Summary logging parameters:
                 SummaryInterval          optional
                 GpsReportInterval        optional
                 GpsBadLimit              optional

   Debugging output parameters:
                 OutputPakFiles         optional
                 PacketsPerFile         optional

 
 

 
 2.  ALPHABETIC COMMAND LISTING & DESCRIPTION 
 
In the following section, all configuration file commands are listed 
in alphabetical order.  Listed along with the command (bold-type) are 
its arguments (in red), the name of the subroutine that processes the 
command, and the function within the module that the command influences. 
A detailed description of the command and is also given.  Default values
and example commands are listed after each command description.
  
command  arg1                                        function
  
 

 

     
  AtodDriverName  driver_name                       USBxCH Configuration
             
 
   
Sets the name of the USBxCH device driver to  driver_name . 
This name is case sensitive and must match the name used when installing the
driver.
  
Default:  None
Example:  AtodDriverName SrUsbXch0
 
 
 

     
  AtodModelName  xch_model_name                     USBxCH Configuration
             
 
   
Indicates which member of the USBxCH family of 24 bit data acquisition devices
is being used.   xch_model_name  can be USB4CH, or
USB8CH.  This name is case sensitive and must match the model you are actually
using.
  
Default:  None
Example:  AtodModelName USB4CH
 
 
 

     
  Debug  n                                          Earthworm setup 
             
 
   
Sets the log output level for srusb2ew.  Errors and warnings are always
logged.  Level 0 adds startup info.  Level 1 adds general summaries every
 SummaryInterval  seconds and GPS lock status summaries
every  GpsReportInterval  seconds.  Level 2 adds
some additional details.  Level 3 adds time and satellite info for every 1 second buffer
of data.  Level 4 adds NMEA messages for every buffer.  Level 5, primarily for code
debugging, adds function calls and program variable values.
  
Default:  0
Example:  Debug 1
 
 
 

     
  EwChannelScnl  n sta comp net loc pin             Channel Configuration
             
 
   
There should be one EwChannelScnl command for each USBxCH channel, including
special channels like the digital and GPS PPS channels.  This information
is used to provide the full station, component, network and location names
(SCNL) plus pin number as headers for the earthworm trace buffer messages.
 n  indicates the USBxCH channel number while
 sta comp net loc pin  give the earthworm SCNL and
pin values.
  
Default:  The channels are named CH## with component xxx, net SR and
          location -- (indicating blank location).  The pin number is 
          set equal to the USBxCH channel number.
Example:  EwChannelScnl 0 CH00 xxx SR -- 0
 
 
 

     
  GpsBadLimit  nbad                                 Summary Logging
             
 
   
The GPS lock status is determined once a second (ie for every buffer of data
acquired).  It is defined as good if 3 or more satellites are in view, and
as bad otherwise.  Since there are typically many seconds in one
 GpsReportInterval , we need a criteria to
determine if the GPS lock status summarized over the interval is good or bad.
GpsBadLimit provides this criteria.  If the GPS lock status was bad for more
than  nbad  seconds during the current GpsReportInterval,
the summary status is defined as bad.  An error message is written to the
Earthworm ring every time the summary GPS lock status changes.
  
Default:  1
Example:  GpsBadLimit 60
 
 
 

     
  GpsModelName  gps_model_name                      USBxCH Configuration
             
 
   
Indicates what equipment is being used to determine GPS time.  Currently,
 gps_model_name  must be GARMIN, TRIMBLE, ONCORE, or
PCTIME.  GARMIN is the default and refers to a Garmin GPS 16x HVS receiver.
TRIMBLE refers to the Trimble Ace III receiver (this model has been
discontinued by Trimble).  ONCORE refers to a Motorola Oncore GT+ unit
(this model has been discontinued by Motorola).  PCTIME means time stamping
of the data is done using the PC system time and assumes this time is accurate
because it has been set using NTP (Network Time Protocol) or some other
GPS receiver.  Other values may be allowed in the future.
  
Default:  GARMIN
Example:  GpsModelName GARMIN
 
 
 

     
  GpsReportInterval  nsec                           Summary Logging
             
 
   
A summary of the GPS lock status is written to the log file every
 nsec  seconds if the log file is turned on
( LogFile  1), a medium log output level is selected
( Debug  >= 1), and you are using GPS
( GpsEnable  ON).  Set  nsec 
to 0 if no GPS lock status lines are desired.
  
Default:  0
Example:  GpsReportInterval 1200
 
 
 

        
  HeartbeatInterval  nsec                           Earthworm Setup
             
 
   
Defines the number of seconds,  nsec , between TYPE_HEARTBEAT 
messages issued by srusb2ew.
  
Default:  none
Example:  HeartbeatInterval 60
 
 
 

         
  LogFile  n                                        Earthworm Setup
             
 
   
Sets the on-off switch for writing a log file to disk. If 
 n  is 0, no log file will be written. If 
 n  is non-zero, srusb2ew will write daily log file(s) 
called nnnnn_yyyymmdd.log where nnnnn is the name of the configuration file
(with the suffix '.d' removed) and yyyymmdd is the current UTC 
date (ex: 20040123) on the system clock. The file(s) 
will be written in the EW_LOG directory (environment variable). 
  
Default:  none
Example:  LogFile 1
 
 
 

        
  ModuleId  mod_id                                  Earthworm setup
             
 
   
Sets the module id for labeling all outgoing trigger, heartbeat, and error 
messages.  mod_id  is a character string (valid strings
are listed in earthworm.d) that relates to a unique single-byte number. In 
general, a different module ID is needed for each instance of srusb2ew.
  
Default:  none
Example:  MyModuleId MOD_SRUSB2EW
 
 
 

     
  OutputMsgType  trace_format                       Earthworm setup
             
 
   
Tells srusb2ew whether to send waveform messages in the the old
or new style tracebuf format.   trace_format  is
a case sensitive character string and must be either TYPE_TRACEBUF or
TYPE_TRACEBUF2.  For Earthworm v7.0 and later, TYPE_TRACEBUF2 is the
default.
  
Default:  TYPE_TRACEBUF2 (for v7.0 and greater, TYPE_TRACEBUF otherwise)
Example:  OutputMsgType TYPE_TRACEBUF2
 
 
 

     
  OutputPakFiles  n                                 Debugging output
             
 
   
Sets the on-off switch for writing native USBxCH acquired data packets
to disk in SR .pak format files. If  n  is 0, no
.pak files will be written. If  n  is 1, srusb2ew
will write .pak files in addition to the normal EW tracebuf packets.
The .pak files can be used for debugging or for alternate processing
with SR utilities such as Pak2Asc.  A new sub-directory will be
created in the current working directory.  The name of the new
directory is based on the acquisition start time and will be something
like 2010-03-05-at-00-13-43.  The .pak files will be written into this
new directory.  With names starting at 00000000.pak and increasing
sequentially.  The number of packets in each .pak file is controlled
by the  PacketsPerFile  parameter.
  
Default:  0
Example:  OutputPakFiles 1
 
 
 

     
  PacketsPerFile  n                                 Debugging output
             
 
   
Specifies the number of packets to include in each output .pak file.  If the
number given is less than the sampling rate/2 it will be increased.  This
parameter is ignored if .pak files were not requested by setting
 OutputPakFiles  to 1.
  
Default:  none
Example:  PacketsPerFile 650
 
 
 

           
  RingName  ring                                    Earthworm setup
             
 
   
Tells srusb2ew which shared memory region to use for output.
 ring  is a character string (valid strings are
listed in earthworm.d) that relates to a unique number for the key to
the shared memory region. 
  
Default:  none
Example:  RingName WAVE_RING
 
 
 

     
  SamplingRate  sps                                 USBxCH Configuration
             
 
   
Requests how many samples per second to acquire.  The actual sampling rate
may be a different than the  sps  requested since
only certain rates are achievable with the USBxCH hardware.  Allowed rates
include 39.1, 65.1, 78.1, 130.2, 651.0.  Currently, the maximum allowed
 sps  is 651.
  
Default:  None
Example:  SamplingRate 130
 
 
 

     
  SummaryInterval  nsec                             Summary Logging
             
 
   
A summary of the analog, GPS PPS, and GPS serial info read is written to the
log file every  nsec  seconds if the log file is turned on
( LogFile  1) and a medium log output level is selected
( Debug  >= 1).
Set  nsec  to 0 if no summary lines are desired.
  
Default:  0
Example:  SummaryInterval 1200
 
 
 




 
  3. Sample Configuration File  
 
# FILE: srusb2ew.d            Copyright (c), Symmetric Research, 2010
#
# This is the srusb2ew parameter file for acquiring data with a 
# Symmetric Research USBxCH 24 bit A/D board and putting that
# data onto an earthworm ring.
#
# The standard earthworm import_generic/export (or export_scnl) functions
# can be used to transfer USBxCH tracebuf data from a local WAVE_RING to
# a WAVE_RING on another machine.  If you have data from two different
# USBxCH boards arriving at the same ring, you must ensure that each is
# using a different ModuleId.  Otherwise, the two sets of data will 
# appear to be scrambled together which leads to lots of message sequence
# number errors.
#
# If you wish to use MOD_SRUSB2EW for a ModuleId instead of
# MOD_ADSEND_C, you must first define it in your earthworm.d file.
#

Debug               2               # control amount of info sent to log file 0-8
                                    # warnings and errors always given
                                    # 0 adds startup info
                                    # 1 adds status summaries every GpsReportInterval
                                    # 2 adds config + more detail (default)
                                    # 3 adds trace data message info every second
                                    # 4 adds counter values, NMEA strings every second
                                    # Remaining levels 5-8 mostly for code debugging

OutputPakFiles      1               # output SR dat files in addition to the
                                    # normal EW tracebuff packets, good for 
                                    # debugging or alternate processing with SR
                                    # utilities such as Pak2Asc
PacketsPerFile     650              # packets in each .PAK file, will be adjusted
                                    # if less than sampling rate/2

#
# Basic Earthworm parameters:
#

MyModuleId         MOD_ADSEND_C     # module id for this instance of srusb2ew
RingName           WAVE_RING        # shared memory ring for outputting acquired data
LogFile            1                # 0 to turn off disk log file; 1 to turn it on
HeartBeatInterval  60               # seconds between heartbeats, only checked
                                    # after each second of data is acquired
#
# Optional Earthworm parameters:
#

OutputMsgType   TYPE_TRACEBUF2      # Older style waveforms were identified by
                                    # Station/Channel/Network (SCN), the newer
                                    # style also includes Location.  For Earthworm 
                                    # v7.0 and greater use, TYPE_TRACEBUF2.  If  
                                    # you need the older style, use TYPE_TRACEBUF.

#
# Basic USBxCH parameters:
#

AtodDriverName     SrUsbXch0        # name of USBxCH kernel mode device driver installed
AtodModelName      USB4CH           # which USBxCH is being used: USB4CH, USB8CH

SamplingRate       130.1            # requested sampling rate - the actual rate may be
                                    # different from this due to hardware considerations
                                    # currently only  78, 130, or 650 is allowed.

#
# Basic USBxCH GPS parameters:
#

GpsModelName       GARMIN           # usually GARMIN or TRIMBLE, may be PCTIME for NTP time


#
# Optional logging parameters:
#
SummaryInterval   1200              # seconds in log file between summary info
                                    # use 0 for no reporting
GpsReportInterval 3600              # seconds in log file between GPS lock lines
                                    # use 0 for no reporting
GpsBadLimit        300              # number of bad locks allowed per interval
                                    # before error message is sent


#
# Channel info: SCNL AND PIN VALUES FOR EACH A/D CHANNEL
#
# EwChannelScnl lines relate the USBxCH channel numbers to the SCNL and pin
# numbers used to identify channels in an earthworm system.  The pin
# numbers are optional.  If a pin number is not specified for a channel, 
# the pin number is set to the A/D channel number.
#
# Three different versions of this section are given below showing how
# the setup would look when using the USB4CH or USB8CH.  Only the
# version corresponding to the USBxCH you are actually using should
# be commented in, the other two should be commented out.
#
# The older keyword EwChannel is the same, except it has no location value.
# If OutputMsgType = TYPE_TRACEBUF2 and EwChannel is used, the location field
# defaults to "--" which means a blank location.
#

#
# Sample EwChannelScnl section for the USB4CH
#

#              A/D      
#            Channel   Sta   Comp  Net  Loc  Pin
#            -------   ---   ----  ---  ---  ---
#EwChannelScnl   0      CH00  xxx   SR   --    0
#EwChannelScnl   1      CH01  xxx   SR   --    1
#EwChannelScnl   2      CH02  xxx   SR   --    2
#EwChannelScnl   3      CH03  xxx   SR   --    3
#EwChannelScnl   4      DGTL  xxx   SR   --    4
#EwChannelScnl   5      PPS   xxx   SR   --    5
#EwChannelScnl   6      CNTR  xxx   SR   --    6



#
# Sample EwChannelScnl section for the USB8CH
#

#              A/D      
#            Channel   Sta   Comp  Net  Loc  Pin
#            -------   ---   ----  ---  ---  ---
#EwChannelScnl   0      CH00  xxx   SR   --    0
#EwChannelScnl   1      CH01  xxx   SR   --    1
#EwChannelScnl   2      CH02  xxx   SR   --    2
#EwChannelScnl   3      CH03  xxx   SR   --    3
#EwChannelScnl   4      CH04  xxx   SR   --    4
#EwChannelScnl   5      CH05  xxx   SR   --    5
#EwChannelScnl   6      CH06  xxx   SR   --    6
#EwChannelScnl   7      CH07  xxx   SR   --    7
#EwChannelScnl   8      DGTL  xxx   SR   --    8
#EwChannelScnl   9      PPS   xxx   SR   --    9
#EwChannelScnl  10      CNTR  xxx   SR   --   10



#
# Actual EwChannelScnl section being used ...
# replace with appropriate values for your installation
#

#              A/D      
#            Channel   Sta   Comp  Net  Loc  Pin
#            -------   ---   ----  ---  ---  ---
EwChannelScnl   0      CH00  SHZ   SR   U4    0
EwChannelScnl   1      CH01  SHZ   SR   U4    1
EwChannelScnl   2      CH02  SHZ   SR   U4    2
EwChannelScnl   3      CH03  SHZ   SR   U4    3
EwChannelScnl   4      DGTL  DIG   SR   U4    4
EwChannelScnl   5      PPS   GPS   SR   U4    5

 
 


  
 Module Index  | 
 srusb2ew Overview 
 

 
 
 
The URL of this page is   [http://folkworm.ceri.memphis.edu/ew-doc/cmd/srusb2ew_cmd.html]  
Contact:   info@symres.com   
 
 
 
