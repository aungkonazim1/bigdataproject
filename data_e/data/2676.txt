STEM (Science, Technology, Engineering, and Mathematics) Teacher Leadership - ICL - University of Memphis    










 
 
 
     



 
    
    
    STEM (Science, Technology, Engineering, and Mathematics) Teacher Leadership  - 
      	ICL
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Instruction and Curriculum Leadership 
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Programs  
                          Students  
                          Faculty  
                          ICL Faculty Research  
                          A-Z List  
                      
                      
                         
                            ICL Programs   
                            
                               
                                  
                                   Early Childhood  
                                   Elementary Education  
                                   Instruction and Curriculum  
                                   Instructional Design   Technology  
                                   Reading  
                                   School Library Specialist  
                                   Secondary Education  
                                   Special Education  
                                   Graduate Certificates  
                                         Urban Education  
                                         IDT  
                                         Literacy, Leadership,   Coaching  
                                         Autism Studies  
                                         School Library Information Specialist  
                                         STEM Teacher Leadership  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	ICL
                              	  
                          
                              	Programs
                              	  
                         STEM (Science, Technology, Engineering, and Mathematics) Teacher Leadership  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                       STEM (Science, Technology, Engineering, and Mathematics) Teacher Leadership  
                     
                      The goal of the certificate program in STEM (Science, Technology, Engineering, and
                        Mathematics) Teacher Leadership is to develop the knowledge and skills of secondary
                        mathematics, science, and engineering teachers and supervisors who are currently working
                        in local area schools. The coursework is designed to promote in-depth understanding
                        of current standards and instructional practices with a particular focus on building
                        teacher leaders in STEM education and promoting STEM education in schools. Gainful
                        Employment Disclosure.
                      
                     
                      X. Graduate Certificate in STEM (Science, Technology, Engineering, and Mathematics)
                        Teacher Leadership
                      
                     
                      A. Program Admission: Students interested in receiving a Certificate in STEM Teacher
                        Leadership must be apply to and be accepted to this graduate certificate program.
                        Applicants must hold a current teaching license. The courses may be completed as part
                        of a degree program with the advisor's approval.
                      
                     
                      B. Program Requirements: A total of twelve (12) credit hours are required to complete
                        this program.
                      
                     
                      
                        
                         ICL 7720/8720, STEM Curriculum Leadership, 3 credits 
                        
                         ICL 7721/8721, Seminar in STEM Teacher Development, 3 credits 
                        
                         ICL 7722/8722, Teaching and Learning in STEM Classrooms, 3 credits 
                        
                         ICL 7723/8723, Equity in STEM Education, 3 credits 
                        
                      
                     
                      C. Graduation Requirements 
                     
                      The student must complete all four required courses with an average grade of B (3.0)
                        or higher, for a total of 12 credit hours.In the semester of graduation, the student
                        must submit the Apply to Graduate form to the Graduate School and a Graduate Certificate
                        Candidacy form to Graduate Analyst in Ball Hall 215 (3798 Walker Ave., Memphis, TN
                        38152) by the deadline specified by the Graduate School.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         ICL Programs 
                         
                            
                               
                                Early Childhood  
                                Elementary Education  
                                Instruction and Curriculum  
                                Instructional Design   Technology  
                                Reading  
                                School Library Specialist  
                                Secondary Education  
                                Special Education  
                                Graduate Certificates  
                                      Urban Education  
                                      IDT  
                                      Literacy, Leadership,   Coaching  
                                      Autism Studies  
                                      School Library Information Specialist  
                                      STEM Teacher Leadership  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                ICL Degrees  
                               Broaden your career choices by gaining the confidence you to need to succeed 
                            
                            
                                Apply Now  
                               Every journey starts with a step, now take yours! 
                            
                            
                                Advising  
                               Your academic success begins with advising. 
                            
                            
                                Contact Us  
                               Questions? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


