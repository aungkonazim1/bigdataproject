Restoring a deleted discussion forum, topic, thread, or post | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Restoring a deleted discussion forum, topic, thread, or post | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Discussions    »    Creating and managing discussions   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Restoring a deleted discussion forum, topic, thread, or post                       
        
        
       
        
     
              
	Restoring a deleted forum or topic
 

 
	You must have the  Delete and Restore Forums and Topics  permission enabled to restore deleted topics and forums.
 

 
	  Restore a deleted forum or topic
 

  
		On the Discussions List page, click    Restore  from the More Actions button.
	 
	 
		Click  Restore  beside the forum or topic you want to restore.
	 
	 
		Click  Yes .
	 
  
	 Note  If the forum you restore contains one or more topics, you will be given the option to also restore the forum's associated topics.
 

 
	Restoring a deleted discussion thread or post
 

 
	To restore a deleted post, you must have permission  See Deleted Posts  enabled and you must have the  Display deleted posts  option enabled on the Settings page.
 

 
	  Restore a deleted thread
 

 
	Click the    Restore Thread  link in the thread you want to restore.
 

 
	  Restore a deleted post
 

 
	Click the    Restore Post  link in the post you want to restore.
 

 
	 Note  If the post you restore is a reply to another post, and the other post was also deleted, both posts restore.
 
     Audience:     Instructor       

    
           

                   ‹ Reordering discussion forums and topics 
        
                   up 
        
                   Monitoring discussions › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Discussions  
  
      Participating in discussions    Following discussions    Creating and managing discussions    Creating discussion forums and topics    Discussion forum and topic restrictions    Topic assessment    Topic objectives    Editing a discussion forum or topic    Copying a discussion forum, topic, thread, or post    Deleting discussion forums, topics, threads, and posts    Reordering discussion forums and topics    Restoring a deleted discussion forum, topic, thread, or post      Monitoring discussions    
                  
           
         

       
     

    
    
    
   
 
   
 
