Earthworm Modules: Ewintegrate Configuration File   
 
   
     
     Earthworm Modules: Ewintegrate Configuration File 
   
   
     
        
        Ewdrift Configuration File  
       (last revised 9 February, 2012)  
     #
#                     Configuration File for Ewdrift
#
 MyModId            MOD_DRIFT
 InRing             WAVE_RING # Transport ring to find waveform data on,
 OutRing            DRIFT_RING # Transport ring to write output to,
                              # InRing and OutRing may be the same.
 HeartBeatInterval  30        # Heartbeat interval, in seconds,
 LogFile            1         # 1 -  Keep log file, 0 -  no log file
                              # 2 -  log to file but not to stderr/stdout
# Debug		 		           # Write out debug messages (optional)

 MaxGap              1.5      # Maximum gap, in sample periods, allowed
                              # between trace data points.
                              # When exceeded, channel is restarted.

# Specify logo(s) of the messages to grab from the InRing.
# Up to 10 GetWavesFrom commands may be used.
# Must specify installation ID, module ID and message type.
# Installation and module may be wildcarded.
# The only valid messages types are TYPE_TRACEBUF and TYPE_TRACEBUF2
# Output message type will be the same as the input message type.
#--------------------------------------------------------------------------
 GetWavesFrom    INST_WILDCARD MOD_WILDCARD TYPE_TRACEBUF2

# List the 2 input SCNL codes of trace messages to compute the drift, # the output SCNL codes and the conversion factor.
# If old GetSCN command is used, location code is set to "--".
# Wildcard characters are not allowed here.
#-------------------------------------------------------------------------- #          Input-SCNL1      Input-SCNL2      Output-SCNL    ConversionFactor  GetSCNL   5405 HdE NP GN   5405 HdE NP FN   5405 HrE NP GN 1.16   
       Module Index  |  Ewdrift Overview   
     
     
      Contact:  
          Questions? Issues?  Subscribe to the Earthworm Google Groups
            List.   
        
     
   
 
