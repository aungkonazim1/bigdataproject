May 2015 Commencement &#8211; The University of Memphis President&#039;s Blog   


 

 
 
 
 


 
 
 
 

 



 May 2015 Commencement   The University of Memphis President s Blog 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 



 
 
  
 
 
 

 
 
 
 
			

         
 		
		



 
 
 
 

 

 


  Skip to content  




 

	
	 

		
		 

			
			 

				
						  The University of Memphis President s Blog  

					
			  

		  

		
		 

			 Primary Menu 
			 Menu 

			
			 
				 
					 
				 
					 Search for: 
					 
				 
				 
			 				 
			 

			 

			  
  Home    About the President  
  

		  

		 

		
		
			
				 

			
		
		 

		
	  

	
	 
	 

		
		 

			
			
	
	 

		
		
		 

			
			 May 2015 Commencement 
			
			 

				   Author   Jeanine Hornish Rakow     Published on    May 15, 2015  May 15, 2015     Leave a comment  
			  

			
		  

		
		 

			 Dear Campus Community: 
 One of the most important events in the lives of our students is attendance at their graduation ceremony.  We are pleased to announce that over 1,700 students participated in the commencement exercises on Sunday, May 10, 2015, where their degrees were conferred.  Additionally, it is estimated that over 30,000 family and friends attended the two ceremonies in the FedEx Forum – the largest participation in recent history. 
 Unfortunately not all of the family and friends of our students are able to be physically present for this very important experience in their student’s lives.  The University of Memphis recognizes the challenges that the family and friends of our graduates face, so we live stream the entire ceremony from the Forum to the internet so that it can be watched remotely.  During last Sunday’s events, there were 1,589 viewings of the two ceremonies from locations across the U.S. and from 84 different countries around the globe.  We are proud to be able to offer multiple ways for families and friends to join in the celebration of this very important day in the life of our students. 
 Please join me in congratulating all of our graduates. 
 Go Tigers! 
 M. David Rudd 
President 
   

		  

		
		 

			  Published on    May 15, 2015  May 15, 2015      Author   Jeanine Hornish Rakow   
			
		  

		
	  

	
				
	 
		 Post navigation 
		    Previous article:  University of Memphis Safety Compared to SEC Schools      Next article:  Update on Summer Initiatives    
	 
				

 

	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  


			
			
		  

		
	  


	
		
		
		 

		 Main Sidebar 

			
			  
				 
					 Search for: 
					 
				 
				 
			  		 		 Recent Posts 		 
					 
				 UofM Quality Indicator Score 
						 
					 
				 University of Memphis Research Foundation Announces Grand Opening of Student-Operated FedEx Call Center 
						 
					 
				 Pause to Grieve 
						 
					 
				 University of Memphis Magazine: Fall 2017 
						 
					 
				 Campus Update 
						 
				 
		 		  Recent Comments      Archives 		 
			  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  July 2014  
	  June 2014  
	  May 2014  
	  April 2014  
		 
		   Categories 		 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 
			
		  

		
		  

	
	
	 

		
		 Footer Content 

		 
		
			
				
				
								
			
		  
	
		 

			
			
			Using  Tiny Framework     
			
			   Log in  

		  
		
		 

			
			

 

	 Social Links Menu 

	      i class= fa fa-twitter   /i    
  
  

			
		  

		
	  

	
  


        

        









		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 
 