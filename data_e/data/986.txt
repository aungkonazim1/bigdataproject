FAQs - Executive MBA - University of Memphis    










 
 
 
     



 
    
    
    FAQs  - 
      	Executive MBA
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - Executive MBA
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Welcome  
                          Curriculum  
                          Admissions  
                          Students  
                          FAQs  
                          News   Updates  
                          FCBE  
                      
                      
                         
                               
                            
                               
                                  
                                   Curriculum  
                                   Admissions  
                                         How To Apply  
                                         Information Sessions  
                                     
                                  
                                   Students  
                                         Program Format  
                                         Faculty  
                                         Student Profile  
                                         Tuition   Fees  
                                         EMA Degree Learning Outcomes  
                                     
                                  
                                   News   Updates  
                                   FAQs  
                                   Contact Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Executive MBA
                              	  
                         
                           	FAQs
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Frequently Asked Questions 
                     
                      Is a laptop computer required in the EMBA Program? 
                     
                       A laptop is not required although we strongly encourage students to have access to
                           one. Most students bring laptops to class and find them to be useful in taking notes
                           and completing in-class assignments. The Fogelman EMBA Program classroom is equipped
                           with internet connections and other state-of-the-art technology that assist the students
                           in learning.  
                     
                      How many years of experience should I have? 
                     
                       A minimum of five years of experience in a managerial or professional position is
                           required.  
                     
                      When does the program start? 
                     
                       The program starts each August with the Week in Residency. Students may not enter
                           the program in mid-year.  
                     
                      Can I sponsor myself? 
                     
                       Yes, you may self-nominate if you would like. However, you will need two letters of
                           recommendation from individuals you have worked for or with over the past five years.  
                     
                      What is the minimum GMAT score I must have to be admitted to the EMBA Program? 
                     
                       Decisions about admission to the program are made on the basis of a variety of factors
                           considered together. Undergraduate GPA and background, GMAT score, quality and level
                           of work experience, personal statement of interest, letters of recommendation, and
                           the personal interview are all considered for admission to the program. There is not
                           an absolute minimum GMAT score, but a competitive GMAT score for students is 500 and
                           above.  
                     
                      Does the EMBA Program accept the GRE for admission in place of the GMAT? 
                     
                       Yes, the EMBA Program does accept the GRE.  A competitive score on the GRE would be
                           1000 or above.  
                     
                      Is financial aid available? 
                     
                       Yes, but you must work through the Financial Aid Office at the University of Memphis.
                           For more information, contact the Financial Aid Office at (901) 678-4825 or www.enrollment.memphis.edu/financialaid
                           or you may log on to www.finaid.org for an in-depth guide to financial aid.  
                     
                      Is the entire program fee due at the beginning of the program? 
                     
                       No, the fee for each semester is paid at the beginning of that semester.  There are
                           also several payment plan options available.  
                     
                      When will I know if I am accepted into the program? 
                     
                       You will be notified of your acceptance into the program approximately one week to
                           ten days after your personal interview.  
                     
                      What is the average size of the class? 
                     
                       Class sizes range from 15 to 25 students.  
                      
                     
                      Do I have to go on the international trip? 
                     
                       The International Study Tour is an integral part of the program. If exceptional circumstances
                           exist such that a student is unable to participate in the trip, we will arrange a
                           significant project assignment in its place. Most past and current students agree
                           that the international experience is one of the most valuable parts of the program.  
                     
                      Do classes meet during the summer? 
                     
                       Yes, there are two classes worth 2 credits each held during the summer.  One 2-hour
                           class is on-line and the other 2-hour class is held during the month of July.  
                     
                      What grade point average must I have in order to graduate? 
                     
                       You must have a 3.0 GPA and no more than 2 C's.  
                     
                      What kind of time commitment is required for the EMBA Program? 
                     
                       At least 10 to 15 hours a week should be allotted for class, working on assignments,
                           and meeting with team members. Therefore, a student in the EMBA Program must make
                           a serious commitment to the program.  
                     
                      Are there any pre-requisites for the EMBA Program? 
                     
                       No, there is no pre-requisite course work required. However, you must have a bachelor's
                           degree from an accredited college or university.  
                     
                     
                     	
                      
                   
                
                
                   
                      
                          
                         
                            
                               
                                Curriculum  
                                Admissions  
                                      How To Apply  
                                      Information Sessions  
                                  
                               
                                Students  
                                      Program Format  
                                      Faculty  
                                      Student Profile  
                                      Tuition   Fees  
                                      EMA Degree Learning Outcomes  
                                  
                               
                                News   Updates  
                                FAQs  
                                Contact Us  
                            
                         
                      
                      
                      
                         
                            
                                The Program  
                               Ready to take your career to your next level?  Let this program show you how. 
                            
                            
                                How To Apply  
                               What’s your first step?  Find out here! 
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


