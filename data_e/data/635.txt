Graduate Facutly Status Application Instructions - Graduate School - University of Memphis    










 
 
 
     



 
    
    
    Graduate Facutly Status Application Instructions - 
      	Graduate School 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Graduate School
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Future Students  
                          Current Students  
                          Degrees  
                          News   Events  
                          Resources  
                          Contact Us  
                      
                      
                         
                            Resources   
                            
                               
                                  
                                   Graduate School Calendar  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Graduate School 
                              	  
                          
                              	Resources
                              	  
                         Graduate Facutly Status Application Instructions 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Graduate Faculty Status Application 
                     
                      Dynamic Forms Entry Instructions 
                     
                      The Graduate School is pleased to release a paperless process for graduate faculty
                        status applications. Please carefully follow the directions below to create an application
                        or approve an application submitted to you. For those who approve applications, please
                        read the instruction sets for both applicants and approvers to be familiarized with
                        the electronic review process. If you have questions regarding the instructions below,
                        please contact please contact Gargee Phansalkar ( gphnslkr@memphis.edu ) or Dr. James Kierulff ( jkerulff@memphis.edu ). Thank you.
                      
                     
                       Applicants (Faculty):  
                     
                      
                        
                         Academic department faculty status application review procedures may require the involvement
                           of your chair, academic department committee, college counsel and college dean. For
                           some departments, not all of these entities are needed. Your first step is to determine,
                           before starting your application, who will be reviewing your application. For this
                           electronic application you will only need to know who will approve your application
                           at the chair and dean level. You will be entering their names and email addresses
                           into the application. (Note separately committee and counsel representatives if needed.
                           You will need to forward your application and supporting documentation by email to
                           the respective chairs or their assigned.) If you do not have either a chair or college
                           dean (as is the case for some departments), please use an equivalent. Two unique signatures
                           of authority are needed to process a faculty status form.
                         
                        
                         In your browser, go to the form URL:  https://umwa.memphis.edu/dynamicforms/df_login.php?formGUID=da2ded12-2dfe-419c-93f8-d1645937b62f  
                        
                         If requested, log in using your UUID and password. 
                        
                         Click on the Complete This Form icon. 
                        
                         For each of the approvers, enter their contact information:
                           
                            
                              
                               Click on Enter contact information 
                              
                               Type the name and email of the approver. 
                              
                               Click on the Save icon. 
                              
                            
                           
                         
                        
                         Once the information for both approvers has been entered, click the Continue to form
                           button.
                         
                        
                         Enter information for the faculty applicant.
                           
                            
                              
                               Required fields are marked with a red asterisk (*) 
                              
                               Attach files of your supporting documentation (e.g. CV, statement - check with your
                                 department to determine what is required if you are not certain) click on  Browse...  , select the desired file (either Word or PDF formats can be uploaded) and then click
                                 on  Upload File .
                               
                              
                            
                           
                         
                        
                         Go to the bottom of the form and click  Next  to proceed to the electronic signature section.
                         
                        
                         Enter your name as noted, and click on  Sign Electronically .
                         
                        
                         You will see a confirmation message. You can choose to view the PDF version of your
                           form, or log out of Dynamic Forms.
                         
                        
                      
                     
                       Approvers (Department Chair, College Dean):  
                     
                      
                        
                         You will receive an email from  forms@memphis.edu , with the subject Graduate Faculty Status Application form requires your signature.
                         
                        
                         Click on the link in the email:  https://umwa.memphis.edu/dynamicforms/df_login.php?formGUID=cosine  
                        
                         You may have to sign in using your UUID and password. 
                        
                         Once logged in, click on  Pending/Draft Forms .
                         
                        
                         The form will appear in the  Forms you need to complete  section.
                         
                        
                         Click on  Complete Form .
                         
                        
                         After reviewing the information on the form, you can approve it, or reject it. 
                        
                         To approve the form:
                           
                            
                              
                               Click on the respective checkbox at the bottom, to confirm that the application has
                                 gone through the appropriate Department Committee/College Council.  This is an optional step, since there may not be a Committee/Council. (Note: The Chair
                                    is designated to account for the Departmental Committee review of the applicant, the
                                    College Dean is designated to account for the College Council review. If the Chair
                                    or Dean is not in agreement with the respective committee/council decision, the chair
                                    or dean cannot move forward with a decision on the application until an agreement
                                    has been reached.) If there is no Department Committee/College Council do not click
                                    the respective checkbox.  
                              
                               Enter the name of the Committee Chair or Council Chair as appropriate. 
                              
                               Click on the  Next  button.
                               
                              
                               On the next screen, enter your signature, and click on the  Sign Electronically  button.
                               
                              
                            
                           
                         
                        
                         To reject the form:
                           
                            
                              
                               Click on the  Reject  button. Similar to the approval process above, the Chair or Dean must be in agreement
                                 with their respective Committee or Council decision (if a Committee or Council is
                                 applicable) before taking action on the application.
                               
                              
                               On the next screen, you will be able to send an email message to the previous form
                                 signer, and let them know the reason why the form has been rejected. They will be
                                 able to review, and/or re-submit the form.
                               
                              
                               Once you have written the message, click on the  Reject this form  button.
                               
                              
                            
                           
                         
                        
                      
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Resources 
                         
                            
                               
                                Graduate School Calendar  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now!  
                               Take the first step toward your advanced degree. 
                            
                            
                                Degree Programs  
                               Explore our graduate catalog. 
                            
                            
                                Support Graduate Education  
                               Your gift makes a difference! 
                            
                            
                                Contact Us  
                               Questions? The Grad School Staff can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


