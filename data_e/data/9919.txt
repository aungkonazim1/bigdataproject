Setting dropbox folder availability and due dates | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Setting dropbox folder availability and due dates | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Dropbox    »    Creating and managing Dropbox   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Setting dropbox folder availability and due dates                       
        
        
       
        
     
              
	Set availability dates for a dropbox folder
 

  
		On the Dropbox Folders page, click    Edit  from the context menu of the folder you want to set an availability.
	 
	 
		Click the  Restrictions  tab.
	 
	 
		In the Availability section, set the dropbox folder's  Start Date  and  End Date .
	 
	 
		Click  Save & Close .
	 
  
	Set dropbox folder due date
 

  
		On the Dropbox Folders page, click    Edit  from the context menu of the folder you want to set a due date.
	 
	 
		Click the  Restrictions  tab.
	 
	 
		In the Availability section, set the dropbox folder's  Due Date .
	 
	 
		Click  Save & Close .
	 
   Note Dropbox folder availability dates appear by default in Calendar. If you delete the dropbox folder, it's availability dates disappear from the course calendar. If you restore the dropbox folder, it's availability dates reappear in the course calendar.      Audience:     Instructor       

    
           

                   ‹ Managing dropbox folder submission handling 
        
                   up 
        
                   Setting release conditions for a dropbox folder › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Dropbox  
  
      Dropbox basics    Creating and managing Dropbox    Creating dropbox categories    Creating dropbox folders    Managing dropbox folder submission handling    Setting dropbox folder availability and due dates    Setting release conditions for a dropbox folder    Adding special access permissions to a dropbox folder    Editing dropbox categories and folders    Reordering dropbox categories and folders    Deleting dropbox categories and folders    Restoring deleted dropbox folders    Viewing the Dropbox event log    Associating dropbox folders with learning objectives    Previewing dropbox folders and submissions      Evaluating dropbox folder submissions    
                  
           
         

       
     

    
    
    
   
 
   
 
