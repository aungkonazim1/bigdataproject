 Location - Institute of Egyptian Art &amp; Archaeology - University of Memphis    










 
 
 
     



 
    
    
     Location - 
      	Institute of Egyptian Art   Archaeology
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Institute of Egyptian Art and Archaeology
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Location  
                          Teaching  
                          Staff  
                          Research  
                          Exhibition  
                          Events  
                          Resources  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Institute of Egyptian Art   Archaeology
                              	  
                         
                           	Location
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Visiting the Egyptian Gallery   Contacting the Institute. 
                     
                      The   offices   of the  Institute of Egyptian Art   Archaeology,  (hereafter IEAA), are located in the Art and Communication Building on the campus
                        of the University of Memphis in Memphis, Tennessee (USA). This building also houses
                        the Department of Art, the Fogelman Galleries of Contemporary Art, and classrooms.
                      
                     
                               
                     
                         The Egyptian Gallery is temporarily  closed.         
                     
                          The gallery will be closed until January 2018 for improvements. Check back with us
                                 for updated information.        
                     
                        The Art Museum and the Egyptian Gallery   
                     
                      The collection of   Egyptian antiquities   of the IEAA is on display in the  Art Museum  of the University of Memphis (AMUM) in the  Communication and Fine Arts  building. An exhibition of about 200 ancient Egyptian objects is on permanent display
                        in the Egyptian Gallery of the Art Museum.
                      
                     
                        
                     
                       Institute of Egyptian Art   Archaeology Contact Information  
                     
                       For an appointment with an IEAA staff member, please call or write using the contact
                        information below. The office is located at:
                      
                     
                      Institute of Egyptian Art   Archaeology  315 Art and Communication Bldg.  The University of Memphis  Memphis, TN 38152-3140
                      
                     
                      Phone: 901.678.2555  FAX: 901.678.1632 
                      
                     
                       Art Museum Contact Information, Hours and Tours  
                     
                      The  Art Museum  of the University of Memphis is located at:
                      
                     
                      142 Communication and Fine Arts Bldg.  3750 Norriswood  The University of Memphis  Memphis, TN 38152-3200
                      
                     
                      Phone: 901.678.2224  FAX: 901.678.5118
                      
                     
                       Art Museum Schedule  
                     
                       Open:  Monday through Saturday   Hours:  9:00 a.m. to 5:00 p.m.   Closed:   University Holidays  and between  Temporary Exhibits  
                     
                      The Art Museum, including the Egyptian Gallery, is  open to the public  and is always   FREE!   
                      
                     
                         Tours  
                     
                      The Institute of Egyptian Art   Archaeology provides trained graduate student  docents for groups  wishing a guided tour of the  ancient Egyptian  and  African ethnographic  exhibitions in the Art Museum of the University of Memphis. In addition, the Art
                        Museum and the IEAA are sometimes able to provide guided tours of the Contemporary
                        exhibitions at the Art Museum.    To  schedule a tour,  contact the Art Museum at 901.678.2224.      NOTE:  Groups are  required   to contact the Art Museum in advance and are strongly encouraged to schedule a docent
                        tour guide. Groups  must  be accompanied by a leader or chaperone.
                      
                      
                     
                         Directions  
                     
                      
                        
                         The  IEAA offices  and the  Art Museum  are both located on the main (north) campus of the University of Memphis in Memphis,
                           Tennessee (USA).
                         
                        
                      
                     
                       Driving directions to the city of Memphis, TN and the University of Memphis  
                     
                      
                        
                         The  city of Memphis  is situated in the extreme southwestern corner of the state of Tennessee, on the
                           east bank of the Mississippi River. Memphis is accessible via Interstate Highways
                           I-55 and I-40 from the west (Arkansas), I-55 from the south (Mississippi), I-51 from
                           the north and I-40 from the east (Tennessee).
                         
                        
                         The  main campus  of the University of Memphis is bordered by Central Avenue on the north, Patterson
                           and Zach Curlin streets to the west and east, and Southern Avenue to the south. Major
                           arterials leading to this area are Airways Blvd./East Parkway Blvd. and Highland Ave.
                           (north-south) and Poplar Ave. (east-west).
                         
                        
                      
                     
                         University of Memphis Campus Map    
                     
                      
                        
                         The  offices  of the  Institute of Egyptian Art and Archaeology  are located in the Art and Communication Building (ACB on the map), in the north
                           campus area.
                         
                        
                         The  Art Museum  and the  Egyptian Gallery  are in the Communication and Fine Arts (CFA) building, located in the  north central area  of the main campus. This is south of Central Avenue and to the east of Innovation
                           Drive (formerly known as Deloach Ave.).
                         
                        
                         The  Art Museum  is located on the ground floor of the CFA building, at the eastern end of the building.
                         
                        
                      
                     
                         Parking at the University of Memphis    
                     
                      
                        
                          Parking for visitors  to the IEAA and the Art Museum is available at  metered spaces  in the Central lot to the north of Central Avenue. Cost is $3.00 per hour.
                         
                        
                         Ticketed parking is available in the  Fogelman/Deloach garage  for $3.00 per hour. The garage is located south of Central Avenue, off of Innovation
                           Drive (formerly known as Deloach Ave.).
                         
                        
                         These parking areas are listed as Lots 44 (Central) and 40 (Fogelman/Deloach) on the
                           parking diagram.
                         
                        
                         Prepaid one-day visitor permits for general parking areas may be purchased in advance
                           online. Visit the Parking and Transportation Visitor Parking page at  https://www.memphis.edu/parking/permit/visitor.php  for this option.
                         
                        
                      
                     
                        Coming from a Distance?   
                     
                      
                        
                          Memphis International Airport  is a 20-minute drive from the University of Memphis campus via Airport Boulevard
                           and Central Avenue.
                         
                        The  Holiday Inn at Wilson School,  located on the north side of Central Avenue at Deloach Avenue, is conveniently located
                        near the Art Museum and the IEAA.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Upcoming Events  
                               Schedule of events and exhibits 
                            
                            
                                Egyptian Art and Archaeology Major  
                               Earn your degree with a concentration in Ancient Egypt 
                            
                            
                                Research Projects  
                               Learn more about current IEAA research initiatives  
                            
                            
                                Contact Us  
                               Questions? Our staff can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


