Change your password | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Change your password | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   English  
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Integrations    »    Binder    »    Change your account information in Binder Web   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Change your password                       
        
        
       
          
     
           Printer-friendly version        
		On the My Binder screen, click  My Account  >  Personal Settings .
	 
	 
		In the  Password  field, enter your new password.
	 
	 
		Click  Save .
	 
      Audience:    Learner      
    
         
               ‹ Connect an account to Binder 
                     up 
                     Binder for iPad › 
           
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Binder  
  
      Binder basics    Binder Web    Basics of Binder Web    Adding and navigating documents in Binder Web    Creating and managing collections and annotations in Binder Web    Change your account information in Binder Web    Connect an account to Binder    Change your password        Binder for iPad    Binder for Android    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
