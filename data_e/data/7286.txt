Creating arithmetic questions | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Creating arithmetic questions | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Question Library    »    Question types   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Creating arithmetic questions                       
        
        
       
          
     
           Printer-friendly version       
	Arithmetic questions enable you to assess users' knowledge and comprehension of mathematics and number theory. You can ensure each respondent receives a unique question by including set variables that randomly generate numbers within the problem.
 

 
	 Note  As a best practice, we recommend that you create long answer (LA) question types for arithmetic problems that require users to demonstrate their calculations and show their work.
 

 
	Automatic decimal rounding rule: Round to Half Even
 

 
	Learning Environment automatically applies the Round to Half Even rounding rule when assessing answers that contain decimal places that end with "5". Currently, there are no options to change rounding rules. Applying the Round to Half Even rule, answers with decimal places that end with "5" will round down instead of round up.
 

 
	Example One: 3.41 * 25 = 85.25 
	If you create an arithmetic question and set the Answer Precision to 1, the correct answer using Round to Half Even is 85.2.
 

 
	Example Two: -3.41* 25 = -85.25 
	If you create an arithmetic question and set the Answer Precision to 1, the correct answer using Round to Half Even is -85.2.
 

 
	Create an arithmetic question
 

  
		Enter a  Title .
	 
	 
		Enter a  Points  value.
	 
	 
		Select a difficulty level in the  Difficulty  drop-down list.
	 
	 
		Enter your arithmetic question in the  Question Text  field. Enclose variables with curly braces to generate random numbers.
		 
			 Example If you set variables x, y, and z with a Min 1 to Max 5 number range in 1-step increments, the question “You have {x} green marbles, {y} red marbles, and {z} blue marbles. How many marbles do you have in total?” will randomly generate a rational number (1, 2, 3, 4, 5) for {x}, {y}, and {z}.
		 
	 
	 
		Click  Add a File  to upload an image to accompany your question. You can enter a description of the image in the  Description  field.
	 
	 
		Enter the solution's  Formula  and enclose all variables in curly braces.
		 
			 Example The formula for the example question in Step 2 is {x}+{y}+{z}.
		 
		The  Formula  field supports the following operations, functions, and constants:

		    
						Enumerations
					 
					 
						Description
					 
				   
						 
							+, -, *, /, \, ^
						 
					 
					 
						 
							Basic arithmetic operators
						 
					 
				   
						 
							%
						 
					 
					 
						 
							Modulo (remainder) operator
						 
					 
				   
						 
							{x}^{y}
						 
					 
					 
						 
							x to the power of y
						 
					 
				   
						 
							abs({n})
						 
					 
					 
						 
							Absolute value of n
						 
					 
				   
						 
							cos({n})
						 
					 
					 
						 
							Cosine of n (in radians)
						 
					 
				   
						 
							sin({n})
						 
					 
					 
						 
							Sine of n (in radians)
						 
					 
				   
						 
							sqr({n})
						 
					 
					 
						 
							Square root of n
						 
					 
				   
						 
							tan({n})
						 
					 
					 
						 
							Tangent of n (in radians)
						 
					 
				   
						 
							log({n})
						 
					 
					 
						 
							Log base 10 of n
						 
					 
				   
						 
							ln({n})
						 
					 
					 
						 
							Log base e of n
						 
					 
				   
						 
							atan({n})
						 
					 
					 
						 
							Inverse tangent of n
						 
					 
				   
						 
							sec({n})
						 
					 
					 
						 
							Secant of n
						 
					 
				   
						 
							cosec({n})
						 
					 
					 
						 
							Cosecant of n
						 
					 
				   
						 
							cotan({n})
						 
					 
					 
						 
							Cotangent of n
						 
					 
				   
						 
							Factorial({n})
						 
					 
					 
						 
							Factorial of n, or (n!)
						 
					 
				   
						 
							exp
						 
					 
					 
						 
							The power of natural log (e)
						 
					 
				   
						 
							pi
						 
					 
					 
						 
							pi 3.14159 (accurate up to 50 decimal places)
						 
					 
				   
						 
							e
						 
					 
					 
						 
							e 2.71828 (accurate up to 50 decimal places)
						 
					 
				    
	 
		Select an  Answer Precision  from the drop-down list to define the number of acceptable decimal places. Select  enforce precision  if correct answers must contain a specific number of decimal places.
	 
	 
		Select and enter a tolerance level in the  units +/-  or  percent +/-  field to accept near-accurate, estimated, and rounded answers.
		 
			 Example  A  percent +/-  tolerance of 3 would allow answers to be off by 3%. A  units +/-  tolerance of 0.5 would allow answers to be off by 0.5 units.
		 
	 
	 
		Enter a unit type (mm, cm, grams, inches, etc.) in the  Units  field to assess if answers include correct units of measurement. Select a percentage from the  Worth % of Points  drop-down list to assign a weighted points value to the measurement unit.
	 
	 
		Set the  Evaluation Options  for your  Units  field:
		  
				 Case Insensitive  Auto-grading searches for a matching character pattern in the answer text with or without letter case correctness.
			 
			 
				 Case Sensitive  Auto-grading searches for a matching character pattern in the answer text that must have letter case correctness.
			 
			 
				 Regular Expression  Auto-grading uses meta-characters to search for one or more matching strings in the answer text's character pattern. What you set as meta-character parameters helps determine letter case sensitivity. See  Understanding regular expressions  for more information.
			 
		  
	 
		You can click   Add Variable  to create additional variables. To reduce the number of variables, click the corresponding   Remove Entry  icon.
	 
	 
		Enter a  Name , a minimum value in  Min , and a maximum value in  Max  for each variable. You can set the number of decimal places in the  Decimal Places  drop-down list.
	 
	 
		Enter a number in the  Step  field to set the system's incrementing steps as it generates numbers from the range set by the  Min  and  Max  fields.
		 
			 Example  If you create variable {x} with Min=100, Max=200 and Step=5, the system will only choose values for {x} that are increments of 5 above 100 (105, 110, 115, etc., up to 200) when generating numbers for your question.
		 
	 
	 
		Now you can click  Test  beside your formula to test the formula. Click  Done  to continue editing your question.
	 
	 
		Provide comments and suggestions in the  Question Hint  and  Question Feedback  fields.
	 
	 
		Click  Preview  to view your question. Click  Done  to end the preview.
	 
	 
		Click  Save  to return to the main page, click  Save and Copy  to save and create another arithmetic question that retains the copied properties, or click  Save and New  to continue creating new arithmetic questions.
	 
  
	See also
 

  
		 Understanding regular expressions 
	 
      Audience:    Instructor      

    
           

                   ‹ Creating ordering questions 
        
                   up 
        
                   Creating significant figures questions › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Question Library  
  
      Question Library basics    Question types    Understanding regular expressions    Creating true or false questions    Creating multiple choice questions    Creating multi-select questions    Creating long answer questions    Creating short answer questions    Creating multi-short answer questions    Creating fill in the blanks questions    Creating matching questions    Creating ordering questions    Creating arithmetic questions    Creating significant figures questions    Creating Likert questions    Creating text information    Creating image information      
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
