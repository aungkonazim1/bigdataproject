Scott D. Fleming   
 
 
   
   Scott D. Fleming 
   
   
  

 
 
  
 
   Scott D. Fleming 
   Associate Professor - University of Memphis 
 

  Home  Publications  Teaching  Vita  

 

   Publications 

   2017 

   

     Toward Principles for the Design of Navigation Affordances in Code Editors: An Empirical Investigation 
     Austin Z. Henley, Scott D. Fleming, and Maria V. Luong. 
     CHI '17: ACM SIGCHI Conference on Human Factors in Computing Systems, Denver, Colorado, May 2017. 
     
       DOI 
       PDF 
       

   

   2016 

   

     Foraging and Navigations, Fundamentally: Developers' Predictions of Value and Cost 
     David Piorkowski, Austin Z. Henley, Tahmid Nabi, Scott D. Fleming, Christopher Scaffidi, and Margaret Burnett. 
     FSE '16: ACM SIGSOFT Int'l Symposium on the Foundations of Software Engineering, Seattle, Washington, Nov. 2016. 
     
       DOI 
       PDF 
       ACM SIGSOFT Distinguished Paper Award 
     

     An Empirical Evaluation of Models of Programmer Navigation 
     Alka Singh, Austin Z. Henley, Scott D. Fleming, and Maria V. Luong. 
     ICSME '16: IEEE Int'l Conference on Software Maintenance and Evolution, Raleigh, North Carolina, Oct. 2016. 
     
       DOI 
       PDF 
     

     Yestercode: Improving Code-Change Support in Visual Dataflow Programming Environments 
     Austin Z. Henley and Scott D. Fleming. 
     VL/HCC '16: IEEE Symposium on Visual Languages and Human-Centric Computing, Cambridge, UK, Sep. 2016. 
     
      DOI 
      PDF 
      Best Paper Award 
    

    Putting Information Foraging Theory to Work: Community-based Design Patterns for Programming Tools 
    Tahmid Nabi, Kyle M.D. Sweeney, Sam Lichlyter, David Piorkowski, Chris Scaffidi, Margaret Burnett, and Scott D. Fleming. 
    VL/HCC '16: IEEE Symposium on Visual Languages and Human-Centric Computing, Cambridge, UK, Sep. 2016. 
    
      DOI 
      PDF 
    

  

  2015 

  

   Idea Garden: Situated Support for Problem Solving by End-User Programmers 
   Jill Cao, Scott D. Fleming, Margaret Burnett, and Christopher Scaffidi. 
    Interacting with Computers . 27(6):640 660, Nov. 2015. 
   
     DOI 
     PDF 
   

   To Fix or to Learn? How Production Bias Affects Developers’ Information Foraging during Debugging 
   David Piorkowski, Scott D. Fleming, Christopher Scaffidi, Margaret Burnett, Irwin Kwan, Austin Z. Henley, Jamie Macbeth, Charles Hill, and Amber Horvath. 
   ICSME '15: IEEE Int'l Conference on Software Maintenance and Evolution, Bremen, Germany, Sep./Oct. 2015. 
   
     DOI 
     PDF 
   

 

 2014 

 

   Helping Programmers Navigate Code Faster with Patchworks: A Simulation Study 
   Austin Z. Henley, Alka Singh, Scott D. Fleming, and Maria V. Luong. 
   VL/HCC '14: IEEE Symposium on Visual Languages and Human-Centric Computing, Melbourne, Australia, Jul./Aug. 2014. 
   
     DOI 
     PDF 
   

   The Patchworks Code Editor: Toward Faster Navigation with Less Code Arranging and Fewer Navigation Mistakes 
   Austin Henley and Scott Fleming. 
   CHI '14: ACM SIGCHI Conference on Human Factors in Computing Systems, Toronto, Canada, Apr./May 2014. 
   
     DOI 
     PDF 
   

 

 2013 

 

   What Use Is a Backseat Driver? A Grounded Theory Investigation of Pair Programming 
   Danielle Jones and Scott Fleming. 
   VL/HCC '13: IEEE Symposium on Visual Languages and Human-Centric Computing, San Jose, CA, Sep. 2013. 
   
     DOI 
     PDF 
   

   End-User Programmers in Trouble:  Can the Idea Garden Help Them to Help Themselves? 
   Jill Cao, Irwin Kwan, Faezeh Bahmani, Margaret Burnett, Josh Jordahl, Amber Horvath, Scott Fleming, and Sherry Yang. 
   VL/HCC '13: IEEE Symposium on Visual Languages and Human-Centric Computing, San Jose, CA, Sep. 2013. 
   
     DOI 
     PDF 
   

   The Whats and Hows of Programmers' Foraging Diets 
   David Piorkowski, Scott D. Fleming, Irwin Kwan, Margaret Burnett, Chris Scaffidi, Rachel K.E. Bellamy, and Joshua Jordhal. 
   CHI '13: ACM SIGCHI Conference on Human Factors in Computing Systems, Paris, France, May 2013. 
   
     DOI 
     PDF 
   

   An Information Foraging Theory Perspective on Tools for Debugging, Refactoring, and Reuse Tasks 
   Scott D. Fleming, Christopher Scaffidi, David Piorkowski, Margaret Burnett, Rachel K. E. Bellamy, Joseph Lawrance, and Irwin Kwan. 
    ACM Transactions on Software Engineering and Methodology . 22(2):14:1 14:41, Mar. 2013. 
   
     DOI 
     PDF 
   

   How Programmers Debug, Revisited: An Information Foraging Theory Perspective 
   Joseph Lawrance, Christopher Bogart, Margaret Burnett, Rachel Bellamy, Kyle Rector, and Scott D. Fleming. 
    IEEE Transactions on Software Engineering . 39(2):197 215, Feb. 2013. 
   
     DOI 
     PDF 
   

 

 2012 

 

   From Barriers to Learning in the Idea Garden: An Empirical Study 
   Jill Cao, Irwin Kwan, Rachel White, Scott D. Fleming, Margaret Burnett, and Christopher Scaffidi. 
   VL/HCC '12: IEEE Symposium on Visual Languages and Human-Centric Computing, Innsbruck, Austria, Sep. 2012. 
   
     DOI 
     PDF 
   

   Reactive Information Foraging: An Empirical Investigation of Theory-Based Recommender Systems for Programmers 
   David Piorkowski, Scott D. Fleming, Christopher Scaffidi, Christopher Bogart, Margaret Burnett, Bonnie E. John, Rachel K. E. Bellamy, and Calvin Swart. 
   CHI '12: ACM SIGCHI Conference on Human Factors in Computing Systems, Austin, TX, May 2012. 
   
     DOI 
     PDF 
   

   Information Foraging Theory for Collaborative Software Development 
   Irwin Kwan, Scott D. Fleming, and David Piorkowski. 
   FutureCSD '12: CSCW 2012 Workshop on The Future of Collaborative Software Development, Seattle, WA, Feb. 2012. 
   
     PDF 
   

 

 2011 

 

   Modeling Programmer Navigation: A head-to-head empirical evaluation of predictive models 
   David Piorkowski, Scott D. Fleming, Christopher Scaffidi, Liza John, Christopher Bogart, Bonnie E. John, Margaret Burnett, and Rachel Bellamy. 
   VL/HCC '11: IEEE Symposium on Visual Languages and Human-Centric Computing, Pittsburgh, PA, Sep. 2011. 
   
     DOI 
     PDF 
   

   An Exploration of Design Opportunities for "Gardening" End-User Programmers' Ideas 
   Jill Cao, Scott D. Fleming, and Margaret Burnett. 
   VL/HCC '11: IEEE Symposium on Visual Languages and Human-Centric Computing, Pittsburgh, PA, Sep. 2011. 
   
     DOI 
     PDF 
     Best Paper Award 
   

   Gender Pluralism in Problem-Solving Software 
   Margaret M. Burnett, Laura Beckwith, Susan Wiedenbeck, Scott D. Fleming, Jill Cao, Thomas H. Park, Valentina Grigoreanu, and Kyle Rector. 
    Interacting with Computers . 23(5):450 460, 2011. 
   
     DOI 
     PDF 
   

 

 2010 

 

   Debugging Concurrent Software: A Study Using Multithreaded Sequence Diagrams 
   Scott D. Fleming, Eileen Kraemer, R. E. K. Stirewalt, and Laura K. Dillon. 
   VL/HCC '10: IEEE Symposium on Visual Languages and Human-Centric Computing, Legan s-Madrid, Spain, Sep. 2010. 
   
     DOI 
     PDF 
   

   A Debugging Perspective on End-User Mashup Programming 
   Jill Cao, Kyle Rector, Thomas H. Park, Scott D. Fleming, Margaret Burnett, and Susan Wiedenbeck. 
   VL/HCC '10: IEEE Symposium on Visual Languages and Human-Centric Computing, Legan s-Madrid, Spain, Sep. 2010. 
   
     DOI 
     PDF 
   

   Gender Differences and Programming Environments: Across Programming Populations 
    Margaret Burnett, Scott Fleming, Shamsi Iqbal, Gina Venolia, Vidya Rajaram, Umer Farooq, Valentina Grigoreanu, and Mary Czerwinski. 
   ESEM '10: Int'l Symposium on Empirical Software Engineering and Measurement, Bolzano-Bozen, Italy, Sep. 2010 
   
     DOI 
     PDF 
   

 

 2009 

 

   Design and Evaluation of Extensions to UML Sequence Diagrams for Modeling Multithreaded Interactions 
   Shaohua Xie, Eileen Kraemer, R. E. K. Stirewalt, Laura K. Dillon, and Scott D. Fleming. 
    Information Visualization , 8(2):120-136, 2009. 
   
     DOI 
     PDF 
   

   Successful Strategies for Debugging Concurrent Software: An Empirical Investigation 
   Scott D. Fleming. 
   PhD thesis, Michigan State University, East Lansing, MI, 2009. 
   
     PDF 
   

 

 2008 

 

   Assessing the Benefits of Synchronization-Adorned Sequence Diagrams: Two Controlled Experiments 
   Shaohua Xie, Eileen Kraemer, R. E. K. Stirewalt, Laura K. Dillon, and Scott D. Fleming. 
   SoftVis '08: ACM Symposium on Software Visualization, Herrsching am Ammersee, Germany, Sep. 2008. 
   
     DOI 
     PDF 
   

   Refining Existing Theories of Program Comprehension During Maintenance for Concurrent Software 
   Scott D. Fleming, Eileen Kraemer, R. E. K. Stirewalt, Laura K. Dillon, and Shaohua Xie. 
   ICPC '08: IEEE Int'l Conference on Program Comprehension, Amsterdam, The Netherlands, Jun. 2008. 
   
     DOI 
     PDF 
   

   A Study of Student Strategies for the Corrective Maintenance of Concurrent Software 
   Scott D. Fleming, Eileen T. Kraemer, R. E. K. Stirewalt, Shaohua Xie, and Laura K. Dillon. 
   ICSE '08: Int'l Conference on Software Engineering Education Track, Leipzig, Germany, May 2008. 
   
     DOI 
     PDF 
   

   Using Formal Models to Objectively Judge Quality of Multi-Threaded Programs in Empirical Studies 
   Laura K. Dillon, R. E. K. Stirewalt, Eileen Kraemer, Shaohua Xie, and Scott D. Fleming. 
   MISE '08: Workshop on Modeling in Software Engineering, Leipzig, Germany, May 2008. 
   
     DOI 
     PDF 
   

 

 2007 

 

   Toward a Task Model of Concurrent Software Maintenance 
   Scott D. Fleming, R. E. K. Stirewalt, and Eileen T. Kraemer. 
   WEASELTech '07: Workshop on Empirical Assessment of Software Engineering Languages and Technologies, Atlanta, GA, Nov. 2007. 
   
     DOI 
     PDF 
   

   Using Program Families for Maintenance Experiments 
   Scott D. Fleming, R. E. K. Stirewalt, and Laura K. Dillon. 
   ACoM '07: Int'l Workshop on Assessment of Contemporary Modularization Techniques, Minneapolis, MN, May 2007. 
   
     DOI 
     PDF 
   

 

 2006 

 

   Separating Syncrhonization Concerns with Frameworks and Generative Programming 
   Scott D. Fleming, R. E. K. Stirewalt, Laura K. Dillon, and Beata Sarna-Starosta. 
   Tech. Report MSU-CSE-06-34. Department of Computer Science, Michigan State University, East Lansing, MI, Dec. 2006. 
   
     URL 
     PDF 
   

   Developing an Alloy Framework Akin to OO Frameworks 
   Laura K. Dillon, R. E. K. Stirewalt, Beata Sarna-Starosta, and Scott D. Fleming. 
   Alloy Workshop, Nov. 2006. 
   
     URL 
     PDF 
   

 

 2005 

 

   An Approach to Implementing Dynamic Adaptation in C++ 
   Scott D. Fleming, Betty H. C. Cheng, R. E. Kurt Stirewalt, and Philip K. McKinley. 
   DEAS '05: Workshop on Design and Evolution of Autonomic Application Software, St. Louis, MO, May 2005. 
   
     DOI 
     PDF 
   

   * Also see  my Google Scholar page  and  my ACM author profile page . 

 

 
 
     Scott D. Fleming 2010 2016 
     
     
 

 
 
