April - Library Spotlights - LibGuides at University of Memphis Law Library</title> <script type="text/javascript" src="/js/dojo-161/dojo/dojo.js" djConfig="parseOnLoad: true"></script> <script type="text/javascript" src="//code.jquery.com/jquery-1.7.min.js"></script> <script type="text/javascript" src="/include/lang.php" ></script> <script type="text/javascript" src="/js112b/lgpublic.min.js"></script> <script type="text/javascript" src="/include/lib_spring_track.php?action=init&iid=2952"></script> <script> // content_config is instantiated in content.js. content_config.pid = 625969; content_config.sid = '5880850'; content_config.iid = 2952; content_config.la_iid = 0; content_config.gid = 0; content_config.hitsource = 'r'; content_config.myboxshape = 's'; var myGNAME = "Library+Spotlights"; var myTNAME = "April"; var myIID = '2952'; var myPID = '625969'; var mySID = '5880850'; // SpringTracking SpringTrack.init({ _st_sid : content_config.sid, _st_iid : content_config.iid, _st_pid : content_config.pid }); </script> <title>Memphis School of Law: ResearchGuides Highlights of historical and current legal issues April 2017  
 
	 
    
         
		
		 
		 
		 
		         
         
         
          
                 
         
         
         
         
         
        
         
         
         
         
         
         
         
         
         
         
          
         
                
                
         
         
        
		        
         April - Library Spotlights - LibGuides at University of Memphis Law Library 
		
		
        
        
        
        
                
		
		 Memphis School of Law: ResearchGuides 

        	 
	 
		  August  September-October  Halloween  Veterans Day  November  December    January  February  March/April  May  October  D'Army Bailey  December    January  African American History Month  Women's History Month  April  May  June  July  August  September  October  World Series  November  National Eggnog Day    January  February  March  		 
                    This is the "April" page of the "Library Spotlights" guide. 
                     Alternate Page for Screenreader Users  
                     Skip to Page Navigation  
                     Skip to Page Content  
                 		 
			 
                  
				   
				                   
						 
							 Admin Sign In 
						    
					 Library 
					   
				  
					 LibGuides 
					   
				  Library Spotlights  
			
		                  
                         
                             Library Spotlights   
                             Tags:  attorneys ,  child support ,  church ,  civil ,  civil marriage ,  court ,  equality of descent ,  etiquette ,  family law ,  hispanic ,  holiday ,  marketing ,  massachusetts ,  plymouth ,  robert burns ,  salem ,  scut farkus ,  state ,  tennessee ,  treatment ,  veterans ,  volunteer    
                         
                                       Highlights of historical and current legal issues  				
		 
			 Last Updated: May 25, 2017 
			 URL: http://libguides.law.memphis.edu/libraryspotlights 
			 
				 
					 Print Guide
					 
				 
			    RSS Updates      Email Alerts   
		 				    
		 April     
		 2017       
		 2016       
		 2015       
		 2014                        
		 
			 
        		 
					 
						 April 
							 
								 
									 
										 Comments(0)
										 
									 
								 
							 
						 
							 
								 Print Page 
							  
						 
					 
					 
		 
			 
				 
				 Search Text 
				 Search Type 
				 
					 
						 
							 
								  Search: 
							 
						 
						 
							 
								  
							 
						 
						 
							 
								 
									 This Guide 
                All Guides 
               
								 
							 
						 
						 
							 
								 Search 
							 
						 
					 
				 
			 
		 
					 
				 
			 
		                   
                 
                     
                                        
                                         
                                             
	 
		
		 
			
			   
			 Poetry and the Law
			 
		 
			
		  April is National Poetry Month! Please enjoy these articles about poets   the law.         
		 
				 Comments (0) 
		 
		  
		 
	   
	 
		
		 
			
			   
			 Mastering Eliot's Paradox: Fostering Cultural Memory in an Age of Illusion & Allusion
			 
		 
			
		        
									  Jim Chen',200,4);" onMouseOut="return hideLinkDesc();">Minnesota Law Review,                  2004-2005     
		 
				 Comments (0) 
		 
		  
		 
	  
                                         
                                        
                                      
                                        
                                         
                                             
	 
		
		 
			
			   
			 The John William Corrington & Charles Bukowski Correspondence: On Poetry and Writing
			 
		 
			
		                                                                                                                                                            
									  James R. Elkins',200,4);" onMouseOut="return hideLinkDesc();">Legal Studies Forum,  2003     
		 
				 Comments (0) 
		 
		  
		 
	  
                                         
                                        
                                      
                                          
                                         
                                             
	 
		
		 
			
			   
			 Libel in Fiction: The Sylvia Plath Case & its Aftermath
			 
		 
			
		        
									  editors: Robert Callargy, Irwin Karp, Victor A. Kovner, Charles Rembar, Judith Rossner',200,4);" onMouseOut="return hideLinkDesc();">Columbia VLA Journal of Law & Arts,  1986-1987     
		 
				 Comments (0) 
		 
		  
		 
	  
                                         
                                        
                                     				 
                
 
	 
    	Powered by  Springshare ; All rights reserved. 
    	 
        	 Report a tech support issue .      	 
  	 
	
	 View this page in a format suitable for  printers and screen-readers  or  mobile devices .  

  
	  				 
                	 
                    	 Description 
                   	 
                     
                    	    Loading... 
                   	 
             	 
				 
				 
                	 
                    	 
                        	 
                            	 
                                	   
                               	 
                           	 
                      	 
                         
                        	 More Information 
                       	 
                 	 
                     
                    	 
                        	   Loading...                      	  
						  Close  
                   	 
              	 
			 
		 
                   
        
		
	 
 