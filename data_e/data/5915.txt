Graduate Organization of Social Work - Social Work - University of Memphis    










 
 
 
     



 
    
    
    Graduate Organization of Social Work - 
      	Social Work
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Social Work
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Undergraduate  
                          Graduate  
                          People  
                          Research  
                          Resources  
                          News  
                      
                      
                         
                            Graduate   
                            
                               
                                  
                                   About the MSW Program  
                                         Brochure  
                                         Program   Study Plans  
                                     
                                  
                                   Prospective MSW Students  
                                         MSW Information Sessions  
                                         Request Information Form  
                                         Program Eligibility  
                                         How to Apply   Deadlines  
                                         FAQs  
                                     
                                  
                                   Current Students  
                                        
                                         Student Handbook  
                                        
                                         Semester Schedules  
                                        
                                         MSW School Social Work Licensure Program Requirements  
                                        
                                         Phi Alpha Honor Society  
                                        
                                         Graduate Organization of Social Work (GO!SW)  
                                     
                                  
                                   Field  
                                         Field Handbook  
                                         Field Agencies  
                                         Forms  
                                         Field Powerpoint  
                                     
                                  
                                   Funding  
                                   UofM Graduate Catalog  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Social Work
                              	  
                          
                              	Graduate
                              	  
                         Graduate Organization of Social Work 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Graduate Organization of Social Work (GO!SW) 
                     
                      The Graduate Organization of Social Work (GO!SW) is a registered student organization
                        at the University of Memphis.
                      
                     
                      News update! 
                     
                      GO! SW and the Undergraduate Student Social Work Organization (SSWO) will be merging
                        this Fall. Check with your officers for more information.
                      
                     
                      MISSION: 
                     
                      The mission of the Graduate Organization of Social Work (GO!SW) at the University
                        of Memphis is to advance communication between students, faculty, administrators and
                        community service providers in the social service fields.
                      
                     
                      VISION: 
                     
                      The Graduate Organization of Social Work (GO!SW) is committed to social change by
                        promoting empowerment and social justice through service and outreach.
                      
                     
                        
                     
                        
                     
                        
                     
                      GO!SW Officers 
                     
                      
                        
                         Rachel Lang- President 
                        
                         
                           
                            Brianna Wachter - Vice President 
                           
                         
                        
                          Rodques Jones - Liaison  
                        
                         Jeremiah Berry - Treasurer 
                        
                         OPEN - Secretary 
                        
                         Akosua Aggery-Bekoe - Sergeant at Arms 
                        
                      
                     
                      Membership 
                     
                       Membership is open to any University of Memphis Social Work student currently in good
                           standing and registered at the University of Memphis for at least one or more credit
                           courses. See the GO!SW website for more information.  
                     
                       If you would like to become a member please complete the GO!SW membership application
                           and submit the membership application to uofmgosw@gmail.com.  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Graduate 
                         
                            
                               
                                About the MSW Program  
                                      Brochure  
                                      Program   Study Plans  
                                  
                               
                                Prospective MSW Students  
                                      MSW Information Sessions  
                                      Request Information Form  
                                      Program Eligibility  
                                      How to Apply   Deadlines  
                                      FAQs  
                                  
                               
                                Current Students  
                                     
                                      Student Handbook  
                                     
                                      Semester Schedules  
                                     
                                      MSW School Social Work Licensure Program Requirements  
                                     
                                      Phi Alpha Honor Society  
                                     
                                      Graduate Organization of Social Work (GO!SW)  
                                  
                               
                                Field  
                                      Field Handbook  
                                      Field Agencies  
                                      Forms  
                                      Field Powerpoint  
                                  
                               
                                Funding  
                                UofM Graduate Catalog  
                            
                         
                      
                      
                      
                         
                            
                                Why Study Social Work?  
                               The study of social work prepares you for a career in a strong growth area 
                            
                            
                                SUAPP  
                               The Department of Social Work is a part of the School of Urban Affairs and Public
                                 Policy.
                               
                            
                            
                                Support Social Work Education  
                               Your gifts make a difference! 
                            
                            
                                Contact Us  
                               Main office and location information. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


