Service Opportunities - Student Leadership &amp; Involvement - University of Memphis    










 
 
 
     



 
    
    
    Service Opportunities - 
      	Student Leadership   Involvement
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Student Leadership   Involvement
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Student Engagement  
                          Leadership   Service  
                          Community Engagement  
                      
                      
                         
                            Leadership   Service   
                            
                               
                                  
                                   Emerging Leaders  
                                         Overview  
                                         Apply  
                                         FAQ  
                                         Current EL Information  
                                     
                                  
                                   Social Change Scholars  
                                         Overview  
                                         Apply  
                                         FAQ  
                                     
                                  
                                   Leadership Training  
                                         Tiger Leadership Institute  
                                         Leader to Leader  
                                     
                                  
                                   Community Service  
                                        
                                         Service on Saturday  
                                        
                                         Days of Service  
                                        
                                         Volunteer Odyssey  
                                     
                                  
                                   Conference and Retreats  
                                         LeaderShape Institute  
                                         LEAD Conference  
                                         Topical Conferences  
                                     
                                  
                                   Alternative Breaks  
                                    Women’s Leadership   
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Student Leadership   Involvement
                              	  
                          
                              	Leadership   Service
                              	  
                         Service Opportunities 
                      
                   
                   
                       
                      
                     		
                     
                     
                      Service on Saturday 
                     
                      This monthly program, sponsored by the Civic Engagement Board, is designed to give
                        students, staff, and faculty a quick and easy way to volunteer in the community with
                        no long-term commitment. The program typically offers about 6-12 different community
                        service opportunities to choose from. Participants who sign up in advance online get
                        their first choice of these projects. The program generally begins at 8:30 am with
                        free breakfast and ends around 12:00 - 1:00 pm.
                      
                     
                      Fall 2017 Dates 
                     
                      September 30, 2017 8:30am-12:00pm 
                     
                      October 28, 2017 8:30am-12:00pm 
                     
                        Register on Volunteer Compass!   
                     
                        
                     
                        
                     
                     	
                      
                   
                
                
                   
                      
                         Leadership   Service 
                         
                            
                               
                                Emerging Leaders  
                                      Overview  
                                      Apply  
                                      FAQ  
                                      Current EL Information  
                                  
                               
                                Social Change Scholars  
                                      Overview  
                                      Apply  
                                      FAQ  
                                  
                               
                                Leadership Training  
                                      Tiger Leadership Institute  
                                      Leader to Leader  
                                  
                               
                                Community Service  
                                     
                                      Service on Saturday  
                                     
                                      Days of Service  
                                     
                                      Volunteer Odyssey  
                                  
                               
                                Conference and Retreats  
                                      LeaderShape Institute  
                                      LEAD Conference  
                                      Topical Conferences  
                                  
                               
                                Alternative Breaks  
                                 Women’s Leadership   
                            
                         
                      
                      
                      
                         
                            
                                Our Mission  
                               Learn about the key elements we use to enhance your college experience. 
                            
                            
                                Fraternity   Sorority Affairs  
                               Enhance your college experience by joining a fraternity/sorority! 
                            
                            
                                Tiger Zone  
                               Learn about student organizations, events   involvement opportunities. 
                            
                            
                                Contact Us  
                               Questions? Our team is here to help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/27/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


