 Q. Can I check out books if I'm not a U of M student? - LibAnswers   
 
 
 
	 
	 
	   
	 
		 
	 
	
     
        Q. Can I check out books if I'm not a U of M student? - LibAnswers     

	 	
    
    
    	
		
 
 
	 Skip to Main Content 
	 
		 
	 
		 
				 
				 
					  University Libraries Home Page  
					  LibAnswers   
				 
			  
				  
					 Q. Can I check out books if I'm not a U of M student? 
					  
				  
			  
				 
					 
			    		 
							   Toggle menu visibility 
						 
						
			    	 
			    	
			    	 		
						  Ask Another Question  
			 
			
			
			   
				 
			 
				
				 Search 
			 
			 
		  
						 
							  Browse:  
							  All  
							
							 
				 Topics    
				    36  Access to Resources      2  Alumni      7  Articles      13  Campus Resources      12  Catalog      11  Circulation      6  Citations      3  Collections      8  Computers      5  Copying      28  Databases      2  Dissertations & Theses      2  Ebooks      2  Educational Support Program      2  Faculty      2  Genealogy      18  Government Resources      1  Group Study Rooms      7  Guests      6  Hours and Locations      5  Instructional Services      6  Interlibrary loan      3  IT Helpdesk      6  Journals      3  Laptops      13  Library Services & Policies      2  Microform      5  Newspapers      1  Nursing      1  Obituaries      1  Off Campus      1  Periodicals      1  Preservations & Special Collections      9  Printing      1  Reciprocal Borrowing      2  RefWorks      7  Research Guides      22  Research Help      5  Reserve Room      3  RODP      1  Scanners      6  Statistics      4  Tests & Testing      2  Textbooks      2  Tutoring      7  UUID & Password      13  Video Tutorials      21  Websites      4  Writing    
			 
						 
	
					 
				 
			   
			  
		    
			 
				
				 
		    		 
						  Toggle action bar  
					 
					 FAQ Actions 
		    	 
		    	
				  
					  
							 
								     Print 
							 
						 
						 
							 
								    Tweet 
							 
						 
						 
							 
							    Share on Facebook 
							 
						 
						
					 				
					
					  Was this helpful? 
	    	 
	    		  
	    		  
	    		 Yes 
	    	 
	    	 1  
	    	  
	    		  
	    		  
	    		 No 
	    	  
	    	 0  

				 

			 
		  
				 
					  Topics   
					   
  Access to Resources    Alumni    Articles    Campus Resources    Catalog    Circulation    Citations    Collections    Computers    Copying    Databases    Dissertations & Theses    Ebooks    Educational Support Program    Faculty    Genealogy    Government Resources    Group Study Rooms    Guests    Hours and Locations     Instructional Services    Interlibrary loan    IT Helpdesk    Journals    Laptops    Library Services & Policies    Microform    Newspapers    Nursing    Obituaries    Off Campus    Periodicals    Preservations & Special Collections    Printing    Reciprocal Borrowing    RefWorks    Research Guides    Research Help    Reserve Room    RODP    Scanners    Statistics    Tests & Testing    Textbooks    Tutoring    UUID & Password    Video Tutorials    Websites    Writing     View All Topics     
					
				 
			    
				 
					      Answered By:  Cindi Nichols        Last Updated:  Oct 12, 2017          Views:  384      
					   Any member of the general public over 18 with a local address can receive a  Community Special Privileges Card  and check out materials after their  application  has been processed and entered into the system. Approved applicants will be issued a UofM Libraries  Special Privilege card at the McWherter Library Circulation desk (on the 1st floor). Additional borrowing information can be found  here . 

 Local high school students can fill out an  application  for a  High School Special Privileges Card  for research that requires access to the University Libraries  collection. Referral by their high school librarian is required. If the student is under the age of 18, parental approval is required.  

 
  Guest computer use policy:  

 Guest computer logins are limited to one, two-hour login per day. Guest computer logins are available during the following time periods: 

 Monday thru Thursday   8:00 am - 9:00 pm 

 Friday   8:00 am - 6:00 pm 

 Saturday   10:00 am - 6:00 pm  

 Sunday   1:00 pm - 9:00 pm  

 Guests will require a Community Special Privileges Card to request guest computer access (granted space availability). Adults accompanied by children may request one additional guest log-in for children if space is available. Guests under 18 years of age must be accompanied by an adult. 

 Students and faculty from  reciprocal institutions  should present their institutional ID when submitting an  application .  
  Links & Files        Special Privileges Card Registration Form         High School Special Privileges Card Registration Form         Visitors Check-Out Guidelines         Borrowing with Local Libraries     

 
					
				 
			    
				 
					  Chat   
					      
					
				 
			  
				 
					  Other Ways to Contact Us   
					  
			                                   
                                


		  
					
				 
			    
			  
				 
					  Comments (0)   
					   
			 
				 Add a public comment to this FAQ Entry 
			  
					
				 
			  
				 
					     
					  
			      University Libraries  -  The University of Memphis  - Memphis, TN 38152 - 901-678-2205 


		  
					
				 
			  
		   
    		  Powered by   Springshare ; All rights reserved.   Report a tech support issue.     Login to LibApps    	 
	 
		 
          
 
 