Scholarships - Department of Physics and Materials Science - University of Memphis    










 
 
 
     



 
    
    
    Scholarships - 
      	Department of Physics and Materials Science
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Physics and Materials Science
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Undergraduate  
                          Graduate  
                          People  
                          Research  
                          Resources  
                          News  
                          SPS  
                      
                      
                         
                            Undergraduate   
                            
                               
                                  
                                   Prospective Students  
                                         Admission Requirements  
                                         Four Year Plan  
                                     
                                  
                                   Current Students  
                                         Program Requirements  
                                         Course Requirements  
                                         Program Timeline  
                                         Second Major in Physics  
                                         Contact your Advisor  
                                     
                                  
                                   Scholarships  
                                   Financial Aid  
                                   Permit  
                                   Tutoring  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of Physics and Materials Science
                              	  
                          
                              	Undergraduate
                              	  
                         Scholarships 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Undergraduate Scholarships 
                     
                      The Department of Physics and Materials Science has scholarships available for undergraduate
                        students based on need and academic performance. Please contact the Undergraduate
                        Advisor for information on applying for the:
                      
                     
                      Albert Woollett Memorial Scholarship 
                     
                      Eligibility 
                     
                      Students who meet the minimum requirements outlined below are eligible to apply for
                        this scholarship.
                      
                     
                      
                        
                         
                           
                            The student must be a registered physics major at the University of Memphis. • The student must have completed at least 12 hours of coursework in physics. • The student must be a full-time student with a cumulative grade point average (GPA)
                              of at least 3.0. • The student must not have received a grade below a B in any Physics course taken
                              in the last three years.
                            
                           
                            Preference will be given to students who have
                               
                                 
                                  completed a greater number of semester hours in Physics 
                                 
                                  a high GPA in Physics 
                                 
                                  a greater financial need 
                                 
                                  made a substantial contribution in undergraduate research 
                                 
                                  contributed strongly to the activities of the SPS chapter 
                                 
                               
                            
                           
                            The recipient may retain the scholarship until graduation if the eligibility requirements
                              are maintained subject to the approval of the selection committee. The scholarship
                              may not be retained for more than eight semesters (fall and spring).
                            
                           
                         
                        
                      
                     
                      Apply 
                     
                      Please follow these steps to make a donation to the Albert Woollett Memorial Scholarship. 
                     
                      
                        
                         Click on the link provided below. 
                        
                         Enter gift amount (enter) 
                        
                         Choose "I would like to give to a fund not listed here" (enter) 
                        
                         Write "Woollett Scholarship" (enter) 
                        
                         Proceed with the remaining questions 
                        
                      
                     
                      To apply for the scholarship, or for more information, please visit the University
                        of Memphis Scholarship Office  website . 
                      
                     
                      
                     
                      Robert R. Marchini Scholarship 
                     
                      Dr. Robert Marchini received the Distinguished Teaching Award in 2008. Professor Robert
                        "Bob" Marchini has been teaching at the UofM since 1967. He also served as the undergraduate
                        advisor and laboratory coordinator in his department and advisor for both Sigma Pi
                        Sigma and the Society of Physics Students. He is well known for his "Magic of Physics"
                        demonstrations, in which he uses an assortment of magician-like tricks to illustrate
                        principles of physics.
                      
                     
                      For more information about this scholarship,  click here  
                     
                      Eligibility 
                     
                      THE AWARD(S) SHALL BE GIVEN TO: 
                     
                      Deserving and qualified students at the University of Memphis majoring in Physics
                        who have
                      
                     
                      
                        
                         
                           
                            completed the Intro to Physics course 
                           
                            Minimum GPA of 2.75 
                           
                            Preference shall be given to high-ability students with a demonstrated financial need. 
                           
                         
                        
                      
                     
                      Each scholarship shall be awarded for one academic year. Recipients may reapply in
                        subsequent years provided they maintain a minimum cumulative GPA of 2.75 and participate
                        in fifteen (15) hours of service for the Department of Physics and Materials Science
                        each semester.
                      
                     
                      
                        
                         
                           
                            Award recipients, the number of awards, and the amount of each award shall be determined
                              by a committee appointed by the Dean, College of Arts and Sciences, which will include
                              Dr. Robert R. Marchini as long as he is willing and able to participate.
                            
                           
                            The selection process shall be coordinated through the  UofM Scholarship Office .
                            
                           
                         
                        
                      
                     
                      Apply 
                     
                      To apply for the scholarship, or for more information, please visit the University
                        of Memphis Scholarship Office  website .
                      
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Undergraduate 
                         
                            
                               
                                Prospective Students  
                                      Admission Requirements  
                                      Four Year Plan  
                                  
                               
                                Current Students  
                                      Program Requirements  
                                      Course Requirements  
                                      Program Timeline  
                                      Second Major in Physics  
                                      Contact your Advisor  
                                  
                               
                                Scholarships  
                                Financial Aid  
                                Permit  
                                Tutoring  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now  
                               Now accepting applications for the Undergraduate and Graduate Programs, MS in Physics
                                 and Materials Science and PhD in Engineering Physics!
                               
                            
                            
                                Contact Us  
                               Main office location and numbers, undergraduate and graduate advising, and Alumni
                                 information update form
                               
                            
                            
                                Seminars   Events  
                               The department presents weekly seminars during the spring and fall semesters 
                            
                            
                                Careers in Physics and Materials Science  
                               Find out what opportunities are available to you with a degree in Physics and Materials
                                 Science
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


