Graduate Assistants - Computer Science - University of Memphis    










 
 
 
     



 
    
    
    Graduate Assistants  - 
      	Computer Science
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Computer Science
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          News  
                          Future Students  
                          Current Students  
                          Research  
                          People  
                          Outreach  
                      
                      
                         
                            People   
                            
                               
                                  
                                   Full-Time Faculty  
                                   Affiliate Faculty  
                                   Part-Time Instructors  
                                   Staff  
                                   Postdoctoral Fellows   Researchers  
                                   Graduate Assistants  
                                   Industrial Advisory Board  
                                   Retired Faculty  
                                   Alumni  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Computer Science
                              	  
                          
                              	People
                              	  
                         Graduate Assistants  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Graduate Assistants (Fall 2017) 
                     
                      Departmental Assistants 
                     
                      
                        
                         
                           
                            
                              
                                 
                              
                                Sayma Akther   sakther@memphis.edu   FIT 314  Research Interests: mHealth, data mining, machine learning
                               
                              
                            
                           
                            
                              
                                 
                              
                                Katie Bridson   kbridson@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                Adithya Chakilam   chakilam@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                Keli Cheng   kcheng@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                Saikat Das   sdas1@memphis.edu   Dunn Hall 314/210  Research Interests: Computer networks, cyber security, cryptography, software engineering
                               
                              
                            
                           
                            
                              
                                 
                              
                                 Saurab Dulal    sdulal@memphis.edu   Dunn Hall 226  Research Interests: Named Data Networking, computer security, cryptography
                               
                              
                            
                           
                            
                              
                                 
                              
                                Laqin Fan   lfan1@memphis.edu   Dunn Hall 221  Research Interests: Named Data Networking
                               
                              
                            
                           
                            
                              
                                 
                              
                                Zannatul Firdous   zfirdous@memphis.edu   Research Interests: Databases, artificial intelligence, computational complexity
                               
                              
                            
                           
                            
                              
                                 
                              
                                Ashok Kumar Gadde   agadde@memphis.edu   Research Interests: Machine learning, data science, artificial intelligence
                               
                              
                            
                           
                            
                              
                                 
                              
                                Rakshit Gangarapu Chinnaswamy Naidu   rgngrpch@memphis.edu   Dunn Hall 314  Research Interests: Data science, machine learning
                               
                              
                            
                           
                            
                              
                                 
                              
                                Md Maminur Islam   mislam3@memphis.edu   Research Interests: Machine learning, artificial intelligence, big data, natural language
                                 processing
                               
                              
                            
                           
                            
                              
                                 
                              
                                Rojina Maharjan   rmharjan@memphis.edu   Dunn Hall 314  Research Interests: Natural language processing, cyber security
                               
                              
                            
                           
                            
                              
                                 
                              
                                Sambriddhi Mainali   smainali@memphis.edu   Dunn Hall 247  Research Interests: Data mining, data science, machine learning
                               
                              
                            
                           
                            
                              
                                 
                              
                                Ayushi Mehta   amehta2@memphis.edu   Research Interests: Cyber security, artificial intelligence
                               
                              
                            
                           
                            
                              
                                 
                              
                                Tyler Moore   tgmoore@memphis.edu   Dunn Hall 247  Research Interests: Self-assembly of complex systems
                               
                              
                            
                           
                            
                              
                                 
                              
                                Trang Pham   dpham2@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                Rong Qi   rqi@memphis.edu   Research Interests: Machine learning, deep learning, natural language processing
                               
                              
                            
                           
                            
                              
                                 
                              
                                 Borhan Samei    bsamei@memphis.edu   FIT 403G  Research Interests: Natural language processing, machine learning, AI, educational
                                 technologies
                               
                              
                            
                           
                            
                              
                                 
                              
                                Shahrokh Shahi   sshahi@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                Mohammad Farooq Shamim   mfshamim@memphis.edu   Dunn Hall 314  Research Interests: Software engineering, database management (SQL and NoSQL), network
                                 security, data science (natural language processing, machine learning, hidden Markov
                                 models)
                               
                              
                            
                           
                            
                              
                                 
                              
                                Jobin Sunny   jjsunny@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                Lasang Tamang   ljtamang@memphis.edu   FIT 403  Research Interests: Information retrieval, machine learning, artificial intelligence,
                                 data mining
                               
                              
                            
                           
                            
                              
                                 
                              
                                Quang Tran   qmtran@memphis.edu   Dunn Hall 217  Research Interests: Bioinformatics, computational biology, machine learning
                               
                              
                            
                           
                         
                        
                      
                     
                      Research Assistants 
                     
                      
                        
                         
                           
                            
                              
                                 
                              
                                Raasi Annavajjala   rmnnvjjl@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                Rabin Banjade   rbnjade1@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                 Daya Ram Budhathoki    dbdhthki@memphis.edu   Dunn Hall 120  Research Interests: Computer systems and security, network security, big data
                               
                              
                            
                           
                            
                              
                                 
                              
                                Soujanya Chatterjee   schttrj1@memphis.edu   FIT 314/316  Research Interests: mHealth, mobile sensor big data, predictive analytics, applied
                                 machine learning, data mining, computational modeling of human behavior
                               
                              
                            
                           
                            
                              
                                 
                              
                                Muktadir Chowdhury   mrchwdhr@memphis.edu   Dunn Hall 221  Research Interests: Computer networks, future Internet architecture, content-centric
                                 networks, vehicular ad-hoc networks
                               
                              
                            
                           
                            
                              
                                 
                              
                                Saurab Dulal   sdulal@memphis.edu   Dunn Hall 226  Research Interests: Named Data Networking, computer security, cryptography
                               
                              
                            
                           
                            
                              
                                 
                              
                                Senjuti Dutta   sdutta1@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                Khan Mohammad Al Farabi   kfarabi@memphis.edu   Dunn Hall 314  Research Interests: Probabilistic graphics models, machine learning, artificial intelligence
                                 (particularly Markov logic networks)
                               
                              
                            
                           
                            
                              
                                 
                              
                                 Kishor Datta Gupta    kgupta1@memphis.edu   Dunn Hall 117  Research Interests: Blockchain, cryptocurrency, image processing, AI, data encoding
                               
                              
                            
                           
                            
                              
                                 
                              
                                Md Shiplu Hawlader   mhwlader@memphis.edu   FIT 314  Research Interests: mHealth, machine learning
                               
                              
                            
                           
                            
                              
                                 
                              
                                Austin Henley   azhenley@memphis.edu   Research Interests: Software engineering
                               
                              
                            
                           
                            
                              
                                 
                              
                                Anik Khan   akhan9@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                Subash Poudyal   spoudyal@memphis.edu   FIT 335A  Research Interests: Cyber security, information security, authentication systems,
                                 data mining, software engineering
                               
                              
                            
                           
                            
                              
                                 
                              
                                Md Lutfar Rahman   mrahman9@memphis.edu   Dunn Hall 314  Research Interests: Cloud computing, network/cyber security, game theory
                               
                              
                            
                           
                            
                              
                                 
                              
                                Mithun Saha   msaha1@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                Nazir Saleheen   nsleheen@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                Sajib Sen   ssen4@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                Md Azim Ullah   mullah@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                Berkeley Willis   bwillis2@memphis.edu  
                              
                            
                           
                            
                              
                                 
                              
                                Alina Zaman   azaman@memphis.edu   FIT 314  Research Interests: mHealth, machine learning
                               
                              
                            
                           
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         People 
                         
                            
                               
                                Full-Time Faculty  
                                Affiliate Faculty  
                                Part-Time Instructors  
                                Staff  
                                Postdoctoral Fellows   Researchers  
                                Graduate Assistants  
                                Industrial Advisory Board  
                                Retired Faculty  
                                Alumni  
                            
                         
                      
                      
                      
                         
                            
                                Career Paths  
                               Internship information and job postings for students 
                            
                            
                                Degree Programs  
                               Information on our degree and certificate programs 
                            
                            
                                Courses  
                               Recent syllabi for our courses 
                            
                            
                                Contact Us  
                               Whom to contact about what 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


