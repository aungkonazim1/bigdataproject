Trey Wiedman - UofM Media Room - University of Memphis    










 
 
 
     



 
    
    
    Trey Wiedman - 
      	UofM Media Room
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			UofM Media Room
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Publications  
                          News Releases  
                          Events  
                          Resources  
                          Contact Us  
                      
                      
                         
                            News Releases   
                            
                               
                                  
                                   Awards  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	UofM Media Room
                              	  
                          
                              	News Releases
                              	  
                         Trey Wiedman 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Former Tiger makes his way into the World Series 
                     
                      October 25, 2017 - Those pesky injuries that plagued former Memphis Tigers baseball
                        player Trey Wiedman didn't keep him from his lifelong goal of making the World Series—it
                        only altered his path to baseball's grand event.
                      
                     
                      Wiedman, who starred for the Tigers from 2006-2010, finds himself in the thick of
                        the Houston Astros-Los Angeles Dodgers World Series tussle as the Astros' assistant
                        strength and conditioning coach. Turn on the TV the next few days and there's a good
                        chance you might just catch a glimpse of him in the dugout.
                      
                     
                      "Injuries definitely hindered my career—they motivated me early on to know exactly
                        what I wanted to do after baseball, which was to be a strength and conditioning coach,"
                        says Wiedman, who led Memphis in batting average (.302), hits (60) and home runs (8)
                        in 2008. "I knew I wanted to do everything I could do to help steer athletes in the
                        right direction where they don't have the same thing happen to them.
                      
                     
                      "You can't prevent injuries, but you can prepare athletes the best way you can and
                        hopefully lower their risks."
                      
                     
                      Wiedman says being at the World Series is a dream come true, and he says his time
                        at Memphis paved the way. "I am fortunate to be where I am right now. My playing experience
                        and the excellent education I got from our kinesiology department and from the UofM
                        helped me get my foot in the door."
                      
                     
                      As a strength and conditioning coach for the Astros, Wiedman's job is to prepare players
                        for the rigors of a 162-game schedule plus any postseason games: "That includes the
                        players' weight room workouts, monitoring their hydration levels, ensuring they have
                        proper supplement intake all the way to how they recover," he says.
                      
                     
                      Wiedman's path to the majors oddly enough resembled that of a minor league player.
                        After interning for the Astros in 2011 the year after he graduated, he worked for
                        Houston's Single-A team at the time, Lexington, and later for the Astros' Double-A
                        affiliate, Corpus Christi, before being promoted to the Astros.
                      
                     
                      "It happened almost the same way a player moves up through the minor leagues—you go
                        through the process of grinding it out through the lower levels.
                      
                     
                      "You almost feel like a player because you are coming up with the players. The Houston
                        players we now have in the majors, I was with them five years ago in the lower leagues.
                        You form some incredible relationships with them."
                      
                     
                      Wiedman says he also gained experience as the strength coach for Houston High School
                        (his alma mater) "especially that first year after I graduated. There were some incredible
                        athletes at that school."
                      
                     
                      Wiedman is intent on staying on the cutting edge of his sport. "Right now I am studying
                        to become a registered dietitian because I believe the nutrition aspect is so underrated
                        and can have such a big effect on a player. That is something that can help me be
                        a better strength and conditioning coach. I feel like strength and conditioning coach
                        plus dietician is going to be a big thing."
                      
                     
                      FOR MORE INFORMATION Gabrielle Maxey 901.678.2135  gmaxey@memphis.edu  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         News Releases 
                         
                            
                               
                                Awards  
                            
                         
                      
                      
                      
                         
                            
                                Music Performance Calendar  
                               Don't miss a School of Music performance 
                            
                            
                                Art Museum of Memphis  
                               Learn about the latest installations 
                            
                            
                                The Martha and Robert Fogelman Galleries of Contemporary Art  
                               View upcoming Department of Art exhibits 
                            
                            
                                Contact Us  
                               Have a story to share or questions about UofM News? 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


