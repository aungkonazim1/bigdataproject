Curriculum - OMBA - University of Memphis    










 
 
 
     



 
    
    
    Curriculum - 
      	OMBA
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - Online Professional MBA
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Welcome  
                          Academics  
                          Admissions  
                          Tuition   Fees  
                          Calendar  
                          Faculty  
                          FCBE  
                      
                      
                         
                            Academics   
                            
                               
                                  
                                   Academics  
                                         Curriculum  
                                         Current Class Schedule  
                                     
                                  
                                   Admissions  
                                         How to Apply  
                                         Application Deadlines  
                                         Class Profile  
                                         Employment Profile  
                                     
                                  
                                   Tuition   Financial Aid  
                                         Tuition   Fees  
                                         Financial Aid  
                                     
                                  
                                   Calendar  
                                   Faculty  
                                   Contact Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	OMBA
                              	  
                         
                           	Academics
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Curriculum 
                     
                      The vast majority of our MBA students are working professionals who don't have time
                        for fluff. They want a curriculum that provides a strategic understanding of today's
                        business problems, and the latest knowledge, best practices and tools to solve those
                        problems.
                      
                     
                      Our MBA students are challenged to apply what they've learned in the classroom --
                        to put it to work the next day in their companies and organizations. Thus, our courses
                        are taught with demanding, no-nonsense approaches that stress application of knowledge
                        and measurable results.
                      
                     
                      The Online Professional MBA program is a 33 credit-hour program that consists of a
                        30 credit-hour Core (common to all of our MBA programs) and one three-credit-hour
                        elective course.  There are no prerequisite courses required for the core curriculum.  The following three courses: ACCT 7050, Law, Ethics, and Corporate Governance; MGMT
                        7135, Leadership and Team-Building; and MKTG 7555, Creativity and Innovation, can
                        be taken as a three-day, on-campus, residency course. These courses will be scheduled
                        on a rotating basis in the weeks before the beginning of a semester.
                      
                     
                      Core Classes (30 hrs.) 
                     
                        MGMT 7135: Seminar in Leadership.   (2). Theoretical and practical consideration of leadership in high performing business
                        organizations; detailed analysis of relevant organizational behavior concepts; particular
                        focus on theories of motivation, styles of leadership and emotional intelligence.
                      
                     
                       MKTG 7555: Creativity and Innovation.  (2). Focused analysis and discussion of the imaginative, creative processes used
                        for innovation in business contexts. Theoretical underpinnings of creativity and innovation
                        are explored; special attention is given to environmental effects on individual and
                        group creativity. Creativity knowledge is applied in the areas of product ideation,
                        innovation management, and product design.
                      
                     
                       ACCT 7050: Corporate Governance and Business Ethics.  (2). Detailed analysis of the role of corporate governance in the free enterprise
                        system and capital markets; focused consideration of moral principles, ethical standards,
                        and corporate code of business ethics.
                      
                     
                       ACCT 7080: Financial and Managerial Accounting for Managers.  (3). Use of accounting information by an organization's investors, creditors, regulatory
                        authorities and managers; develop financial and credit analysis skills that are useful
                        in business decision making. Analysis of accounting information that can be used by
                        management to monitor the efficiency, quality, and timeliness of its operations; pricing
                        and costing of products and services, planning, and performance measurement.
                      
                     
                       ECON 7100: Economics for the Global Executive.  (3). Concepts and tools of economic theory and their application to business and
                        social issues in the context of a global economy; the decisions of firms, consumers
                        and governments and how their interactions determine market outcomes; market structures,
                        the impact of international trade and currency markets on firms that compete in a
                        global economy.
                      
                     
                       MIS 7650: Information Systems in the Global Enterprise.  (3). Information technology's impact on globalizations of businesses; international
                        IT environment; models and issues in international IS; planning and managing global
                        systems; case studies and applications.
                      
                     
                       FIR 7155: Global Financial Management.  (3). Theory and practice of modern financial theory as it is currently practiced
                        in an interdependent global economy by corporate financial managers, financial consultants
                        and managers of financial institutions.
                      
                     
                       SCMS 7313: Global Operations Management.  (3). A comprehensive course that addresses the acquisition, transformation and distribution
                        of goods and services within the global supply chain. The course will present concepts,
                        tools and strategies used to design and manage operations. Topics covered in the course
                        include, but are not limited to: strategic implications, performance measurement,
                        process management, sourcing, operations design, quality, inventory, logistics, enabling
                        information systems and technology, and global issues.
                      
                     
                       SCMS 7110: Intro to Business Analytics.  (3). Statistical concepts and tools, optimization and simulation techniques useful
                        in understanding, assessing, and controlling operations of business and economic society.
                      
                     
                       MKTG 7140: Global Strategic Marketing.  (3). Marketing strategy and in-depth analysis of issues impacting global management
                        of marketing, including: interrelationships among global business environments and
                        strategies, analysis value creating global strategies, competitive intelligence gathering,
                        customer segment analysis, integrated marketing technologies, customer relationship
                        management.
                      
                     
                       MGMT 7160: Global Strategic Management.  (3). Decisions and actions for development and implementation of long-term plans that
                        determine organizational performance; role of top management decision making in establishing
                        the firm’s mission; focus on strategic analysis of alternative actions; evaluation
                        of environmental conditions, industry characteristics, and organizational capabilities
                        in determining strategy in a global context.  *Can only be taken in the last semester as the Capstone course for the program*  
                     
                      ELECTIVE (3 HRS.) 
                     
                      Students who do not desire an MBA concentration are required to take a 3 hour elective
                        (approved by the MBA Program Director) beyond the 30 hour Core. Electives vary from
                        semester to semester. Please see the Current Class Schedule for a list of approved
                        electives.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Academics 
                         
                            
                               
                                Academics  
                                      Curriculum  
                                      Current Class Schedule  
                                  
                               
                                Admissions  
                                      How to Apply  
                                      Application Deadlines  
                                      Class Profile  
                                      Employment Profile  
                                  
                               
                                Tuition   Financial Aid  
                                      Tuition   Fees  
                                      Financial Aid  
                                  
                               
                                Calendar  
                                Faculty  
                                Contact Us  
                            
                         
                      
                      
                      
                         
                            
                                How To Apply  
                               What's your first step? Find out here! 
                            
                            
                                Class Profile  
                               Curious to know who is attending our program?  The answers are here! 
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


