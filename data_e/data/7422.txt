Webcasting a live CaptureCast presentation | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Webcasting a live CaptureCast presentation | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Capture    »    Using Capture   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Webcasting a live CaptureCast presentation                       
        
        
       
        
     
              
	If you are webcasting a live presentation, use a high-speed internet connection to ensure quality streaming for viewers. There should be at least 1 Mbit/s of upstream and downstream network bandwidth availability for SD videos, and 3+ Mbit/s for HD videos. Additionally, network ports for RTMP (Port 1935) and RTMPT/HTTP (Port 80) must be open.
 

 
	You should create and schedule your live event in Capture Portal or Capture Central before you begin webcasting to inform viewers ahead of time about the event. Once you create a live event, its information and access link appear in Capture Portal's Calendar page and in the Live Event Schedule widget on the Capture Portal Home page. If you embed a live event in Learning Environment, the event remains paused in the Capture viewer until the presenter begins the live webcast session. The Capture viewer automatically loads the live webcast once it begins.
 

 
	 Tip If your presentation is longer than three hours, we recommend you divide it into multiple live events to ensure you minimize interruptions during lengthy recordings. For example, you can create two 2.5-hour long live events for a five hour presentation.
 

 
	Present a live webcast
 

  
		On the main Capture Software screen, click    Webcast .
	 
	 
		Do one of the following:
		  
				Select your pre-scheduled live event's name from the  Event  drop-down list. The event names listed correspond to "Upcoming" live events in the Capture Portal or Capture Central connected to your Capture Software.
				 
					 Note By default, the Title, Presenter, and Layout fields, and the Chat check box automatically populate with information entered from Capture Portal or Capture Central. You can change this information by selecting different options on this screen.
				 
			 
			 
				Create a new webcast by selecting    Create a new event  from the Event drop-down list. Enter information for your live event.
			 
		  
	 
		Click  Continue .
	 
	 
		You can switch between  Add Media  and  Capture Screen  during a webcast if you want to display static files and images, and desktop demonstrations. You can do the following to enhance your presentation:
		  
				Click  Add Media  to import files (PPT, PPTX DOC, DOCX, PDF) to the presentation. This option is ideal for presenting static slides or document pages.
			 
			 
				Click  Capture Screen (Image Mode)  to share your desktop as screenshots.
			 
			 
				Click  Capture Screen (Video Mode)  to share your desktop in continuous video. This option is ideal for presenting animated content such as web browsing or a software demonstration.
			 
		  
	 
		Once the interface loads, you can see audio and video preview feeds in the encoder screen. Click    Start  to begin streaming the live webcast in Capture Portal or Learning Environment.
		 
			 Tip You can click the    Pause  icon anytime during streaming to pause your live stream. Paused sections of your presentation are not released to viewers. Click the    Start  icon to resume streaming.
		 
	 
	 
		When you are finished webcasting, click    Stop .
		 
			 Important Clicking    Stop  during a live event indicates you are finished recording. Clicking the    Start  icon after clicking    Stop  overwrites your current recording with a new recording.
		 
	 
	 
		Click  Finish  to return to the main Capture Software screen.
	 
      Audience:     Instructor       

    
           

                   ‹ Using Capture 
        
                   up 
        
                   Recording an offline CaptureCast presentation › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Capture  
  
      Participating in CaptureCast presentations    Understanding Capture components    Using Capture    Webcasting a live CaptureCast presentation    Recording an offline CaptureCast presentation    Publishing a CaptureCast presentation    Creating live events    Managing live events    Managing published CaptureCast presentations    Managing folders    Presenting and publishing with Web Capture    Managing participants in live event chat    Understanding access controls      Capture Station    Editing in post-production    Understanding Capture Toolbox    Capture Central in Learning Environment    
                  
           
         

       
     

    
    
    
   
 
   
 
