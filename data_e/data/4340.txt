Projects 2016 - FedEx Institute - University of Memphis    










 
 
 
     



 
    
    
    Projects 2016 - 
      	FedEx Institute
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			FedEx Institute of Technology
                           	 
                          
                   
                
             
          
       
       
          
             
                
                    
                         
                           			
                           
                            
                           
                           	
                           
                
             
          
          
             
                
                   
                      
                          About  
                          Research  
                          Events  
                          Training  
                          OTT  
                          News  
                          Contact Us  
                      
                      
                         
                            Research   
                            
                               
                                  
                                   Biologistics  
                                   CAST  
                                   Drones  
                                   IIS  
                                   MD2K  
                                   Smart Cities  
                                   Additive Manufacturing  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	FedEx Institute
                              	  
                          
                              	Research
                              	  
                         Projects 2016 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Past CAST Research Projects: 
                     
                      
                        
                         
                           
                            CAST 
                           
                            
                              
                                 About   
                              
                                 Events   
                              
                                Research Projects 
                                 
                                  
                                    
                                       2017 Projects   
                                    
                                       2016 Projects   
                                    
                                  
                                 
                               
                              
                                 Leadership   
                              
                                Trainings 
                                 
                                  
                                    
                                       CAST Foundations of Certification   
                                    
                                       Software Testing Excellence Program (STEP)   
                                    
                                  
                                 
                               
                              
                            
                           
                         
                        
                      
                     
                      Investigation and Testing of Cybersecurity - Protective Relay (Mohd Hasan Ali, Dipankar
                        Dasgupta)
                      
                     
                      Automated Document Classification - Sensitive Information Disclosure (Zhuo Lu, Dipankar
                        Dasgupta, Su Chen)
                      
                     
                      Cloud Computing Security and Privacy Assessment (Sajjan Shiva) 
                     
                      Security Online - Healthcare Communities (Naveen Kumar, Deepak Venugopal, Robin Poston,
                        Dipankar Dasgupta)
                      
                     
                      Cognitive Neuroscience - Security Behaviors in Information Security Contexts (Thomas
                        Stafford, George Deitz)
                      
                     
                      Technology Dependency Perspectives on Cybersecurity Failures (Thomas Stafford, Sanderford
                        Schaeffer)
                      
                     
                      Exploring a Data-Centric Approach to Securing Smart Homes (Lan Wang) 
                     
                      Investigating Characteristics of Cyberbullying in Higher Education (Mitsunori Misawa) 
                     
                      The Effects of Gamification on Security Compliance (Bill Kettinger, Chen Zhang, Ruby
                        Booth)
                      
                     
                      Cybersecurity Employment Pipelines: Successful Paths to Careers in Cybersecurity (Judy
                        Simon, Sandi Richardson, Ruby Booth)
                      
                     
                      Criminology and Cyber Security Dimensions of Public Health in Urban Environments (Marian
                        Levy, Andy Kitsinger, Debra Bartelli, KB Turner)
                      
                     
                      Privacy Data Impact on Retail Consumers and Suppliers (George Deitz, Mohammed Amini) 
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Research 
                         
                            
                               
                                Biologistics  
                                CAST  
                                Drones  
                                IIS  
                                MD2K  
                                Smart Cities  
                                Additive Manufacturing  
                            
                         
                      
                      
                      
                         
                            
                                Community Partners  
                               We work together to make our community better. 
                            
                            
                                Corporate Engagement  
                               Collaboration leads to innovation. 
                            
                            
                                Newsroom  
                               Catch up on what the Institute has been doing. 
                            
                            
                                Contact Us  
                               Questions? The FIT team can help. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


