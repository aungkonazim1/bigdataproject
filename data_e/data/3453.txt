Tennessee&#8217;s Next National Research University &#8211; The University of Memphis President&#039;s Blog   


 

 
 
 
 


 
 
 
 

 



 Tennessee s Next National Research University   The University of Memphis President s Blog 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 



 
 
  
 
 
 

 
 
 
 
			

         
 		
		



 
 
 
 

 

 


  Skip to content  




 

	
	 

		
		 

			
			 

				
						  The University of Memphis President s Blog  

					
			  

		  

		
		 

			 Primary Menu 
			 Menu 

			
			 
				 
					 
				 
					 Search for: 
					 
				 
				 
			 				 
			 

			 

			  
  Home    About the President  
  

		  

		 

		
		
			
				 

			
		
		 

		
	  

	
	 
	 

		
		 

			
			
	
	 

		
		
		 

			
			 Tennessee s Next National Research University 
			
			 

				   Author   Jeanine Hornish Rakow     Published on    October 22, 2015  November 9, 2015     Leave a comment  
			  

			
		  

		
		 

			 Dear Campus Community: 
 Over the last few weeks, I’ve shared several items with you including our THEC performance funding score, efforts to expand our national visibility, our goal to grow the University’s enrollment 10% in the coming five years, and the remarkable performance of MD2K, our NIH Center of Excellence.  As we move towards a new legislative session, I will be pursuing support for a dedicated effort to advance the University of Memphis to the status of a nationally ranked public research university. I think few would argue that a state the size of ours deserves two such state universities. As the only institution in the Tennessee Board of Regents system classified by the prestigious Carnegie Foundation as a Research University with High Activity (RU/HA) and the university with the largest Honors Program in the state, we are well positioned to achieve this goal. 
 After considerable thought and discussion it’s clear that to achieve our research aspirations we need to define our goal in clear terms. Becoming a nationally recognized research university is not a goal that can be reached without additional, targeted state support.  The impact of our research, scholarly and creative activity reaches well beyond the boundaries of Tennessee and stretches across the globe.  The depth and breadth of faculty expertise is impressive.  We have accomplished remarkable things with limited resources.  You’ll be hearing more about this effort in the coming months, and I hope you will support our efforts to attain this important goal. 
 Warm Regards, 
 M. David Rudd | President 

		  

		
		 

			  Published on    October 22, 2015  November 9, 2015      Author   Jeanine Hornish Rakow   
			
		  

		
	  

	
				
	 
		 Post navigation 
		    Previous article:  Performance Funding Score      Next article:  In Memory of U.S. Senator Fred Thompson    
	 
				

 

	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  


			
			
		  

		
	  


	
		
		
		 

		 Main Sidebar 

			
			  
				 
					 Search for: 
					 
				 
				 
			  		 		 Recent Posts 		 
					 
				 UofM Quality Indicator Score 
						 
					 
				 University of Memphis Research Foundation Announces Grand Opening of Student-Operated FedEx Call Center 
						 
					 
				 Pause to Grieve 
						 
					 
				 University of Memphis Magazine: Fall 2017 
						 
					 
				 Campus Update 
						 
				 
		 		  Recent Comments      Archives 		 
			  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  July 2014  
	  June 2014  
	  May 2014  
	  April 2014  
		 
		   Categories 		 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 
			
		  

		
		  

	
	
	 

		
		 Footer Content 

		 
		
			
				
				
								
			
		  
	
		 

			
			
			Using  Tiny Framework     
			
			   Log in  

		  
		
		 

			
			

 

	 Social Links Menu 

	      i class= fa fa-twitter   /i    
  
  

			
		  

		
	  

	
  


        

        









		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 
 