Viewing a course in Course Catalog as a student | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Viewing a course in Course Catalog as a student | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Integrations    »    Course Catalog    »    Managing participants in Course Catalog   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Viewing a course in Course Catalog as a student                       
        
        
       
          
     
           Printer-friendly version        
		In the Manage Catalog area, click   Courses .
	 
	 
		Click the   View as student  icon for the course you want to view.
	 
      Audience:    Instructor      

    
           

                   ‹ Unenrolling participants from courses  
        
                   up 
        
                   Viewing Course Catalog participants › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Course Catalog  
  
      Course Catalog basics    Managing courses and programs in Course Catalog     Managing participants in Course Catalog    Emailing participants    Unenrolling participants from courses     Viewing a course in Course Catalog as a student    Viewing Course Catalog participants    Manage enrollment requests      Managing course waitlists in Course Catalog    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
