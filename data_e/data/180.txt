Student Leadership &amp; Involvement - Student Leadership &amp; Involvement - University of Memphis    










 
 
 
     



 
    
    
    Student Leadership   Involvement - 
      	Student Leadership   Involvement
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Student Leadership   Involvement
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Student Engagement  
                          Leadership   Service  
                          Community Engagement  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                         
                           	Student Leadership   Involvement
                           	
                         
                      
                   
                   
                      
                         
                              
                             
                            
                             
                            
                             
                            
                             
                            
                            
                                
                            
                         
                         
                             
                         
                      
                      
                     		
                     
                     
                      Make the most of your college experience. 
                     
                      In Student Leadership   Involvement our mission is to engage students in promoting
                        positive social change through transformative learning, community engagement, and
                        leadership development.
                      
                     
                      We provide opportunities for students to engage in organizations and activities that
                        promote personal and professional development, community engagement, transformational
                        relationships, and positive social change that will enhance the student experience,
                        and prepare them to be competent professionals and alumni of the University of Memphis.
                      
                     
                      The Value of Engagement 
                     
                      
                        
                         
                           
                            You'll meet new people and network with students, staff, faculty, and community members. 
                           
                         
                        
                         
                           
                            You'll refine skills that will help you in your future career, such as working with
                              others, communication, time management, decision-making, and problem-solving.
                            
                           
                         
                        
                         
                           
                            You'll feel connected to your university experience and have fun. 
                           
                         
                        
                         
                           
                            You'll grow as an individual and future professional. 
                           
                         
                        
                      
                     
                      Join a student organization, participate in leadership training, perform community
                        service, attend on - campus programs there are so many opportunities to get involved
                        and engage in your co-curricular learning.
                      
                     
                      Opportunities to Engage 
                     
                      Student Leadership   Involvement is organized into three areas that support different
                        programs, organizations, and opportunities.
                      
                     
                      Student Engagement 
                     
                      
                        
                         
                           
                            Registered Student Organizations 
                           
                         
                        
                         
                           
                            Fraternity   Sorority Affairs 
                           
                         
                        
                         
                           
                            Student Activities Council 
                           
                         
                        
                         
                           
                            Tiger Zone 
                           
                         
                        
                         
                           
                            Student Event Allocation 
                           
                         
                        
                      
                     
                      Leadership   Service 
                     
                      
                        
                         
                           
                            Leadership Conferences   Workshops 
                           
                         
                        
                         
                           
                            Emerging Leaders Scholarship Program 
                           
                         
                        
                         
                           
                            Leader to Leader 
                           
                         
                        
                         
                           
                            Civic Engagement Board 
                           
                         
                        
                         
                           
                            Alternative Break Trips 
                           
                         
                        
                         
                           
                            Service on Saturday 
                           
                         
                        
                      
                     
                      Community Engagement   Campus Partnerships 
                     
                      
                        
                         
                           
                            Frosh Camp   Frosh Fusion 
                           
                         
                        
                         
                           
                            Sophomore Initiatives 
                           
                         
                        
                         
                           
                            Social Justice Programs 
                           
                         
                        
                         
                           
                            Tiger Spirit Initiatives  
                           
                         
                        
                         
                           
                            Professional Development Programs 
                           
                         
                        
                      
                     
                      Whether you are trying to get connected with an existing opportunity, or wanting to
                        start your own organization stop by  University Center 211 , give us a call at  901.678.8679 , or e-mail us at   studentinvolvement@memphis.edu  .
                      
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Our Mission  
                               Learn about the key elements we use to enhance your college experience. 
                            
                            
                                Fraternity   Sorority Affairs  
                               Enhance your college experience by joining a fraternity/sorority! 
                            
                            
                                Tiger Zone  
                               Learn about student organizations, events   involvement opportunities. 
                            
                            
                                Contact Us  
                               Questions? Our team is here to help! 
                            
                         
                      
                      
                      
                      
                         Follow SLI Online 
                         
                              Twitter   
                              Facebook   
                         
                       
 
    
         Get Involved  
       
          
             "LeaderShape  was a phenomenal experience that connected me to the campus and to other
               students  with common goals. For example, I have connected with students who share
               a commitment to helping children have access to quality education."
             
          
          Joel Jassu 
       
    
                    
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/27/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


