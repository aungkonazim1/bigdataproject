Doctoral Degree Candidacy Form Instructions - Graduate School - University of Memphis    










 
 
 
     



 
    
    
    Doctoral Degree Candidacy Form Instructions - 
      	Graduate School 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Graduate School
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Future Students  
                          Current Students  
                          Degrees  
                          News   Events  
                          Resources  
                          Contact Us  
                      
                      
                         
                            Resources   
                            
                               
                                  
                                   Graduate School Calendar  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Graduate School 
                              	  
                          
                              	Resources
                              	  
                         Doctoral Degree Candidacy Form Instructions 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Doctoral Degree Candidacy Form Instructions 
                     
                      NOTE: All doctoral students and those who may also be completing a graduate certificate
                        must " Apply to Graduate " (located in the MyMemphis portal) and must also submit candidacy forms.
                      
                     
                      FAILURE TO COMPLETE BOTH THE GRADUATION APPLICATION AND CANDIDACY FORM WILL AUTOMATICALLY
                        DELAY YOUR GRADUATION UNTIL THE NEXT SEMESTER.
                      
                     
                      Please read instructions carefully 
                     
                      
                        
                         When filing for graduation, doctoral students must have a cumulative GPA of 3.00 on
                           all graduate work undertaken at The University of Memphis whether or not the courses
                           are listed on the candidacy form.
                         
                        
                         Grades of D or F are not accepted for credit for graduation purposes, but these grades
                           are computed in the overall cumulative GPA.
                         
                        
                         No more than 15 post-baccalaureate hours of 6000-level courses may be applied to a
                           doctoral degree.
                         
                        
                         No more than seven (7) hours of C, C+, or C- grades will be counted toward degree
                           requirements.
                         
                        
                         If you were admitted as a Non-Degree student and eventually admitted to a doctoral
                           program, you must have taken at least 2/3 of the credit hours AFTER acceptance.
                         
                        
                         Do NOT include undergraduate level courses, courses completed for "Audit" credit,
                           or courses dropped or withdrawn on the candidacy form. List only graduate level courses
                           required for your degree program, including all electives -- No excess courses, please.
                         
                        
                         Course titles listed on the candidacy form may be abbreviated and should be no longer
                           than 100 characters long for both letters and space between words.
                         
                        
                         If you are currently enrolled in course work and/or dissertation credit, include these
                           courses on the candidacy form. Enter the maximum number of dissertation hours required
                           for your degree in the appropriate field, i.e., 3, 6, 9, 12, 15, 18,or 30. Do not
                           enter every semester of dissertation enrollment. One entry is sufficient.
                           
                            
                              
                               Doctoral Candidates must register for dissertation credit each academic semester (fall
                                 and spring) until the dissertation is completed. Students must also enroll in the
                                 summer semester but ONLY if the dissertation will be defended.
                               
                              
                               Failure to continuously enroll in dissertation credit will result in the student being
                                 charged back tuition in addition to applicable late fees charges for each semester
                                 he/she did not enroll.
                               
                              
                               If you do not complete your dissertation in your intended term of graduation, your
                                 graduation application in the MyMemphis portal will be automatically updated by the
                                 Graduate School.
                               
                              
                            
                           
                         
                        
                         If another course was substituted for a required course or for an elective, your major
                           advisor MUST complete an   Approved Course Substitution Form   and attach form to the candidacy form.
                         
                        
                         If you have transfer credit from another graduate institution that has not been posted
                           to your U of M record, complete and attach a   Evaluation of Transfer Credit Form   to the candidacy form.
                           
                            
                              
                               If transfer credit has already been posted to your U of M record, complete and attach
                                 an   Approved Transfer Credit form  .
                               
                              
                            
                           
                         
                        
                         If you have previously completed a Credit by Examination/Course Validation Examination
                           or are planning to complete either type of examination in your intended term of graduation,
                           indicate the type of credit on your candidacy form in parentheses AFTER you have listed
                           the course title, e.g., LDPS 7130, Microcomputer Application (CBE) or ICL 7001, Fundamentals
                           of Curriculum (CBV).
                         
                        
                         If you were accepted to a U of M post-baccalaureate doctoral program (e.g., Biology,
                           Economics, History, Mathematical Sciences, Philosophy, Psychology, etc.) with an earned
                           master's degree from another institution, include the total number of hours accepted
                           by your department on the last page of the candidacy form.
                         
                        
                         Upon completion, print the candidacy form. Signatures must be obtained from your major
                           professor, your departmental graduate studies coordinator, and your  College Director of Graduate Studies . 
                         
                        
                      
                     
                      Printing Instructions: 
                     
                      Please set your margin specifications in your internet browser to .50 for all margins.
                        Print completed form on front and back of page (duplex printing).
                      
                     
                      If you have questions regarding the completion of the candidacy form, contact the
                        Graduation Analyst,  Michelle Stout , in the Graduate School for further information.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Resources 
                         
                            
                               
                                Graduate School Calendar  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now!  
                               Take the first step toward your advanced degree. 
                            
                            
                                Degree Programs  
                               Explore our graduate catalog. 
                            
                            
                                Support Graduate Education  
                               Your gift makes a difference! 
                            
                            
                                Contact Us  
                               Questions? The Grad School Staff can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


