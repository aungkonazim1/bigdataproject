Fellowships &amp; Assistantships - Computer Science - University of Memphis    










 
 
 
     



 
    
    
    Fellowships   Assistantships - 
      	Computer Science
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Computer Science
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          News  
                          Future Students  
                          Current Students  
                          Research  
                          People  
                          Outreach  
                      
                      
                         
                            Financial Aid   
                            
                               
                                  
                                   Undergraduate Scholarships  
                                   Fellowships   Assistantships  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Computer Science
                              	  
                          
                              	Financial Aid
                              	  
                         Fellowships   Assistantships 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Fellowships   Assistantships 
                     
                      Additional information on graduate funding opportunities can be found at the University's
                          Financial Aid   page, or the Graduate School's   Assistantships   page.
                      
                     
                      
                     
                      Departmental Awards 
                     
                      
                        
                         The   Peter I. Neathery Fellowship   may be awarded competitively each year to up to four qualified graduate students.
                         
                        
                         The   Information Assurance Scholarship Program   offers full-ride funding to qualified students in return for serving the Department
                           of Defense after graduation. U.S. citizenship is required.
                         
                        
                          Graduate Assistantships  are offered by the Department to a limited number of graduate students. Graduate
                           assistants work a total of 20 hours per week to support departmental teaching, lab,
                           and other needs. The appointment provides a stipend and tuition   fee waiver for the
                           academic year, but it does not include summer support. To be considered for a graduate
                           assistantship, your graduate program application must be received by the end of February
                           for the Fall semester, or the end of November for Spring.
                         
                        
                          Research Assistantships  are available through research faculty and are supported by external grants. They
                           provide stipends and waivers comparable to those from Graduate Assistantships, but
                           duties depend on specific research projects. Please contact individual  faculty  for further information.
                         
                        
                         
                      
                     
                      University-Level Awards 
                     
                      
                        
                           Van Vleet Memorial Doctoral Award   
                        
                           Part-Time Master's Graduate Fellowship   
                        
                           Morton Thesis/Dissertation Awards   
                        
                           Graduate Assistant Meritorious Teaching Awards   
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Financial Aid 
                         
                            
                               
                                Undergraduate Scholarships  
                                Fellowships   Assistantships  
                            
                         
                      
                      
                      
                         
                            
                                Career Opportunities  
                               Internship information and job postings for students 
                            
                            
                                Degree Programs  
                               Information on our degree and certificate programs 
                            
                            
                                Courses  
                               Recent syllabi for our courses 
                            
                            
                                Contact Us  
                               Whom to contact about what 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/21/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


