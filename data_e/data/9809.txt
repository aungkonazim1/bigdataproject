Rename, update, or delete a collection | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Rename, update, or delete a collection | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   English  
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Integrations    »    Binder    »    Creating and managing collections and annotations in Binder Web   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Rename, update, or delete a collection                       
        
        
       
          
     
           Printer-friendly version       
	You can change the color scheme of a collection, rename it, or move documents to a different collection.
 

  
		On the My Binder screen, click the letter beside the collection name.
	 
	 
		Do any of the following actions:
		  
				To rename a collection, click  Modify . Enter the new name for the collection. Click  Modify .
			 
			 
				To change the color scheme of a collection, click  Modify . Select a new color for the collection. Click  Modify . 
			 
			 
				To delete a collection, click  Delete . Click  Delete .
			 
		  
      Audience:    Learner      
    
         
               ‹ Create a collection of documents 
                     up 
                     Change your account information in Binder Web › 
           
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Binder  
  
      Binder basics    Binder Web    Basics of Binder Web    Adding and navigating documents in Binder Web    Creating and managing collections and annotations in Binder Web    Mark up a document    Delete document mark up    Create a collection of documents    Rename, update, or delete a collection      Change your account information in Binder Web      Binder for iPad    Binder for Android    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
