Managing discussion subscriptions | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Managing discussion subscriptions | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Discussions    »    Following discussions   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Managing discussion subscriptions                       
        
        
       
        
     
              
	You can make changes to your subscriptions in  Notifications  and in the  Subscriptions  area of Discussions.
 

 
	Change default frequency of notifications and email address
 

  
		Do one of the following:
		  
				Click  Subscriptions  from the tool navigation in Discussions, then click  Change your notification settings .
 
			 Note  You must be subscribed to at least one forum, topic, or thread to view this option.
		 
			 
			 
				Click  Notifications  from the personal menu on the minibar.
			 
		  
	 
		Modify your  Notification Frequency  setting and email address.
	 
	 
		Click  Save .
	 
  
	Manage subscriptions for forums, topics, or threads
 

  
		Click  Subscriptions  from the tool navigation.
	 
	 
		Modify your  Default Notification Method  settings, or unsubscribe from any forum, topic, or thread you do not want to follow.
	 
  
	See also
 

  
		 Setting your notifications 
	 
      Audience:     Learner       

    
           

                   ‹ Marking discussion threads and posts as read or unread 
        
                   up 
        
                   Subscribing and unsubscribing to discussion forums, topics, and threads › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Discussions  
  
      Participating in discussions    Following discussions    Flagging a discussion post    Marking discussion threads and posts as read or unread    Managing discussion subscriptions    Subscribing and unsubscribing to discussion forums, topics, and threads      Creating and managing discussions    Monitoring discussions    
                  
           
         

       
     

    
    
    
   
 
   
 
