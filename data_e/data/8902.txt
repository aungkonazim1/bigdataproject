Creating formula grade items | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Creating formula grade items | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   English  
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Grades    »    Creating grade items and grade book categories   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Creating formula grade items                       
        
        
       
        
     
              
	Formula grade items automatically grade users using a custom formula based on achievements in other grade items.
 

 
	 Notes 

	  
			Formula grade items cannot belong to a category.
		 
		 
			You must create all of the grade items you want to include in the formula grade item before you create the formula item.
		 
		 
			Formula grade items cannot contribute to the calculated final grade unless you are using the Formula grading system.
		 
	  

 
	Create a formula grade item
 

  
		On the Manage Grades page, click  Item  from the New button.
	 
	 
		Select  Formula .
	 
	 
		Enter a  Name  for the grade item.
	 
	 
		You can enter a  Short Name  to display in the grade book.
	 
	 
		Enter a  Description  of the grade item. If you want to make the description available to users, select  Allow users to view grade item description .
	 
	 
		Enter the value you want the item graded out of in the  Max. Points  field.
	 
	 
		Click  Edit Using the Formula Editor  to define a calculation formula. See  Creating a formula in the Grades Formula Editor  for more information.
	 
	 
		If you want users to be able to receive a grade higher than the Max. Points specified, select  Can Exceed .
	 
	 
		Select a  Grade Scheme  to associate with the item.
	 
	 
		You can click  Add Rubric  to add a rubric, or click the  Create Rubric in New Window  link to create a new rubric.
	 
	 
		Select  Display Options .
	 
	 
		Click  Save and Close .
	 
  
	See also
 

  
		 Creating a formula in the Grades Formula Editor 
	 
      Audience:     Instructor       
    
         
               ‹ Creating pass/fail grade items 
                     up 
                     Creating calculated grade items › 
           
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Grades  
  
      Finding my grades    Creating grade book    Creating grade items and grade book categories    Understanding grade items    Creating grade book categories    Creating numeric grade items    Creating selectbox grade items    Creating pass/fail grade items    Creating formula grade items    Creating calculated grade items    Creating text grade items      Managing grade items and grade book categories    Managing grade schemes    Managing users  grades    Managing final grades    Changing Grades settings and display options    
                  
           
         

       
     

    
    
    
   
 
   
 
