Importing survey questions | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Importing survey questions | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Surveys    »    Managing survey questions and sections   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Importing survey questions                       
        
        
       
        
     
              
	You can import survey questions from numerous sources. Questions can come from an existing collection of questions (quizzes, surveys, self assessments, and the Question Library), a CSV file, or a learning object repository.
 

 
	Import survey questions from an existing collection
 

 
	You can import survey questions from existing collections of questions within Quizzes, Surveys, Self Assessments, and the Question Library.
 

  
		On the Manage Surveys page, click on the survey you want to import questions to.
	 
	 
		Click  Add/Edit Questions  in the Properties tab.
	 
	 
		Click  Import  . 
	 
	 
		On the Import page, choose  From an Existing Collection  in the Import Source drop-down list.
	 
	 
		In the Source Collection drop-down list, choose the source where existing questions reside.
	 
	 
		 In the Source Section drop-down list, choose the   Collection Root  or a specific section from which existing questions reside.
	 
	 
		Select the check boxes beside the questions you want to import, and click  Save .
	 
  
	Import survey questions from a CSV file
 

 
	You can create survey questions offline using a CSV formatted text file. Follow the steps below to access a template for creating the text file, or upload a question file you have completed.
 

  
		On the Manage Surveys page, click on the survey you want to import questions to.
	 
	 
		Click  Add/Edit Questions  in the Properties tab.
	 
	 
		Click  Import  . 
	 
	 
		On the Import page, choose  From a Desire2Learn Text Format File  in the Import Source drop-down list.
	 
	 
		You can click the  CSV Sample File  link and  Save  the file to your computer to download the text file template.
	 
	 
		Click  Browse  and select a CSV question file you have completed and want to upload to the survey. Click  Open .
	 
	 
		Click  Save .
	 
  
	Import survey questions from a learning object repository
 

  
		On the Manage Surveys page, click on the survey you want to import questions to.
	 
	 
		Click  Add/Edit Questions  in the Properties tab.
	 
	 
		Click  Import  . 
	 
	 
		On the import page, choose  Learning Repository (LOR)  in the Import Source drop-down list.
	 
	 
		Click  Add Learning Object  and browse the repository for the CSV file you want to import.
	 
	 
		When you find the file you want to import, select the option button next to the object and click  Select .
	 
	 
		Click  Save . If you want to replace or remove the object you chose, click the    Delete  icon and repeat from Step 4.
	 
      Audience:     Instructor       

    
           

                   ‹ Deleting survey questions and sections 
        
                   up 
        
                   Publishing questions and sections in Surveys to a learning object repository › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Surveys  
  
      Participating in Surveys    Using Surveys    Managing survey questions and sections    Understanding  Question in Use     Editing survey questions and sections    Previewing survey questions    Reordering survey questions and sections    Deleting survey questions and sections    Importing survey questions    Publishing questions and sections in Surveys to a learning object repository      Viewing surveys    
                  
           
         

       
     

    
    
    
   
 
   
 
