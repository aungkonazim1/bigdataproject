Course Descriptions - Kemmons Wilson School of Hospitality &amp; Resort Management - University of Memphis    










 
 
 
     



 
    
    
    Course Descriptions - 
      	Kemmons Wilson School of Hospitality   Resort Management
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Kemmons Wilson School of Hospitality   Resort Management 
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Hospitality  
                          Sport   Leisure  
                          Students  
                          Careers  
                          Faculty  
                          Contact Us  
                      
                      
                         
                            Programs   
                            
                               
                                  
                                   Hospitality and Resort Management  
                                         Bachelor of Arts in Hospitality and Resort Management  
                                         Minor in Hospitality and Resort Management  
                                         Master of Arts in Liberal Studies (MALS)  
                                         Graduate Certificate in Liberal Studies  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Kemmons Wilson School of Hospitality   Resort Management
                              	  
                          
                              	Programs
                              	  
                         Course Descriptions 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                       Bachelor of Arts In Hospitality and Resort Management   
                     
                      In addition to the courses below, the department may offer the following Special Topics
                        courses:
                      
                     
                        HPRM 4950-59. Special Topics in Hospitality and Resort Management. (3) . Topics are varied and in online class listings. May be repeated with a change in
                        topic. PREREQUISITE: permission of instructor.
                      
                     
                        HPRM 1050 - Business of Hospitality (3)  (2010). Varied aspects of lodging, food service, clubs, cruise lines, natural attractions,
                        man-made attraction, transportation, infrastructure; retail businesses; sports; special
                        events and activities; outfitters, tour operators, travel management; destination
                        marketing organizations.
                      
                     
                       HPRM 2003 - Special Events (1)  Introduction to special events management and potential careers in this field. Hands
                        on experience with special events while partnering with destination Management Company
                        to work with planning and operations in multifaceted special events area.
                      
                     
                       HPRM 2004 - Distinguished Speakers (1)  Series of lectures from high ranking professionals from the hospitality industry in
                        areas of lodging, food service and tourism.
                      
                     
                       HPRM 2006 - Professional Development (1)  Conducting a self-assessment; development of a resume and self promotion materials;
                        presentation skills; social skills needed to obtain and retain career position; corporate
                        communication skills.
                      
                     
                       HPRM 2007 - Resort Management (1)  Currents events and trends in the resort and tourism industry; impact of external
                        publics on resort industry; identifying resort management decision-making tools.
                      
                     
                       HPRM 2011 - Hosp Ind Managerial Acct (3)  Presentation, interpretation, and analysis of internal and external hospitality financial
                        reports affecting management decisions using Uniform Systems of Accounting. PREREQUISITE:
                        HPRM 1050.
                      
                     
                       HPRM 2050 - Food Production and Service (4)  Theory, application and understanding of safe food production methods and terminology;
                        culinary hand tools and equipment operation including knife usage; principles and
                        applied fundamentals of property service techniques and beverage handling. PREREQUISITE:
                        HPRM 1050.
                      
                     
                       HPRM 2330 - Managing Hotel/Resort Ops (3)  (3330). Management of critical resources for running hotel and resort operation in
                        effective and efficient manner; customer services, service quality, and service delivery.
                      
                     
                       HPRM 2999 - Purchase/Hospitality Industry (1)  Purchasing in commercial restaurants, hotels, food service, contract food service,
                        institutional food service, catering, quick service.
                      
                     
                       HPRM 3010 - Hospitality/Resort Colloquium (1)  Introduction to hospitality and resort industry; presentations and discussions by
                        major industry professionals; students submission of a synopsis of each speaker's
                        major points. PREREQUISITE: junior-level standing.
                      
                     
                       HPRM 3050 - Food/Beverage Controls (3)  Fundamentals of food, beverage and labor control through menu planning, engineering
                        and cost analysis. PREREQUISITE: HPRM 1050, 2050, ACCT 2010.
                      
                     
                       HPRM 3130 - Hospitality Law, Ethics   Risk (3)  Laws and regulations applicable to the ownership and operation of inns, hotels, motels,
                        resorts, restaurants, bars, and other hospitality businesses; laws and regulations
                        that influence business and management decisions in the hospitality industry; ethical
                        issues involved in managing hospitality operations.
                      
                     
                       HPRM 3315 - Hospitality Human Resource Mgmt (3)  Acquisition, utilization, and retention of human resources in hospitality industry;
                        human resource planning, job analysis, recruitment, selection, orientation, training,
                        development, motivation, associate relations, performance appraisal, legal issues.
                        PREREQUISITE: MGMT 3110.
                      
                     
                       HPRM 3320 - Hospitality Industry/Rev Mgmt (3)  Fundamental principles and concepts of revenue management including capacity management,
                        duration control, revenue forecasting, discounting, displacement analysis, rate management,
                        and dynamic pricing. PREREQUISITE: HPRM 2330, ACCT 2010.
                      
                     
                       HPRM 3340 - Principles of Social Media Commerce (3)  How social media impacts marketing strategy, brand development, return on investment,
                        and other fundamental business concepts; using social media to effectively convey
                        messages and engage constituents.
                      
                     
                       HPRM 3600 - Entertainment Management (3)  Fundamental standards, techniques, practices of club, cruise, gaming, theme park,
                        special events management. PREREQUISITE: HPRM 1050.
                      
                     
                       HPRM 3911 - Pre-Internship in HPRM (1)  Focus on the requirements and process for applying and successfully completing an
                        internship program at Kemmons Wilson School of Hospitality and Resort Management.
                        Students will also learn about professionalism and resume writing skills. Prerequisite
                        for HPRM 1050.
                      
                     
                       HPRM 4007 - Resort and Timeshare Mgmt (3)  Overview of dynamic resort and distinctive lodging industry; differences in core lodging
                        competencies between resorts and traditional hotels, such as accounting and human
                        resources.
                      
                     
                       HPRM 4320 - Hospitality Services Marketing (3)  Principles and models of services marketing with focus on applications to hospitality
                        services industry; expansion of traditional marketing mix variables into additional
                        development of hospitality service concepts, marketing plans, and service quality
                        assessments. PREREQUISITE: MKTG 3010.
                      
                     
                       HPRM 4322 - Advanced Food/Beverage Management (3)  Foundations of managing restaurants and associated beverage operations; planning and
                        control procedures, human resources management, customer service, marketing strategies,
                        and emerging technologies; relationship between food and beverage operations. PREREQUISITE:
                        MGMT 3110, MKTG 3010, or permission of department chair.
                      
                     
                       HPRM 4331 - Advanced Resort/Lodging Management (3)  Issues, theories, and best practices of resort and lodging industry. PREREQUISITE:
                        HPRM 2330, MGMT 3110.
                      
                     
                       HPRM 4340 - Information Technology HPRM (3)  Framework for information technology, systems development methodologies, and strategic
                        information systems planning; current issues relating to the hospitality industry;
                        focus on using information technology in networked environment to achieve organizational
                        goals and objectives.
                      
                     
                       HPRM 4350 - Properties Development/Planning (3)  Problems and opportunities inherent in developing and planning resort and hospitality
                        facilities; sequence of property development, conceptual and space planning, design
                        criteria, and construction management; establishing appropriate facilities requirements,
                        understanding industry practices, and implementing properties decisions with integrated
                        design, operations, financial and real estate framework.
                      
                     
                       HPRM 4400 - International Hospitality (3)  Issues, challenges and practices of global hospitality and tourism marketplace.
                      
                     
                       HPRM 4401 - Management of Tourism (3)  Tourism as a system; interaction of various parts of tourism; managers influence in
                        tourism's success.
                      
                     
                       HPRM 4620 - Hospitality Operational Analysis (3)  Management tools in analyzing operational effectiveness of hotels and resorts. PREREQUISITE:
                        HPRM 2330.
                      
                     
                       HPRM 4630 - Social Communications Strategies/Analytic (3)  Integrate social media techniques and trends into business strategy; recognize advanced
                        principles of social value chain and enterprise systems; formulate action plans to
                        combine key social media networks and tools.
                      
                     
                       HPRM 4700 - Integrative Challenge (3)  Capstone course integrating academic and experiential learning through service learning
                        projects building on leadership skills. PREREQUISITE: HPRM 4315, 4320.
                      
                     
                       HPRM 4910 – Recreation Food Service (3)  Recreational foodservice is fast-growing industry and refers to food planning for
                        special events such as: one-time events, repeated events that are not on a fixed schedule
                        (i.e. concerts), weekly events such as football-baseball-or basketball games, or other
                        similar venues (movie theatres, concert halls, entertainment facilities, etc.) Concessions
                        are a large part of these fan based settings. With the solid revenue figures involved
                        as well as the number of people anticipated for such events, planning, training of
                        staff, purchasing and supply, money and banking, facility access, and equipment, are
                        some of the few of the elements that are crucial. This course is designed to cover
                        such topics including contract negotiations.
                      
                     
                       HPRM 4910 – Food Safety   Sanitation (3)  This course introduces students to the basic principles of sanitation, hygiene and
                        safety as it relates to the hospitality and tourism industry. Emphasis is placed upon
                        training of supervisory personnel in sanitation procedures. Course meets standards
                        for ServSafe certificate. Students must pass the certification examination as a part
                        of this course.
                      
                     
                       HPRM 4911 - Hospitality Mgmt Internship (3)  Work-based learning course that enables students to develop practical skills, relate
                        theory to practice and to gain a sound base of industrial experience by working, on
                        a paid or voluntary basis, for an organization within the hospitality and tourism
                        industry; develops practical competencies to assist in progressing toward a career
                        in the hospitality industry. Students are expected to submit weekly reports and a
                        final report as a requirement for this course. PREREQUISITE: HPRM 3911. Junior standing,
                        and a minimum 2.7 GPA.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Programs 
                         
                            
                               
                                Hospitality and Resort Management  
                                      Bachelor of Arts in Hospitality and Resort Management  
                                      Minor in Hospitality and Resort Management  
                                      Master of Arts in Liberal Studies (MALS)  
                                      Graduate Certificate in Liberal Studies  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply to the KWS Program  
                               Choose the degree program that's right for you 
                            
                            
                                Academic Advising  
                               Schedule an appointment with your advisor 
                            
                            
                                Research Highlights  
                               KWS faculty are leaders in their fields of study 
                            
                            
                                Contact Us  
                               Questions? Our team can help. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/21/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


