Letter of Recommendation - Registrar - University of Memphis    










 
 
 
     



 
    
    
    Letter of Recommendation  - 
      	Registrar
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Registrar
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Register  
                          Students  
                          Veterans  
                          Faculty   Staff  
                          Forms  
                      
                      
                         
                            Faculty   Staff   
                            
                               
                                  
                                   Faculty   Staff  
                                         Authorized Signatures  
                                         FERPA Compliance  
                                         Scheduling   Term Planning  
                                         Self Service   Online Aids  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Registrar
                              	  
                          
                              	Faculty   Staff
                              	  
                         Letter of Recommendation  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Letter of Recommendation 
                     
                      Because a letter of recommendation may include information from the student's academic
                        record, and is itself considered part of the student's academic record, writing a
                        letter of recommendation is not as straightforward as you might think. Key considerations
                        are discussed below, but first review these terms.
                      
                     
                      Terms 
                     
                      
                        
                          Personally identifiable information from the student's education record   This is non-directory information such as student courses, course schedules, grades,
                           GPA information, etc.
                         
                        
                          Personal knowledge of the student   Your observations on such matters as the student's work ethic, punctuality, reliability,
                           creativity, cooperation, etc. This excludes personally identifiable information from
                           the student's education record.
                         
                        
                          Release   The student's permission for you to include personally identifiable education record
                           information in the letter.
                         
                        
                          Waiver   The student's consent to waive his/her right to ever view your letter.
                         
                        
                           Request for Letter of Recommendation   For your convenience, a form for requesting a letter of recommendation has been developed. It
                           contains pertinent student and recipient information sections as well as release and
                           waiver sections. Further discussion references this form.
                         
                        
                      
                     
                      Considerations 
                     
                      Ask the student needing a letter to complete the   Request for Letter of Recommendation  . In guiding the student through the form, consider the following:
                      
                     
                      Confidentiality Flag 
                     
                      Does the student have a confidentiality flag set in the Banner system? If  Yes , the student must give you permission to write a letter ( by completing the form),
                        whether it will contain personally identifiable information from the student's education
                        record or not.
                      
                     
                      Release of the Student's Education Record Information 
                     
                      Is the student willing to have student education record to be included in the letter?
                        Have the student indicate  Yes  or  No  in the Release portion of the form.
                      
                     
                      If  Yes , he/she should specify what information can be included; your letter may then include
                        the specified information as well as your personal knowledge of the student.
                      
                     
                      If  No , your letter may include only your personal knowledge of the student.
                      
                     
                      Waiver of Student Right to Review Letter 
                     
                      Since the letter of recommendation becomes part of the student's education record, which
                        under FERPA the student has a right to view, he/she would be able to view the letter
                        in the future.
                      
                     
                      If you don't mind the student's seeing the letter, have the student indicate, "NO.
                        I do NOT waive my right to review a copy of the letter at any time in the future."
                      
                     
                      If you do mind, ask the student to waive his/her right to review by indicating, "YES.
                        I waive my right to review."
                      
                     
                        NOTE :   A student’s waiving his/her right to review letters of recommendation on  our  campus does not apply to a receiving institution. If the student does not waive his/her
                        right to review letters of recommendation at the receiving institution, the letter
                        may be viewed by the student as part of the education record there.
                      
                     
                      Processing the Form 
                     
                      Have the student sign and date the form; then make a copy for your records. When you
                        have written the letter, send a copy of it and the student release to the Registrar's
                        Office to be imaged as part of the student's education record.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Faculty   Staff 
                         
                            
                               
                                Faculty   Staff  
                                      Authorized Signatures  
                                      FERPA Compliance  
                                      Scheduling   Term Planning  
                                      Self Service   Online Aids  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Calendars  
                               View Academic Year calendars; semester dates/deadlines; final exam schedules. 
                            
                            
                                Transcripts  
                               Request copies of your official transcript. 
                            
                            
                                Notice!  
                                Check for alerts and reminders. 
                            
                            
                                Contact Us   
                               Need additional assistance or information? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


