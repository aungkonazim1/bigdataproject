Externship testimonials - School of Law - University of Memphis    










 
 
 
     



 
    
    
    Externship testimonials  - 
      	School of Law 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Cecil C. Humphreys School of Law
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Admissions  
                          Programs  
                          Current Students  
                          Faculty  
                          Careers  
                          Library  
                      
                      
                         
                            Programs   
                            
                               
                                  
                                   Degree Programs  
                                         JD  
                                         JD/MBA  
                                         JD/MA in Pol. Science  
                                         JD/MPH  
                                         MPH Fast-Track  
                                         Part-time Program  
                                     
                                  
                                   Certificate Programs  
                                         Advocacy  
                                         Business Law  
                                         Health Law  
                                         Tax Law  
                                     
                                  
                                   Experiential Learning  
                                         Experiential Learning Home  
                                         Legal Clinics  
                                         Externship Program  
                                     
                                  
                                   Law Review  
                                         Law Review Home  
                                         Editorial Board  
                                         Editor-in-Chief  
                                         Joining Law Review  
                                         Virtual Tour of Law Review Suite  
                                         Symposium  
                                         Subscriptions  
                                         Archives  
                                         Current Volume  
                                         Connect with Us  
                                     
                                  
                                   Advocacy Program  
                                         Moot Court Board  
                                         Joining Moot Court Board  
                                         In-School Competitions  
                                         Travel Team Competitions  
                                         Past Champions  
                                     
                                  
                                   Inst. for Health Law   Policy  
                                         Institute Home   
                                         Advisory Committee  
                                         Symposium  
                                         Pro Bono Opportunities  
                                         Health Law Certificate  
                                         Health Law Society  
                                         MBA Health Law Section  
                                     
                                  
                                   International Law Programs  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	School of Law 
                              	  
                          
                              	Programs
                              	  
                         Externship testimonials  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      EXTERNSHIP STUDENT TESTIMONIALS 
                     
                       Zach Hoyt - Class of 2013  
                     
                      Spring 2012 Externship Program – Field Placement EEOC Hearings Unit. Summer 2012 Externship
                        Program – Field Placement with U.S. Attorney's Office (W.D. TN)  "At the time I started my first externship, my idealistic view of the law that I had
                        coming into school was on life support after three semesters of doctrinal classes
                        focused on the technical interpretation and operation of the law. Getting out and
                        working with real people and real problems in my externship reminded why I went to
                        law school in the first place. Seeing how the law worked in practice also made the
                        occasionally dry doctrinal subject much more interesting. Getting feedback from mentors
                        was an invaluable aid in developing practical legal skills and gave me a chance to
                        form relationships with future colleagues. Finally, the externship program helped
                        me figure out what direction I wanted to focus my career on by exposing me to different
                        areas of practice. I strongly encourage every student to take advantage of the externship
                        program."
                      
                     
                      
                     
                       Jenna Dillier - Class of 2013  
                     
                      Summer 2012 Externship Program - Field Placement with U.S. Bankruptcy Court Judge
                        David S. Kennedy
                      
                     
                      "Participating in the Externship Program was a wonderful experience for me. I learned
                        more than I could have ever hoped for. Not only was I able to see real legal issues
                        in an authentic setting, but I gained a deeper understanding of the judicial decision-making
                        process as well. It was a great feeling to be able to put my classroom knowledge to
                        use outside the classroom. I now feel more comfortable about what to expect inside
                        of a courtroom and believe this experience has bettered prepared me to become a practicing
                        attorney.
                      
                     
                      Externing in the United States Bankruptcy Court was an enlightening experience which
                        allowed me to learn more about the field of bankruptcy law. Without this experience
                        I would not have been able to discover my interest in this area of the law. Judge
                        Kennedy is an amazing judge, not to mention, a remarkable individual. Judge Kennedy
                        freely shares his wisdom and offered a wonderful new perspective for me about the
                        judicial decision-making process. I truly appreciated the time my supervisor devoted
                        to teaching and mentoring. I highly recommend a judicial externship for students who
                        want to learn more about the judicial decision-making process."
                      
                     
                      
                     
                       Deanna Windham  
                     
                      Administrative Law Judge, Equal Employment Opportunity Commision, Memphis District
                        Office, Federal Sector Hearings Unit. Externship Field Placement Supervisor, Summer
                        2011 - Present
                      
                     
                      "Through this program, students provide valuable assistance to the administrative
                        judges in performing their day-to-day tasks and duties, and in the process, help further
                        the overall mission of the EEOC to stop and remedy unlawful workplace discrimination.
                        This benefits not only the students and the EEOC but also provides a public service
                        to the federal workforce in our district."
                      
                     
                      
                     
                      Current and former experiential learning students are encouraged to submit testimonials
                        to Professor Daniel Schaffzin (dschffzn@memphis.edu).
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Programs 
                         
                            
                               
                                Degree Programs  
                                      JD  
                                      JD/MBA  
                                      JD/MA in Pol. Science  
                                      JD/MPH  
                                      MPH Fast-Track  
                                      Part-time Program  
                                  
                               
                                Certificate Programs  
                                      Advocacy  
                                      Business Law  
                                      Health Law  
                                      Tax Law  
                                  
                               
                                Experiential Learning  
                                      Experiential Learning Home  
                                      Legal Clinics  
                                      Externship Program  
                                  
                               
                                Law Review  
                                      Law Review Home  
                                      Editorial Board  
                                      Editor-in-Chief  
                                      Joining Law Review  
                                      Virtual Tour of Law Review Suite  
                                      Symposium  
                                      Subscriptions  
                                      Archives  
                                      Current Volume  
                                      Connect with Us  
                                  
                               
                                Advocacy Program  
                                      Moot Court Board  
                                      Joining Moot Court Board  
                                      In-School Competitions  
                                      Travel Team Competitions  
                                      Past Champions  
                                  
                               
                                Inst. for Health Law   Policy  
                                      Institute Home   
                                      Advisory Committee  
                                      Symposium  
                                      Pro Bono Opportunities  
                                      Health Law Certificate  
                                      Health Law Society  
                                      MBA Health Law Section  
                                  
                               
                                International Law Programs  
                            
                         
                      
                      
                      
                         
                            
                                Apply to Memphis Law  
                                
                            
                            
                                News   Events  
                                
                            
                            
                                Alumni   Support  
                                
                            
                            
                                ABA Required Disclosures  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


