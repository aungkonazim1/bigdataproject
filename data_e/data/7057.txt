Accessing Capture Portal | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Accessing Capture Portal | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Capture    »    Participating in CaptureCast presentations   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Accessing Capture Portal                       
        
        
       
          
     
           Printer-friendly version       
	
 

  
		Navigate to your Capture Portal URL.
	 
	 
		Click  Log In .
	 
	 
		Enter your Capture Portal  Username  and  Password . If you are redirected to your institution's login page, enter your general username and password.
	 
	 
		Click  Log In .
		 
			 Note If you do not have an account, click  Join  and fill in the registration form to register for one.
		 
	 
      Audience:    Learner      

    
           

                   ‹ Participating in CaptureCast presentations 
        
                   up 
        
                   Viewing a CaptureCast presentation › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Capture  
  
      Participating in CaptureCast presentations    Accessing Capture Portal    Viewing a CaptureCast presentation    Viewing CaptureCast presentations offline and in mobile playback    Participating in a live event chat    Adding comments and replies to a CaptureCast presentation      
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
