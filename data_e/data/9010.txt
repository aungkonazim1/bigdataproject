Working with Learning Repository search results | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Working with Learning Repository search results | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Repository    »    Searching for and using learning resources   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Working with Learning Repository search results                       
        
        
       
        
     
              
	Change display settings for search results
 

  
		Click     Settings  on the LOR Results page.
	 
	 
		Edit display options.
	 
	 
		Click  Save .
	 
  
	Work with LOR search results
 

 
	From the LOR Results page, you can (depending on permissions):
 

  
		View the learning object or file.
	 
	 
		View the learning object's file type (if any).
	 
	 
		View the learning object’s details, including:
		  
				Overview of the modules and topics that make up the learning object (its 
				sub-objects)
			 
			 
				Event logs detailing the activity for the learning object or file
			 
			 
				Reviews of the learning object or file
			 
			 
				The files that the object is composed of
			 
			 
				A thumbnail display of the learning object if it consists of image files
			 
			 
				View versions of learning objects, from oldest to most recently updated
			 
		  
	 
		View or edit metadata for the learning object or file and its sub-objects
	 
	 
		Retrieve the learning object in the following ways:
		  
				Create a dynamic or locked link to the object (for objects from MERLOT, you can only create locked links)
			 
			 
				Copy the object into the Content tool, keeping the navigational structure of the object intact and creating a copy of all associated files in your course files
			 
			 
				Copy the files of the learning object or file to your course files folder, but without the object’s navigational structure
			 
			 
				Download the learning object or file to your desktop as a content package
			 
		  
	 
		Add the learning object to your collection
	 
	 
		Manage the learning object’s properties:
		  
				Repository the learning object currently resides in
			 
			 
				Status as Unassigned, Draft, In Review, Approved, or Archived
			 
			 
				Visibility
			 
			 
				External availability
			 
			 
				Add or edits rights information on single learning objects
			 
		  
	 
		Delete the learning objects or files and all previous versions.
	 
	 
		Add or delete classifications.
	 
	 
		Add a review to the learning object
	 
  
	Viewing versions in search results
 

 
	Learning Repository enables users to view, manage, and use older versions of learning objects in local repositories. You can access versions of any learning objects that can have a locked link.
 

 
	Search results display the available learning object versions in the Course Builder tool and Insert Stuff. You can create a locked link to older versions of learning objects or the newest version by clicking  change.  
 

 
	 Note  Versions are not available for objects in federated or harvested repositories.
 

 
	Filtering search results
 

 
	When Learning Repository returns your results, you can filter them, except for results from federated repositories (i.e., MERLOT).
 

 
	Filter your search results
 

 
	Click the links in the Search Filters panel. To remove a filter, click  Clear  beside the filter you want to remove.
 
     Audience:     Instructor       

    
           

                   ‹ Browsing Learning Repository s content 
        
                   up 
        
                   Viewing files in a learning object › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Learning Repository  
  
      Main Learning Repository concepts    Searching for and using learning resources    Searching Learning Repository    Browsing Learning Repository s content    Working with Learning Repository search results    Viewing files in a learning object    Adding reviews to a learning object or file    Viewing reviews of a learning object or file    Retrieving learning objects with navigation    Retrieving learning objects without navigation    Retrieving a learning object as a new topic    Importing a learning object into a quiz or a question library    Searching for and retrieving a collection    Saving a learning object to your PC    Using RSS feeds in Learning Repository      Publishing to Learning Repository    Managing learning objects    
                  
           
         

       
     

    
    
    
   
 
   
 
