Michael Monahan - Philosophy - University of Memphis    










 
 
 
     



 
    
    
    Michael Monahan - 
      	Philosophy
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Philosophy
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Undergraduate  
                          Graduate  
                          People  
                          Research  
                          News   Events  
                          Resources  
                          Contact  
                      
                      
                         
                            People   
                            
                               
                                  
                                   Core Faculty  
                                   Visiting Scholars  
                                   Instructors  
                                   Emeritus Faculty  
                                   Graduate Students  
                                   Staff  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Philosophy
                              	  
                          
                              	People
                              	  
                         Michael Monahan 
                      
                   
                   
                       
                      
                      
                        
                        	
                        
                        		  
                         
                           
                           
                            
                              
                              
                                                                   
                                 
                                 
                                  
                                 
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                    Michael Monahan
                                    
                                  
                                 
                                 
                                  
                                    
                                    Associate Professor
                                    
                                  
                                 
                                 
                                  
                                    
                                     
                                       
                                        Phone 
                                       
                                        
                                          
                                          901.678.2535
                                          
                                        
                                       
                                     
                                    
                                     
                                       
                                        Email 
                                       
                                         mjmnahan@memphis.edu  
                                       
                                     
                                    
                                     
                                       
                                        Fax 
                                       
                                        
                                          
                                          901.678.4365
                                          
                                        
                                       
                                     
                                    
                                  
                                 
                                 
                                  
                                    
                                     
                                       
                                        Office 
                                       
                                        
                                          
                                          325 Clement Hall
                                          
                                        
                                       
                                     
                                    
                                     
                                       
                                        Office Hours 
                                       
                                        
                                          
                                          TR 9:30-11:00am
                                          
                                        
                                       
                                     
                                    
                                  
                                 
                                 
                                  
                                    										
                                    
                                    
                                     Website 
                                    
                                      CV  
                                                                            
                                    
                                  
                                 
                               
                              
                              
                            
                           			 
                           			  
                           
                           
                            About Professor Monahan 
                           
                            Michael Monahan (Ph.D., University of Illinois) joined the department in 2017. Prior
                              to coming to Memphis, he taught at Marquette University (2003-2017). He is a founding
                              member of the Phenomenology Roundtable, and is past Vice-President (2009-2013) and
                              current Treasurer of the Caribbean Philosophical Association. His primary philosophical
                              interests are in questions of oppression and liberation, with a particular emphasis
                              on race and racism. He draws primarily on Africana and phenomenological texts and
                              traditions in his work. He has taught courses in Africana Philosophy, Philosophy of
                              Race, Political Philosophy, Ethics, Feminist Philosophy, Hegel, and Nietzsche. His
                              current work investigates the uses and abuses of theories of "recognition" in the
                              context of racial oppression and liberation. He also practices and teaches martial
                              arts.
                            
                           
                            Selected Publications 
                           
                              The Creolizing Subject: Race, Reason, and the Politics of Purity  , Fordham University Press, 2011
                            
                           
                              Creolizing Hegel  , Rowman and Littlefield International, 2017
                            
                           
                            "The Concept of Privilege: A Critical Appraisal", in  The South African Journal of Philosophy , 33:1 (2014)
                            
                           
                            "Emancipatory Affect: bell hooks on Love and Liberation", in  The CLR James Journal , Volume 17:1 (2011)
                            
                           
                            "Sartre's Critique and the Inevitability of Violence: Human Freedom in the Milieu
                              of Scarcity",  Sartre Studies Internationa l, Volume 14, Issue 2 (2008)
                            
                           
                            "The Practice of Self Overcoming: Nietzschean Reflections on the Martial Arts",  The Journal of the Philosophy of Sport , Volume 34 (2007)
                            
                           
                            "Recognition Beyond Struggle: On a Liberatory Account of Hegelian Recognition", Social
                              Theory and Practice, Volume 32:3 (2006)
                            
                           
                            Recent and Upcoming Presentations 
                           
                            "On 'Creolizing' Theory: Reflections on and Challenges for the South African Context,"
                              presented as part of the annual uBuntu Project meeting at the University of Venda,
                              Thohoyandou, South Africa, July 2016.
                            
                           
                            "Reason, Race, and the Human Project," presented as a colloquium at the University
                              of Minnesota Duluth, February 2017.
                            
                           
                            "Revitalizing Hegelian Recognition: Identity Politics and Solidarity," invited presentation
                              at the Race, Recognition, and Respect conference at Johns Hopkins University, April
                              2017.
                            
                           
                            Participated in an author meets critics panel on my book, Creolizing Hegel at the
                              Caribbean Philosophical Association's annual meeting, New York, June 2017.
                            
                           
                            "Where Do I Belong? On Maronnage,'Hometactics,' and Liberation," to be presented at
                              the California Roundtable on Philosophy and Race, Emory University, October 2017.
                            
                           
                           
                         
                        								
                        
                        
                        
                        
                        	
                      
                      
                   
                
                
                   
                      
                         People 
                         
                            
                               
                                Core Faculty  
                                Visiting Scholars  
                                Instructors  
                                Emeritus Faculty  
                                Graduate Students  
                                Staff  
                            
                         
                      
                      
                      
                         
                            
                                Apply to the Graduate Program  
                               Want to pursue a MA or PhD in Philosophy? Find out how to apply. 
                            
                            
                                Online B.A.  
                               Want to pursue your college degree and manage your busy lifestyle? Experience online
                                 classes at the UofM.
                               
                            
                            
                                Pre-law Advising  
                               Are you interested in a career in the legal profession? Consider a degree in Philosophy! 
                            
                            
                                Support the Department  
                               Find out how you can support the Department of Philosophy. 
                            
                         
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


