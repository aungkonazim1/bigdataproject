Adding reflections | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Adding reflections | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    ePortfolio    »    Using reflections   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Adding reflections                       
        
        
       
        
     
              
	Use reflections to discuss items in your ePortfolio, record your thoughts on topics that interest you, set goals, and think critically about your learning.
 

 
	Add a reflection
 

  
		On the My Items page, click  ​  Reflection  from the Add button.
	 
	 
		Give your reflection a  Title .
	 
	 
		Enter your thoughts in the  Reflection  field.
	 
	 
		Add any tags you want the reflection to have.
	 
	 
		Select  Allow others to add/view comments if they have sufficient permission  if you want to provide others the option to comment on your reflection.
	 
	 
		Click  Save .
	 
      Audience:     Learner       

    
           

                   ‹ Using reflections 
        
                   up 
        
                   Associating reflections with items › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        ePortfolio  
  
      Understanding the main pages of ePortfolio    Adding artifacts    Using reflections    Adding reflections    Associating reflections with items      Creating presentations    Creating collections    Understanding assessment types in ePortfolio    Understanding the basic concepts in sharing    Importing and exporting items    Sharing items within courses    Using form templates    Integrating ePortfolio with Content     Assessing ePortfolio content    
                  
           
         

       
     

    
    
    
   
 
   
 
