Update on Review of Library Report &#8211; The University of Memphis President&#039;s Blog   


 

 
 
 
 


 
 
 
 

 



 Update on Review of Library Report   The University of Memphis President s Blog 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 



 
 
  
 
 
 

 
 
 
 
			

         
 		
		



 
 
 
 

 

 


  Skip to content  




 

	
	 

		
		 

			
			 

				
						  The University of Memphis President s Blog  

					
			  

		  

		
		 

			 Primary Menu 
			 Menu 

			
			 
				 
					 
				 
					 Search for: 
					 
				 
				 
			 				 
			 

			 

			  
  Home    About the President  
  

		  

		 

		
		
			
				 

			
		
		 

		
	  

	
	 
	 

		
		 

			
			
	
	 

		
		
		 

			
			 Update on Review of Library Report 
			
			 

				   Author   Jeanine Hornish Rakow     Published on    October 15, 2014  October 14, 2014     Leave a comment  
			  

			
		  

		
		 

			 At my request, the Redefining the Library Committee was established to propose a plan for the future direction of the Libraries.  The committee has fulfilled their charge, developed a report with recommendations, and solicited response(s) to the report via a survey sent to Library faculty and staff and Information Technology Services (ITS) staff.  The Library Policies Committee of the Faculty Senate is currently reviewing this report and will present their feedback to Provost Weddle-West and me by the end of 2014 Fall Semester.  Let me express my gratitude to Dr. Green and the Faculty Senate committee for taking on this important task.  Let me assure all that we are committed to a strong, healthy and effective library, one that advances both our educational and research missions. 
  The feedback from the Faculty Senate committee will become part of the documentation that will be considered as we chart the future direction of the library and information services on campus.  It is anticipated that a decision will be communicated to the university community by mid-January, 2015. At that time, planning for implementation will begin.  Both faculty and staff will have significant engagement in both the planning and implementation processes as we move ahead.  
 I appreciate your patience throughout this process.  These are complex issues with significant impact for UofM, and we value the work done to assist in the understanding of our options and opportunities.  No changes have been implemented to date.  We will await direction from the committee first. 
  We look forward to charting a path that reflects the best practices for the libraries and information services.  I am very much committed to the library being the heart of intellectual activity and collaborative learning on campus and our future will certainly reflect that commitment in concrete fashion.  
  Regards,  
 M. David Rudd, President 

		  

		
		 

			  Published on    October 15, 2014  October 14, 2014      Author   Jeanine Hornish Rakow   
			
		  

		
	  

	
				
	 
		 Post navigation 
		    Previous article:  SACSOC  Re-accreditation Report Filed      Next article:  The Kemmons Wilson School of Hospitality and Resort Management to merge with Sport and Leisure Commerce    
	 
				

 

	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  


			
			
		  

		
	  


	
		
		
		 

		 Main Sidebar 

			
			  
				 
					 Search for: 
					 
				 
				 
			  		 		 Recent Posts 		 
					 
				 UofM Quality Indicator Score 
						 
					 
				 University of Memphis Research Foundation Announces Grand Opening of Student-Operated FedEx Call Center 
						 
					 
				 Pause to Grieve 
						 
					 
				 University of Memphis Magazine: Fall 2017 
						 
					 
				 Campus Update 
						 
				 
		 		  Recent Comments      Archives 		 
			  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  July 2014  
	  June 2014  
	  May 2014  
	  April 2014  
		 
		   Categories 		 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 
			
		  

		
		  

	
	
	 

		
		 Footer Content 

		 
		
			
				
				
								
			
		  
	
		 

			
			
			Using  Tiny Framework     
			
			   Log in  

		  
		
		 

			
			

 

	 Social Links Menu 

	      i class= fa fa-twitter   /i    
  
  

			
		  

		
	  

	
  


        

        









		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 
 