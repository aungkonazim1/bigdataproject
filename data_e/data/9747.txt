Dealing With Complaints (Student, Faculty, and Staff) - CAS - University of Memphis    










 
 
 
     



 
    
    
    Dealing With Complaints (Student, Faculty, and Staff) - 
      	CAS
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			College of Arts   Sciences
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Areas of Study  
                          Research  
                          Scholarships  
                          Students  
                          Resources  
                          News  
                      
                      
                         
                            CAS Chair Handbook   
                            
                               
                                  
                                   Part I  
                                         Preface  
                                         Roles and Responsibilities  
                                         Survival Strategies  
                                         Departmental Culture   Chair Leadership Style  
                                         Length of Term and Transition to a New Chair  
                                     
                                  
                                   Part II  
                                        
                                         Management Introduction  
                                        
                                         Major Event Timeline  
                                        
                                         Attendance Expectations   Absence Reporting  
                                        
                                         Department Meetings  
                                        
                                         Budget Responsibilities 
                                          
                                           
                                             
                                               Understanding Your Budget  
                                             
                                               Viewing Your Budgets in Self-Service Banner  
                                             
                                               Monitoring Payrolls and Budgets in e~Print  
                                             
                                           
                                          
                                        
                                        
                                         Staff Management  
                                        
                                         Time and Leave  
                                        
                                         Annual Evaluations (Faculty and Staff)  
                                        
                                         Dealing with Complaints (Student, Faculty, and Staff)  
                                        
                                         Development (Donor Relations)  
                                     
                                  
                                   Part III  
                                         Faculty Recruitment  
                                         Joint Appointments  
                                         Tenure   Promotion  
                                         Teaching Load Guidelines  
                                         Tenure Track Faculty  
                                         Non-tenure Track Full-Time Faculty  
                                         Tenured Faculty  
                                         Part-Time Faculty  
                                         Retirement and Post-Retirement  
                                         Emeritus status for retired faculty members  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	CAS
                              	  
                          
                              	CAS Chair Handbook
                              	  
                         Dealing With Complaints (Student, Faculty, and Staff) 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Dealing with Complaints 
                     
                      Student misconduct 
                     
                      Familiarize yourself with the Office of Student Conduct website.  It contains all
                        the policies, procedures, and forms for handling student misconduct.  It is also highly
                        recommended that you invite the associate dean of students to a departmental meeting.
                         Don't make up your own rules; follow the Office of Student Conduct procedures.  This
                        will save you a world of trouble.
                      
                     
                      Office of Student Conduct functions and responsibilities 
                     
                       The Office of Student Conduct administers the Code of Student Rights and Responsibilities
                           and other student policies, including academic dishonesty, classroom disruptive behavior,
                           peer-to-peer sexual harassment, sexual assault, and student organizations. Our responsibilities
                           include investigating complaints filed against students, referring students for academic
                           and personal counseling services, administrative adjudication of cases, and monitoring
                           student completion of sanctions. We provide student ethical programming addressing
                           issues of academic integrity, civility, and dispute resolution .  Go to  http://www.memphis.edu/studentconduct/   for complete information.
                      
                     
                      Student grade appeals 
                     
                      
                        
                         Graduate grade appeal process  http://catalog.memphis.edu/content.php?catoid=4 navoid=102   
                         
                        
                         Undergraduate grade appeal  process  http://www.memphis.edu/studentlife/pdfs/student_handbook.pdf   
                         
                        
                         RODP grade appeal process  http://www.memphis.edu/advising/advisors/rodpfaq.php   
                         
                        
                      
                     
                      
                     
                      Faculty conflict 
                     
                      Dealing with faculty collegiality problems can be one of the biggest challenges a
                        department chair faces.  In departments of all sizes there can be factions within
                        the faculty, or deep-seated personality conflict between two faculty members, or a
                        faculty member who gets by without doing his/her share—usually being so grumpy nobody
                        wants to ask him/her to do anything.  If these types of problems are not addressed,
                        they can have long-term, negative impacts on your department's productivity and just
                        generally make the workplace unpleasant. 
                      
                     
                      What should you do? 
                     
                      
                        
                         Don't go it alone.  If the problem is too sensitive to discuss with senior members
                           of your department (or if they are part of the problem), ask a trusted fellow chair
                           for advice.  Meet with the dean or an associate dean.  They were department chairs
                           before they landed in the dean's office and they are always willing to lend an ear.  
                         
                        
                         Read Chapters 8-11 of Jeffrey Buller's  The Essential Department Chair .  Dr. Buller provides very concrete suggestions for dealing with faculty conflict.
                           The dean's office will lend you a copy.  We'd even be happy to buy you a copy!   
                         
                        
                         Be aware of the Faculty Ombudsperson.   http://www.memphis.edu/facultysenate/ombudsperson.php   
                         
                        
                      
                     
                      
                     
                      Staff conflict 
                     
                      If you have staff members who are not working well together, you are not the only
                        one who has noticed.  The entire department suffers.  Staff conflict is only slightly
                        less miserable than faculty conflict and it hits you in the face every time you go
                        to the office.
                      
                     
                      What should you do? 
                     
                      
                        
                         Don't jump to conclusions. Investigate gently. Get all sides of the story.  
                         
                        
                         Bring the parties together and discuss workplace expectations.  They do not have to
                           like one another but they must be civil in the workplace.  Don't take it for granted
                           that everyone has the same idea of what civility means.  
                         
                        
                         Review job duties and clarify as needed.  The smallest confusion over who is responsible
                           for what can lead to major conflict.  
                         
                        
                         Make sure you are distributing work loads appropriately. Is it possible that you are
                           dumping extra work on a good-natured employee and avoiding interaction with a surly
                           or incompetent one?   
                         
                        
                         If you have tried all of the above and still not made significant headway, contact
                           the HR Employment Engagement manager and ask for a consult. Or contact the dean's
                           office.  We've probably had a similar problem.  
                         
                        
                      
                     
                      
                     
                      Sexual harassment 
                     
                      If you have not already taken the university's sexual harassment training, please
                        do so now.  All university employees are required to take the training.  Go to the
                        affirmative action training webpage:   http://www.memphis.edu/oie/policies/training.php  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         CAS Chair Handbook 
                         
                            
                               
                                Part I  
                                      Preface  
                                      Roles and Responsibilities  
                                      Survival Strategies  
                                      Departmental Culture   Chair Leadership Style  
                                      Length of Term and Transition to a New Chair  
                                  
                               
                                Part II  
                                     
                                      Management Introduction  
                                     
                                      Major Event Timeline  
                                     
                                      Attendance Expectations   Absence Reporting  
                                     
                                      Department Meetings  
                                     
                                      Budget Responsibilities 
                                       
                                        
                                          
                                            Understanding Your Budget  
                                          
                                            Viewing Your Budgets in Self-Service Banner  
                                          
                                            Monitoring Payrolls and Budgets in e~Print  
                                          
                                        
                                       
                                     
                                     
                                      Staff Management  
                                     
                                      Time and Leave  
                                     
                                      Annual Evaluations (Faculty and Staff)  
                                     
                                      Dealing with Complaints (Student, Faculty, and Staff)  
                                     
                                      Development (Donor Relations)  
                                  
                               
                                Part III  
                                      Faculty Recruitment  
                                      Joint Appointments  
                                      Tenure   Promotion  
                                      Teaching Load Guidelines  
                                      Tenure Track Faculty  
                                      Non-tenure Track Full-Time Faculty  
                                      Tenured Faculty  
                                      Part-Time Faculty  
                                      Retirement and Post-Retirement  
                                      Emeritus status for retired faculty members  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply now for an Undergraduate Degree  
                               Pursue your dream, broaden your career choices and gain the confidence you need to
                                 succeed
                               
                            
                            
                                Offering Master's and Doctoral Degrees  
                               Enhance your knowledge, skills and experience 
                            
                            
                                Community Engagement  
                               Teaching and serving the Memphis and Mid-South community through programs and events 
                            
                            
                                Contact Us  
                               Dean's Office Contacts, Advising, Department Chairs... 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


