 Instruction and Curriculum Leadership - Graduate School - University of Memphis    










 
 
 
     



 
    
    
     Instruction and Curriculum Leadership - 
      	Graduate School 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Graduate School
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Future Students  
                          Current Students  
                          Degrees  
                          News   Events  
                          Resources  
                          Contact Us  
                      
                      
                         
                            Resources   
                            
                               
                                  
                                   Graduate School Calendar  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Graduate School 
                              	  
                          
                              	Resources
                              	  
                          Instruction and Curriculum Leadership 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Department of Instruction and Curriculum Leadership 
                     
                      The University bestows the designation "Graduate Faculty" upon faculty following review
                        of their credentials and recommendation by their colleagues. The University maintains
                        six levels of graduate faculty: (1) Full, (2) Associate, (3) Adjunct, (4) Adjunct
                        Research Co-Mentor, (5) Affiliate, and (6) Adjunct Teaching.
                      
                     
                      Only Full graduate faculty members may chair doctoral committees. Adjunct Research
                        Co-Mentor members may serve as co-chair of a master's or doctoral committee. Full
                        or Associate graduate faculty may chair master's committees. Only one adjunct or affiliate
                        graduate faculty member may serve as a voting member on any master's or doctoral committee.
                      
                     
                      Teaching adjunct members may not serve on graduate committees. 
                     
                      
                     
                      Full MEMBERS 
                     
                      LEE E. ALLEN, Associate Professor, EdD (2005), Pepperdine University [2020] 
                     
                      CELIA ROUSSEAU ANDERSON, Associate Professor, PhD (2002), The University of Wisconsin-Madison
                        [2019]
                      
                     
                      REBECCA S. ANDERSON, Professor, PhD (1993), Virginia Polytechnic   State University
                        [2020]
                      
                     
                      ERIKA C. BULLOCK, Assistant Professor, PhD (2013), Georgia State University [2021] 
                     
                      JEFFREY M. BYFORD, Associate Professor, PhD (2002), University of Oklahoma [2020] 
                     
                      LAURA BAYLOT CASEY, Associate Professor, PhD (2006), Mississippi State University
                        [2020]
                      
                     
                      MARK W. CONLEY, Professor, PhD (1983), Syracuse University [2020] 
                     
                      SHELLY COUNSELL, Assistant Professor, EdD (2007), University of Northern Iowa [2019] 
                     
                      BEVERLY E. CROSS, Associate Professor, PhD (1992), The Ohio State University [2019] 
                     
                      SUTTON FLYNT, Professor, EdD (1979), The University of Georgia [2016] 
                     
                      DUANE M. GIANNANGELO, Professor, PhD (1972), The University of Iowa [2019] 
                     
                      LUCY GREEN,  Associate Professor ,PhD (2010), Texas Tech University [2022] 
                     
                      WILLIAM HUNTER, Assistant Professor, EdD (2011), University of Cincinnati [2020] 
                     
                      DEBORAH L. LOWTHER, Professor, PhD (1994), Arizona State University [2020] 
                     
                      LAURIE MACGILLIVRY, Professor, EdD (1992), The University of Houston [2019] 
                     
                      TREY MARTINDALE, Associate Professor, PhD (1998), Texas A M University [2017] 
                     
                      JAMES N. MEINDL, Assistant Professor, PhD (2011), The Ohio State University [2017] 
                     
                      NEAL MILLER, Assistant Professor, PhD (2012), The Ohio State University [2020] 
                     
                      CLIF MIMS, Associate Professor, PhD (2004), University of Georgia [2020] 
                     
                      RENEE C. MURLEY, Clinical Associate Professor, EdD (2003), The University of Memphis
                        [2021]
                      
                     
                      DEANNA OWENS, Assistant Professor, PhD (2010), Capella University [2020] 
                     
                      J. HELEN PERKINS, Associate Professor, EdD (1999), Oklahoma State University [2023] 
                     
                      ANGILINE POWELL, Associate Professor, PhD (1999), University of Alabama [2019] 
                     
                      AMANDA ROCKINSON-SZAPKIW, Associate Professor, PhD (2009), Regent University [2022] 
                     
                      CLINTON SMITH, PhD (2010), The University of Memphis [2022] 
                     
                      SATOMI IZUMI TAYLOR, Professor, PhD (1992), Virginia Polytechnic   State University
                        [2020]
                      
                     
                      NICOLE L. THOMPSON, Assistant Professor, PhD (2004), University of Georgia [2017] 
                     
                      DEBORAH WATLINGTON, Clinical Assistant Professor, PhD (1990), Texas A M University
                        [2021]
                      
                     
                      ROBERT WILLIAMSON, Assistant Professor, EdD (2009), The University of Memphis [2019] 
                     
                      JULIA W. WILSON, PhD (2015), University of Memphis [2022] 
                     
                      BRIAN L. WRIGHT, Assistant Professor, PhD (2007), Tufts University [2020] 
                     
                      
                     
                      ASSOCIATE MEMBERS 
                     
                      ERIKA C. BULLOCK, Assistant Professor, PhD (2013), Georgia State University [2020] 
                     
                      LAUREN BURROW, Assistant Professor, EdD (2012), University of Houston [2019] 
                     
                      ALFRED HALL II, Assistant Professor, PhD (1999), George Mason University [2019] 
                     
                      NEAL MILLER, Assistant Professor, PhD (2012), The Ohio State University [2019] 
                     
                      DEANNA OWENS, Assistant Professor, PhD (2010), Capella University [2019] 
                     
                      JEREMY TODD WHITNEY, Assistant Professor, PhD (2011), University of Louisville [2020] 
                     
                      
                     
                        
                     
                      ADJUNCT MEMBERS 
                     
                      J. BART CAMPBELL, O.D. (1987), Southern College of Optometry [2018] 
                     
                      VICKIE S. COOK, PhD (2004), Capella University [2020] 
                     
                      CLAIRE KNOWLES, PhD (2016), University of Memphis [2019] 
                     
                      VIVIAN GUNN MORRIS, Professor, PhD (1973), George Peabody College of Vanderbilt University
                        [2020]
                      
                     
                      JENNIFER NELSON,  EdD (2014), University of Memphis [2018] 
                     
                      ALLISON POTTER, PhD (2004), University of Memphis [2020] 
                     
                      JACQUES SINGLETON, EdD (2006), University of Memphis [2017] 
                     
                      SUHA RAHIF TAMIM, EdD (2012), University of Memphis [2017] 
                     
                      ROBERT L. WILLIAMSON, Assistant Professor, EdD (2009), The University of Memphis [2019] 
                     
                      XIAOPING XIONG, EdD (1991), Purdue University [2015] 
                     
                      
                     
                      ADJUNCT TEACHING MEMBERS 
                     
                      TERESA BANKS, EdD (2008), Jackson State University [2015] 
                     
                      MARGARET BARBER MURDOCK, EdS (2006), University of Memphis [2018] 
                     
                      DORIAN BAILEY BROWN, EdD (2014), University of Memphis [2017] 
                     
                      CAROL A. BROWN, PhD (1999), University of Memphis [2020]  
                     
                      RICHARD CORTESE CASEY Jr, EdD (2010) University of Memphis [2018] 
                     
                      TAROL CLEMENTS, EdD (2012), Union University [2018] 
                     
                      KRISTY CONGER, MS (2011), The University of Memphis [2018] 
                     
                      CRYSTAL COOK, PhD (2015), University of Memphis [2019] 
                     
                      SARAH CAMPBELL, PhD (2010), Mississippi State University [2015] 
                     
                      FRAN CLARK, EdD (2002), University of Memphis [2015] 
                     
                      TAROL CLEMENTS, EdD (2012), Union University [2020] 
                     
                      EDWARD MICHAEL DUCEY Jr., PhD (2013), University of Memphis [2019] 
                     
                      SUSAN B. DOLD, EdD (1994), Memphis State University [2017] 
                     
                      CYNTHIA DIXON, EdS (2011), University of Mississippi [2016] 
                     
                      EDWARD M. DUCEY, JR., EdD (2013), University of Memphis [2016] 
                     
                      MELISSA N. DUCEY, EdD (2011), University of Memphis [2017] 
                     
                      LINDA S. ELLER, MEd (1986), Memphis State University [2018] 
                     
                      GEORGE FALLS III, EdD (1997), The University of Memphis [2018] 
                     
                      JULIE FORBESS, EdD (2010), University of Memphis [2015] 
                     
                      LESTER GRAVES, MS (2002), Mississippi Valley State University [2016] 
                     
                      DINAH HARRIS, MS (2008), University of Alabama [2017] 
                     
                      LECHARLE HARRIS, EdD (2009), University of Memphis [2015] 
                     
                      ELIZABETH HEEREN, PhD (2007), University of Memphis [2019] 
                     
                      WENDY JOHNSON LINZ, EdD (2010), University of Memphis [2017] 
                     
                      TRACY LONG, MAT (2003), Middle Tennessee State University [2015] 
                     
                      LAURIE MILLER, EdD (2012), Union University [2015] 
                     
                      MARLA PHILLIPS, MEd (1997), Trevecca Nazarene University [2019] 
                     
                      KATHERINE POWERS, PhD (2005), Mississippi State University [2016] 
                     
                      MARY PULLEN, EdD (1993), Memphis State University [2015] 
                     
                      SHARON E. SMALDINO, PhD (1987), Southern Illinois University [2018] 
                     
                      SARAH SMILOWITZ, EdD (2014), University of Memphis [2018] 
                     
                      JOANN SEVIER-LAWS, EdD (2008), The University of Memphis [2017] 
                     
                      JEFFREY SMITH, EdD (2014), University of Memphis [2018] 
                     
                      KATHRYN STAKEM, EdD (2013), University of Memphis [2016] 
                     
                      DANNY E. THOMPSON, JR., EdD (2011), The University of Memphis [2017] 
                     
                      JOE D. WEAVER, JR., MS (2010), University of Memphis [2017] 
                     
                      KENDALE WHITE, EdS (2004), Union University [2016] 
                     
                      MARY WILKINS, EdD (2012), The University of Memphis [2019] 
                     
                      VIVIAN WILLIAMS, EdD (2004), Union University [2016] 
                     
                      JEANNE WILSON, EdD (2001), The University of Memphis [2015] 
                     
                      YURI G. YANCY, EdD (2012), Union University [2016] 
                     
                      CAROL YOUNG, MA (1987), St. Joseph College [2015] 
                     
                      
                     
                      AFFILIATE MEMBERS 
                     
                      KIMBERLY A. ALEXANDER, PhD (2014), Saint Louis University [2019] 
                     
                      BONNIE CUMMINGS, MEd+ (1974), Memphis State University [2017] 
                     
                      ANNETTE CORNELIUS, EdD (2013), The University of Memphis [2017] 
                     
                      CHAD EPPS, MD (2000) Medical College of Georgia [2020] 
                     
                      FELICIA FOWLER, EdD (2000), University of Memphis [2020] 
                     
                      NANCY GALLAVAN, PhD (1994), University of Denver [2019] 
                     
                      BRIAN GREEN, MD (2001), University of Tennessee [2020] 
                     
                      LISA J. HIGHT, EdD (1996), University of Memphis [2017] 
                     
                      TORRE KELLEY, EdD (2010), Trevecca Nazarene University [2017] 
                     
                      CATHY MEREDITH, EdD (1996), The University of Memphis [2017] 
                     
                      ELLEN JEAN MCDONALD, EdD (1973), University of Memphis [2017] 
                     
                      DONA PACKER, EdD (2003), University of Louisiana, Monroe [2017] 
                     
                      LINDA MCNATT PAGE, EdD (2005), Union University [2017] 
                     
                      ANDREW POLLY, PhD (2006), University of North Carolina at Charlotte [2019] 
                     
                      MARY RANSDELL, Clinical Assoicate Professor, EdD (2002), University of Kentucky [2018] 
                     
                      KAY C. REEVES, Associate Clinical Professor, EdD (2003), The University of Memphis
                        [2017]
                      
                     
                      JERRIE SCOTT, Professor, PhD (1976), University of Michigan [2017] 
                     
                      ELIZABETH J. SHUMATE, PhD (2012), Harding University [2020] 
                     
                      SUHA TAMIN, EdD (2012), University of Memphis [2018] 
                     
                      ROBERT TAYLOR, PhD (1982), University of Oregon [2018] 
                     
                      ALANA TEAGUE, EdD (2012), Trevecca Nazarene University [2020] 
                     
                      CARMEN WEAVER, EdD (2012), The University of Memphis [2019] 
                     
                      CATHERINE TEMPLE WILSON, EdD (1996), University of Memphis [2017] 
                     
                      
                     
                       adjunct research co-mentor  
                     
                      LISA M. WATTS, PhD (2005), University of Wisconsin- Milwaukee [duration of committee] 
                     
                        
                     
                       NOTE:  Expiration date of graduate faculty membership is indicated in brackets.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Resources 
                         
                            
                               
                                Graduate School Calendar  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now!  
                               Take the first step toward your advanced degree. 
                            
                            
                                Degree Programs  
                               Explore our graduate catalog. 
                            
                            
                                Support Graduate Education  
                               Your gift makes a difference! 
                            
                            
                                Contact Us  
                               Questions? The Grad School Staff can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


