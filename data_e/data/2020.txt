About Directions Buildings Campus Map - UofM Archives - University of Memphis    










 
 
 
     



 
    
    
    About Directions Buildings Campus Map - 
      	UofM Archives
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			UofM Archives
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                       
                   
                
             
          
          
             
                
                   
                      
                         
                            Section Config error: breadcrumb not defined in section_config.pcf file. Please see
                               (Click here)  for details how to setup breadcrumb correctly.
                            
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      About the campus 
                     
                      Directions 
                     
                        From Interstate 40   
                     
                      
                        
                         Take Exit 82A and head south on Highway 45 which becomes North Highland Avenue. 
                        
                         At the sixth traffic light, turn right onto Lambuth Boulevard. 
                        
                         Go approximately one mile. The University of Memphis Lambuth is on the right. 
                        
                      
                     
                        From Highway 45 South   
                     
                      
                        
                         Follow Highway 45 through downtown Jackson until it becomes North Highland Avenue. 
                        
                         Go through a traffic light at Forest Avenue and through a second light at Campbell
                           Street.
                         
                        
                         Turn left at the third light which is Lambuth Boulevard. 
                        
                         Go approximately one mile. The University of Memphis Lambuth is on the right. 
                        
                      
                     
                        From Highway 45 By-Pass   
                     
                      
                        
                         Exit at Hollywood Drive. 
                        
                         Turn left at the bottom of the exit ramp. 
                        
                         At the first traffic light, turn left on West Forest Avenue. 
                        
                         Turn right at the third light which is Lambuth Boulevard. 
                        
                         Go approximately one mile.  The University of Memphis Lambuth is on the right. 
                        
                      
                     
                        From Highway 412   
                     
                      
                        
                         Entering Jackson from the northwest side of town, Highway 412 becomes Hollywood Drive
                           after you pass under Interstate 40.
                         
                        
                         Turn left on West Forest Avenue which is the fifth traffic light after you pass under
                           I40.
                         
                        
                         Turn right at the third traffic light which is Lambuth Boulevard. 
                        
                         Go approximately one mile. The University of Memphis Lambuth is on the right. 
                        
                         Varnell-Jones Hall is visible from the street and has a semicircular drive in the
                           front.
                         
                        
                         Visitor parking is available on the semicircular drive and on Lambuth Boulevard. 
                        
                      
                     
                      Bldg Hours of operation 
                     
                      
                        
                         Regular Library hours, Monday - Thursday, 9:00 am - 5:30 pm; Friday, 8:00 am - 4:30
                           pm.
                         
                        
                         Varnell-Jones Hall, Please refer to daily  list of classes .
                         
                        
                         Wilder Student Union Building, Monday - Thursday, 7:00 am - 10:00 pm; Friday, 7:00
                           am - 4:30 pm; CLOSED Saturday; Sunday, 1:00 - 9:00 pm.
                         
                        
                         
                           
                            TAF LAB, Room 6, Monday – Thursday 9:00 am – 8:00 pm; Friday  9:00 am – 4:30 pm; CLOSED
                              Saturday; Sunday 1:00 – 6:00 pm 
                            
                           
                            LEARNING CENTER, Room 4A, Monday - Thursday, 1:00 - 5:00 pm, and by appointment on
                              Fridays. Call 425-7398 or email  Michelle Reddick  to schedule a Friday appointment with a tutor.
                            
                           
                            CAREER SERVICE CENTER, suite 4H, 8:00 am - 4:30 pm, Monday - Friday .    
                           
                            FITNESS CENTER, Monday - Thursday, 12:00 - 8:00 pm; Friday, 12:00 - 6:00 pm. 
                           
                            POOL HOURS, Monday - Thursday, 3:00 - 7:00 pm; Friday, 3:00 - 5:30 pm 
                           
                            STUDENT SERVICES LOUNGE, Monday - Thursday, 12:00 pm - 9:00 pm; Friday, 12:00 pm -
                              4:30 pm; Saturday and Sunday, 2:00 - 9:00 pm.
                            
                           
                            MATH LAB, WSU 4-I 
                           
                            WRITING CENTER, 4-J 
                           
                         
                        
                         Wellness, Health   Fitness Building, Monday - Thursday, 12:00 - 8:00 pm; Friday, 12:00
                           - 6:00 pm for recreational use
                         
                        
                         For information on showtimes at the The M. D. Anderson planetarium, click  HERE .       
                        
                      
                     
                        STUDENT TAF COMPUTER LABS are available in the following locations:   
                     
                      
                        
                         Library, Monday - Thursday, 9:00 am - 5:30 pm; Friday, 8:00 am - 4:30 pm. 
                        
                         Wilder Student Union, Room 6, Monday – Thursday 9:00 am – 8:00 pm; Friday  9:00 am
                           – 4:30 pm; CLOSED Saturday; Sunday 1:00 – 6:00 pm.
                         
                        
                      
                     
                      Campus Map 
                     
                       Click here to view and print a map  of our campus!
                      
                     
                      Arboretum map 
                     
                      UofM Lambuth is a Level 2 Certified Arboretum as designated by the Tennessee Urban Forestry
                        Council. Take a tour with our  map .
                      
                     
                        
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Search People  
                               UofM Whitepages 
                            
                            
                                AskTOM  
                               umTech Service Desk 
                            
                            
                                Campus Map  
                               UofM Destinations 
                            
                            
                                Web Directory  
                               Web Directories and Listings 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


