AIS    
 
 
 AIS 
 


 
 
 

 
 Artificial Immune Systems 
 

 
 
 
  Main Page  
  Home  
  Students  
  Publications  
  Books  
  Dissertations  
  Theses  
  Datasets  
  Research  
  IEEE Task Force  
  Other Sites  
  Bibliography  

 

 
 

 
  
  
 
 
Dr. Dipankar Dasgupta 
333 Dunn Hall 
Memphis, TN 38152-3240 
 
phone: (901) 678-4147 
fax: (901) 678-2480 
 dasgupta@memphis.edu 
 
 Professor,  Department of Computer Science ,  University of Memphis  
 Director,  Center for Information Assurance  
 Director,  Intelligent Security Systems Research Laboratory  
 
You can read more about  Dr. Dipankar Dasgupta , his publications, and his research interests at his  profile .
 
 

 


 What Are Artificial Immune Systems? 

 

 The biological immune system is a highly parallel, distributed, and adaptive system. It uses learning, memory, and associative retrieval to solve recognition and classification tasks. In particular, it learns to recognize relevant patterns, remember patterns that have been seen previously, and use combinatorics to construct pattern detectors efficiently. These remarkable information processing abilities of the immune system provide important aspects in the field of computation. This emerging field is sometimes refered to as Immunological Computation, Immunocomputing, or Artificial Immune Systems (AIS). Although it is still relatively new, AIS, having a strong relationship with other biology-inspired computing models, and computational biology, is establishing its uniqueness and effectiveness through the zealous efforts of researchers around the world. 

New Book on Aritifical Immune Systems:  "Immunological Computation: Theory and Applications"  ( Cover Page ).

   Dipankar  Dasgupta's talk at the University of Illinois at Urbana Champaign (NIGEL 2006 workshop).  




 

 
 
 
