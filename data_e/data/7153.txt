Understanding ePortfolio settings | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Understanding ePortfolio settings | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    ePortfolio    »    ePortfolio basics   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Understanding ePortfolio settings                       
        
        
       
          
     
           Printer-friendly version       
	Settings allow you change display options and preferences for your ePortfolio. Click    Settings  on the Dashboard, My Items, Explore, or Sharing Groups page to access these preferences.
 

    
				Setting
			 
			 
				Description
			 
		   
				Display Options
			 
		   
				 
					Default list size
				 
			 
			 
				 
					Select the number of items to display in your newsfeed, My Items, and Explore lists.
				 
			 
		   
				 
					Number of activities on newsfeed items
				 
			 
			 
				 
					Select the maximum number of events you want to display in each newsfeed item.
				 
			 
		   
				 
					My Modifications
				 
			 
			 
				 
					Choose whether to include activity you make on your own items to your newsfeed.
				 
			 
		   
				 
					Number of unread invites on dashboard
				 
			 
			 
				 
					Select how many unread invites appear on your Dashboard.
				 
			 
		   
				 
					Date Range
				 
			 
			 
				 
					Select how many days of past activity you want to show in your newsfeed.
				 
			 
		   
				Item Defaults
			 
		   
				 
					Item feedback
				 
			 
			 
				 
					Choose whether you want to allow comments or assessments by default when creating new items.
				 

				 
					 Note  These settings enable commenting and assessments; you still need to set permissions around who can add comments or assessments.
				 
			 
		   
				RSS Updates
			 
		   
				 
					Feedback RSS
				 
			 
			 
				 
					Notifies you when edits, comments, or assessments are made on one of your portfolio items (does not include changes that you make yourself).
				 

				 
					To use the RSS feeds you must subscribe to an external RSS reader. There are many free RSS readers, such as Live Bookmarks and Google Reader, available on the internet.
				 
			 
		   
				Ignore List
			 
		   
				 
					Ask for confirmation before ignoring items from users
				 
			 
			 
				 
					Select this check box if you want to recieve a confirmation dialog asking if you want to ignore items from a user.
				 
			 
		   
				 
					Ask for confirmation before restoring items from users
				 
			 
			 
				 
					Select this check box if you want to recieve a confirmation dialog asking if you want to restore items from a user.
				 
			 
		   
				 
					Invite RSS
				 
			 
			 
				 
					Notifies you when you receive a new invite to view another user's item.
				 

				 
					To use the RSS feeds you must subscribe to an external RSS reader. There are many free RSS readers, such as Live Bookmarks and Google Reader, available on the internet.
				 
			 
		   
				Sharing Settings
			 
		   
				 
					Invite Delivery Method
				 
			 
			 
				 
					Select  Email Invitation  if you want to receive an email when others invite you to view items from their ePortfolio.
				 

				 
					 Note  A copy of the invite always appears on your Invites page.
				 
			 
		   
				 
					Default Sharing Group
				 
			 
			 
				 
					Select a default sharing group to apply to all new items in your portfolio that meet your filter criteria. Use this option if you consistently share new items with the same group of users.
				 
			 
		        Audience:    Learner      

    
           

                   ‹ Understanding the My Items page 
        
                   up 
        
                   Enabling notifications in ePortfolio › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        ePortfolio  
  
      ePortfolio basics    Accessing ePortfolio    Understanding the Dashboard    Understanding the Explore page    Understanding the My Items page    Understanding ePortfolio settings    Enabling notifications in ePortfolio      Using artifacts    Creating and editing presentations    Creating collections    Managing comments and assessments in ePortfolio    Understanding the basic concepts in sharing    Importing and exporting items    Sharing items within courses    Using form templates    Integrating ePortfolio with Content     Assessing ePortfolio content    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
