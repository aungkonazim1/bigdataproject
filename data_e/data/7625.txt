University of Memphis Creates JD/MPH Degree Program - UofM Media Room - University of Memphis  Public health, public policy and issues of health disparities are at the forefront of many of America's unresolved social problems. Rising health care costs pose a growing threat to the economy, while for some access to even the most basic health care is tenuous. Lawyers play a central role in society's approach to these public health issues. However, their effectiveness is frequently limited by an inadequate knowledge of public health matters. To close that educational gap, the University of Memphis has created the JD/MPH program.  










 
 
 
     



 
    
    
    University of Memphis Creates JD/MPH Degree Program - 
      	UofM Media Room
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			UofM Media Room
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Publications  
                          News Releases  
                          Events  
                          Resources  
                          Contact Us  
                      
                      
                         
                            News Releases   
                            
                               
                                  
                                   Awards  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	UofM Media Room
                              	  
                          
                              	News Releases
                              	  
                         University of Memphis Creates JD/MPH Degree Program 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      University of Memphis Creates JD/MPH Degree Program 
                     
                      Contact: Gabrielle Maxey 901.678.2843  gmaxey@memphis.edu  
                     
                      May 8, 2015 - Public health, public policy and issues of health disparities are at
                        the forefront of many of America's unresolved social problems. Rising health care
                        costs pose a growing threat to the economy, while for some access to even the most
                        basic health care is tenuous. Lawyers play a central role in society's approach to
                        these public health issues. However, their effectiveness is frequently limited by
                        an inadequate knowledge of public health matters. To close that educational gap, the
                        University of Memphis has created the JD/MPH program.
                      
                     
                      The program offers students the opportunity to pursue JD (juris doctor) and MPH (master
                        of public health) degrees simultaneously. Students will have the ability to complete
                        both degrees in four years of full-time study, a year less than it would take to complete
                        each degree separately.
                      
                     
                      The goal of the JD/MPH program is to prepare and train students to apply law and policy
                        to benefit public health. The dual degree allows law students to combine in-depth
                        advocacy and policy skills learned in the UofM's JD program with the population health
                        perspective gained in the MPH program to address policy and advocacy needs at the
                        community level.
                      
                     
                      "This interdisciplinary professional education supports the training of highly competent
                        public health lawyers, as well as public health professionals skilled in advocacy
                        and public health policy," said Dr. Marian Levy, assistant dean of Students and Public
                        Health Practice in the School of Public Health. "This dual degree program addresses
                        community needs related to environmental and social justice, access to health care
                        and other pressing public health concerns."
                      
                     
                      Amy Campbell, associate law professor and director of the Institute for Health   Law
                        Policy, said, "We're delighted by this formal partnership and the commitment to interdisciplinary
                        education that it represents. The JD/MPH dual degree option opens up for law students
                        interested in public health law – a growth area within health law – an invaluable
                        opportunity for deepened engagement with public health professional colleagues, with
                        the hope that this enhances their appreciation for and skills in developing evidence-informed
                        health policy. It also reaffirms the institute's mission of 'using law to advance
                        health' for the benefit of our Memphis community."
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         News Releases 
                         
                            
                               
                                Awards  
                            
                         
                      
                      
                      
                         
                            
                                Music Performance Calendar  
                               Don't miss a School of Music performance 
                            
                            
                                Art Museum of Memphis  
                               Learn about the latest installations 
                            
                            
                                The Martha and Robert Fogelman Galleries of Contemporary Art  
                               View upcoming Department of Art exhibits 
                            
                            
                                Contact Us  
                               Have a story to share or questions about UofM News? 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


