 Professorships - Academic Affairs - University of Memphis    










 
 
 
     



 
    
    
     Professorships - 
      	Academic Affairs
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Academic Affairs
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Provost  
                          Resources  
                          Academic Affairs Units  
                          Employee Recognition  
                      
                      
                         
                            Employee Recognition   
                            
                               
                                  
                                   Chairs of Excellence  
                                   University Professorships  
                                   Awards Program  
                                         About Awards Programs  
                                         Academic Affairs Nominations  
                                         Academic Affairs Winners  
                                         Faculty Awards  
                                         Faculty Award Winners  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Academic Affairs
                              	  
                         
                           	Employee Recognition
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      University Professorships 
                     
                      Purpose 
                     
                      The University of Memphis employs a number of faculty members who have made significant
                        contributions to its educational, research, outreach, and service missions and bring
                        national and international recognition to the University and to our community. Their
                        contributions are recognized in a number of university-level awards programs, including
                        the Eminent Faculty Award, the Distinguished Research Award, the Outstanding Teaching
                        Award, and other comparable types of recognition. These programs, however, do not
                        reach as many of the University's eligible faculty as we would like. We have consequently
                        developed a University Professorships program that augments out current awards program
                        and will help us retain our best faculty. This program presently includes three Professorships.
                        Click on the links below to see additional information on a specific professorship:
                      
                     
                       The Faudree University Professorships  -- To donate,  click here .  The Atkins University Professorships  -- To donate,  click here .  The First Tennessee University Professorships  -- To donate,  click here .  Previous Award Recipients  
                     
                      Process 
                     
                      
                        
                          Faculty members, department chairs, or other college administrators would nominate
                              eligible recipients.  
                        
                          The Provost, with input from an advisory faculty group, would select the recipient(s)
                              of a University Professorship subject to the approval of the President of the University.  
                        
                          Awardees would be allowed to use their allocations for salaries, travel, supplies,
                              or other expenses in any proportion they choose, as long as the total sum does not
                              exceed the annual award amount. The actual amount of the salary supplement and miscellaneous
                              awards may vary slightly from year-to-year due to investment income results, but would
                              usually be approximately $5,000 per year for full professors and $2,500 per year for
                              associate professors.   
                        
                          Continued salary increases would be computed on the awardee's base salary, less the
                              value of the supplemental income related to the University Professorship. Upon relinquishing
                              the University Professorship, the faculty member's salary would revert to his/her
                              base, including any routine salary increases provided during the period of the award,
                              but less the supplemental increase associated with the University Professorship.     
                        
                      
                     
                          Faudree University Professorships
                      
                     
                      In consideration of Mr. and Mrs. Jabie Hardin's desire to demonstrate their regard
                        and support for Dr. Ralph Faudree, former Provost of the University of Memphis, this
                        program was established. 
                      
                     
                      
                        
                         Full and associate tenured professors with ten years of service at the University
                           of Memphis are eligible for the program. Associate professors with five years of service
                           at the University are eligible for the program.
                         
                        
                         Awardees would hold University Professorships for a maximum of three years. 
                        
                         Selection will be based upon the quality of the faculty member's teaching, research,
                           and service/outreach, with a priority given to faculty contributing in all three areas.
                         
                        
                      
                     
                      To donate to the Faudree University Professorship, please  click here .
                      
                     
                          Atkins University Professorships
                      
                     
                      In consideration of Mr. Olin Atkins's desire to support the Olin Atkins University
                        Professorships program, a faculty rewards program designed to help the University
                        retain its best and brightest faculty and to make an investment in the University's
                        people. 
                      
                     
                      
                        
                         Full and associate tenured professors with ten years of service at the University
                           of Memphis are eligible for the program.
                         
                        
                         Awardees would hold University Professorships for a maximum of three years and would
                           be ineligible for re-consideration for a minimum of three years following expiration
                           of initial awards.
                         
                        
                         Selection will be based upon the quality of the faculty member's teaching, research,
                           and service, with a special emphasis on teaching skill.
                         
                        
                      
                     
                      To donate to the Atkins University Professorship, please  click here .
                      
                     
                          First Tennessee University Professorships
                      
                     
                      In consideration of the desire of the First Tennessee Foundation to support a University
                        professorships program, this program was established to promote the University's goals
                        of academic quality, community connections, and expanded research capabilities by
                        recognizing individual faculty members with financial rewards designed to help the
                        University recruit and retain outstanding professors.
                      
                     
                      
                        
                         Full and associate tenured professors with ten years of service at the University
                           of Memphis are eligible for the program.
                         
                        
                         Awardees would hold University Professorships for a maximum of three years and would
                           be ineligible for re-consideration for a minimum of three years following expiration
                           of initial awards.
                         
                        
                         Selection for professorship will recognize outstanding individual faculty contributions
                           in teaching, scholarship, service, and community outreach. 
                         
                        
                      
                     
                      To donate to the First Tennessee University Professorship, please  click here .
                      
                     
                          PREVIOUS AWARD RECIPIENTS
                      
                     
                      2017 First Tennessee: Eddie Jacobs, Electrical Engineering Faudree: Joe Hayden, Journalism; Bob Hetherington, Theatre   Dance; Chrysanthe Preza,
                        Electrical Engineering
                      
                     
                      2016 First Tennessee: Eddie Jacobs, Electrical Engineering Faudree: Joe Hayden, Journalism; Bob Hetherington, Theatre   Dance; Chrysanthe Preza,
                        Electrical Engineering
                      
                     
                      2015 First Tennessee: Ralph Faudree, Mathematical Sciences; Brian Janz, Management Information
                        Systems; Carol Crown, Art; Robert Kozma, Mathematical Sciences Faudree: Marla Stafford, Marketing Supply Chain; Gisele Goldstein, Mathematical Sciences;
                        Santosh Kumar, Computer Science
                      
                     
                      2014 First Tennessee: Ralph Faudree, Mathematical Sciences; Brian Janz, Management Information
                        Systems; Carol Crown, Art; Robert Kozma, Mathematical Sciences Faudree: Marla Stafford, Marketing Supply Chain; Gisele Goldstein, Mathematical Sciences;
                        Santosh Kumar, Computer Science
                      
                     
                      2013 This FY we were unable to offer any new Faudree, Atkins, or First Tennessee professorships.
                      
                     
                      2012 First Tennessee: Robert Kozma, Mathematical Sciences; Carol Crown, Art; Brian Janz,
                        Management Information Systems Faudree: Ralph Faudree, Provost
                      
                     
                      2011 First Tennessee: Marla Stafford, Marketing   Supply Management Faudree: Gisele Goldstein, Mathematics; Santosh Kumar, Computer Science
                      
                     
                      2010 Faudree: Rick Bloomer, Health and Sport Sciences; Jonathan Judaken, History; Ken Ward,
                        Public Health Atkins: David Appleby, Communication
                      
                     
                      2009 First Tennessee: Deborah Lowther, ICL; Cary Holladay, English Faudree: Pu-Qi Jiang, Music
                      
                     
                      2008 First Tennessee: David Allen, Management; Emin Babakus, Marketing Supply Management;
                        Eugene Buder, AUDSLP; Theodore Burkey, Chemistry; Roy Van Arsdale, Earth Sciences Faudree: Joan Schmelz, Physics
                      
                     
                      2007 First Tennessee: Albert Okunade, Economics Faudree: Stanley Hyland, School of Urban Affairs   Public Policy; Anna Kaminska, Mathematics;
                        Vivian Morris, Education Atkins: Abby Parrill-Baker, Chemistry
                      
                     
                      2006 First Tennessee: David Evans, School of Music; Allison Graham, Communication Faudree: Monika Nenon, Foreign Languages
                      
                     
                      2005 First Tennessee: Gloria Baxter, Theatre   Dance; Carl Halford, Electrical Engineering Faudree: Don Franceschetti, Physics
                      
                     
                      2004 Faudree: Leonard Lawlor, Philosophy: Steven J. Ross, Communication; Steven M. Ross,
                        Counseling, Educational Psychology and Research Atkins: Janann Sherman, History
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Employee Recognition 
                         
                            
                               
                                Chairs of Excellence  
                                University Professorships  
                                Awards Program  
                                      About Awards Programs  
                                      Academic Affairs Nominations  
                                      Academic Affairs Winners  
                                      Faculty Awards  
                                      Faculty Award Winners  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Provost's Blog  
                               Check out the latest Academic Affairs news 
                            
                            
                                Academic Affairs Award Nominations  
                               Submit your nomination to recognize outstanding faculty   staff 
                            
                            
                                Critical Conversations  
                               Join the conversation today! 
                            
                            
                                Healthy Memphis Initiative  
                               Making Memphis a healthier place 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/26/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


