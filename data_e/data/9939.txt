Deleting rubrics | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Deleting rubrics | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Rubrics    »    Rubrics Basics   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Deleting rubrics                       
        
        
       
        
     
              
	You can only delete a rubric from the org unit in which it was created and you cannot delete a rubric that is being used by a Competencies activity or ePortfolio item.
 

 
	Delete a rubric
 

 
	On the Rubrics page,    Delete  from the context menu of the rubric you want to delete.
 
     Audience:     Instructor       

    
           

                   ‹ Reordering rubric achievement levels or criteria 
        
                   up 
        
                   Deleting rubric achievement levels, criteria, or criteria groups › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Rubrics  
  
      Rubrics Basics    Understanding Rubrics    Accessing Rubrics    Creating holistic rubrics    Creating analytic rubrics    Adding rubric achievement levels , criteria, or criteria groups    Editing rubrics, achievement levels, criteria, or criteria groups     Managing rubric sharing properties    Managing rubric status settings    Copying rubrics    Reordering rubric achievement levels or criteria    Deleting rubrics    Deleting rubric achievement levels, criteria, or criteria groups    Viewing rubric statistics      Using Rubrics in Competencies    Using Rubrics in ePortfolio    
                  
           
         

       
     

    
    
    
   
 
   
 
