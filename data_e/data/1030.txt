Hours - Library - LibGuides at University of Memphis Law Library</title> <script type="text/javascript" src="/js/dojo-161/dojo/dojo.js" djConfig="parseOnLoad: true"></script> <script type="text/javascript" src="//code.jquery.com/jquery-1.7.min.js"></script> <script type="text/javascript" src="/include/lang.php" ></script> <script type="text/javascript" src="/js112b/lgpublic.min.js"></script> <script type="text/javascript" src="/include/lib_spring_track.php?action=init&iid=2952"></script> <script> // content_config is instantiated in content.js. content_config.pid = 667976; content_config.sid = '5530417'; content_config.iid = 2952; content_config.la_iid = 0; content_config.gid = 0; content_config.hitsource = 'r'; content_config.myboxshape = 's'; var myGNAME = "Library"; var myTNAME = "Hours"; var myIID = '2952'; var myPID = '667976'; var mySID = '5530417'; // SpringTracking SpringTrack.init({ _st_sid : content_config.sid, _st_iid : content_config.iid, _st_pid : content_config.pid }); </script> <title>Memphis School of Law: ResearchGuides general information and policies  
 
	 
    
         
		
		 
		 
		 
		         
         
         
          
                 
         
         
         
         
         
        
         
         
         
         
         
         
         
         
         
         
          
         
                
                
         
         
        
		        
         Hours - Library - LibGuides at University of Memphis Law Library 
		
		
        
        
        
        
                
		
		 Memphis School of Law: ResearchGuides 

        	 
	 
				 
                    This is the "Hours" page of the "Library" guide. 
                     Alternate Page for Screenreader Users  
                     Skip to Page Navigation  
                     Skip to Page Content  
                 		 
			 
                  
				   
				                   
						 
							 Admin Sign In 
						    
					 Library 
					   
				  
					 LibGuides 
					   
				  Library  
			
		                  
                         
                             Library   
                             Tags:  library ,  library privileges ,  library resources ,  library services    
                         
                                      general information and policies 				
		 
			 Last Updated: Nov 29, 2017 
			 URL: http://libguides.law.memphis.edu/libinfo 
			 
				 
					 Print Guide
					 
				 
			    RSS Updates      Email Alerts   
		 				    
		 Hours     
		 Contact Us     
		 Check-out Privileges     
		 Floor Maps                      
		 
			 
        		 
					 
						 Hours 
							 
								 
									 
										 Comments(0)
										 
									 
								 
							 
						 
							 
								 Print Page 
							  
						 
					 
					 
		 
			 
				 
				 Search Text 
				 Search Type 
				 
					 
						 
							 
								  Search: 
							 
						 
						 
							 
								  
							 
						 
						 
							 
								 
									 This Guide 
                All Guides 
               
								 
							 
						 
						 
							 
								 Search 
							 
						 
					 
				 
			 
		 
					 
				 
			 
		                   
                 
                     
                                        
                                         
                                             
                                         
                                        
                                      
                                        
                                         
                                             
	 
		
		 
			
			   
			 Public Hours
			 
		 
			
		   
   7:30 am until 6:00 pm   
   Monday through Friday   
  
  Members of the public conducting legal research are welcome during these hours.  
  Students, faculty, and staff of the Cecil C. Humphreys School of Law have 24/7 access to the library.  
  The library is closed on university and major holidays and on inclement weather days when the university is closed.   
    
		 
				 Comments (0) 
		 
		  
		 
	  
                                         
                                        
                                      
                                          
                                         
                                             
                                         
                                        
                                     				 
                
 
	 
    	Powered by  Springshare ; All rights reserved. 
    	 
        	 Report a tech support issue .      	 
  	 
	
	 View this page in a format suitable for  printers and screen-readers  or  mobile devices .  

  
	  				 
                	 
                    	 Description 
                   	 
                     
                    	    Loading... 
                   	 
             	 
				 
				 
                	 
                    	 
                        	 
                            	 
                                	   
                               	 
                           	 
                      	 
                         
                        	 More Information 
                       	 
                 	 
                     
                    	 
                        	   Loading...                      	  
						  Close  
                   	 
              	 
			 
		 
                   
        
		
	 
 