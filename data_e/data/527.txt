Student Giving - Annual Giving - University of Memphis    










 
 
 
     



 
    
    
    Student Giving - 
      	Annual Giving
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Annual Giving
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Donate Now  
                          Giving Options  
                          Affinity Groups  
                          Matching Gifts  
                          FAQs  
                      
                      
                         
                            Student Giving   
                            
                               
                                  
                                   Donate Now  
                                   Senior Class Gift  
                                   Common Cents Philanthropy Council  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Annual Giving
                              	  
                         
                           	Student Giving
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                        
                     
                      Student Giving 
                     
                      Students are the heart of the University of Memphis. Often the recipients of the generosity
                        of University donors, many times students are donors themselves. Giving of their resources
                        and time, our students are continuing to build a proud tradition of philanthropy that
                        has made the UofM great.
                      
                     
                      What does the tradition of philanthropy mean to you as a student? 
                     
                      By giving back to the University of Memphis, you support yourself and future generations
                        of students at the UofM. The UofM would be drastically different without the support
                        of private donors, and this is your opportunity to join the legacy through student
                        giving. Giving back to the University of Memphis improves not just your student experience,
                        but the experience of future students by providing scholarships, technology upgrades,
                        and other impactful projects.
                      
                     
                      How Can I Give? 
                     
                      The Senior Class Gift Campaign 
                     
                       The Senior Class Gift Campaign provides each graduating class the opportunity to create
                        a legacy at the University of Memphis. Students can donate directly to the Senior
                        Class Gift Campaign to support any college or fund at the University.  Click here  to learn more about how you can participate in the Senior Class Gift Campaign.
                      
                     
                      Common Cents Philanthropy Council 
                     
                      The Common Cents Philanthropy Council at the University of Memphis consists of a group
                        of highly motivated students interested in the promotion of philanthropic awareness
                        on campus. Students who serve on the council gain experience in event planning, project
                        management, fundraising, strategic planning, and marketing.  Click here  to learn more about how you can participate in the Common Cents Philanthropy Council.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Student Giving 
                         
                            
                               
                                Donate Now  
                                Senior Class Gift  
                                Common Cents Philanthropy Council  
                            
                         
                      
                      
                      
                         
                            
                                Senior Class Gift  
                               Pay it forward to a deserving incoming Tiger student!  
                            
                            
                                Faculty and Staff  
                               It’s not too late to participate in this year’s campaign! 
                            
                            
                                Phonathon Online Giving Form  
                               Click here to make your gift online. 
                            
                            
                                Phonathon Student Callers  
                               We’re recruiting great students who enjoy talking to our alumni! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


