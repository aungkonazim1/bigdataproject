DSST - Testing Center - University of Memphis    










 
 
 
     



 
    
    
    DSST - 
      	Testing Center
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Testing Center
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Services  
                          Accommodations  
                          Visitor Information  
                          Lambuth Testing  
                      
                      
                         
                            Services   
                            
                               
                                  
                                   ACT Residual  
                                   ACCUPLACER  
                                   CLEP  
                                   DSST  
                                   Educational Testing Service  
                                   Miller Analogies Test  
                                   Oral Proficiency Interview  
                                   Para Pro Assessment  
                                   Proctor Services  
                                   TEAS  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Testing Center
                              	  
                          
                              	Services
                              	  
                         DSST 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      DSST (DANTES Subject Standardized Test) 
                     
                      The DSST (DANTES Subject Standardized Test) is a computerized exam given Monday -
                        Friday by  Appointment only  between the hours of 8:30 am – 1:30 pm. DSST exams are held in room  112 of the Brister Hall/ Wilder Tower complex ; exams are  two hours  in length and  $80 (credit/debit card only)  per exam.
                      
                     
                      The University of Memphis serves as a national test center for all DSST exams; because
                        policies regarding DSST credits differ at various institutions, candidates should
                        verify with their respective school on which exams are accepted.  All DSST exams offer
                        immediate scoring with the exception of: Principles of Public Speaking.
                      
                     
                        DSST REGISTRATION PROCEDURES :
                      
                     
                      
                        
                         Read the Test Taker Information Bulletin thoroughly ( www.getcollegecredit.com ).
                         
                        
                         Please review all enclosed site information for additional exam details. 
                        
                         Click the payment link below and pay the  $34.50  registration fee through our convenient online payment system ( NOTE: Registration fee must be paid through the online payment system ).
                         
                        
                         Contact the UofM Testing Center by calling  901.678.2428  to schedule an appointment time.
                         
                        
                      
                     
                       EXAM DAY PROCEDURES :
                      
                     
                      
                        
                         On the day of test, please report to Brister Hall Room 112. 
                        
                         Present your  printed  payment receipt for $34.50 (without a printed proof of payment, examinees may not
                           sit for the exam and all applicable fees may be forfeited).
                         
                        
                         Present one form of valid Government Issued photo identification. 
                        
                         Please have the 4-digit score recipient code from your perspective institution ( NOTE: School codes are on the DSST website,  www.getcollegecredit.com  ).
                         
                        
                      
                     
                        Please click here to submit the $34.50 registration fee   
                     
                       Recommended Preparation Aid  
                     
                      The list below pertains only to those considering the UofM as the score recipient.  
                     
                      
                        
                         
                           
                            
                              
                               
                                    DSST EXAMINATION  
                                  
                              
                               
                                    UNIVERSITY OF MEMPHIS EQUIVALENT  
                                  
                              
                            
                           
                            
                              
                               
                                  HISTORY OF THE VIETNAM WAR 
                                  
                              
                               
                                   UNASSIGNED CREDIT (UPPER DIVISION) 
                                  
                              
                            
                           
                            
                              
                               
                                  ART OF THE WESTERN WORLD 
                                  
                              
                               
                                   UNASSIGNED CREDIT 
                                  
                              
                            
                           
                            
                              
                               
                                  CRIMINAL JUSTICE 
                                  
                              
                               
                                   CJUS 1100 
                                  
                              
                            
                           
                            
                              
                               
                                  ETHICS IN AMERICA 
                                  
                              
                               
                                   UNASSIGNED CREDIT  
                                  
                              
                            
                           
                            
                              
                               
                                  FOUNDATIONS OF EDUCATION 
                                  
                              
                               
                                   UNASSIGNED CREDIT 
                                  
                              
                            
                           
                            
                              
                               
                                  FUNDAMENTALS OF COUNSELING 
                                  
                              
                               
                                   UNASSIGNED CREDIT 
                                  
                              
                            
                           
                            
                              
                               
                                  GENERAL ANTHROPOLOGY 
                                  
                              
                               
                                   UNASSIGNED CREDIT 
                                  
                              
                            
                           
                            
                              
                               
                                  HUMAN/CULTURAL GEOGRAPHY 
                                  
                              
                               
                                   UNASSIGNED CREDIT 
                                  
                              
                            
                           
                            
                              
                               
                                  INTRO. TO LAW ENFORCEMENT 
                                  
                              
                               
                                   UNASSIGNED CREDIT (UPPER DIVISION) 
                                  
                              
                            
                           
                            
                              
                               
                                  INTRO. TO WORLD RELIGIONS 
                                  
                              
                               
                                   RLGN 1100  
                                  
                              
                            
                           
                            
                              
                               
                                  LIFESPAN DEVELOPMENTAL PSYCHOLOGY 
                                  
                              
                               
                                   UNASSIGNED CREDIT 
                                  
                              
                            
                           
                            
                              
                               
                                  PRINCIPLES OF PUBLIC SPEAKING 
                                  
                              
                               
                                   UNASSIGNED CREDIT 
                                  
                              
                            
                           
                            
                              
                               
                                  HISTORY OF THE SOVIET UNION (FORMERLY RISE AND FALL OF SOVIET UNION) 
                                  
                              
                               
                                   UNASSIGNED CREDIT (UPPER DIVISION) 
                                  
                              
                            
                           
                            
                              
                               
                                  SUBSTANCE ABUSE 
                                  
                              
                               
                                   UNASSIGNED CREDIT (UPPER DIVISION) 
                                  
                              
                            
                           
                            
                              
                               
                                  TECHNICAL WRITING 
                                  
                              
                               
                                   UNASSIGNED CREDIT 
                                  
                              
                            
                           
                            
                              
                               
                                  THE CIVIL WAR AND RECONSTRUCTION 
                                  
                              
                               
                                   HIST UD01 (UPPER DIVISION) 
                                  
                              
                            
                           
                            
                              
                               
                                  BUSINESS ETHICS AND SOCIETY 
                                  
                              
                               
                                   UNASSIGNED CREDIT  
                                  
                              
                            
                           
                            
                              
                               
                                  BUSINESS MATHEMATICS 
                                  
                              
                               
                                   UNASSIGNED CREDIT 
                                  
                              
                            
                           
                            
                              
                               
                                  INTRO. TO BUSINESS 
                                  
                              
                               
                                   MGMT 1010 
                                  
                              
                            
                           
                            
                              
                               
                                  COMPUTING and INFO TECHNOLOGY 
                                  
                              
                               
                                   TECH 101 
                                  
                              
                            
                           
                            
                              
                               
                                  FUNDAMENTALS OF CYBER SECURITY 
                                  
                              
                               
                                  UNASSIGNED CREDIT (UPPER DIVISION) 
                                  
                              
                            
                           
                            
                              
                               
                                  MANAGEMENT INFORMATION SYSTEMS 
                                  
                              
                               
                                   UNASSIGNED CREDIT 
                                  
                              
                            
                           
                            
                              
                               
                                  MONEY AND BANKING 
                                  
                              
                               
                                   ECON 3610  
                                  
                              
                            
                           
                            
                              
                               
                                  ORGANIZATIONAL BEHAVIOR 
                                  
                              
                               
                                   MGMT 3110 (LOWER DIVISION) 
                                  
                              
                            
                           
                            
                              
                               
                                  PERSONAL FINANCE 
                                  
                              
                               
                                   FIR 2220 
                                  
                              
                            
                           
                            
                              
                               
                                  HUMAN RESOURCE MANAGEMENT 
                                  
                              
                               
                                   UNASSIGNED CREDIT 
                                  
                              
                            
                           
                            
                              
                               
                                  PRINCIPLES OF FINANCE 
                                  
                              
                               
                                   FIR 3410 (LOWER DIVISION) 
                                  
                              
                            
                           
                            
                              
                               
                                  PRINCIPLES OF SUPERVISION 
                                  
                              
                               
                                   TECH 4381 (LOWER DIVISION) 
                                  
                              
                            
                           
                            
                              
                               
                                  ASTRONOMY  
                                  
                              
                               
                                   PHYS 1020 
                                  
                              
                            
                           
                            
                              
                               
                                  ENVIRONMENTAL SCIENCE   
                                  
                              
                               
                                   UNASSIGNED CREDIT 
                                  
                              
                            
                           
                            
                              
                               
                                  FUNDAMENTALS OF COLLEGE ALGEBRA 
                                  
                              
                               
                                   MATH 1710 
                                  
                              
                            
                           
                            
                              
                               
                                  HEALTH AND HUMAN DEVELOPMENT (FORMERLY HERE'S TO YOUR HEALTH) 
                                  
                              
                               
                                   HMSE UNASSIGNED CREDIT 
                                  
                              
                            
                           
                            
                              
                               
                                  MATH FOR LIBERAL ARTS 
                                  
                              
                               
                                  UNASSIGNED CREDIT 
                                  
                              
                            
                           
                            
                              
                               
                                  PRINCIPLES OF PHYSICAL SCIENCE I 
                                  
                              
                               
                                   UNASSIGNED CREDIT 
                                  
                              
                            
                           
                            
                              
                               
                                  PRINCIPLES OF STATISTICS 
                                  
                              
                               
                                   UNASSIGNED CREDIT 
                                  
                              
                            
                           
                            
                              
                               
                                  PRINCIPLES OF ADVANCED ENGLISH COMP 
                                  
                              
                               
                                  UNASSIGNED CREDIT 
                                  
                              
                            
                           
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Services 
                         
                            
                               
                                ACT Residual  
                                ACCUPLACER  
                                CLEP  
                                DSST  
                                Educational Testing Service  
                                Miller Analogies Test  
                                Oral Proficiency Interview  
                                Para Pro Assessment  
                                Proctor Services  
                                TEAS  
                            
                         
                      
                      
                      
                         
                            
                                Testing Locations  
                               Find out where we are located. 
                            
                            
                                Proctor Services  
                               For outside schools or institutions that offer online or correspondence courses. 
                            
                            
                                Parking Information  
                               Find out where to park when you visit us. 
                            
                            
                                Contact Us  
                               Questions? Our team is here to help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/1/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


