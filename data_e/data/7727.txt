Recommended Reading and Sources Consulted - University Libraries' Instructional Services - LibGuides at University of Memphis Libraries   
 
	 
         
         
         
        
		 Recommended Reading and Sources Consulted - University Libraries' Instructional Services - LibGuides at University of Memphis Libraries 
		 
		 
 
 
 
 
 
 
 
 
 
 
 

		 
		 
		 
		 
		 
		 
         
         
				 
				 
             
         
        
        
            
		
		 
		
		 
		 
		
		 
		
		
	   
		         
        	 
     
	 
		 Skip to main content                  
                 
         
  		 
             
                
			 
				
					 
						 University of Memphis Libraries
						 
					 
					 
						 LibGuides
						 
					 
					 
						 University Libraries' Instructional Services
						 
					 
					 Recommended Reading and Sources Consulted
					 
			              
			 
				 
					
                 
                     
                         
                             
                             Search this Guide  
                             
                                 Search 
                             
                         
                     
                 				 
							 
             
                 University Libraries' Instructional Services 
                 
                     Learn more about the Instructional Services department: our services, policies, and long-term goals and plans. 
                 
             
         
         
          
         
             
                 
                     
                         
                             
                 
                     
                         Services 
                        
                     
                 
                 
                     
                         Policies 
                        
                     
                 
                     
                         
                             Instructional Services Plan 2017 - 2021 
                            
                         
                         
                             
                                 Introduction and Purpose of Instructional Plan
                                    
                                 
                             
                             
                                 Departmental Priorities
                                    
                                 
                             
                             
                                 Modes of Instruction
                                    
                                 
                             
                             
                                 Instruction Guidelines
                                    
                                 
                             
                             
                                 Recommended Reading and Sources Consulted
                                    
                                 
                              
                      
                            
                 
                     
                         
                        
                         
                     
                                          
                     
                 
                 
                     
                         
                             
     
                  
					 
						 
							
							 
								 
									
			 
				     Recommended Reading and Sources Consulted   

 ACRL. (2016). Framework for Information Literacy for Higher Education. 

 Appalachian State University. (2009). Library Instruction Plan.  

 Benson, D.   DeSanto, D. (2016). The 360  feedback model for library instructors: Observing, teaching, reflecting, adapting.  College   

    Research Libraries, 77 (9), 448-451. 

 Bizup, J. (2008). BEAM: A rhetorical vocabulary for teaching research-based writing.  Rhetoric Review. Communication   Mass   

  Media Complete, 27 (1), 72-86. 

 Greer, K., Hess, A. N.,   Kraemer, E. W. (2016). The librarian leading the machine: A reassessment of library instruction methods. 

  College   Research Libraries, 77 (3), 286-301. doi:10.5860/crl.77.3.286 

 Meyer, J.   Land, R. (2003).  Threshold Concepts and Troublesome Knowledge: Linkages to Ways of Thinking and Practicing within   

  the Disciplines . Edinburgh, UK: University of Edinburgh. 

  Moore, J. L. (2012). Designing for Transfer: A Threshold Concept.    Journal Of Faculty Development   ,    26   (3), 19-24.  

    Riehle  , C. F.,   Weiner, S. A. (2013). High-Impact Educational Practices: An Exploration of the Role of Information Literacy.     College       

   Undergraduate Libraries   ,    20   (2), 127-143. doi:10.1080/10691316.2013.789658  

       Townsend, L.,   Brunetti  , K.,     Hofer  , A. R. (2011). Threshold Concepts and Information Literacy.     Portal: Libraries And The Academy    , (3), 853.     
 
University Libraries. (2013). University Libraries, University of Memphis Strategic Plan 2013 ‐ 2018. 

 Wiggins, G. P.,   McTighe, J. (2005).  Understanding by Design . Alexandria, VA: Assoc. for Supervision and Curriculum 

 Development. 
		    
								 
								
							 
						 
					  
         
     
                          
                     
                    
                 
                     
                         Previous:  Instruction Guidelines 
                     
                     
                    
                     
                                  
             
         
         
         
             
                 
                     
                         Last Updated:   Dec 6, 2017 4:42 PM                      
                     
                         URL:   http://libguides.memphis.edu/instruction                      
                     
                    	    Print Page                      	
                     
                 
                     Login to LibApps                  
             
             
                 
                     Report a problem  
                 
                 
                    
                     Subjects:  
                      Help/How-To Guides ,  Library and Information Science  
                                     
                 
                    
                     Tags:  
                      composition ,  information literacy ,  instruction & curriculum leadership ,  learning  
                                     
             
         
         
        
			 
				 
					 
					    
					    
					 
				 
			          
                              
                                
                 
                

		 
  	 
 
