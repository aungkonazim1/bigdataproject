Julia Reed, Stanton Thomas, and Melissa Hall speaking at 2014 Delta Symposium! | University of Memphis Libraries   
 
 
 
 
 Julia Reed, Stanton Thomas, and Melissa Hall speaking at 2014 Delta Symposium! | University of Memphis Libraries 
 
 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 
 




 
 
  
 
 
 

 
 
 
 
			

         



 

 
 
	 Skip to content 
		 
				 
						  University of Memphis Libraries  
			 News and Events 
		 

		 
			 Menu 
			  
  Home    
		  
	  

	 
			 
			 Home  Julia Reed, Stanton Thomas, and Melissa Hall speaking at 2014 Delta Symposium! 		 
		 
		 

					 
				
 
	 
					 Julia Reed, Stanton Thomas, and Melissa Hall speaking at 2014 Delta Symposium! 
		
		 
						   April 23, 2014       jschnbel   
			  Events ,  News ,  Special Collections  		  
	  

	 
		                 
 Now in its ninth year, The Delta Symposium has drawn hundreds of people for the day-long celebration of the  history, culture, and life of the Delta region .  This year’s Delta is themed  “Two Sides of the River,” and will be held on Thursday, June 5, 2014, at the  Fogelman Executive Center and Holiday Inn Ballroom  on the University of Memphis campus. 
 The Delta 2014, hosted by writer and filmmaker Willy Bearden, includes an outstanding lineup of  speakers  and entertainment.  The morning will begin with a continental breakfast, followed by presentations by Brooks Museum of Memphis curator Dr. Stanton Thomas on “Carroll Cloar’s Delta,” renowned author Julia Reed on “A Taste of the Delta” and Southern Foodways Alliance assistant director  Melissa Hall on “Lebanese Culture in the Delta.”  The buffet luncheon at the Holiday Inn will include humorous entertainment by John Pritchard (a shuttle service will be available between Fogelman and the Holiday Inn).  The afternoon comprised of panel sessions held in Fogelman allowing for close interaction with our speakers will continue with presentations on the blues, genealogy, catfish farming, and more. 
 Every year, The Delta helps to raise funds for the  University Libraries  and to support our many educational exhibitions and events.  Early bird registration ($65) is available online until May 6; regular registration ($75) is available online until May 30.   Reserve your place today!  
 The Delta has its own  Facebook  page! Visit us for updated information about the symposium, videos from past conferences,  and stunning photos from the Delta region! 

 
			  

	 
			  
  
			 

				 
		 Post navigation 

	
		      Celebrate  Inspiring Women of the 1960s  on March 18  		  Registration going strong for The Delta! Don t miss out!      
	
	  
	
			
 

	
	
	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  

		
		  
	  

					 
					 		 Recent Posts 		 
					 
				 3D Printing Workshop 
						 
					 
				 NedXStudents 
						 
					 
				 Craft-n-Chat 
						 
					 
				 Bridging East and West: The First Steel Bridge of Memphis 
						 
					 
				 Banned Books Display 
						 
				 
		 		  Archives 		 
			  November 2017  
	  October 2017  
	  September 2017  
	  August 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  April 2016  
	  February 2016  
	  September 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  May 2014  
	  April 2014  
	  March 2014  
	  February 2014  
	  January 2014  
		 
		   Categories 		 
	  Alerts 
 
	  Branch Libraries 
 
	  Events 
 
	  Exhibition 
 
	  Homepage 
 
	  News 
 
	  Research and Instructional Services 
 
	  Special Collections 
 
	  Trial Resources 
 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 		  
	
	  

	 
		 
			 
								 Proudly powered by WordPress 
				  |  
				Theme: Big Brother by  WordPress.com .			  
					  
	  
  


 

        

        			 
				   Subscribe  
				 

					
						 Follow this blog 

						 
							
															 Get every new post delivered right to your inbox. 
							
							 
								 
							 
							
							 
							 
							
							  							   
						 

					
				 
			 
		







		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 