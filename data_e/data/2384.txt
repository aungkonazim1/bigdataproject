2015 Posters - MemphisCRESH - University of Memphis    










 
 
 
     



 
    
    
    2015 Posters - 
      	MemphisCRESH
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			MemphisCRESH
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About CRESH  
                          Who Can Apply  
                          Frequent Questions  
                          Sponsors  
                      
                      
                         
                            Student Research Posters and Work   
                            
                               
                                  
                                   Award Recipients  
                                   2017 Posters  
                                   2015 Posters  
                                   2015 Pictures  
                                   2014 Posters  
                                   2013 Posters  
                                   2012 Posters  
                                   Photo Gallery  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	MemphisCRESH
                              	  
                          
                              	Student Research Posters and Work
                              	  
                         2015 Posters 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      2015 Posters 
                     
                      MemphisCRESH 2015 Student Posters 
                     
                      Congratulations to the Award Winning Student Posters for 2015: 
                     
                      
                        
                         1st Place -  Tony Chen  
                        
                         2nd Place -  Rishab Jain  
                        
                         3rd Place -  Sarah Oh  
                        
                      
                     
                      
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                        
                                 
                                   Tony Chen , Lausanne Collegiate School
                                  
                                 
                               
                              
                                 
                              
                               
                                 
                                        
                                 
                                   Andrei Fendley , Germantown High School    Kian Ziai  (Arlington High School)
                                  
                                 
                               
                              
                                 
                              
                               
                                 
                                      
                                 
                                   Rashab Jain , Lausanne Collegiate School
                                  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                      
                                 
                                   Markayla Key , Southwind High School, **Recipient of TN SCORE Scholarship**
                                  
                                 
                               
                              
                                 
                              
                               
                                 
                                      
                                 
                                   Raghav Ranga,  Lausanne Collegiate School
                                  
                                 
                                    
                                 
                               
                              
                                 
                              
                               
                                 
                                       
                                 
                                   Paul Rozzi,  Lausanne Collegiate School
                                  
                                 
                               
                              
                            
                           
                            
                              
                                 
                              
                                 
                              
                                 
                              
                                 
                              
                                 
                              
                            
                           
                            
                              
                               
                                 
                                      
                                 
                                   Swarna Sakshi , St. Mary's Episcopal School
                                  
                                 
                               
                              
                                 
                              
                               
                                 
                                      
                                 
                                   Elijah Tyler , Freedom Preparatory School, **Recipient of TN SCORE Scholarship**
                                  
                                 
                               
                              
                                 
                              
                                 
                              
                            
                           
                         
                        
                      
                     
                        
                     
                       Saatvik Mohan  - Contains Unpublished Proprietary Material (cannot be displayed at this time)
                      
                     
                       Sarah Oh  - Contains Unpublished Proprietary Material (cannot be displayed at this time)
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Student Research Posters and Work 
                         
                            
                               
                                Award Recipients  
                                2017 Posters  
                                2015 Posters  
                                2015 Pictures  
                                2014 Posters  
                                2013 Posters  
                                2012 Posters  
                                Photo Gallery  
                            
                         
                      
                      
                      
                         
                            
                                Student Research Posters and Work  
                               From cancer research, to solar energy, to materials for space applications, view examples
                                 of previous years of student research posters and work.
                               
                            
                            
                                Forms and Materials  
                               Application, Financial Aid and Teacher Endorsements forms 
                            
                            
                                Department of Physics and Materials Science  
                               MemphisCRESH is an outreach program through the Department of Physics and Materials
                                 Science
                               
                            
                            
                                Contact Us  
                               Main contact information 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


