Insights Portal | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Insights Portal | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Insights    »    Using Insights Portal   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Insights Portal                       
        
        
       
          
     
           Printer-friendly version       
	The Insights Portal displays reports from the Insights environment directly inside Learning Environment. This integration provides several advantages over accessing the Insights Console directly:
 

  
		It allows instructors to access Insights reports for their specific courses.
	 
	 
		It utilizes the security of Learning Environment to access Insights.
	 
	 
		It incorporates Learning Environment-style navigation and filtering controls that are more familiar to instructors and administrators.
	 
	 
		Administrators can control and customize the portal to determine which reports are available to specific roles and/or specific users.
	 
  
	The Insights Portal page displays the list of reports that are available according to a particular role (for example, an administrator or an instructor), and/or specific users within Learning Environment. From this page, you can:
 

  
		View the report list.
	 
	 
		Add a  New Report .
	 
	 
		Edit portal reports.
	 
	 
		 Manage   Categories .
	 
	 
		Run portal reports.
	 
	 
		 Search For  portal reports.
	 
      Audience:    Instructor      

    
           

                   ‹ Using Insights Portal 
        
                   up 
        
                   Adding Insights Portal to a navbar › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Insights   
  
      Using Insights Portal    Insights Portal    Adding Insights Portal to a navbar    Accessing Insights Portal     Adding and editing custom reports in Insights Portal    Running reports in Insights Portal    Running searches in Insights Portal    Deleting custom reports in Insights Portal      Standard reports    Data Mining reports    Understanding the Student Success System    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
