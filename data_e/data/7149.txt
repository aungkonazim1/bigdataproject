Inserting CaptureCast presentations into Learning Environment | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 Inserting CaptureCast presentations into Learning Environment | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Capture    »    Capture Central in Learning Environment   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Inserting CaptureCast presentations into Learning Environment                       
        
        
       
          
     
           Printer-friendly version       
	 Tip  If you want to include a CaptureCast presentation as a topic in Content, create a    New Document  in the Content tool and enter a title that indicates it is a CaptureCast presentation topic.
 

  
		In the HTML Editor where you want to insert your CaptureCast presentation, click    Insert Stuff .
	 
	 
		Click    Capture  in the sidebar.
	 
	 
		Click  Search  to view a list of all presentations you can insert into the course. If available, you will see the presentation name, its published date or presentation date and time, the presenter's name, and the tags added to the presentation.
		 
			 Note Contact your administrator if you do not see your presentation in the list of presentations available.
		 
	 
	 
		Select the presentation you want to insert, and click  Next .
	 
	 
		Click the    Play  icon to preview the presentation. Select the  Auto Start  check box if you want the video to play automatically after the page loads for users.
	 
	 
		Click  Insert .
	 
	 
		Save your changes.
	 
      Audience:    Instructor      

    
           

                   ‹ Configuring CaptureCast presentations with Learning Environment 
        
                   up 
        
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Capture  
  
      Participating in CaptureCast presentations    Understanding Capture components    Using Capture    Capture Station    Editing in post-production    Capture Central in Learning Environment    Accessing Capture Central    Sharing CaptureCast presentations from Capture Portal or Capture Central    Configuring CaptureCast presentations with Learning Environment    Inserting CaptureCast presentations into Learning Environment      
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
