Student Employment - Financial Aid - University of Memphis    










 
 
 
     



 
    
    
    Student Employment - 
      	Financial Aid
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Financial Aid
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Undergraduate  
                          Graduate  
                          Law  
                          Scholarships  
                          Student Employment  
                      
                      
                         
                            Student Employment   
                            
                               
                                  
                                   Pay Ranges/Payroll Dates  
                                   Forms and Handbooks  
                                   Regular Student Employment (RSE)  
                                   Federal Work-Study (FWS)  
                                   International Students  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Financial Aid
                              	  
                         
                           	Student Employment
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      News and Important Information 
                     
                       **News Updates  
                     
                      Summer Student Employment Processing 
                     
                      We’ve already been getting many questions about Summer and Fall employment. We hope
                        the information listed below will help. If not, feel free to e-mail us at  StuEmp@memphis.edu .
                      
                     
                      
                        
                           Forms should be submitted at least five (5) business days prior to start date.   
                        
                         Students do not need to be enrolled for the Summer to work under Regular Student Employment
                           (RSE);  however , they need to have maintained at least half time status for Spring 2017 and be enrolled
                           again at least half time for the upcoming Fall semester.   Please do NOT submit paperwork if student is not yet enrolled for the Fall .  Also:
                           
                            
                              
                               Notify student that FICA taxes will be withheld if they are not enrolled at least
                                 half time in the Summer.
                               
                              
                               Student is responsible for obtaining a Summer Parking hangtag to avoid parking tickets. 
                              
                            
                           
                         
                        
                         If employing for both the  Summer and Fall semesters under RSE , complete  one  RSE Payroll Action Form with employment dates inclusive of  5/13/2017  through  12/22/2017 .
                         
                        
                           There is no Federal Work Study (FWS) available for Summer  , so  if RSE for the Summer only , list employment dates from  5/13/2017  through  8/18/2017 .
                         
                        
                         If RSE in the Summer, then  switching to FWS for Fall , fill out a FWS Payroll Action Form to pick up from  8/19/2017  through  12/22/2017 .
                         
                        
                         If student is  graduating at the end of the Summer  semester, they have to be enrolled in at least half time in the Summer. Their employment
                           dates will be from  5/13/2017  through  8/4/2017 . If student cannot meet enrollment requirement, please contact our Human Resources
                           Department at (901) 678-3573 to hire student as a Temporary Employee.
                         
                        
                         If  new student starting in the Fall  (i.e., never enrolled here prior to Fall 2017), the first day student can work is
                           Monday,  8/28/2017  and can go through  12/22/2017 . These would be incoming freshmen, new graduate, transfer students, etc.
                         
                        
                          We will begin posting FWS awards for 2017-2018 once Spring 2017 grades are posted .
                         
                        
                         The  Student Employee Request Form  has been updated to add a job posting and to remove from the website.
                         
                        
                      
                     
                      STUDENT EMPLOYMENT GENERAL INFORMATION 
                     
                      The University of Memphis offers two types of on-campus student employment: FEDERAL
                        WORK-STUDY (FWS) and REGULAR STUDENT EMPLOYMENT (RSE). You cannot be employed under
                        both programs simultaneously. Since there are differences in the two programs, it
                        is important that you know under which program you are employed, and what your responsibilities
                        will be.
                      
                     
                      Federal Work-Study (FWS) 
                     
                      FWS is a need based Financial Aid award. It is a federally funded, university administered
                        program, designed to help students defray the costs of post-secondary education.  A FWS AWARD CANNOT BE USED TO MEET REGISTRATION FEES. You are paid for hours worked,
                        based on time submitted by your department through the University payroll system.
                        You are eligible to work up to the point that your cumulative earnings equal the amount
                        of your FWS award. It is the responsibility of both the student and the hiring department
                        to monitor earnings to prevent working beyond the awarded amount.
                      
                     
                      PLEASE NOTE: 
                     
                      ALL FWS RECIPIENTS MUST BE ENROLLED, AND REMAIN ENROLLED AT LEAST PART-TIME (6 CREDIT
                        HOURS-UNDERGRADUATE/LAW, OR 5 CREDIT HOURS-GRADUATE) FOR FALL AND SPRING TERM. You
                        must submit the results of the Free Application for Federal Student Aid (FAFSA). 
                        To apply, go to  www.fafsa.gov .  Additional documentation may be required by the Financial Aid office.
                      
                     
                      Listed below are answers to frequently asked questions regarding the FWS Program.
                        If you have additional questions, please contact the Student Employment Office via
                        email at  StuEmp@memphis.edu .
                      
                     
                      
                     
                      How do I apply? 
                     
                      You must demonstrate financial eligibility, or "need" by completing a  Free Application for Federal Student Aid (FAFSA)  prior to January 1 for the following academic year since FWS funds are limited. You
                        must be a US citizen or permanent resident to be eligible.
                      
                     
                      Where will I be working? 
                     
                      Various departments hire FWS recipients. Actual placement depends on your skills and
                        interests, and the needs of the departments. Job responsibilities can range from general
                        office duties to highly specialized laboratory and computer work. If you have not
                        been awarded FWS but are interested in the program, contact the Financial Aid Office to
                        be reviewed for eligibility.
                      
                     
                      What is the job placement procedure? 
                     
                      If you have been awarded FWS, you may check the current jobs under  Federal Work-Study . You may also check with departments individually. If you and the prospective employer
                        agree on employment, you and the hiring department must complete/submit the following
                        (see the  Student Employment Forms  page):
                      
                     
                      
                        
                         Payroll Action Form 
                        
                         Direct Deposit Authorization 
                        
                         W-4 Employee's Withholding Allowance Form 
                        
                         I-9 Employment Eligibility Form (with a photocopy of an unexpired state or federal
                           form of identification)
                         
                        
                      
                     
                      Once approved, you may begin work. 
                     
                      I just got admitted to this university, when can I start working? 
                     
                      If you have just been admitted, you cannot start working until the first day of classes for
                        the semester you are admitted to.
                      
                     
                      How many hours a week do I work? 
                     
                      You may work up to 25 hours a week. You may not exceed the number of hours of employment
                        for which you are approved. Also, it is up to you and the department to ensure that
                        your work schedule does not interfere with your class schedule. All scheduling arrangements
                        should be made with your employer.
                      
                     
                      You are terminated when earnings reach the FWS award amount, or if you drop below
                        half-time enrollment, withdraw, or graduate. Employment is automatically terminated
                        at the end of each semester.
                      
                     
                      Where do I get my check? 
                     
                      You are paid bi-weekly at an hourly rate. Payment is made every other Friday and direct
                        deposited into your bank account. If you are a new student employee, the first paycheck
                        will need to be picked up at the Bursar's Office, Wilder Tower, Room 115.
                      
                     
                      Once I have a FWS job, must I reapply each year? 
                     
                      Yes. A FWS award is made for one academic year only, and you must submit the Free
                        Application for Federal Student Aid, and other required documents each year to be
                        awarded.
                      
                     
                      Can I have more than one job? 
                     
                      No. You can only work one job under the FWS program, and cannot work a Regular Student
                        Employment job or Graduate Assistant job along with a FWS job.
                      
                     
                      
                     
                      Regular Student Employment 
                     
                      The Regular Student Employment (RSE) program offers on-campus, part-time jobs to students
                        who do not qualify for Federal Work-Study. The program is monitored by the university.
                      
                     
                      PLEASE NOTE: 
                     
                      YOU MUST BE ENROLLED, AND REMAIN ENROLLED AT LEAST PART-TIME (6 CREDIT HOURS-UNDERGRADUATE/LAW
                        or 5 CREDIT HOURS-GRADUATE) FOR THE FALL AND SPRING SEMESTERS. For the Summer term
                        under RSE, you do not have to be enrolled as long as you were enrolled at least half-time
                        for the Spring semester and are already enrolled at least half-time in the Fall semester
                        at this university.    Effective the Summer 2016 semester, students who are not enrolled at least half time
                              will be subject to FICA tax withholdings.  Half time enrollment for summer is defined
                              as:  6 credit hours for undergraduates; 5 hours for graduate, and 3 for law.   
                     
                       INTERNATIONAL STUDENTS ON F-1 VISAS MUST BE ENROLLED AT LEAST FULL-TIME (12 CREDIT
                        HOURS-UNDERGRADUATE/LAW, OR 9 CREDIT HOURS-GRADUATE) FOR THE FALL AND SPRING SEMESTERS.
                        The exceptions to less than full-time enrollment for international students on F-1
                        visas are: (1) if it is your last semester of enrollment, or you are working on your
                        phase of dissertation or thesis, then you must be enrolled at least part-time; (2)
                        if it is due to special circumstances (i.e. medical), then you will have to provide
                        in writing to the Student Employment Office the reason(s) and it must be approved
                        by the Financial Aid Director; (3) if it is during the Summer term and school breaks
                        between semesters (Winter break, Spring break), no enrollment is required. Concurrent
                        enrollment at this university and a community college, college, and another university
                        is not acceptable for enrollment requirements for employment eligibility at this university.
                      
                     
                      INTERNATIONAL STUDENTS ON F-1 VISAS MUST OBTAIN AND USE A VALID SSN IN THE BANNER
                        STUDENT SYSTEM IN ORDER TO BE ELIGIBLE FOR EMPLOYMENT ON-CAMPUS.  To obtain a valid
                        Federal social security number, you must go to the Social Security Administration
                        Office located at 1330 Monroe Avenue. You must bring with you your I-20 and Passport
                        to show them, and proof of employment on-campus. Once you obtain a valid Federal social
                        security number, go the Registrar's Office in Wilder Tower, Room 003, and request
                        that they add your social security number to your academic record. Contact Student
                        Employment once your SSN has been added to the Banner Student System.
                      
                     
                      Listed below are answers to frequently asked questions regarding RSE. If you have
                        additional questions, please contact the Student Employment Office.
                      
                     
                      
                     
                       How can I obtain a RSE position?  
                     
                      The availability of jobs depends on student skills and the needs of campus departments. Job
                        responsibilities can range from general office duties to highly specialized laboratory
                        and computer work. Current jobs are listed under  Regular Student Employment . NOTE: Some departments also list job opportunities on their own web site. Contact
                        the department for a job interview. If you and the prospective employer agree on employment,
                        you and the department must complete all required forms. Once approved, you may begin
                        work.
                      
                     
                       I just got admitted to this university, when can I start working?  
                     
                      If you have just been admitted, you cannot start working until the first day of classes for
                        the semester you are admitted to.
                      
                     
                       How many hours may I work?  
                     
                      You may work up to 25 hours a week, unless a limit has been placed on your hours due
                        to visa restrictions. Also, it is up to you and the department to ensure that your
                        work schedule does not interfere with your class schedule. All scheduling arrangements
                        should be made with your employer.
                      
                     
                      You are automatically terminated at the end of each semester, or if you drop below
                        half-time enrollment, withdraw, or graduate.
                      
                     
                      Note: According to INS, if you are an international student on F-1 visa and enrolled
                        at least full-time, you may engage in part-time on-campus employment, but not to exceed
                        20 hours per week, which includes all pay sources. However, you may be employed on-campus
                        full-time, not to exceed 25 hours a week, which includes all pay sources, during school
                        breaks (winter break, Spring break, and Summer term), your last semester before graduating,
                        or during your thesis or dissertation phase of study, or you have special permission
                        for full-time employment as part of a coop or internship program. The Student Employment
                        Office will need a copy of this special permission for your employment file. If you
                        have questions regarding employment visa restrictions, contact your International
                        Students Advisor, Wilder Tower, Room 204, at (901) 678-1765.
                      
                     
                       Where do I get my check?  
                     
                      You are paid bi-weekly at an hourly rate. Payment is made every other Friday and direct
                        deposited into your bank account. If you are a new student employee, the first paycheck
                        will need to be picked up at the Bursar's Office, Wilder Tower, Room 115.
                      
                     
                       Can I have more than one job?  
                     
                      Yes. You may have more than one job under RSE; however, you and your department must complete
                        Payroll Action Forms for each job, and you may not exceed a total of 25 hours per
                        week. If you are already employed as a graduate assistant, then you cannot work under
                        RSE at the same time.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Student Employment 
                         
                            
                               
                                Pay Ranges/Payroll Dates  
                                Forms and Handbooks  
                                Regular Student Employment (RSE)  
                                Federal Work-Study (FWS)  
                                International Students  
                            
                         
                      
                      
                      
                         
                            
                                Financial Aid Forms  
                               Manage your forms in myMemphis 
                            
                            
                                Financial Aid Checklist  
                               Step-by-step guide to receiving financial aid 
                            
                            
                                Frequently Asked Questions  
                               Answers to the most common questions 
                            
                            
                                Contact Us  
                               Phone: 901.678.4825  |  financialaid@memphis.edu 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


