Earthworm Modules: Tankplayer commands   
 
 
   Earthworm Modules: Tankplayer commands 
 
 
 
 Tankplayer Configuration File Commands 
 (last revised April 24, 2010)  
Page Index: 
1.  Example configuration file  
2.  Functional command listing  
3.  Alphabetic command listing   description 
  Tankplayer
reads file(s) containing waveform messages (of either
TYPE_ADBUF,
TYPE_TRACEBUF, or TYPE_TRACEBUF2) from a single data source. It places
the
waveform messages
into shared memory in simulated real time, using the delta-t between
time-stamps in successive message headers to determine its timing.
On startup, tankplayer reads the configuration file named on the
command
line. Commands in this file set up all parameters used in playing back
waveform messages from a file to a shared memory ring. In the control
file, lines may begin with a valid tankplayer command (listed below) or
with one of 2 special characters:
 
 
   #  marks the line as a comment (example: # This is a comment).     @  allows control files to be nested; one control file can be     accessed from another with the command "@" followed by     a string representing the path name of the next control file     (example: @model.d).  
 
Command names must be typed in the control file exactly as shown in
this
document (upper/lower case matters!).
    
  1. EXAMPLE CONFIGURATION FILE  
 
 
  # tankplayer config file for playing TYPE_TRACEBUF2 waveforms # tankplayer config file for playing waveform files  RingName      WAVE_RING        # play waveforms into this ring MyModuleId    MOD_ADSEND_A     # as this module id PlayMsgType   TYPE_TRACEBUF2   # msg type to read from file LogFile       1                # 0=no log; 1=keep log file HeartBeatInt  30               # seconds between heartbeats Pause         10               # seconds to pause between wavefiles StartUpDelay  10               # seconds to wait before playing 1st file ScreenMsg     1                # (optional) if non-zero, informational messages will be                                #   written to the screen as data is played # SendLate      10.0           # (optional) if present, packets will be                                #   timestamped this many seconds before                                #   current time;                                # if absent, packets will have original time                                #   stamps                                # Debug         1                # for verbosity  # List of files to play (up to 50 allowed): WaveFile      e:\QAnew\900819a.ew1.tbuf  # or you could use a GetFromDir in lieu of WaveFile entires: (new in 2008-09-09) #GetFromDir /home/paulf/memphis/params/live_tank # where to get tank files from (all must be the same format) #OpenWait 200    # wait time between tries in msecs #OpenTries 5     # number of retry times for copies in of files ot GetFrom Dir #CheckPeriod 1   # number of seconds to check the dir and then sleep #SaveDataFiles 1 # set to 0 to have tanks deleted  # IgnoreTBVersionNumbers - # Prevents tankplayer from objecting to tracebuf2 packets that don't have # the correct version field.  Not recommended. #IgnoreTBVersionNumbers 0   
 
   
  2. FUNCTIONAL COMMAND LISTING  
   
Below are the commands recognized by tankplayer, grouped by the
function they influence. All of the commands are required; they must be
specified in the control file in order for tankplayer to operate.
    
  	Earthworm system setup: 		  HeartBeatInt  	 	required  		 MyModuleId 	   	required 		 RingName 	   	required  	Waveform Playback: 		 Pause 			required 		 PlayMsgType 		required 		 StartUpDelay 		required 		 SendLate 		optional 		 WaveFile 		required 		 IgnoreTBVersionNumbers 	optional 
		 GetFromDir 		optional 
		 InterMessageDelayMillisecs 		optional 
		 OpenWait 		optional 
		 OpenTries 		optional 
		 CheckPeriod 		optional 
		 SaveDataFiles 		optional 
 	Output Control: 		 LogFile 		   	required   
 
 
  3. ALPHABETIC COMMAND LISTING   DESCRIPTION  
 
 In the following section, all configuration file
commands are listed in alphabetical order. Listed along with the
command (bold-type) are its arguments (in red), the name of the
subroutine that processes the command, and the function within the
module that the command influences. A detailed description of the
command and is also given. Default values and the values used by Calnet
are listed after each command description.
  
    command  arg1 				processed by		function    
 
   CheckPeriod  n  			tankplayer_config	Playback  		    
 Defines the  n  seconds delay that the
GetFromDir should be checked for new tank files. Note that this only
effects checks when no tanks are being played back. 
 
 
 Default:  1				 
 
  

 
   GetFromDir  dir  			tankplayer_config	Playback  		  
 
 Defines the directory   dir  where to find tank files. This cannot be used
in conjunction with WaveFile mode of running. If you set this
parameter, then you may also set the  OpenWait ,
 OpenTries ,  CheckPeriod ,
and  SaveDataFiles .  
 
 Default:  none				 
 

 
   InterMessageDelayMillisecs  n_msecs  			tankplayer_config	Playback  		  
 
 Defines the speed with which to release tracebufs  in units of milliseconds  n_msecs  This should be used with caution since it speeds up the playback and can overwhelm some modules if buffers are not sufficient sizes and CPU speed is not fast enough. Some experimentation is required to use this option for rapid playback.
 
 Default:  none				 
 

 
 
   HeartBeatInt  nsec  			tankplayer_config	Earthworm setup  		  
    
   
Defines the number of seconds  nsec  between
TYPE_HEARTBEAT messages
issued by tankplayer.  
      Default:  none				Calnet:  HeartBeatInt 15   
 
 
  
   IgnoreTBVersionNumbers  switch 		tankplayer_config	Playback 
  
   Prevents tankplayer from
objecting
to tracebuf2 packets that don't have
the correct version field. Not recommended.  
    Default: 0	False   
 
 
    
   LogFile  switch 				tankplayer_config	output  		   
   
Sets the on-off switch for writing a log file to disk. If  switch 
is 0, no log file will be written. If  switch 
is 1, tankplayer will write a daily log file(s) called
tankplayerxx.log_yymmdd where xx is tankplayer's module id (set with
"MyModuleId" command) and yymmdd is the current UTC date (ex: 960123)
on the system clock. The file(s) will be written in the EW_LOG
directory (environment variable).  
      Default:  none   
 
 
    
   MyModuleId  mod_id 			tankplayer_config	Earthworm setup  		   
   
Sets the module id for labeling all outgoing messages.  mod_id  is
a character string (valid strings are listed in earthworm.d) that
relates (in earthworm.d) to a unique single-byte number. Tankplayer
also places this module id in the module id field of each waveform
message header before it puts the message into shared memory.  
      Default:  none				 Example:  MyModuleId MOD_ADSEND_A   
 
 
   OpenTries  n  			tankplayer_config	Playback  		  
 
 Defines the number   n  of times to retry opening a
tank file found in the GetFromDir directory. The purpose of this
parameter is that sometimes if a copy is used to move the file, it will
not be finished writing by the time the tankplayer module gets to it.
For that reason, the OpenTries number of open attempts is made to open
the file for updating (even though it is just being read from). This
assures the file can be read exclusively by the tankplayer module. If
the file is not readable after 5 tries it is not attempted. Between
each try, the program waits OpenWait milliseconds. 
 
 
 
 Default:  5				 
 
 
   OpenWait  n  			tankplayer_config	Playback  		  
 
 Defines the number   n  
milliseconds to pause before retrying an open of a file in the
GetFromDir directory. 
 
 
 
 Default:  200				 
 
  
   
   Pause  nsec 				tankplayer_config	Playback  		   
   
Defines the integer number of seconds  nsec  to
pause after completing the playback of one waveform file before
starting the playback of the next. Tankplayer will continue to issue
its heartbeat while
it is in pause-mode.  
      Default:  none Example:  Pause 15   
 
 
    
   PlayMsgType  type 			tankplayer_config	Playback  		   
   
Tells tankplayer what type of message the waveform file(s) contain.  type  is a character string (valid strings are
listed in earthworm.d) that relates (in earthworm.d) to a unique
single-byte number. By the message-type, tankplayer knows the format of
the waveform message; currently tankplayer only knows how to read and
manipulate TYPE_ADBUF TYPE_TRACEBUF, and TYPE_TRACEBUF2 waveform
messages.  
      Default:  none Example:  PlayMsgType TYPE_TRACEBUF2   
 
 
    
   RingName  ring 				tankplayer_config	Earthworm setup  		   
   
Tells tankplayer which shared memory region to use for output.  ring  is a character string (valid strings
are listed in earthworm.d) that relates (in earthworm.d) to a unique
number for the key to the
shared memory region.  
      Default:  none				Calnet:  RingName WAVE_RING   
 
  
   SaveDataFiles  n  			tankplayer_config	Playback  		  
 
 If   n   is set to 1, then the
tankfiles found in the GetFromDir directory are saved to
GetFromDir/save. If  n  is
set to 0, the tank files are deleted.. 
 
 
 
 Default:  1				 
 
 
   SendLate  xsec 			tankplayer_config	Playback  		   
   
Defines the integer number of seconds  xsec 
before current time to label the packets with. This is useful for
carlsubtrig usage and tuning.  
      Default:  none, this is an optional setting. Example:  SendLate 45   
 
 
    
   StartUpDelay  xsec 			tankplayer_config	Playback  		   
   
Defines the integer number of seconds  xsec  to
wait on startup
before beginning the playback of the first waveform file. Tankplayer
will continue to issue its heartbeat while it is in startup-delay mode.
   
      Default:  none Example:  StartUpDelay 45   
 
 
    
   WaveFile  path-file 			tankplayer_config	Playback  		   
   
Gives the name  path-file  of one waveform file
that tankplayer should
play back.  path-file  is a character string up
to 45 characters long.
Up to 50 "WaveFile" commands may be issued. Tankplayer will play
the files in the order that they were listed in the configuration file,
pausing between files by amount of time set in the "Pause"
command. All files must contain the same type of waveform message
(specified in the "PlayMsgType" command).  
      Default:  none Example:  WaveFile  event1.waves  
 
 
     Module Index 
|  Tankplayer Overview 
 
 
 
 Contact:  
   Questions? Issues?  Subscribe to the Earthworm Google Groups List.    
 
 
 
 
