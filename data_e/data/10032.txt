 August 2016 Commencement - Commencement Office - University of Memphis    










 
 
 
     



 
    
    
     August 2016 Commencement - 
      	Commencement Office
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Commencement Office
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Apply to Graduate  
                          Graduates  
                          Accommodations  
                          Diplomas  
                          Guests  
                          Faculty  
                      
                      
                         
                            August 2016 Commencement   
                            
                               
                                  
                                   Overview  
                                   Dr. Roy B. Van Arsdale  
                                         About Dr. Roy Van Arsdale  
                                         Read Address to Graduates  
                                         Watch Address to Graduates  
                                     
                                  
                                   Photo Album  
                                   View Commencement Program  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Commencement Office
                              	  
                         
                           	August 2016 Commencement
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      August 2016 Commencement 
                     
                      The University of Memphis awarded 667 degrees at its summer Commencement Saturday,
                        August 6, at 10:00 am at FedExForum. The total included 52 doctoral degrees.
                      
                     
                      The featured speaker was Dr. Roy B. Van Arsdale, professor of geology in the Department
                        of Earth Sciences and recipient of the 2016 Willard R. Sparks Eminent Faculty Award.
                        Van Arsdale has been involved in earthquake-related research at the UofM for 23 years.
                        One of the principal goals of his research has been to assess seismic threats, in
                        particular the New Madrid seismic zone of northwestern Tennessee and the surrounding
                        region.
                      
                     
                      In one study, Van Arsdale excavated trenches across the Reelfoot fault and documented
                        a 500-year recurrence interval for large earthquakes on the fault that has subsequently
                        been accepted as the New Madrid seismic zone earthquake recurrence interval. Seismic
                        reflection studies and spatial distribution of earthquakes have allowed him and his
                        students to model the subsurface structure of the Reelfoot fault system and how its
                        movement caused regional uplift like the Reelfoot Lake basin. Understanding the subsurface
                        geometry of fault systems helps determine what areas will topographically rise, which
                        will sink, and where the Mississippi River levees may fail during future earthquakes.
                      
                     
                      Van Arsdale has received funding for his research from the National Science Foundation,
                        Nuclear Regulatory Commission and United States Geological Survey. He has participated
                        in two nationally televised programs that focus on the New Madrid seismic zone: one
                        produced by the History Channel and the second by the National Geographic Society.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         August 2016 Commencement 
                         
                            
                               
                                Overview  
                                Dr. Roy B. Van Arsdale  
                                      About Dr. Roy Van Arsdale  
                                      Read Address to Graduates  
                                      Watch Address to Graduates  
                                  
                               
                                Photo Album  
                                View Commencement Program  
                            
                         
                      
                      
                      
                         
                            
                                Contact Us  
                               Got questions? Let us know how we may assist you. 
                            
                            
                                Commencement Attendance Form  
                               Will you be attending Commencement?  Inform us! 
                            
                            
                                News  
                               Learn about possible date changes, future and past Commencements, Honors Assembly
                                 and more.
                               
                            
                            
                                Commencement Live  
                               Can't attend Commencement?  Watch it live! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


