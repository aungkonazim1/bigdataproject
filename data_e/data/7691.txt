UofM Global Update - Office of the President - University of Memphis    










 
 
 
     



 
    
    
    UofM Global Update - 
      	Office of the President
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Office of the President
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Mission and Plan  
                          Communications  
                          Leadership  
                          Contact  
                      
                      
                         
                            Communications from the President's Office   
                            
                               
                                  
                                   President's Report  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Office of the President
                              	  
                          
                              	Communications
                              	  
                         UofM Global Update 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Dear Faculty and Staff: 
                     
                      Early in 2017, the University of Memphis will launch a new pilot online initiative,
                        UofM Global, targeting FULLY online students. As was shared last week, we now have
                        30+ programs ranked in the top 30 nationally, with our overall online efforts as a
                        University ranked 40th nationally. Given uncertainty about demand and potential enrollment,
                        this initial effort involves only four programs on campus, allowing us to gather data
                        to project not only interest but potential growth trajectory.
                      
                     
                      As you may or may not be aware, the Tennessee Board of Regents Online Degree Program
                        (now eTN online) will dissolve in the coming year following implementation of the
                        Focus Act and autonomous board status for the TBR universities. Given that the UofM
                        is a significant beneficiary of RODP, a coordinated program ready to launch and compete
                        in this critical area for the UofM is required and essential. Hence, the UofM Global
                        initiative.
                      
                     
                      UofM Global will target FULLY online students and have a separate admission designation
                        for these students. Partially online students status will not change. Remember, almost
                        a third of our students take at least one course online during their college career.
                        UofM Global also allows us to offer a differential tuition rate for these FULLY online
                        students, one that is competitive not just regionally, but nationally. As has always
                        been the case, faculty, programs and departments will manage course development, content,
                        delivery and assessment.
                      
                     
                      Launching the UofM Global pilot simply allows the following: 
                     
                      
                        
                         Target a new group of students, those FULLY online. 
                        
                         Offer a reduced tuition rate allowing the UofM to compete regionally and nationally. 
                        
                         Gather data on interest and potential growth trajectory without any financial risks
                           or investment outside of our existing marketing budget.
                         
                        
                         Maintain the high quality of our online course delivery while demand and any need
                           for potential faculty growth (which will be decided by the programs and departments)
                           is gauged.
                         
                        
                         Develop an empirically-driven strategic vision for the future of online offerings
                           for all of our students, including those both partially and fully online.
                         
                        
                      
                     
                      Updates will be provided as we move forward with this effort. It is an exciting initiative
                        and offers considerable potential for our University. We are doing exceptional work
                        already, and UofM Global offers the chance to do even more in a thoughtful and measured
                        fashion.
                      
                     
                      Warm Regards, 
                     
                      M. David Rudd President | Distinguished University Professor
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Communications from the President's Office 
                         
                            
                               
                                President's Report  
                            
                         
                      
                      
                      
                         
                            
                                Dashboard  
                               UofM steps towards success 
                            
                            
                                FOCUS Act  
                               Independent Governing Board Updates 
                            
                            
                                President's Blog  
                               News, stories, videos and more 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


