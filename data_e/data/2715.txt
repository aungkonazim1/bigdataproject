Junmin Wang - Sociology - University of Memphis    










 
 
 
     



 
    
    
    Junmin Wang - 
      	Sociology
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Sociology
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About Us  
                          People  
                          Research  
                          Undergraduate  
                          Graduate  
                          Resources  
                      
                      
                         
                            People   
                            
                               
                                  
                                   Faculty  
                                   Emeritus   Emerita Faculty  
                                   Advisors   Instructors  
                                   Staff  
                                   Graduate Students  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Sociology
                              	  
                          
                              	People
                              	  
                         Junmin Wang 
                      
                   
                   
                       
                      
                     
                     	
                     
                     		  
                      
                        
                        
                         
                           
                           
                                                                
                              
                              
                               
                              
                              
                            
                           
                           
                            
                              
                               
                                 
                                 Junmin Wang
                                 
                               
                              
                              
                               
                                 
                                 Associate Professor
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Phone 
                                    
                                     
                                       
                                       (901) 678-1606
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Email 
                                    
                                     
                                       
                                       junmin.wang@memphis.edu
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Fax 
                                    
                                     
                                       
                                       (901) 678-2525
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Office 
                                    
                                     
                                       
                                       Clement Hall, Room 229B
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Office Hours 
                                    
                                     
                                       
                                       Call for Hours
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 										
                                 
                                 
                                   Website  
                                 
                                   CV  
                                                                         
                                 
                               
                              
                            
                           
                           
                         
                        			 
                        			  
                        
                        
                         Junmin Wang is Associate Professor of Sociology. She received her Ph.D. in Sociology
                           in 2007 from New York University. During the academic year of 2007-2008, she was a
                           post-doctoral fellow in China's political economy at the Research Center for Chinese
                           Politics   Business of Indiana University at Bloomington. Dr. Wang received her B.A.
                           and M.A. in Sociology from Peking University, China.
                         
                        
                         Dr. Wang's main research integrates the sociological subfields of economic sociology,
                           sociology of organizations, and comparative/global sociology with an empirical focus
                           on globalization and the interactions between global processes and China's organizational
                           and institutional changes. Her research combines theories of globalization and international
                           political economy with theories of organizations, social change and economic sociology.
                           Dr. Wang's research contributes to three categories of literature: (1) China's recent
                           integration into the international market and the macro-level transformations in state/market
                           relationships; (2) the institutional and organizational innovations in the global
                           corporate setting; (3) China's ongoing economic reforms at the organizational level
                           and their implications for China's political prospects. She has employed both quantitative
                           and qualitative methods in her research projects.
                         
                        
                         Dr. Wang's book entitled   State-Market Interactions in China's Reform Era: Local State Competition and Global
                                 Market Building in the Tobacco Industry   is published by Routledge (2013). She has published peer-reviewed articles on China's
                           political economy, state/market transitions, and institutional and organizational
                           changes. These publications appear in Theory and Society, Comparative Sociology, Management
                           and Organization Review, Research in the Sociology of Work, and European Journal of
                           Sociology.
                         
                        
                         Dr. Wang's research has been founded by the National Science Foundation (NSF) during
                           the years of 2005-2006 and of 2012-2014 and the Faculty Research Grant in Social Sciences,
                           Business and Law of University of Memphis during the year of 2009-2010. Currently,
                           Dr. Wang is conducting  a research project  regarding China's interactions with global capital and the implications for China's
                           indigenous technological innovation, funded by the NSF's Science of Organizations
                           Program. The project contributes to the thriving interdisciplinary literature on the
                           recent globalization of capital and technology across the globe and the institutions
                           of corporate governance in emerging markets. In her future research, Dr. Wang will
                           continue examining the organizational and institutional processes shaping developing
                           nations' transition towards being more global and more innovative.
                         
                        
                         Findings from this line of research offer policy insights into how developing countries
                           can initiate organizational and institutional changes to take advantage of the potential
                           benefits brought by global capital and international technology transfer over time.
                           For advanced economies like the U.S., policy makers as well as corporate practitioners
                           will benefit from the knowledge regarding industrial and technological transformations
                           of emerging economies and their political-economic transitions in general during the
                           ongoing globalization process, and thus develop better strategies of dealing with
                           increasing competitiveness across the globe and building more effective partnerships
                           with these emerging economies.
                         
                        
                         During the academic year of 2012-2013, Dr. Wang received the Early Career Research
                           Award from College of Arts and Sciences at University of Memphis.
                         
                        
                        
                      
                     								
                     
                     
                     
                     
                     	
                      
                   
                
                
                   
                      
                         People 
                         
                            
                               
                                Faculty  
                                Emeritus   Emerita Faculty  
                                Advisors   Instructors  
                                Staff  
                                Graduate Students  
                            
                         
                      
                      
                      
                         
                            
                                Community Out of Chaos  
                               "portray the idea of chaos to community in Memphis." 
                            
                            
                                Online B.A.  
                               Want to pursue your college degree and manage your busy lifestyle? Experience online
                                 classes at the UofM.
                               
                            
                            
                                Contact Us  
                               Main Office Contacts and Location 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


