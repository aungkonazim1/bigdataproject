Emergency Closing Information - Crisis Management - University of Memphis    










 
 
 
     



 
    
    
    Emergency Closing Information - 
      	Crisis Management
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Crisis Management
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Be TigerREADY  
                          LiveSafe  
                          Announcements  
                          Info for Parents  
                          BCP  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Crisis Management
                              	  
                         Emergency Closing Information 
                      
                   
                   
                       
                      
                     		
                     
                     
                      Emergency Closing Information 
                     
                      
                        
                         
                           
                            
                                 
                               Delayed Opening (until 9:30 am) 
                               Closed All Day 
                               Close During Day 
                               Cancellation of Evening Classes (5 pm and later) 
                               Cancellation of Off-Campus Classes 
                            
                           
                            
                               Day 
                               Evening 
                            
                           
                            
                               Targeted announcement time 
                              
                               Starting at 5:30 am 
                              
                               Starting at 5:30 am 
                              
                               When decided 
                              
                               Starting at 3:00 pm 
                              
                               When decided 
                              
                               Starting at 3:00 p.m. 
                              
                            
                           
                            
                              
                               Situations can change – stay tuned to radio/TV for additional information 
                              
                            
                           
                            
                               Radio/TV stations 
                              
                                TV:  WREG (Ch. 3), WMC (Ch. 5), WHBQ (Ch. 13), WLMT (Ch. 24)    Radio:  WREC (AM 600), WMC (AM 790), WUMR (91.7 FM)
                               
                              
                               Out of town media 
                              
                            
                           
                            
                               What will be affected 
                              
                               
                                 
                                  
                                    
                                     classes 
                                    
                                     University, non-RODP online   video-assisted courses 
                                    
                                     University offices 
                                    
                                     day care 
                                    
                                     events 
                                    
                                     meetings 
                                    
                                     University Store 
                                    
                                     Continuing Education classes 
                                    
                                     First South Credit Union 
                                    
                                     Campus School* 
                                    
                                     Lipman School 
                                    
                                  
                                 
                               
                              
                               
                                 
                                  
                                    
                                     classes 
                                    
                                     University offices 
                                    
                                  
                                 
                               
                              
                            
                           
                            
                               What won't be affected 
                              
                               
                                 
                                  
                                    
                                     Police Services 
                                    
                                     Residence Halls 
                                    
                                     Hotel 
                                    
                                     RODP classes 
                                    
                                  
                                 
                                   Staff:  Personnel that meet the immediate needs of campus residents and facilities will report
                                    appropriately. This includes designated staff from Police Services, Residence Life,
                                    Dining Services, Physical Plant, and other employees other areas as pre-designated
                                    (consult your department head)
                                  
                                  Emergency Crucial personnel will report to work  
                              
                            
                           
                            
                               Athletics 
                              
                               Intercollegiate athletics events will take place as scheduled unless a separate announcement
                                 is made on local radio/TV stations Athletics Information: (901) 678-2331
                               
                              
                            
                           
                            
                               Libraries, Student Rec Center, UC 
                              
                               Same as University 
                              
                            
                           
                            
                               Information 
                              
                               
                                 
                                  
                                    
                                     Call the Special Information Telephone Line: (901) 678-0888 
                                    
                                     Check the web at  www.memphis.edu  
                                    
                                     LiveSafe targeted announcement  LiveSafe download directions here  
                                    
                                  
                                 
                               
                              
                                Center for Innovative Teaching and Learning  
                              
                            
                           
                         
                        
                      
                     * The Campus School adheres to the Shelby County Schools (SCS) directives for weather;
                     when SCS are closed due to severe weather, so is the Campus School. However, the Campus
                     School will close when the University closes.
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                UofM Emergency Preparedness Workbook  
                               Be aware. Be prepared. Have a plan. 
                            
                            
                                Police Services  
                               Police Services at the UofM 
                            
                            
                                Forms  
                               All of B F's forms in one place 
                            
                            
                                Contact Us  
                               Our team is here to help 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/20/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


