WILLIAM SMITH - ECON - University of Memphis    










 
 
 
     



 
    
    
    WILLIAM SMITH - 
      	ECON
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - Department of Economics
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Welcome  
                          Programs  
                          Faculty  
                          Research  
                          FCBE  
                      
                      
                         
                               
                            
                               
                                  
                                   About  
                                   Programs  
                                   Faculty  
                                   Research  
                                   Contact Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	ECON
                              	  
                          
                              	Faculty
                              	  
                         WILLIAM SMITH 
                      
                   
                   
                       
                       
                     
                       
                        
                          
                           
                             
                              
                              
                               
                               
                              
                             
                           
                             
                              
                               
                                 
                                 WILLIAM SMITH
                                 
                                
                              
                               
                                 
                                 Professor and Chair,  Department of Economics
                                 
                                
                              
                                
                                 
                                   
                                    
                                     Phone  
                                    
                                     
                                       
                                       901.678.3675
                                       
                                      
                                    
                                   
                                 
                                   
                                    
                                     Email  
                                    
                                     
                                       
                                       wtsmith@memphis.edu
                                       
                                      
                                    
                                   
                                 
                                   
                                    
                                     Fax  
                                    
                                     
                                       
                                       901.678.2685
                                       
                                      
                                    
                                   
                                 
                                
                              
                                
                                 
                                   
                                    
                                     Office  
                                    
                                     
                                       
                                       BA 400A
                                       
                                      
                                    
                                   
                                 
                                   
                                    
                                     Office Hours  
                                    
                                     
                                       
                                       TR 4:15-5:15 or by appointment
                                       
                                      
                                    
                                   
                                 
                                
                              
                                
                                 
                                 
                                 
                                   CV  
                                 
                                   Website  
                                  
                                 
                                
                              
                             
                           
                          
                        
                        
                        
                         Areas of Interest:  
                        
                         Macroeconomics, Growth, Risk   Uncertainty    
                        
                         Working Papers: 
                        
                         "Changes in Risk and Strategic Interaction," with Diego Nocetti (2014), submitted
                           to Journal of Mathematical Economics.
                         
                        
                         “Does Public Consumption Increase Economic Growth?” with Young Seob Son (2013), under
                           revision for resubmission to Journal of Public Economic Theory.
                         
                        
                         Honors/Awards 
                        
                         
                           
                            Great Oaks Foundation Professor - Fogelman College - 2013 
                           
                            Best Paper Award (2nd Place, Conceptual Category) - Fogelman College - 2010 
                           
                            Dean's Service Award - Fogelman College - 2009 
                           
                            Best Paper Award (2nd Place, Conceptual Category) - Fogelman College, University of
                              Memphis - 2008
                            
                           
                            Distinguished Research Award in the Social Sciences   Business - Alumni association,
                              University of Memphis - 2008
                            
                           
                            Best Paper Award (2nd Place, Conceptual Category) - Fogelman College, University of
                              Memphis - 2006
                            
                           
                            Palmer Award for Research - Fogelman College, University of Memphis - 2006 
                           
                            University Distinguished Teaching Award - University of Memphis - 2006 
                           
                            Summer Research Grant - Pamplin School of Business, VPI   SU - 1988 
                           
                            Tipton R. Snavely Award for Best Dissertation - Department of Economics, University
                              of Virginia - 1988
                            
                           
                            Certificate of Teaching Excellence - MBA Association, VPI   SU - 1989 
                           
                            Certificate of Teaching Excellence - Pamplin College of Business, VPI   SU - 1989 
                           
                            Summer Research Grant - Fogelman College of Business, University of Memphis - 1993 
                           
                            Best Paper Award ( Second Place, Theoretical Category) - Fogelman College of Business,
                              University of Memphis - 1997
                            
                           
                            Summer Research Grant (with Michael Gootzeit) - Fogelman College of Business, University
                              of Memphis - 1998
                            
                           
                            Summer Research Grant - Fogelman College of Business, University of Memphis - 1999 
                           
                            Summer Research Grant (with Kee Chung) - Fogelman College of Business   Economics
                              - 2001
                            
                           
                            Summer Research Grant (with Heng-fu Zou and Yulei Luo) - Fogelman College of Business
                                Economics - 2002
                            
                           
                            Best Paper Award ( First Place, Conceptual Category) - Fogelman College of Business
                                Economics - 2002
                            
                           
                            Palmer Award - Fogelman College of Business   Economics - 2003 
                           
                         
                        
                         Journal Articles 
                        
                         
                           
                            
                              
                               "Forward Exchange Rates in General Equilibrium,"  Journal of International Money   Finance  10 (1991), pp. 497-511.
                               
                              
                            
                           
                            
                              
                               "Price Discrimination with Correlated Demands," with Catherine Eckel,  Southern Economic Journal  59 (1992), pp. 58-65.
                               
                              
                            
                           
                            
                              
                               "Investment, Uncertainty, and Price Stabilization Schemes,"  Journal of Economic Dynamics and Control  18 (1994), pp. 561-579.
                               
                              
                            
                           
                            
                              
                               "It Pays to Be Different: Endogenous Heterogeneity of Firms in an Oligopoly," with
                                 David Mills,  International Journal of Industrial Organization  14 (1996), pp. 317-329.
                               
                              
                            
                           
                            
                              
                               "Taxes, Uncertainty, and Long-Term Growth,"  European Economic Review  40 (1996), pp. 1647-1664. 
                               
                              
                            
                           
                            
                              
                               "Feasibility and Transversality Conditions for Models of Portfolio Choice with Non-Expected
                                 Utility in Continuous Time,"  Economics Letters  53 (1996), pp. 123-131.
                               
                              
                            
                           
                            
                              
                               "The Treatment of Medical Indebtedness in Personal Bankruptcy," with Dennis Wilson,
                                 John Rogers, and Cyril Chang,  The   Journal of Economics  23 (1997).  
                               
                              
                            
                           
                            
                              
                               "Birth, Death and Consumption: Overlapping Generations and the Random Walk Hypothesis,"
                                  International Economic Journal  23 (1998), pp. 105-116.
                               
                              
                            
                           
                            
                              
                               "Risk, the Spirit of Capitalism and Growth: the Implications of a Preference for Capital,"
                                  Journal of Macroeconomics  21 (1999), pp. 241-262.
                               
                              
                            
                           
                            
                              
                               "The Calculus of Fear: Revolution, Repression, and the Rational Peasant," with Julia
                                 Heath, David Mason, and Joseph Weingarten,  Social Sciences Quarterly  81 (1999).
                               
                              
                            
                           
                            
                              
                               "How Does the Spirit of Capitalism Affect Stock-Market Prices?"  Review of Financial Studies  14 (2001): 1215-1232.
                               
                              
                            
                           
                            
                              
                               "Shipping the Good Times Out: A Note on Apples and Donations of Time and Money," with
                                 Cyril Chang,  Economics Bulletin  January 2002.
                               
                              
                            
                           
                            
                              
                               "Marshallian Recursive Preferences and Growth," with Michael Gootzeit and Johannes
                                 Schneider,  Journal of Economic Behavior   Organization  49 (2002): 381-404.
                               
                              
                            
                           
                            
                              
                               "Consumption and Saving With Habit Formation and Durability,"  Economics Letters  75 (2002): 369-375. 
                               
                              
                            
                           
                            
                              
                               "But Can She Cook? Women's Education and Housework Productivity," with David Sharp,
                                 Julia Heath, and David Knowlton,  Economics of Education Review  23 (2004): 605-614.
                               
                              
                            
                           
                            
                              
                               "Can the Desire to Conserve Our Natural Resources Be Self-Defeating?" with Young Seob
                                 Son,  Journal of Environmental Economics   Management  49 (2005): 52-67.  
                               
                              
                            
                           
                            
                              
                                "Equilibrium Consumption and Precautionary Savings in a Stochastically Growing Economy,"
                                 with Stephen Turnovsky.   Journal of Economic Dynamics   Control  30 (2006), 243-278.  
                               
                              
                            
                           
                            
                              
                               "Fertility, Volatility, and Growth," with Aude Pommeret.   Economics Letters  87 (2005): 347-353  
                               
                              
                            
                           
                            
                              
                               “Why Do Pooled Forecasts Do Better Than Individual Forecasts?  An Alternative Explanation,”
                                 with Diego Nocetti,  Economics Bulletin , Vol 4, No. 36 (2006).
                               
                              
                            
                           
                            
                              
                                “Asset Pricing with Multiplicative Habits and Power-expo Preferences,” with Richard
                                 Zhang,  Economics Letters  March (2007): 319-325.
                               
                              
                            
                           
                            
                              
                                “Consumption and Risk with Hyperbolic Discounting,” with Heng-Fu Zou and Liutang
                                 Gong,  Economics Letters  96 (2007): 153-160.
                               
                              
                            
                           
                            
                              
                                “Inspecting the Mechanism Exactly:  A Closed-form Solution to a Stochastic Growth
                                 Model,” BE Contributions to Macroeconomics  (August2007).
                               
                              
                            
                           
                            
                              
                               “The Spirit of Capitalism and Excess Smoothness,” with Yulei Luo and Heng-fu Zou,
                                  Annals of Economics   Finance  10 (2008): 281-301.     
                              
                            
                           
                            
                              
                               “The Spirit of Capitalism, Precautionary Savings, and Consumption,” with Yulei Luo
                                 and Heng-fu Zou,”  Journal of Money, Credit,   Banking  41 (2009): 543-554.
                               
                              
                            
                           
                            
                              
                               “Time Diversification: Definitions and Some Closed-form Solutions,” with Kee Chung
                                 and Tao Wu,  Journal of Banking   Finance  33 (2009): 1101-1111.
                               
                              
                            
                           
                            
                              
                               “Uncertainty, the Demand for Health Care, and Precautionary Saving,” with Diego Nocetti,
                                  BE Advances in Economic Analysis   Policy  vol. 10, issue 1(2010). 
                               
                              
                            
                           
                            
                              
                               “Price Uncertainty, Saving, and Welfare” (2011), with Diego Nocetti,  Journal of Economic Dynamics and Control  35, 1139-1149
                               
                              
                            
                           
                            
                              
                               “Precautionary Savings and Labor Supply with Non-Expected Utility Preferences,” with
                                 Diego Nocetti (2011),   Journal of Money, Credit   Banking  43, 1475-1504.“
                               
                              
                            
                           
                            
                              
                               A New Look at the Determinants of the Ecological Discount Rate: Disentangling Social
                                 Preferences,” with Luciana Echazu and Diego Nocetti,  B.E. Journal of Economic Analysis   Policy, Advances  (2012).
                               
                              
                            
                           
                            
                              
                               "The Effects of Wage Volatility on Growth," with Michael Jetter and Olexander Nikolsko-Rzheskyy,
                                 Journal of Macroeconomics 37 (2013), pp. 93-109.
                               
                              
                            
                           
                            
                              
                               "Saving-Based Asset Pricing," with Johannes Dyer and Johannes Schneider, Journal of
                                 Banking and Finance 37 (2013), pp. 3704-3715,
                               
                              
                            
                           
                            
                              
                               "The Discriminating beta: Prices and Capacity with Correlated Demands," with Catherine
                                 Eckel, (2013), forthcoming Southern Economic Journal
                               
                              
                            
                           
                         
                         
                        
                       
                     
                      
                   
                
                
                   
                      
                          
                         
                            
                               
                                About  
                                Programs  
                                Faculty  
                                Research  
                                Contact Us  
                            
                         
                      
                      
                      
                         
                            
                                Undergraduate Programs  
                               Learn about our undergraduate offerings! 
                            
                            
                                Seminars  
                               Keep up with the latest Economic academic research in our upcoming seminars.  
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


