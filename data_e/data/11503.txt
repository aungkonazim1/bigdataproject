Welcome - Bridging Cultures -- Muslim Journeys - LibGuides at University of Memphis Libraries   
 
	 
         
         
         
        
		 Welcome - Bridging Cultures -- Muslim Journeys - LibGuides at University of Memphis Libraries 
		 
		 
 
 
 
 
 
 
 
 
 
 
 

		 
		 
		 
		 
		 
		 
         
         
				 
				 
             
         
        
        
            
		
		 
		
		 
		 
		
		 
		
		
	   
		         
        	 
     
	 
		 Skip to main content                  
                 
         
  		 
             
                
			 
				
					 
						 University of Memphis Libraries
						 
					 
					 
						 LibGuides
						 
					 
					 
						 Bridging Cultures -- Muslim Journeys
						 
					 
					 Welcome
					 
			              
			 
				 
					
                 
                     
                         
                             
                             Search this Guide  
                             
                                 Search 
                             
                         
                     
                 				 
							 
             
                 Bridging Cultures -- Muslim Journeys 
                 
                      
                 
             
         
         
          
         
             
                 
                     
                         
                             
                 
                     
                         Welcome 
                        
                     
                 
                     
                         
                             Bookshelf Themes 
                            
                         
                         
                                      
                                     Toggle Dropdown 
                                  
                             
                                 American Stories
                                    
                                 
                             
                             
                                 Connected Histories
                                    
                                 
                             
                             
                                 Literary Reflections
                                    
                                 
                             
                             
                                 Pathways of Faith
                                    
                                 
                             
                             
                                 Points of View
                                    
                                 
                             
                             
                                 Art, Architecture, and Film
                                    
                                 
                              
                     
                     
                         
                             Bookshelf Films 
                            
                         
                         
                                      
                                     Toggle Dropdown 
                                  
                             
                                 Islamic Art: Mirror of the Invisible World (2011)
                                    
                                 
                             
                             
                                 A Prince Among Slaves: The Cultural Legacy of Enslaved Africans (2007)
                                    
                                 
                             
                             
                                 Koran by Heart (2011)
                                    
                                 
                             
                             
                                 Islamic Art Spots (2013)
                                    
                                 
                              
                     
                 
                     
                         Events 
                        
                     
                 
                 
                     
                         Resources 
                        
                     
                 
                 
                     
                         Community Partners 
                        
                     
                 
                 
                     
                         Sponsors and Partners 
                        
                     
                  
                            
                 
                     
                         
                        
                         
                     
                                          
                     
                 
                 
                     
                         
                             
     
                  
					 
						 
							   NEH Bridging Cultures Bookshelf  
                                 
							 
								 
									
			 
				      
  The books and films comprising the Bookshelf were selected with the advice librarians and cultural programming experts, as well as distinguished scholars in the fields of anthropology, world history, religious studies, interfaith dialogue, the history of art and architecture, world literature, Middle East studies, Southeast Asian studies, African studies, and Islamic studies.     
		    
								 
								
							 
						 
					   
					 
						 
							   Upcoming Events!  
                                 
							 
								 
									
			 
				  Please be sure to check out the event flyers   here   -   All   events are free and open to the public!  
  
 
   Tuesday, November 19, 2013   
  Film Screening:   Koran by Heart   (2011)  
 Followed by panel discussion led by Professor Beverly Tsacoyianis (Department of History), Dr. Yasir Qadhi (Rhodes College), Professor Farah Ali (Department of Foreign Languages and Literatures) and an executive board member of the U of M Muslim Student Association. 
 For more information, contact Jennifer Schnabel at 901.678.8210 or jschnbel@memphis.edu 
                      University of Memphis                      
                      University Center, Bluff Room                      
          6:00 - 8:30 PM                 
           Sponsored by the University Libraries, the Department of History, and the Muslim Student Association.           
 
       Sunday, November 10, 2013       
       Panel Discussion around the themes of       
        Bridging Cultures and Acts of Faith        
          Benjamin L. Hooks Central Library           
        2:00 PM - 4:00 PM        
         for community read information, contact Wang-Ying Glasgow at                 901-415-2709 or at                 Wang-ying.Glasgow@memphistn.gov         
		    
								 
								
							 
						 
					   
					 
						 
							  University of Memphis Catalog  Muslim Journeys Bookshelf 
                                 
							 
								 
									
			 
				       
		    
								 
								
							 
						 
					   
					 
						 
							 NEH
                                 
							 
								 
									
			 
				     
   The Bridging Cultures Bookshelf: Muslim Journeys is a project of the National Endowment for the Humanities, conducted in cooperation with the American Library Association, the Ali Vural Ak Center for Global Islamic Studies at George Mason University, Oxford University Press, and Twin Cities Public Television. Support was provided by a grant from Carnegie Corporation of New York. Additional support for the arts and media components was provided by the Doris Duke Foundation for Islamic Art.   
		    
								 
								
							 
						 
					   
					 
						 
							 NEH Bookshelf
                                 
							 
								 
									
			 
				 Tweets by @NEHbookshelf 


		    
								 
								
							 
						 
					   
					 
						 
							 NPR Religion Podcasts
                                 
							 
								 
									 
                            
                
                 
                     
                 
                        
								 
								
							 
						 
					   
					 
						 
							 NYT Religion and Ethics RSS Feed
                                 
							 
								 
									 
                            
                
                 
                     
                 
                        
								 
								
							 
						 
					  
         
     
                          
                     
                    
                 
                     
                    
                     
                     
                      Next:  Bookshelf Themes    
                     
                                  
             
         
         
         
             
                 
                     
                         Last Updated:   May 19, 2017 9:29 AM                      
                     
                         URL:   http://libguides.memphis.edu/muslimjourneys                      
                     
                    	    Print Page                      	
                     
                 
                     Login to LibApps                  
             
             
                 
                     Report a problem  
                 
                 
                    
                     Subjects:  
                      Special Collections  
                                     
                 
                                     
             
         
         
        
			 
				 
					 
					    
					    
					 
				 
			          
                              
                                
                 
                

		 
  	 
 
