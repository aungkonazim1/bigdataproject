Printing discussion threads and posts | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Printing discussion threads and posts  | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Discussions    »    Participating in discussions   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Printing discussion threads and posts                        
        
        
       
          
     
           Printer-friendly version       
	You can view posts in a printable format and send the posts to your printer:
 

  
		Do one of the following:
		  
				Select    Print Thread  from the context menu of a thread to print a thread.
			 
			 
				Select    Print  from the context menu of an individual post to print a post.
			 
		  
	 
		In the Printable View pop-up window, click  Print .
	 
      Audience:    Learner      

    
           

                   ‹ Rating discussion posts 
        
                   up 
        
                   Changing Discussions settings › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Discussions  
  
      Participating in discussions    Accessing Discussions     Finding and reading discussion posts and threads    Creating and replying to discussion threads    Saving a draft discussion thread    Rating discussion posts    Printing discussion threads and posts     Changing Discussions settings      Following discussions    Creating and managing discussions    Monitoring discussions    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
