Deleting grade items or categories | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Deleting grade items or categories | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Grades    »    Managing grade items and grade book categories   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Deleting grade items or categories                       
        
        
       
          
     
           Printer-friendly version        
		On the Manage Grades page, click    Delete  from the More Actions button.
	 
	 
		Select the check box for each grade item or category want to delete.
	 
	 
		Click  Delete .
	 
  
	 Notes 

	  
			Selecting a category does not select the grade items that reside in it. If you delete a category, the grade items associated with it become independent grade items.
		 
		 
			You cannot delete grade items that are associated with another course object, such as a quiz, discussion topic, or dropbox folder. To delete the item you must first remove the association. Click the    Information  icon for a grade item with an association to view details about where the item is used.
		 
	  
     Audience:    Instructor      

    
           

                   ‹ Setting release conditions for grade items or categories 
        
                   up 
        
                   Restoring deleted grade items or categories › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Grades  
  
      Finding my grades    Creating a grade book    Creating grade items and grade book categories    Managing grade items and grade book categories    Editing grade items or categories    Hiding and showing grade items in the grade book    Reordering grade items and categories    Setting availability for grade items or categories    Setting release conditions for grade items or categories    Deleting grade items or categories    Restoring deleted grade items or categories    Associating grade items with course objects    Associating grade items or categories with learning objectives      Managing grade schemes    Managing users  grades    Managing final grades    Changing Grades settings and display options    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
