Welcome to the Student Health Center! - Student Health Center - University of Memphis    










 
 
 
     



 
    
    
    Welcome to the Student Health Center! - 
      	Student Health Center
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Student Health Center
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Emergency  
                          Immunization  
                          Clinics  
                          Policies  
                          Tiger Health 101  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                         
                           	Student Health Center
                           	
                         
                      
                   
                   
                      
                         
                              
                             
                            
                             
                            
                             
                            
                            
                                
                            
                         
                         
                             
                         
                      
                      
                     		
                     
                     
                      Welcome to the Student Health Center! 
                     
                      Mission 
                     
                      The mission of the University of Memphis Student Health Center (SHS) is: to empower
                        students to make informed healthy choices and to take responsibility for a goal of
                        lifelong wellness; to provide personalized health services, education and disease
                        prevention to a diverse student population; and to enhance academic achievement, personal
                        growth, and out of the classroom learning.
                      
                     
                      Announcements 
                     
                      
                        
                          Affordable Care Act (ACA) - Information for Students  
                        
                          Decoding Universal Health Care; The Affordable Care Act  
                        
                          Flu Shots  
                        
                          HIV Testing (Free)  
                        
                          Memphis Healthy U  
                        
                          Satisfaction Survey Survey  
                        
                          Student Health 101  
                        
                          Student Insurance Information  
                        
                          Support for Nursing Mothers  
                        
                          Tiger Cookbook  
                        
                          Tiger Scoop  
                        
                          Zika Virus Information  
                        
                      
                     
                       Disclaimer:
                      
                     
                      University of Memphis does not claim responsibility for information found herein,
                        nor for effects suffered by the adherence to such information. The documents contained
                        within this site are presented by University of Memphis Health Services, solely with
                        the intent of providing public service information on health and health related issues.
                        This information is not intended nor implied to be a substitute for professional medical
                        advice. Always consult your healthcare provider or physician prior to utilizing any
                        of the information presented in this web site.
                      
                     
                      Student Health Services provides links to other web sites for the convenience of site
                        visitors in locating information that may be of interest. Student Health Services
                        is not responsible for the availability or content of these external sites, nor does
                        Student Health Center endorse, warrant or guarantee the products, services or information
                        described or offered at these other Internet sites.
                      
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Upload Immunization Documentation  
                               Upload state mandated immunizations documentation to meet certain health requirements. 
                            
                            
                                Family Planning Clinic  
                               Service provided by the Memphis and Shelby County Public Health Department. 
                            
                            
                                Health Education Resources  
                               Useful links to help bump up your health IQ! 
                            
                            
                                Contact Us  
                               Main contact information and hours of operation. 
                            
                         
                      
                      
                                         
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


