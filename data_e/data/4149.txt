Legal Studies FAQ - University College - University of Memphis    










 
 
 
     



 
    
    
    Legal Studies FAQ - 
      	University College
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			University College
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Undergraduate  
                          Graduate  
                          Resources  
                          Online  
                          Faculty and Staff  
                      
                      
                         
                            Legal Studies   
                            
                               
                                  
                                   Degree Requirements  
                                   Course Descriptions   
                                   Internships  
                                   Senior Project  
                                   Lambda Epsilon Chi  
                                   Listserv  
                                   Additional Resources  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	University College
                              	  
                          
                              	Legal Studies
                              	  
                         Legal Studies FAQ 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Legal Studies FAQ 
                     
                      Our Legal Studies Program provides students the knowledge and skills required to provide
                        substantive legal services under the supervision of an attorney. Only licensed attorneys
                        are permitted to practice law.
                      
                     
                      Choose among the following topics for important information on our program. 
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                    Program   
                                 
                               
                              
                               
                                 
                                    Admission   
                                 
                               
                              
                               
                                 
                                    Tuition   
                                 
                               
                              
                               
                                 
                                    Financial Assistance   
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                    Transfer Students   
                                 
                               
                              
                               
                                 
                                    Legal Studies Internships   
                                 
                               
                              
                               
                                 
                                    Placement   
                                 
                               
                              
                               
                                 
                                    Law School   
                                 
                               
                              
                            
                           
                         
                        
                      
                     
                       PROGRAM  
                     
                       Q: How many credit hours must be completed in order to receive the BPS - Legal Studies
                           degree?  
                     
                      A: The BPS degree requires  120 credit-hours of college coursework  which meets Legal Studies academic guidelines.
                      
                     
                       Q: How long will it take me to complete the BPS degree?  
                     
                      A: That depends entirely upon you. If your lifestyle is such that you can only take
                        one course per semester, it will take quite some time. If, however, you can take seven
                        courses each semester, you will be finished in no time.
                      
                     
                       ADMISSION  
                     
                       Q: How do I apply to the Legal Studies Program?  
                     
                      A: First, apply to the University of Memphis by completing an Undergraduate Application
                        for Admission. Follow their instructions to pay the admissions fee and complete the
                        process.
                      
                     
                      Second, complete the appropriate   University College Admission Application .  
                     
                       TUITION  
                     
                       Q: How much does it cost to complete the BPS degree?  
                     
                      A: Unlike the for-profit companies that have gotten into the "business" of teaching
                        and who can quote you an astronomical sum for a six or nine-month paralegal program,
                        University of Memphis tuition is based upon 1) the number of credit hours you take
                        any given semester, and 2) whether you are considered a "resident" or "non-resident"
                        of Tennessee.
                      
                     
                       FINANCIAL AID  
                     
                       Q: Is financial aid available to Legal Studies students?  
                     
                      A: Yes, students pursuing the BPS - Legal Studies degree are eligible for financial
                        aid.  The University of Memphis Financial Aid Office  can answer additional questions you may have about aid.
                      
                     
                       Q: Are there any scholarships available to Legal Studies students?  
                     
                      A: There are no scholarships on campus specifically for Legal Studies students. However,
                        there is a University College Alumni Scholarship for which Legal Studies students
                        are eligible. Also, the Tennessee Paralegal Association awards the Lawassa B. Jones
                        Memorial Scholarship on an annual basis. In addition, the  University of Memphis' Scholarship Office  is a great resource for other scholarship opportunities.
                      
                     
                       TRANSFER STUDENTS  
                     
                       Q: I took paralegal courses through a correspondence program. Will these course credits
                           transfer to the Legal Studies Program?  
                     
                      A: Typically not since these courses are often offered by proprietary schools (for-profit,
                        business-type schools) as opposed to regionally-accredited institutions such as The
                        University of Memphis.
                      
                     
                       Q: What if I took paralegal courses on the campus of a regionally-accredited institution.
                           Will these course credits transfer to the Legal Studies Program?  
                     
                      A: Yes, in most instances, particularly if those courses were actually offered and
                        taught by that institution and its instructors. There has been a proliferation of
                        businesses coming into certain areas and leasing space from regionally-accredited
                        institutions in an attempt to give the public the impression that the institution
                        is actually offering the courses. Be very careful. Contact the institution on whose
                        campus the courses are being offered and ask whether those courses are part of that
                        institution's curriculum.
                      
                     
                       INTERNSHIPS  
                     
                       Q: How many clock-hours of work must I complete within a legal environment to earn
                           the three hours of college credit required for the paralegal internship?  
                     
                      A: A minimum of 150. 
                     
                       Q: If I already work in a law firm, do I have to complete an internship?  
                     
                      A: Yes. This program requires all students complete an internship. 
                     
                       Q: Since I already work in a law firm and must complete a Legal Studies internship,
                           must I do my internship in a different legal setting?  
                     
                      A: No. We want you to keep your paycheck coming in so feel free to complete your internship
                        with your current employer.
                      
                     
                       Q: I am a legal secretary at my firm. May I continue my current duties for 150 clock-hours
                           and receive credit for the internship?  
                     
                      A: Perhaps. The internship must give students experience in substantive legal work,
                        rather than clerical or administrative duties.  To find out if your current job qualifies,
                        please speak to the Legal Studies Coordinator.
                      
                     
                       Q: What if I am unable to complete my internship during the semester in which I am
                           enrolled?  
                     
                      A: You will receive a grade of "I" (i.e., incomplete) and be given an additional 45
                        days from the end of the semester to complete your internship.
                      
                     
                       Q: May I enroll in the internship during a summer term?  
                     
                      A: Yes. Internships are available year-round. 
                     
                       PLACEMENT  
                     
                       Q: What are my chances of getting a job in the legal field once I complete the Legal
                           Studies Program?  
                     
                      A: Your career prospects are excellent. I invite you to see what the  United States Department of Labor's Bureau of Labor Statistics  has to say about the legal profession.
                      
                     
                       Q: How can I learn about local and national job openings in the legal field?  
                     
                      A: You may learn about current job openings through several sources: 
                     
                      
                        
                         
                           
                            All University of Memphis legal studies students are required to have e-mail access
                              and are placed on our legal studies mail server. Graduates of the program are added
                              to the alumni mail server and receive current announcements which often contain job
                              openings from within our region.
                            
                           
                         
                        
                         
                           
                            Job placement services are available free-of-charge through the  University of Memphis Office of Career and Employment Services Center.  
                           
                         
                        
                         
                           
                            Area newspapers, magazines, and legal journals (such as those published by local bar
                              associations)
                            
                           
                         
                        
                         
                           
                            LinkedIn is an excellent resource for job opportunities in the legal field. We highly
                              recommend creating a LinkedIn profile with a professional photo and joining lots of
                              networking groups from the legal field. Several job announcements are posted on LinkedIn
                              every day.
                            
                           
                         
                        
                      
                     
                       LAW SCHOOL  
                     
                       Q: Why are people using the Legal Studies Program as a pre-law program?  
                     
                      A: Unlike medical school where you are actually taught the practice of medicine, law
                        school does not teach you the law. Any law school administrator or professor will
                        tell you that law schools teach students how to "think like a lawyer." That entails
                        an abundance of theory, philosophy, and debate. The Legal Studies Program teaches
                        its students the substantive points of the law so that graduates of the program step
                        into their first law class with a level of comfort, assurance, and skills that the
                        typical law student will not have.
                      
                     
                       Q: Will any of my Legal Studies courses transfer to law school?  
                     
                      A: No. Legal Studies courses are undergraduate in nature while law courses are considered
                        graduate-level courses.
                      
                     
                       Q: Is the Bachelor of Professional Studies degree in Legal Services sufficient to
                           get me into law school?  
                     
                      A: Most law schools will accept students from almost any major, as long as their grades,
                        resumes, personal statements, and  scores on the LSAT (Law School Admission Test)  otherwise warrant admission. However, many alumni have commented that their bachelor's
                        degree in Legal Studies was very helpful during their first year of law school, and
                        they felt better prepared than some of their classmates from other disciplines. The
                        Legal Studies degree means you would already know how to brief cases, conduct legal
                        research, and read legal citations long before law school, which could be a big advantage. 
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Legal Studies 
                         
                            
                               
                                Degree Requirements  
                                Course Descriptions   
                                Internships  
                                Senior Project  
                                Lambda Epsilon Chi  
                                Listserv  
                                Additional Resources  
                            
                         
                      
                      
                      
                         
                            
                                Find the program that's right for you  
                               Explore our degree programs 
                            
                            
                                Learn more about University College  
                               Request more program information 
                            
                            
                                Advising Appointments  
                               Academic Advising is available in-person, by phone or online 
                            
                            
                                Contact Us  
                               The University College team is here to help 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


