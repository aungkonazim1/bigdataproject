Scholarship Form - Herff College of Engineering - University of Memphis    










 
 
 
     



 
    
    
    Scholarship Form - 
      	Herff College of Engineering
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Herff College of Engineering
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Future Students  
                          Students  
                          Programs  
                          Research  
                          Alumni  
                          Events  
                      
                      
                         
                            Future Students   
                            
                               
                                  
                                   Choose Your Major  
                                   Visit the College  
                                   Apply to the UofM  
                                   Scholarships  
                                   Student Life  
                                   FAQs  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Herff College of Engineering
                              	  
                          
                              	Future Students
                              	  
                         Scholarship Form 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                        
                     
                      The Herff College of Engineering 
                     
                       Freshman Engineering Scholarship Application 2017-18  
                     
                       
                     
                       
                     
                      I hereby give permission to the University of Memphis Herff College of Engineering
                        to secure a copy of my transcript and test scores to be  sent to the Herff College
                        of Engineering Scholarship Committee and potential sponsors of my scholarship, if applicable.
                      
                     
                       
                     
                      
                         
                              
                              If you see this don't fill out this input box.    
                                  
                                     E-Signature for above conditions 
                                       E-Signature    
                               
                                Name *    
                                Birth Date *    
                                Gender  
                                     
                                       Please Select
                                       
                                     
                                     Male 
                                     Female    
                                Home Address *    
                                City *    
                                State   
                                     Alabama 
                                       						
                                     
                                     Alaska 
                                       						
                                     
                                     Arizona 
                                       						
                                     
                                     Arkansas 
                                       						
                                     
                                     California 
                                       						
                                     
                                     Colorado 
                                       						
                                     
                                     Connecticut 
                                       						
                                     
                                     Delaware 
                                       						
                                     
                                     Florida 
                                       						
                                     
                                     Georgia 
                                       						
                                     
                                     Hawaii 
                                       						
                                     
                                     Idaho 
                                       						
                                     
                                     Illinois 
                                       						
                                     
                                     Indiana 
                                       						
                                     
                                     Iowa 
                                       						
                                     
                                     Kansas 
                                       						
                                     
                                     Kentucky 
                                       						
                                     
                                     Louisiana 
                                       						
                                     
                                     Maine 
                                       						
                                     
                                     Maryland 
                                       						
                                     
                                     Massachusetts 
                                       						
                                     
                                     Michigan 
                                       						
                                     
                                     Minnesota 
                                       						
                                     
                                     Mississippi 
                                       						
                                     
                                     Missouri 
                                       						
                                     
                                     Montana 
                                       						
                                     
                                     Nebraska 
                                       						
                                     
                                     Nevada 
                                       						
                                     
                                     New Hampshire 
                                       						
                                     
                                     New Jersey 
                                       						
                                     
                                     New Mexico 
                                       						
                                     
                                     New York 
                                       						
                                     
                                     North Carolina 
                                       						
                                     
                                     North Dakota 
                                       						
                                     
                                     Ohio 
                                       						
                                     
                                     Oklahoma 
                                       						
                                     
                                     Oregon 
                                       						
                                     
                                     Pennsylvania 
                                       						
                                     
                                     Rhode Island 
                                       						
                                     
                                     South Carolina 
                                       						
                                     
                                     South Dakota 
                                       						
                                     
                                     Tennessee 
                                       						
                                     
                                     Texas 
                                       						
                                     
                                     Utah 
                                       						
                                     
                                     Vermont 
                                       						
                                     
                                     Virginia 
                                       						
                                     
                                     Washington 
                                       						
                                     
                                     West Virginia 
                                       						
                                     
                                     Wisconsin 
                                       						
                                     
                                     Wyoming 
                                       						
                                        
                                Zip *    
                                Email *    
                                High School Name *    
                                Graduation Year   
                                     2007 
                                       						
                                     
                                     2008 
                                       						
                                     
                                     2009 
                                       						
                                     
                                     2010 
                                       						
                                     
                                     2011 
                                       						
                                     
                                     2012 
                                       						
                                     
                                     2013 
                                       						
                                     
                                     2014 
                                       						
                                     
                                     2015 
                                       						
                                     
                                     2016 
                                       						
                                     
                                     2017 
                                       						
                                     
                                     2018 
                                       						
                                     
                                     2019 
                                       						
                                     
                                     2020 
                                       						
                                        
                                Current Grade *    
                                ACT/SAT Composite *    
                                ACT/SAT Math  *    
                                High School GPA  *    
                               
                                  
                                     Engineering Field of Interest 
                                       Biomedical Engineering  
                                       Civil Engineering  
                                       Computer Engineering _  
                                       Electrical Engineering  
                                       Mechanical Engineering  
                                       Engineering Technology  
                                       Unknown at this time    
                                  
                                 Submit
                                   
                              
                              
                            
                         
                      
                     
                       
                     
                        All applications are due by January 15th, 2018 for incoming freshmen in Fall 2018 :
                      
                     
                       If you have any question send email to: rjdeaton@memphis.edu   Dr. Russell J. Deaton   Associate Dean for Academic Affairs     Administration Herff College of Engineering   University of Memphis   202 E Engineering Administration Bldg.   Memphis, TN 38152   fax 901-678-4180   office 901-678-3258    
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Future Students 
                         
                            
                               
                                Choose Your Major  
                                Visit the College  
                                Apply to the UofM  
                                Scholarships  
                                Student Life  
                                FAQs  
                            
                         
                      
                      
                      
                         
                            
                                Visit Herff  
                               See what Herff has to offer YOU! 
                            
                            
                                Apply to UofM  
                               Submit your application and enroll in the College of Engineering 
                            
                            
                                2017 E-day Results  
                               Thanks to 3,000 student participants!  
                            
                            
                                Contact Us  
                               Have Questions? We can help!  
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/5/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
     



 


