About the Health Center - Student Health Center - University of Memphis    










 
 
 
     



 
    
    
    About the Health Center - 
      	Student Health Center
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Student Health Center
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Emergency  
                          Immunization  
                          Clinics  
                          Policies  
                          Tiger Health 101  
                      
                      
                         
                            About    
                            
                               
                                  
                                   Charges  
                                   Eligibility  
                                   Health Education  
                                   Health History Form  
                                   Health Insurance Exchange  
                                   Parking   Transportation  
                                   Records   Permission to Treat  
                                   Staff  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Student Health Center
                              	  
                         
                           	About
                           	
                         
                      
                   
                   
                       
                      
                     		
                     
                     
                      About the Student Health Center 
                     
                       Our major emphasis is  on maintaining, restoring and/or improving the physical and emotional health and well being of students. Student Health Center is an accessible, cost effective
                        health care facility that emphasizes campus wide health promotion, disease prevention,
                        and acute episodic outpatient medical care. Individualized attention, courtesy and
                        patient confidentiality to all is of primary importance.
                      
                     
                       We are a Walk-In Clinic. No appointment is necessary.   Student Health Center is staffed with Nurse Practitioners, Registered Nurses, Licensed
                        Practical Nurse, Laboratory and X-ray Technologist, Health Educator, and various administrative
                        support personnel.
                      
                     
                       A Dispensary is available  for the convenience of our students. Faculty and Staff are not eligible to have prescriptions
                        filled at our onsite dispensary. Most frequently used prescription medications written
                        by the Student Health Center's Nurse Practitioners, as well as a variety of over-the-counter
                        medications, may be purchased at a nominal charge.
                      
                     
                       Short-term, acute illnesses and injuries  are addressed at Student Health Center. Patients with chronic, complex or recurrent
                        medical conditions must continue to use their primary care physician for issues related
                        to their chronic illness. No routine physicals are performed at Student Health Center.
                        Only lab tests ordered by Student Health Center medical providers will be performed.
                      
                     
                       Job related injuries  are not treated at Student Health Center. If you are an employee or student employee
                        at the university and you are injured on the job, you must report the incident to
                        your supervisor who will obtain the appropriate paperwork from the Human Resources
                        Department.
                      
                     
                        
                     
                     	
                      
                   
                
                
                   
                      
                         About  
                         
                            
                               
                                Charges  
                                Eligibility  
                                Health Education  
                                Health History Form  
                                Health Insurance Exchange  
                                Parking   Transportation  
                                Records   Permission to Treat  
                                Staff  
                            
                         
                      
                      
                      
                         
                            
                                Upload Immunization Documentation  
                               Upload state mandated immunizations documentation to meet certain health requirements. 
                            
                            
                                Family Planning Clinic  
                               Service provided by the Memphis and Shelby County Public Health Department. 
                            
                            
                                Health Education Resources  
                               Useful links to help bump up your health IQ! 
                            
                            
                                Contact Us  
                               Main contact information and hours of operation. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


