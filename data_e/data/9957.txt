Submitting a user&#039;s quiz (impersonating a user) | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Submitting a user s quiz (impersonating a user) | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Quizzes    »    Viewing quizzes   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Submitting a user s quiz (impersonating a user)                       
        
        
       
        
     
              
	If a user starts a quiz but does not submit the attempt, the quiz displays as "in progress" on the Grade Quiz page, and the    Enter Quiz as User  icon appears beside the attempt. If you want to submit the quiz for the user:
 

  
		Click the    Enter Quiz as User  icon beside the quiz attempt in progress.
	 
	 
		Click  Go to Submit Quiz .
	 
	 
		Click  Submit Quiz . You will return to the Grade Quiz page and stop impersonating the user.
	 
  
	 Note  The option to impersonate a user and submit the user's quiz is not available if your organization turns on User Privacy.
 
     Audience:     Instructor       

    
           

                   ‹ Viewing quizzes 
        
                   up 
        
                   Viewing quiz statistics › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Quizzes  
  
      Participating in Quizzes    Using Quizzes    Managing quiz questions and sections    Viewing quizzes    Submitting a user s quiz (impersonating a user)    Viewing quiz statistics    Viewing quiz reports      
                  
           
         

       
     

    
    
    
   
 
   
 
