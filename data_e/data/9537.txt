The Profile of Dipankar Dasgupta   
 
 The Profile of Dipankar Dasgupta 
 
 




 
 



 


 

 
 Dr. Dipankar Dasgupta 
 

 
 * Research Sites * 
 
 
  Artificial Immune Systems (AIS)  
  Center for Information Assurance (CfIA)  
 
 
            


 
 
 
Dr. Dipankar Dasgupta 
333 Dunn Hall 
Memphis, TN 38152-3240 
phone: (901) 678-4147 
fax: (901) 678-1506 
 dasgupta@memphis.edu 
 
 
 
  Dr. Pat E. Burlison  Professor of   Computer Science  
 Director,  Center for Information Assurance  
 Director,  Intelligent Security Systems Research Laboratory  
 


       
  Elevated to IEEE Fellow(Batch of 2015) 
   Distinguished ACM Speaker  
  Recipient of 2012 Willard R. Sparks Eminent Faculty Award.  
 
 
  Advisory Board Member of  MIT in Cyber Security   
  Editorial Board of journals  


 

 Evolutionary Intelligence, Springer-Verlag 

 Evolutionary Optimization, Polish Academy of Science.  

 Recent Patents on Computer Science, online journal Bentham Science Publishers Ltd.  

 Swarm and Evolutionary Computing - Elsevier Press 
 
 
  Announcement:  
 
  Research Assistant Professor Position  (in Cyber Security) Available
  
 
 

 
 * Principal Investigator * 
 
 
  Act Online  
 
 
      


 

 
 
  Home  
  Events 
	 
	  News  
	  Professional Activities  
	  Invited Talks  
	 
 
  Research 
	 
	  Interests 
		 
		  Artificial Immune Systems  
		  Evolutionary Computation  
		  Immunological Computation  
		  Intrusion Detection  
		  Fault Detection  
		 
	 
	  Projects 
		 		
		  Current Projects  
                  Previous Projects  
		 
	 
	
	  Grants  
	  Publications  
 
	 
 
  Teaching 
	 
	  Courses  
	 
 

  Staff/Students  
  Books 
         
          AUA Book  
          IC Book  
          AIS Book  
          EC Book  
         
 
  Recognitions 
         
          Awards  
          Videos  
	 
 
  About  
  
 

 
 
Dr. Dasgupta will Organize IEEE Symposium on  Computational Intelligence in Cyber Security (CICS 2017)  at Hawaii, USA from November 27-December 1, 2017.
Program Committee Member of the 1st IEEE International Workshop on  Cyber Resiliency Economics (CRE 2016)  , Vienna, Austria, August 1-3, 2016.
Prof. Dasgupta will give an invited talk at the  Computer Science Department, University of Tennessee, Knoxville, TN, April 7, 2016
   
Prof. Dasgupta will present a research paper at 11th Annual Cyber and Information Security Research  (CISR)  Conference will be held at the conference center at Oak Ridge National Laboratory, Oak Ridge, TN, April 4 - 6, 2016.
   
Prof. Dasgupta will give invited talk at  Regional Symposium  "Graduate Education and Research in Information Security",'GERIS'16, on  March 8, 2016, at Binghamton University,Binghamton, New York.
   
Announcement for the available position in  Research Assitant Professor  (in Cyber Security)
   
Prof. Dasgupta was interviewed by a local TV Channel (FOX 13) and telecast on Feb. 19, 2016.  Click here for Video. 
   
Organized  "Cybersecurity Certificate Course"  foundational program at FedEx Institute of Technology,UofM, February 1-5, 2016.
   
Prof. Dasgupta gave an invited talk on  5th International Conference on Fuzzy and Neural Computing , FANCCO-2015, December 16-19, 2015.
   
Cluster to Advance Cyber Security & Testing (CAST)  hosted  Cybersecurity Lightning Talks  at the FedEx Institute of Technology, afternoon of December 3, 2015
   
CfIA Receives Cyber Security Training Grant from FEMA
   
UofM's CfIA Will Develop Course for  Mobile Device Security and Privacy Issues 
   
Prof. Dasgupta gave an invited talk on  Adaptive Multi-Factor Authentication  at the  Department of Electrical Engineering and Computer Science and CASE Center, Syracuse University, Syracuse, NY 13224-5040 November 18, 2015
   
Organize a Symposium on Computational Intelligence in Cyber Security (CICS) at IEEE Symposium Series on Computational Intelligence ( SSCI, ), December 7-10, 2015 at Cap Town, South Africa
   
Gave keynote speech at St. Louis at  Cyber Security workshop (STL-CyberCon) , University of Missouri-St. Louis, November 20, 2015
   
Prof. Dasgupta attended the  NIST-NICE  conference at San Diego from November 1-4, 2015
   
Prof. Dasgupta gave an invited talk at 9th  International Research Workshop  on Advances and Innovations in Systems Testing at FedEx Institute of Technology, the University of Memphis, October 20, 2015
   
Our   Cyber Security Team  got a   second position   on Cyber Defense Competition  @CANSec 2015 , held on 24th October at University of Arkansas at Little Rock
  
 

 


 List of Funded Projects: 

  Project Title : A Negative Authentication System (Funded from the Washington, D.C.-based Intelligence Advanced Research Projects Activity (IARPA) in collaboration with Massachusetts Institute of Technology (MIT) 

 This research explores a new paradigm in user authentication

 in accessing in order to improve the security of computer

 systems.  

    Click here for details    



  Project Title :Adaptive Cyber-security Training (ACT) Online  

 The Adaptive Cyber-security Training (ACT) project is in collaboration with Vanderbilt University and Sparta Corporation and is involved in the development and delivery of a multi-level, multi-track cyber security training curriculum  

    Click here for details    



  Project Title : Game Theoretic Approaches to Protect Cyberspace (Funded from the Office of Naval Research (ONR): April '09) 



 This research explores the applicability of game theoretic approaches to address the network security issues. Thus the goal of the research is to design a solution for malicious network attacks using game theory. This ONR grant was started with Dr. Sajjan Shiva as the PI and Dr. Dipankar Dasgupta and Dr. Qishi Wu as co-PIs. 

  Project Title :Task-Based Sailor Assignment Problem( GenoSAP-III)(Funded by US Army research office,Period: September 2008 - June 2009) 



  Click here for details   



  Project Title : Immunity-Based Intrusion Detection Systems (Funded by DARPA, Period: April 2000 - December 2004) 



 The goal of this project was to develop an intelligent multi-agent system for intrusion/anomaly detection and response in networked computers. The approach was inspired by the defense mechanisms of the immune system that is a highly distributed in nature. In this approach, immunity-based agents roam around the machines (nodes or routers), and monitor the situation in the network (i.e. look for changes such as malfunctions, faults, abnormalities, misuse, deviations, intrusions, etc.). These agents can mutually recognize each other's activities and can take appropriate actions according to the underlying security policies. Specifically, their activities are coordinated in a hierarchical fashion while sensing, communicating and generating responses.  Moreover, such an agent can learn and adapt to its environment dynamically and can detect both known and unknown intrusions. 



  Project Title : Anomaly Detection using a Technique Inspired by the Immune System (Funded by ONR, Period: May 1999 - August 2001). 



 We investigated an immunity-based anomaly detection algorithm for monitoring significant changes in time series data and for data quality analysis. This anomaly detection algorithm incorporates probabilistic methods motivated by the negative selection mechanism of the immune system to detect deviations from historical behavior patterns. In particular, the detection system learns the knowledge of the domain from historical data set to generate probabilistically a set of pattern detectors that can detect any abnormalities in the behavior pattern of the monitored data series. Moreover, the detection system is distributed and dynamic (can be updated by generating a new set of detectors as the behavior shifts due to changes in organizational or operational environments). The adaptive nature of this algorithm makes it superior to the static approaches that in current practice. 



  Project Title : Preliminary Research on Immunity-Based Computational Techniques (NSF SGER grant, Period: March 2001 - February 2002). 



 The PI conducted a preliminary investigation of immunity-based computational techniques to pave the way for more complex studies of this subject in the future. The ultimate goal of this research was to develop computational techniques inspired by the natural immune system for solving real-world science and engineering problems. The natural immune system is a distributed novel-pattern recognizer, which uses intelligent mechanisms to detect a wide variety of antigens (novel patterns). From the computational point of view the immune system uses learning, memory, and associative retrieval to solve recognition and classification tasks. The immune system is a subject of great research interest, not only in the hope of finding cures for many diseases but also as a means for understanding its powerful information processing capabilities. In the current project the PI will investigate immunological principles, explore the underlying concepts and mechanisms, and take initial steps towards the development of intelligent computational techniques for solving problems in the field of science and engineering. 



  Project Title : Instruments for Systems, Software, and Database Research: NSF Research Instrumentation Grant (Jointly), Period: June 1999 - May 2001. 



 This instrumentation grant was used to set up a (UNIX-based) computational lab to support a number of research projects of four co-principal investigators. The intent was to acquire the equipment that will comprise a laboratory dedicated to specific research in networking, systems, software, and database.  All these research projects use the equipment in various ways and share of the laboratory.  These help to reduce overall costs and enhance the quality of research through improved opportunities for collaboration. 


 

 
 






 
 
