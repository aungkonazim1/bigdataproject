Removing sharing permissions | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Removing sharing permissions | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    ePortfolio    »    Understanding the basic concepts in sharing   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Removing sharing permissions                       
        
        
       
          
     
           Printer-friendly version       
	Remove permissions from an item
 

  
		Click   ePortfolio  on the navbar or in the My Settings widget.
	 
	 
		On the My Items page, click   Share  from the context menu of the item you want to modify permissions for.
	 
	 
		Click the  ​  Remove  icon next to the user or group of users you want to remove permissions from.
	 
  
	Remove permissions in a sharing group
 

  
		Click   ePortfolio  on the navbar or in the My Settings widget.
	 
	 
		Click  Sharing Groups  from the tool navigation.
	 
	 
		Select the group you want to modify permissions for by clicking its name.
	 
	 
		Click the  ​  Remove  icon next to the user or group of users you want to remove permissions from.
	 
  
	 Note  You cannot modify sharing groups that your course or organization shares with you.
 

 
	Delete a sharing group
 

  
		Click   ePortfolio  on the navbar or in the My Settings widget.
	 
	 
		Click  Sharing Groups  from the tool navigation.
	 
	 
		Click the   Delete  icon beside the group you want to delete.
	 
  
	 Note  You cannot delete sharing groups that your course or organization share with you.
 

 
	Temporarily hide an item from all users
 

  
		On the My Items page, click   Share  from the context menu of the item you want to hide.
	 
	 
		Click  Show Display Options  and change the  Visibility  options available.
	 
  
	 
		 Tip  Temporarily hide an item when:
	 

	  
			You want to make changes to it and don’t want others to see it in draft stages.
		 
		 
			You want to make it available on a specific, predetermined date and want to set up sharing permissions in advance.
		 
	  

     Audience:    Learner      

    
           

                   ‹ Setting up sharing groups 
        
                   up 
        
                   Ignoring and restoring items from users › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        ePortfolio  
  
      ePortfolio basics    Using artifacts    Creating and editing presentations    Creating collections    Managing comments and assessments in ePortfolio    Understanding the basic concepts in sharing    Viewing items shared with you    Sharing with internal and external users    Creating quicklinks to ePortfolio items    Sharing permission options    Understanding cascading permissions    Setting up sharing groups    Removing sharing permissions    Ignoring and restoring items from users      Importing and exporting items    Sharing items within courses    Using form templates    Integrating ePortfolio with Content     Assessing ePortfolio content    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
