Staff - Libraries - University of Memphis    










 
 
 
     



 
    
    
    Staff  - 
      	Libraries
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			University Libraries
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Services  
                          Resources  
                          Instruction  
                          Special Collections  
                          Technology  
                      
                      
                         
                            Administration   
                            
                               
                                  
                                   About Us  
                                        
                                         Staff  
                                        
                                         Hours and Location  
                                     
                                  
                                   Library Faculty and Staff Portal  
                                   Libraries Standing Committees  
                                   Libraries Organizational Chart  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Libraries
                              	  
                          
                              	Administration
                              	  
                         Staff  
                      
                   
                   
                       
                      
                     
                     		
                     
                      
                     
                      
                        
                         
                           
                            
                              
                                Bess Robinson, Interim Dean of University Libraries Associate Professor     merobnsn@memphis.edu   Phone: (901) 678-2201 Office: McWherter Library: RM 203A
                                 
                                   Responsibilities:  The interim dean has administrative responsibility for planning, budgeting, and decision
                                    making in support of the programs and services of the University Libraries. She represents
                                    the University Libraries on campus, in the community, and on the state and national
                                    levels.
                                  
                                 
                               
                              
                            
                           
                            
                              
                               
                                  
                               
                              
                            
                           
                            
                              
                                 Jill Green, Administrative Associate     jdgreen6@memphis.edu   Phone: (901) 678-8219 Office: McWherter Library: RM 203H
                                 
                                   Responsibilities:  Provides administrative support to the Dean of University Libraries.
                                  
                                 
                               
                              
                            
                           
                            
                              
                                 
                              
                            
                           
                            
                              
                                Shanika Jones, Business Officer II    snjones8@memphis.edu   Phone: (901) 678-2209 Office: McWherter Library: RM 203E
                                 
                                   Responsibilities:  Oversees the overall budget of the University Libraries as well as specific project
                                    related budgets
                                  
                                 
                               
                              
                            
                           
                            
                              
                               
                                  
                               
                              
                            
                           
                            
                              
                               
                                  
                               
                              
                            
                           
                            
                              
                                Justin London, Mail Clerk    jlondon1@memphis.edu   Phone: (901) 678-5436 Office: McWherter Library: RM 126B
                                 
                                   Responsibilities:  This position is responsible for the day-to-day operations of the University Libraries’
                                    Receiving Room assuring the proper handling of incoming and outgoing mail and deliveries.
                                  
                                 
                               
                              
                            
                           
                            
                              
                                Max Pennington, Departmental Support Associate    mppnngt@memphis.edu   Phone: (901) 678-4310 Office: McWherter Library: RM 207
                                 
                                   Responsibilities:  
                                 
                               
                              
                            
                           
                            
                              
                                 Sharon Prager, Operations Associate    seprager@memphis.edu  Phone: (901) 678-2201 Office: McWherter Library: RM 204
                                 
                                  Responsibilities: Provides administrative assistance to the Business Officer II and
                                    the Administration Office.
                                  
                                 
                               
                              
                            
                           
                            
                              
                                James Singleton, Coordinator of Building Management    jesngltn@memphis.edu   Phone: (901) 678-8202 Office: McWherter Library: RM 206
                                 
                                   Responsibilities:  Supervises the physical maintenance of the McWherter Library building.
                                  
                                 
                               
                              
                            
                           
                            
                              
                                Anna Swearengen, Community Outreach Associate    mswrngen@memphis.edu   Phone: (901) 678-2744 Office: McWherter Library: RM 203F
                                 
                                   Responsibilities:  Provides administrative assistance to the Friends of the University Libraries, the
                                    Associate Dean of University Libraries, the Assistant to the Dean for Community Engagement,
                                    and the Administration Office.
                                  
                                 
                               
                              
                            
                           
                            
                              
                               
                                  
                               
                              
                            
                           
                         
                        
                      
                     
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Administration 
                         
                            
                               
                                About Us  
                                     
                                      Staff  
                                     
                                      Hours and Location  
                                  
                               
                                Library Faculty and Staff Portal  
                                Libraries Standing Committees  
                                Libraries Organizational Chart  
                            
                         
                      
                      
                      
                         
                            
                                Ask-a-Librarian  
                               Got a question? Got a problem? Ask Us! 
                            
                            
                                Interlibrary Loan  
                               Request books, articles, and other research materials from other libraries. 
                            
                            
                                Reserve space  
                               Group study and presentation spaces available in the library. 
                            
                            
                                My Library Account  
                               Review your library account for due dates and more. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


