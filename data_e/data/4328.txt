Ruby Booth - BIT - University of Memphis    










 
 
 
     



 
    
    
    Ruby Booth - 
      	BIT
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - Department of Business Information and Technology
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          BIT  
                          Programs  
                          Faculty  
                          Research  
                          Prospective Students  
                          Current Students  
                          FCBE  
                      
                      
                         
                            Ph.D Students   
                            
                               
                                  
                                   Cody Behles  
                                   Ruby Booth  
                                   Prajakta Kolte  
                                   He Li  
                                   Soham Sengupta  
                                   Yao Shi  
                                   Stephanie Totty  
                                    Sungjin Yoo   
                                    Qiunan Zhang   
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	BIT
                              	  
                          
                              	Ph.D Students
                              	  
                         Ruby Booth 
                      
                   
                   
                       
                      
                     
                     	
                     
                     		  
                      
                        
                        
                         
                           
                           
                                                                
                              
                              
                               
                              
                              
                            
                           
                           
                            
                              
                               
                                 
                                 Ruby Booth
                                 
                               
                              
                              
                               
                                 
                                 Ph.D Candidate, DEPARTMENT OF BUSINESS INFORMATION AND TECHNOLOGY
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Phone 
                                    
                                     
                                       
                                       (901) 678-2478
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Email 
                                    
                                     
                                       
                                       rbooth@memphis.edu
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Fax 
                                    
                                     
                                       
                                       (901) 678-4151
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Office 
                                    
                                     
                                       
                                       363 Fogelman College Class Bldg
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Office Hours 
                                    
                                     
                                       
                                       
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 										
                                 
                                 
                                   Website  
                                 
                                   CVS  
                                 
                                   SSRN  
                                                                         
                                 
                               
                              
                            
                           
                           
                         
                        			 
                        			  
                        
                        
                         About Ruby Booth 
                        
                         Ruby first joined the University of Memphis as a member of the staff, serving as a
                           Systems Support Specialist I with the Center for Research in Educational Policy (CREP)
                           where she was responsible for maintaining projects, instruments, and accounts within
                           a secure survey database as well as for managing the Center's website and social media
                           presence.
                         
                        
                         In 2013, she entered Fogelman Business College's MIS program as a Masters student
                           and has been accepted into its Ph.D program. Miss Booth has fifteen years professional
                           writing experience, and received her B.A. in the craft of poetry. Her personal interests
                           include books of all sorts, board games, cryptomythology, and the Future.
                         
                        
                         Education 
                        
                         M.S., Business Administration Management of Information Systems, University of Memphis,
                           Memphis, Tennessee.  B.A., Creative Writing, Rhodes College, Memphis, Tennessee. 
                         
                        
                          Interests  
                        
                         Teaching Interests 
                        
                         Cyber Security; Critical Thinking and Project Management; Writing for Publication;
                           Cultural Expectation Assessment and Moderation
                         
                        
                         Research Interests 
                        
                         Cyber Security and Information Assurance; Cyber Ethics; Women and Minorities in STEM;
                           IT Identity; Non-anthropocentric AI Development
                         
                        
                         Selected Research 
                        
                         Journal Publications 
                        
                         Gillenson, M., Racer, M., Zhang, X., Booth, R., and Dugan, J. 2017. "A Heuristic Method
                           for Scheduling Requirements Implementation in Agile Software Development Projects."
                            Journal of Information Technology Management  (27:4), pp.169-178.
                         
                        
                         Conference Proceedings 
                        
                         Booth, R. E "The Effect of Freedom of Expression and Access to Information on the
                           Relationship between ICTs and the Well-being of Nations," Proceedings of the  23nd Americas Conference on Information Systems , 2017.
                         
                        
                         Shi, Yao, Booth, R. E., and Simon J.C. "The Iterative Effect of IT Identity on Employee
                           Cybersecurity Compliance Behaviors," Proceedings of the  23nd Americas Conference on Information Systems , 2017.
                         
                        
                         Booth, R. E., Richardson, S. M., and Simon J.C. "Security Risks Related to Employee
                           'Extra-Role' Creation of an 'Online-persona'," Proceedings of the  22nd Americas Conference on Information Systems , 2016.
                         
                        
                         Booth, R. E., Richardson, S. M., and Simon J.C. "Cybersecurity Employment Pipelines:
                           Identifying Successful Paths to Careers in Cybersecurity for Under-represented Groups,"
                            1st Annual Research Workshop on Advances   Innovations in Cyber Security . 2016.
                         
                        
                         Booth, R. E., Richardson, S. M., and Simon J.C. "Cybersecurity Employment Pipelines:
                           Identifying Successful Paths to Careers in Cybersecurity for Under-represented Groups,"
                            Computer Research Association-Women Workshop , 2016. (Abstract 18). Poster presentation.
                         
                        
                         Technical Reports 
                        
                         Murphy, K. E. and Booth, R.E. 2014. "Supplemental Educational Services in the State
                           of Illinois 2011-2012 Summary of Provider Effectiveness." Memphis, TN: The University
                           of Memphis, Center for Research in Educational Policy.
                         
                        
                         Murphy, K. E. and Booth, R.E. 2014. "Supplemental Educational Services in the State
                           of Illinois 2012-2013 Summary of Provider Effectiveness." Memphis, TN: The University
                           of Memphis, Center for Research in Educational Policy.
                         
                        
                           
                        
                        
                      
                     								
                     
                     
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Ph.D Students 
                         
                            
                               
                                Cody Behles  
                                Ruby Booth  
                                Prajakta Kolte  
                                He Li  
                                Soham Sengupta  
                                Yao Shi  
                                Stephanie Totty  
                                 Sungjin Yoo   
                                 Qiunan Zhang   
                            
                         
                      
                      
                      
                         
                            
                                BIT News  
                               Publications, recognitions and spotlights about faculty and students of the BIT department. 
                            
                            
                                BIT Events  
                               Find out what's happening at the department and visit our weekly colloquium. 
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


