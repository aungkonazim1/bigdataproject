Enrollment Verification Request - Registrar - University of Memphis    










 
 
 
     



 
    
    
    Enrollment Verification Request  - 
      	Registrar
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Registrar
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Register  
                          Students  
                          Veterans  
                          Faculty   Staff  
                          Forms  
                      
                      
                         
                            Students   
                            
                               
                                  
                                   Students  
                                   Enrollment Verification  
                                   Grades  
                                         Overview  
                                         GPA and Grading Scale  
                                         Grade Change  
                                         FAQ  
                                     
                                  
                                   Transcripts  
                                         Overview  
                                         Official  
                                         Unofficial  
                                         Lambuth University  
                                         FAQ  
                                     
                                  
                                   FERPA and Student Educational Records  
                                         Annual FERPA Notification  
                                         Release of Student Education Record  
                                     
                                  
                                   Changing Personal Data  
                                   Withdrawing from UofM  
                                   Self Service Guides  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Registrar
                              	  
                          
                              	Students
                              	  
                         Enrollment Verification Request  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Enrollment Verification Request 
                     
                      Use Self Service to request either an enrollment verification for the current term
                        or your enrollment history, and to   check on the status of your request  .
                      
                     
                      
                        
                         Click on the  Student  page after you login to the portal (in menu bar at the top of the screen).   
                        
                         Locate the  Banner Self-Service  channel and expand the  Banner Self-Service  --   Student  --   Student Records  folders (by clicking on the folder icons); then click on the  Request Enrollment Verification  link.   
                        
                         The initial Enrollment Verification screen appears; definitions of the various Verification
                           Types that are and will be available to you appear at the top of the screen. Enter
                           your basic selections:
                           
                            
                              
                                Term:  Select the term you want from the drop-down box.
                               
                              
                                Verification Type:  Select  Pre-Term ,  Current Enrollment ,  Enrollment History , or  Term Grade Report .
                               
                              
                                Number of Copies:  Specify the number you need.
                               
                              
                               Click  [Continue] .   
                              
                            
                               
                        
                         The Enrollment Verification Delivery screen appears. Select a delivery method for
                           your verification and click [Continue].      
                        
                         The Address screen now appears; specify an address or a FAX number. Specify an address
                           if your earlier requested the "Mail to" or "Hold for Pick-Up" options. You may either
                           select one of the addresses on file for you or enter a new address.  
                           
                            
                              
                                Note : An address is needed for "Hold for Pick-Up" because the Registrar's Office needs to
                                 mail the verification to you in case you fail to pick it up.
                               
                              
                            
                           
                            Specify a FAX number if you earlier requested the FAX option. 
                           
                            Click the [Continue] button. 
                               
                        
                         A summary of your enrollment verification request appears. If something is not right,
                           go back to a previous screen and correct it; otherwise, click [Submit Request].      
                        
                         A confirmation of your request--the Signature screen--appears.       
                        
                         Logout: Click on the logout icon in the upper right-hand corner of the screen when
                           you are finished using the portal.   
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Students 
                         
                            
                               
                                Students  
                                Enrollment Verification  
                                Grades  
                                      Overview  
                                      GPA and Grading Scale  
                                      Grade Change  
                                      FAQ  
                                  
                               
                                Transcripts  
                                      Overview  
                                      Official  
                                      Unofficial  
                                      Lambuth University  
                                      FAQ  
                                  
                               
                                FERPA and Student Educational Records  
                                      Annual FERPA Notification  
                                      Release of Student Education Record  
                                  
                               
                                Changing Personal Data  
                                Withdrawing from UofM  
                                Self Service Guides  
                            
                         
                      
                      
                      
                         
                            
                                Calendars  
                               View Academic Year calendars; semester dates/deadlines; final exam schedules. 
                            
                            
                                Transcripts  
                               Request copies of your official transcript. 
                            
                            
                                Notice!  
                                Check for alerts and reminders. 
                            
                            
                                Contact Us   
                               Need additional assistance or information? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


