Importing course content | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Importing course content | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    ePortfolio    »    Adding artifacts   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Importing course content                       
        
        
       
        
     
              
	Track your progress over time by including your course work in your ePortfolio. You can then review your work at any point in the future and compare it to other assignments and courses. You can also add course content artifacts to your ePortfolio directly from your courses. Click  Add to ePortfolio  while viewing grades, competencies, quiz results, or dropbox folder feedback.
 

 
	 Note  If you import quiz or dropbox folder results into your ePortfolio, any learning objectives associated with the quiz or dropbox folder also import into your ePortfolio pre-associated with your course content artifacts.
 

  
		On the My Items page, click  Course Results  from the Add button.
	 
	
 
		If you have more than one role in the system, select the role you want to import course results for from the  Enrolled As  drop-down list.
	 

	 
		Click on the name of the course you want to import results from.
	 
	 
		Select the items you want to import, then click  Next .
	 
	 
		Enter a  Name  and  Description  for each item.
	 
	 
		Add any tags you want the artifact to have.
	 
	 
		Click  Save .
	 
  
	 Tip   Click  Apply Tags to All Artifacts  to add a set of tags to all items on the page.
 
     Audience:     Learner       

    
           

                   ‹ Adding form responses 
        
                   up 
        
                   Using learning objectives in ePortfolio › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        ePortfolio  
  
      Understanding the main pages of ePortfolio    Adding artifacts    Adding linked web addresses    Uploading files    Creating web documents    Adding audio recordings    Adding form responses    Importing course content    Using learning objectives in ePortfolio      Using reflections    Creating presentations    Creating collections    Understanding assessment types in ePortfolio    Understanding the basic concepts in sharing    Importing and exporting items    Sharing items within courses    Using form templates    Integrating ePortfolio with Content     Assessing ePortfolio content    
                  
           
         

       
     

    
    
    
   
 
   
 
