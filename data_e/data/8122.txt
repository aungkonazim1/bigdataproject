Professor Thomas Hagen   

 
 
 
 
 
 
 
 
 
 Professor Thomas Hagen 
 
 
 
  

 
 
  
 

 

 

 
  
   
              
   
    
              
   
  
 
  
     
  
 
 

       

  

 
  
    
              
   
  
 

       

  

 
  
    
          
          
          
                
         
   
     
  
    
           
   
 
    
      Welcome to my
  Homepage!      
   
   
    
      
         Contact Information      
     
 
       
  
       
    
   
    
      
       Thomas Hagen,
    PhD 
    Professor       
       Department of
    Mathematical Sciences 
    The University of Memphis 
     Memphis , TN 38152-3240       
           
     
       
  
      
               
     
      
        
         Office:       
       
 
        
         367 Dunn Hall       
       
      
 
      
        
         Phone:       
       
 
        
         (901) 678-2481       
       
      
 
      
        
         Fax:       
       
 
        
         (901) 678-2480       
       
      
 
      
        
         E-mail:       
       
 
        
          thagen@memphis.edu        
       
      
 
     
     
    
  
   
   
         
  
   
   
    
      
        Research       
     
 
       
  
      
        Teaching       
     
    
 
    
      
     
 
      
        
                
       
        
         Research Interests: 
      Analysis, Partial Differential Equations, Rheology     
         and Fluid Dynamics, Population
      Dynamics       
       
      
   
      
       
                
       
        
          List of
      Publications        
       
      
  
     
            
     
 
       
 
      
     
 
      
        
                
       
        
          Office
      Hours        
       
      
  
      
       
                
       
        
          Lecture
      Times/Locations  
              
       
      
     
  
    
    
      
       Main Links      
     
 
       
  
      
       Search      
     
    
 
    
      
     
 
      
        
                
       
        
          American
      Mathematical Society  (AMS)       
       
      
  
      
       
                
       
        
          The
      Society for Industrial and Applied Mathematics  (SIAM)     
       
      
  
      
       
                
       
        
          The Society of Rheology  ( SoR )        
       
      
  
      
       
                
       
        
          The University of Memphis  ( UofM )       
       
      
  
      
       
                
       
        
          Department of Mathematical Sciences 
      ( UofM )       
       
      
  
     
            
     
 
       
 
      
     
 
      
        
                
       
        
          Google        
       
      
  
      
       
                
       
        
          Yahoo  
              
       
      
  
      
       
                
       
        
          People  ( UofM )       
       
      
  
      
       
                
       
        
          Web Pages  ( UofM ) 
              
       
      
  
     
          
     
    
 
   
   
        
   
  
 

                                              
  Home  |   Research   |   Teaching   |   Links         

   Date:
 
07/22/14
  
 Thomas Hagen       

 

 
 

 
