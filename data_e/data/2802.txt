Clinical Alumni News - Department of Psychology - University of Memphis    










 
 
 
     



 
    
    
    Clinical Alumni News - 
      	Department of Psychology
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Psychology
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Undergraduate  
                          Graduate  
                          People  
                          Centers  
                          Research  
                          Resources  
                      
                      
                         
                            Graduate   
                            
                               
                                  
                                   PhD in Clinical Psychology  
                                        
                                         About the Program  
                                        
                                         Clinical Faculty  
                                        
                                         Child Clinical  
                                        
                                         Clinical Health Psychology  
                                        
                                         Psychotherapy Research  
                                        
                                         Admission  
                                        
                                         Clinical Student Handbook  
                                        
                                         Clinical Forum Schedule  
                                        
                                         Clinical Program Announcements  
                                        
                                         Clinical Alumni News  
                                     
                                  
                                   PhD in Experimental Psychology  
                                         About the Program  
                                         Experimental Faculty  
                                         Cognitive  
                                         Behavioral Neuroscience  
                                         Required Courses  
                                         Admission  
                                         Financial Support  
                                     
                                  
                                   PhD in School Psychology  
                                         About the Programs  
                                         School Faculty  
                                         Historical Statement  
                                         Financial Support  
                                         M.S./Ph.D. Program Philosophy and Training Model  
                                         M.S./Ph.D. Program Goals and Objectives  
                                         Doctoral Handbook  
                                         School Psychology Association  
                                         Child   Family Studies Research Group  
                                     
                                  
                                   MA/Ed.S. in School Psychology  
                                         About the Programs  
                                         School Faculty  
                                         M.A./Ed.S. Program Goals  
                                         Historical Statement  
                                         Financial Support  
                                         School Psychology Association  
                                         What is a School Psychologist?  
                                         Child   Family Studies Research Group  
                                     
                                  
                                   Masters in General Psychology  
                                         About the Program  
                                         Program Goals  
                                         Required Courses  
                                         Admission  
                                     
                                  
                                   Apply to Psychology Graduate Programs  
                                         Instructions for Applying  
                                         Department Application  
                                         Graduate School Application  
                                     
                                  
                                   Graduate Student Coordinating Committee  
                                         About GSCC  
                                         GSCC Concerns/Complaints  
                                         GSCC Travel Funding  
                                     
                                  
                                   Graduate Program Forms and Resources  
                                   Clinical Student Admissions, Outcomes and Other Data  
                                   School Student Admissions, Outcomes and Other Data  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of Psychology
                              	  
                          
                              	Graduate
                              	  
                         Clinical Alumni News 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Clinical Alumni News 
                     
                      Courtney Peasant, Ph.D., Clinical, 2014 
                     
                       Courtney is a research clinical psychologist at RTI International, headquartered in
                        the Research Triangle Park in North Carolina. At RTI, a nonprofit organization that
                        provides research and technical services, she collaborates with team members in the
                        Substance Use, Gender and Applied Research (SUGAR) program to develop, evaluate and
                        scale-up women-focused HIV prevention interventions for substance-using women.
                      
                     
                      Her research investigates how the intersection of gender-based violence, mental health
                        and substance use influences HIV-related behaviors among minority populations and
                        explores how to increase the sustainability of HIV prevention interventions.
                      
                     
                      Courtney currently serves on the Governing Board of the Society for the Analysis of
                        African American Public Health Issues, a special interest group of the American Public
                        Health Association, and is the HIV/AIDS topic chair for the Society of Behavioral
                        Medicine.
                      
                     
                      Prior to joining RTI, Courtney completed an NIMH-funded T-32 postdoctoral fellowship
                        at Yale University's Center for Interdisciplinary Research on AIDS.
                      
                     
                      During graduate school, Courtney worked in the Community Outreach Lab to implement
                        and evaluate community-based interventions for vulnerable youth and in the HABIT lab
                        to help implement an NIAAA-funded clinical trial for an intervention to reduce alcohol
                        consumption among heavy-drinking college students. She also worked extensively in
                        the Department of Psychology and in Infectious Diseases at St. Jude Children's Research
                        Hospital as a research assistant.
                      
                     
                      In the Memphis community, Courtney co-led initiatives to improve mental health services
                        received by individuals living with HIV and mentored young girls in the Westwood community.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Graduate 
                         
                            
                               
                                PhD in Clinical Psychology  
                                     
                                      About the Program  
                                     
                                      Clinical Faculty  
                                     
                                      Child Clinical  
                                     
                                      Clinical Health Psychology  
                                     
                                      Psychotherapy Research  
                                     
                                      Admission  
                                     
                                      Clinical Student Handbook  
                                     
                                      Clinical Forum Schedule  
                                     
                                      Clinical Program Announcements  
                                     
                                      Clinical Alumni News  
                                  
                               
                                PhD in Experimental Psychology  
                                      About the Program  
                                      Experimental Faculty  
                                      Cognitive  
                                      Behavioral Neuroscience  
                                      Required Courses  
                                      Admission  
                                      Financial Support  
                                  
                               
                                PhD in School Psychology  
                                      About the Programs  
                                      School Faculty  
                                      Historical Statement  
                                      Financial Support  
                                      M.S./Ph.D. Program Philosophy and Training Model  
                                      M.S./Ph.D. Program Goals and Objectives  
                                      Doctoral Handbook  
                                      School Psychology Association  
                                      Child   Family Studies Research Group  
                                  
                               
                                MA/Ed.S. in School Psychology  
                                      About the Programs  
                                      School Faculty  
                                      M.A./Ed.S. Program Goals  
                                      Historical Statement  
                                      Financial Support  
                                      School Psychology Association  
                                      What is a School Psychologist?  
                                      Child   Family Studies Research Group  
                                  
                               
                                Masters in General Psychology  
                                      About the Program  
                                      Program Goals  
                                      Required Courses  
                                      Admission  
                                  
                               
                                Apply to Psychology Graduate Programs  
                                      Instructions for Applying  
                                      Department Application  
                                      Graduate School Application  
                                  
                               
                                Graduate Student Coordinating Committee  
                                      About GSCC  
                                      GSCC Concerns/Complaints  
                                      GSCC Travel Funding  
                                  
                               
                                Graduate Program Forms and Resources  
                                Clinical Student Admissions, Outcomes and Other Data  
                                School Student Admissions, Outcomes and Other Data  
                            
                         
                      
                      
                      
                         
                            
                                Psychology Graduate Programs Application  
                               Click on link to Apply to the Department of Psychology Graduate Program 
                            
                            
                                Academic Advising   Resource Center (AARC)  
                                The AARC provides advising to students helping them make the most of their undergraduate
                                 education at the UofM.
                               
                            
                            
                                The Psychological Services Center  
                               PSC provides general outpatient psychotherapeutic and psychological assessment services
                                 to individuals and families
                               
                            
                            
                                Teaching Take-Out  
                               This website is a resource for busy teachers who want to enrich their classes while
                                 preserving the time they need for research and other important professional activities.
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/10/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


