Courses - Department of Chemistry - University of Memphis    










 
 
 
     



 
    
    
    Courses  - 
      	Department of Chemistry
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Chemistry
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          People  
                          Research  
                          Resources  
                      
                      
                         
                            Programs   
                            
                               
                                  
                                   Undergraduate  
                                        
                                         Chemistry Major (B. S.) - Chemistry Concentration  
                                        
                                         Chemistry Major (B. S.) - Biochemistry Concentration  
                                        
                                         Chemistry Major (ACS Certified B. S.)  
                                        
                                         Chemistry Minor  
                                        
                                         Courses and Information  
                                        
                                         Advising  
                                        
                                         Permit Requests  
                                        
                                         Scholarships   Awards  
                                        
                                         Research Opportunities  
                                        
                                         Undergraduate Research Conference  
                                        
                                         NSF REU Program  
                                        
                                         Student Members of ACS (SMACS)  
                                        
                                         Career Options  
                                     
                                  
                                   Graduate  
                                        
                                         Ph. D. Chemistry  
                                        
                                         Master of Science in Chemistry  
                                        
                                         Courses  
                                        
                                         Advising  
                                        
                                         Assistantships  
                                        
                                         Grad Student Association  
                                        
                                         Apply!  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of Chemistry
                              	  
                          
                              	Programs
                              	  
                         Courses  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Courses 
                     
                      
                        
                          6001 : Environmental Chemistry [S]
                         
                        
                         6111: Intermediate Inorganic Chemistry (lecture) [S] 
                        
                          6211 : Advanced Instrumental Analysis [F]
                         
                        
                          6311 : Intermediate Organic Chemistry [F]
                         
                        
                          6315 : Organic Medicinal Chemistry [ST]
                         
                        
                          6382 : Polymer Chemistry [F]
                         
                        
                         6411: Advanced Physical Chemistry [S] 
                        
                         6415: Computational Chemistry [ST] 
                        
                         6416: Molecular Spectroscopy (lecture) [alt S] 
                        
                         6501: Biochemistry Laboratory (cross listed as BIOL 6503) [F] 
                        
                         6511: Biochemistry 1 (lecture, cross listed as BIOL 6511) [F] 
                        
                         6512: Biochemistry 2 (lecture, cross listed as BIOL 6512) [S] 
                        
                          6604 : Instrumental Methods [S]*
                         
                        
                         6613: Materials Synthesis [alt S] 
                        
                         7001/8001: Directed Research [F, S, Su] 
                        
                         7111/8111: Systematic Inorganic Chemistry [alt F] 
                        
                         7112/8112: Structural Inorganic Chemistry [alt F] 
                        
                         7185/8185: Bioinorganic Chemistry [ST] 
                        
                         7211/8211: Advanced Analytical Chemistry 1 [alt S] 
                        
                         7212/8212: Advanced Analytical Chemistry 2 [alt S] 
                        
                          7311/8311 : Advanced Organic Chemistry [alt S]
                         
                        
                          7312/8312 : Synthetic Organic Chemistry [alt S]
                         
                        
                         7307/8307: Nanomaterials [ST] 
                        
                         7314/8314: Heterocyclic Chemistry [ST] 
                        
                         7411/8411: Electronic Structure and Symmetry [ST] 
                        
                         7413/8413: Molecular Spectroscopy [ST] 
                        
                         7414/8414: Advanced Quantum Chemistry [ST] 
                        
                         7600: Introduction to Graduate Study in Chemistry [F] 
                        
                          7711/8711 : Approximate Chemical Modeling Methods [ST]
                         
                        
                         7712/8712: Computational Chemistry Programming [ST] 
                        
                         7713/8713: Advanced Solid State Physics and Chemistry [ST] 
                        
                         7910/8910: Special Problems in Chemistry [F, S, Su] 
                        
                         7911: Presentation [F, S, Su] 
                        
                         7913/8913: Chemistry Seminar [F, S] 
                        
                         7996: Thesis [F, S, Su] 
                        
                         8911: Advanced Presentation [F, S, Su] 
                        
                         9000: Dissertation [F, S, Su] 
                        
                      
                     The symbols [F], [S], and [Su] in the lists below indicate semesters (Fall, Spring,
                     and Summer, respectively) in which courses are usually taught. Some courses (indicated
                     by [ST]) are offered on an irregular basis, usually not more frequently than once
                     every two years.   For the laboratory courses indicated with an asterisk (*) in the list below, the University
                     will assess a $30 material fee in addition to tuition.    Links to useful information:   University Academics    Graduate School    Graduate catalog and graduate course descriptions    2013-2014 Academic Calendars    All Academic Calendars    My Memphis    American Chemical Society Careers    UofM Career Services    What can you do with a degree in Chemistry? 
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Programs 
                         
                            
                               
                                Undergraduate  
                                     
                                      Chemistry Major (B. S.) - Chemistry Concentration  
                                     
                                      Chemistry Major (B. S.) - Biochemistry Concentration  
                                     
                                      Chemistry Major (ACS Certified B. S.)  
                                     
                                      Chemistry Minor  
                                     
                                      Courses and Information  
                                     
                                      Advising  
                                     
                                      Permit Requests  
                                     
                                      Scholarships   Awards  
                                     
                                      Research Opportunities  
                                     
                                      Undergraduate Research Conference  
                                     
                                      NSF REU Program  
                                     
                                      Student Members of ACS (SMACS)  
                                     
                                      Career Options  
                                  
                               
                                Graduate  
                                     
                                      Ph. D. Chemistry  
                                     
                                      Master of Science in Chemistry  
                                     
                                      Courses  
                                     
                                      Advising  
                                     
                                      Assistantships  
                                     
                                      Grad Student Association  
                                     
                                      Apply!  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply to the Department of Chemistry  
                               Now accepting applications for graduate school 
                            
                            
                                Contact us!  
                               Call us (901.678.2621) or stop by the Departmental office at Smith Chemistry Rm 210 
                            
                            
                                Seminar Schedule  
                               Click here for the current seminar schedule 
                            
                            
                                What's new?  
                               Click here to see what's new in our Department 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/30/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


