Online Learning Readiness Assessment - UofM Global - University of Memphis    










 
 
 
     



 
    
    
    Online Learning Readiness Assessment - 
      	UofM Global
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






 

   
   
   
    
    
 
 
   
   
   
   
 

   
   
   
    
      
      
       
 
	
	
	
 
 

 


 
 

 
  
 
	 
		 
			 
				 
					     
				 
				     
				 
					 
						 
							 
								 
									   UofM Home  
										  myMemphis  
										  eCourseware  
										  Webmail  
										  Contact  
								     
								 
								 
									 Search 
									 
									 
									    
									 Site Index 
								 																
								  
							 
						 
					 
					 
						  
							 
								   About UofM Global      
									 
										  UofM Global  
										  Ranked Online Degrees  	
										  The UofM Global Experience  
										  Our Faculty  
										  Tuition   Fees  	
										  Accreditations  
									 
								 
								   Online Degrees      
									 
										  Online Degrees  
										  Undergraduate Degrees  
										  Graduate Degrees  
										  Graduate Certificates  
										  Online Admissions  
										  Contact Us  
									 
								 
								   Online Admissions      
									 
										  Online Admissions  
										  Apply Now   
										  Smart Start  
										  Financial Aid  
										  All Online Services  
									 
								 
								   Current Students      
									 
										  Student Services and Support  
										  Online Advising  
										  Course Registration  
										  Alternative Degree Pathways  
										  Learning Support  
										  Technology Support  
										  Library Services  
										  Career Services  
										  Additional Services  
										  Graduation  
									 
								 
								  Request Info  
							 
						 
					 
				 
			 
		 
		 
			 
				 
					  Online Degrees  
				 
			 
		 
	 
 
 
      
      
      
    
    
       
          
             
                
                   
                       
                           			UofM Global
                           	  
                   
                
             
          
       
       
          
             
                
                   
                      
                         
                             Home  
                             
                                 	UofM Global
                                 	  
                             
                                 	Student Services
                                 	  
                            Online Learning Readiness Assessment 
                         
                      
                      
                         
                            Student Services and Support   
                            
                               
                                  
                                   Student Services and Support  
                                   Online Advising Menu  
                                         Overview  
                                         Degree Planning  
                                         Find My Advisor  
                                     
                                  
                                   Course Registration  
                                         Overview  
                                         Online Course Search  
                                         Undergraduate Catalog  
                                         Graduate Catalog  
                                         Dates and Deadlines  
                                         Tuition   Fees  
                                         Scholarships  
                                     
                                  
                                   Alternative Degree Pathways  
                                         Overview  
                                         Finish Line Program  
                                         Prior Learning Assessment  
                                     
                                  
                                   Learning Support  
                                        
                                         Overview  
                                        
                                         Readiness Assessment  
                                        
                                         Success Strategies  
                                        
                                         Get Connected  
                                        
                                         Online Tutoring  
                                     
                                  
                                   Technology Support  
                                         Overview  
                                         Technology Requirements  
                                         Additional Services  
                                         Online Course Preview  
                                     
                                  
                                   Library Services  
                                         Overview  
                                         Ask-a-Librarian  
                                         Electronic Delivery  
                                         Research Consultations  
                                     
                                  
                                   Career Services  
                                   Additional Services  
                                         Overview  
                                         Adult and Veteran Students  
                                         Disability Resources  
                                     
                                  
                                   Graduation  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                     		
                     
                     
                        
                     
                      SmartStart Readiness Assessment 
                     
                      Smart Start is a three tiered initiative designed to address the needs of adult learners
                        returning to school. It is our aspiration to inform students who have been pre-admitted,
                        currently enrolled to the University of Memphis about their academic preparedness
                        to return to college and provide them with resources and assistance to address their
                        possible deficiencies prior to enrollment into an online course or program.
                      
                     
                      Resources for Preparedness 
                     
                      Intervention 1: Readiness Assessment 
                     
                      Using a mixed methods approach, involving a web based readiness instrument, students
                        will be assessed for "preparedness" and receive a recommended intervention for the
                        following categories: Technical Knowledge, Online Learning Experience, Self-regulation,
                        and Self-Management. The intent of this stage is to determine Prior Learning Assessment
                        (PLA) opportunities (Experiential Learning Credit as well as Credit By Exam), modality
                        match (online vs. face-to-face/hybrid), and support service prescription (tutoring,
                        coaching, etc.).
                      
                     
                      Intervention 2: Preparation 
                     
                      Drawn from the aforementioned Readiness Assessment, a prescribed resource-based, readiness
                        plan will be prepared for each returning student. While past academic performance
                        may provide hints that academic tutoring or coaching be recommended, results from
                        the readiness assessment instrument will direct students to key resources based on
                        their categorical scores (e.g. time management, technology access   skill, etc.).
                      
                     
                      Intervention 3: Practice Zone 
                     
                      Based on the results of the Readiness Assessment and the completed readiness plan,
                        the respondent will be directed to the Practice Zone. Within the Practice Zone, students
                        will be given the opportunity to hone their skills prior to entry into a fully online
                        or hybrid course offered by the University of Memphis. The Zone will include practice
                        course modules and proven academic strategic resources designed to get prospective
                        students ready for the "college experience".
                      
                     
                        Begin the Smart Start Readiness Assessment      
                     
                        SUCCESS STRATEGIES      
                     
                     	
                      
                   
                
                
                   
                      
                         Student Services and Support 
                         
                            
                               
                                Student Services and Support  
                                Online Advising Menu  
                                      Overview  
                                      Degree Planning  
                                      Find My Advisor  
                                  
                               
                                Course Registration  
                                      Overview  
                                      Online Course Search  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Dates and Deadlines  
                                      Tuition   Fees  
                                      Scholarships  
                                  
                               
                                Alternative Degree Pathways  
                                      Overview  
                                      Finish Line Program  
                                      Prior Learning Assessment  
                                  
                               
                                Learning Support  
                                     
                                      Overview  
                                     
                                      Readiness Assessment  
                                     
                                      Success Strategies  
                                     
                                      Get Connected  
                                     
                                      Online Tutoring  
                                  
                               
                                Technology Support  
                                      Overview  
                                      Technology Requirements  
                                      Additional Services  
                                      Online Course Preview  
                                  
                               
                                Library Services  
                                      Overview  
                                      Ask-a-Librarian  
                                      Electronic Delivery  
                                      Research Consultations  
                                  
                               
                                Career Services  
                                Additional Services  
                                      Overview  
                                      Adult and Veteran Students  
                                      Disability Resources  
                                  
                               
                                Graduation  
                            
                         
                      
                      
                     
                     
                      
                                 Need Help? 
                                 
                                     
                                         
											     Call 844-302-3886 
                                         
                                     
                                     
                                         
                                                 Email Us 
                                         
                                     
                                 
                             
                     
                     
                     
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
 
 
 
 
  Full sitemap  
 
 
 
 
  About UofM Global  
 
 
  UofM Global  
  Ranked Online Degrees  	
  The UofM Global Experience  
  Our Faculty  
  Tuition   Fees  	
  Accreditations  
 
  
 
  Online Degrees  
 
 
  Online Degrees  
  Undergraduate Degrees  
  Graduate Degrees  
  Graduate Certificates  
  Online Admissions  
  Contact Us  
 
  
 
 
 
  Online Admissions  
 
 
  Online Admissions  
  Apply Now   
  Smart Start  
  Financial Aid  
  All Online Services  
 
  
  Contact Us  
 
  Tuition  
  Financial Aid  
  
 
	  Current Students  
 
 
  Student Services and Support  
  Online Advising  
  Course Registration  
  Alternative Degree Pathways  
  Learning Support  
  Technology Support  
  Library Services  
  Career Services  
  Additional Services  
  Graduation  
 
  
 
 
 
 
 
   
 
 
   
 
 
   
 
 
 
 
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/29/17 
                                  
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


