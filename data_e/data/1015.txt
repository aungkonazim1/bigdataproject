Curriculum - IMBA - University of Memphis    










 
 
 
     



 
    
    
    Curriculum  - 
      	IMBA
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - International MBA
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Welcome  
                          Admissions  
                          Curriculum  
                          Students  
                          Media  
                          News  
                          FCBE  
                      
                      
                         
                            Curriculum   
                            
                               
                                  
                                   Welcome  
                                   Admissions  
                                         How To Apply  
                                         Tuition and Fees  
                                     
                                  
                                   Curriculum  
                                   Students  
                                         Assistantships  
                                         Morgan Morton Student Travel Enrichment Fund  
                                         Peer Power Scholars Program  
                                         Scholarships  
                                         Study Abroad and Internships  
                                         Learning Outcomes  
                                     
                                  
                                   Media  
                                   News  
                                   Contact Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	IMBA
                              	  
                         
                           	Curriculum
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Curriculum 
                     
                      The IMBA Program is tailored to assist students in meeting their goal of acquiring
                        knowledge of international business, even if they do not have a traditional business
                        background or a second language.
                      
                     
                      The International MBA Program at the University of Memphis offers seven Business Tracks.
                        Five tracks are Business Language Tracks covering Spanish, German, French, Chinese
                        or Japanese. These tracks are for students with three years of foreign language proficiency
                        at the junior year college level. The World Region Business Track is reserved for
                        students who wish to specialize in the practice of business in geographical regions
                        where English is the accepted business language. The U.S. Business Track, reserved
                        for International students only, provides a more thorough exposure to U.S. business
                        and markets.
                      
                     
                      Business Language Track: 
                     
                      Five Country Business Tracks: 
                     
                      
                        
                         Spanish 
                        
                         German 
                        
                         French 
                        
                         Chinese 
                        
                         Japanese 
                        
                      
                     
                      The above tracks are for students with three years of foreign language proficiency
                        at the junior year college level.
                      
                     
                      World Region Business Track: 
                     
                      Reserved for students who wish to specialize in the practice of business in geographical
                        regions where English is the accepted business language.
                      
                     
                      U.S. Business Track: 
                     
                      This track is reserved for International students, only. Students will take classes
                        that provided greater emphasis on U.S. business and markets in addition to the core
                        international business curriculum.
                      
                     
                       Core Courses:  
                     
                      
                        
                         
                           
                            
                              
                               ACCT 7050 
                              
                               Corporate Governance and Business Ethics 
                              
                            
                           
                            
                              
                               ACCT 7080 
                              
                               Financial and Managerial Accounting for Managers 
                              
                            
                           
                            
                              
                               ECON 7100 
                              
                               Economics for Global Executives 
                              
                            
                           
                            
                              
                               FIR 7155 
                              
                               Global Financial Management 
                              
                            
                           
                            
                              
                               SCMS 7110 
                              
                               Quantitative Tools for Managers 
                              
                            
                           
                            
                              
                               SCMS 7313 
                              
                               Global Operations Management 
                              
                            
                           
                            
                              
                               MGMT 7135 
                              
                               Leadership and Team Building 
                              
                            
                           
                            
                              
                               MGMT 7160 
                              
                               Global Strategic Management 
                              
                            
                           
                            
                              
                               MIS 7650 
                              
                               Information Systems for the Global Enterprise 
                              
                            
                           
                            
                              
                               MKTG 7140 
                              
                               Global Strategic Marketing 
                              
                            
                           
                            
                              
                               MKTG 7555 
                              
                               Creativity and Innovation 
                              
                            
                           
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Curriculum 
                         
                            
                               
                                Welcome  
                                Admissions  
                                      How To Apply  
                                      Tuition and Fees  
                                  
                               
                                Curriculum  
                                Students  
                                      Assistantships  
                                      Morgan Morton Student Travel Enrichment Fund  
                                      Peer Power Scholars Program  
                                      Scholarships  
                                      Study Abroad and Internships  
                                      Learning Outcomes  
                                  
                               
                                Media  
                                News  
                                Contact Us  
                            
                         
                      
                      
                      
                         
                            
                                How To Apply  
                               What's your first step?  Find out here! 
                            
                            
                                Earn While You Learn  
                               International MBA students, apply for the Peer Power Scholars Program today and have
                                 your tuition covered!
                               
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


