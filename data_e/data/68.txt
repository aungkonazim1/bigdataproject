Register - Registrar - University of Memphis  Registration is more than adding or dropping classes, learn more about the registration process at the Registrar’s Office.  










 
 
 
     



 
    
    
    Register - 
      	Registrar
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Registrar
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Register  
                          Students  
                          Veterans  
                          Faculty   Staff  
                          Forms  
                      
                      
                         
                            Register   
                            
                               
                                  
                                   Register  
                                   Quick Guide  
                                   Schedule Builder  
                                   Errors/Problems  
                                   Holds  
                                   Permits  
                                   Credit by Exam  
                                   Auditing  
                                   Complete Self Service Guides  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Registrar
                              	  
                         
                           	Register
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Register for Classes 
                     
                      Registration is more than adding or dropping classes: it's a process that takes preparation
                        as well as follow-through in order to make sure that you get best schedule possible
                        with the least amount of trouble. With that in mind, review the process as outlined
                        below. 
                      
                     
                      Before Registration Opens 
                     
                      Dates 
                     
                      Check the appropriate semester   Dates   Deadlines   calendar to determine registration and term dates, and your first day to register.
                      
                     
                      Course Search: 
                     
                        Admitted Students   
                     
                      UofM students should login to the myMemphis portal and search the semester's class
                        offerings using the   Look Up Classes   feature. Consider using the   Schedule Builder   tool to plan and build your course schedule. Class offerings are available online,
                        usually one month before registration opens.
                      
                     
                        General Public   
                     
                      If you are not an admitted student, use the   Class Schedule Search (Dynamic Schedule)  . Class offerings are available online, usually one month before registration opens.
                      
                     
                      Financial Aid 
                     
                      Go to your Account$ tab in the myMemphis portal to review and accept your financial
                        aid awards.
                      
                     
                      Registration Status 
                     
                      Check your Registration Status screen in the portal (Student page). Look in particular
                        at:
                      
                     
                      
                        
                         Holds - You cannot register if you have any Registration holds. Contact the office
                           that placed the hold to determine how you can remove it.   Read about Holds  .
                         
                        
                         Student Status messages - You must be a current student with an Active status for
                           the term. If you see these messages--"you have no registration time ticket," "you
                           require readmission," "your status prevents registration,"--you must   apply for admission   or   readmission   before registering. ( NOTE : When you skip a Fall or Spring term, your status changes to "Inactive" and you must
                           be readmitted.)
                         
                        
                      
                     
                      Advising 
                     
                      If you are a degree-seeking student, you must be cleared to register by your advisor
                        or advising unit. Otherwise, you will be blocked from registering by the "Alternate
                        PIN" screen. Once your advisor clears you, you will not encounter this roadblock again.
                        Visit   Academic Advising    for advising information.
                      
                     
                      Once Registration Opens 
                     
                      Login and Register 
                     
                        Register online through the myMemphis portal   as early as possible for the best course selection; once you are in the portal, look
                        for the  Registration Tools  portlet on your  Student  page. Also:
                      
                     
                      
                        
                         Refer to the   Registration Quick Guide   for an overview of online registration.
                         
                        
                         If you encounter problems trying to register, refer to our   Errors and Problems   page.
                         
                        
                         If you need a permit to register for a particular section, refer to our   Permits   page .  
                        
                           Print your personal class schedule   after you have registered for classes. Check it for accuracy. Look especially at
                           time, days, room, and campus location.
                         
                        
                      
                     
                      Pay 
                     
                      Pay your fees online (select your Account$ page in the portal), or pay in person at
                        the Bursar's Office, 115 Wilder Tower. Refer to the   Bursar's website   for fee payment deadlines and other fee-related information, including fee payment
                        methods, and possible refunds.
                      
                     
                      Before You Go To Class 
                     
                       Reprint  and  review  your schedule just prior to the first day of classes: buildings and room numbers
                        often need to be revised before classes start.
                      
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Register 
                         
                            
                               
                                Register  
                                Quick Guide  
                                Schedule Builder  
                                Errors/Problems  
                                Holds  
                                Permits  
                                Credit by Exam  
                                Auditing  
                                Complete Self Service Guides  
                            
                         
                      
                      
                      
                         
                            
                                Calendars  
                               View Academic Year calendars; semester dates/deadlines; final exam schedules. 
                            
                            
                                Transcripts  
                               Request copies of your official transcript. 
                            
                            
                                Notice!  
                                Check for alerts and reminders. 
                            
                            
                                Contact Us   
                               Need additional assistance or information? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


