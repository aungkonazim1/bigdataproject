Public - CERI - University of Memphis    










 
 
 
     



 
    
    
    Public - 
      	CERI
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Center for Earthquake Research and Information
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Study  
                          Research  
                          People  
                          Public  
                          News  
                          Donate  
                      
                      
                         
                            CERI Public   
                            
                               
                                  
                                   History of the New Madrid Seismic Zone  
                                   Recent Earthquake Maps  
                                   Memphis Earthquake Hazard Maps  
                                   New Madrid Compendium  
                                   Earthquake Survival  
                                   Earthquake Resources  
                                   Information Sheets  
                                         CERI Overview  
                                         Program  
                                         Seismic Network  
                                         New Madrid Compendium  
                                         Survey Mt St. Helens  
                                         Aftershock Survey: Gujurat India  
                                         Student Poster  
                                         Faculty Poster  
                                         ANSS-Mid America Urban Strong Motion Stations  
                                     
                                  
                                   Request a Speaker  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	CERI
                              	  
                         
                           	CERI Public
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Education   Outreach 
                      
                     
                      So many areas of the globe are prone to earthquakes. As citizens of the world, we
                        worry about the devastating effects they can have on society. CERI plays a huge role
                        in providing both scientific and practical information about earthquakes and earthquake
                        preparedness to people in the central and eastern U.S. who could someday be affected
                        by a significant earthquake.
                      
                     
                      During the Iben Browning episode of 1990, CERI was the main regional source of earthquake
                        information. Filling the information vacuum during the period of intense public concern
                        had a permanent impact on public awareness and understanding of regional earthquake
                        hazards.
                      
                     
                      Town Hall meetings are conducted by staff and faculty members on an on-going basis
                        to address specific concerns of citizens related to earthquake hazards.
                      
                     
                      Request a speaker for a school group, schedule a tour, or request a teacher's workshop
                         HERE .
                      
                     
                      Check our Calendar for upcoming events  HERE.  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         CERI Public 
                         
                            
                               
                                History of the New Madrid Seismic Zone  
                                Recent Earthquake Maps  
                                Memphis Earthquake Hazard Maps  
                                New Madrid Compendium  
                                Earthquake Survival  
                                Earthquake Resources  
                                Information Sheets  
                                      CERI Overview  
                                      Program  
                                      Seismic Network  
                                      New Madrid Compendium  
                                      Survey Mt St. Helens  
                                      Aftershock Survey: Gujurat India  
                                      Student Poster  
                                      Faculty Poster  
                                      ANSS-Mid America Urban Strong Motion Stations  
                                  
                               
                                Request a Speaker  
                            
                         
                      
                      
                      
                         
                            
                                Graduate Study at CERI  
                               Want to perform cutting edge research in a collegial and supportive environment? 
                            
                            
                                Seismic information  
                               View a list of Stations, Earthquake Catalogs, Monitoring Partners, and more... 
                            
                            
                                Earthquake Resources  
                               Recent Earthquakes, Education   Outreach, Preparedness, Data Products, Media Products,
                                 and more...
                               
                            
                            
                                Contact Us  
                               For questions or more information, please contact the main office of CERI 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


