Admissions Dashboard - Office of the President - University of Memphis    










 
 
 
     



 
    
    
    Admissions Dashboard - 
      	Office of the President
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Office of the President
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Mission and Plan  
                          Communications  
                          Leadership  
                          Contact  
                      
                      
                         
                            Mission and Plan   
                            
                               
                                  
                                   Mission and Vision  
                                   Goals  
                                   Dashboard  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Office of the President
                              	  
                          
                              	Mission and Plan
                              	  
                         Admissions Dashboard 
                      
                   
                   
                       
                      
                     
                     		
                     
                      
                     
                      Admissions Dashboard 
                     
                      Admissions - Fall 2017 
                     
                        
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  Memphis: FTF Applications 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  5,682 
                                 
                               
                              
                               
                                 
                                  18,781 
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                               
                                 
                                  18,781 
                                 
                               
                              
                            
                           
                            
                              
                               Latest data year - 2017-18 
                              
                            
                           
                         
                        
                      
                     
                      
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  Memphis: FTF Admitted 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  4,116 
                                 
                               
                              
                               
                                 
                                  11,417 
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                               
                                 
                                  11,417 
                                 
                               
                              
                            
                           
                            
                              
                               Latest data year - 2017-18 
                              
                            
                           
                         
                        
                      
                     
                      
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  Memphis: FTF Acceptance Rate 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  41.9% 
                                 
                               
                              
                               
                                 
                                  60.8% 
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                               
                                 
                                  74.0% 
                                 
                               
                              
                            
                           
                            
                              
                               Latest data year - 2017-18 
                              
                            
                           
                         
                        
                      
                     
                      
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  Lambuth: FTF Applications 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  175 
                                 
                               
                              
                               
                                 
                                  340 
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                               
                                 
                                  593 
                                 
                               
                              
                            
                           
                            
                              
                               Latest data year - 2017-18 
                              
                            
                           
                         
                        
                      
                     
                      
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  Lambuth: FTF Admitted 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  122 
                                 
                               
                              
                               
                                 
                                  122 
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                               
                                 
                                  222 
                                 
                               
                              
                            
                           
                            
                              
                               Latest data year - 2017-18 
                              
                            
                           
                         
                        
                      
                     
                      
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  Lambuth: FTF Acceptance Rate 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  27.8% 
                                 
                               
                              
                               
                                 
                                  35.9% 
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                               
                                 
                                  74.3% 
                                 
                               
                              
                            
                           
                            
                              
                               Latest data year - 2017-18 
                              
                            
                           
                         
                        
                      
                     
                      
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  UG Freshman Acceptance rate (All) 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  41.4% 
                                 
                               
                              
                               
                                 
                                  59.0% 
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                               
                                 
                                  74.0% 
                                 
                               
                              
                            
                           
                            
                              
                               Latest data year - 2017-18 
                              
                            
                           
                         
                        
                      
                     
                      
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  GR acceptance rate 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  40.3% 
                                 
                               
                              
                               
                                 
                                  42.0% 
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                               
                                 
                                  47.7% 
                                 
                               
                              
                            
                           
                            
                              
                               Latest data year - 2017-18 
                              
                            
                           
                         
                        
                      
                     
                      
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  % students o/s of Memphis MSA 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  17.0% 
                                 
                               
                              
                               
                                 
                                  22.0% 
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                               
                                 
                                  22.0% 
                                 
                               
                              
                            
                           
                            
                              
                               Latest data year - 2017-18 
                              
                            
                           
                         
                        
                      
                     
                      
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  Accepted Freshman Average ACT                    (composite) 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  21.7 
                                 
                               
                              
                               
                                 
                                  22.2 
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                               
                                 
                                  22.7 
                                 
                               
                              
                            
                           
                            
                              
                               Latest data year - 2017-18 
                              
                            
                           
                         
                        
                      
                     
                      
                     
                      Admissions ALL FTF - Fall 2016 
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  Applications - First Time Freshman 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  6,037 
                                 
                               
                              
                               
                                 
                                  19,547 
                                 
                               
                              
                                 
                              
                            
                           
                            
                              
                               
                                 
                                  21,397 
                                 
                               
                              
                            
                           
                            
                              
                               Latest data year - 2017-18 
                              
                            
                           
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Mission and Plan 
                         
                            
                               
                                Mission and Vision  
                                Goals  
                                Dashboard  
                            
                         
                      
                      
                      
                         
                            
                                Dashboard  
                               UofM steps towards success 
                            
                            
                                FOCUS Act  
                               Independent Governing Board Updates 
                            
                            
                                President's Blog  
                               News, stories, videos and more 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


