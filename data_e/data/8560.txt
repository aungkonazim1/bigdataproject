Adding ePortfolio reflections from Content | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Adding ePortfolio reflections from Content | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    ePortfolio    »    Integrating ePortfolio with Content   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Adding ePortfolio reflections from Content                       
        
        
       
          
     
           Printer-friendly version       
	You can add an ePortfolio reflection within Learning Environment by clicking  Reflect in ePortfolio  on any page in Content. The  Reflect in ePortfolio  button is enabled by default.
 

 
	 Note  When you add a reflection from Content, the reflection is automatically tagged with three tags that specify the reflection's origin: the course ID, the module name, and the topic name.
 

 
	Enable Reflect in ePortfolio button
 

  
		Navigate to the course topic you wish to allow reflections from.
	 
	 
		Select  Reflecting in ePortfolio is disabled  from the Activity Details area.
	 
	 
		Select  Allow Reflecting in ePortfolio .
	 
	 
		Click  Update .
	 
  
	Disable Reflect in ePortfolio button
 

  
		Navigate to the course topic wish to disable reflections from.
	 
	 
		Select  Reflecting in ePortfolio is enabled  from the Activity Details area.
	 
	 
		Clear  Allow reflecting in ePortfolio .
	 
	 
		Click  Update .
	 
      Audience:    Instructor      

    
           

                   ‹ Administering learning objectives for ePortfolio 
        
                   up 
        
                   Integrating form templates in Content › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        ePortfolio  
  
      ePortfolio basics    Using artifacts    Creating and editing presentations    Creating collections    Managing comments and assessments in ePortfolio    Understanding the basic concepts in sharing    Importing and exporting items    Sharing items within courses    Using form templates    Integrating ePortfolio with Content     Administering learning objectives for ePortfolio    Adding ePortfolio reflections from Content    Integrating form templates in Content      Assessing ePortfolio content    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
