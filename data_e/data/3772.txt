Universal User ID (UUID) - ITS - University of Memphis    










 
 
 
     



 
    
    
    Universal User ID (UUID) - 
      	ITS
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Information Technology Services
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          umTech  
                          Security  
                          Governance  
                          Resources  
                      
                      
                         
                            Accounts   
                            
                               
                                  
                                   Accounts  
                                   Guest Account  
                                   Intensive English for International (IEI) Students Account  
                                   Sponsored Account  
                                   UUID  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	ITS
                              	  
                          
                              	Accounts
                              	  
                         Universal User ID (UUID) 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Universal User ID (UUID) 
                     
                      The Universal User ID ( UUID ) is the username shared among various university services. Your UUID forms your e-mail
                        address as follows: UUID@memphis.edu.
                      
                     
                      Services that require your UUID: 
                     
                      
                        
                          umMail  - University E-mail
                         
                        
                          TigerLAN  lab computers
                         
                        
                          eCourseware  
                        
                          Library Electronic Databases  
                        
                          myMemphis portal  
                        
                          University software - Software Solutions Page  
                        
                          U of M White Pages  - wp.memphis.edu
                         
                        
                          iPrint  
                        
                          umDrive  
                        
                          umWiki  
                        
                      
                     
                      Who can have a UUID? How do I get my UUID? 
                     
                      All currently enrolled students and currently employed staff and faculty automatically
                        have a Universal User ID or UUID created. Anyone who needs assistance should contact
                        the Service Desk at 901-678-8888 or e-mail  umtech@memphis.edu .
                      
                     
                      Students 
                     
                      A student's UUID is created as soon as the student is admitted to the university.
                        Information outlining the steps to initialize the UUID is mailed along with the acceptance
                        letter. This letter contains a twenty-character account activation code used with
                        the university  Identity Management system :  https://iam.memphis.edu .
                      
                     
                      Faculty and returning adjunct faculty 
                     
                      UUIDs are created for every tenure-track faculty member and many adjunct faculty members
                        when their contracts are approved. Faculty members should receive information about
                        initializing their accounts in the mail along their contracts.
                      
                     
                      Staff and new adjunct faculty 
                     
                      New staff and new adjunct faculty should contact the ITS Service Desk to check the
                        status of their UUIDs if they have not received one from their supervisor or LSP.
                        While staff and adjunct faculty have UUIDs created automatically when their records
                        are created in the HR system, we strongly recommend contacting the Service Desk to
                        check its status. New staff should check with the Service Desk on their first day
                        at work. New Adjunct faculty should check after receiving a copy of their signed contract.
                        The Service Desk can be contacted at 901-678-8888 or by e-mailing  umtech@memphis.edu .
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Accounts 
                         
                            
                               
                                Accounts  
                                Guest Account  
                                Intensive English for International (IEI) Students Account  
                                Sponsored Account  
                                UUID  
                            
                         
                      
                      
                      
                         
                            
                                Security Information  
                               Review security policies and guidelines 
                            
                            
                                Need Help with Technology?  
                               Request technical support via UMhelpdesk 
                            
                            
                                Service Catalog   
                               Review the various solutions provided by ITS 
                            
                            
                                Contact Us  
                               For technical assistance, contact the umTech Service Desk: 100 Administration Bldg.,
                                 901.678.8888. To discuss other topics, contact the Office of the CIO: 901.678.8324
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


