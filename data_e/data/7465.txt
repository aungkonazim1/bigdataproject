Inserting links to presentation pages in content areas | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Inserting links to presentation pages in content areas | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    ePortfolio    »    Creating presentations   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Inserting links to presentation pages in content areas                       
        
        
       
        
     
              
	Users can view the various pages of your presentation by clicking the page names in the page navigation panel. You can also link the user to another page within the presentation by adding links to content areas. This provides greater flexibility in customization and organization of your presentation.
 

 
	For example, a student may have three separate pages on their eportfolio: an About Me page, a personal philosophy page, and her work experience. In the middle of her About Me page, she may reference her skills and insert a custom quicklink to direct readers to her resume for further information.
 

 
	Link to presentation pages in content areas
 

  
		Click the  Content/Layout  tab on the Edit Presentation page.
	 
	 
		Click  Page Properties  from the context menu of the page you want as a link.
	 
	 
		Copy the Page Link URL.
	 
	 
		Click the page where you want the link to appear.
	 
	 
		Click    Edit  from the context menu of the component you want to add your link to. Add a component to the page if none currently exists.
	 
	 
		Insert the link using the URL you copied in step 3 through one of the ways below:
		  
				Insert a quicklink using the HTML Editor
			 
			 
				Insert a link using Insert Stuff
			 
		  
  
	 Notes 

	  
			See  Inserting quicklinks in the HTML Editor  and  Inserting media files in the HTML Editor  for additional information on how to add your link.
		 
		 
			Once you export your presentation, or submit the presentation to a dropbox, all content on quicklinked pages become static and no further changes made to the pages are reflected.
		 
	  

 
	See also
 

  
		 Inserting quicklinks in the HTML Editor 
	 
	 
		 Inserting media files in the HTML Editor 
	 
      Audience:     Learner       

    
           

                   ‹ Setting banner text 
        
                   up 
        
                   Creating collections › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        ePortfolio  
  
      Understanding the main pages of ePortfolio    Adding artifacts    Using reflections    Creating presentations    Adding a new presentation    Adding items to a presentation    Copying presentations    Editing display options for items    Modifying the presentation layout    Changing the presentation theme    Setting banner text    Inserting links to presentation pages in content areas      Creating collections    Understanding assessment types in ePortfolio    Understanding the basic concepts in sharing    Importing and exporting items    Sharing items within courses    Using form templates    Integrating ePortfolio with Content     Assessing ePortfolio content    
                  
           
         

       
     

    
    
    
   
 
   
 
