Schedule of Events - MLK50 - University of Memphis    










 
 
 
     



 
    
    
    Schedule of Events - 
      	MLK50
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			MLK50
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Events  
                          News  
                          Contact Us  
                      
                      
                         
                            Law Symposium   
                            
                               
                                  
                                   Schedule of Events  
                                   Panelists and Bios  
                                   Sponsors  
                                   Register Online  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	MLK50
                              	  
                          
                              	Law Symposium
                              	  
                         Schedule of Events 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Symposium Schedule 
                     
                      Day One - April 2, 2018 
                     
                       *All panels take place in the Wade Auditorium at the UofM Law School. The keynote
                           presentation and luncheon will be held at the Peabody Hotel.  
                     
                       9:00 am - Welcome Remarks from the Dean of the UofM Law School  
                     
                       9:15-10:30 am - Panel 1: Criminal Justice  
                     
                       10:30-10:45 am - Refreshment Break  
                     
                       10:45 am-Noon - Panel 2: Voting Rights  
                     
                       12:15-1:45 pm - Keynote Speaker, The Honorable Eric Holder, and Luncheon  
                     
                       2:00-3:15 pm - Panel 3: Confronting Persistent Poverty  
                     
                       3:15-3:30 pm - Refreshment Break  
                     
                       3:30-4:45 pm - Panel 4: 21st Century Activism  
                     
                       4:45-5:00 pm - Concluding Remarks  
                     
                        
                     
                       Panel Discussion Overview  
                     
                      The first day of the symposium will consist of four moderated panels, each with four
                        panelists drawn from academia, government, and the legal profession.  Each panel will
                        examine a specific aspect of Dr. King's legal legacy and each panelist will prepare
                        an essay for publication. The essays presented will examine the progress over the
                        last 50 years and propose actions and solutions for the future. The Honorable Eric
                        Holder, our nation's first African-American Attorney General, will address participants
                        as our Keynote speaker at a luncheon at The Peabody Hotel. The UofM Law School will
                        publish the essays from the symposium in a separate volume intended to become a conceptual
                        plan for the next 50 years. The volume will be distributed nationally.
                      
                     
                       Panel 1  Confirmed Panelists: Mark Osler (U. of St. Thomas Law School), Toussaint Losier (Univ.
                        of Mass.), Roy Austin (Domestic Policy Advisor to Pres. Obama), Tracey Maclin (Boston
                        Univ.); Moderator: Demetria Frank
                      
                     
                      Panel 1, on criminal justice, will examine the current state of the justice system
                        and whether the aspirations of the civil rights movement have been fulfilled over
                        the last 50 years.  Panelists will specifically address policing in communities of
                        color and contemporary penal policy, while grappling with the complex question of
                        what policing and punishment should look like going forward.
                      
                     
                       Panel 2  Confirmed Panelists: Debo Adegbile (Wilmer Hale), Rick Hasen (UC Irvine), Pamela Karlan
                        (Stanford Univ.); Sherrilyn Ifill (Pres., NAACP LDF); Moderator: Steve Mulroy    
                     
                      Panel 2, on voting rights, will examine current voting rights challenges and strategies
                        for reform, whether through litigation, legislation, or mobilization.   Topics to
                        be examined may include voter ID laws, felon disenfranchisement, and other alleged
                        "voter suppression" strategies; racial and political gerrymandering; methods of election;
                        and minority electoral participation.
                      
                     
                       Panel 3  Confirmed Panelists: Dorothy Brown (Emory), Tomiko Brown-Nagin (Harvard), Dorothy
                        Roberts (U. Penn), Dayna Matthew (UVA); Moderator: Amy Campbell    
                     
                      Panel 3, focusing on persistent poverty, will examine some of the contemporary areas
                        where poverty impacts life chances for individuals.  Topics to be examined may include
                        adequate and equitable housing, education, and health opportunities and outcomes.
                      
                     
                       Panel 4  Confirmed Panelists: Claude Steele (UC Berkeley), Beverly Tatum (Spellman), Cornell
                        Brooks (NAACP), Charles McKinney (Rhodes); Moderator: Daniel Kiel    
                     
                      Finally, Panel 4, on 21st Century Activism, panel will examine the ways in which shifts
                        in the legal landscape, modern technology, and the understanding of more subtle manifestations
                        of discrimination impact, advocacy and activism.  Topics to be examined may include
                        psychological causes or impacts of contemporary discrimination and structural barriers
                        to addressing modern disparities, including potentially effective modes of activism.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Law Symposium 
                         
                            
                               
                                Schedule of Events  
                                Panelists and Bios  
                                Sponsors  
                                Register Online  
                            
                         
                      
                      
                      
                         
                            
                                Law Symposium  
                               Learn more and register 
                            
                            
                                MLK50 Events  
                               Complete list of 50th Anniversary Events 
                            
                            
                                National Civil Rights Museum  
                               MLK 50 Yearlong Commemoration 
                            
                            
                                Contact Us  
                               Questions? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/1/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


