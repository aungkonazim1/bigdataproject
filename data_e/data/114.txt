TranscriptsPlus FAQ - Registrar - University of Memphis    










 
 
 
     



 
    
    
    TranscriptsPlus FAQ - 
      	Registrar
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Registrar
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Register  
                          Students  
                          Veterans  
                          Faculty   Staff  
                          Forms  
                      
                      
                         
                            Students   
                            
                               
                                  
                                   Students  
                                   Enrollment Verification  
                                   Grades  
                                         Overview  
                                         GPA and Grading Scale  
                                         Grade Change  
                                         FAQ  
                                     
                                  
                                   Transcripts  
                                         Overview  
                                         Official  
                                         Unofficial  
                                         Lambuth University  
                                         FAQ  
                                     
                                  
                                   FERPA and Student Educational Records  
                                         Annual FERPA Notification  
                                         Release of Student Education Record  
                                     
                                  
                                   Changing Personal Data  
                                   Withdrawing from UofM  
                                   Self Service Guides  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Registrar
                              	  
                          
                              	Students
                              	  
                         TranscriptsPlus FAQ 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      TranscriptsPlus Information 
                     
                      TranscriptsPlus is our electronic transcript online ordering service that allows you
                        to request official University of Memphis (UofM) transcripts online and gives you
                        the option of having them delivered electronically, sent via USPS, FedEx'ed with a
                        valid FedEx account number. (For a list of general transcript FAQs not specific to
                        TranscriptsPlus, refer to   Transcripts and Grades FAQ  .)
                      
                     
                      General Information/Features 
                     
                      
                        
                         UofM has partnered with CredentialsSolutions to provide a safe, secure, and efficient
                           manner of ordering and delivering electronic transcripts.   
                        
                         Any current or former student can request their UofM transcript online.   
                        
                         A certified pdf transcript may be sent to any recipient you choose. However, you must
                           confirm that the recipient has an email address and will consider an electronic transcript
                           to be official.   
                        
                         Your electronic transcript is delivered in an encrypted .pdf format to a secure website
                           for the recipient to access. The transcript ordering process requires that you provide
                           an email address for the intended recipient who will receive an email notification
                           that your transcript is ready for pick up at the specific URL named in the email notification.   
                        
                         TranscriptsPlus has been designed to work with all major browsers, but it is recommended
                           that you use the current browser release.
                         
                        
                      
                     
                       Why am I being asked to complete a FERPA release form? (Former Student)  
                     
                      Since you no longer have, or have never had, an active myMemphis portal account, your
                        identity cannot be validated through the portal. FERPA regulations prevent UofM from
                        releasing your educational records to any third party without your written consent.
                        Thus, you must complete and submit the release form. But completing the form is a
                        one-time requirement; you will not have to complete it for subsequent requests.
                      
                     
                      Costs/Payment 
                     
                      
                        
                         A $2.85 non-refundable service fee is charged for each transcript requested. If you
                           request that a hard copy of your transcript be delivered by FedEx, additional charges
                           apply.   
                        
                         The service fee for TranscriptsPlus is charged when transcripts are ordered. No refunds
                           will be issued.   
                        
                         TranscriptsPlus accepts credit or debit payments through all major credit cards--Visa,
                           MasterCard, American Express, and Discover.
                         
                        
                      
                     
                      Notifications/Delivery 
                     
                      
                        
                         Upon submitting an order through TranscriptsPlus, emails will be sent to you confirming
                           that: (1) your order has been submitted, (2) that your transcript has been sent, and
                           (3) that your transcript has been received by the recipient.   
                        
                         You may check on the status of your transcript order by returning to the TranscriptsPlus
                           portlet and clicking on the product logo or, if you are a former student, by revisiting
                           the TranscriptsPlus website.   
                        
                         Once your order is submitted, it must be reviewed and processed by the Registrar's
                           Office. Processing times may vary based on the number of orders received daily, but
                           normal processing time is 1–3 business days. If there are no problems with the request,
                           the electronically delivered transcript will be delivered minutes after it is processed.   
                        
                         If you requested that your transcript be sent electronically, contact the receiver.   
                        
                         If you requested that your transcript be sent by USPS First Class mail, please allow
                           sufficient time for delivery.   
                        
                         If you requested that your transcript be sent by an express delivery service, use
                           your tracking number to determine the delivery status and direct inquiries/complaints
                           to the express delivery service.   
                        
                         Your UofM ID number is required to order transcripts but your Social Security Number
                           may be used as an alternative. If you are an international student without a social
                           security number and do not know your UofM ID number, contact the Registrar’s Office
                           at 901-678-2810 for guidance.   
                        
                         Check the status of your request. If the UofM Registrar has not processed your request
                           yet, then contact the Registrar's Office with regard to changing the information.
                           Be prepared to supply information from your TranscriptsPlus receipt. If the order
                           has been processed, then you will have to make another request; you will be charged
                           a new processing fee.   
                        
                         You may upload as many as 5 attachments when making your transcript request. You must
                           upload the files during the same session in which you make your request. The attachments
                           will be included with the transcript when delivered electronically or by mail.   
                        
                         You have the option to order online but pick up in person.   
                        
                         Standard processing time is 1-3 business days but electronic transcripts can be delivered
                           immediately.   
                        
                         TranscriptsPlus provides the option of delivery by FedEx, so if you have a valid FedEx
                           account, you may select the FedEx delivery option. If you use some other courier service,
                           you will need to contact the Registrar at 901.678.3927 to arrange for special delivery
                           options. FedEx is the only courier delivery option available  within  TranscriptsPlus.
                         
                        
                      
                     
                        
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Students 
                         
                            
                               
                                Students  
                                Enrollment Verification  
                                Grades  
                                      Overview  
                                      GPA and Grading Scale  
                                      Grade Change  
                                      FAQ  
                                  
                               
                                Transcripts  
                                      Overview  
                                      Official  
                                      Unofficial  
                                      Lambuth University  
                                      FAQ  
                                  
                               
                                FERPA and Student Educational Records  
                                      Annual FERPA Notification  
                                      Release of Student Education Record  
                                  
                               
                                Changing Personal Data  
                                Withdrawing from UofM  
                                Self Service Guides  
                            
                         
                      
                      
                      
                         
                            
                                Calendars  
                               View Academic Year calendars; semester dates/deadlines; final exam schedules. 
                            
                            
                                Transcripts  
                               Request copies of your official transcript. 
                            
                            
                                Notice!  
                                Check for alerts and reminders. 
                            
                            
                                Contact Us   
                               Need additional assistance or information? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


