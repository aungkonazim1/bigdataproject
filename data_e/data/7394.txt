Screen reader tips | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Screen reader tips | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Accessibility    »    Using assistive technology in Desire2Learn   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Screen reader tips                       
        
        
       
        
     
              
	Learning Environment is a learning management system that enables you to access course material online. There are a number of learning tools within the system that course content is contained in. This topic discusses how the system is laid out, and how different functionality, settings, and preferences benefit individuals that navigate the system using a screen reader (or other assistive technologies that interpret the contents of a page).
 

 
	This topic does not document how to use specific assistive technologies. Please refer to help material for the software or device you are using if you want to learn about its specific functionality, shortcuts, and commands.
 

 
	Table of contents
 

  
		 Logging in 
	 
	 
		 Logging out 
	 
	 
		 Site organization 
	 
	 
		 General page layout 
	 
	 
		 Personal settings 
	 
	 
		 Calendar page layout 
	 
  
	  Logging in
 

 
	The Login page for Learning Environment has three form elements:  Username ,  Password , and  Login . The  Username  field has focus when you enter the page. The  Password  field and  Login  button are the next tab options.
 

 
	There are two additional options displayed as links:
 

  
		The  Forgot Password?  link opens a new window where you can request a password reset link be sent to the email address associated with your username.
	 
	 
		If enabled for your organization, the  System Check  link opens a new page that lets you know if your computer meets the minimum criteria for using Learning Environment. If you do not meet the criteria, a message displays explaining what components need updating.
	 
  
	  Logging out
 

 
	The  Logout  link is available in your personal menu on the minibar at the top of all pages. The personal menu opens when you click on your name on the minibar. You can access your personal menu using an assistive technology's links list, or by tabbing through the minibar links at the top of the page.
 

 
	System time-outs
 

 
	The system may log you out if you are inactive for an extended period of time. The length of time is determined by your institution. A JavaScript warning will provide an option to remain logged in before your time expires.
 

 
	  Site organization
 

 
	Learning Environment is typically organized into two levels of information: organization related information and course related information. Organization related information typically appears on My Home, while course related information typically appears on Course Home and different course tools.
 

 
	My Home
 

 
	My Home is generally the first page you access when you log in to Learning Environment. It is a central area for checking institution-wide news and events, and accessing organization-level tools.
 

 
	Like all pages in Learning Environment, My Home has a navigation area across the top of the page. This navigation area includes the minibar and navbar. The minibar is a navigation area that contains links and menus specific to you, such as links to your courses and alerts about events related to you. The navbar typically contains links to different tools. Since My Home is an organization-level page, the links on the navbar for My Home usually go to organization-level or course-independent tools, such as your personal Locker, Email, and ePortfolio. The navbar has a Heading 2 called "Navigation". It is organized using unordered lists.
 

 
	Other My Home content is organized in widgets. Typical widgets include:  News ,  My Courses ,  Tasks  and  Calendar . You can navigate and search for widgets by Heading 2s, and navigate sections within a widget by Heading 3s.
 

 
	One of the main purposes of My Home is to provide access to organization-level tools and information. We recommend you use your screen reader to view a link list and heading list for your My Home to get a feel for what options are available to you.
 

 
	To access a Course Home, you must select it from the course selector on the minibar or the  My Courses  widget. If you are using the  My Courses  widget and have more than one role in Learning Environment (for example, you are both a Graduate Student and a Teaching Assistant) you need to select the tab for the role you want to view courses for. Depending on your screen reader, widget tabs may be read as tabs or links. They should always be the first content after the widget's heading.
 

 
	Course Home
 

 
	Course Home is the first page you visit when you enter a course. It is a central area for accessing course specific information. Like My Home, Course Home pages have a navigation area across the top of the page and a number of widgets that contain information and links. Information and links on Course Home are specific to that course (unless they are for course-independent tools). For example, the navigation area for a Course Home might contain links to  Grades ,  Discussions  and  Quizzes  for that specific course. Use Heading 2s to navigate to the main Navigation area and the different course widgets.
 

 
	  General page layout
 

 
	Navigation area and Skip to main content links
 

 
	At the top of each page is a navigation area that includes the minibar and the navbar. You can skip the navigation area on any page by selecting the  Skip to main content  link.
 

 
	The minibar appears at the top of every page. It contains links to: My Home; a course selector that enables you to switch between courses; alerts about events and updates specific to you and your courses; and a personal menu for setting your preferences and logging out.
 

 
	The navbar contains an unordered list with the main tool links for the course you are viewing (or for My Home or a department if you are not in a course). You can jump to the navbar by selecting the heading "Navigation".
 

 
	Headings and ARIA landmarks
 

 
	Headings and ARIA landmarks are used throughout the system to help you navigate pages. Heading 1s and Main landmarks are used for page titles. Heading 2s are used for widgets and major page sections. Heading 3s are used to organize information within widgets and major sections. Navigation, Search and Complementary ARIA landmarks are used to provide context.
 

 
	 Note   If you do not set the accessibility preference "Show secondary windows as pop-ups," your screen reader’s heading and landmark lists will read two heading 1s and two Main ARIA landmarks when you open dialog boxes.
 

 
	Tool navigation and action buttons
 

 
	Tool navigation and action buttons are used to navigate areas and perform tasks within a specific tool. Typically, the tool navigation links are used for complicated tools that need to be divided into different types of tasks. For example, in Grades the tool navigation divides the  Enter Grades  and  Manage Grades  areas. You can find the tool navigation links under the hidden heading 2 called Tool Navigation. Action buttons encompass page specific tasks. For example, the Manage Grades page include a  New  action button to add grade items or categories.
 

 
	Context menus
 

 
	While tool navigation and action buttons are used to navigate between tool areas and perform tasks, context menus are used to select an action for a specific item on a list page. Every context menu has unique link text that references the item it applies to. For example, a file in your Locker will have a context menu beside it named " Actions for [file name] " which opens a menu with item specific actions.
 

 
	Tables
 

 
	Tables (grids) are often used to organize content within a tool.
 

 
	All tables use proper table summaries and headings. However, the easiest way to navigate items in a table is by a check box, since most table items have a  Select [item]  check box before their name.
 

 
	Most items also have a context menu after their name. These menus contain item specific actions. Each context menu has unique link text so you can use a links list to locate the context menu for the item you want to perform actions on.
 

 
	You can also select multiple items from a table using the  Select [item]  check boxes and then selecting an action that can apply to multiple items. Actions that can apply to multiple items appear at the top and bottom of a table and use the alt text format  [action] Selected Items . For example, you could use a links list to "Select Topic 1" and "Select Topic 2" and then "Edit Selected Items".
 

 
	Complex tables often have drop-down lists for filtering information in the table. For example, some tables have a  View  drop-down list that allows you to change how information is sorted. Most drop-downs have an accompanying  Apply  or  Go  button that initiates the action.
 

 
	A few drop-downs, such as  per page  drop-downs, update on selection so you must use  Alt  +  Down Arrow  (Windows/Linux) or  Option  +  Down Arrow  (Mac) to open the drop-down and then use the  Up Arrow  or  Down Arrow  and  Enter  key to select an option.
 

 
	If there are more items in a table than will display on a single page, use the  Page  drop-down list,    Next Page  icon, or    Previous Page  icon to navigate to other pages.
 

 
	Forms
 

 
	Pages that use form fields for entering data or changing settings have a logical tab order. If a page is divided into multiple tabs, your screen reader may read the tabs as tab stops or as links. You normally cannot open a secondary tab until you have filled in all the mandatory fields on the first tab. Mandatory fields are indicated with an asterisk (*). The last options on a form are usually  Cancel  and  Save . Sometimes the  Save  button is called something else related to completing the action, such as  Select  or  Upload .
 

 
	Many forms allow you to create content using our HTML Editor (WYSIWYG). The HTML Editor is fully accessible by keyboard, but lacks non-visual feedback when options or formatting are selected in the editor view. You can make changes in the source view so you can read your changes in the code as you work, or turn off the HTML Editor in your Account Settings. If you turn off the HTML Editor it is replaced by text fields that accept HTML.
 

 
	Some form pages contain links to additional actions, which may not be recognized as form elements by your screen reader. For example, there is a link to create a  New Folder  on the Add Contact form in Email. Always check for links when filling out a form in Learning Environment and other Desire2Learn products.
 

 
	Most form pages provide either a confirmation or error message when you submit the form using an ARIA alert. The message appears at the top of the page and should take focus. If there were errors in your submission the message explains each error and provides links to the appropriate fields so you can resolve the issues.
 

 
	Reading equations outside of forms mode
 

 
	If you complete a quiz, survey, or self-assessment and answer choices contain equations made from an equation editor, you can gain the best experience by reading the choices while out of forms mode. Out of forms mode, MathPlayer is able to render the equations more comprehensibly. If you read equations within forms mode, your screen reader will read out MathML code instead of the equation.
 

 
	Treeviews and frames
 

 
	We have tried to keep page layouts as simple as possible. However, some tools, such as Manage Files and Email, use treeviews and frames to layout complicated lists of items and options. Most of these pages have options to  Hide Tree  (Manage Files) or  Show (or hide) the folder list pane  (Email). Check the tool's settings and tool bars for ways to simplify the page layout.
 

 
	Help text
 

 
	Many forms contain inline help;    Help  icons and links to additional help material appear either immediately after the page heading or section heading, or after individual fields. It is a good idea to read the entire contents of a form before filling it out, and to look for help text or a help link immediately after a field if you have difficulty understanding its purpose.
 

 
	Show/Hide
 

 
	Some pages contain sections that are collapsed by default. Collapsed sections contain advanced or supplemental information that is not required to complete standard tasks. To expand a collapsed section using a keyboard or screen reader, select the appropriate    Expand  or  Show  link.
 

 
	Load More
 

 
	When a list contains more items than the page currently displays, a  Load More  link appears at the bottom of the list as the final list item. Clicking this link appends more items to the list.
 

 
	Pop-ups
 

 
	Some links open secondary pop-up windows for completing page-specific tasks. These links should indicate that they open in a new window through a title attribute. Use the  Down Arrow  and  Tab  keys to read the contents of the pop-up. The last options should be buttons to cancel or complete the task. Occasionally, these buttons are in a separate frame.
 

 
	 Important   Some secondary pages use modal dialogs instead of separate windows to display information. If you primarily navigate the web using a screen reader we recommend that you select  Show secondary windows as pop-ups  in the Account Settings tool.
 

 
	Search
 

 
	Most pages that contain lists of items or users have a  Search For  field near the top of the page. To perform a search, enter a word or partial word in the  Search For  field and select the  Search  button or press the  Enter  key.
 

 
	Use the  Show Search Options  link to select advanced search options.
 

 
	Use the  Clear Search  link to clear the  Search For  field.
 

 
	WAI-ARIA support
 

 
	Desire2Learn uses WAI-ARIA markup in a number or areas to help support navigation by keyboard and assistive technologies. For example, WAI-ARIA markup is used for tabs, context menus, error and confirmation messages, and for page navigation landmarks.
 

 
	  Personal settings
 

 
	The minibar includes a personal menu with links to tools that store your personal information and settings. To open the personal menu, select the link that is your name. The following links are available:
 

  
		 Profile  Edit your shared personal information.
	 
	 
		 Notifications  Set how you receive notifications about activity in your courses.
	 
	 
		 Account Settings  Change display settings for Learning Environment.
	 
  
	Account Settings
 

 
	We recommend that you adjust the settings available in the Account Settings tool to meet your personal needs. Here are some recommendations:
 

 
	Account Settings tab
 

  
		The  Login Destination  option lets you log directly into a course, rather than My Home. This option can simplify navigation if you are only taking one or a few courses.
	 
	 
		Use the  Font Face  and  Font Size  options to adjust the appearance of text in Learning Environment. If you want to adjust the size of icons as well, we recommend using a zooming or magnifying tool such as the zoom feature built into most browsers.
	 
	 
		If you rely on a screen reader to navigate Learning Environment we recommend you select  Show secondary windows as pop-ups ; otherwise, some secondary pages may not be distinguishable from the main page.
	 
	 
		Many screen reader and keyboard-only users select the  Turn off rich text editor and view source  option. This option simplifies form pages, while still allowing users to upload HTML content authored in an external editor.
	 
	 
		Select the option,  Optimize video presentation for programmatically-driven assistive technologies , to enable detection of embedded videos.
	 
  
	Discussions tab
 

  
		Clear the  Always show the Discussions List pane  option to simplify the layout of discussion lists.
	 
	 
		Some users also  Include original post in reply  so they don’t have to move between discussion messages as frequently.
	 
  
	Email tab
 

  
		Clear the  Show the Message Preview pane  and  Show the Folder List pane  options to simplify the layout of your Inbox.
	 
	 
		Some users also  Include original message in email replies  so they don’t have to move between messages as frequently.
	 
  
	Metadata tab
 

 
	Select the  Basic  and  Expand all Categories  options to simplify movement through the main metadata page.
 

 
	  Calendar page layout
 

 
	The Calendar tool can display events from multiple calendars in one view. Read the following sections to help orient yourself to key elements of the Calendar interface.
 

 
	Calendar view modes
 

 
	In addition to standard Day, Week, and Month views, the Calendar tool also displays calendar events in an Agenda and List view. The mode in which you're viewing the calendar appears as part of the heading 1 on the page.
 

 
	The Agenda view groups course events from your active calendars by Date, Course, or Type. Events display in chronological order and all-day events display at the top of each grouped listing.
 

 
	List view displays all events from your active course calendars in chronological order. You can filter this list by event type. Click an event's name to view more details about that event. If the list contains many items, a  Load More  link appears at the bottom of the list as the final list item. Clicking this link appends more items to the list.
 

 
	Default calendar
 

 
	The default calendar is always set as the course from which you accessed the Calendar tool; its name appears as a link on the page. If you have permission to create course events, those events appear on the default calendar.
 

 
	You can change your default calendar by selecting the link labeled with the default calendar's name. This opens the calendar selector where you can select a course to view its calendar. Changing your default calendar also switches your current course in Learning Environment to correspond to the course calendar you select.
 

 
	Calendar selector
 

 
	A bulleted list of calendars available to you displays once you select the link for the current calendar. Each calendar corresponds to an individual course you are enrolled in. If one of your courses is missing from the calendar selector, you can add course calendars to your list by selecting the  Add Calendar  link. Select a course link from the bulleted list to display its calendar events in the main calendar display.
 

 
	 Note   If you have Calendar set to display all calendars, events in the main calendar display do not currently distinguish which courses they are part of when you scan the calendar. To distinguish which course an event is part of, you must select the event name for its details.
 

 
	Mini calendar
 

 
	The mini calendar appears after a heading 2 that matches the current month and year. It's a quick reference for the active calendars in the Calendar tool. It indicates which dates have events and the day, week, or month currently selected in the main calendar display.
 

 
	The mini calendar includes leading and trailing dates. This means that you might see the last days of the previous month and the first days of the next month depending on which day of the week the current month begins.
 

 
	Tasks
 

 
	Tasks are not connected to specific calendars. The task pane enables you to keep a personal list of tasks and set their deadlines to keep track of things to do.
 
     Audience:     Learner       

    
           

                   ‹ Screen reader accessibility features 
        
                   up 
        
                   Keyboard-only navigation accessibility features › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Accessibility  
  
      Using assistive technology in Desire2Learn    Screen reader accessibility features    Screen reader tips    Keyboard-only navigation accessibility features    Keyboard-only navigation tips    Screen magnifiers, zooming, and color contrast accessibility features    Screen magnifiers, zooming, and color contrast tips    Getting additional support      Accessible course design    
                  
           
         

       
     

    
    
    
   
 
   
 
