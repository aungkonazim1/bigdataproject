February | 2016 | University of Memphis Libraries   
 
 
 
 
 February | 2016 | University of Memphis Libraries 
 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 
 




 
 
  
 

			

         



 

 
 
	 Skip to content 
		 
				 
						  University of Memphis Libraries  
			 News and Events 
		 

		 
			 Menu 
			  
  Home    
		  
	  

	 
			 
			 Home   2016   February 		 
		 
		 

		
			 
				 
					Month:  February 2016 				 
							  

						
				
 
	 
		 
					  What s New in 2016?  
								 
								   February 29, 2016  March 1, 2016       mswrngen   
															  Uncategorized  					
												  
					  

				 
			               
  Ongoing Display: Black, White   Read 
  
    On display on the first and fourth floors of McWherter Library through May, the “Black, White   Read: Reporting the 1866 Memphis Massacre” exhibition, as part of Black History Month and the  Memories of a Massacre: Memphis in 1866 Project , commemorates the150th anniversary of this event in African-American history. 
 To learn more about this historical event, visit the  Memphis Massacre of 1866 Research Guide . 
   
   
  March: Women’s History Month  
    Women s History Month at the Libraries will begin with a “Feminist Issues in the News” Panel Discussion, Monday, February 29 at 7:00 p.m. in McWherter Library, room 226. Drs. Amanda Nell Edgar (Communication), Virginia Solomon (Art), Robert Boyd (Journalism), Susan Nordstrom (CEPR), and Ms. Elle Perry will facilitate a discussion about feminist issues (e.g., gender, ability, sexual, racial, economic, environmental, neo-colonialist, body, and age) in the day s  New York Times . 
 This event coincides with an ongoing display,    Feminist Sculpture for Strolling,  a papier-mâché spherical sculpture which is a part of the Art Museum of the University of Memphis   Disassembling Statements =  Assembling Solidarity  project which seeks to disassemble the daily acts of gender, sexual, racial, class, environmental, body, and age inequalities presented in newspapers and reassemble them as a spherical sculpture. Everyone is encouraged to bring newspaper articles concerning any and all feminist issues and drop them off at the display in the center of McWherter Library  rotunda. A representative will paste the articles on the sculpture each Wednesday from noon   1:30 p.m. The finished sculpture will be displayed at the Women’s History Month Closing Ceremony, April 1 at 12:30 p.m.   
    
 Women s History Month at the Libraries will close with two events. The first will be a book reading and signing of  Down Home Blues  by Phyllis Dixon, Tuesday, March 22 at 2:00 p.m. in McWherter Library, room 226. 
     Frances Dancy Hooks (1928   2016)  
 The second event will be  The Life of Frances Hooks,  an honorarium and lecture by Will Love, Library Assistant and staff of the Benjamin L. Hooks Institute for Social Change, Wednesday, March 23 at 3:00 p.m. in McWherter Library, room 226. Born and raised in Memphis, Frances Dancy Hooks was a well-known educator and wife of NAACP executive director Benjamin Hooks. Through her work with the NAACP and other organizations, Mrs. Hooks influenced the trajectory of initiatives involving education, poverty, and women s rights here in Memphis and throughout the nation. This presentation will focus on her life as a teacher, activist, and church member, demonstrating how her work at the intersection of African-American and women s civil rights greatly influenced modern women s history. 
 Visit the  Women s History Month 2016 Calendar of Events  for a full list of events on campus .   #UMemWomen  
   
   
  New Technology Available to Check Out 
  
    The Libraries now offer Leap motion tracking sensors and Eye Tribe eye tracking sensors for check out at the McWherter Library circulation desk. The Leap Motion sensor is analogous to a mouse; it plugs into the USB port and allows you to use your hands and fingers to interact with a computer application as a kind of touch-less mouse. The Eye Tribe sensor allows you to use your eyes to operate a web-browser. To learn more, visit the  Sensors Research Guide . 
   
  Technology Training  
 The Libraries offers 3D printing, web design, and soldering workshops. Visit the  technology tab on the Libraries’ homepage  to see a full schedule of workshops. 
   
					  
		
		 
						  Leave a comment  
			
					  
	  
 
			
			
		
		  
	  

					 
					 		 Recent Posts 		 
					 
				 3D Printing Workshop 
						 
					 
				 NedXStudents 
						 
					 
				 Craft-n-Chat 
						 
					 
				 Bridging East and West: The First Steel Bridge of Memphis 
						 
					 
				 Banned Books Display 
						 
				 
		 		  Archives 		 
			  November 2017  
	  October 2017  
	  September 2017  
	  August 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  April 2016  
	  February 2016  
	  September 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  May 2014  
	  April 2014  
	  March 2014  
	  February 2014  
	  January 2014  
		 
		   Categories 		 
	  Alerts 
 
	  Branch Libraries 
 
	  Events 
 
	  Exhibition 
 
	  Homepage 
 
	  News 
 
	  Research and Instructional Services 
 
	  Special Collections 
 
	  Trial Resources 
 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 		  
	
	  

	 
		 
			 
								 Proudly powered by WordPress 
				  |  
				Theme: Big Brother by  WordPress.com .			  
					  
	  
  


 

        

        			 
				   Subscribe  
				 

					
						 Follow this blog 

						 
							
															 Get every new post delivered right to your inbox. 
							
							 
								 
							 
							
							 
							 
							
							  							   
						 

					
				 
			 
		






		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 