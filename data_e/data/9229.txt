Earthworm Modules: gcf2ew overview   
 
 Earthworm Modules: gcf2ew overview 
 
  Earthworm Modules:   gcf2ew Overview 
 (Last Revised 17 May, 2006)  

 
gcf2ew is a Guralp Digitizer GCF DM data feeding program for Earthworm. The
gcf2ew module provides for three modes of data input:
 
 Lantronix MSS100 serial to IP server
 Direct Serial connection to a DM
 Network connection to  gcfserv 
 
This version uses a Lantronix MSS100 for the reliable transmission of data over a network.
It also provides the capability for direct serial connection to a DM24 digitizer from
a Solaris workstation. The direct serial connection makes full use of the BRP protocol
that DM24's use to correct for serial line errors. In addtion, the gcfserv program can
be used to serve data to multiple receivers using a PULL protocol that the gcf2ew
module is configured for. V1.0.0 and later has SCNL capability for EW v7.0 and greater.
It needs to be compiled with EW v7.0 and later, but is backward compatible to earlier
versions of Earthworm.
 
When running with an MSS100 or gcfserv, the module provides a
reliable reconnection if the IP connection should drop. The network input also
uses the Guralp BRP serial protocol to rewind the DM to request missing packets.
 
 Module Index  |
 gcf2ew Commands 
 

 
 
 
Author:  Instrumental Software Technologies, Inc.  Questions? Issues?  Subscribe to the Earthworm Google Groups List.    
 
 
 
