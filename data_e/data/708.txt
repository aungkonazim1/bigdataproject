Advocacy Program In-school Competitions - School of Law - University of Memphis    










 
 
 
     



 
    
    
    Advocacy Program In-school Competitions  - 
      	School of Law 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Cecil C. Humphreys School of Law
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Admissions  
                          Programs  
                          Current Students  
                          Faculty  
                          Careers  
                          Library  
                      
                      
                         
                            Programs   
                            
                               
                                  
                                   Degree Programs  
                                         JD  
                                         JD/MBA  
                                         JD/MA in Pol. Science  
                                         JD/MPH  
                                         MPH Fast-Track  
                                         Part-time Program  
                                     
                                  
                                   Certificate Programs  
                                         Advocacy  
                                         Business Law  
                                         Health Law  
                                         Tax Law  
                                     
                                  
                                   Experiential Learning  
                                         Experiential Learning Home  
                                         Legal Clinics  
                                         Externship Program  
                                     
                                  
                                   Law Review  
                                         Law Review Home  
                                         Editorial Board  
                                         Editor-in-Chief  
                                         Joining Law Review  
                                         Virtual Tour of Law Review Suite  
                                         Symposium  
                                         Subscriptions  
                                         Archives  
                                         Current Volume  
                                         Connect with Us  
                                     
                                  
                                   Advocacy Program  
                                        
                                         Moot Court Board  
                                        
                                         Joining Moot Court Board  
                                        
                                         In-School Competitions  
                                        
                                         Travel Team Competitions  
                                        
                                         Past Champions  
                                     
                                  
                                   Inst. for Health Law   Policy  
                                         Institute Home   
                                         Advisory Committee  
                                         Symposium  
                                         Pro Bono Opportunities  
                                         Health Law Certificate  
                                         Health Law Society  
                                         MBA Health Law Section  
                                     
                                  
                                   International Law Programs  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	School of Law 
                              	  
                          
                              	Programs
                              	  
                         Advocacy Program In-school Competitions  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                        
                     
                      IN-SCHOOL COMPETITIONS  
                     
                      Advanced Moot Court Competition 
                     
                      The Advanced Moot Court Competition begins shortly after Labor Day. This competition
                        gives second- and third-year students the opportunity to practice and improve skills
                        in appellate advocacy. Teams of two students write an appellate brief and then present
                        oral arguments. Each team argues both the petitioner's and respondent's position.
                        The brief is due in September and the competition begins in October.
                      
                     
                      First-year students may serve as bailiffs during oral arguments, giving them an opportunity
                        to observe appellate advocacy before competing in the Freshman Moot Court Competition
                        in the spring semester. Participation as a bailiff does not count toward Moot Court
                        credit.
                      
                     
                      Scores on the brief and from oral arguments are a factor determining eligibility for
                        membership on the Moot Court Board. Successful completion of the competition goes
                        toward obtaining credit for graduation.
                      
                     
                      The problem for the Advanced Moot Court Competition is prepared under the direction
                        of the Associate Justice for the Advanced Competition and is distributed as a part
                        of the Advanced Appellate Advocacy curriculum.
                      
                     
                      Past winners: 
                     
                      
                        
                         Rebecca Holden and Tyler Tollison (2016) 
                        
                         Dylan Gillespie and McKenzie Reed (2015)
                           
                            
                              
                               Topic: Panhandlers' First and Fourth Amendment Rights 
                              
                            
                           
                         
                        
                      
                     
                      If you have any questions regarding this competition, please contact Whitney Trujillo,
                        the Associate Justice for the Advanced Competition, at  wpsipple@memphis.edu  
                     
                      A student's perspective: 
                     
                      "Working with a partner makes this competition slightly less nerve-wracking than the
                        Freshman. I had a great experience. I believe both my oral argument skills and courtroom
                        demeanor improved during the competition. I received constructive criticism that was
                        helpful." - Madison Patey
                      
                     
                      Mock Trial Competition 
                     
                      The Mock Trial Competition is held at the beginning of each spring semester. It is
                        open to second- and third-year students, offering them an opportunity to practice
                        and improve skills in trial advocacy. Two-person teams submit a a motion and trial
                        notebook and participate in a mock trial. Each team represents both the plaintiff
                        and the defendant.
                      
                     
                      The plaintiff team for each round must provide two witnesses and a bailiff. The defendant
                        also must provide two witnesses. It is each team's duty to ensure that witnesses are
                        briefed and prepared to testify. Participating as a witness or a bailiff provides
                        first-year students with an opportunity to experience trial advocacy. Participation
                        as a bailiff or a witness does not count towards Moot Court credit.
                      
                     
                      Students need not have completed Trial Advocacy or Evidence in order to compete successfully,
                        although it is suggested.
                      
                     
                      Scores on the trial notebook and from performance on oral arguments are factors in
                        determining eligibility for membership on the Moot Court Board. Successful completion
                        of the competition goes towards obtaining credit for graduation.
                      
                     
                      Past winners: 
                     
                      
                        
                         Patrick Hillard and Dominique Winfrey (2017) 
                        
                         Will Hampton and Quynh-Anh Dang (2016) 
                        
                      
                     
                      If you have any questions regarding this competition, please contact Lan Chen, the
                        Associate Justice for Mock Trial at  lchen@memphis.edu.  
                     
                      First Year Moot Court Competition 
                     
                      The First Year Moot Court Competition begins after Spring Break. The First Year Moot
                        Court Competition is open only to first-year students. The competition is held each
                        year during the spring semester, providing first-year students with the opportunity
                        to practice and improve skills in appellate advocacy. Teams of two present oral arguments
                        based on the Legal Methods II appellate brief assignment. Each team member argues
                        both the petitioner's and respondent's position. The scores for the competition are
                        based solely on performance during the oral arguments.
                      
                     
                      The petitioner team must provide the bailiff for the round. The bailiff may be anyone
                        who is not currently a participant in the competition.
                      
                     
                      Scores from performance during oral arguments are a factor in determining eligibility
                        for membership on the Moot Court Board. Successful completion of the competition goes
                        toward obtaining Moot Court credit for graduation.
                      
                     
                      The problem for the First Year Moot Court Competition is written by members of the
                        Moot Court Board under the direction of the Associate Justice for the First Year Competition
                        and is distributed as a part of the Legal Methods II curriculum.
                      
                     
                      Past winners: 
                     
                      
                        
                         Alan Matthews (2017) 
                        
                         Maggie McGowan (2016) 
                        
                         Zach Johnson (2015) 
                        
                         Preston Battle (2014) 
                        
                      
                     
                      If you have any questions regarding this competition, please contact Rebecca Holden,
                        the Associate Justice for the First Year Competition, at  rrholden@memphis.edu .
                      
                     
                      A student's perspective: 
                     
                      "This competition requires one to be flexible, because you never know what questions
                        you will get from a judge. Win or lose, this competition is a great experience. Everyone's
                        in the same boat, which means you have the potential to be the best if you drop your
                        nerves and give it your all!" - Ashley Finch
                      
                     
                       Pointers for Credit-Worthy Oral Arguments  
                     
                      Judges typically look for: 
                     
                      
                        
                         Forensic Ability 
                        
                         Knowledge of the Law 
                        
                         Responsiveness to Questions 
                        
                         Argument Form 
                        
                         Argument Substance 
                        
                         Appropriate Attire 
                        
                         Deference to the Court 
                        
                         Persuasiveness 
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Programs 
                         
                            
                               
                                Degree Programs  
                                      JD  
                                      JD/MBA  
                                      JD/MA in Pol. Science  
                                      JD/MPH  
                                      MPH Fast-Track  
                                      Part-time Program  
                                  
                               
                                Certificate Programs  
                                      Advocacy  
                                      Business Law  
                                      Health Law  
                                      Tax Law  
                                  
                               
                                Experiential Learning  
                                      Experiential Learning Home  
                                      Legal Clinics  
                                      Externship Program  
                                  
                               
                                Law Review  
                                      Law Review Home  
                                      Editorial Board  
                                      Editor-in-Chief  
                                      Joining Law Review  
                                      Virtual Tour of Law Review Suite  
                                      Symposium  
                                      Subscriptions  
                                      Archives  
                                      Current Volume  
                                      Connect with Us  
                                  
                               
                                Advocacy Program  
                                     
                                      Moot Court Board  
                                     
                                      Joining Moot Court Board  
                                     
                                      In-School Competitions  
                                     
                                      Travel Team Competitions  
                                     
                                      Past Champions  
                                  
                               
                                Inst. for Health Law   Policy  
                                      Institute Home   
                                      Advisory Committee  
                                      Symposium  
                                      Pro Bono Opportunities  
                                      Health Law Certificate  
                                      Health Law Society  
                                      MBA Health Law Section  
                                  
                               
                                International Law Programs  
                            
                         
                      
                      
                      
                         
                            
                                Apply to Memphis Law  
                                
                            
                            
                                News   Events  
                                
                            
                            
                                Alumni   Support  
                                
                            
                            
                                ABA Required Disclosures  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


