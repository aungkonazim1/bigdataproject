FAQs - CDMBA - University of Memphis    










 
 
 
     



 
    
    
    FAQs  - 
      	CDMBA
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - Customer-Driven MBA
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Welcome  
                          How to Apply  
                          Pre-Screening  
                          Curriculum  
                          FAQs  
                          FCBE  
                      
                      
                         
                            FAQs   
                            
                               
                                  
                                   Admissions  
                                         How to Apply  
                                         Pre-Screening Form  
                                     
                                  
                                   Curriculum  
                                   Corporate Partners  
                                   Media  
                                   FAQs  
                                   Contact Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	CDMBA
                              	  
                         
                           	FAQs
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Frequently Asked Questions 
                     
                      Is a GMAT required to be admitted? If so, what score should I shoot for? 
                     
                      A GMAT or GRE is necessary for admission into the Customer-Driven MBA.  One exception
                        is for those students who hold a graduate degree already and who have demonstrated
                        academic excellence in their prior graduate studies.  The Customer-Driven MBA is a
                        highly competitive, rigorous program; therefore, accepts students of the highest caliber
                        both academically and professionally. Because this is a non-traditional admissions
                        process, we want to make sure that we include all pertinent information about the
                        applicants to our corporate sponsors who will be interviewing them. We highly recommend
                        candidates have at least a 600 GMAT or 315 GRE or equivalent.
                      
                     
                      Is there an undergraduate GPA requirement? 
                     
                      We highly recommend students applying to the Customer-Driven MBA have an undergraduate
                        of 3.0 on a 4.0 scale.  We do however, encourage students with a lesser GPA to apply
                        as admission to the program is determined on several factors, not just GPA.
                      
                     
                      How many years of professional experience is necessary to be admitted into this program? 
                     
                      Preferably 2 years of professional experience in a marketing related field. 
                     
                      How much does the program cost me? Is there a stipend included? 
                     
                      The Customer-Driven MBA program is sponsored by Corporations in the Memphis community. 
                        Corporations pay full-tuition for students that they are sponsoring in this program. 
                        In addition to a free tuition, students will receive $27,000 over the 17-month duration
                        of the program.
                      
                     
                      Would I be eligible for additional scholarships through the University? 
                     
                      No. You will not be eligible for additional scholarships through the University or
                        Fogelman College. We do encourage you to research private scholarships; however.
                      
                     
                      Where are the Corporate Sponsors located? 
                     
                      Corporate Sponsors are located in the Memphis / West Tennessee Region. 
                     
                      What types of companies are sponsoring the CDMBA? 
                     
                      Currently the Corporate Partner associated with this program is in the marketing and
                        information systems industries.
                      
                     
                      What if I decide that I do not want to work for the Corporate Partner after I graduate
                        from the CDMBA program?
                      
                     
                      Corporate Sponsors are paying your tuition and stipend for you during the 17-month
                        period of the program with an understanding that you work for them a minimum of 2
                        years after you graduate. If you cannot work for the Corporate Sponsor during this
                        time, you will simply need to reimburse the Corporate Sponsor for the funds that it
                        provided to sponsor you through the program.
                      
                     
                      When does the Customer-Driven MBA program begin? Can I join in the spring? 
                     
                      The Customer-Driven MBA begins in the fall semester. It is the objective of the University
                        to establish a collaborative environment among high-caliber students going through
                        the Customer-Driven MBA program. To do this, we feel that it is important to have
                        all new coming students begin together in the fall semesters; hence, students will
                        only be admitted to the program beginning in the fall semesters. The University will
                        not admit students into spring semesters for the Customer-Driven MBA.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         FAQs 
                         
                            
                               
                                Admissions  
                                      How to Apply  
                                      Pre-Screening Form  
                                  
                               
                                Curriculum  
                                Corporate Partners  
                                Media  
                                FAQs  
                                Contact Us  
                            
                         
                      
                      
                      
                         
                            
                                How To Apply  
                               What's your first step? Find out here! 
                            
                            
                                Corporate Sponsors  
                               Looking to develop the best and brightest in the Memphis area?  Let us help you! 
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


