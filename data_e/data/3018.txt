Accessibility - OIE - University of Memphis    










 
 
 
     



 
    
    
    Accessibility - 
      	OIE
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Office for Institutional Equity
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          EOAA  
                          Harassment  
                          Title IX  
                          ADA  
                          Policies  
                          Resources  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	OIE
                              	  
                          
                              	ADA
                              	  
                         Accessibility 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      The ADA and Accessibility 
                     
                      The University is committed to ensuring equal access to its educational benefits and
                        opportunities. Therefore, the University provides the information on this page in
                        an effort to meet its goal of eliminating barriers for those with disabilities in
                        regard to accessing the programs, services and activities of the University.
                      
                     
                      Physical Access 
                     
                      Individuals with disabilities need to be able to physically access UofM programs,
                        services and activities. Therefore, campus buildings, paths of travel, and other physical
                        facilities are to be accessible to the extent that no individual with a disability
                        is denied access to programs, services, or activities offered by UofM as a consequence
                        of inaccessible physical facilities. Those encountering an access barrier on the UofM
                        campus, may report it to the ADA Coordinator in the Office for Institutional Equity.
                      
                     
                      Information and Materials 
                     
                      Information and materials are any items created, purchased or identified to serve
                        in instruction and/or communicate information both in the curricular and non-curricular
                        setting of the U of M. Examples include:
                      
                     
                      
                        
                         Textbooks (in bound, unbound, kit or package form) and eBooks 
                        
                         Library media (print, non-print and electronic resources) 
                        
                         Informational software content (web/online content and learning objects) 
                        
                         CD-ROMs and DVDs 
                        
                         Videos, slides, films, filmstrips 
                        
                         Learning laboratories 
                        
                         Handouts, presentations, and syllabi 
                        
                      
                     
                      All information presented and materials used must be accessible "from the outset"
                        – which means from the point of design or selection. Following are some resources
                        from WebAIM for ensuring compliance with accessibility of information and technologies.
                        You can also contact Disability Resources for Students for assistance.
                      
                     
                      
                        
                          UofM Top Ten Accessibility Points   
                        
                          Creating Accessible Word Documents   
                        
                          PowerPoint Accessibility   
                        
                          Adobe, Acrobat and PDF Accessibility   
                        
                          The University of Washington's Do-IT  (Disabilities, Opportunities, Internetworking and Technology) program has a variety
                           of resources for faculty for promoting inclusion and success for students with disabilities.
                         
                        
                      
                     
                      Web and Technical Accessibility 
                     
                      Most people today can hardly imagine a life without the internet. However, web accessibility
                        is one of the most critical issues facing higher education. Web accessibility refers
                        to the practice of creating websites that are usable by people of all abilities or
                        disabilities. Following are some resources for ensuring compliance with web accessibility
                        as well as other technical components of your class or program.
                      
                     
                       UofM Universal Design for Learning (UDL)   • Contains resources about the Universal Design for Learning framework and how UDL
                        can assist you in making your courses more inclusive and accessible to a diverse population
                        of learners.
                      
                     
                       WebAIM   • Sponsored by the Center for Persons with Disabilities at Utah State University,
                        this site includes an introductory tutorial, articles for audiences of all levels
                        of expertise, a blog, and an active discussion list.
                      
                     
                       HTCTU Trainings and Tutorials   • Developed by the High Tech Center Training Unit of the California Community College
                        system, this website has tutorials and covers a broad variety of topics related to
                        IT accessibility.
                      
                     
                       2016 ROADMAP TO WEB ACCESSIBILITY IN HIGHER EDUCATION by 3PlayMedia   • A whitepaper that provides guidance for making online university content accessible
                        to as many stakeholders as possible.
                      
                     
                       Web Content Accessibility Guidelines 2.0 (WCAG)   • This is the definitive set of web accessibility guidelines, from the World Wide
                        Web Consortium (W3C).
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Training  
                               OIE sponsors several online training modules, available in Learning Curve. 
                            
                            
                                Contact Us  
                               OIE provides a range of services that support equal opportunity and nondiscrimination. 
                            
                            
                                How to File a Complaint  
                               File a complaint of discrimination, harassment, sexual misconduct or retaliation. 
                            
                            
                                Sexual Misconduct  
                               General information and resources 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


