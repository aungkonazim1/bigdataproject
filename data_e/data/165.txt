Emerging Leaders Frequently Asked Questions - Student Leadership &amp; Involvement - University of Memphis    










 
 
 
     



 
    
    
    Emerging Leaders Frequently Asked Questions - 
      	Student Leadership   Involvement
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Student Leadership   Involvement
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Student Engagement  
                          Leadership   Service  
                          Community Engagement  
                      
                      
                         
                            Leadership   Service   
                            
                               
                                  
                                   Emerging Leaders  
                                        
                                         Overview  
                                        
                                         Apply  
                                        
                                         FAQ  
                                        
                                         Current EL Information  
                                     
                                  
                                   Social Change Scholars  
                                         Overview  
                                         Apply  
                                         FAQ  
                                     
                                  
                                   Leadership Training  
                                         Tiger Leadership Institute  
                                         Leader to Leader  
                                     
                                  
                                   Community Service  
                                         Service on Saturday  
                                         Days of Service  
                                         Volunteer Odyssey  
                                     
                                  
                                   Conference and Retreats  
                                         LeaderShape Institute  
                                         LEAD Conference  
                                         Topical Conferences  
                                     
                                  
                                   Alternative Breaks  
                                    Women’s Leadership   
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Student Leadership   Involvement
                              	  
                          
                              	Leadership   Service
                              	  
                         Emerging Leaders Frequently Asked Questions 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Emerging Leaders Frequently Asked Questions 
                     
                      What is the scholarship amount? 
                     
                      For in-state or border county students, the scholarship is $5,500 per year, and it
                        is split between fall and spring semester installments. Out-of-state students can
                        also receive an amount to pay the difference between in-state and out-of-state tuition.
                        This amount can vary because of the 250R vs. fully out-of-state charges.
                      
                     
                      How many spaces are available? 
                     
                      The program can accept up to 30 students with each incoming freshman class. A portion
                        of those spaces are available to out-of-state students.
                      
                     
                       Does Emerging Leaders have a separate application process?  
                     
                      Yes.  Students apply online through Tiger Scholarship Manager.  Applications open
                        mid-November each year and are due February 1st. By February 1st, a high school senior
                        must have submitted the application to be considered for the program. A review of
                        all applications begins after that deadline.
                      
                     
                      What are the application requirements? 
                     
                      Student interested in the Emerging Leaders Program must have at least a 3.0 GPA on
                        a 4.0 scale and a 21 on the ACT.  Students should also exhibit ongoing involvement
                        and leadership throughout their high school years included, but not limited to clubs
                        and organizations, community service, athletic involvement, or local and community
                        organization membership.
                      
                     
                      How does the selection process work? 
                     
                      All completed applications are reviewed. Due to application volume, we are unable
                        to commit to communicating when an application has been received. The review process
                        takes a look at all application components and utilizes a rubric to assign scores
                        to the essays, high school involvement,  and academics of all candidates. At the end
                        of that review, about 100 candidates are invited to interview with program staff and
                        students.
                      
                     
                      A standard interview protocol is employed for all interviews, including phone and
                        video interviews is a candidate lives too far away to travel to campus. Interviews
                        last about 10 minutes, but they help a great deal for the final selections. Once all
                        interviews have been completed, a final review of all application materials occurs.
                        Applicants are informed of selections soon thereafter.
                      
                     
                      From the February 1st application deadline through to the initial round of selections,
                        the process takes between 6 and 8 weeks. All applicants should anticipate some response
                        by the end of March.
                      
                     
                      What does the program require? 
                     
                      When a student is selected, that student will receive a letter that outlines next
                        steps. All Emerging Leaders live on campus as freshmen, in a residence hall community.
                        Incoming ELs must attend New Student Orientation and Frosh Camp. Finally, a retreat
                        is mandatory for all new students; however, that does not occur until students move
                        on campus in August.
                      
                     
                      Once a student arrives on campus, the obligations are different for each year in the
                        program, but they include special coursework, student involvements, professional development,
                        community service, and leadership speaker attendance. A thorough review for each year
                        is available on the Current EL webpage. For some, it may sound like an overwhelming
                        amount of requirements, but it should take most an average of 3-5 hours per week.
                        Students are guided through their program experiences by staff members and upperclassmen.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Leadership   Service 
                         
                            
                               
                                Emerging Leaders  
                                     
                                      Overview  
                                     
                                      Apply  
                                     
                                      FAQ  
                                     
                                      Current EL Information  
                                  
                               
                                Social Change Scholars  
                                      Overview  
                                      Apply  
                                      FAQ  
                                  
                               
                                Leadership Training  
                                      Tiger Leadership Institute  
                                      Leader to Leader  
                                  
                               
                                Community Service  
                                      Service on Saturday  
                                      Days of Service  
                                      Volunteer Odyssey  
                                  
                               
                                Conference and Retreats  
                                      LeaderShape Institute  
                                      LEAD Conference  
                                      Topical Conferences  
                                  
                               
                                Alternative Breaks  
                                 Women’s Leadership   
                            
                         
                      
                      
                      
                         
                            
                                Our Mission  
                               Learn about the key elements we use to enhance your college experience. 
                            
                            
                                Fraternity   Sorority Affairs  
                               Enhance your college experience by joining a fraternity/sorority! 
                            
                            
                                Tiger Zone  
                               Learn about student organizations, events   involvement opportunities. 
                            
                            
                                Contact Us  
                               Questions? Our team is here to help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/27/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


