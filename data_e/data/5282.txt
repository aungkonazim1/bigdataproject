Using Dropbox GradeMark | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Using Dropbox GradeMark | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Dropbox    »    Evaluating dropbox folder submissions   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Using Dropbox GradeMark                       
        
        
       
          
     
           Printer-friendly version       
	If OriginalityCheck is enabled, you can use the GradeMark functionality to add comments, insert inline text, and highlight sections in file submissions, directly from the Dropbox tool.
 

 
	 Note  OriginalityCheck and GradeMark features are enabled through integration with Turnitin from iParadigms, LLC. To use these features, first obtain a valid Turnitin account, and then login to your account to enable and configure the Desire2Learn integration and obtain your shared key. Visit  http://www.turnitin.com  for more information about obtaining a Turnitin account.
 

 
	Activating GradeMark for your course dropbox folders
 

 
	By default, GradeMark is turned off for your organization. To activate GradeMark, you must first activate OriginalityCheck, and then enable the following config variable for your org unit:
 

    
				Config Variable
			 
			 
				Value
			 
		   
				d2l.3rdParty.Turnitin.HasGradeMark
			 
			 
				On
			 
		    
	Using GradeMark for dropbox submissions
 

 
	If GradeMark is activated for your org unit, when you enable OriginalityCheck and choose to "Generate Originality Reports", the GradeMark functionality is automatically enabled.
 

 
	 Note  Instructors can enter grades via GradeMark one day after the dropbox folder due date has passed. You must create due dates in a dropbox folder's Restrictions tab to make GradeMark grades available to students.
 

 
	Open a version of a dropbox submission in GradeMark
 

  
		On the Folder Submissions page, click  Evaluate Submission  for the applicable user.
	 
	 
		Click the    GradeMark  icon beside the file you want to open. The GradeMark report opens in a new window.
	 
  
	Add comments, highlight text, save, and print your GradeMark report using the available tools in the menu bar. You can view a summary of the submission at the top of the page, including word count, submission date, and Originality Report rating.
 

 
	  

 
	Viewing the Markup information section
 

 
	 Tip  For more detailed help with GradeMark functionality, click the Help menu item on the report.
 

 
	Files compatible with GradeMark
 

 
	The following file types are compatible with GradeMark:
 

  
		MS Word (DOC, DOCX)
	 
	 
		WordPerfect (WPD)
	 
	 
		PostScript (EPS)
	 
	 
		Acrobat PDF
	 
	 
		HTML (HTM, HTML)
	 
	 
		Rich text (RTF)
	 
	 
		Plain text (TXT)
	 
  
	 Note  If your file type is incompatible with GradeMark,    Information  icon displays instead of the    GradeMark  icon.
 
     Audience:    Instructor      

    
           

                   ‹ Flagging dropbox folder submissions 
        
                   up 
        
                   Using Dropbox OriginalityCheck › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Dropbox  
  
      Dropbox basics    Creating and managing Dropbox    Evaluating dropbox folder submissions    Viewing dropbox folder file submissions    Accessing the Evaluate Submission page    Leaving feedback and grading dropbox submissions    Evaluating non-submissions and external submissions    Bulk publishing dropbox feedback evaluations    Downloading dropbox folder submission files    Uploading and attaching feedback from downloaded submission files    Retracting published feedback    Emailing users from a dropbox folder    Marking dropbox folder submissions as read or unread    Flagging dropbox folder submissions    Using Dropbox GradeMark    Using Dropbox OriginalityCheck      
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
