Hooks Open House - UofM Media Room - University of Memphis    










 
 
 
     



 
    
    
    Hooks Open House - 
      	UofM Media Room
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			UofM Media Room
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Publications  
                          News Releases  
                          Events  
                          Resources  
                          Contact Us  
                      
                      
                         
                            News Releases   
                            
                               
                                  
                                   Awards  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	UofM Media Room
                              	  
                          
                              	News Releases
                              	  
                         Hooks Open House 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Hooks Institute Will Host Open House and Panel Discussion Oct. 5 
                     
                      September 26, 2017 - The Benjamin L. Hooks Institute for Social Change at the University
                        of Memphis will lead a community conversation about defending diversity, social justice
                        and human rights in today's America at its annual open house on Oct. 5. At the event,
                        the Hooks Institute will release the 2017 Hooks Institute  Policy Papers Bending the Arc Toward Justice: Including the Excluded . The open house will be held in the University Center Bluff Room (304). A reception
                        will begin at 5:30 p.m., followed at 6 p.m. by a panel discussion led by the authors
                        of the Policy Papers.  The event is free and open to the public. Parking will be available in the Zach Curlin
                        Street garage.  The 2017 Hooks Institute Policy Papers use national conversations about critical civil
                        and human rights issues as a starting point to discuss, and propose solutions to,
                        contemporary issues of race, class, public policy and social justice. "Immigrants
                        in Small Cities: Opportunities and Challenges" by Michael R. Duke (UofM Department
                        of Anthropology) looks at the challenges and opportunities of immigrants from diverse
                        countries who have settled in the small city of Springdale, Ark. "Welfare Dependency
                        and Extractive Economies: Lessons Learned from Pine Ridge" by Peter A. Kindle (Department
                        of Social Work, University of South Dakota) examines how intergenerational poverty
                        and reliance on welfare by the Oglala Sioux Tribe in South Dakota is rooted in historical
                        discrimination and bad government policies.  "Prioritizing Lesbian, Gay, Bisexual and Transgender Lives in the Era of Resistance
                        and Prejudicial Rhetoric" by Idia B. Thurston (UofM Department of Psychology) examines
                        both the legal, social and policy advancements made by the LGBT community, the aggressive
                        challenges underway to undermine these gains, and proposes initiatives to sustain
                        and support this community. Finally, "Memphis and Islam: Integrating Muslims and Islam
                        into the Community Fabric" by Nabil A. Bayakly (UofM Department of World Language
                        and Literature) provides an in-depth overview of Islam, which embraces peace and love
                        of one's neighbor. He explores how Islam has been distorted both by terrorist acts
                        and by a lack of understanding in many communities about Islam and Muslims.
                      
                     
                       About the Benjamin L. Hooks Institute for Social Change  In 1996, University of Memphis officials received approval from the Tennessee Board
                        of Regents to create the Benjamin L. Hooks Institute for Social Change in the College
                        of Arts   Sciences. The mission of the Institute is teaching, studying and promoting
                        civil rights and social change. The Hooks Institute archives, housed in the Mississippi
                        Valley Collection in the University's McWherter Library, include Hooks' personal papers.
                        To learn more about the Hooks Institute, visit  memphis.edu/benhooks .
                      
                     
                      FOR MORE INFORMATION Contact: Nathaniel C. Ball  ncball@memphis.edu  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         News Releases 
                         
                            
                               
                                Awards  
                            
                         
                      
                      
                      
                         
                            
                                Music Performance Calendar  
                               Don't miss a School of Music performance 
                            
                            
                                Art Museum of Memphis  
                               Learn about the latest installations 
                            
                            
                                The Martha and Robert Fogelman Galleries of Contemporary Art  
                               View upcoming Department of Art exhibits 
                            
                            
                                Contact Us  
                               Have a story to share or questions about UofM News? 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


