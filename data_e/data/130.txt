Diagrams - Commencement Office - University of Memphis    










 
 
 
     



 
    
    
    Diagrams - 
      	Commencement Office
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Commencement Office
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Apply to Graduate  
                          Graduates  
                          Accommodations  
                          Diplomas  
                          Guests  
                          Faculty  
                      
                      
                         
                            Graduates   
                            
                               
                                  
                                   Graduating Students  
                                   Attendance   Non-Attendance Form  
                                   Program Proof  
                                   Cord   Stole Policy  
                                   Student Speaker Selection  
                                   Dress  
                                   Undergraduates  
                                         Checklist  
                                         Honors Distinction  
                                     
                                  
                                   Graduate Students  
                                         Checklist  
                                         Graduate School Requirements  
                                         Hooding Information  
                                     
                                  
                                   Law  
                                         Checklist  
                                         Law School Information  
                                     
                                  
                                   Cap and Gown Pick up  
                                   Announcer Information  
                                         Record Difficult Name  
                                     
                                  
                                   Graduation Analyst  
                                         College School List  
                                     
                                  
                                   Diagrams  
                                   Invitations  
                                   Photos  
                                   Alumni /FAM Membership  
                                   FAQs  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Commencement Office
                              	  
                          
                              	Graduates
                              	  
                         Diagrams 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Diagrams 
                     
                      All Commencement events take place at FedExForum, located in downtown Memphis, TN.
                        The facility is the home of the Memphis Grizzlies, NBA Team. For each Commencement,
                        FedExForum can hold over 18,000 graduates and guests combined. Find seating charts
                        below: 
                      
                     
                      Commencement seating areas  
                     
                      
                        
                         Commencement Guests -  Disabled Seating  
                        
                         August Forum Floor -  Student and Guests Seats  
                        
                         December Forum Floor 1st Ceremony -  Student and Guests Seats  
                        
                         December Forum Floor 2nd Ceremony -  Student and Guests Seats  
                        
                         May Forum Floor 1st Ceremony -  Student and Guests Seats  
                        
                         May Forum Floor 2nd Ceremony -  Student and Guests Seats  
                        
                      
                     
                        
                     
                      Directions to FedExForum/drop offs 
                     
                      
                        
                          Route Map   
                        
                          Directions - Disabled Patrons Drop Offs  
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Graduates 
                         
                            
                               
                                Graduating Students  
                                Attendance   Non-Attendance Form  
                                Program Proof  
                                Cord   Stole Policy  
                                Student Speaker Selection  
                                Dress  
                                Undergraduates  
                                      Checklist  
                                      Honors Distinction  
                                  
                               
                                Graduate Students  
                                      Checklist  
                                      Graduate School Requirements  
                                      Hooding Information  
                                  
                               
                                Law  
                                      Checklist  
                                      Law School Information  
                                  
                               
                                Cap and Gown Pick up  
                                Announcer Information  
                                      Record Difficult Name  
                                  
                               
                                Graduation Analyst  
                                      College School List  
                                  
                               
                                Diagrams  
                                Invitations  
                                Photos  
                                Alumni /FAM Membership  
                                FAQs  
                            
                         
                      
                      
                      
                         
                            
                                Contact Us  
                               Got questions? Let us know how we may assist you. 
                            
                            
                                Commencement Attendance Form  
                               Will you be attending Commencement?  Inform us! 
                            
                            
                                News  
                               Learn about possible date changes, future and past Commencements, Honors Assembly
                                 and more.
                               
                            
                            
                                Commencement Live  
                               Can't attend Commencement?  Watch it live! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


