Thurgood Marshall Team - School of Law - University of Memphis    










 
 
 
     



 
    
    
    Thurgood Marshall Team  - 
      	School of Law 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Cecil C. Humphreys School of Law
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Admissions  
                          Programs  
                          Current Students  
                          Faculty  
                          Careers  
                          Library  
                      
                      
                         
                            Programs   
                            
                               
                                  
                                   Degree Programs  
                                         JD  
                                         JD/MBA  
                                         JD/MA in Pol. Science  
                                         JD/MPH  
                                         MPH Fast-Track  
                                         Part-time Program  
                                     
                                  
                                   Certificate Programs  
                                         Advocacy  
                                         Business Law  
                                         Health Law  
                                         Tax Law  
                                     
                                  
                                   Experiential Learning  
                                         Experiential Learning Home  
                                         Legal Clinics  
                                         Externship Program  
                                     
                                  
                                   Law Review  
                                         Law Review Home  
                                         Editorial Board  
                                         Editor-in-Chief  
                                         Joining Law Review  
                                         Virtual Tour of Law Review Suite  
                                         Symposium  
                                         Subscriptions  
                                         Archives  
                                         Current Volume  
                                         Connect with Us  
                                     
                                  
                                   Advocacy Program  
                                         Moot Court Board  
                                         Joining Moot Court Board  
                                         In-School Competitions  
                                         Travel Team Competitions  
                                         Past Champions  
                                     
                                  
                                   Inst. for Health Law   Policy  
                                         Institute Home   
                                         Advisory Committee  
                                         Symposium  
                                         Pro Bono Opportunities  
                                         Health Law Certificate  
                                         Health Law Society  
                                         MBA Health Law Section  
                                     
                                  
                                   International Law Programs  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	School of Law 
                              	  
                          
                              	Programs
                              	  
                         Thurgood Marshall Team  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                        
                     
                      Thurgood Marshall Mock Trial Competition 
                     
                      This team is comprised of members of BLSA (Black Law Students Association) who want
                        to learn and practice trial advocacy skills. This competition allows you to practice
                        skills such as opening and closing arguments, direct and cross examinations of witnesses,
                        arguing pre-trial motions, objections, and offering evidence. Team members get to
                        practice on both sides of the action until teams of two are selected to be prosecution
                        or defense.
                      
                     
                      This competition is held in January of every year at the NBLSA convention. Attorneys
                        Kenneth Walker (Lewis Thomason) and Will Perry (Butler Snow) coach the team.
                      
                     
                      For information on this competition and past issues please visit the  competition website.  
                     
                      Recent members: 
                     
                      
                        
                         Faith Sanford, Brandon Boykin, Kioni Logan, Christian West-Coleman (2015-16)
                           
                            
                              
                               Topic: Criminal—murder, with a focus on causation issues 
                              
                            
                           
                         
                        
                         Places traveled:
                           
                            
                              
                               Durham, North Carolina (2016) 
                              
                            
                           
                         
                        
                      
                     
                      
                        
                         A student's perspective: 
                        
                         "Our coaches were energetic and were able to give us positive advice at each practice.
                           Participation on this team is a challenging, yet rewarding experience for anyone looking
                           to grow as an attorney."
                         
                        
                         - Christian West-Coleman 
                        
                      
                     
                      Thurgood Marshall Mock Trial Competition Past Acheivements: 
                     
                      
                        
                         Regional Finalists 2015 -Brittany Williams, Jana Mitchell competed at the National
                           Level in 2015.
                         
                        
                         Regional Finalist 2010-2011 
                        
                         National Champions 2010-2011 
                        
                         Regional Second Place - 2010 
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Programs 
                         
                            
                               
                                Degree Programs  
                                      JD  
                                      JD/MBA  
                                      JD/MA in Pol. Science  
                                      JD/MPH  
                                      MPH Fast-Track  
                                      Part-time Program  
                                  
                               
                                Certificate Programs  
                                      Advocacy  
                                      Business Law  
                                      Health Law  
                                      Tax Law  
                                  
                               
                                Experiential Learning  
                                      Experiential Learning Home  
                                      Legal Clinics  
                                      Externship Program  
                                  
                               
                                Law Review  
                                      Law Review Home  
                                      Editorial Board  
                                      Editor-in-Chief  
                                      Joining Law Review  
                                      Virtual Tour of Law Review Suite  
                                      Symposium  
                                      Subscriptions  
                                      Archives  
                                      Current Volume  
                                      Connect with Us  
                                  
                               
                                Advocacy Program  
                                      Moot Court Board  
                                      Joining Moot Court Board  
                                      In-School Competitions  
                                      Travel Team Competitions  
                                      Past Champions  
                                  
                               
                                Inst. for Health Law   Policy  
                                      Institute Home   
                                      Advisory Committee  
                                      Symposium  
                                      Pro Bono Opportunities  
                                      Health Law Certificate  
                                      Health Law Society  
                                      MBA Health Law Section  
                                  
                               
                                International Law Programs  
                            
                         
                      
                      
                      
                         
                            
                                Apply to Memphis Law  
                                
                            
                            
                                News   Events  
                                
                            
                            
                                Alumni   Support  
                                
                            
                            
                                ABA Required Disclosures  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


