Grade Policy - Biological Sciences - University of Memphis    










 
 
 
     



 
    
    
    Grade Policy  - 
      	Biological Sciences
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Biological Sciences
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Undergraduate  
                          Graduate  
                          Centers  
                          People  
                          Research  
                          Events  
                          Scholarships  
                      
                      
                         
                            Undergraduate Resources   
                            
                               
                                  
                                   Undergraduate Advising/BARC  
                                   Major Requirements  
                                   Biology Degree Planning Tools  
                                   Common Minors for Biology Majors  
                                   Biology Minor Requirements  
                                   Grade Policy and Course Repeat Policy  
                                   Petition to repeat a course a second time  
                                   Frequency of Course Offerings  
                                   Undergraduate Research  
                                   Advanced Placement  
                                   Scholarships  
                                   Release for letter of recommendation  
                                   Alumni  
                                   Undergraduate Catalog  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Biological Sciences
                              	  
                          
                              	Undergraduate
                              	  
                         Grade Policy  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Grade Policy 
                     
                      Students must earn a grade of at least "C-" in all biology courses applied toward
                        the major, the minor, and in all required chemistry, physics, and mathematical courses.
                      
                     
                      Students must earn a grade of at least "C-" in CHEM 1110/1111 before enrolling in
                        any Biology course towards the major.
                      
                     
                      Students must earn a grade of at least "C-" in BIOL 1110/1111 and BIOL 1120/1121,
                        before enrolling in any upper division biology course. Students must earn a grade
                        of at least "C-" in CHEM 1110/1111 before enrolling in any Biology course towards
                        the major.
                      
                     
                      Any required or elective course applied toward the major or minor may be repeated
                        twice (taken three times) to earn a minimum grade of "C-." To repeat a course for
                        the second time, a student must fill out a  repeat request form  and receive approval from the Undergraduate Studies Committee. Application information
                        is available in the BARC. A student may not repeat a course more than three time,
                        regardless of where taken, to receive a minimum grade of "C-."
                      
                     
                       Academic Regulations  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Undergraduate Resources 
                         
                            
                               
                                Undergraduate Advising/BARC  
                                Major Requirements  
                                Biology Degree Planning Tools  
                                Common Minors for Biology Majors  
                                Biology Minor Requirements  
                                Grade Policy and Course Repeat Policy  
                                Petition to repeat a course a second time  
                                Frequency of Course Offerings  
                                Undergraduate Research  
                                Advanced Placement  
                                Scholarships  
                                Release for letter of recommendation  
                                Alumni  
                                Undergraduate Catalog  
                            
                         
                      
                      
                      
                         
                            
                                Biology@Memphis Newsletter  
                               Read about graduate and undergraduate student news; awards, grants and scholarships;
                                 publications, and more...
                               
                            
                            
                                Administration  
                               Budgets, accounting, facilities management, committees, meetings, and group email
                                 accounts
                               
                            
                            
                                Facilities  
                               Not your ordinary classrooms... 
                            
                            
                                Contact Us  
                               Main office contacts, faculty and staff administration 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/28/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


