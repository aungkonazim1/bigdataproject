Deleting a group in Wiggio | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Deleting a group in Wiggio | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Integrations    »    Wiggio    »    Managing groups in Wiggio   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Deleting a group in Wiggio                       
        
        
       
          
     
           Printer-friendly version       
	 Notes 

	  
			Only group administrators can delete the group.
		 
		 
			Deleting a group deletes all subgroups associated with the group.
		 
	  

  
		Click on the group you want to delete from the Groups area.
	 
	 
		Select  Close Group  from the More Actions drop-down list, then click  Ok .
	 
      Audience:    Learner      

    
           

                   ‹ Changing the owner for a group in Wiggio 
        
                   up 
        
                   Updating a subgroup in Wiggio › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Wiggio  
  
      Wiggio basics    Creating and using groups in Wiggio    Managing groups in Wiggio     Editing settings for a group in Wiggio    Updating membership for a group in Wiggio    Changing the owner for a group in Wiggio    Deleting a group in Wiggio    Updating a subgroup in Wiggio      Adding resources to Wiggio    Creating and using folders in Wiggio    Using virtual meetings in Wiggio    Using polls in Wiggio    Using to-do lists in Wiggio    Managing events in Wiggio    Importing and exporting calendars in Wiggio    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
