Retention of Acquisitions Documents - Libraries - University of Memphis    










 
 
 
     



 
    
    
    Retention of Acquisitions Documents - 
      	Libraries
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			University Libraries
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Services  
                          Resources  
                          Instruction  
                          Special Collections  
                          Technology  
                      
                      
                         
                            Collection Management   
                            
                               
                                  
                                   Department Liaisons  
                                   Staff  
                                   Serials Review  
                                   Textbook Selections  
                                         Handbook Addendum  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Libraries
                              	  
                          
                              	Collection Management
                              	  
                         Retention of Acquisitions Documents 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Retention of Acquisitions Documents 
                     
                      For paper business documents, the Collection Management Department will observe the
                        following schedule (storage space permitting):
                      
                     
                      
                        
                         
                           
                            
                              
                               Category 
                              
                               Retention  Time
                               
                              
                               Responsibility  for Disposal
                               
                              
                            
                           
                            
                              
                               
                                 
                                  Paid invoices (by vendor) 
                                 
                               
                              
                               
                                 
                                  as long as 
                                 
                                  space permits 
                                 
                               
                              
                               
                                 
                                  Dept. Head 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  Closed correspondence on standing orders 
                                 
                               
                              
                               
                                 
                                  as long as 
                                 
                                  space permits 
                                 
                               
                              
                               
                                 
                                  Library Assistant II 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  Closed correspondence on firm orders 
                                 
                               
                              
                               
                                 
                                  2 years 
                                 
                               
                              
                               
                                 
                                  Library Assistant II 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  Purchase orders (list by P.O. #) 
                                 
                               
                              
                               5 years 
                              
                               
                                 
                                  Library Assistant III 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  Purchase orders (by vendors) 
                                 
                               
                              
                               
                                 
                                  5 years 
                                 
                               
                              
                               
                                 
                                  Library Assistant III 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  Ads for completed firm orders 
                                 
                               
                              
                               
                                 
                                  1-1/2 years 
                                 
                               
                              
                               
                                 
                                  Library Assistant III 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  Duplicate invoices (by vendor) 
                                 
                               
                              
                               
                                 
                                  5 years 
                                 
                               
                              
                               
                                 
                                  Library Assistant III 
                                 
                               
                              
                            
                           
                         
                        
                      
                     
                      Electronic invoices are preserved within the Integrated Library Systems data files;
                        retention of ILS data is the responsibility of the Systems department. Electronic
                        correspondence is subject to the retention schedule established by the University
                        IT department.
                      
                     
                      
                     
                        
                     
                      Please send questions/comments/suggestions to  Gail Barton , Interim Head of Collection Management
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Collection Management 
                         
                            
                               
                                Department Liaisons  
                                Staff  
                                Serials Review  
                                Textbook Selections  
                                      Handbook Addendum  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Ask-a-Librarian  
                               Got a question? Got a problem? Ask Us! 
                            
                            
                                Interlibrary Loan  
                               Request books, articles, and other research materials from other libraries. 
                            
                            
                                Reserve space  
                               Group study and presentation spaces available in the library. 
                            
                            
                                My Library Account  
                               Review your library account for due dates and more. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


