Following other users' blogs   
 
 
 
 
 
 Following other users' blogs 
 













 
 

      
 
 Following other users' blogs 
 You can add peers, friends and acquaintances to your Blog Watch to keep current on topics and events that interest them. 
 The  Blog Watch  list is empty by default. You must first add blogs to watch from the  Blog List  to populate your  Blog Watch . 
   Add users to your Blog Watch 
 
 From the Entry List page, click the    Blog List  icon. 
 You can then either:
 
 Enter any portion of the first or last name of the user you want to add in the  Search For  field. Click the  Show Search   Options  link for more options. 
  navigate the entire list of users. You can sort the table by clicking on any of the column names. If you go to a new page you lose the users you previously on the previous page.  
 
 
 Select the box beside the user’s name, or select multiple users. 
 Click the    Add selected users to Blog Watch  icon at the top or bottom of the list. 
 
   View blogs you follow 
 From the Entry List page, click the    Blog Watch  icon to see recent posts from the blogs you follow. 
   Delete users from your Blog Watch 
 
 From the Entry List page, click the    Blog Watch  icon. 
 Select the users you want to delete. 
 Click the    Remove selected users from Blog Watch  icon. 
 
   
 
   
 
 
 
  Desire2Learn Help  |  About Learning Environment  
 © 1999-2010 Desire2Learn Incorporated. All rights reserved. 
 

 
 
