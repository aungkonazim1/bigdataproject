Locking a discussion forum or topic | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Locking a discussion forum or topic | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Discussions    »    Discussion forum and topic restrictions   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Locking a discussion forum or topic                       
        
        
       
          
     
           Printer-friendly version       
	Locking a topic prevents users from creating new posts or modifying existing posts, essentially making the conversation “read only”. Users with permission can modify the contents of a locked topic. Locking a topic is an effective way of closing a conversation while allowing users to refer back to posts.
 

 
	You can specify start and end dates for when a forum or topic unlocks so you can set things up ahead of time to match your course’s calendar.
 

 
	 Tip  Consider locking a topic to end a conversation, then using a pinned summary post inside the topic. When users access the topic, your summary appears at the top.
 

 
	Lock a forum or topic
 

  
		On the Discussions List page, click      Edit  from the context menu of the forum or topic you want to hide. You can also set visibility options when creating a new forum or topic.
	 
	 
		In the Locking Options section of the Properties tab, select the options you want:
		  
				To lock the forum or topic immediately, select  Lock forum  or  Lock topic . The forum or topic remains locked until you select  Unlock forum  or  Unlock topic  again.
			 
			 
				To open the forum or topic within a date range, select  Unlock forum for a specific date range  or  Unlock topic for a specific date range , then select  Has Start Date  or  Has End Date  to specify the dates.
			 
		  
	 
		Click  Save .
	 
      Audience:    Instructor      

    
           

                   ‹ Hiding a discussion forum or topic 
        
                   up 
        
                   Setting release conditions for a discussion forum or topic › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Discussions  
  
      Participating in discussions    Following discussions    Creating and managing discussions    Creating discussion forums and topics    Discussion forum and topic restrictions    Hiding a discussion forum or topic    Locking a discussion forum or topic    Setting release conditions for a discussion forum or topic    Setting group and section restrictions for a discussion forum or topic      Topic assessment    Topic objectives    Editing a discussion forum or topic    Copying a discussion forum, topic, thread, or post    Deleting discussion forums, topics, threads, and posts    Reordering discussion forums and topics    Restoring a deleted discussion forum, topic, thread, or post      Monitoring discussions    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
