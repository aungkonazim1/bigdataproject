Portfolio Requirements - LEAD - University of Memphis    










 
 
 
     



 
    
    
    Portfolio Requirements  - 
      	LEAD
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Leadership
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Higher Ed/Adult Ed  
                          Leadership   PS  
                          Faculty   Staff  
                          News  
                      
                      
                         
                            HIAD Menu   
                            
                               
                                  
                                   Online Degree Resources  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	LEAD
                              	  
                          
                              	HIAD
                              	  
                         Portfolio Requirements  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                       Portfolio Requirements  
                     
                       College of Education   Department of Leadership  
                     
                       M.S. Leadership and Policy Studies Leadership   Student Personnel Concentrations  
                     
                      The portfolio is to be based on the compilation of objectives in the student's program
                        of studies and the program competencies delineated in the  Portfolio Matrix .
                      
                     
                      In planning the portfolio, students should 
                     
                      
                        
                         Meet with program advisor to review portfolio contents 
                        
                         Meet with program advisor to review portfolio in progress 
                        
                         Submit completed portfolio to advisor: Prior to the committee receiving the link to
                           your portfolio, your committee chair must review and approve the content. Please give
                           your chair adequate time for this review process. Once approved, the portfolio must
                           be submitted to the committee at least two weeks prior to your defense date.
                         
                        
                         Meet with program advisor to select one of the scheduled dates for your defense. The
                           program advisor will put you on the schedule.
                         
                        
                          Portfolio dates will be posted each semester on the HIAD web site. You should be
                           prepared to defend your portfolio around mid-term of the semester you plan to graduate.
                           (fall or spring semesters)
                         
                        
                         Email announcement of portfolio defense to all HIAD faculty members. 
                        
                         Defend approved portfolio at meeting of faculty committee. Students outside Memphis
                           may conduct their portfolio defense via UMmeet Web Conferencing.
                         
                        
                      
                     
                      Examples of items that could be included in the portfolio are: 
                     
                       1. Evidence of classroom experience      papers      projects      summaries of activities      annotated bibliography 2. Applications of program learning in a work related experience    curriculum/ training materials    workshops planned and/or attended    programs planned    committee work  These items must appear in every portfolio:  
                     
                      1. An up-to-date resume 
                     
                      2. An approved program of studies 
                     
                      3. Table (matrix) that indicates the courses and competencies 
                     
                      4. An artifact (paper, project report, video, photograph) documenting each competency.
                        If possible, use the graded paper (including faculty comments). If you have a revised
                        or updated version, you may include both.
                      
                     
                      In addition: 
                     
                      
                        
                         Each item included in your portfolio must be dated 
                        
                         If the item submitted as evidence is course related, the course number must be included
                           and the graded version must be submitted
                         
                        
                         Time, place, and nature of the activity from which the artifact derives must be clear 
                        
                         Submit the artifact that is best evidence. For example, a committee report listing
                           you as a member is better than an internship log page claiming participation
                         
                        
                         A written reflection should be prepared to accompany each artifact. 
                        
                         The student must show in the reflection the relationship that exists between the artifact
                           and the competency.
                         
                        
                         In the reflection you may give examples from up to three courses to demonstrate you
                           have met the compentency.
                         
                        
                         In the reflection, compare and contrast your experience with theories you studied
                           in your masters classes.
                         
                        
                         At your portfolio defense, prepare to talk about the following: 
                        
                      
                     
                      1. Look back across the materials in your portfolio and reflect on the process and
                        progress you have made towards becoming a competent educational leader. Take a holistic
                        look at your growth.
                      
                     
                      
                        
                         What successes can you describe? 
                        
                         Where did you miss the mark? 
                        
                         What did you learn? 
                        
                         How has your experience resulted in the improvement of your leadership skills? 
                        
                      
                     
                      2. During the final presentation of your portfolio to the faculty, you will be asked
                        to reflect on your professional growth. Focus on what you have accomplished and how
                        you know you have expanded your abilities in these areas. Develop a strategy for your
                        future to continue to hone your leadership skills.
                      
                     
                       Electronic Portfolio Guide/Tutorial  
                     
                      The portfolio must be submitted in an electronic format. We recommend a program called
                        Weebly. This is a free website builder that can be accessed with your browser at  http://www.weebly.com /. Additionally, the links below provide a tutorial to assist in developing your portfolio.
                        Within the tutorial you will find a sample portfolio from a former student who used
                        Weebly.
                      
                     
                       Weebly Tutorial Handout   Weebly Video Tutorial  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         HIAD Menu 
                         
                            
                               
                                Online Degree Resources  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now   
                               Start an application and view the Graduate Catalog 
                            
                            
                                About Us  
                               Learn more about the Department of Leadership and what we do 
                            
                            
                                Need Help? Contact Us!   
                               Questions or comments? Get in touch with our support staff! 
                            
                            
                                Online Resources   
                               A list of useful links, forms, and documents for current and prospective Leadership
                                 students 
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


