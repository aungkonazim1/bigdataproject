Chair of Excellence - Department of Art - University of Memphis    










 
 
 
     



 
    
    
    Chair of Excellence - 
      	Department of Art
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Art
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          People  
                          Facilities  
                          Resources  
                          News/Events  
                      
                      
                         
                            About Us   
                            
                               
                                  
                                   Contact Us  
                                   Community  
                                         A.R.T.S.  
                                         Photographic Society  
                                     
                                  
                                     
                                   Exhibition Space  
                                         BOX Gallery  
                                         Fogelman Galleries  
                                         AMUM  
                                     
                                  
                                   IEAA  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of Art
                              	  
                          
                              	About Us
                              	  
                         Chair of Excellence 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                       
                     
                      Dorothy Kayser Hohenberg Chairs of Excellence in Art History Inaugurated April 2,
                        1989
                      
                     
                      The Dorothy Kayser Hohenberg Chair of Excellence in Art History (COE) is designed
                        for a visiting scholar in art history whose field of research will supplement the
                        disciplines of the full-time faculty in the Department of Art. Duration of residence
                        may vary from one semester to three years. All fields of art history and visual culture
                        are welcomed. Specific duties involve teaching one upper-level undergraduate course
                        and a graduate seminar in the COE's area of expertise, possibly serving as a reader
                        on MS thesis committees. The holder of this chair is expected to maintain residence
                        and lecture in the city as well as maintain visibility in research nationally. To
                        facilitate the accomplishments of these duties, the COE are provided with a travel
                        budget for research and conference attendance, book allowance, and a graduate assistant.
                        The holder of this chair also reviews the university library holdings in her/his area
                        and recommend acquisitions where appropriate. Ph.D. in art history, visual culture,
                        or an appropriate related fields are encouraged to apply. Scholars who hold a terminal
                        degree with experience in either academic or museum settings are also eligible. We
                        welcome senior and junior scholars who have established a significant publication
                        record nationally and internationally in their field.
                      
                     
                      Pamela Nunn 
                     
                      2016-17, Independent, Feminist Art    
                     
                          BIOGRAPHY  
                     
                       Pamela Gerrish Nunn  began her career as an art historian in her native country of England, teaching in
                        Bristol from 1976 and publishing in the field of 19th-century women artists from 1978.
                        She moved to New Zealand in 1989 to teach at the University of Canterbury (Christchurch),
                        where she worked for 20 years. After the 2011 earthquake in Christchurch, she moved
                        north to the Wellington area, where she works as a freelance lecturer, curator and
                        researcher.
                      
                     
                                                  
                     
                        
                     
                      
                     
                      The following list includes past COEs: 
                     
                       Celeste Bernier , 2014-15, U of Nottingham, African Diaspora
                      
                     
                       Yasser Tabbaa , 2013-14, Independent, Islamic
                      
                     
                       Edward Shanken , 2012-13, Independent, New Media
                      
                     
                       Julie McGee , 2011-12, U of Delaware, African Diaspora,  UDaily  
                     
                       Mikelle Smith Omari-Tunkara , Spring 2010 and 2010-2011, U of Arizona, African Diaspora
                      
                     
                       Katherine Hoffmann , Spring 2009, St. Anselm, Photography
                      
                     
                       Till-Holger Borchert , 2007-2008, Bruges Museum, Renaissance
                      
                     
                       Nigel Strudwick , 2006-2007, British Museum, Egyptian
                      
                     
                       Kenneth Haltman , 2005-2006, U of Oklahoma, American
                      
                     
                       Francesco Tiradritti,  2004-2005, U of Naples, Egyptian
                      
                     
                       Joan Holladay,  2003-2004, UT Austin, Medieval
                      
                     
                       Christopher Reed , 2002-2003, Penn State, Gender Studies
                      
                     
                       Doris Srinivasan , 2001-2002, Independent, India
                      
                     
                       Paula Gerson , 2000-2001, FSU, Medieval
                      
                     
                       Michael Weaver , Spring 2000, Oxford, Photography
                      
                     
                       Christopher Johns , 1997-1998, Vanderbilt, 18th-19th Century
                      
                     
                       Marcia Kupfer , 1996-1997, Ohio State, Medieval
                      
                     
                      * Andrew Ladis , 1995-1996, UGA, Renaissance
                      
                     
                      * Mary Sheriff , 1994-1995, UNC, 18th Century
                      
                     
                       Bruce Cole Spring , 1994, Renaissance Center, Am.Revolution
                      
                     
                       Norman Land , 1992-1993, Un Mo., Columbia, Renaissance
                      
                     
                       Michael Weaver Fall , 1991 Oxford University, Photography
                      
                     
                       Babatunde Lawal , 1990-1991, VCU, African Art
                      
                     
                      *deceased 
                     
                     
                     	
                      
                   
                
                
                   
                      
                         About Us 
                         
                            
                               
                                Contact Us  
                                Community  
                                      A.R.T.S.  
                                      Photographic Society  
                                  
                               
                                  
                                Exhibition Space  
                                      BOX Gallery  
                                      Fogelman Galleries  
                                      AMUM  
                                  
                               
                                IEAA  
                            
                         
                      
                      
                      
                         
                            
                                Apply to the UofM  
                               Be part of our artistic community. 
                            
                            
                                Art Appreciation  
                               Visit the Martha and Robert Fogelman Galleries of Contemporary Art 
                            
                            
                                Support the College  
                               Your gift supports future professional artists. 
                            
                            
                                Contact Us  
                               Office hours and location 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


