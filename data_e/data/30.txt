UofM Global Student Services and Support - UofM Global - University of Memphis  UofM Global Student Services and Support  










 
 
 
     



 
    
    
    UofM Global Student Services and Support - 
      	UofM Global
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






 

   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 

   
   
   
    
      
      
       
 
	
	
	
 
 

 


 
 

 
  
 
	 
		 
			 
				 
					     
				 
				     
				 
					 
						 
							 
								 
									   UofM Home  
										  myMemphis  
										  eCourseware  
										  Webmail  
										  Contact  
								     
								 
								 
									 Search 
									 
									 
									    
									 Site Index 
								 																
								  
							 
						 
					 
					 
						  
							 
								   About UofM Global      
									 
										  UofM Global  
										  Ranked Online Degrees  	
										  The UofM Global Experience  
										  Our Faculty  
										  Tuition   Fees  	
										  Accreditations  
									 
								 
								   Online Degrees      
									 
										  Online Degrees  
										  Undergraduate Degrees  
										  Graduate Degrees  
										  Graduate Certificates  
										  Online Admissions  
										  Contact Us  
									 
								 
								   Online Admissions      
									 
										  Online Admissions  
										  Apply Now   
										  Smart Start  
										  Financial Aid  
										  All Online Services  
									 
								 
								   Current Students      
									 
										  Student Services and Support  
										  Online Advising  
										  Course Registration  
										  Alternative Degree Pathways  
										  Learning Support  
										  Technology Support  
										  Library Services  
										  Career Services  
										  Additional Services  
										  Graduation  
									 
								 
								  Request Info  
							 
						 
					 
				 
			 
		 
		 
			 
				 
					  Online Degrees  
				 
			 
		 
	 
 
 
      
      
      
    
    
       
          
             
                
                   
                       
                           			UofM Global
                           	  
                   
                
             
          
       
       
          
             
                
                   
                      
                         
                             Home  
                             
                                 	UofM Global
                                 	  
                            
                              	Student Services
                              	
                            
                         
                      
                      
                         
                            Student Services and Support   
                            
                               
                                  
                                   Student Services and Support  
                                   Online Advising Menu  
                                         Overview  
                                         Degree Planning  
                                         Find My Advisor  
                                     
                                  
                                   Course Registration  
                                         Overview  
                                         Online Course Search  
                                         Undergraduate Catalog  
                                         Graduate Catalog  
                                         Dates and Deadlines  
                                         Tuition   Fees  
                                         Scholarships  
                                     
                                  
                                   Alternative Degree Pathways  
                                         Overview  
                                         Finish Line Program  
                                         Prior Learning Assessment  
                                     
                                  
                                   Learning Support  
                                         Overview  
                                         Readiness Assessment  
                                         Success Strategies  
                                         Get Connected  
                                         Online Tutoring  
                                     
                                  
                                   Technology Support  
                                         Overview  
                                         Technology Requirements  
                                         Additional Services  
                                         Online Course Preview  
                                     
                                  
                                   Library Services  
                                         Overview  
                                         Ask-a-Librarian  
                                         Electronic Delivery  
                                         Research Consultations  
                                     
                                  
                                   Career Services  
                                   Additional Services  
                                         Overview  
                                         Adult and Veteran Students  
                                         Disability Resources  
                                     
                                  
                                   Graduation  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                     		
                     
                      
                     
                      Your Degree. Your Success. 
                     
                      Through UofM Global, the University of Memphis brings learning to you.  From admission to graduation, we are invested in your success as a UofM Global student.  Each of our online degree programs are developed by University of Memphis faculty
                        for University of Memphis students. Our online courses provide flexibility, so you
                        can access your class and the many resources available to you as a student at a major
                        university. Most of our online course and programs are also completely asynchronous,
                        which means you can participate in class and access support services  anytime, anywhere .
                      
                     
                      Services available to you include registration, advising, learning support, technology
                        support, career services, tutoring, and more. Each of these services provide you an
                        opportunity to reach your personal and professional goals in a fully online environment
                        and at times and days convenient for you. See the links below to learn more about
                        services available to you as a UofM Global student.
                      
                     
                      UofM Global Student Services Resources 
                     
                      
                        
                          Enroll in Courses  
                        
                          Seek Academic Advising  
                        
                          Find Learning and Academic Support  
                        
                          Pursue Alternative Degree Pathways  
                        
                          Get Technology Support  
                        
                          Access Library Services  
                        
                          Complete Your Degree  
                        
                          Establish Your Career  
                        
                      
                     
                      Service and Support Orientation 
                     
                      This website is also designed as an  orientation to fully online services and support  available to you as a UofM Global student. To complete this orientation, click the
                        link to the bottom-right of each page, beginning with Advising.
                      
                     
                        Advising      
                     
                     	
                      
                   
                
                
                   
                      
                         Student Services and Support 
                         
                            
                               
                                Student Services and Support  
                                Online Advising Menu  
                                      Overview  
                                      Degree Planning  
                                      Find My Advisor  
                                  
                               
                                Course Registration  
                                      Overview  
                                      Online Course Search  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Dates and Deadlines  
                                      Tuition   Fees  
                                      Scholarships  
                                  
                               
                                Alternative Degree Pathways  
                                      Overview  
                                      Finish Line Program  
                                      Prior Learning Assessment  
                                  
                               
                                Learning Support  
                                      Overview  
                                      Readiness Assessment  
                                      Success Strategies  
                                      Get Connected  
                                      Online Tutoring  
                                  
                               
                                Technology Support  
                                      Overview  
                                      Technology Requirements  
                                      Additional Services  
                                      Online Course Preview  
                                  
                               
                                Library Services  
                                      Overview  
                                      Ask-a-Librarian  
                                      Electronic Delivery  
                                      Research Consultations  
                                  
                               
                                Career Services  
                                Additional Services  
                                      Overview  
                                      Adult and Veteran Students  
                                      Disability Resources  
                                  
                               
                                Graduation  
                            
                         
                      
                      
                     
                     
                      
                                 Need Help? 
                                 
                                     
                                         
											     Call 844-302-3886 
                                         
                                     
                                     
                                         
                                                 Email Us 
                                         
                                     
                                 
                             
                     
                     
                     
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
 
 
 
 
  Full sitemap  
 
 
 
 
  About UofM Global  
 
 
  UofM Global  
  Ranked Online Degrees  	
  The UofM Global Experience  
  Our Faculty  
  Tuition   Fees  	
  Accreditations  
 
  
 
  Online Degrees  
 
 
  Online Degrees  
  Undergraduate Degrees  
  Graduate Degrees  
  Graduate Certificates  
  Online Admissions  
  Contact Us  
 
  
 
 
 
  Online Admissions  
 
 
  Online Admissions  
  Apply Now   
  Smart Start  
  Financial Aid  
  All Online Services  
 
  
  Contact Us  
 
  Tuition  
  Financial Aid  
  
 
	  Current Students  
 
 
  Student Services and Support  
  Online Advising  
  Course Registration  
  Alternative Degree Pathways  
  Learning Support  
  Technology Support  
  Library Services  
  Career Services  
  Additional Services  
  Graduation  
 
  
 
 
 
 
 
   
 
 
   
 
 
   
 
 
 
 
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/10/17 
                                  
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


