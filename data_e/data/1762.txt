Population Health Certificate - SPH - University of Memphis    










 
 
 
     



 
    
    
    Population Health Certificate - 
      	SPH
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			School of Public Health
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          People  
                          Students   Alumni  
                          News  
                          Research  
                      
                      
                         
                            Programs   
                            
                               
                                  
                                   MHA  
                                         Admission  
                                         Apply Now  
                                         Competencies  
                                         Day One  
                                         Internship  
                                         Handbook  
                                         Mission, Vision, and Values  
                                         Newsletter  
                                         Program Requirements  
                                     
                                  
                                   MPH   
                                         Admissions  
                                         Accelerated MPH Program  
                                         Dual JD/MPH Program  
                                         Apply Now  
                                         Competencies  
                                         Course Listings  
                                         Handbook  
                                         Practicum  
                                         Program Requirements   
                                     
                                  
                                   Epi PhD   
                                         Admissions  
                                         Apply Now  
                                         Competencies  
                                         Curriculum  
                                         Handbook  
                                     
                                  
                                   HSP PhD   
                                         Admissions  
                                         Apply Now  
                                         Competencies  
                                         Curriculum  
                                         Handbook  
                                     
                                  
                                   SBS PhD   
                                         Admissions  
                                         Apply Now  
                                         Competencies  
                                         Curriculum  
                                         Handbook  
                                     
                                  
                                   Population Health Certificate  
                                   Health Analytics Certificate  
                                   Health Systems Leadership Certificate  
                                   Interpreter Course   
                                         Interpreter Course Information  
                                         Interpreter Course Requirements  
                                         Interpreter Course Registration   Payment  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	SPH
                              	  
                          
                              	Programs
                              	  
                         Population Health Certificate 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                        
                     
                      Population Health Graduate Certificate 
                     
                      SPH is pleased to offer a graduate certificate in Population Health.  The certificate,
                        which can be completed in two semesters, offers focused academic training for public
                        health practitioners and other healthcare leaders who are interested in improving health
                        outcomes in the community.  Credit earned for the courses may be applied to an MPH
                        degree.  Courses include:
                      
                     
                      
                        
                         PUBH 7150 Biostatistical Methods (Fall) 
                        
                         PUBH 7170 Epidemiology in Public Health (Fall) 
                        
                         HADM 7105 Health Policy and Organization of Health Services (Spring) 
                        
                         PUBH 7120 Environmental Health (Spring) 
                        
                         PUBH 7160 Social and Behavioral Science Principles (Spring) 
                        
                      
                     
                      admission requirements 
                     
                      Students must have completed a bachelor's degree with a cumulative grade point average
                        of at least 2.5, and apply for admission to the  University of Memphis Graduate School  as a non-degree seeking student, selecting Graduate Certificate in Population Health
                        from the drop-down menu.  Acceptance into the certificate program is not an implied
                        acceptance into any master's degree program. The following is required:
                      
                     
                      
                        
                         Transcripts of undergraduate degree program and any prior graduate study; to be submitted
                           to the University of Memphis Graduate School.
                         
                        
                         A completed  application form and a one-page letter describing applicant's intent to pursue the certificate, and
                           its relevance to his/her career goals; to be submitted to the Academic Services Coordinator
                           II (contact information is provided below).
                         
                        
                         Applicants for whom English is not the primary language are required to meet the University's
                           minimum TOEFL score.
                         
                        
                      
                     
                      certificate and graduation requirements 
                     
                      Students must complete the five courses (15 credit hours) listed above, with a minimum
                        grade of B in each course.  All five courses must be completed within five (5) years
                        of enrollment.  In the semester of graduation, the student must submit the Intent
                        to Graduate form to the University of Memphis Graduate School and a Graduate Certificate
                        Candidacy form to the School's Director of Graduate Studies by the deadline specified
                        by the University of Memphis Graduate School.
                      
                     
                      for more information, or to apply, please contact: 
                     
                      Shirl Sharpe, MS, Academic Services Coordinator II 225 Robison Hall 901.678.1710  Shirl Sharpe  
                     
                        
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Programs 
                         
                            
                               
                                MHA  
                                      Admission  
                                      Apply Now  
                                      Competencies  
                                      Day One  
                                      Internship  
                                      Handbook  
                                      Mission, Vision, and Values  
                                      Newsletter  
                                      Program Requirements  
                                  
                               
                                MPH   
                                      Admissions  
                                      Accelerated MPH Program  
                                      Dual JD/MPH Program  
                                      Apply Now  
                                      Competencies  
                                      Course Listings  
                                      Handbook  
                                      Practicum  
                                      Program Requirements   
                                  
                               
                                Epi PhD   
                                      Admissions  
                                      Apply Now  
                                      Competencies  
                                      Curriculum  
                                      Handbook  
                                  
                               
                                HSP PhD   
                                      Admissions  
                                      Apply Now  
                                      Competencies  
                                      Curriculum  
                                      Handbook  
                                  
                               
                                SBS PhD   
                                      Admissions  
                                      Apply Now  
                                      Competencies  
                                      Curriculum  
                                      Handbook  
                                  
                               
                                Population Health Certificate  
                                Health Analytics Certificate  
                                Health Systems Leadership Certificate  
                                Interpreter Course   
                                      Interpreter Course Information  
                                      Interpreter Course Requirements  
                                      Interpreter Course Registration   Payment  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply Now  
                                
                            
                            
                                Contact Us  
                                
                            
                            
                                Support SPH  
                                
                            
                         
                      
                      
                      
                      
                        	
                         
                           		
                           
                           
                           
                           		
                            
                              
                               
                                 
                                  
                                     
                                    
                                  
                                 
                                  
                                    
                                  
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     #47 
                                    
                                     U.S. News and World Report 
                                    
                                     MHA - Health Care Management - Graduate 
                                    
                                  
                                 
                                  
                                    
                                     #23 
                                    
                                     AffordableColleges.com 
                                    
                                     Online Master's in Public Health 
                                    
                                  
                                 									 
                                  
                                    
                                     #21 
                                    
                                     AffordableCollegesOnline.org 
                                    
                                     Best Online Master's in Public Health 
                                    
                                  
                                 									
                                  
                                    
                                     #25 
                                    
                                      Best College Reviews 
                                    
                                     Best Onine Public Health Program 
                                    
                                  
                                 
                                  
                                    
                                     #9 
                                    
                                     BestMastersDegrees.com 
                                    
                                     Most Affordable Online Master's in Healthcare Administration 
                                    
                                  
                                 
                                 									 
                                  
                                    
                                     #27 
                                    
                                     TheBestSchools.org 
                                    
                                     Best Online Master's in Public Health 
                                    
                                  
                                 									 
                                  
                                    
                                     #12 
                                    
                                     TopMastersinHealthcare.com 
                                    
                                     Best Master's Program in Healthcare Administration 
                                    
                                  
                                 									
                                  
                                    
                                     #23 
                                    
                                     TopMastersinHealthcare.com 
                                    
                                     Most Affordable Online Master's in Public Health 
                                    
                                  
                                 
                               
                              
                            
                           	
                           
                         
                        
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/29/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


