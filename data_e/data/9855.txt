Manually initiating an evaluation | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Manually initiating an evaluation | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Competencies    »    Automating competency structure evaluation   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Manually initiating an evaluation                       
        
        
       
          
     
           Printer-friendly version       
	
 

 
	If your organization uses manual evaluation, a notice about an outdated competency or learning objective appears on the Results page of every element affected by the change. For example, if an activity’s Achievement threshold is changed, the notice will appear on the Results page of the activity and its parents, all the way up the structure to the competency.
 

 
	Manually initiate an evaluation
 

  
		On the Competency Home page, click on the competency or learning objective you want to re-evaluate.
		 
			 Note  If you want to re-evaluate results for an activity, click    Structure  on the Edit Learning Objective page, then click on the activity you want to evaluate.
		 
	 
	 
		Click    Results .
	 
	 
		Click  Re-evaluate .
	 
  
	Re-evaluation requests are placed in a queue and processed in sequence. Depending on the number of requests already in the queue, there might be a delay between the time of your request and when users are evaluated.
 
     Audience:    Instructor      

    
           

                   ‹ Disabling automatic evaluation 
        
                   up 
        
                   Monitoring the re-evaluation service › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Competencies  
  
      Competency structure basics    Automating competency structure evaluation    Understanding automatic evaluation    Initiating automatic evaluation    Disabling automatic evaluation    Manually initiating an evaluation    Monitoring the re-evaluation service      Creating and managing competency structure elements    Evaluating competency structure activities    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
