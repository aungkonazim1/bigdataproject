Associating grade items with course objects | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Associating grade items with course objects | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Grades    »    Managing grade items and grade book categories   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Associating grade items with course objects                       
        
        
       
        
     
              
	You can attach a grade item to individual course objects when editing their properties in the following tools:
 

  
		 Quizzes 
	 
	 
		 Dropbox 
	 
	 
		 Discussions 
	 
	 
		 Content  (only SCORM objects)
	 
  
	 Note  Completing a quiz included in a SCORM package automatically generates a grade item in the grades list, populated with the quiz results. To manually create a grade item for a SCORM package, see  Creating and editing a SCORM package .
 

 
	See also
 

  
		 Creating and editing a SCORM package 
	 
      Audience:     Instructor       

    
           

                   ‹ Restoring deleted grade items or categories 
        
                   up 
        
                   Associating grade items or categories with learning objectives › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Grades  
  
      Finding my grades    Creating grade book    Creating grade items and grade book categories    Managing grade items and grade book categories    Editing grade items or categories    Hiding and showing grade items in the grade book    Reordering grade items and categories    Setting availability for grade items or categories    Setting release conditions for grade items or categories    Deleting grade items or categories    Restoring deleted grade items or categories    Associating grade items with course objects    Associating grade items or categories with learning objectives      Managing grade schemes    Managing users  grades    Managing final grades    Changing Grades settings and display options    
                  
           
         

       
     

    
    
    
   
 
   
 
