Are you Ready for your Advising Appointment? - CAS - University of Memphis    










 
 
 
     



 
    
    
    Are you Ready for your Advising Appointment? - 
      	CAS
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			College of Arts   Sciences
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Areas of Study  
                          Research  
                          Scholarships  
                          Students  
                          Resources  
                          News  
                      
                      
                         
                            Advising Links   
                            
                               
                                  
                                   Advising Center Staff   Contact  
                                   Guide for Advisors  
                                   Newsletter for Advisors  
                                   Departments  
                                   Finding Your Advisor  
                                   First Year Students  
                                   Graduation/Degree Certification  
                                   Major and Minor Information  
                                   Major Four Year Plans  
                                   Meeting with your Advisor  
                                   Pre-Professional Advising  
                                   Upper Division Humanities  
                                   Upper Division Social Science  
                                   Withdrawal  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	CAS
                              	  
                          
                              	Advising
                              	  
                         Are you Ready for your Advising Appointment? 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Are You Ready for Your Advising Appointment? 
                     
                      When you meet with your advisor each semester to prepare for the next semester’s courses,
                        you need to be prepared so that your meeting will go well.
                      
                     
                      
                        
                         Schedule an appointment well in advance of your registration time. 
                        
                         Read the  Undergraduate Catalog  sections on your degree and your major requirements as well as the General Education
                           requirements.
                         
                        
                         Look over any materials your advisor has given you previously regarding your major
                           and degree, and bring them with you to every appointment.
                         
                        
                         Check your transcript in My Memphis so that you are prepared to ask about anything
                           that is unclear to you.
                         
                        
                          Print out a degree sheet  from the College of Arts and Sciences webpages and keep a record of the courses you
                           have completed.
                         
                        
                         Come prepared with a list of questions and any concerns that you would like to discuss. 
                        
                         Take a list of other concerns such as problems affecting your academic performance
                           or career considerations. Your advisor can supply you with contact information for
                           sources of help on campus, for example, tutoring or personal or career counseling.
                           Having a list of concerns will ensure that you do not forget to discuss them.
                         
                        
                         Bring a pen or pencil and a printed copy of the Registration Worksheet located on
                           the Advising website to record courses that you and your advisor discuss: ( http://www.memphis.edu/cas/pdfs/advisingplanningworksheet.pdf ).
                         
                        
                         If possible, have a rough schedule worked out with the courses you think will fulfill
                           your requirements
                         
                        
                         Ask your advisor for alternate courses that you can take in the event some courses
                           are closed or do not fit your schedule.
                         
                        
                         Inform your advisor of any changes in your plans since the last time you met- for
                           example, if you have decided to add a minor or a second major, or if you are planning
                           to go to graduate school or to professional school after graduation
                         
                        
                         Thank your advisor for spending time with you. 
                        
                      
                     
                      Your advisor is available to assist you with your college career, but ultimately it
                        is your responsibility to make appropriate academic choices. Being prepared for each
                        advising session will ensure that your choices are wise.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Advising Links 
                         
                            
                               
                                Advising Center Staff   Contact  
                                Guide for Advisors  
                                Newsletter for Advisors  
                                Departments  
                                Finding Your Advisor  
                                First Year Students  
                                Graduation/Degree Certification  
                                Major and Minor Information  
                                Major Four Year Plans  
                                Meeting with your Advisor  
                                Pre-Professional Advising  
                                Upper Division Humanities  
                                Upper Division Social Science  
                                Withdrawal  
                            
                         
                      
                      
                      
                         
                            
                                Apply now for an Undergraduate Degree  
                               Pursue your dream, broaden your career choices and gain the confidence you need to
                                 succeed
                               
                            
                            
                                Offering Master's and Doctoral Degrees  
                               Enhance your knowledge, skills and experience 
                            
                            
                                Community Engagement  
                               Teaching and serving the Memphis and Mid-South community through programs and events 
                            
                            
                                Contact Us  
                               Dean's Office Contacts, Advising, Department Chairs... 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


