Ida B. Wells Documentary - Ben Hooks - University of Memphis    










 
 
 
     



 
    
    
    Ida B. Wells Documentary - 
      	Ben Hooks
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			The Benjamin L. Hooks Institute for Social Change
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Programs  
                          Events  
                          HAAMI  
                          Documentaries  
                          Collections  
                          About  
                          Newsroom  
                      
                      
                         
                            Documentaries   
                            
                               
                                  
                                   Duty of the Hour  
                                   The Memphis 13  
                                   Freedom's Front Line  
                                   The Civil Rights Movement: A Cultural Revolution  
                                   Ida B. Wells  
                                   Support the Institute  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Ben Hooks
                              	  
                          
                              	Hooks Institute Documentaries
                              	  
                         Ida B. Wells Documentary 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Ida B. Wells Documentary 
                     
                      Overview 
                     
                       The Hooks Institute is producing its newest documentary film about the life of Ida
                        B. Wells (1862-1931), her experiences in Memphis, Tennessee, and her campaign against
                        the practice of lynching in the United States.
                      
                     
                      Our film explores the unique social, cultural, and political atmosphere of late 19th
                        century Memphis and how these conditions shaped and fueled the activism of Ida B.
                        Wells. The Hooks Institute is excited to tell a new generation the story of Wells,
                        a courageous and unyielding crusader for civil rights!
                      
                     
                      
                        
                          "The way to right wrongs is to turn the light of truth upon them"  Ida B. Wells-Barnett  
                        
                      
                     
                        Ida B. Wells and the Lynching at the Curve   (Documentary short) feature film coming in Spring 2018.
                      
                     
                       
                           
                              
                     
                      Documentary Summary 
                     
                       Born a slave in Holly Springs Mississippi, Ida B. Wells became an internationally
                           recognized advocate for the rights of African Americans and Women in American society.
                           In 1884, after moving to Memphis, Wells challenged one of the nation's first Jim Crow
                           laws when she refused to leave the "whites only" ladies car for the smoke filled "colored
                           car" on a train from the Poplar Station to northern Shelby County. Wells used the
                           event to begin her journalistic career, establishing herself as one of the most popular
                           figures in the national African American Press. Wells became the first female owner
                           of a Memphis-based newspaper, a publication titled The Free Speech. Facing both racial
                            prejudice , and blatant sexism, Wells honed her unforgiving rhetoric, leading the attack against
                           injustices in the Jim Crow south, and across the nation.  
                     
                        Memphis and the Lynching at the Curve | Hooks Institute Blog   
                     
                       In Memphis, Wells embarked on the most important mission of her young career. In 1892
                        three African American businessmen, Thomas Moss, Clint McDowell, and Will Stewart
                        were lynched in Memphis by a mob led by a white business competitor. Devastated, Wells
                        thereafter dedicated herself to discrediting the practice of lynching in the American
                        South. Wells' fearless and uncompromising fight against those who advocated or participated
                        in these bigoted attacks angered the white establishment of Memphis. In less than
                        a year, Wells was forced to flee Memphis in fear for her life. Living in exile, Wells
                        continued to fight against the practice of lynching through her writing and speeches
                        in both England and the United States. Many historians believe Wells' work helped
                        to discredit lynching in American society, making Wells one of the most important
                        figures in the fight for racial justice and basic human rights.
                      
                     
                      Sponsors 
                     
                      The Hooks Institute extends its gratitude to FedEx and Tennessee Humanities for seed
                        funding for this documentary.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Documentaries 
                         
                            
                               
                                Duty of the Hour  
                                The Memphis 13  
                                Freedom's Front Line  
                                The Civil Rights Movement: A Cultural Revolution  
                                Ida B. Wells  
                                Support the Institute  
                            
                         
                      
                      
                      
                         
                            
                                Make a gift to the Hooks Institute  
                               Join us as we work to uplift Memphis and uplift the nation. Make a gift of any amount
                                 to the Hooks Institute.
                               
                            
                            
                                Critical Conversations  
                               Where is your voice? A University-wide initiative to promote and sustain an inclusive
                                 and democratic society.
                               
                            
                            
                                Benjamin L. Hooks Papers Website  
                               Explore the Civil Rights Movement and beyond through the eyes of Benjamin L. Hooks. 
                            
                            
                                2017 Hooks Institute Policy Papers  
                               Read the 2017 Hooks Institute Policy Papers Online! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


