Teaching &amp; Learning - umTech - Information Technology Services - University of Memphis    










 
 
 
     



 
    
    
    Teaching   Learning  - 
      	umTech - Information Technology Services 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			umTech - Information Technology Services 
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Service  
                          Support  
                          Teaching  
                          Smart Tech  
                          Students  
                          Solutions  
                      
                      
                         
                            Teaching   Learning   
                            
                               
                                  
                                   Mission, Vision and Values  
                                   Teaching   Learning News  
                                   Events Calendar  
                                   Service Updates   Alerts  
                                   eCourseware and Online Support  
                                         eCourseware Overview  
                                         Student Resources  
                                         Faculty Resources  
                                         Training Videos   
                                     
                                  
                                   Teaching   Learning Tools  
                                         Accessibility  
                                         Clickers (iClicker REEF)  
                                         TurnItIn (Academic Integrity)  
                                         Respondus  
                                     
                                  
                                   umMedia - Media Services   
                                         Media Services Overview  
                                         BlueJeans  
                                         Ensemble Video  
                                         iTunes U  
                                         MediaSite  
                                         umMeet (Adobe Connect)  
                                     
                                  
                                   umServices  
                                         umServices Overview  
                                         umApps  
                                         umBlog  
                                         umDrive  
                                         umSurvey  
                                         umWiki  
                                     
                                  
                                   A to Z List  
                                   CTL Contact List  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	umTech - Information Technology Services 
                              	  
                         
                           	Teaching   Learning
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                        
                     
                      Get up to Speed on Technology. 
                     
                      The  Center for Teaching and Learning  provides face-to-face, online, and on campus technical support for the faculty and
                        staff. We also provide technical-driven support on enterprise services for students.
                        Our continuous commitment to student success is proven through our focus to provide
                        professional, customer-focused services.
                      
                     
                      
                     
                      Temporary Naming Convention in eCourseware 
                     
                        
                     
                      A temporary naming convention began in Summer 2017, the naming convention now contains
                        the course title, followed by the CRN (Course Registration Number), the year, and
                        the two-digit semester code. (i.e.: 17th-century French Literature - 95986.201780)
                        While Banner and eCourseware both use the same format for identifying UofM for-credit
                        courses, the naming format within eCourseware no longer reflects the course prefix
                        and number within the displayed name. For best results when searching for courses
                        within eCourseware, remember to use the CRN, year and two-digit semester code! If
                        you are an eCourseware DA (Departmental Aide) or a faculty member  follow this link for directions on changing your course names .
                      
                     
                      ITS Launching New Wireless Access Pilot 
                     
                      Information Technology Services (ITS) is launching a new wireless access network pilot
                        with an additional layer of security. Effective Aug. 14, uofm-secure will provide
                        wireless users enhanced security for passwords and protection of University data.
                        ITS welcomes your participation and cooperation in helping to keep both you and the
                        University more secure with improved encryption. Existing UofM and uofm-guest wireless
                        networks remain in place while the new uofm-secure network is tested. The need for
                        two older networks will be addressed during the fall semester.  To learn more visit our uofm-secure wireless page .
                      
                     
                      
                     
                       Meet with a Trainer Tuesdays  
                     
                      
                        
                         
                           
                            Meet with a Technology Trainer online every Tuesday from 8:00 am - 8:00 pm 
                            Chat with us!  
                        
                      
                     
                     
                     
                     	
                       
                
                
                   
                      
                         Teaching   Learning 
                         
                            
                               
                                Mission, Vision and Values  
                                Teaching   Learning News  
                                Events Calendar  
                                Service Updates   Alerts  
                                eCourseware and Online Support  
                                      eCourseware Overview  
                                      Student Resources  
                                      Faculty Resources  
                                      Training Videos   
                                  
                               
                                Teaching   Learning Tools  
                                      Accessibility  
                                      Clickers (iClicker REEF)  
                                      TurnItIn (Academic Integrity)  
                                      Respondus  
                                  
                               
                                umMedia - Media Services   
                                      Media Services Overview  
                                      BlueJeans  
                                      Ensemble Video  
                                      iTunes U  
                                      MediaSite  
                                      umMeet (Adobe Connect)  
                                  
                               
                                umServices  
                                      umServices Overview  
                                      umApps  
                                      umBlog  
                                      umDrive  
                                      umSurvey  
                                      umWiki  
                                  
                               
                                A to Z List  
                                CTL Contact List  
                            
                         
                      
                      
                      
                         
                            
                                Enter a Service Request  
                               Need Help with Technology? Request technical support via umHelpdesk, or search AskTom,
                                 a collection of questions and answers to technical problems.
                               
                            
                            
                                ITS Service Catalog  
                               Browse a collection of ITS services offered, learn how to access them and what Information
                                 is needed to fulfill service requests. 
                               
                            
                            
                                Preparing for Banner 9  
                               The UofM is upgrading to Banner 9. Keep up with the latest developments. 
                            
                            
                                Contact Us  
                               Contact the ITS Service Desk at 901.678.8888. (Available 24 hours a day, excluding
                                 posted holidays) Chat Live Monday - Friday 8:00 am - 7:00 pm CDT
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


