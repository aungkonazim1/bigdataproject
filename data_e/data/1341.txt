Prospective Parent E-newsletter - Parent &amp; Family Services - University of Memphis    










 
 
 
     



 
    
    
    Prospective Parent E-newsletter - 
      	Parent   Family Services
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Parent   Family Services
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Events   Services  
                          Get Involved  
                          News  
                          Resources  
                          Support Us  
                          Visit UofM  
                      
                      
                         
                            News   
                            
                               
                                  
                                   Parent   Family News  
                                   Prospective Parent E-news  
                                   Parent   Family Calendar  
                                   In the Now  
                                   Parent of the Year  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Parent   Family Services
                              	  
                          
                              	News
                              	  
                         Prospective Parent E-newsletter 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Prospective Parent E-Newsletter 
                     
                      For our parents of prospective students we have an e-news just for you! This e-news
                        is sent out 4-5 times a year. It includes dates, deadlines, updates, and specialized
                        information, including getting your student ready to go to college and how to help
                        them along the way.
                      
                     
                      
                        
                          Name     
                        
                          Email     
                        
                          Student's Name     
                        
                          Student's Date of Birth   
                              
                                
                              
                               Jan 
                              
                               Feb 
                              
                               Mar 
                              
                               Apr 
                              
                               May 
                              
                               Jun 
                              
                               Jul 
                              
                               Aug 
                              
                               Sep 
                              
                               Oct 
                              
                               Nov 
                              
                               Dec 
                                
                              
                                
                              
                               1 
                              
                               2 
                              
                               3 
                              
                               4 
                              
                               5 
                              
                               6 
                              
                               7 
                              
                               8 
                              
                               9 
                              
                               10 
                              
                               11 
                              
                               12 
                              
                               13 
                              
                               14 
                              
                               15 
                              
                               16 
                              
                               17 
                              
                               18 
                              
                               19 
                              
                               20 
                              
                               21 
                              
                               22 
                              
                               23 
                              
                               24 
                              
                               25 
                              
                               26 
                              
                               27 
                              
                               28 
                              
                               29 
                              
                               30 
                              
                               31 
                                
                              
                                
                              
                               1901 
                              
                               1902 
                              
                               1903 
                              
                               1904 
                              
                               1905 
                              
                               1906 
                              
                               1907 
                              
                               1908 
                              
                               1909 
                              
                               1910 
                              
                               1911 
                              
                               1912 
                              
                               1913 
                              
                               1914 
                              
                               1915 
                              
                               1916 
                              
                               1917 
                              
                               1918 
                              
                               1919 
                              
                               1920 
                              
                               1921 
                              
                               1922 
                              
                               1923 
                              
                               1924 
                              
                               1925 
                              
                               1926 
                              
                               1927 
                              
                               1928 
                              
                               1929 
                              
                               1930 
                              
                               1931 
                              
                               1932 
                              
                               1933 
                              
                               1934 
                              
                               1935 
                              
                               1936 
                              
                               1937 
                              
                               1938 
                              
                               1939 
                              
                               1940 
                              
                               1941 
                              
                               1942 
                              
                               1943 
                              
                               1944 
                              
                               1945 
                              
                               1946 
                              
                               1947 
                              
                               1948 
                              
                               1949 
                              
                               1950 
                              
                               1951 
                              
                               1952 
                              
                               1953 
                              
                               1954 
                              
                               1955 
                              
                               1956 
                              
                               1957 
                              
                               1958 
                              
                               1959 
                              
                               1960 
                              
                               1961 
                              
                               1962 
                              
                               1963 
                              
                               1964 
                              
                               1965 
                              
                               1966 
                              
                               1967 
                              
                               1968 
                              
                               1969 
                              
                               1970 
                              
                               1971 
                              
                               1972 
                              
                               1973 
                              
                               1974 
                              
                               1975 
                              
                               1976 
                              
                               1977 
                              
                               1978 
                              
                               1979 
                              
                               1980 
                              
                               1981 
                              
                               1982 
                              
                               1983 
                              
                               1984 
                              
                               1985 
                              
                               1986 
                              
                               1987 
                              
                               1988 
                              
                               1989 
                              
                               1990 
                              
                               1991 
                              
                               1992 
                              
                               1993 
                              
                               1994 
                              
                               1995 
                              
                               1996 
                              
                               1997 
                              
                               1998 
                              
                               1999 
                              
                               2000 
                              
                               2001 
                              
                               2002 
                              
                               2003 
                              
                               2004 
                              
                               2005 
                              
                               2006 
                              
                               2007 
                              
                               2008 
                              
                               2009 
                              
                               2010 
                              
                               2011 
                              
                               2012 
                              
                               2013 
                              
                               2014 
                              
                               2015 
                              
                               2016 
                              
                               2017 
                              
                               2018 
                              
                               2019 
                              
                               2020 
                              
                               2021 
                              
                               2022 
                              
                               2023 
                              
                               2024 
                              
                               2025 
                              
                               2026 
                              
                               2027 
                              
                               2028 
                              
                               2029 
                              
                               2030 
                              
                               2031 
                              
                               2032 
                              
                               2033 
                              
                               2034 
                              
                               2035 
                              
                               2036 
                              
                               2037 
                              
                               2038 
                              
                               2039 
                              
                               2040 
                              
                               2041 
                              
                               2042 
                              
                               2043 
                              
                               2044 
                              
                               2045 
                              
                               2046 
                              
                               2047 
                              
                               2048 
                              
                               2049 
                              
                               2050 
                              
                               2051 
                              
                               2052 
                              
                               2053 
                              
                               2054 
                              
                               2055 
                              
                               2056 
                              
                               2057 
                              
                               2058 
                              
                               2059 
                              
                               2060 
                              
                               2061 
                              
                               2062 
                              
                               2063 
                              
                               2064 
                              
                               2065 
                              
                               2066 
                              
                               2067 
                              
                               2068 
                              
                               2069 
                              
                               2070 
                              
                               2071 
                              
                               2072 
                              
                               2073 
                              
                               2074 
                              
                               2075 
                              
                               2076 
                              
                               2077 
                              
                               2078 
                                
                        
                          When will your student be attending UofM?   
                              
                                
                              
                               Spring 2018 
                              
                               Fall 2018 
                              
                               Spring 2019 
                              
                               Fall 2019 
                                
                        
                          Subscribe  
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         News 
                         
                            
                               
                                Parent   Family News  
                                Prospective Parent E-news  
                                Parent   Family Calendar  
                                In the Now  
                                Parent of the Year  
                            
                         
                      
                      
                      
                         
                            
                                Parent   Family Association  
                               Stay connected and engaged with us through the Parent   Family Association 
                            
                            
                                Parent   Family Handbook  
                               Learn about opportunities and services available to students 
                            
                            
                                In the Now  
                               What you need to know this month to help you help your student 
                            
                            
                                Contact Us  
                               Got questions, comments, or concerns?  We're here to help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/29/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


