Vendor’s Right to Protest - Procurement and Contract Services - University of Memphis    










 
 
 
     



 
    
    
    Vendor’s Right to Protest - 
      	Procurement and Contract Services
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Procurement and Contract Services
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Staff Directory  
                          Contract Services  
                          Tigerbuy and Bids  
                          Purchasing Card  
                          Travel Services  
                      
                      
                         
                            Tigerbuy   
                            
                               
                                  
                                   New Supplier/Vendor Registration  
                                   Bid Calendar  
                                   Current Bid Opportunities  
                                   Vendor Information  
                                   FAQs  
                                   Tigerbuy Training Classes/Registration  
                                   Tigerbuy Training Materials  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Procurement and Contract Services
                              	  
                          
                              	Tigerbuy
                              	  
                         Vendor’s Right to Protest 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Vendor’s Right to Protest 
                     
                      PROTESTED BIDS 
                     
                      
                        
                          Right to Protest  
                              
                               Any actual proposer who claims to be aggrieved in connection with a specific solicitation
                                 process may submit a protest in writing to the Director of Procurement and Contract
                                 Services within seven (7) calendar days after he or she knows or should have known
                                 the facts giving rise to the protest.   All proposers should know and shall be deemed responsible for knowing the facts documented
                                 in the University’s procurement files on the day the University opens the bid files
                                 for public inspection.   Any issues raised by the protesting party after the seven (7) calendar day period
                                 shall not be considered as part of the protest.
                               
                              
                                Signature on Protest Constitutes Certificate . The signature of an attorney or protesting party on a request for consideration,
                                 protest, motion, or other document constitutes a certificate by the signer that the
                                 signer has read such document, that to the best of the signer’s knowledge, information,
                                 and belief formed after reasonable inquiry, it is well grounded in fact and is warranted
                                 by existing law or a good faith argument for the extension, modification, or reversal
                                 of existing law, and that it is not interposed for any improper purpose, such as to
                                 harass, limit competition, or to cause unnecessary delay, or needless increase in
                                 the cost of the procurement or of the litigation. If a request for consideration,
                                 protest, pleading, motion, or other document is signed in violation of this subsection
                                 before or after appeal to the Chancellor, the Chancellor upon motion or upon his/her
                                 own initiative, may impose upon the person who signed it, a represented party, or
                                 both, an appropriate sanction, which may include an order to pay to the other party
                                 or parties, including the affected University, the amount of the reasonable expenses
                                 incurred because of the filing of the protest, a petition for a stay of award, pleading,
                                 motion, or other paper, including reasonable attorneys’ fees.
                               
                              
                               Neither a protest nor a stay of award shall proceed under this section unless the
                                 protesting party posts a protest bond (See Appendix I). The protesting party shall
                                 post, with the Director of Procurement and Contract Services of the University, at
                                 the time of filing a notice of protest, a bond payable to the University in the amount
                                 of five percent (5%) of the lowest cost proposal evaluated or five percent (5%) of
                                 the highest revenue proposal evaluated. Such protest bond shall be in form and substance
                                 acceptable to the University and shall be immediately payable to the University conditioned
                                 upon a decision by the Chancellor that:
                                  
                                    
                                     A request for consideration, protest, pleading, motion, or other document is signed,
                                       before or after appeal to the Chancellor, in violation of subsection A. (ii);
                                     
                                    
                                     The protest has been brought or pursued in bad faith; or 
                                    
                                     The protest does not state on its face a valid basis for protest. 
                                    
                                  
                               
                              
                               The University shall hold such protest bond for at least eleven (11) calendar days
                                 after the date of the final determination by the University. If the protesting party
                                 appeals the determination in accordance with subdivision (vii), the University shall
                                 hold such protest bond until instructed by the Chancellor to either keep the bond
                                 or return it to the protesting party.
                               
                              
                               At the time of filing notice of a protest of a procurement in which the lowest evaluated
                                 cost proposal is less than one million dollars ($1,000,000), or in which the highest
                                 evaluated revenue proposal is less than one hundred thousand dollars ($100,000), a
                                 minority or small business protesting party may submit a written petition to the Director
                                 of Procurement and Contract Services for exemption from the protest bond requirement
                                 of subsection A.(iii). Such a petition must include clear evidence of minority or
                                 small business status. On the day of receipt, the petition shall be given (may be
                                 faxed) to the Chancellor or designee. The Chancellor has five (5) business days in
                                 which to make a determination. If an exemption from the protest bond requirement is
                                 granted, the protest shall proceed as though the bond were posted. Should the Chancellor
                                 deny an exemption from the requirement, the protesting party shall post the bond with
                                 the Director of Procurement and Contract Services of the University as required in
                                 subsection A.(iii) within three (3) business days of the determination. For the purposes
                                 of this section, “minority business” is defined as solely owned or at least fifty-one
                                 percent (51%) owned by a person or persons who control the daily operation of such
                                 business and who is disabled (a person having a physical or mental impairment that
                                 in the written opinion of the person’s licensed physician, substantially limits one
                                 (1) or more of the major life activities of such person, including caring for oneself,
                                 and performing manual tasks, which include writing, walking, seeing, hearing, speaking,
                                 and breathing); African American (persons having origins in any of the Black racial
                                 groups of Africa); Asian American (persons having origins in any of the original peoples
                                 of the Far East, Southeast Asia and Asia, the subcontinent, or the Pacific Islands);
                                 Hispanic American (persons of Cuban, Mexican, Puerto Rican, Central or South American,
                                 or other Spanish or Portuguese origin, culture, or descent, regardless of race,);
                                 or Native American (persons having origins in any of the original peoples of North
                                 America). For purposes of this section, “small business” is defined as one which is
                                 independently owned and operated, has total gross receipts of no more than two million
                                 dollars ($2,000,000) for the most recently ended federal tax year, and employs no
                                 more than thirty (30) persons on a full-time basis.
                               
                              
                            
                         
                        
                          Authority to Resolve Protest  
                              
                               The Director of Procurement and Contract Services of the University has the authority
                                 to resolve the protest. If deemed necessary, the Director of Procurement Services
                                 may request a meeting with the protesting party to seek clarification of the protest
                                 issues.
                               
                              
                               The final determination of the Director of Procurement and Contract Services shall
                                 be given in writing and submitted to the protesting party.
                               
                              
                               The protesting party may request that the final determination of the Chief Procurement
                                 Officer be considered by the Vice President for Business and Finance of the University.
                                 The request for consideration shall be made in writing to the Vice President for Business
                                 and Finance within seven (7) calendar days from the date of the final determination
                                 by the Director of Procurement and Contract Services.
                               
                              
                               The Vice President for Business and Finance has the authority to review and resolve
                                 the protest. If deemed necessary, the Vice President for Business and Finance may
                                 request a meeting with the protesting party to seek clarification of the protest issues.
                                 The final determination of the Vice President for Business and Finance shall be given
                                 in writing and submitted to the protesting party.
                               
                              
                               The protesting party may request that the final determination of the Vice President
                                 for Business and Finance be considered by the President of the University. The request
                                 for consideration shall be made in writing to the President within seven (7) calendar
                                 days from the date of the final determination by the Vice President for Business and
                                 Finance.
                               
                              
                               The University shall have no longer than sixty (60) calendar days from receipt of
                                 the protest to resolve the protest.
                               
                              
                               The protesting party may request that the final determination of the President be
                                 considered by the Chancellor. The request for consideration shall be made in writing
                                 to the Chancellor within seven (7) calendar days from the date of the final determination
                                 by the President. The determination of the Chancellor or designee is final and shall
                                 be given in writing and submitted to the protestor.
                               
                              
                               In the event that the University fails to acknowledge receipt of a protest within
                                 fifteen (15) days of receipt of a protest or fails to resolve the protest within sixty
                                 (60) calendar days, the protesting party may request that the Chancellor consider
                                 the protest at a meeting.
                               
                              
                            
                         
                        
                          Stay of Award   Prior to the award of a contract, a proposer who has protested may submit to the
                           Vice President for Business and Finance a written petition for stay of award. Such
                           stay shall become effective upon receipt by the Vice President for Business and Finance.
                           The Director of Procurement and Contract Services shall not proceed further with the
                           solicitation process or the award of the contract until the protest has been resolved
                           in accordance with this section, unless the Chancellor makes a written determination
                           that continuation of the solicitation process or the award of the contract without
                           delay is necessary to protect substantial interests of the University. It shall be
                           the responsibility of the Vice President for Business and Finance to seek such determination
                           by the Chancellor.
                         
                        
                          Protest Subsequent to Award   The Tennessee Claims Commission has exclusive jurisdiction to determine all monetary
                           claims against the state for the negligent deprivation of statutory rights.
                         
                        
                          Appeal to Chancery Court   Protests appealed to the chancery court from the Chancellor’s decision shall be by
                           common law writ of certiorari.  The scope of review in the proceedings shall be limited
                           to the record made before the Chancellor and shall involve only an inquiry into whether
                           the Chancellor exceeded his/her jurisdiction, followed an unlawful procedure, or acted
                           illegally, fraudulently or arbitrarily without material evidence to support his/her
                           action.
                         
                        
                          Appendix I   A  protest bond  (PDF) may be presented to the University in form and substance compliant with the
                           Protest Bond format attached in Appendix I. Any protest bond presented to the University
                           that represents a deviation from the Appendix I format shall be considered for acceptability
                           by the University on a case by case basis.
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Tigerbuy 
                         
                            
                               
                                New Supplier/Vendor Registration  
                                Bid Calendar  
                                Current Bid Opportunities  
                                Vendor Information  
                                FAQs  
                                Tigerbuy Training Classes/Registration  
                                Tigerbuy Training Materials  
                            
                         
                      
                      
                      
                         
                            
                                Doing Business with the University of Memphis  
                               Information on how to do business with the U of M and our e-procurement and bidding
                                 system, Tigerbuy
                               
                            
                            
                                University of Memphis Contracts Report  
                               Contracts listing for the UofM 
                            
                            
                                Forms  
                               All of B F's forms in one place 
                            
                            
                                Business   Finance  
                               The Division of Business   Finance at the UofM 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


