Luvell Anderson - Philosophy - University of Memphis    










 
 
 
     



 
    
    
    Luvell Anderson - 
      	Philosophy
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Philosophy
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Undergraduate  
                          Graduate  
                          People  
                          Research  
                          News   Events  
                          Resources  
                          Contact  
                      
                      
                         
                            People   
                            
                               
                                  
                                   Core Faculty  
                                   Visiting Scholars  
                                   Instructors  
                                   Emeritus Faculty  
                                   Graduate Students  
                                   Staff  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Philosophy
                              	  
                          
                              	People
                              	  
                         Luvell Anderson 
                      
                   
                   
                       
                      
                     
                     	
                     
                     		  
                      
                        
                        
                         
                           
                           
                                                                
                              
                              
                               
                              
                              
                            
                           
                           
                            
                              
                               
                                 
                                 Luvell Anderson
                                 
                               
                              
                              
                               
                                 
                                 Assistant Professor
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Phone 
                                    
                                     
                                       
                                       901.678.2535
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Email 
                                    
                                     
                                       
                                       lndrsn14@memphis.edu
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Fax 
                                    
                                     
                                       
                                       901.678.4365
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Office 
                                    
                                     
                                       
                                       323 Clement Hall
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Office Hours 
                                    
                                     
                                       
                                       by appointment
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 										
                                 
                                 
                                   Website  
                                                                         
                                 
                               
                              
                            
                           
                           
                         
                        			 
                        			  
                        
                        
                         About Professor Anderson 
                        
                         Professor Anderson (PhD, Rutgers University) joined the department in 2012. Before
                           coming to Memphis, he was Alain Locke Postdoctoral Fellow at Pennsylvania State University.
                           His research lies principally in Philosophy of Language, Philosophy of Race, and Aesthetics.
                           He has published articles on the semantics of racial slurs and on racist humor. Professor
                           Anderson’s current writing projects include analyzing the linguistic underpinnings
                           of racial humor, investigating the ways power interacts with our interpretative practices,
                           and attempts to construct viable strategies for bridging certain racially-motivated
                           divides that foster miscommunication.
                         
                        
                         Publications 
                        
                         Edited Books 
                        
                         
                           
                            The Routledge Companion to Philosophy of Race, co-edited with Linda Martín Alcoff
                              and Paul Taylor (Forthcoming)
                            
                           
                         
                        
                         Peer-reviewed articles 
                        
                         
                           
                            "Hermeneutical Impasses." ​Philosophical Topics (forthcoming). 
                           
                            "Modeling Inclusive Pedagogy: Five Approaches" (with Verena Erlenbusch), (2017) Journal
                              of Social Philosophy 48(1): 6-19.
                            
                           
                            "Racist Humor," Philosophy Compass (2015), Philosophy Compass 10(8):501-509. 
                           
                            "What Did You Call Me? Slurs as Prohibited Words" (with Ernie Lepore) (2013), Analytic
                              Philosophy 54(3):350 – 363.
                            
                           
                            "Slurring Words"(with Ernie Lepore) (2013), Nous 47(1):25 – 48. 
                           
                         
                        
                         Book chapters 
                        
                         
                           
                            "Epistemic Injustice and the Philosophy of Race," (2017), in Kidd, I., Medina, J.,
                              and Pohlhaus, G. (eds.) Handbook on Epistemic Injustice. Routledge.
                            
                           
                            "Calling, Addressing, and Appropriation," (Forthcoming), in David Sosa (ed.) Bad Words.
                              Oxford University Press.
                            
                           
                            "When Reporting Others Backfires," (2016), in Capone, A., Kiefer, F., and Piparo,
                              F. Lo (eds.) Indirect Reports and Pragmatics. Springer.
                            
                           
                            "Notorious Thugs," (Forthcoming) in Black and Male: Critical Voices from Behind the
                              Racial Veil, ed. George Yancy.
                            
                           
                            "A Brief Essay on Slurs" (with Ernie Lepore) (2013), in Alessandro Capone (ed.) Perspectives
                              on Pragmatics and Philosophy. Springer.
                            
                           
                            "Language and Race" (with Sally Haslanger and Rae Langton) (2012), in Gillian Russell
                              and Delia Graff Fara (eds.), The Routledge Companion to the Philosophy of Language.
                              Routledge.
                            
                           
                         
                        
                         Book reviews 
                        
                         
                           
                            Review of Jennifer Saul Lying, Misleading, and What is Said: Explorations in the Philosophy
                              of Language and in Ethics, in Notre Dame Philosophical Reviews (2013).
                            
                           
                         
                        
                         Miscellaneous 
                        
                         
                           
                            "On Slurs: A Response" (with Ernie Lepore), (New York Times, December 24, 2010). 
                           
                         
                        
                         Upcoming talks 
                        
                         
                           
                            TBD, Colloquium, GAP.10, Cologne, Germany, Sept. 17-21, 2018. 
                           
                            TBD, Philosophy Colloquium, University of Virginia, April 13, 2018. 
                           
                            TBD, Philosophy Colloquium, University of Notre Dame, Mar 2, 2018. 
                           
                            TBD, Philosophy Colloquium, Mississippi State University, Feb 16, 2018. 
                           
                            TBD, Philosophy Colloquium, Duke University, Feb 9, 2018 
                           
                            TBD, Symposium, Rice University, Jan. 26-27, 2018. 
                           
                            "Peer Review, Underrepresented Scholarship, and (Questions of) Responsibility" 2018
                              Eastern APA panel, Savannah, GA, Jan. 3-6, 2018.
                            
                           
                            TBD, Philosophy Colloquium, Ohio State University, Dec 1, 2017 
                           
                            "Navigating Racial Satire," ASA Conference, New Orleans, Nov. 15-18, 2017. 
                           
                            TBD, "Who's Got the Power? Philosophical Critique of Social and Political Structures,"
                              Conference, Keynote speaker, University of Iceland, Reykjavík, Iceland, October 6-7,
                              2017.
                            
                           
                            "Black Humor, White Interpretation," Race, Art, and Aesthetics Conference, Oberlin
                              College, Sept. 29-30, 2017.
                            
                           
                            TBD, Philosophy Colloquium, St. Mary's College, South Bend, IN, Sept. 13, 2017. 
                           
                         
                        
                        
                      
                     								
                     
                     
                     
                     
                     	
                      
                   
                
                
                   
                      
                         People 
                         
                            
                               
                                Core Faculty  
                                Visiting Scholars  
                                Instructors  
                                Emeritus Faculty  
                                Graduate Students  
                                Staff  
                            
                         
                      
                      
                      
                         
                            
                                Apply to the Graduate Program  
                               Want to pursue a MA or PhD in Philosophy? Find out how to apply. 
                            
                            
                                Online B.A.  
                               Want to pursue your college degree and manage your busy lifestyle? Experience online
                                 classes at the UofM.
                               
                            
                            
                                Pre-law Advising  
                               Are you interested in a career in the legal profession? Consider a degree in Philosophy! 
                            
                            
                                Support the Department  
                               Find out how you can support the Department of Philosophy. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


