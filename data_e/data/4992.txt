WORKFORUM: University of Memphis Career Opportunities   
 
   
       
       WORKFORUM: University of Memphis Career Opportunities 
 
 

 
 
 
 
 
 
 
 
 

     

   
   
       
       
         
           Skip to Main Content 
         
         
           
             
   
     
       
         
           
         
       
       
         
          University of Memphis Career Opportunities  
       
     
   
 

           
           
             
   
     
       
         Toggle navigation 
          
          
          
       
     
     
       
    Home  
    Search Jobs  
      Create Account  
      Log In  
    Help  
 

       
             
               University Benefits 
             
             
               Equal Employment 
             
             
               Annual Security Report 
             
             
               Campus Map 
             
             
               About UofM 
             
       
     
   
 

             
               
                
                 
  Search Postings  (125) 
     
 
 View all open Postings below, or enter search criteria to narrow your search. 
    
   
   
     
       
         Keywords 
       
       
         
       
         
           Posted Within 
         
         
            Any time period 
 Last Day 
 Last Week 
 Last Month  
         
     
     
     
   
 

   
   
       
           
             Department 
           
           
             
                Academic Administration 
 Academic Affairs Finance 
 Academic Counseling Center 
 Academic Innovation   Support Services 
 Academic Technology Services 
 Accounting Office 
 Admissions 
 Adult and Commuter Student Services  
 Adult Services 
 Advanced Learning Center 
 Advancement Services 
 African   African American Studies 
 Air Force ROTC 
 Alumni and Constituent Relations 
 Alumni Programs and Special Events 
 Animal Care Facility 
 Anthropology 
 Architecture 
 Army ROTC 
 Art 
 Art Museum 
 Assoc Dean Education 
 Assoc VP Information Technology 
 Athletic Academic Services 
 Athletic Administration 
 Athletic Business Office 
 Athletic Committee 
 Athletic Communications 
 Athletic Director 
 Athletic External Affairs 
 Athletic Management Finance 
 Athletic Ticket Office 
 Auxiliary Services 
 AVP Admin Business Services 
 AVP Campus Planning Design 
 AVP Finance 
 AVP Physical Plant 
 AVP Student Affairs 
 Avron B. Fogelman Professional Development Center 
 Baseball 
 Basketball Men 
 Basketball Women 
 Benefits 
 Bioinformatics 
 Biology 
 Biomedical Engineering 
 Budget 
 Bureau Business Economic Research 
 Bursar 
 Business and Finance 
 Business Development 
 Business Information   Technology 
 Campus Recreation 
 Campus School 
 Career Services 
 Center for Academic Retention and Enrichment Services 
 Center for Community Health 
 Center for Counsel Learn Testing 
 Center for Research on Women (CROW) 
 Center Higher Education 
 Center Rehab Employment Research 
 Center Research Education Policy 
 CERI 
 Chemistry 
 Child Care Center 
 Chucalissa Museum 
 City   Regional Planning 
 Civil Engineering 
 COE Communicative Impaired 
 College of Arts and Sciences  
 College of Business Economics 
 College of Communication and Fine Arts 
 College of Education 
 College of Engineering 
 Communication 
 Communication Sciences and Disorders 
 Community Music School 
 Computer Science 
 Computer Systems Support 
 Conference and Event Services 
 Continuing Education 
 Counsel Education Psych Research 
 Criminal Justice 
 CRISTAL 
 Curriculum and Assessments 
 Dean of Students 
 Disability Resources for Students 
 Distance Education 
 Diversity Office 
 Earth Sciences 
 Economics 
 Educational Support Program 
 Electrical Computer Engineering 
 Engineering Technology 
 English 
 Enrollment Services 
 Enterprise Application Services 
 Enterprise Infrastructure Services 
 Environmental Health and Safety 
 Epidemiology   Biostatistics 
 Executive Assistant Student Affairs 
 Extended Programs 
 External Relations 
 Faculty Senate 
 Family and Consumer Sciences 
 FCBE Academic Administration 
 FCBE Academic Program 
 Fedex Institute of Technology 
 Feinstone Center 
 Finance Insurance Real Estate 
 Financial Aid Operations 
 Financial Planning 
 Finish Line Program 
 First Scholars 
 Food Services 
 Football 
 General Accounting 
 General Human Resources 
 General Payroll 
 General Plant 
 Golf Men 
 Golf Women 
 Government Affairs 
 Graduate Programs 
 Health Administration 
 Health Center 
 Health Sciences 
 Health Sport Sciences 
 History 
 Honors Program 
 Hooks Inst for Social Change 
 Hospitality and Resort Management 
 Hotel Conference Center 
 Human Resources 
 Innovation in Teaching   Learning 
 Institute for Intelligent Systems 
 Institutional Equity 
 Institutional Research 
 Instruction Curriculum Leadership 
 Integrated Microscopy Center 
 Intensive English for Internatls 
 Interdisciplinary Studies 
 Intermodal Freight Transportation Institute 
 Internal Audit 
 International Programs 
 International Studies 
 International Trade Center 
 Investment Banking Agreements 
 IT Planning and Budget 
 IT Security, ID Mgmt and Compliance 
 ITS Desktop and Smart Tech Support 
 ITS Service Desk 
 ITS Support Teaching and Learning 
 Journalism 
 Judaic Studies 
 Judicial Affairs 
 Keep TN Beautiful 
 Lambuth 
 Law Library 
 Leadership 
 Lipman School 
 Loewenberg College of Nursing 
 Management 
 Management Information Systems 
 Marketing and Communications 
 Marketing Supply Management 
 Mathematics 
 Mechanical Engineering 
 Multicultural Affairs 
 Navy ROTC 
 Network Services 
 Office for Institutional Equity 
 Office of President 
 Office of Student Conduct 
 Office of University Development 
 Olympic Sports 
 One Stop Shop 
 Parking 
 Payroll Office 
 Philosophy 
 Physical Plant 
 Physics and Materials Science 
 Police Services 
 Political Science 
 PP Business Operations 
 PP Crafts Trades 
 PP Custodial 
 PP Landscape 
 Principal Gifts 
 Procurement and Contract Services 
 Provost Office 
 Psychology 
 Public Administration 
 Regional Economic Development Ctr 
 Registrar Office 
 Residence Life 
 Revenue and Waivers 
 Rifle 
 RODP Admin 
 Scholarships 
 School of Accountancy 
 School of Health Studies 
 School of Law 
 School of Music 
 School of Public Health 
 School of Urban Affairs 
 Shared Services Center 
 Soccer Men 
 Soccer Women 
 Social   Behavioral Sciences 
 Social Work 
 Sociology 
 Softball 
 Space Planning 
 Sport   Leisure Management 
 Staff Senate 
 Stores Inventory 
 Student Financial Aid 
 Student Leadership 
 Student Success Programs 
 Teacher Education 
 Tennis Men 
 Tennis Women 
 Testing Center 
 Theatre and Dance  
 Tiger Copy and Graphics 
 Track Men 
 Track Women 
 Undergraduate Programs 
 University Admin Applications Svcs 
 University Center 
 University College 
 University College Programs 
 University College RODP 
 University Counsel 
 University District Support 
 University Libraries 
 University of Memphis Foundation 
 Various 
 Vice President Research 
 Vice Provost Assessment, IR, Report 
 Vice Provost Extended Programs 
 Volleyball 
 VP Advancement 
 VP Business and Finance 
 VP Communications, PR, Marketing 
 VP Information Technology 
 VP Research 
 VP Student Affairs 
 Weight Room Operations 
 Women and Gender Studies 
 Women Athletics and Compliance 
 World Languages   Literatures  
             
           
           
             Category 
           
           
             
                Staff (Hourly/Monthly) 
 Full-Time Faculty 
 Part-Time Faculty 
 Administrative Executive 
 Academic Executive 
 Athletic Contract 
 Temporary Employees  
             
           
       
       
           
             Campus Location 
           
           
             
                Main Campus (Memphis, TN) 
 Chucalissa Museum   Indian Village (Memphis, TN) 
 Collierville Campus (Collierville, TN) 
 Lambuth Campus (Jackson, TN) 
 Meeman Biological Field Station (Millington, TN) 
 Millington Center (Millington, TN) 
 Park Avenue Campus (Memphis, TN) 
 School of Law (Downtown Memphis) 
 May vary by position  
             
           
       
   
 

   
   
   Previous    1   2   3   4   5    Next   
 View Results  (125)  
  
 
   
       
   
     
       Closing Date ↑
     
     
       Category 
     
     
       Department 
     
     
       Hiring Range 
     
 


 
       
   
     
       
         
             Part-Time Faculty Pool, University College 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            University College
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Interdisciplinary Studies 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Interdisciplinary Studies
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Teacher Candidate Supervisor 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Teacher Education
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, TN eCampus   Distance Education 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Distance Education
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Dance 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Theatre and Dance 
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Business Information and Technology 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Business Information   Technology
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Foreign Languages (Japanese) 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            World Languages   Literatures
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, History 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            History
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Foreign Languages (Hebrew) 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            World Languages   Literatures
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Public Administration 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Public Administration
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Health Sport Sciences 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Health Sport Sciences
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Substitute Teacher Pool, Campus School 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Campus School
         
         
            Typically $85.00 per day
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Sociology 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Sociology
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, School of Music 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            School of Music
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Theatre 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Theatre and Dance 
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Foreign Languages (Classics/Latin) 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            World Languages   Literatures
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Earth Sciences 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Earth Sciences
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Communication 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Communication
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Psychology 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Psychology
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Criminal Justice 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Criminal Justice
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Biology 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Biology
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Computer Science 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Computer Science
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, International Studies 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            International Studies
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, African American Studies 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            African   African American Studies
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Political Science 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Political Science
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Social Work 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Social Work
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Finance, Insurance,   Real Estate 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Finance Insurance Real Estate
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Health Sport Sciences (Exercise Sport Sciences) 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Health Sport Sciences
         
         
            Salary will be commensurate with qualifications.  The position is semester-to-semester only, based on need.  The anticipated date of initial employment is August 21, 2017.
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Economics 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Economics
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Foreign Languages (Arabic) 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            World Languages   Literatures
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   

 

   Previous    1   2   3   4   5    Next   

                  
               
                
             
           
           
             
   
     University of Memphis — Human Resources 
165 Administration Building, Memphis, TN 38152 
901.678.3573  workforce@memphis.edu  
 Follow us on Twitter  
      
      ©University of Memphis. All Rights Reserved. The University of Memphis is an Equal Opportunity/Affirmative Action university.  
   
 

           
         
       
         
 
        
  

  


    
      
    
     
   
 
