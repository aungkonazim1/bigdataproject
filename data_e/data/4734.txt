Japanese Minor - WLL - University of Memphis    










 
 
 
     



 
    
    
    Japanese Minor  - 
      	WLL
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of World Languages and Literatures
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          Languages  
                          People  
                          Study Abroad  
                          Scholarships  
                      
                      
                         
                            Japanese Program   
                            
                               
                                   About  
                                   Degree Program  
                                        
                                         Courses Offered  
                                        
                                         Major  
                                        
                                         Minor  
                                        
                                         IMBA  
                                        
                                         Study Abroad  
                                     
                                  
                                   News   Events  
                                         News  
                                         Upcoming Events  
                                         Past Events  
                                     
                                  
                                   Faculty and Staff  
                                   Resources/Study Tools  
                                   Facebook  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	WLL
                              	  
                          
                              	Japanese Program
                              	  
                         Japanese Minor  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Japanese Minor 
                     
                       Course numbers and descriptions are the same as for Japanese Major; however, for a
                           Japanese Minor, students must take eighteen hours (JAPN1020, JAPN2010, JAPN2020 and
                           nine upper-division hours). After completion of JAPN2020, it is highly recommended
                           that students take JAPN3301 and 3302 as these courses are prerequisites for other
                           upper-division courses. Students may take JAPN3301 or 3302 and another upper-division
                           course at the same time.    To declare Japanese Minor, please go to Scates Room 107.  
                     
                        JAPN 1020 - Elementary Japanese II (3)    (1102). Further development of basic skills fundamental to language proficiency and
                           culture. PREREQUISITE: JAPN 1010, or equivalent.  
                     
                        JAPN 2010 - Intermediate Japanese I (3)    (2201). Continued development of listening, speaking, reading and writing skills.
                           PREREQUISITE: JAPN 1020, or equivalent.  
                     
                        JAPN 2020 - Intermediate Japanese II (3)    (2202). Expansion of Japanese skills with more extensive listening comprehension practice
                           and readings. PREREQUISITE: JAPN 2010, or equivalent.  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Japanese Program 
                         
                            
                                About  
                                Degree Program  
                                     
                                      Courses Offered  
                                     
                                      Major  
                                     
                                      Minor  
                                     
                                      IMBA  
                                     
                                      Study Abroad  
                                  
                               
                                News   Events  
                                      News  
                                      Upcoming Events  
                                      Past Events  
                                  
                               
                                Faculty and Staff  
                                Resources/Study Tools  
                                Facebook  
                            
                         
                      
                      
                      
                         
                            
                                Events  
                               Including Language Fair, Film Festivals, Lectures, and other activities 
                            
                            
                                Student Resources  
                               Language Media Center, Language Tables, Placement Testing and Tutoring 
                            
                            
                                Newsletter  
                               Get the News from World Languages and Literatures 
                            
                            
                                Contact Us!  
                               Email, location and hours for the main office. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


