Carlos Estrano - Biological Sciences - University of Memphis    










 
 
 
     



 
    
    
    Carlos Estrano - 
      	Biological Sciences
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Biological Sciences
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Undergraduate  
                          Graduate  
                          Centers  
                          People  
                          Research  
                          Events  
                          Scholarships  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Biological Sciences
                              	  
                          
                              	People
                              	  
                         Carlos Estrano 
                      
                   
                   
                       
                      
                     
                     	
                     
                     		  
                      
                        
                        
                         
                           
                           
                                                                
                              
                              
                               
                              
                              
                            
                           
                           
                            
                              
                               
                                 
                                 Carlos E. Estraño, Ph.D.
                                 
                               
                              
                              
                               
                                 
                                 Associate Professor
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Phone 
                                    
                                     
                                       
                                       901.678.2245
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Email 
                                    
                                     
                                       
                                        cestrano@memphis.edu 
                                        
                                    
                                  
                                 
                                  
                                    
                                     Fax 
                                    
                                     
                                       
                                       901.678.4457
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Office 
                                    
                                     
                                       
                                       Life Sciences 409B
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Office Hours 
                                    
                                     
                                       
                                       By Appointment
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 										
                                 
                                 
                                  
                                    
                                      Website  
                                    
                                  
                                                                         
                                 
                               
                              
                            
                           
                           
                         
                        			 
                        			  
                        
                        
                          About Dr. Estrano  
                        
                         Dr. Estrano joined the Department in 2003. During my postdocs I worked on gene expression
                           regulation and protein trafficking in two major parasites: Trypanosoma brucei causative
                           agent of Sleeping Sickness in the African and the malaria parasite Plasmodium falciparum.
                           After joining the University of Memphis I continue my research in promoter regulation
                           in P. falciparum. My current focus is studying the molecular mechanisms underlying
                           the intestinal disturbances in Giardia lamblia infections. The intestinal disease
                           giardiasis is one of the most common parasitic infections worldwide affecting approximately
                           2.5 million humans every year, and has been included in the list of WHO Neglected
                           diseases since 2004. Giardiasis in humans is caused by Giardia lamblia belonging to
                           the Genus Giardia a group of binucleate, flagellated protozoan parasites which lack
                           common eukaryotic structures such as peroxisomes, a traditional Golgi apparatus and
                           typical mitochondria. In addition, I am also studying the bases for symptoms variability
                           and severity associated with this infection. Moreover, using an overexpression system
                           my laboratory is cloning and expressing human mitochondria and parasite genes in the
                           Giardia to understand the mechanism of mitosomal, a mitochondrial remnant, protein
                           import system. I am currently an Associate professor teaching Molecular Biology of
                           Parasites and Vectors in the spring semester with a lecture and Lab component. This
                           course familiarizes students with the diversity of tropical parasites that affect
                           humans and animals of human interest. This is a 4000/6000 level course. In the fall
                           semester I teach a graduate course, Molecular mechanisms of protein sorting and targeting.
                           This course focus on current theories of protein trafficking. I review the different
                           signal and machinery for correct targeting of proteins. Virology, a 4000/6000 course,
                           is also offer in the fall semester. In this course I focus primarily in the diversity
                           of human viruses, differences between DNA and RNA viruses, structure, mechanism of
                           infection and pathogenesis.
                         
                        
                         Research Interests 
                        
                          Cross-regulation of cellular function at the host-Giardia interface  Protein trafficking and import to the Giardia's mitosome.  Mechanims of signal transduction and protein regulation in Giardia lamblia
                         
                        
                         Education 
                        
                         B.S. degree in Cellular Biology, Universidad Central of Venezuela; PhD in Molecular
                           Biology, the Universidad Autonoma of Madrid, Spain; Postdoctoral associate at Yale
                           University; Visiting Scientist and postdoctoral fellowship at Northwestern University
                           Chicago, IL 2000-2003.
                         
                        
                         Recent Publications 
                        
                         • Nyindodo-Ogari L, Schwartzbach SD, Estraño CE. 2014. "Giardia mitosomal protein
                           import machinery differentially recognizes mitochondrial targeting signals". Infect
                           Disord Drug Targets. 14;14(1):23-9.
                         
                        
                         • Fisher BS, Estraño CE, Cole JA. 2013. "Modeling long-term host cell-Giardia lamblia
                           interactions in an in vitro co-culture system". PLoS One. 2013 Dec 3;8(12):e81104.
                         
                        
                         • López-Estraño C. 2010. "Comparative analysis of stage specific gene regulation of
                           apicomplexan parasites: Plasmodium falciparum and Toxoplasma gondii".Editorial. Infect
                           Disord Drug.
                         
                        
                         • Gopalakrishnan AM, López-Estraño C. 2010. "Comparative analysis of stage specific
                           gene regulation of apicomplexan parasites: Plasmodium falciparum and Toxoplasma gondii.
                           Infect Disord Drug Targets. 2010 Aug;10(4):303-11. Review.
                         
                        
                         • Gopalakrishnan AM, López-Estraño C. 2010. "Role of cis-regulatory elements on the
                           ring-specific hrp3 promoter in the human parasite Plasmodium falciparum. Parasitol
                           Res. Mar;106(4):833-45.
                         
                        
                         • Gopalakrishnan AM, Nyindodo LA, Ross Fergus M, López-Estraño C.2009 "Plasmodium
                           falciparum: Preinitiation complex occupancy of active and inactive promoters during
                           erythrocytic stage. Exp Parasitol. 2009 Jan;121(1):46-54
                         
                        
                         .• Adris P, Lopez-Estraño C, Chung KT. "The metabolic activation of 2-aminofluorine,
                           4-aminobiphenyl, and benzidine by cytochrome P-450-107S1 of Pseudomonas aeruginosa".
                           Toxicol In Vitro. 2007 Dec;21(8):1663-71.
                         
                        
                         • López-Estraño C, Gopalakrishnan AM, Semblat JP, Fergus MR, Mazier D, Haldar K. 2007.
                           "An enhancer-like region regulates hrp3 promoter stage-specific gene expression in
                           the human malaria parasite Plasmodium falciparum". Biochim Biophys Acta. 1769(7-8):506-13.
                         
                        
                         • López-Estraño C, Semblat JP, Gopalakrishnan AM, Turner L, Mazier D, Haldar K. 2007.
                           "Plasmodium falciparum: hrp3 promoter region is associated with stage-specificity
                           and episomal recombination".Exp Parasitol. 2007 Jul;116(3):327-33.
                         
                        
                         • Hiller NL, Bhattacharjee S, van Ooij C, Liolios K, Harrison T, Lopez-Estraño C,
                           Haldar K. 2004. "A host-targeting signal in virulence proteins reveals a secretome
                           in malarial infection". Science. 10;306(5703):1934-7.
                         
                        
                        
                      
                     								
                     
                     
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Biology@Memphis Newsletter  
                               Read about graduate and undergraduate student news; awards, grants and scholarships;
                                 publications, and more...
                               
                            
                            
                                Administration  
                               Budgets, accounting, facilities management, committees, meetings, and group email
                                 accounts
                               
                            
                            
                                Facilities  
                               Not your ordinary classrooms... 
                            
                            
                                Contact Us  
                               Main office contacts, faculty and staff administration 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/28/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


