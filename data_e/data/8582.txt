Deleting a conversation in Wiggio | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Deleting a conversation in Wiggio | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Integrations    »    Wiggio    »    Wiggio basics   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Deleting a conversation in Wiggio                       
        
        
       
          
     
           Printer-friendly version       
	 Note  Group administrators can delete any conversation in Wiggio. Group members can only delete their own conversations.
 

 
	Delete a conversation in Wiggio
 

  
		In the Feed area, click the    Delete this post  icon.
	 
	 Click  Delete . 
  
	 Tip  You may need to hover your mouse over the conversation to make the 
	   Delete this post  icon appear. 
	     Audience:    Learner      

    
           

                   ‹ Replying to a conversation in Wiggio 
        
                   up 
        
                   Filtering and unfiltering group feeds in Wiggio › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Wiggio  
  
      Wiggio basics    Accessing Wiggio    Wiggio browser support    Using Wiggio without belonging to a group    Wiggio terminology    Starting a conversation in Wiggio    Replying to a conversation in Wiggio    Deleting a conversation in Wiggio    Filtering and unfiltering group feeds in Wiggio    Updating your profile in Wiggio    Adding contacts to Wiggio    Communicating privately with contacts in Wiggio    Deleting contacts in Wiggio    Wiggio notifications      Creating and using groups in Wiggio    Managing groups in Wiggio     Adding resources to Wiggio    Creating and using folders in Wiggio    Using virtual meetings in Wiggio    Using polls in Wiggio    Using to-do lists in Wiggio    Managing events in Wiggio    Importing and exporting calendars in Wiggio    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
