Civil War - Mississippi Delta - LibGuides at University of Memphis Libraries   
 
	 
         
         
         
        
		 Civil War - Mississippi Delta  - LibGuides at University of Memphis Libraries 
		 
		 
 
 
 
 
 
 
 
 
 
 
 

		 
		 
		 
		 
		 
		 
         
         
				 
				 
             
         
        
        
            
		
		 
		
		 
		 
		
		 
		
		
	   
		         
        	 
     
	 
		 Skip to main content                  
                 
         
  		 
             
                
			 
				
					 
						 University of Memphis Libraries
						 
					 
					 
						 LibGuides
						 
					 
					 
						 Mississippi Delta 
						 
					 
					 Civil War
					 
			              
			 
				 
					
                 
                     
                         
                             
                             Search this Guide  
                             
                                 Search 
                             
                         
                     
                 				 
							 
             
                 Mississippi Delta  
                 
                     This guide covers the Mississippi river Delta region. 
                 
             
         
         
          
         
             
                 
                     
                         
                             
                 
                     
                         Welcome 
                        
                     
                 
                 
                     
                         Delta Symposium at the U of M 
                        
                     
                 
                 
                     
                         Agriculture 
                        
                     
                 
                 
                     
                         Native Americans 
                        
                     
                 
                 
                     
                         Civil War 
                        
                     
                 
                 
                     
                         Religion 
                        
                     
                 
                 
                     
                         Civil Rights 
                        
                     
                 
                 
                     
                         Music 
                        
                     
                 
                 
                     
                         Literature 
                        
                     
                 
                 
                     
                         African American History 
                        
                     
                 
                 
                     
                         Art 
                        
                     
                 
                 
                     
                         Arkansas Delta 
                        
                     
                  
                            
                 
                     
                         
                        
                         
                     
                                          
                     
                 
                 
                     
                         
                             
     
                  
					 
						 
							 Featured Books @ U of M
                                 
							 
								 
									 
                          
                        
			 
				 
						 
					 
				 
					 
					 War upon the Land
					   by   Lisa M. Brady
					   Call Number: Special Collections  ISBN: 9780820329857  Publication Date: 2012-04-01  
			  
                        
                             
                             
			 
				
		     
                          
                        
			 
				 
						 
					 
				 
					 
					 Freedom's Women
					   by   Noralee Frankel
					   Call Number: Special Collections  ISBN: 0253334950  Publication Date: 1999-10-22  
			  
                         
                        
			 
				 
						 
					 
				 
					 
					 Champion Hill
					   by   Timothy B. Smith; Terrence J. Winschel (Foreword by)
					   ISBN: 1932714197  Publication Date: 2006-01-01  
			  
                         
                        
			 
				 
						 
					 
				 
					 
					 Vicksburg
					   by   Timothy T. Isbell (Photographer)
					   ISBN: 9781578068401  Publication Date: 2006-04-21  
			  
                         
                        
			 
				 
						 
					 
				 
					 
					 Civil War Mississippi
					   by   Michael B. Ballard
					   ISBN: 1578061962  Publication Date: 2000-03-14  
			  
                        
                             
                             
								 
								
							 
						 
					   
					 
						 
							 Government Publications @ U of M
                                 
							 
								 
									
			 
				       
     Vicksburg   : official   map   and guide:  Vicksburg   National Military Park, Louisiana/Mississippi     
 National Park Service, U.S. Department  of  the Interior 
 
 
 
  Call Number: I 29.21:V 66/2/999    
        Subject:     Vicksburg  National Military Park (Miss.)  
 
  Military parks -- Mississippi.  
   Vicksburg  (Miss.) -- History -- Civil War, 1861-1865  
 
     
 
 
 
		    
								 
								
							 
						 
					   
					 
						 
							  New York Time's American Civil War RSS Feed 
                                 
							 
								 
									 
                            
                
                 
                     
                 
                        
								 
								
							 
						 
					   
					 
						 
							 The Union Siege of Vicksburg from  The History Channel 
                                 
							 
								 
									
			 
				    
  http://www.history.com/videos/the-union-siege-of-vicksburg#the-union-siege-of-vicksburg  
		    
								 
								
							 
						 
					   
					 
						 
							 Civil War Facts from  PBS 
                                 
							 
								 
									
			 
				 
 from  http://www.pbs.org/civilwar/war/facts.html  
     More than three million men fought in the war.    Two percent of the population more than 620,000 died in it.    In two days at  Shiloh  on the banks of the Tennessee River, more Americans fell than in all previous American wars combined.    During the  Battle of Antietam , 12,401 Union men were killed, missing or wounded; double the casualties of D-Day, 82 years later. With a total of 23,000 casualties on both sides, it was the bloodiest single day of the Civil War.    Missouri sent 39 regiments to fight in the siege of  Vicksburg : 17 to the Confederacy and 22 to the Union.    At the start of the war, the value of all manufactured goods produced in all the Confederate states added up to less than one-fourth of those produced in New York State alone.    In March 1862, European powers watched in worried fascination as the Monitor and Merrimack battled off Hampton Roads, Va. From then on, after these ironclads opened fire, every other navy on earth was obsolete.    In 1862, the U.S. Congress authorized the first paper currency, called "greenbacks."    On July 4, 1863, after 48 days of siege, Confederate General John C. Pemberton surrendered the city of  Vicksburg  to the Union s General,  Ulysses S. Grant . The Fourth of July was not be celebrated in Vicksburg for another 81 years.    Disease was the chief killer during the war, taking two men for every one who died of battle wounds.    African Americans constituted less than one percent of the northern population, yet by the war s end made up ten percent of the Union Army. A total of 180,000 black men, more than 85% of those eligible, enlisted.    In November 1863, President Lincoln was invited to offer a "few appropriate remarks" at the opening of a new Union cemetery at Gettysburg. The main speaker, a celebrated orator from Massachusetts, spoke for nearly two hours. Lincoln offered just 269 words in his Gettysburg Address.    In 1864,  Ulysses S. Grant  was promoted to Lieutenant General, a rank previously held by General George Washington, and led the 533,000 men of the Union Army, the largest in the world. Three years later, he was made President of the United States.    Andersonville Prison in southwest Georgia held 33,000 prisoners in 1864. It was the fifth largest city in the Confederacy.   By the end of the war, Unionists from every state except South Carolina had sent regiments to fight for the North.    On November 9, 1863, President Lincoln attended a theater in Washington, D.C., to see "The Marble Heart." An accomplished actor,  John Wilkes Booth , was in the cast.    On May 13, 1865, a month after Lee s surrender at  Appomattox , Private John J. Williams of the 34th Indiana became the last man killed in the Civil War, in a battle at Palmito Ranch, Texas. The final skirmish was a Confederate victory.    Hiram Revels of Mississippi became the first black man ever elected to the U.S. Senate. He filled the seat last held by  Jefferson Davis .  
 
		    
								 
								
							 
						 
					   
					 
						 
							  Library of Congress' Voices of the Civil War 
                                 
							 
								 
									 
                            
                
                 
                     
                 
                        
								 
								
							 
						 
					   
					 
						 
							 The Civil War Day-by-Day
                                 
							 
								 
									 
                            
                
                 
                     
                 
                        
								 
								
							 
						 
					   
					 
						 
							 Interesting Links
                                 
							 
								 
									
			 
				     
 
    The Civil War in the Lower     Mississippi River Valley    
 
     
  Vicksburg National Military Park  
 
            
   American Civil War   
 
     
  The Civil War, PBS  
		    
								 
								
							 
						 
					  
         
     
                          
                     
                    
                 
                     
                         Previous:  Native Americans 
                     
                     
                      Next:  Religion    
                     
                                  
             
         
         
         
             
                 
                     
                         Last Updated:   May 19, 2017 9:29 AM                      
                     
                         URL:   http://libguides.memphis.edu/mississippi_delta                      
                     
                    	    Print Page                      	
                     
                 
                     Login to LibApps                  
             
             
                 
                     Report a problem  
                 
                 
                                     
                 
                    
                     Tags:  
                      american indian ,  blues ,  cherokee ,  chickasaw ,  choctaw ,  chucalissa ,  civil rights ,  civil war ,  cotton ,  delta ,  help guide ,  mississippi river ,  native american ,  religion  
                                     
             
         
         
        
			 
				 
					 
					    
					    
					 
				 
			          
                              
                                
                 
                

		 
  	 
 
