Chemistry 6001 - Department of Chemistry - University of Memphis    










 
 
 
     



 
    
    
    Chemistry 6001 - 
      	Department of Chemistry
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Chemistry
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          People  
                          Research  
                          Resources  
                      
                      
                         
                            Programs   
                            
                               
                                  
                                   Undergraduate  
                                         Chemistry Major (B. S.) - Chemistry Concentration  
                                         Chemistry Major (B. S.) - Biochemistry Concentration  
                                         Chemistry Major (ACS Certified B. S.)  
                                         Chemistry Minor  
                                         Courses and Information  
                                         Advising  
                                         Permit Requests  
                                         Scholarships   Awards  
                                         Research Opportunities  
                                         Undergraduate Research Conference  
                                         NSF REU Program  
                                         Student Members of ACS (SMACS)  
                                         Career Options  
                                     
                                  
                                   Graduate  
                                         Ph. D. Chemistry  
                                         Master of Science in Chemistry  
                                         Courses  
                                         Advising  
                                         Assistantships  
                                         Grad Student Association  
                                         Apply!  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of Chemistry
                              	  
                          
                              	Programs
                              	  
                         Chemistry 6001 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Environmental Chemistry--CHEM 4001/6001 
                     
                        
                     
                       Students to be Served:  Senior undergraduates and first year graduate students majoring in chemistry or a
                        closely related discipline.
                      
                     
                       Prerequisite:   CHEM 3311 .
                      
                     
                       Description:  Chemical phenomena occurring in soil, atmospheric, and aquatic environments; consideration
                        of natural resources and energy.  (Three lecture hours per week, 3 credit hours)
                      
                     
                       Textbooks and other materials:  
                     
                      
                        
                         Gary W. van Loon and Stephen J. Duffy, “Environmental Chemistry, a Global Perspective”,
                           Oxford University Press (2000).
                         
                        
                      
                     
                       Course Objectives:  The purpose of this three-credit course is to learn the chemical processes going on
                        in our environment. Environmental Chemistry is a rather broad and interdisciplinary
                        subject. The coverage of this course will include, but is not limited to, the chemical
                        species in air, water, and soil environments with regards to their sources, reactions,
                        transport, effects, and fates.
                      
                     
                       Course Outline:  
                     
                      
                        
                         The Earth’s atmosphere 
                        
                         Stratosphere chemistry – ozone 
                        
                         Troposphere chemistry – smog 
                        
                         Troposphere chemistry – precipitation 
                        
                         Atmospheric aerosols 
                        
                         Indoor air pollution 
                        
                         The hydrosphere 
                        
                         Distribution of species in aquatic systems 
                        
                         Gases and organic matter in water 
                        
                         Metals in the hydrosphere 
                        
                         Environmental chemistry of colloids and surfaces 
                        
                         Waste water treatment 
                        
                         Energy 
                        
                         Currently concerned environmental problems (in chemistry) etc 
                        
                      
                     
                       Grades:  There will be two hour-exams and a comprehensive final exam. Some home-work problems
                        will be assigned and graded during the semester. Each student is required to give
                        an oral presentation to the class on a pre-approved topic. CHEM 6001 students will
                        write a term paper on a pre-approved topic.
                      
                     
                       Policies:  
                     
                      
                        
                         Books and journals may be used to help with graded homework assignments, but assistance
                           may not be sought from other students or faculty.
                         
                        
                         Tardiness and unexcused absence from class are unprofessional behaviors which should
                           be avoided, but attendance will not be graded.
                         
                        
                         Electronic devices such as cell phones and pagers should be turned off in the classroom. 
                        
                         Reasonable and appropriate accommodations will be made for students who present a
                           memo from Student Disability Services.
                         
                        
                      
                      Instructor Homepages    Dr. Richard Petersen     On-line Resources   None currently
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Programs 
                         
                            
                               
                                Undergraduate  
                                      Chemistry Major (B. S.) - Chemistry Concentration  
                                      Chemistry Major (B. S.) - Biochemistry Concentration  
                                      Chemistry Major (ACS Certified B. S.)  
                                      Chemistry Minor  
                                      Courses and Information  
                                      Advising  
                                      Permit Requests  
                                      Scholarships   Awards  
                                      Research Opportunities  
                                      Undergraduate Research Conference  
                                      NSF REU Program  
                                      Student Members of ACS (SMACS)  
                                      Career Options  
                                  
                               
                                Graduate  
                                      Ph. D. Chemistry  
                                      Master of Science in Chemistry  
                                      Courses  
                                      Advising  
                                      Assistantships  
                                      Grad Student Association  
                                      Apply!  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply to the Department of Chemistry  
                               Now accepting applications for graduate school 
                            
                            
                                Contact us!  
                               Call us (901.678.2621) or stop by the Departmental office at Smith Chemistry Rm 210 
                            
                            
                                Seminar Schedule  
                               Click here for the current seminar schedule 
                            
                            
                                What's new?  
                               Click here to see what's new in our Department 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


