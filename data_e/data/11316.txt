The Profile of Dipankar Dasgupta   
 
 The Profile of Dipankar Dasgupta 
 
 




 
 



 


 

 
 Dr. Dipankar Dasgupta 
 

 
 * Research Sites * 
 
 
  Artificial Immune Systems (AIS)  
  Center for Information Assurance (CfIA)  
 
 
            


 
 
 
Dr. Dipankar Dasgupta 
333 Dunn Hall 
Memphis, TN 38152-3240 
phone: (901) 678-4147 
fax: (901) 678-1506 
 dasgupta@memphis.edu 
 
 
 
  Dr. Pat E. Burlison  Professor of   Computer Science  
 Director,  Center for Information Assurance  
 Director,  Intelligent Security Systems Research Laboratory  
 


       
  Elevated to IEEE Fellow(Batch of 2015) 
   Distinguished ACM Speaker  
  Recipient of 2012 Willard R. Sparks Eminent Faculty Award.  
 
 
  Advisory Board Member of  MIT in Cyber Security   
  Editorial Board of journals  


 

 Evolutionary Intelligence, Springer-Verlag 

 Evolutionary Optimization, Polish Academy of Science.  

 Recent Patents on Computer Science, online journal Bentham Science Publishers Ltd.  

 Swarm and Evolutionary Computing - Elsevier Press 
 
 
  Announcement:  
 
  Research Assistant Professor Position  (in Cyber Security) Available
  
 
 

 
 * Principal Investigator * 
 
 
  Act Online  
 
 
      


 

 
 
  Home  
  Events 
	 
	  News  
	  Professional Activities  
	  Invited Talks  
	 
 
  Research 
	 
	  Interests 
		 
		  Artificial Immune Systems  
		  Evolutionary Computation  
		  Immunological Computation  
		  Intrusion Detection  
		  Fault Detection  
		 
	 
	  Projects 
		 		
		  Current Projects  
                  Previous Projects  
		 
	 
	
	  Grants  
	  Publications  
 
	 
 
  Teaching 
	 
	  Courses  
	 
 

  Staff/Students  
  Books 
         
          AUA Book  
          IC Book  
          AIS Book  
          EC Book  
         
 
  Recognitions 
         
          Awards  
          Videos  
	 
 
  About  
  
 

 
 
Dr. Dasgupta will Organize IEEE Symposium on  Computational Intelligence in Cyber Security (CICS 2017)  at Hawaii, USA from November 27-December 1, 2017.
Program Committee Member of the 1st IEEE International Workshop on  Cyber Resiliency Economics (CRE 2016)  , Vienna, Austria, August 1-3, 2016.
Prof. Dasgupta will give an invited talk at the  Computer Science Department, University of Tennessee, Knoxville, TN, April 7, 2016
   
Prof. Dasgupta will present a research paper at 11th Annual Cyber and Information Security Research  (CISR)  Conference will be held at the conference center at Oak Ridge National Laboratory, Oak Ridge, TN, April 4 - 6, 2016.
   
Prof. Dasgupta will give invited talk at  Regional Symposium  "Graduate Education and Research in Information Security",'GERIS'16, on  March 8, 2016, at Binghamton University,Binghamton, New York.
   
Announcement for the available position in  Research Assitant Professor  (in Cyber Security)
   
Prof. Dasgupta was interviewed by a local TV Channel (FOX 13) and telecast on Feb. 19, 2016.  Click here for Video. 
   
Organized  "Cybersecurity Certificate Course"  foundational program at FedEx Institute of Technology,UofM, February 1-5, 2016.
   
Prof. Dasgupta gave an invited talk on  5th International Conference on Fuzzy and Neural Computing , FANCCO-2015, December 16-19, 2015.
   
Cluster to Advance Cyber Security & Testing (CAST)  hosted  Cybersecurity Lightning Talks  at the FedEx Institute of Technology, afternoon of December 3, 2015
   
CfIA Receives Cyber Security Training Grant from FEMA
   
UofM's CfIA Will Develop Course for  Mobile Device Security and Privacy Issues 
   
Prof. Dasgupta gave an invited talk on  Adaptive Multi-Factor Authentication  at the  Department of Electrical Engineering and Computer Science and CASE Center, Syracuse University, Syracuse, NY 13224-5040 November 18, 2015
   
Organize a Symposium on Computational Intelligence in Cyber Security (CICS) at IEEE Symposium Series on Computational Intelligence ( SSCI, ), December 7-10, 2015 at Cap Town, South Africa
   
Gave keynote speech at St. Louis at  Cyber Security workshop (STL-CyberCon) , University of Missouri-St. Louis, November 20, 2015
   
Prof. Dasgupta attended the  NIST-NICE  conference at San Diego from November 1-4, 2015
   
Prof. Dasgupta gave an invited talk at 9th  International Research Workshop  on Advances and Innovations in Systems Testing at FedEx Institute of Technology, the University of Memphis, October 20, 2015
   
Our   Cyber Security Team  got a   second position   on Cyber Defense Competition  @CANSec 2015 , held on 24th October at University of Arkansas at Little Rock
  
 

 


 Dr. Dipankar Dasgupta's research interests are mainly in the area of scientific computing and tracking real-world problems through interdisciplinary cooperation. His areas of special interests include:

 

  Cyber Security  

  Artificial Immune Systems  

  Evolutionary Computation  

 

He published more than 250 research papers in book chapters, journals, and international conferences. He authored a book, published two edited volumes and co-edited several conference proceedings over the last 20 years. A search for "Dipankar Dasgupta" on Google Scholar shows a total count: 14,000+ citations. To get the current listing from Google Scholar   click here  . For more information about the publications     click here    . 

 Dipankar Dasgupta s Scholar Indices:

         

          Google Scholar Entry   (h-index: 55) 

          Microsoft Academic Entry  

        	  Scopus Entry  

         

  



His name is listed among the top computer scientists whose h-index is above 50 (available at   UCLA site  ), thus, influencing the research community. One of his current researches is applying Computational Intelligence techniques in Network and Internet Security. For more information, please     click here    .  

Professor Dr. Dasgupta is the recipient of 2014  ACM SIGEVO  Impact Award. 

His work on "Password Immunizer"  (based on  Negative Authentication System ) was submitted for patent. A demo illustrating the concept is available   here  . 

Current projects can be viewed under under    Project  tab and   click here    to view his work on his previous projects.

Prof. Dasgupta s list of collaborators are available from ..  Microsoft Academic Search  



 His published books include: 

                                                                        

If you are interested in any of the above mentioned research areas, contact the  Computer Science Department  for graduate programs.  

 



 

 
 






 
 
