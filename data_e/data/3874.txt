January 2015 &#8211; On Legal Grounds | Memphis Law Events &amp; Announcements    
 

 
	 
	 
	 
	 
	
	 January 2015   On Legal Grounds | Memphis Law Events   Announcements 
 

 
 
 
 
		
		
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 










 
 
  
 
 
    

 

 

 

	 
	
				
		 
			 
				
				 
					     
					 On Legal Grounds | Memphis Law Events   Announcements 									 
				
									 
						    
						   
						    Student Organizations 
 
	  AWA  
	  BLSA  
	  Bus.   Tax Law Society  
	  Christian Legal Society  
	  Federalist Society  
	  Health Law Society  
	  HLSA  
	  Honor Council  
	  ILS  
	  Law Review  
	  Memphis Law +1  
	  Moot Court Board  
	  OutLaw  
	  PAD  
	  PALS  
	  SELS  
	  SBA  
	  SGA  
	  Street Law  
	  TACDL  
 
 
  Law School Announcements 
 
	  Academic Affairs 
	 
		  Academic Affairs Calendar  
	 
 
	  Academic Success Program 
	 
		  Academic Success Program Calendar  
	 
 
	  Career Services Office 
	 
		  Career Services Office Calendar  
	 
 
	  Information Technology 
	 
		  Information Technology Calendar  
	 
 
	  Law Admissions, Recruiting,   Scholarships 
	 
		  Law Admissions, Recruiting, and Scholarships Calendar  
	 
 
	  Law School Registrar 
	 
		  Law School Registrar Calendar  
	 
 
	  Library 
	 
		  Library Calendar  
	 
 
	  Office of the Dean 
	 
		  Office of the Dean Calendar  
	 
 
	  Office of Diversity 
	 
		  Office of Diversity Calendar  
	 
 
	  Pro Bono Office 
	 
		  Pro Bono Office Calendar  
	 
 
	  Student Affairs 
	 
		  Student Affairs Calendar  
	 
 
 
 
  Blog Information  
  Writing Competitions  
  Events  
  
					  
								
			  
		  
		
	  
	
	 
		 			
			 
				 
 

	 

			   Monthly Archive:  January 2015  
			
	
  	
	 		
		
				
				
			 
				 				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Career Services Office  
			 30 Jan, 2015 
		  
		
		 
			 Day in the Life: Family Law is on Wednesday   Lunch provided 
		  
		
				 				
			 Come hear three successful family law attorneys candidly discuss their typical days on Wednesday, February 4th from 12:00   12:50 p.m.    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Law School Announcements  
			 30 Jan, 2015 
		  
		
		 
			 REMINDER   ROOTS Memphis   Farm to Office CSA at the law school 
		  
		
				 				
			 In an effort to offer our students, faculty and staff easier access to fresh vegetables and other produce, a local organization    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Career Services Office  
			 29 Jan, 2015 
		  
		
		 
			 1Ls:  Apply now for an on-campus interview with Butler Snow LLP 
		  
		
				 				
			 1Ls: Butler Snow is coming on-campus on February 18th to hire summer law clerks.  You must apply through Symplicity s OCI tab by    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Library  
			 29 Jan, 2015 
		  
		
		 
			 Password change for Westlaw 
		  
		
				 				
			 Starting the week of February 8th, you will be required to update your OnePass password. To save time, you can be    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Academic Affairs  
			 28 Jan, 2015 
		  
		
		 
			 Study Abroad Information 
		  
		
				 				
			 A number of law schools offer study abroad opportunities for law students.  If you are interested in studying abroad, please visit    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  PALS  /  Pro Bono Office  /  Student Organizations  
			 28 Jan, 2015 
		  
		
		 
			 Alternative Spring Break Application Deadline 
		  
		
				 				
			 The 2015 Alternative Spring Break applications are due this Friday, January 30, by 5 p.m.  Get on it guys, especially the 3Ls-who-wish-to-graduate-soon-but-have-not-fulfilled-their-pro-bono-hours-requirement (you know    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Student Affairs  
			 28 Jan, 2015 
		  
		
		 
			 Street Law Interest Meeting 
		  
		
				 				
			 The Street Law chapter is hosting an informational meeting this Thursday, January 29, 2015, at noon in room 244  Street Law is a    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Law School Registrar  
			 28 Jan, 2015 
		  
		
		 
			 REMINDER: May 2015 Graduates   Program Proof 
		  
		
				 				
			 Please review the Law School program draft to confirm that your information is correct.  If changes are necessary, please email Cheryl    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Library  
			 28 Jan, 2015 
		  
		
		 
			 Reminder Lunch and Learn: LexisNexis  Help for Writing That Document 
		  
		
				 				
			 Need to write a paper this semester?  a note or comment for law review?  a brief?  Just need a research refresher?     
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Student Organizations  
			 28 Jan, 2015 
		  
		
		 
			 Tennessee Association of Criminal Defense Lawyers Meeting 
		  
		
				 				
			 TACDL will be having a short beginning of the semester meeting Thursday 1/29 to discuss plans for the rest of the year.  Anyone interested    
		  
				
	  	
  					   			  
		
			 
			 
 Page 1 of 6  1  2  3  4  5  ...     Last   
 	  
			
				
	  
	
  


	 
		
		    
		
		 
			
						 
				 Follow: 
							 
						
						
						
			   Law School Website                     		 		 Recent Posts 		 
					 
				 1L Minority Clerkship Program   Reception (Chattanooga) 
						 
					 
				 New resources! 
						 
					 
				 Community Legal Center Volunteer Opportunity 
						 
					 
				 Law School Bookstore 2 Day Sale   Dec. 6th and 7th! 
						 
					 
				 St. Jude Memphis Marathon/Downtown Street Closures 
						 
				 
		 		  Archives 		 
			  December 2017  
	  November 2017  
	  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
		 
		   Categories 		 
	  Academic Affairs 
 
	  Academic Success Program 
 
	  AWA 
 
	  BLSA 
 
	  Career Services Office 
 
	  Christian Legal Society 
 
	  Experiential Learning 
 
	  Federal Bar Association 
 
	  Federalist Society 
 
	  Health Law Society 
 
	  HLSA 
 
	  Honor Council 
 
	  ILS 
 
	  Information Technology 
 
	  Law Admissions, Recruiting,   Scholarships 
 
	  Law Review 
 
	  Law School Announcements 
 
	  Law School Registrar 
 
	  Library 
 
	  Memphis Law +1 
 
	  Mock Trial 
 
	  Moot Court Board 
 
	  National Lawyer s Guild 
 
	  Office of Diversity 
 
	  Office of the Dean 
 
	  OutLaw 
 
	  Outside Organizations 
 
	  PAD 
 
	  PALS 
 
	  Pro Bono Office 
 
	  SBA 
 
	  Sports   Entertainment Law Society 
 
	  Street Law 
 
	  Student Affairs 
 
	  Student Organizations 
 
	  Tennessee Association of Criminal Defense Lawyers 
 
	  Writing Center 
 
	  Writing Competitions 
 
		 
 			
		  
		
	  

	
 
	
	    
	
	 
		
				 
			 More 
		 
				
				
		  Search   
	 
		 
	 
    Login 					 
					 
										 
					 
					 
					  Username:  
					  
					  Password:  
					  
										  
					  Remember me  
															   
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
											  Don't have an account?  
										  Lost your password?  
					 
					
					 
					
										 
				
					 
					  Choose username:  
					  
					  Your Email:  
					  
										   
					 
					 
					 
					 
					 
					 
					 
					 
					   Have an account?   
					 
										
					 
			
					 
					  Enter your username or email:  
					  
										   
					 
					 
					 
					 
					 
					 
					 
					 
					   Back to login   
					 
					
					 
					
										  
					   
					 
					   Subscribe 	
	
 
	 
		 
		 
		 		
		
	 
		
		 
					  

		
 
		 Email Address *  
	 
  
 
		 First Name 
	 
  
 
		 Last Name 
	 
  
 
		 Middle Name 
	 
  			 
				* = required field			  
			
		 
			 
		  
	
	
				
	  
	  
  
	    Blog Feedback                     		
	  
	
  	

				  
			  			
		  
	  

	 
		
				
				
				
		 
			 
				
				    
				
				 
					
					 
						
												
						 
															 On Legal Grounds | Memphis Law Events   Announcements   2017. All Rights Reserved. 
													  
						
												 
							 Powered by  WordPress . Theme by  Alx . 
						  
												
					 
					
					 	
											 
				
				  
				
			  
		  
		
	  

  

 		
		










 
 
 