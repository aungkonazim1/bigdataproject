Financial Aid Disbursement Information - Bursar's Office - University of Memphis    










 
 
 
     



 
    
    
    Financial Aid Disbursement Information - 
      	Bursar's Office
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Bursar's Office
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Staff Directory  
                          Fees  
                          Calendars  
                          Students  
                          Parents  
                          Campus Card  
                          Bursar Areas  
                      
                      
                         
                            Other Student Info   
                            
                               
                                  
                                   eRefund Sign-up  
                                   Financial Aid Disbursement  
                                   Bookstore Advance Payment Plan (BAPP)  
                                   Student Financial Appeals  
                                   1098-T Tax Information  
                                   Tutorials and Video Presentations  
                                   TN STRONG Act  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Bursar's Office
                              	  
                          
                              	Other Student Info
                              	  
                         Financial Aid Disbursement Information 
                      
                   
                   
                       
                      
                     		
                     
                     
                      Financial Aid Disbursement Information 
                     
                      Your financial aid award(s) must actually be completed and credited to your account
                        (not estimated awards), and you must have a credit balance in your account after fees
                        have been satisfied for the semester before receiving a refund of excess aid.
                      
                     
                      Refund by e-Refunds (Direct Deposit) 
                     
                      Excess financial aid/scholarships for Fall 2017 will be direct deposited to your bank
                        account beginning August 25, 2017 (Law School - August 11, 2017) and daily thereafter
                        through the end of the semester for financial aid/scholarship awards that are disbursed
                        after the initial eRefund noted above.
                      
                     
                      It is important that you sign up for eRefund on TigerXpress by August 21, 2017 (Law
                        School - August 7, 2017).
                      
                     
                      If you receive financial aid/scholarship award amounts in excess of fees, you need
                        to  sign up online  on TigerXpress to have your refund deposited directly to your bank account. Note
                        that direct deposit authorization submitted to the Payroll Office is separate from
                        direct deposit authorization for financial aid/scholarship disbursement.
                      
                     
                      Refund by Check 
                     
                      If you do NOT sign up for eRefund, check refunds will be mailed beginning September
                        1, 2017 (Law School - August 18, 2017), and every Friday thereafter through the end
                        of the semester for financial aid/scholarship awards that are disbursed after the
                        initial mailing noted above.
                      
                     
                       Download the Fall 2017 Refund by Check Schedule  
                     
                       Download the Summer 2017 Refund by Check Schedule  
                     
                       NOTE:  For students who do not receive their refund checks in a timely manner, a stop payment
                        (initiated through the Bursar's Office) cannot be processed until 2 weeks from the
                        mail date of the check. The stop payment, re-issuance, and mailing of the replacement
                        check takes approximately 10 days.  It is recommended that you sign up for direct deposit to avoid this possible 3-4 week
                           delay.  
                     
                      eRefunds are the fastest way to receive your excess financial aid/scholarship funds! 
                     
                       Financial Aid Disbursement Information for Students with Outstanding Debt  
                     
                      
                        
                         Federal regulations require authorization from the student to use Title IV funds (i.e.
                           federal financial aid) to pay prior term charges and current non-educational charges
                           (e.g. parking tickets, library fines, etc.) within the same award year as the Title
                           IV funds. Without your authorization, your federal aid will pay only current tuition,
                           fees, Dining Dollars and dorm room charges.  Federal regulations prohibit using current Title IV funds to pay any prior award/academic
                              year outstanding charges. Your account will remain on hold until you pay all past
                              due charges owed to The University of Memphis.  
                        
                         Access and complete the  Title IV Authorization Form  online and submit to the Financial Aid Office, 103 Wilder Tower. Failure to do so
                           will require payment in full of any outstanding charges and may result in a delay
                           of processing financial aid excess refunds and/or deletion of classes. In addition
                           to the Title IV Authorization Form, a repayment agreement may be required to be completed
                           with the Bursar's Office.
                         
                        
                         If above conditions are satisfied and/or if receiving non Title IV funds to pay outstanding
                           charges, registration holds will be released beginning August 1 for Fall terms, December
                           1 for Spring terms, and May 1 for Summer terms. Grades/Transcript holds will not be
                           released until account balance is paid in full.
                         
                        
                         State Law prohibits the release of a registration hold for any student who owes debt
                           to the University.
                         
                        
                      
                     
                       NOTE:  First Time Borrowers must have completed an entrance interview and master promissory
                        note in order for your funds to be disbursed to your student account and any excess
                        to be refunded to you. For additional information, please contact the  Financial Aid Office .
                      
                     
                     	
                      
                   
                
                
                   
                      
                         Other Student Info 
                         
                            
                               
                                eRefund Sign-up  
                                Financial Aid Disbursement  
                                Bookstore Advance Payment Plan (BAPP)  
                                Student Financial Appeals  
                                1098-T Tax Information  
                                Tutorials and Video Presentations  
                                TN STRONG Act  
                            
                         
                      
                      
                      
                         
                            
                                TigerXpress  
                               View your statements and pay fees online in TigerXpress. 
                            
                            
                                Tuition Estimator  
                               How much will your courses cost? Get an estimate online! 
                            
                            
                                Forms  
                               All of B F's forms in one place 
                            
                            
                                Business   Finance  
                               The Division of Business   Finance at the U of M 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


