Michael Duke - Department of Anthropology - University of Memphis    










 
 
 
     



 
    
    
    Michael Duke - 
      	Department of Anthropology
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Anthropology
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Undergraduate  
                          Graduate  
                          People  
                          Research  
                          Resources  
                          News   Events  
                      
                      
                         
                            People in the Department of Anthropology   
                            
                               
                                  
                                   Faculty   Staff  
                                   Emeriti Faculty  
                                   Faculty in Other Departments  
                                   Part-time Instructors  
                                   Student Directory  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of Anthropology
                              	  
                          
                              	People
                              	  
                         Michael Duke 
                      
                   
                   
                       
                      
                     
                     	
                     
                     		  
                      
                        
                        
                         
                           
                           
                                                                
                              
                              
                               
                              
                              
                            
                           
                           
                            
                              
                               
                                 
                                 Michael Duke
                                 
                               
                              
                              
                               
                                 
                                 Associate Professor
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Phone 
                                    
                                     
                                       
                                       901.609.8857 
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Email 
                                    											
                                     
                                       
                                       m.duke@memphis.edu
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Fax 
                                    
                                     
                                       
                                       901.678.2069
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Office 
                                    
                                     
                                       
                                       319 Manning Hall
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Office Hours 
                                    
                                     
                                       
                                       Call for Hours
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 										
                                 
                                 
                                   CV  
                                                                         
                                 
                               
                              
                            
                           
                           
                         
                        			 
                        			  
                        
                        
                         About Dr. Duke 
                        
                         Michael Duke is a Social/Medical Anthropologist (University of Texas at Austin 1996)
                           with over 20 years of experience carrying out social research focusing on the intersection
                           of labor, substance abuse, migration, gender and masculinity, sexuality, and mental
                           health, particularly among Latin American and Pacific Islander immigant populations.
                           He is also a nationally recognized expert on qualitative and mixed method research,
                           particularly with hard-to-reach populations, and has written and lectured extensively
                           on this topic.
                         
                        
                         Prior to coming to the University of Memphis, Dr. Duke served as Principal Investigator
                           or Co-Investigator on multiple studies, including an investigation of mental health
                           treatment barriers for Puerto Rican adolescents and a large, National Institute of
                           Health-funded study of heavy drinking and sexual risk among New England-based farmworkers.
                           This was followed by an investigation on the physical and mental health effects of
                           migration on a rural sending community in Mexico. He also served as Co-Investigator
                           of a study on syringe sharing and HIV risk among injection drug users in China's Guangdong
                           Province.
                         
                        
                         Dr. Duke's subsequent research focused on heavy drinking and their associated problems
                           among blue collar populations, specifically construction workers, restaurant workers,
                           and military personnel. More recently, he served as Principal Investigator on two
                           additional studies; one focusing on problem drinking and partner violence among farmworkers
                           in San Diego County, CA, and the other on stress and alcohol use among day laborers.
                         
                        
                         Dr. Duke joined the faculty of the University of Memphis in 2011. He is the founding
                           member and Chair of the Anthropology and Mental Interest Group (AMHIG) of the Society
                           for Medical Anthropology.
                         
                        
                         Expertise and Interests 
                        Drug and  alcohol use; migration; gender and masculinity; labor; Latin American and Caribbean
                           populations; mental health; Micronesian populations in the US; migration; research
                           methods; sexual comportment; social theory.  
                        
                         
                           
                            
                              
                               
                                 
                                  
                                    
                                     Research Projects 
                                    
                                     Dr. Duke's primary project consists of a community-based participatory research study
                                       on the physical and mental health needs of Marshall Islanders residing in Northwest
                                       Arkansas. Specifically, the study focuses on the relationship between migration patterns,
                                       health care access, traditional healing practices, labor, and historical trauma. As
                                       an extension of this work, he is beginning a research project in collaboration with
                                       the Arkansas Coalition of Marshallese focusing on indigenous perceptions of alcohol
                                       use, drinking patterns, and prevention strategies.
                                     
                                    
                                     Lastly, Dr. Duke has several projects in development focusing on the causes, correlates,
                                       and measurement of acculturative stress among immigrant populations, and occupational
                                       stress among low income workers.
                                     
                                    
                                     Selected Publications 
                                    
                                     2014. Duke, M.R.  Marshall Islanders: Migration Patterns and Healthcare Challenges.
                                       Migration Information Source (Migration Policy Institute). http://www.migrationpolicy.org/article/marshall-islanders-migration-patterns-and-health-care-challenges.
                                        
                                     
                                    
                                     2013     Duke, M.R., L. Bergmann, C.M. Cunradi, and G.M. Ames. Like Swallowing a Butcher Knife:
                                       Layoffs, Masculinity and Couple Conflict in the U.S. Construction Industry.  Human Organization, 72 (4):293-301.  
                                     
                                    
                                     2011  Duke, M.R. Ethnicity, well being, and the organization of labor among shade
                                       tobacco workers.  Medical Anthropology, 30 (4):409-424.  
                                     
                                    
                                     2011  Duke, M.R. and C.B. Cunradi. Measuring intimate partner violence among male
                                       and female farmworkers in San Diego County, CA.  Cultural Diversity and Ethnic Minority Psychology,17 (1):59-67.  
                                     
                                    
                                     2010  Duke, M.R., G.M. Ames, and  L. Bergmann. Competition and the limits of solidarity
                                       among unionized construction workers.  Anthropology of Work Review, 31 (2):83-91.  
                                     
                                    
                                     2010  Duke, M.R., B. Bourdeau, and J.D. Hovey. Day laborers and occupational stress:
                                       Testing the Migrant Farmworker Stress Inventory with a Latino day laborer population.
                                       Cultural Diversity and Ethnic Minority Psychology, 16(2):166-22.  
                                     
                                    
                                     2009  Duke, M.R. and F.J. Gomez Carpenterio. The effects of problem drinking and sexual
                                       risk among Mexican migrant workers on their community of origin. Human Organization
                                       68(3):328-339.
                                     
                                    
                                       
                                    
                                     Selected Awards 
                                    
                                     
                                       
                                        
                                          
                                           2016.  Engaged Scholarship Research Grant, University of Memphis. 
                                          
                                        
                                       
                                        
                                          
                                           2014   Faculty Research Grant, University of Memphis. 
                                          
                                        
                                       
                                        
                                          
                                           2013  Travel Enrichment Award, University of Memphis, College of Arts and Sciences
                                              
                                           
                                          
                                        
                                       
                                        
                                          
                                           2006-2010  Professional Development Award, Pacific            Institute for Research
                                             and Evaluation
                                           
                                          
                                        
                                       
                                        
                                          
                                           2004-2006  Fogarty International Research Award,  Migration, Alcohol, and HIV Risk in   Rural Mexico  (R03-AA015401).
                                           
                                          
                                        
                                       
                                        
                                          
                                           2002-2003  Connecticut Health Foundation,  Access to Mental Health Services for Youth   of Puerto Rican Descent in Hartford, CT  
                                          
                                        
                                       
                                        
                                          
                                           2001-2005  National Institute of Alcohol Abuse and Alcoholism,  Drinking Behaviors of   Migrant and Non-Migrant Farmworkers  (1 R01 AA 12829-01).
                                           
                                          
                                        
                                       
                                        
                                          
                                           2000-2001  Yale University Center for Interdisciplinary Research on AIDS Development
                                             Program , HIV Sex Risk Among Migrant Farmworkers .
                                           
                                          
                                        
                                       
                                     
                                    
                                       
                                    
                                     Courses 
                                    
                                     
                                       
                                        
                                          
                                           Introduction to Cultural Anthropology (ANTH 1200) 
                                          
                                        
                                       
                                        
                                          
                                           People and Cultures of the World (ANTH 3100) 
                                          
                                        
                                       
                                        
                                          
                                           Anthropological Field Methods (ANTH 4010) 
                                          
                                        
                                       
                                        
                                          
                                           Alcohol, Drugs, and Culture (ANTH 4531/6531) 
                                          
                                        
                                       
                                        
                                          
                                           Culture, Society, and Mental Health (ANTH 4993/6993) 
                                          
                                        
                                       
                                        
                                          
                                           Migration and Health (ANTH 4995/6995) 
                                          
                                        
                                       
                                        
                                          
                                           Medical Anthropology:Theory and Practice (ANTH 7511) 
                                          
                                        
                                       
                                     
                                    
                                       
                                    
                                     Additional Resources 
                                    
                                     
                                       
                                        
                                          
                                            Marshall Islanders: Migration Patterns and Healthcare Challenges. Migration Information
                                                Source (Migration Policy Institute)  
                                          
                                        
                                       
                                        
                                          
                                            Government of the Republic of the Marshall Islands  
                                          
                                        
                                       
                                        
                                          
                                            The Mental Health Needs of Immigrants (American Psychological Association)  
                                          
                                        
                                       
                                        
                                          
                                            Anthropology and Mental Health Interest Grou p
                                           
                                          
                                        
                                       
                                        
                                          
                                            National Day Laborer Organizing Network  
                                          
                                        
                                       
                                        
                                          
                                            United Campus Workers- Communication Workers of America (UCW-CWA)   
                                          
                                        
                                       
                                     
                                    
                                  
                                 
                               
                              
                            
                           
                         
                        
                        
                      
                     								
                     
                     
                     
                     
                     	
                      
                   
                
                
                   
                      
                         People in the Department of Anthropology 
                         
                            
                               
                                Faculty   Staff  
                                Emeriti Faculty  
                                Faculty in Other Departments  
                                Part-time Instructors  
                                Student Directory  
                            
                         
                      
                      
                      
                         
                            
                                Undergrads:   
                               Declare Anthropology your major! 
                            
                            
                                Join the Graduate Program  
                               Learn about applied and engaged anthropology! 
                            
                            
                                Make a Gift  
                               Support the programs and research of the Department of Anthropology 
                            
                            
                                Contact Us  
                               Main office location and numbers, undergraduate and graduate advising 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/15/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


