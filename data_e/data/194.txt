Advising - UofM Global - University of Memphis  UofM Global Advising  










 
 
 
     



 
    
    
    Advising - 
      	UofM Global
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






 

   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 

   
   
   
    
      
      
       
 
	
	
	
 
 

 


 
 

 
  
 
	 
		 
			 
				 
					     
				 
				     
				 
					 
						 
							 
								 
									   UofM Home  
										  myMemphis  
										  eCourseware  
										  Webmail  
										  Contact  
								     
								 
								 
									 Search 
									 
									 
									    
									 Site Index 
								 																
								  
							 
						 
					 
					 
						  
							 
								   About UofM Global      
									 
										  UofM Global  
										  Ranked Online Degrees  	
										  The UofM Global Experience  
										  Our Faculty  
										  Tuition   Fees  	
										  Accreditations  
									 
								 
								   Online Degrees      
									 
										  Online Degrees  
										  Undergraduate Degrees  
										  Graduate Degrees  
										  Graduate Certificates  
										  Online Admissions  
										  Contact Us  
									 
								 
								   Online Admissions      
									 
										  Online Admissions  
										  Apply Now   
										  Smart Start  
										  Financial Aid  
										  All Online Services  
									 
								 
								   Current Students      
									 
										  Student Services and Support  
										  Online Advising  
										  Course Registration  
										  Alternative Degree Pathways  
										  Learning Support  
										  Technology Support  
										  Library Services  
										  Career Services  
										  Additional Services  
										  Graduation  
									 
								 
								  Request Info  
							 
						 
					 
				 
			 
		 
		 
			 
				 
					  Online Degrees  
				 
			 
		 
	 
 
 
      
      
      
    
    
       
          
             
                
                   
                       
                           			UofM Global
                           	  
                   
                
             
          
       
       
          
             
                
                   
                      
                         
                             Home  
                             
                                 	UofM Global
                                 	  
                             
                                 	Student Services
                                 	  
                            Advising 
                         
                      
                      
                         
                            Student Services and Support   
                            
                               
                                  
                                   Student Services and Support  
                                   Online Advising Menu  
                                        
                                         Overview  
                                        
                                         Degree Planning  
                                        
                                         Find My Advisor  
                                     
                                  
                                   Course Registration  
                                         Overview  
                                         Online Course Search  
                                         Undergraduate Catalog  
                                         Graduate Catalog  
                                         Dates and Deadlines  
                                         Tuition   Fees  
                                         Scholarships  
                                     
                                  
                                   Alternative Degree Pathways  
                                         Overview  
                                         Finish Line Program  
                                         Prior Learning Assessment  
                                     
                                  
                                   Learning Support  
                                         Overview  
                                         Readiness Assessment  
                                         Success Strategies  
                                         Get Connected  
                                         Online Tutoring  
                                     
                                  
                                   Technology Support  
                                         Overview  
                                         Technology Requirements  
                                         Additional Services  
                                         Online Course Preview  
                                     
                                  
                                   Library Services  
                                         Overview  
                                         Ask-a-Librarian  
                                         Electronic Delivery  
                                         Research Consultations  
                                     
                                  
                                   Career Services  
                                   Additional Services  
                                         Overview  
                                         Adult and Veteran Students  
                                         Disability Resources  
                                     
                                  
                                   Graduation  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                     		
                     
                     
                        
                     
                      Advising 
                     
                      Academic Advising is a critical element of your success. During your time as a UofM
                        Global student, you will work closely with an academic advisor to assist you in meeting
                        goals towards your academic performance and personal development. Your academic advisor
                        will help you:
                      
                     
                      
                        
                         Set and achieve academic and career goals 
                        
                         Connect to academic and career resources 
                        
                         Help you understand degree requirements, academic policies, practices, programs and
                           support services.
                         
                        
                         Assist in keeping you on track for graduation 
                        
                      
                     
                      Before registering for courses, you should meet with your academic advisor, a critical
                        step toward staying on track for graduation. If needed, your advisor's name can be
                        found in  myMemphis  under the Student tab in the Academic Advising and Planning channel. (Note: If you
                        have recently been admitted, there is a wait period for an assignment of an advisor.
                        Contact us at uofmglobal@memphis.edu with questions regarding the assignment of your
                        advisor.)
                      
                     
                        DEGREE PLANNING      
                     
                     	
                      
                   
                
                
                   
                      
                         Student Services and Support 
                         
                            
                               
                                Student Services and Support  
                                Online Advising Menu  
                                     
                                      Overview  
                                     
                                      Degree Planning  
                                     
                                      Find My Advisor  
                                  
                               
                                Course Registration  
                                      Overview  
                                      Online Course Search  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Dates and Deadlines  
                                      Tuition   Fees  
                                      Scholarships  
                                  
                               
                                Alternative Degree Pathways  
                                      Overview  
                                      Finish Line Program  
                                      Prior Learning Assessment  
                                  
                               
                                Learning Support  
                                      Overview  
                                      Readiness Assessment  
                                      Success Strategies  
                                      Get Connected  
                                      Online Tutoring  
                                  
                               
                                Technology Support  
                                      Overview  
                                      Technology Requirements  
                                      Additional Services  
                                      Online Course Preview  
                                  
                               
                                Library Services  
                                      Overview  
                                      Ask-a-Librarian  
                                      Electronic Delivery  
                                      Research Consultations  
                                  
                               
                                Career Services  
                                Additional Services  
                                      Overview  
                                      Adult and Veteran Students  
                                      Disability Resources  
                                  
                               
                                Graduation  
                            
                         
                      
                      
                     
                     
                      
                                 Need Help? 
                                 
                                     
                                         
											     Call 844-302-3886 
                                         
                                     
                                     
                                         
                                                 Email Us 
                                         
                                     
                                 
                             
                     
                     
                     
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
 
 
 
 
  Full sitemap  
 
 
 
 
  About UofM Global  
 
 
  UofM Global  
  Ranked Online Degrees  	
  The UofM Global Experience  
  Our Faculty  
  Tuition   Fees  	
  Accreditations  
 
  
 
  Online Degrees  
 
 
  Online Degrees  
  Undergraduate Degrees  
  Graduate Degrees  
  Graduate Certificates  
  Online Admissions  
  Contact Us  
 
  
 
 
 
  Online Admissions  
 
 
  Online Admissions  
  Apply Now   
  Smart Start  
  Financial Aid  
  All Online Services  
 
  
  Contact Us  
 
  Tuition  
  Financial Aid  
  
 
	  Current Students  
 
 
  Student Services and Support  
  Online Advising  
  Course Registration  
  Alternative Degree Pathways  
  Learning Support  
  Technology Support  
  Library Services  
  Career Services  
  Additional Services  
  Graduation  
 
  
 
 
 
 
 
   
 
 
   
 
 
   
 
 
 
 
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


