Creating playlists from published presentations | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Creating playlists from published presentations | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Capture    »    Using Capture   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Creating playlists from published presentations                       
        
        
       
          
     
           Printer-friendly version       
	Once you publish presentations and presentation playlists to Capture Portal, you can combine them into new playlists or create presentations from individual playlist segments.
 

 
	Create a new playlist from a presentation
 

  
		Do one of the following:
		  
				Log in to Capture Portal and click  Admin .
			 
			 
				In Learning Environment, click    Capture Central  on your course navbar.
			 
		  
	 
		In the Manage Content area's On-Demand section, click  Manage Presentations .
	 
	 
		Click the    Playlist Management  icon for the presentation you want to turn into a playlist.
	 
	 
		Select  Create New Playlist  from the list of options.
	 
  
	Create a new playlist from a playlist segment
 

  
		Do one of the following:
		  
				Log in to Capture Portal and click  Admin .
			 
			 
				In Learning Environment, click    Capture Central  on your course navbar.
			 
		  
	 
		In the Manage Content area's On-Demand section, click  Manage Presentations .
	 
	 
		Click the    Copy  icon for the playlist segment you want to turn into a playlist.
	 
	 
		Click the    Playlist Management  icon for the copied playlist segment.
	 
	 
		Select  Create New Playlist  from the list of options.
	 
  
	Move a presentation or playlist segment to an existing playlist
 

  
		Do one of the following:
		  
				Log in to Capture Portal and click  Admin .
			 
			 
				In Learning Environment, click    Capture Central  on your course navbar.
			 
		  
	 
		In the Manage Content area's On-Demand section, click  Manage Presentations .
	 
	 
		Click on the    Playlist Management  icon for the presentation or playlist segment you want to move.
	 
	 
		Select the  Move to  option you want from the list of move options.
	 
      Audience:    Instructor      

    
           

                   ‹ Splitting long CaptureCast presentations into playlists 
        
                   up 
        
                   Purging deleted CaptureCast presentations › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Capture  
  
      Participating in CaptureCast presentations    Understanding Capture components    Using Capture    Webcasting a live CaptureCast presentation    Recording an offline CaptureCast presentation    Publishing a CaptureCast presentation    Creating live events    Managing live events    Managing published CaptureCast presentations and playlists    Managing folders    Presenting and publishing with Web Capture    Managing participants in live event chat    Understanding access controls    Uploading non-Capture videos    Splitting long CaptureCast presentations into playlists    Creating playlists from published presentations    Purging deleted CaptureCast presentations      Capture Station    Editing in post-production    Capture Central in Learning Environment    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
