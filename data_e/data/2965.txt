Anniece Robinson - WUMR - University of Memphis    










 
 
 
     



 
    
    
    Anniece Robinson - 
      	WUMR
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			WUMR
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          On-Air  
                          DJ Profiles  
                          News  
                          Supporters  
                          Underwriting  
                          CCFA  
                      
                      
                         
                            DJ Profiles   
                            
                               
                                  
                                   Ron Buck  
                                   Barry Ford  
                                   Eric Harris  
                                   Royale Johnson  
                                   Kevin Jones  
                                   Daniel Martinez  
                                   Toya Mason  
                                   Malvin Massey  
                                   Steve More  
                                   Chuck O'Bannon  
                                   Sandeep Pednekar  
                                   Linda Reed  
                                   Anniece Robinson  
                                   David Saks  
                                   Caleb Suggs  
                                   Kumasi Turner  
                                   Clay Yager  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	WUMR
                              	  
                          
                              	DJ Profiles
                              	  
                         Anniece Robinson 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Anniece Robinson 
                     
                       Currently, listeners can hear Anniece Robinson, "The Smooth Angel," on U92 FM every
                        Monday from 4 p.m. to 6 p.m. bringing the very best in Smooth Jazz.
                      
                     
                      Growing up in a music rich setting, Robinson's great grand-father was a W.C. Handy
                        band member and music teacher, Otis Elder. She is surrogate family and former nanny
                        to the late great Isaac Hayes. Anniece recalls fun days of listening to music hooks
                        at his home, family 2:00 a.m. volleyball games, eating Mary Jo's BBQ for summer holidays,
                        and road trips. She went from flutist and Fairley High band president to University
                        of Memphis 1976 Erma Clanton's renowned Evening of Soul cast member. This evolved
                        to countless local solo performances and in a few parts of the world, with highlights
                        being 2 African Presidents, an Italy 5-city tour, opening for Yolanda Adams, Jessie
                        Jackson and as a 20 year FedEx employee, requested as vocal representative for many
                        corporate and community events.
                      
                     
                      As a gospel jazz artist with Artesian Records label in the 90's, Anniece Robinson
                        was proud to be the Production Manager for Shabach, the Mid-South's only Christian
                        Supper Club, which the highlight was collaboration with 95.7 FM in featuring Kirk
                        Franklin's 911 CD Release. Currently, she serves as Executive Director of UBABY, Inc.
                        (U Be A Better You), an edutainment formatted wellness organization. She received
                        an appointment through the Tennessee Faith Based Initiative Division as Shelby County
                        Health Ambassador. Anniece is wife of Arthur Robinson for 40 plus years and the mother
                        to Desmond Robinson and Jozmen Robinson.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         DJ Profiles 
                         
                            
                               
                                Ron Buck  
                                Barry Ford  
                                Eric Harris  
                                Royale Johnson  
                                Kevin Jones  
                                Daniel Martinez  
                                Toya Mason  
                                Malvin Massey  
                                Steve More  
                                Chuck O'Bannon  
                                Sandeep Pednekar  
                                Linda Reed  
                                Anniece Robinson  
                                David Saks  
                                Caleb Suggs  
                                Kumasi Turner  
                                Clay Yager  
                            
                         
                      
                      
                      
                         
                            
                                Listen Now!  
                               Tune into U92 online 24/7! 
                            
                            
                                Make A Gift  
                                
                            
                            
                                Listener Request Line  
                               (901) 678-4867 
                            
                            
                                Contact WUMR U92 FM  
                               (901) 678-2560 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


