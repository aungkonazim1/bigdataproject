Undergraduate Program - WLL - University of Memphis    










 
 
 
     



 
    
    
    Undergraduate Program  - 
      	WLL
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of World Languages and Literatures
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          Languages  
                          People  
                          Study Abroad  
                          Scholarships  
                      
                      
                         
                            About the French Program   
                            
                               
                                  
                                   About  
                                   Why French  
                                   Undergraduate  
                                   Graduate  
                                   Study Abroad  
                                   Activities  
                                   Tutoring  
                                   Faculty  
                                   Useful Links  
                                   Department of World Languages and Literatures  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	WLL
                              	  
                          
                              	French
                              	  
                         Undergraduate Program  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Undergraduate Program 
                     
                      The Major 
                     
                      A major in French consists of twenty-four hours (eight courses) at the upper-division
                        level (3000 level or above). There are three possible concentrations:
                      
                     
                      1) concentration in French;   2) a two-language concentration, combining French with one other language taught
                        in the department;*   3) a three-language concentration: French and two other languages taught in the department.*
                      
                     
                      After completion of the language requirement (FREN 2010 and 2020), students normally
                        take FREN 3301 and 3302 (Conversation and Composition), as these courses are prerequisites
                        for other upper-division courses. Students may take 3301 or 3302 and another upper-division
                        course at the same time, if they have the permission of the instructor.
                      
                     
                       * Other languages available for the concentration taught at The University of Memphis:
                           Chinese, German, Greek, Italian, Japanese, Latin, Portuguese, Russian, and Spanish.  
                     
                      The Minor 
                     
                      Students must take eighteen hours (FREN 1020, FREN 2010, FREN 2020 and nine upper-division
                        hours). Students who place into FREN 2010 or higher may apply for credit by course
                        placement for the lower numbered courses upon completion (with a minimum grade of
                        “C”) of the course in which they have placed. After completion of FREN 2020, it is
                        highly recommended that students take FREN 3301 or 3302 as this course is a prerequisite
                        for other upper-division courses. Students may take FREN 3301 or 3302 and another
                        upper-division course at the same time, with permission from the instructor. In addition
                        to FREN 3301, the student must take two other upper-division courses. The choice of
                        these courses should be made with the help of the undergraduate advisor for French.
                      
                     
                      More Information 
                     
                      For a full description of the program, please refer to the  undergraduate catalog . A sample four-year plan for studying foreign languages can also be found by  clicking here.  
                     
                      If you are interested in finding out more about the French program, please contact: 
                     
                      Dr. Will Thompson Undergraduate Advisor  wjthmpsn@memphis.edu   Jones 108 
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         About the French Program 
                         
                            
                               
                                About  
                                Why French  
                                Undergraduate  
                                Graduate  
                                Study Abroad  
                                Activities  
                                Tutoring  
                                Faculty  
                                Useful Links  
                                Department of World Languages and Literatures  
                            
                         
                      
                      
                      
                         
                            
                                Events  
                               Including Language Fair, Film Festivals, Lectures, and other activities 
                            
                            
                                Student Resources  
                               Language Media Center, Language Tables, Placement Testing and Tutoring 
                            
                            
                                Newsletter  
                               Get the News from World Languages and Literatures 
                            
                            
                                Contact Us!  
                               Email, location and hours for the main office. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


