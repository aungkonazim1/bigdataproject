PSC - Department of Psychology - University of Memphis    










 
 
 
     



 
    
    
    PSC - 
      	Department of Psychology
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Psychology
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Undergraduate  
                          Graduate  
                          People  
                          Centers  
                          Research  
                          Resources  
                      
                      
                         
                            Centers   
                            
                               
                                  
                                   Centers  
                                        
                                         Center for Applied Psychological Research  
                                        
                                         Gambling Clinic  
                                        
                                         Psychological Services Center  
                                        
                                         Institute for Intelligence Systems  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of Psychology
                              	  
                          
                              	Centers
                              	  
                         PSC 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      The Psychological Services Center 
                     
                       Services  |  Fees For Services  |  Location and Hours  |  RRB Information  
                     
                       Department of Psychology The University of Memphis 400 Innovation Drive Memphis, TN 38152-6400 901.678.2147 Room 126, Psychology Building
                      
                     
                      At the Psychological Services Center, general outpatient psychotherapeutic and psychological
                        assessment services are provided to the individuals and families of the greater Memphis
                        metropolitan area. The Center’s mission is threefold:
                      
                     
                      
                        
                         train future psychologists, 
                        
                          provide affordable help to those in need of psychological service, and  
                        
                          promote research to support the empirical bases of psychology practice.    
                        
                      
                     
                      Students in the Clinical and School Psychology Doctoral programs, as well as those
                        in the specialist program in School Psychology, are the professional staff of the
                        Center. Our training faculty, which includes our Psychology faculty and adjunct faculty
                        chosen for their professional expertise, closely supervise the services provided by
                        these advanced graduate students.
                      
                     
                      The direct result of this training is affordable psychological services for the citizens
                        of Memphis and the surrounding communities. The Center is one of the only low-cost
                        psychology clinics in the city. Every year hundreds of individuals and families who
                        could not otherwise afford psychological assistance receive quality services at reduced
                        fee.
                      
                     
                      Finally, the Center is dedicated to applied research. All clinical grants and research
                        projects where clinical services are delivered use the Center as the service delivery
                        setting. Thus, the Center’s efforts are closely associated with the Psychology Department’s
                        focus on applied psychological research and the empirical-basis of psychology practice.
                      
                     
                      James P. Whelan, Ph.D. 
                     
                      Counseling Services 
                     
                       Adult Services  
                     
                      Individual counseling is provided for clients who are experiencing interpersonal difficulties
                        at home or at work. Problems such as depression, anxiety, phobias, sleep disturbances,
                        adjustment problems, and stress-related difficulties are commonly treated. The center
                        also has a state-of-the-art program on the treatment of problem gambling. Additionally,
                        marital/couples' therapy is provided for couples who are experiencing difficulties
                        related to conflict resolution, spouse abuse, sexual dysfunctions, or other problems
                        requiring joint intervention.
                      
                     
                       Child Services  
                     
                      Services are provided for children experiencing difficulties at school or at home.
                        Services include individual counseling, consultation, and other interventions for
                        use in the school and/or home. Center staff who specialize in dealing with childhood
                        problems work closely with children, their parents or guardians, and school personnel
                        in the management of the child's difficulties.
                      
                     
                       Family Services  
                     
                      Counseling is provided for problems requiring family interventions. Therapists trained
                        in family therapy provide counseling for problems resulting from interpersonal conflicts,
                        blended families' issues, and adjustment to divorce.
                      
                     
                       Health Psychology  
                     
                      Individual and group counseling is provided to aid clients in such activities as smoking
                        cessation, weight loss, pain and headache management, adjustment to chronic illness
                        and traumatic loss, and stress reduction. Therapists trained in medical psychology
                        and cognitive-behavioral interventions work with individuals experiencing problems
                        in such areas to promote a more healthy lifestyle.
                      
                     
                      Specialty Services 
                     
                       Psychoeducational Evaluation  
                     
                      Services include the evaluation of intelligence, achievement, personality, psychomotor,
                        and other skills and abilities in children, adolescents, and adults. The staff work
                        closely with school personnel in the evaluation of children suspected of being eligible
                        for special education (e.g. children with learning disabilities, mental retardation,
                        giftedness, attention deficit/hyperactivity disorder, behavioral disorders). In conjunction
                        with The University of Memphis Office for Students with Disabilities, the Center provides
                        evaluation and consultation for students experiencing academic problems in college.
                      
                     
                       Elimination Disorders  
                     
                      Screening and treatment are provided for children with problems of enuresis or encopresis.
                        The emphasis of the program is on training parents to deal with the frustrations of
                        soiling or bedwetting, and to implement behavioral treatment procedures for the child
                        in the home.
                      
                     
                      
                     
                      For further information or to schedule an appointment to talk with one of our therapists,
                        please call The Psychological Services Center at 901.678.2147.
                      
                     
                      Fees for Services 
                     
                      Fees for services at The Psychological Services Center are based on the total family
                        income of the client. Students at University of Memphis who are enrolled at least
                        part time are eligible for a special low fee. Additionally, University of Memphis
                        faculty and staff receive a substantial discount for all services. Please contact
                        the clinic at 901.678.2147 for more specific information.
                      
                     
                      
                     
                      For further information or to schedule an appointment to talk with one of our therapists,
                        please call The Psychological Services Center at 901.678.2147.
                      
                     
                      Location and Service Hours 
                     
                       The Psychological Services Center is open from 9 a.m. to 8 p.m. Monday through Thursday
                        and from 9 a.m. to 3:30 p.m. on Friday. The Clinic generally remains open even when
                        University classes are not in session. Therapists are on duty during operating hours
                        to handle emergency situations.
                      
                     
                         The Psychological Services Center is located on the first floor (Room 126) of the
                        Psychology Building on the University of Memphis campus. This building is located
                        at 400 Innovation Drive, one Block south of Central Avenue. Parking is available at
                        the parking garage on Innovation Drive, located directly next to the Psychology Building
                        or in the parking lot next to the Psychology Building off of Veterans Street.
                      
                     
                      RRB Information 
                     
                       RRB Procedures (DOC)  
                     
                       RRB Application (PDF)  
                     
                       RRB Review Guidelines (PDF)  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Centers 
                         
                            
                               
                                Centers  
                                     
                                      Center for Applied Psychological Research  
                                     
                                      Gambling Clinic  
                                     
                                      Psychological Services Center  
                                     
                                      Institute for Intelligence Systems  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Psychology Graduate Programs Application  
                               Click on link to Apply to the Department of Psychology Graduate Program 
                            
                            
                                Academic Advising   Resource Center (AARC)  
                                The AARC provides advising to students helping them make the most of their undergraduate
                                 education at the UofM.
                               
                            
                            
                                The Psychological Services Center  
                               PSC provides general outpatient psychotherapeutic and psychological assessment services
                                 to individuals and families
                               
                            
                            
                                Teaching Take-Out  
                               This website is a resource for busy teachers who want to enrich their classes while
                                 preserving the time they need for research and other important professional activities.
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


