Requesting captions from CaptionSync | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Requesting captions from CaptionSync | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Capture    »    Editing in post-production   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Requesting captions from CaptionSync                       
        
        
       
        
     
              
	CaptionSync is a web-based solution that generates captions for CaptureCast presentations. It is a paid service your institution can set up between Desire2Learn Capture and Automatic Sync Technologies. After a captions request is processed, CaptionSync requires 1-3 business days to create captions for your presentation.
 

 
	
 

 
	Request captions from CaptionSync
 

  
		Do one of the following:
		  
				Log in to Capture Portal and click  Admin .
			 
			 
				In Learning Environment, click    Capture Central  on your course navbar.
			 
		  
	 
		In the Manage Content area's On-Demand section, click  Manage Presentations .
	 
	 
		Click on the presentation you want to request captions for.
	 
	 
		In the Automatic Caption Generation section, select the source audio language from the drop-down list. This is the language you want to display in captions.
		 
			 Note If you cannot see the Automatic Caption Generation section, contact your Capture administrator for support to ensure CaptionSync is available and enabled for Capture Central or your Capture Portal.
		 
	 
	 
		CaptionSync turnaround time is approximately 1-3 business days. If your request for captions is urgent, select the  Rush service  check box.
		 
			 Note Due to a higher fee for Rush service requests, it is at your Capture administrator's discretion to accept or deny your request.
		 
	 
	 
		Click  Request captions .
		 
			Once the request processes, you will receive an email to indicate if your request is accepted or denied by your Capture administrator. If your captions request is accepted, you will also receive an email once the captions are created (captions appear within your presentation) and you can view it from Capture Portal or Capture Central.
		 
	 
  
	
 
     Audience:     Instructor       

    
           

                   ‹ Adding closed captions with the Producer tool 
        
                   up 
        
                   Creating and adding SRT files › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Capture  
  
      Participating in CaptureCast presentations    Understanding Capture components    Using Capture    Capture Station    Editing in post-production    Understanding the Producer tool    Accessing Producer    Loading a presentation from revision history in the Producer tool    Editing audio and video    Editing slides    Editing chapters    Adding captions    Adding closed captions with the Producer tool    Requesting captions from CaptionSync    Creating and adding SRT files      Understanding Capture Toolbox    Capture Central in Learning Environment    
                  
           
         

       
     

    
    
    
   
 
   
 
