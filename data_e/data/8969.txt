Emailing users from a dropbox folder | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Emailing users from a dropbox folder | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Dropbox    »    Evaluating dropbox folder submissions   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Emailing users from a dropbox folder                       
        
        
       
        
     
              
	Email users from a dropbox folder
 

  
		On the Dropbox Folders page, click on the folder that contains users you want to email.
	 
	 
		On the Folder Submissions page, select the check box beside each user or group you want to email and click    Email .
	 
	 
		Enter your message in the Compose New Message pop-up.
	 
	 
		Click  Send .
	 
  
	Email users who have not submitted to a dropbox folder
 

  
		On the Dropbox Folders page, click on the folder that contains users who have no submissions.
	 
	 
		On the Folder Submissions page, click  Email Users Without Submissions  or click  Email Groups Without Submissions .
	 
	 
		Enter your message in the Compose New Message pop-up. The email addresses of the selected users populate automatically in the  To  field of the message.
	 
	 
		Click  Send .
	 
  
	 Note If any member of a group submits work to a group dropbox folder, no one in that group receives an email when  Email Groups Without Submissions  is selected.
 
     Audience:     Instructor       

    
           

                   ‹ Retracting published feedback 
        
                   up 
        
                   Marking dropbox folder submissions as read or unread › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Dropbox  
  
      Dropbox basics    Creating and managing Dropbox    Evaluating dropbox folder submissions    Viewing dropbox folder file submissions    Accessing the Evaluate Submission page    Leaving feedback and grading dropbox submissions    Evaluating non-submissions and external submissions    Bulk publishing dropbox feedback evaluations    Downloading dropbox folder submission files    Uploading and attaching feedback from downloaded submission files    Retracting published feedback    Emailing users from a dropbox folder    Marking dropbox folder submissions as read or unread    Flagging dropbox folder submissions    Using Dropbox GradeMark    Using Dropbox OriginalityCheck      
                  
           
         

       
     

    
    
    
   
 
   
 
