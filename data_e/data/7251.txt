Submitting a Google document to ePortfolio | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 Submitting a Google document to ePortfolio | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   English  
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Integrations    »    Google Apps User Guide 1.3    »    Uploading Google documents   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Submitting a Google document to ePortfolio                       
        
        
       
          
     
           Printer-friendly version        
		Click   ePortfolio  on the navbar.
	 
	 
		On the My Items page, from the Add button, click  File Upload .
	 
	 
		Click    Google Drive  on the Add a File page. You can access Google Drive from any location that the Add a File page displays; however, the Google Apps integration must be authorized.
	 
	 
		Select the check box for the document you want to add.
	 
	 
		Click  Add .
	 
      Audience:    Learner      
    
         
               ‹ Submitting a Google document to Learning Environment 
                     up 
                 
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Google Apps User Guide 1.3  
  
      Google Apps basics    Uploading Google documents     Adding a Google document to tools in Learning Environment    Submitting a Google document to Learning Environment    Submitting a Google document to ePortfolio      
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
