Visit the Campus - Undergraduate Admissions and Orientation - University of Memphis  Student-led tours are held Monday through Friday at 9:30 a.m. &amp; 1:30 p.m. with the exception of university closings.  










 
 
 
     



 
    
    
    Visit the Campus - 
      	Undergraduate Admissions and Orientation
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Undergraduate Admissions and Orientation
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Freshmen  
                          Transfer  
                          Readmit  
                          Graduate  
                          Law  
                          International  
                          High School  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Undergraduate Admissions and Orientation
                              	  
                         Visit the Campus 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Visit the Campus 
 Guided Campus Tours 
 We invite all undergraduate prospective students who are interested in a general overview of the University of Memphis and the admissions process to schedule an official campus tour. 
 Student-led tours are held Monday through Friday at 9:30 am and 1:30 pm (with the exception of University closings). The tour consists of an information session led by an Admissions representative and a 60-minute walking tour led by a student guide. Tours are conducted rain or shine so please dress appropriately. Reservations are required. 
 Schedule a campus tour  here ! 
 Group Tours 
 If you plan to bring a group of 10 or more middle or high school students to campus, we ask that you register for a group tour. All students must be at least in 7th grade to attend this campus tour. We ask that you provide at least one chaperone for every 20 students in your group. 
 Group tours are held Monday through Friday at 10:00 am and 2:00 pm. The tour consists of a 60-minute walking tour led by a student guide. Tours are conducted rain or shine so please dress appropriately. Reservations are required. We cannot guarantee your preferred date, so please try to schedule your visit at least two weeks in advance. 
 Schedule a group tour  here ! 
 Self-Guided Tours 
 If you’re not able to come to the University of Memphis for a regular campus visit, we encourage you to come for a self-guided campus tour instead. Remember to wear your walking shoes! 
 Download our self-guided tour script  here . 
 RSVP for Special Events 
 Throughout the year, the Office of Recruitment and Orientation Services organizes events both on and off-campus. We invite you to visit our  schedule of events  often to stay up-to-date on recruitment events that may be of interest to you. 
 Traveling to the U of M 
All tours will originate in Wilder Tower (labeled WT on the  campus map ) unless otherwise noted. The physical address for Wilder Tower is 3675 Alumni Ave, Memphis, TN 38152.
 Coming from out of town? Visitors to the University of Memphis are eligible for special rates with the  Holiday Inn – University of Memphis . Please call 901-678-8200 to book your stay with a special discount. Please note that rates are based on hotel availability and are unavailable during high-occupancy, sold-out or blackout dates. 
 Lambuth Campus (Jackson, TN) Tours 
 For students considering the Lambuth Campus, we invite you to schedule your campus tour  here .                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Meet Your Counselor  
                               Our team can help you navigate the college planning process. 
                            
                            
                                #MEMBOUND?  
                               Sign up as a True Blue Tiger to receive information and updates. 
                            
                            
                                Campus Visits and Events  
                               Schedule a tour and view upcoming events. 
                            
                            
                                Contact Us  
                               Question?  The Office of Undergraduate Admissions and Orientation can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


