Law School Registrar &#8211; On Legal Grounds | Memphis Law Events &amp; Announcements    
 

 
	 
	 
	 
	 
	
	 Law School Registrar   On Legal Grounds | Memphis Law Events   Announcements 
 

 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 










 
 
  
 
 
    

 

 

 

	 
	
				
		 
			 
				
				 
					     
					 On Legal Grounds | Memphis Law Events   Announcements 									 
				
									 
						    
						   
						    Student Organizations 
 
	  AWA  
	  BLSA  
	  Bus.   Tax Law Society  
	  Christian Legal Society  
	  Federalist Society  
	  Health Law Society  
	  HLSA  
	  Honor Council  
	  ILS  
	  Law Review  
	  Memphis Law +1  
	  Moot Court Board  
	  OutLaw  
	  PAD  
	  PALS  
	  SELS  
	  SBA  
	  SGA  
	  Street Law  
	  TACDL  
 
 
  Law School Announcements 
 
	  Academic Affairs 
	 
		  Academic Affairs Calendar  
	 
 
	  Academic Success Program 
	 
		  Academic Success Program Calendar  
	 
 
	  Career Services Office 
	 
		  Career Services Office Calendar  
	 
 
	  Information Technology 
	 
		  Information Technology Calendar  
	 
 
	  Law Admissions, Recruiting,   Scholarships 
	 
		  Law Admissions, Recruiting, and Scholarships Calendar  
	 
 
	  Law School Registrar 
	 
		  Law School Registrar Calendar  
	 
 
	  Library 
	 
		  Library Calendar  
	 
 
	  Office of the Dean 
	 
		  Office of the Dean Calendar  
	 
 
	  Office of Diversity 
	 
		  Office of Diversity Calendar  
	 
 
	  Pro Bono Office 
	 
		  Pro Bono Office Calendar  
	 
 
	  Student Affairs 
	 
		  Student Affairs Calendar  
	 
 
 
 
  Blog Information  
  Writing Competitions  
  Events  
  
					  
								
			  
		  
		
	  
	
	 
		 			
			 
				 
 

	 

			   Category:  Law School Registrar  

	
  	
	 		
		
				
				
			 
				 				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Law School Registrar  
			 28 Nov, 2017 
		  
		
		 
			 Spring 2018 Schedule 
		  
		
				 				
			 Copyright has been cancelled for the spring 2018 semester. If you have any questions, please contact the Registrar s office. 
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Academic Affairs  /  Law School Registrar  
			 20 Nov, 2017 
		  
		
		 
			 OPEN SEATS FOR SPRING 2018 ELECTIVES 
		  
		
				 				
			 Are you  looking for a course to round out your SPRING schedule? Good news!  There are seats still available in many elective courses    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Law School Registrar  
			 15 Nov, 2017 
		  
		
		 
			 Spring 2018 Conflict Exams 
		  
		
				 				
			 Please be sure to consult the exam schedule before registering for your spring 2018 courses. If two of your exams conflict,    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Law School Registrar  
			 13 Nov, 2017 
		  
		
		 
			 Are you completing the requirements for a Certificate Program? 
		  
		
				 				
			 If you believe you are enrolled or you are working to fulfill the requirements for a Certificate Program, you are required    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Law School Registrar  
			 13 Nov, 2017 
		  
		
		 
			 Spring 2018 Registration! 
		  
		
				 				
			 Registration began TODAY, Monday, November 13: Veterans [1L, 2L and 3L] and continues the rest of this week. Tuesday, November 14    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Law School Registrar  
			 6 Nov, 2017 
		  
		
		 
			 SKILLS AND SEMINARS OPENINGS REMAIN 
		  
		
				 				
			 Open seats still remain in the courses listed below. If you have any interest in enrolling, please email lawregistrar@memphis.edu. SEMINARS /    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Law School Registrar  
			 3 Nov, 2017 
		  
		
		 
			 GRADUATING IN MAY 2018? 
		  
		
				 				
			 Apply to Graduate! You can submit your application by following the steps below: Visit your MyMemphis portal. Click on the MyDegree    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Law School Registrar  
			 2 Nov, 2017 
		  
		
		 
			 Student Evaluations of Teaching Effectiveness (SETE) are still open! 
		  
		
				 				
			 Student Evaluations of Teaching Effectiveness (SETE) are still open but will end November 20th! Your honest and complete feedback is crucial    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Law School Registrar  
			 1 Nov, 2017 
		  
		
		 
			 Skills and Seminar seats available! 
		  
		
				 				
			 Are you looking for a course to fill a hole in your Spring 2018 schedule? Did you miss the sign-up period    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Law School Registrar  
			 30 Oct, 2017 
		  
		
		 
			 Skills and Seminar sign-ups end tomorrow 
		  
		
				 				
			 Be sure to complete the appropriate survey before noon tomorrow, October 31.  Students will be notified of seat(s) received by Friday,    
		  
				
	  	
  					   			  
		
			 
			 
 Page 1 of 20  1  2  3  4  5  ...  10  20  ...     Last   
 	  
			
				
	  
	
  


	 
		
		    
		
		 
			
						 
				 Follow: 
							 
						
						
						
			   Law School Website                     		 		 Recent Posts 		 
					 
				 1L Minority Clerkship Program   Reception (Chattanooga) 
						 
					 
				 New resources! 
						 
					 
				 Community Legal Center Volunteer Opportunity 
						 
					 
				 Law School Bookstore 2 Day Sale   Dec. 6th and 7th! 
						 
					 
				 St. Jude Memphis Marathon/Downtown Street Closures 
						 
				 
		 		  Archives 		 
			  December 2017  
	  November 2017  
	  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
		 
		   Categories 		 
	  Academic Affairs 
 
	  Academic Success Program 
 
	  AWA 
 
	  BLSA 
 
	  Career Services Office 
 
	  Christian Legal Society 
 
	  Experiential Learning 
 
	  Federal Bar Association 
 
	  Federalist Society 
 
	  Health Law Society 
 
	  HLSA 
 
	  Honor Council 
 
	  ILS 
 
	  Information Technology 
 
	  Law Admissions, Recruiting,   Scholarships 
 
	  Law Review 
 
	  Law School Announcements 
 
	  Law School Registrar 
 
	  Library 
 
	  Memphis Law +1 
 
	  Mock Trial 
 
	  Moot Court Board 
 
	  National Lawyer s Guild 
 
	  Office of Diversity 
 
	  Office of the Dean 
 
	  OutLaw 
 
	  Outside Organizations 
 
	  PAD 
 
	  PALS 
 
	  Pro Bono Office 
 
	  SBA 
 
	  Sports   Entertainment Law Society 
 
	  Street Law 
 
	  Student Affairs 
 
	  Student Organizations 
 
	  Tennessee Association of Criminal Defense Lawyers 
 
	  Writing Center 
 
	  Writing Competitions 
 
		 
 			
		  
		
	  

	
 
	
	    
	
	 
		
				 
			 More 
		 
				
				
		  Search   
	 
		 
	 
    Login 					 
					 
										 
					 
					 
					  Username:  
					  
					  Password:  
					  
										  
					  Remember me  
															   
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
											  Don't have an account?  
										  Lost your password?  
					 
					
					 
					
										 
				
					 
					  Choose username:  
					  
					  Your Email:  
					  
										   
					 
					 
					 
					 
					 
					 
					 
					 
					   Have an account?   
					 
										
					 
			
					 
					  Enter your username or email:  
					  
										   
					 
					 
					 
					 
					 
					 
					 
					 
					   Back to login   
					 
					
					 
					
										  
					   
					 
					   Subscribe 	
	
 
	 
		 
		 
		 		
		
	 
		
		 
					  

		
 
		 Email Address *  
	 
  
 
		 First Name 
	 
  
 
		 Last Name 
	 
  
 
		 Middle Name 
	 
  			 
				* = required field			  
			
		 
			 
		  
	
	
				
	  
	  
  
	    Blog Feedback                     		
	  
	
  	

				  
			  			
		  
	  

	 
		
				
				
				
		 
			 
				
				    
				
				 
					
					 
						
												
						 
															 On Legal Grounds | Memphis Law Events   Announcements   2017. All Rights Reserved. 
													  
						
												 
							 Powered by  WordPress . Theme by  Alx . 
						  
												
					 
					
					 	
											 
				
				  
				
			  
		  
		
	  

  

 		
		










 
 
 