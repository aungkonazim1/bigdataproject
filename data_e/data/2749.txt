Degree Programs - MGMT - University of Memphis    










 
 
 
     



 
    
    
    Degree Programs - 
      	MGMT
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - Department of Management
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Welcome  
                          Programs  
                          Courses  
                          Faculty  
                          PhD Students  
                          Research  
                          FCBE  
                      
                      
                         
                            Programs   
                            
                               
                                  
                                   Welcome  
                                   Programs  
                                   Courses  
                                   Faculty  
                                   PhD Students  
                                   Research  
                                   Contact Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	MGMT
                              	  
                         
                           	Programs
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Degree Programs 
                     
                      Bachelor of Business Administration (BBA) with concentration in Management 
                     
                      The BBA degree in Management allows students to specialize in one of three emphasis
                        areas:    Leadership and Organizations, Human Resource Management, and General Management.
                        Each of these areas of emphasis allows students to focus their studies with a curriculum
                        geared toward leadership and management success in a varied array of organizations
                        and occupations.
                      
                     
                       Click here for more information  
                     
                      Master of Business Administration (MBA) with concentration in Management 
                     
                      At the master’s level, we provide courses in the  MBA ,  Executive MBA , and  International MBA  programs, which support career advancement in management for students with technical
                        and non-technical backgrounds and interests.
                      
                     
                      Doctor of Philosophy (PhD) in Business Administration with concentration in Management 
                     
                      The PhD Program in Management develops skilled researchers who can be successful in
                        research-oriented academic institutions.  We have trained doctoral students who were
                        placed as assistant professors in institutions such as Akron, Alabama, Alaska, Brock,
                        Fresno State, Illinois State, Kansas State, Mississippi State, New Mexico, North Texas,
                        Ohio, Ole Miss, Oregon State, Rutgers-Camden, and UT-Arlington.
                      
                     
                       Click here for more information  
                     
                       Entrepreneurship Minor  (UNDERGRADUATE)
                      
                     
                       HR MANAGEMENT CONCENTRATION  (UNDERGRADUATE) 
                      
                     
                      As of June 29, 2016, the   Society for Human Resource Management   (SHRM) has acknowledged that our BBA Degree in Management with a Concentration in
                        HR Management fully aligns with SHRM's  HR Curriculum Guidebook and Templates . Throughout the world, 425 programs in 323 educational institutions have been acknowledged
                        by SHRM as being in alignment with its suggested guides and templates. The  HR Curriculum Guidebook and Templates  were developed by SHRM to define the minimum HR content areas that should be studied
                        by HR students at the undergraduate and graduate levels. The guidelines -- created
                        in 2006 and revalidated in 2010 and 2013 -- are part of SHRM's Academic Initiative
                        to define HR education standards taught in university business schools and help universities
                        develop degree programs that follow these standards.
                      
                     
                      Additional Department Offerings (MILE Program, SHRM   IBSO Student Chapters) 
                     
                      1.   MILE Program  
                     
                      Qualifying students can participate in the    MILE (Memphis Institute for Leadership Education) Leadership Mentoring Program and
                                 2nd MILE Program   , which are designed to provide extra leadership development opportunities.  The MILE
                        Program is sponsored by the Department of Management and is available to all majors in
                        the Fogelman College of Business and Economics.  The MILE Leadership Mentoring Program
                        matches top students, primarily juniors and seniors, with local area business and
                        organization leaders who serve as mentors for one academic year.  The 2nd MILE Program
                        allows students who have graduated from the MILE Program and who have another year
                        before graduation to give back to the college and community by engaging in community
                        service project work and serving as mentors to local area high school or middle school
                        students.  For more information about MILE, please contact Dr. Bob Taylor( rrtaylor@memphis.edu ) or browse  http://mile-program.com .  
                         
                      
                     
                      2.   SHRM Student Chapter  
                     
                      The Society for Human Resource Management (SHRM) is a professional organization dedicated
                        to the professional advancement of human resource (HR) management.  The University
                        of Memphis Student Chapter of SHRM provides innovative programming for students to
                        learn more about the HR profession and network with local HR professionals via professional
                        chapter meetings, seminars, job shadowing, and internships.  Through regular meetings
                        on campus, SHRM student members learn about the importance of career development in
                        the HR field and, upon graduation, leave the University of Memphis with valuable networking
                        contacts and industry knowledge.  Prepare for your HR future, join today!  Contact
                        Dr. Carol Danehower ( vdanehwr@memphis.edu ) or Dr. Kathy Tuberville ( ktbrvlle@memphis.edu ) or  click here  for information on how to join SHRM.  Click Here  for a SHRM Flyer. 
                         
                      
                     
                                 SHRM Student Chapter Competition Team  #1 in the US Central Division SHRM Student Case Competition  
                     
                       3. International Business Student Organization (IBSO)  
                     
                      Our mission is to enhance the ability of FCBE students to perform and conduct business
                        on a global scale with a superior understanding of the many diverse international
                        market and business practices. For the current semester's meeting agenda,  click here . For a membership application form,  click here . For more information, please contact:
                      
                     
                      Raqhelle Millbrooks Chief Executive Officer/President International Business Student Organization (IBSO) Email:  Ibsomemphis1@gmail.com  
                     
                      
                          
                         
                      
                     
                      For information about the importance of internships from the University of Memphis,
                        please view  this video  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Programs 
                         
                            
                               
                                Welcome  
                                Programs  
                                Courses  
                                Faculty  
                                PhD Students  
                                Research  
                                Contact Us  
                            
                         
                      
                      
                      
                         
                            
                                MGMT News   Events  
                               Learn about current events happening in our department! 
                            
                            
                                Research Colloquia  
                               Keep up with the latest Management academic research in our upcoming seminars.  
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/1/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


