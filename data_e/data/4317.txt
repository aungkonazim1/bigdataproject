Curriculum - FCBE PhD - University of Memphis    










 
 
 
     



 
    
    
    Curriculum  - 
      	FCBE PhD
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - Doctoral Programs
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Welcome  
                          Programs  
                          Students  
                          Faculty   Staff  
                          Research  
                          News  
                          FCBE  
                      
                      
                         
                               
                            
                               
                                  
                                   Academics  
                                        
                                         Curriculum  
                                        
                                         Program Concentrations  
                                        
                                         Ph.D. Degree Learning Outcomes  
                                     
                                  
                                   How To Apply  
                                   Students  
                                         Information for Students  
                                         Program Policies  
                                         Policy For Retention Appeal Hearing  
                                     
                                  
                                   Alumni News  
                                   FAQs  
                                   Contact Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	FCBE PhD
                              	  
                          
                              	Academics
                              	  
                         Curriculum  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Curriculum at a Glance 
                     
                      The doctoral program is a post baccalaureate program and typically takes four years
                        to complete. The Ph.D. candidate at Fogelman College can select a major concentration
                        from one of six areas of Business Administration: Accounting, Economics, Finance,
                        Management, Management Information Systems, and Marketing.
                      
                     
                         Residency Requirement
                      
                     
                      Must be enrolled in two consecutive semesters with at least 9 hours credit (full-time).
                        The last 30 hours of doctoral course work excluding prerequisites, language, mathematical
                        competency and dissertation must be completed at The University of Memphis.
                      
                     
                         Research Core
                      
                     
                      The research core (12 - 15 semester hours) includes at least four doctoral level courses
                        in research methodology. The courses in the Research Core are designed by the student's
                        Program Committee to support research in that discipline.
                      
                      
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                    ACCT   
                                 
                                    ECON   
                                 
                                    FIN   
                                 
                                    MGMT   
                                 
                                    MIS   
                                 
                                    MKTG   
                                 
                               
                              
                               
                                 
                                   Research Core  
                                 
                               
                              
                               
                                 
                                  ECON 8125 
                                 
                                  ECON 6810 
                                 
                                  ECON 8120 
                                 
                                  SCMS 8530 
                                 
                                  SCMS 8530 
                                 
                                  SCMS 8530 
                                 
                               
                              
                               
                                 
                                  ECON 8810 
                                 
                                  ECON 8125 
                                 
                                  ECON 8810 
                                 
                                  SCMS 8540 
                                 
                                  SCMS 8540 
                                 
                                  SCMS 8540 
                                 
                               
                              
                               
                                 
                                  ECON 8811 or 
                                 
                                  ECON 8810 
                                 
                                  ECON 8811 
                                 
                                  MGMT 8921 
                                 
                                  MGMT 8921 
                                 
                                  MGMT 8921 
                                 
                               
                              
                               
                                 
                                  MKTG 8217 
                                 
                                  ECON 8811 or 
                                 
                                  ECON 7300 or 
                                 
                                  MKTG 8217 
                                 
                                  MKTG 8217 
                                 
                                  MKTG 8217 
                                 
                               
                              
                               
                                 
                                    
                                 
                                  ECON 8812 
                                 
                                  ECON 7125 
                                 
                                    
                                 
                                    
                                 
                                    
                                 
                               
                              
                               
                                 
                                   Major  
                                 
                               
                              
                               
                                 
                                  18 hrs in ACCT courses. An additional 12 hrs determined by advisor 
                                 
                                  24 hrs in ECON courses. An additional 6 hrs determined by advisor 
                                 
                                  21 hrs in FIR courses. An additional 9 hrs determined by advisor 
                                 
                                  18 hrs in MGMT courses. An additional 12 hrs determined by advisor
                                  
                                 
                                  21 hrs in MIS courses. An additional 9 hrs determined by advisor 
                                 
                                  21 hrs in MKTG courses. An additional 9 hrs determined by advisor 
                                 
                               
                              
                            
                           
                         
                        
                      
                      
                     
                      The concentration (30 semester hours total) may be selected from the following: Accountancy,
                        Economics, Finance, Management, Management Information Systems, and Marketing.  Additional
                        courses designed to complement the student's overall educational and research objectives,
                        must be approved by the Departmental Program Committee and by the Associate Dean for
                        Academic Programs.
                      
                     
                      The dissertation (18 semester hours) requires a major research project of an original
                        and creative nature.  The dissertation is the research capstone of the Ph.D. program. 
                        It must be a significant contribution to an area of study in Business Administration
                        and must meet the requirements of the Graduate School.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                          
                         
                            
                               
                                Academics  
                                     
                                      Curriculum  
                                     
                                      Program Concentrations  
                                     
                                      Ph.D. Degree Learning Outcomes  
                                  
                               
                                How To Apply  
                                Students  
                                      Information for Students  
                                      Program Policies  
                                      Policy For Retention Appeal Hearing  
                                  
                               
                                Alumni News  
                                FAQs  
                                Contact Us  
                            
                         
                      
                      
                      
                         
                            
                                Program Concentrations  
                               Learn about our doctoral offerings! 
                            
                            
                                Information for Students  
                               Looking for forms or policies?  Look no further! 
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/30/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


