Program Service Fees - Bursar's Office - University of Memphis    










 
 
 
     



 
    
    
    Program Service Fees - 
      	Bursar's Office
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Bursar's Office
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Staff Directory  
                          Fees  
                          Calendars  
                          Students  
                          Parents  
                          Campus Card  
                          Bursar Areas  
                      
                      
                         
                            Fees   
                            
                               
                                  
                                   Fee Charts  
                                   Fee Payment Policies  
                                   Deadlines, Refunds   Important Dates  
                                   Fee Payment Methods  
                                   Installment Payment Plan  
                                   Explanation of Fees  
                                   Financial Assistance/ Sponsorships General Info  
                                   Fee Discounts   Waivers  
                                   Fee Adjustment (Refund) Policy  
                                   Third Party/Direct Billing  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Bursar's Office
                              	  
                          
                              	Fees
                              	  
                         Program Service Fees 
                      
                   
                   
                       
                      
                     		
                     
                     
                      Program Service Fees 
                     
                      The Program Service Fee is a combination of the Debt Service Fee, General Access Fee,
                        Facility Fee, and Student Activity Fee. This is a required fee of all students who
                        participate in any campus courses at The University of Memphis and is assessed based
                        on the student's matriculation level.
                      
                     
                      General Access Fee - Required of All Students 
                     
                      General Access Fee provides funding for various student services. General Access Fees
                        are assessed based on the student's matriculation level (undergraduate/graduate/law)
                        and the number of hours enrolled. For fall and spring semesters, all students enrolled
                        for one to five hours are assessed a part-time general access fee; all students enrolled
                        for six or more hours are assessed a full-time general access fee. There is no maximum
                        full-time general access fee for summer semesters; Sustainable Campus "Green" Fee
                        is not assessed during summer semesters. The following services are funded through
                        the General Access Fee:
                      
                     
                      
                        
                         General Parking Access 
                        
                         Change of Course 
                        
                         Technology Access Fee 
                        
                         Student Health Services 
                        
                         Sustainable Campus "Green" Fee* 
                        
                         Graduation Fee 
                        
                         Career Connections 
                        
                         Student Athletic Fee 
                        
                         International Experience 
                        
                      
                     
                      Student Activity Fee - Required of All Students 
                     
                      The purpose of the Student Activity Fee is to provide and promote programs to assist
                        in the development of educational, social, cultural, recreational and leadership skills
                        of all students. The fee is designed to encourage broad student participation and
                        to include all members of the student body. These fees are assessed based on the student's
                        matriculation level (undergraduate/graduate/law) and the number of hours enrolled.
                        For fall and spring semesters, all students enrolled for one to five hours are assessed
                        a part-time student activity fee; all students enrolled for six or more hours are
                        assessed a full-time student activity fee. There is no maximum full-time student activity
                        fee for summer semesters.
                      
                     
                      Debt Service Fee - Required of All Students 
                     
                      Debt Service Fee supports the retirement of indebtedness funds used for various capital
                        projects as approved by the Tennessee Board of Regents for each academic year. These
                        fees are assessed based on the student's matriculation level (undergraduate/graduate/law)
                        and the number of hours enrolled. For fall and spring semesters, all students enrolled
                        for one to five hours are assessed a part-time debt service fee; all students enrolled
                        for six or more hours are assessed a full-time debt service fee. There is no maximum
                        full-time debt service fee for summer semesters.
                      
                     
                      Facilities Fee - Required of All Students 
                     
                      Facilities Fee will fund deferred maintenance and renovations of University facilities,
                        including upgrading classrooms and campus infrastructure improvements. These fees
                        are assessed based on the number of hours for which the student is enrolled. For fall
                        and spring semesters, all students enrolled for one to five hours are assessed a part-time
                        facilities fee; all students enrolled for six or more hours are assessed a full-time
                        facilities fee. There is no maximum full-time facilities fee for summer semesters.
                      
                     
                      *Students registered for 6 hours or more in the fall and spring semesters are assessed
                        a "Full Time" Program Service Fee. Summer registrations are assessed at the per-hour-rate,
                        with no maximum.
                      
                     
                      *Effective Spring 2010, Law Students are no longer assessed the General Parking Access
                        fee.
                      
                     
                      *Sustainable Campus "Green" Fee is assessed during Fall and Spring semesters only.
                        This fee is not assessed during summer terms.
                      
                     
                     	
                      
                   
                
                
                   
                      
                         Fees 
                         
                            
                               
                                Fee Charts  
                                Fee Payment Policies  
                                Deadlines, Refunds   Important Dates  
                                Fee Payment Methods  
                                Installment Payment Plan  
                                Explanation of Fees  
                                Financial Assistance/ Sponsorships General Info  
                                Fee Discounts   Waivers  
                                Fee Adjustment (Refund) Policy  
                                Third Party/Direct Billing  
                            
                         
                      
                      
                      
                         
                            
                                TigerXpress  
                               View your statements and pay fees online in TigerXpress. 
                            
                            
                                Tuition Estimator  
                               How much will your courses cost? Get an estimate online! 
                            
                            
                                Forms  
                               All of B F's forms in one place 
                            
                            
                                Business   Finance  
                               The Division of Business   Finance at the U of M 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


