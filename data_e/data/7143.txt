Adding closed captions with the Producer tool | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Adding closed captions with the Producer tool | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Capture    »    Editing in post-production   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Adding closed captions with the Producer tool                       
        
        
       
          
     
           Printer-friendly version       
	 Note If you want to include an SRT file in addition to captions you create in Producer, you must upload the SRT file first. If you upload an SRT file after you create captions in Producer, the file will overwrite your existing captions.
 

  
		Do one of the following:
		  
				Log in to Capture Portal and click  Admin .
			 
			 
				In Learning Environment, click    Capture Central  on your course navbar.
			 
		  
	 
		In the Manage Content area's On-Demand section, click  Manage Presentations .
	 
	 
		Click the    Post-Production  icon.
	 
	 
		Click the  Closed Captions  tab.
	 
	 
		Click the    Add a new caption  icon to open the Caption area.
	 
	 
		Enter your caption in the  Title  field. As you enter text, the text displays in your captions list.
	 
	 
		Select the    Time  to adjust when the caption appears in the presentation. You can click the    Set time to current playback position  icon to add the caption to the presentation's current playback position.
	 
	 
		Click  Save . Repeat steps 2-5 to continue adding captions.
	 
      Audience:    Instructor      

    
           

                   ‹ Adding captions 
        
                   up 
        
                   Requesting captions from CaptionSync › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Capture  
  
      Participating in CaptureCast presentations    Understanding Capture components    Using Capture    Capture Station    Editing in post-production    Understanding the Producer tool    Accessing Producer    Loading a presentation from revision history in the Producer tool    Editing audio and video    Editing slides    Editing chapters    Adding captions    Adding closed captions with the Producer tool    Requesting captions from CaptionSync    Creating and adding SRT files      Capture Central in Learning Environment    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
