EO - OIE - University of Memphis    










 
 
 
     



 
    
    
    EO - 
      	OIE
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Office for Institutional Equity
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          EOAA  
                          Harassment  
                          Title IX  
                          ADA  
                          Policies  
                          Resources  
                      
                      
                         
                            EOAA   
                            
                               
                                  
                                   Equal Opportunity  
                                   Affirmative Action  
                                   Equal Opportunity and Affirmative Action Policy  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	OIE
                              	  
                          
                              	Equal Opportunity and Affirmative Action
                              	  
                         EO 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Equal Opportunity 
                     
                       Equal Opportunity is the right of all persons to enter, study and advance in academic
                           programs on the basis of merit, ability, and potential without regard to race, color,
                           national origin, sex, sexual orientation, genetic information, disability or status
                           as a veteran.  
                     
                      Equal Employment Opportunity is the right of all persons to work and to advance on
                        the basis of merit, ability, and potential without regard to race, color, national
                        origin, sex, sexual orientation, genetic information, religion, disability, age or
                        status as a veteran.
                      
                     
                      Equal Opportunity is the law and discrimination is prohibited by laws such as: 
                     
                      
                        
                         Title VI and VII Civil Right Acts of 1964, as amended 
                        
                         Equal Pay Act of 1963 
                        
                         Age Discrimination in Employment Act of 1967, as amended 
                        
                         Title IX of the Education Amendments of 1972 
                        
                         Vietnam Era Veterans Readjustment Act of 1974 
                        
                         Executive Order 11246, as amended 
                        
                         Rehabilitation Act of 1973, including Section 503 and 504 
                        
                         Americans with Disabilities Act of 1990/Americans with Disabilities Act Amendments
                           Act of 2008
                         
                        
                      
                     
                      Commitment...The University of Memphis is committed to maintaining a learning and
                        working environment that is free from discrimination, harassment and retaliation.
                      
                     
                      Responsibility.... All members of the University community have a shared responsibility
                        to see that Equal Opportunity and Affirmative Action procedures are considered in
                        all academic and employment practices - admissions, grading, recruiting, hiring, transfers,
                        promotions, compensation, discipline, benefits and other terms, conditions, benefits
                        and privileges associated with academia or employment. All members of the University
                        community should be familiar with the University's policies on discrimination and
                        harassment and  sexual misconduct .
                      
                     
                     	
                      
                   
                
                
                   
                      
                         EOAA 
                         
                            
                               
                                Equal Opportunity  
                                Affirmative Action  
                                Equal Opportunity and Affirmative Action Policy  
                            
                         
                      
                      
                      
                         
                            
                                Training  
                               OIE sponsors several online training modules, available in Learning Curve. 
                            
                            
                                Contact Us  
                               OIE provides a range of services that support equal opportunity and nondiscrimination. 
                            
                            
                                How to File a Complaint  
                               File a complaint of discrimination, harassment, sexual misconduct or retaliation. 
                            
                            
                                Sexual Misconduct  
                               General information and resources 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


