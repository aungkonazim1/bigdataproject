Earthworm Modules: Startstop commands (Linux version)   
 
 
 Earthworm Modules: Startstop commands (Linux version) 
 
 

  Earthworm Modules:  Startstop Configuration File Commands (Linux version) 
 (last revised 8 July 2016, adapted from  Solaris version )  

Page Index: 

1.  Example Configuration File  

2.   Functional command listing  

3.   Detailed command description 
 
 
Startstop is a system-specific module; the configuration file described here is for
the Linux version.  On startup, startstop changes to the directory specified by the EW_PARAMS environment variable.  Then it reads the configuration file
named  startstop_unix.d .  This file sets up all parameters required to run an Earthworm system.  In the configuration file, lines may begin with
a valid startstop command (listed below) or with a # to denote a comment.  Command names must be typed in the configuration file exactly as shown in this
document (upper/lower case matters!).
 
  
 1. EXAMPLE startstop_unix.d CONFIGURATION FILE 
 
#
#       Startstop (Linux Version) Configuration File
#
#     nRing  is the number of transport rings to create.
#     Ring  specifies the name of a ring followed by it's size
#    in kilobytes, eg        Ring    WAVE_RING 1024
#    The maximum size of a ring depends on your operating system,
#    amount of physical RAM and configured virtual (paging) memory
#    available. A good place to start is 1024 kilobytes.
#    Ring names are listed in file earthworm.d.
#
 nRing               3
 Ring   WAVE_RING 1024
 Ring   PICK_RING 1024
 Ring   HYPO_RING 1024
#
 MyModuleId    MOD_STARTSTOP  # Module Id for this program
 HeartbeatInt  50             # Heartbeat interval in seconds
 MyClassName   TS             # For this program
 MyPriority     0             # For this program
 LogFile        1             # 1=write a log file to disk, 0=don't
 KillDelay      5             # seconds to wait before killing modules on
                              #  shutdown
 HardKillDelay  5             # seconds to wait after SIGKILL
                              #   if absert, no SIGKILL is sent
# maxStatusLineLen 120         # Uncomment to specify length of lines in status
 Stderr         None          # Where to send stderr messages: None, Console or File
#
#    Class must be RT or TS
#    RT priorities from 0 to 59
#    TS priorities le 0
#
#    If the command string required to start a process contains
#    embedded blanks, it must be enclosed in double-quotes.
#    Processes may be disabled by commenting them out.
#    To comment out a line, preceed the line by #.
#    The optional Agent command lists the user and group names under which
#       the command will be run. Make sure they are valid names!
#       You cannot use "root" as the Agent user. If an earthworm module
#       needs to run as root, you should make that module's binary setuid root.
#
 Process          "pick_ew pick_ew.d"
 Class/Priority    TS 0
 Agent             "eworm" "net"
#
# Process          "coaxtoring coaxtoring.d"
# Class/Priority    RT 10
#
# Process          "export_generic export_picks.d"
# Class/Priority    TS 0
#
 Process          "binder_ew binder_ew.d"
 Class/Priority    TS 0
 Stderr            File
#
#
 Process          "eqproc eqproc.d"
 Class/Priority    TS 0
 Stderr            Console
#
 Process          "diskmgr diskmgr.d"
 Class/Priority    TS 0
#
 Process          "pagerfeeder pagerfeeder.d"
 Class/Priority    TS 0
#
 Process          "statmgr statmgr.d"
 Class/Priority    TS 0
#
# Process          "eqalarm_ew eqalarm_ew.d"
# Class/Priority    TS 0
#
 Process          "copystatus WAVE_RING HYPO_RING"
 Class/Priority    RT 5
#
 Process          "copystatus PICK_RING HYPO_RING"
 Class/Priority    RT 5
#
 Process          "menlo_report menlo_report.d"
 Class/Priority    TS 0
#
 

  
 2.  FUNCTIONAL COMMAND LISTING 
 
 
Unlike most other Earthworm modules, startstop expects the lines in its
control file to be in a certain order.  Below are the commands recognized
by startstop, grouped by the function they influence and listed in
the order in which they must appear in the control file.  All commands
are required!
 
 
 

 	Shared memory setup:
                 nRing 		   	required
 		 Ring 		   	required (nRing)

        Startstop-specific info:
 		 MyModuleId 	   	required
		 HeartbeatInt 		required
		 MyClassName 		required
		 MyPriority 		required
		 LogFile 		   	required
		 KillDelay 		required
		 HardKillDelay 		optional
		 maxStatusLineLen 	optional
		 Stderr 			optional

	Earthworm modules to run:
		 Process 		 	required \
		 Class/Priority 		required  |  one set
		 Stderr 			optional  | per module
		 Agent 			optional /

 

  
 3.  DETAILED COMMAND DESCRIPTION 
 
In the following section, a detailed description of each startstop command
is given.  Again, the commands are listed in the order that they must appear
in the configuration file,  startstop_unix.d .  Example commands are given after
each command description. 
Comment lines in  startstop_unix.d  begin with a #.
 
  
command  arg1 
  
 

 

     
  nRing  nRing 
  
   
Specifies the number of shared memory regions (aka transport rings)
that startstop will create for "public" use by the modules of this
Earthworm system.  nRing  should be an integer from 1 to MAX_RING
(currently defined to be 50).
  
Example:  nRing 3
 
 
 

     
  Ring  name   size   
   
Specifies the  name  and  size , in kilobytes, of one transport ring.
There must be exactly  nRing  "Ring" commands following the "nRing"
command in the  startstop_unix.d .   name  is a character string (up to
19 characters long, valid strings are listed in earthworm.d) that
relates (in  earthworm.d ) to a unique number for the key to the shared
memory region.   size  should be an integer between 1 and 1024 (the
default maximum size).  When deciding how large to make
a memory region, realize that the transport layer uses a portion of
the memory region for its own bookkeeping.  The region size is NOT
required to be an even multiple of the size of the messages it will
contain.  However, suppose you want the region to be exactly large
enough to store NUM messages of size MSGSIZE.  To include space for
transport bookkeeping too, the region size (in bytes) should be:
 
sizeof(SHM_HEAD) + NUM * ( sizeof(TPORT_HEAD) + MSGSIZE )
 
where SHM_HEAD and TPORT_HEAD are structures defined in  transport.h .
Startstop places all of its outgoing messages in the memory region
specified by the first "Ring" command.
  
Example:  Ring  WAVE_RING 1024
 
 
 

     
  MyModuleId  mod_id 
  
   
Sets the module id for labeling all outgoing messages.  mod_id  is
a character string (up to 30 characters, valid strings listed in
 earthworm.d ) that relates (in  earthworm.d ) to a unique single-byte
number.	 Startstop places its outgoing messages on the shared memory
ring specified by the first "Ring" command in startstop_sol.d.
  
Example:  MyModuleId MOD_STARTSTOP
 
 
 

     
  HeartbeatInt  nsec 
  
   
Defines the number of seconds,  nsec , between TYPE_HEARTBEAT messages
issued by startstop.  Startstop places its heartbeats on the shared
memory ring specified by the first "Ring" command in  startstop_unix.d .
  
Example:  HeartbeatInt 30
 
 
 

     
  MyClassName  class 
  
   
Sets the process  class  for startstop.   class  is a character string
that must be set to either "RT" (for Real-Time) or "TS" (for Time-
Share) on solaris.
  
Example:  MyClassName RT
 
 
 

     
  MyPriority  priority 
  
   
Sets the process  priority  for startstop.  See the "Class/Priority"
command below for allowed values.  After startstop reads its
configuration file, it changes its own class/priority to the values
specified in the "MyClassName" and "MyPriority" commands.
  
Example:  MyPriority 4
 
 
 

     
  LogFile  switch 
  
   
Sets the on-off switch for writing a log file to disk.  If  switch 
is 0, no log file will be written.  If  switch  is 1, startstop
will write a daily log file(s) called  cfname  _  yyyymmdd  .log 
where  cfname  is the name (without extension) of startstop's configuration file
(ex: startstop_unix)
and  yyyymmdd  is the current UTC date (ex: 19960123) on the system clock.
The file(s) will be written in the EW_LOG directory (environment
variable).
  
Example:  LogFile 1
 
 
 

     
  KillDelay  nsec 
  
   
Gives the number of seconds  nsec  that startstop will wait
for modules to shutdown gracefully on termination. After this delay, startstop will force
modules to shut down with the TERM signal.  If module still does not stop after  nsec 
seconds, behavior is determined by the "HardKillDelay" command.
  
Example:  KillDelay 10
 
 
 

    
  HardKillDelay  nsec 
  
  
Optional specification of number of seconds  nsec  that
startstop will wait
for modules to shutdown after being sent a KILL signal before aborting a restart of the module.
If not specified, no KILL signal is issued, and any restart is aborted after sequence described in
the "KillDelay" command.
  
Example:  HardKillDelay 10
 
 
 

    
  maxStatusLineLen  nchars   
  
Optional specification of maximum length of a line when printing status to  nchars  characters; default is 80, and new value cannot be smaller.
When specified, status will adjust column widths to minimize truncation of information.
  
Example:  maxStatusLineLen 120
 
 
 

    
  Stderr  target   
  
Optional specification of what to do with messages printed to stderr;  target  can be either "Console"
(the screen, the default), "None" (messages are eliminated), or "File" (messages get written to a file in the Log directory,
named as the log file for this module would be but with the extension .err).  When specified for startstop itself, it is
the default for all modules (save startstop); when specified for a specific module, it overrides the specification for all modules.
In the  example configuration , every module's stderr messages are surpressed except for  eqproc 
(whose go to the screen) and  binder_ew  (whose go to a file).
  
Example:  Stderr File
 
 
 

     
  Process  cmdstring 
  
   
Gives the command string,  cmdstring , (up to 79 characters) that will
be used to start one Earthworm module.  Each "Process" command
must be immediately followed by a "Class/Priority" command.  If the
 cmdstring  required to start a process contains embedded blanks, it
must be enclosed in double-quotes.  Startstop can create up to MAX_CHILD (currently defined as 200) child processes.
  
Example:  Process "binder_ew binder_ew.d"
 
 
 

     
  Class/Priority  class priority 
  
   
Specifies the  class  and  priority  under which the module given by
the previous "Process" command will be run.   class  is a 2-character
string and  priority  is an integer; their values are restricted to:
 
		      class     priority
	Real-Time     "RT"      0 to 59
	Time-Share    "TS"        = 0
 
Class RT processes run at higher priority than class TS processes.
Take care not to assign known CPU hogs (binder_ew, for example) to the RT
class.  Such an assignment could bog down all class TS processes,
causing poor Earthworm system performance. After a "Class/Priority"
line, startstop expects to find the "Process" command of the next
Earthworm module it should spawn.
  
Example:  Class/Priority  TS  0
 
 
 

     
  Agent  user group 
  
   
Optional command to specify the  user  and
 group  names under which this module will run. Normally this
feature will not be used. But if you need to have the output and log files from
this module owned by a user other than the one who runs startstop, then you can use
Agent to do this. You cannot specify root as the user agent. If you need to
have a module run with root privelege, you should set that module's file
permissions to make it setuid root. Make sure that  user  is a
valid user name and  group  is a valid group name for your system.
  
Example:  Agent "eworm" "net"
 
 
 

 
 Module Index  |
 Startstop Overview 
 

 
 
 
Contact:  Questions? Issues?  Subscribe to the Earthworm Google Groups List.    
 
 
 
