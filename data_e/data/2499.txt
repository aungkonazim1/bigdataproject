Advising - Department of English - University of Memphis    










 
 
 
     



 
    
    
    Advising  - 
      	Department of English
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of English
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Undergraduate  
                          Graduate  
                          People  
                          Community  
                          News/Events  
                      
                      
                         
                            Graduate Students   
                            
                               
                                  
                                   Application Process  
                                   MA  
                                         Composition Studies  
                                         ESL  
                                         Linguistics  
                                         Literature  
                                         Professional Writing  
                                     
                                  
                                   MFA  
                                         Creative Writing  
                                     
                                  
                                   PhD  
                                         Applied Linguistics  
                                         Composition Studies  
                                         Professional Writing  
                                         Literary and Cultural Studies  
                                     
                                  
                                   Certificate  
                                         African American Literature  
                                         Teaching English as a Second/Foreign Language  
                                     
                                  
                                   Online Degree Programs  
                                   Advising  
                                   Financial Aid and Scholarships  
                                   Graduate Courses  
                                         Two-Year Template 2016-2018  
                                         Two-Year Template 2018-2020  
                                         Graduate Course Descriptions  
                                     
                                  
                                   Organizations  
                                         UM English Graduate Organization  
                                          The Pinch   
                                         Society for Technical Communication  
                                     
                                  
                                   Forms  
                                         UofM Graduate Online Forms  
                                         English Teaching/Graduate Assistantship Application  
                                         Graduate Advising Worksheet  
                                         Graduate Independent Study Form  
                                         Thesis Hour Registration and Prospectus Form  
                                         Dissertation Hour Registration Form  
                                     
                                  
                                   Graduate Catalog  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of English
                              	  
                          
                              	Graduate Students
                              	  
                         Advising  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Graduate Advising 
                     
                      Graduate students are advised by faculty members in their chosen concentration. Incoming
                        students are generally advised by the Concentration Coordinator, while advanced students
                        should choose an appropriate advisor for their academic interests.
                      
                     
                      Students should consult the  Graduate Catalog  for degree requirements and information on Department of English and University of
                        Memphis policies. Additional information can be found on the webpages devoted to each
                        degree program.
                      
                     
                      Every graduate student must be advised by a member of the Graduate Faculty before
                        his or her advising hold will be cleared. Students should complete an  English Advising Worksheet  with his or her advisor to facilitate this process. Once completed, the worksheet
                        should be submitted to the Graduate Office for filing. If a student fails to submit
                        the worksheet, it is the responsibility of the faculty advisor to contact the Graduate
                        Office to have holds cleared for registration.
                      
                     
                      Students are encouraged to meet with advisors early to make time to obtain restricted
                        course permits from the Departmental Registrar in Patterson 467 or obtain course overload
                        permission from the CAS Associate Dean for Graduate Studies in Scates Hall.
                      
                     
                      New students should contact  Ms. Sarah Ellis  in the Graduate Office, Patterson Hall 471, 901.678.1448, to be connected to their
                        concentration coordinator for initial advising.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Graduate Students 
                         
                            
                               
                                Application Process  
                                MA  
                                      Composition Studies  
                                      ESL  
                                      Linguistics  
                                      Literature  
                                      Professional Writing  
                                  
                               
                                MFA  
                                      Creative Writing  
                                  
                               
                                PhD  
                                      Applied Linguistics  
                                      Composition Studies  
                                      Professional Writing  
                                      Literary and Cultural Studies  
                                  
                               
                                Certificate  
                                      African American Literature  
                                      Teaching English as a Second/Foreign Language  
                                  
                               
                                Online Degree Programs  
                                Advising  
                                Financial Aid and Scholarships  
                                Graduate Courses  
                                      Two-Year Template 2016-2018  
                                      Two-Year Template 2018-2020  
                                      Graduate Course Descriptions  
                                  
                               
                                Organizations  
                                      UM English Graduate Organization  
                                       The Pinch   
                                      Society for Technical Communication  
                                  
                               
                                Forms  
                                      UofM Graduate Online Forms  
                                      English Teaching/Graduate Assistantship Application  
                                      Graduate Advising Worksheet  
                                      Graduate Independent Study Form  
                                      Thesis Hour Registration and Prospectus Form  
                                      Dissertation Hour Registration Form  
                                  
                               
                                Graduate Catalog  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now  
                                
                            
                            
                                Alumni and Friends  
                                
                            
                            
                                Course Offerings  
                                
                            
                            
                                Contact Us  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/30/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


