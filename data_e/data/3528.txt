Services - University Libraries' Instructional Services - LibGuides at University of Memphis Libraries   
 
	 
         
         
         
        
		 Services - University Libraries' Instructional Services - LibGuides at University of Memphis Libraries 
		 
		 
 
 
 
 
 
 
 
 
 
 
 

		 
		 
		 
		 
		 
		 
         
         
				 
				 
             
         
        
        
            
		
		 
		
		 
		 
		
		 
		
		
	   
		         
        	 
     
	 
		 Skip to main content                  
                 
         
  		 
             
                
			 
				
					 
						 University of Memphis Libraries
						 
					 
					 
						 LibGuides
						 
					 
					 
						 University Libraries' Instructional Services
						 
					 
					 Services
					 
			              
			 
				 
					
                 
                     
                         
                             
                             Search this Guide  
                             
                                 Search 
                             
                         
                     
                 				 
							 
             
                 University Libraries' Instructional Services 
                 
                     Learn more about the Instructional Services department: our services, policies, and long-term goals and plans. 
                 
             
         
         
          
         
             
                 
                     
                         
                             
                 
                     
                         Services 
                        
                     
                 
                 
                     
                         Policies 
                        
                     
                 
                     
                         
                             Instructional Services Plan 2017 - 2021 
                            
                         
                         
                                      
                                     Toggle Dropdown 
                                  
                             
                                 Introduction and Purpose of Instructional Plan
                                    
                                 
                             
                             
                                 Departmental Priorities
                                    
                                 
                             
                             
                                 Modes of Instruction
                                    
                                 
                             
                             
                                 Instruction Guidelines
                                    
                                 
                             
                             
                                 Recommended Reading and Sources Consulted
                                    
                                 
                              
                      
                            
                 
                     
                         
                        
                         
                     
                                          
                     
                 
                 
                     
                         
                             
     
                  
					 
						 
							 Request Formal Instruction
                                 
							 
								 
									
			 
				     

 Helpful hints before you schedule: 

 
	 Instructional sessions are most effective when tied to a specific assignment. These assignments allow the students to apply the concepts learned and make the instructional session more worthwhile for the students. Librarians can collaborate with instructors to develop assignments that meet information literacy objectives. 
 

 
	 Make students aware of the assignment ahead of time. Students are more likely to come prepared for a library instructional session if they understand the objective of the session and how it is tied to their coursework. 
 

 
	 Faculty attendance at course-integrated instruction is essential. Not only does it create better collaboration between library and teaching faculty, it demonstrates that mutual participation in the session is important to the instructor. 
 

 
	 Library instruction should be arranged as early as possible to avoid scheduling conflicts. 
 

 
	 Plan your library instructional session as close to the time of need as possible. The session will be more meaningful for students if they can immediately begin to apply what they learn. General introductions to library resources/research do not have a lasting impact for students. The most successful instructional sessions are followed by activities that reinforce information literacy skills. 
 

   Request Library Instruction  

I 

 If you have not heard from the library within two regular working days, please contact Barbara Thomas at  brthomas@memphis.edu  or call the Research and Information Services Desk at 901.678.2208. 
		    
								 
								
							 
						 
					   
					 
						 
							 E-Learning
                                 
							 
								 
									
			 
				     

 Get started on your own using specialized  Research Guides . Find the best databases, journals, articles, books, ebooks, and websites for what you re researching. 

   

   

 Learn at your own pace, whenever, wherever. List of tutorials: 

 
	   UNIV Research Path  . 6 tutorials on developing a research topic, learning search strategies, database selection, resources outside of the library, source evaluation, and APA citation style.  
	  
 
		    
								 
								
							 
						 
					   
					 
						 
							 Research Consultations
                                 
							 
								 
									
			 
				     

 Sign up for one-on-one research help with a librarian in-person or online. 
		    
								 
								
							 
						 
					   
					 
						 
							 Workshops
                                 
							 
								 
									
			 
				     
		    
			   
		    
								 
								
							 
						 
					  
         
     
                          
                     
                    
                 
                     
                    
                     
                     
                      Next:  Policies    
                     
                                  
             
         
         
         
             
                 
                     
                         Last Updated:   Dec 6, 2017 4:42 PM                      
                     
                         URL:   http://libguides.memphis.edu/instruction                      
                     
                    	    Print Page                      	
                     
                 
                     Login to LibApps                  
             
             
                 
                     Report a problem  
                 
                 
                    
                     Subjects:  
                      Help/How-To Guides ,  Library and Information Science  
                                     
                 
                    
                     Tags:  
                      composition ,  information literacy ,  instruction & curriculum leadership ,  learning  
                                     
             
         
         
        
			 
				 
					 
					    
					    
					 
				 
			          
                              
                                
                 
                

		 
  	 
 
