Copying a discussion forum, topic, thread, or post | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Copying a discussion forum, topic, thread, or post | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Discussions    »    Creating and managing discussions   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Copying a discussion forum, topic, thread, or post                       
        
        
       
        
     
              
	Copying a forum
 

 
	Copying a forum creates a new forum with the same properties as the original. You can copy the forum’s topics and any pinned threads within those topics. Copying a forum does not copy any normal, unpinned threads inside the forum’s topics; however you can copy or move individual threads from existing topics once you create the new topics.
 

 
	  Copy a forum
 

  
		On the Discussions List page, click    Copy  from the More Actions button.
	 
	 
		Click    Copy a Forum .
	 
	 
		Select the forum you want to copy from the  Forum to Copy  drop-down list.
	 
	 
		In the  New Forum Title  field, enter a name for the copied forum.
	 
	 
		Select your Copy Options:
		  
				 Copy topics  Select this option to copy all of the topics inside the forum; this creates new topics with the same titles and properties as the existing ones.
			 
			 
				 Copy pinned threads  If you choose to copy topics, select this option to copy any pinned threads within those topics. If you use pinned threads to post information about a topic (rules, evaluation criteria, etc.), you should copy these threads along with the topics.
			 
		  
	 
		Click  Copy . See  Creating discussion forums and topics  for information on how to customize the settings of your copied forum.
	 
  
	Copying a topic
 

 
	Copying a topic creates a new topic with the same properties as the original. Links to grade items or competency activities are not copied. You can choose to copy pinned threads along with the topic. Copying a topic does not copy any normal, unpinned threads inside the topic; however you can copy or move individual threads from inside the existing topic after you create the new topic. See  Copying or moving a discussion thread or post  for more information.
 

 
	If you want to copy topics into another course offering, see  Copying course components between org units . If you want to move a topic from one forum to another, click    Edit   Topic  from the context menu of the topic you want to move, then select the desired forum from the  Forum  drop-down list in the  Properties  tab.
 

 
	Copy a topic
 

  
		On the Discussions List page, click    Copy  from the More Actions button.
	 
	 
		Click    Copy a Topic .
	 
	 
		Select the forum that contains the topic you want to copy from the  Forum to Copy  drop-down list.
	 
	 
		Select the  Topic to Copy  from the drop-down list.
	 
	 
		In the  New Topic Title  field, enter a name for the copied topic.
	 
	 
		In the  Copy Destination  list, select the forum you want to copy the topic to. If you select multiple forums, each forum creates a copy of the topic.
	 
	 
		Select the  Copy pinned threads  check box if you want to copy pinned threads into the new topic. Select this if you use pinned threads to provide information and instructions about the topic.
	 
	 
		Click  Copy . See  Creating discussion forums and topics  for information on how to customize the settings of your copied topic.
	 
  
	  Copying or moving a discussion thread or post
 

 
	Discussion threads and posts can be moved to topics other than the original topic they are posted to. If you think a thread or post belongs in a different topic, you can move it by copying it and choosing the option to delete the original thread or post. If a thread or post is applicable to more than one topic, you can copy it into as many other topics as you want.
 

 
	Copy or move a thread
 

  
		Click    Copy Thread  from the thread's context menu.
	 
	 
		Select where you want to copy the thread to in the  Destination Forum  and  Destination Topic  drop-down lists.
	 
	 
		Select addition Copy Options:
		  
				Select the  Copy replies  check box to copy replies to the thread.
			 
			 
				Select the  Delete original post after copy completes  check box to delete the original post in the thread after the copy completes.
				 
					 Important  If you choose to delete the original thread post and do not choose to copy its replies, the system will delete any replies made to the thread.
				 
			 
		  
	 
		Click  Copy .
	 
  
	  Copy or move a post
 

  
		Click    Copy Post  from the post's context menu.
	 
	 
		Select where you want to copy the post to in the  Destination Forum  and  Destination Topic  drop-down lists.
	 
	 
		Select addition Copy Options:
		  
				Select the  Copy replies  check box to copy replies to the thread.
			 
			 
				Select the  Delete original post after copy completes  check box to delete the original post after the copy completes.
				 
					 Important  If you choose to delete the original post and do not choose to copy its replies, the system will delete any replies made to the post.
				 
			 
		  
	 
		Click  Copy .
	 
	  See also 
   Creating discussion forums and topic  
	  Copying course components between org units  
      Audience:     Instructor       

    
           

                   ‹ Editing a discussion forum or topic 
        
                   up 
        
                   Deleting discussion forums, topics, threads, and posts › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Discussions  
  
      Participating in discussions    Following discussions    Creating and managing discussions    Creating discussion forums and topics    Discussion forum and topic restrictions    Topic assessment    Topic objectives    Editing a discussion forum or topic    Copying a discussion forum, topic, thread, or post    Deleting discussion forums, topics, threads, and posts    Reordering discussion forums and topics    Restoring a deleted discussion forum, topic, thread, or post      Monitoring discussions    
                  
           
         

       
     

    
    
    
   
 
   
 
