President’s New Web Site Goes Live &#8211; The University of Memphis President&#039;s Blog   


 

 
 
 
 


 
 
 
 

 



 President’s New Web Site Goes Live    The University of Memphis President s Blog 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 



 
 
  
 
 
 

 
 
 
 
			

         
 		
		



 
 
 
 

 

 


  Skip to content  




 

	
	 

		
		 

			
			 

				
						  The University of Memphis President s Blog  

					
			  

		  

		
		 

			 Primary Menu 
			 Menu 

			
			 
				 
					 
				 
					 Search for: 
					 
				 
				 
			 				 
			 

			 

			  
  Home    About the President  
  

		  

		 

		
		
			
				 

			
		
		 

		
	  

	
	 
	 

		
		 

			
			
	
	 

		
		
		 

			
			 President’s New Web Site Goes Live 
			
			 

				   Author   Jeanine Hornish Rakow     Published on    October 2, 2014  October 1, 2014     Leave a comment  
			  

			
		  

		
		 

			 My home page on the University of Memphis website has a great new look, launched this week. You can find it at  http://memphis.edu/presweb/ . 
 I invite you to take a look at it. The focus on the web page is squarely on student success (recruitment, retention, completion), community engagement/partnerships, and advancement of our research mission.  Given the inaugural year, my welcome message emphasizes the upcoming “Year of Service” where students, faculty, staff and alums hope to donate at least 500,000 hours of service to their communities in Memphis, Jackson and beyond. 
 Users are now able to easily find the tools and information online that will move them forward in their college career from admission to graduation. A new Dashboard tab provides instant access to relevant data and supports the University’s determination to share the metrics that drive decision-making across all domains, divisions and units. 
 Prospective and current students, faculty and staff should find the site more seamless to navigate, thanks to the succinct menu bar across the center of the page. Frequently used pages are listed on the bottom portion of every web page. The prominently displayed Stay Connected window provides links to Twitter, Facebook, Instagram and the President’s Blog sites. 
 One noticeable difference happens when resizing the webpage’s window. As part of the University’s overall website redesign, the page utilizes responsive design. This means that navigation to the most used resources will be easier, and the website will automatically adjust to the screen size of most mobile devices. 
 “The goal is to make it easier for you to stay connected to the University of Memphis, and a comprehensive website redesign project will better reflect our engagement with and service to Memphis and the region,” explains Ellen Watson,  vice provost for Information Technology and chief information officer  .   Another example of the redesign can be found in a newly launched University Libraries’ homepage at  http://www.memphis.edu/libraries/ . 
 And there is much more ahead. The University has engaged Second to Nunn Design (S2N Design), which will oversee the creation of a comprehensive redesign project in the coming months, says Watson. “We expect the new University website to launch near the end of fall semester, but it will take time for all of the 16,000+ web pages to be converted to the new design.” 
 In the meantime, please look in at my website regularly to see how we are doing and to keep up with our progress. I think you will be impressed by it. It is one of the many ways to stay connected at U of M. 
 Go Tigers! 
 M. David Rudd, President 

		  

		
		 

			  Published on    October 2, 2014  October 1, 2014      Author   Jeanine Hornish Rakow   
			
		  

		
	  

	
				
	 
		 Post navigation 
		    Previous article:  Research Expenditures and Applications Up      Next article:  SACSOC  Re-accreditation Report Filed    
	 
				

 

	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  


			
			
		  

		
	  


	
		
		
		 

		 Main Sidebar 

			
			  
				 
					 Search for: 
					 
				 
				 
			  		 		 Recent Posts 		 
					 
				 UofM Quality Indicator Score 
						 
					 
				 University of Memphis Research Foundation Announces Grand Opening of Student-Operated FedEx Call Center 
						 
					 
				 Pause to Grieve 
						 
					 
				 University of Memphis Magazine: Fall 2017 
						 
					 
				 Campus Update 
						 
				 
		 		  Recent Comments      Archives 		 
			  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  July 2014  
	  June 2014  
	  May 2014  
	  April 2014  
		 
		   Categories 		 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 
			
		  

		
		  

	
	
	 

		
		 Footer Content 

		 
		
			
				
				
								
			
		  
	
		 

			
			
			Using  Tiny Framework     
			
			   Log in  

		  
		
		 

			
			

 

	 Social Links Menu 

	      i class= fa fa-twitter   /i    
  
  

			
		  

		
	  

	
  


        

        









		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 
 