MMR &amp; Varicella - Student Health Center - University of Memphis    










 
 
 
     



 
    
    
    MMR   Varicella - 
      	Student Health Center
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Student Health Center
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Emergency  
                          Immunization  
                          Clinics  
                          Policies  
                          Tiger Health 101  
                      
                      
                         
                            Immunization   
                            
                               
                                  
                                   Documentation Upload  
                                   Certificate of Immunization  
                                   Exemption  
                                   Flu Shots  
                                   Health Prerequisite Summary  
                                   Hepatitis B - Meningitis  
                                   Hepatitis B - Meningitis Form (under 18 only)  
                                   MMR   Varicella  
                                   TB Skin Test  
                                   Vaccination Availability  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Student Health Center
                              	  
                          
                              	Immunization
                              	  
                         MMR   Varicella 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      MMR   Varicella 
                     
                       NOTE: You may upload immunization documentation at   https://umwa.memphis.edu/immunizations  
                     
                      Immunizations Required Under Tennessee State Law 
                     
                      All students registering as full-time students at the University of Memphis must proof
                        of immunizations. Documentation of 2 MMR immunizations or proof of immunity for Measles,
                        Mumps, and Rubella and documentation of 2 Varicella immunizations or proof of immunity
                        for Varicella (Chicken Pox) are required. Acceptable documentation and special notes
                        concerning these requirements are listed below. Photocopies and faxed documentation
                        are acceptable as originals cannot be returned.
                      
                     
                      Meningitis Immunization Requirement for New Students Living In Any University of Memphis
                        Residence
                      
                     
                      The State of Tennessee requires all new students under the age of 22 who will be living
                        in a University of Memphis residence to be immunized against meningococcal disease
                        on or after their 16th birthday and provide proof of receiving this immunization before
                        moving into their residence. If this documentation is not provided, students will
                        not be allowed to move into their residence.
                      
                     
                       Acceptable documents are:  
                     
                      
                        
                         The Certificate of Immunization form completed and signed by health care provider 
                        
                         
                           
                             Certificate of Immunization Form  
                           
                         
                        
                         A copy of an official immunization card 
                        
                         Shot record from your local Public Health Department 
                        
                         Military form DD214 
                        
                         Active Military – ID must be provided 
                        
                         Official documentation from a prior college or university showing immunization dates 
                        
                         Proof of Immunity 
                        
                         
                           
                            A  positive  result for all three components of the MMR (Measles, Mumps, Rubella) Titer test
                            
                           
                            A  positive  result for the Varicella Zoster IgG (Chicken Pox) test
                            
                           
                            
                              
                               
                                 
                                  Or if diagnosed with the disease of Chicken Pox or Shingles, the student must provide
                                    documentation from a healthcare provider (i.e. physician, nurse practitioner, etc)
                                    confirming when the student had the disease 
                                  
                                 
                               
                              
                            
                           
                         
                        
                      
                     
                             MMR - Special Notes   
                     
                      
                        
                          Graduate students  who graduated from a  Tennessee  high school in May 1999 or after may send a copy of their high school diploma or
                           documentation of 2 MMR immunizations
                         
                        
                         Students who graduated from a   Tennessee  high school between May 1979 and May 1998, must provide documentation of  ONE  MMR immunization given after their graduation date or documentation of 2  MMR  immunizations
                         
                        
                      
                     
                        The following students are not required to provide MMR documentation:   
                     
                      
                        
                         If you were born before 1957 
                        
                         Part-time student (If status changes to full-time, documentation must be provided.) 
                        
                          Undergraduate  student who graduated from a  Tennessee  high school in May 1999 or thereafter
                         
                        
                      
                     
                           Varicella/Chicken Pox - Special Notes  
                     
                      
                        
                         Students who graduated from a   Tennessee  high school between May 1999 and May 2016, must provide documentation of  ONE   Varicella  immunization given after their graduation date or documentation of 2  Varicella  immunizations
                         
                        
                      
                     
                        The following students are not required to provide Varicella documentation:  
                     
                      
                        
                         If you were born before January 1, 1980 
                        
                         Part-time student (If status changes to full-time, documentation must be provided.) 
                        
                      
                     
                          Meningitis – Special Notes  
                     
                      
                        
                         Before moving into their residence, new students under the age of 22 and planning
                           to live in a University of Memphis residence must provide proof of receiving the meningitis
                           immunization on or after their 16th birthday.  If this documentation is not provided
                           before move-in day, students may not be able to move into their residence.  
                         
                        
                      
                     
                       If documentation is not in English,  the student must have the documentation translated into English by an official translator
                        or simply ask their health care provider to complete the Certificate of Immunization
                        Form.  When the form is signed by a provider from outside the US, the form must also
                        be stamped with the organizations stamp.         
                     
                       Documentation may be uploaded, faxed, mailed or brought to Student Health Services.
                              
                     
                       Upload immunization documents at:   https://umwa.memphis.edu/immunizations  
                     
                       Our Mailing Address:  Student Health Services 200 Hudson Health Center Memphis, TN 38152
                      
                     
                       Our fax numbers:  901.678.3124 901.678.1359
                      
                     
                        Not able to locate MMR or Varicella documentation?    Your options are:  
                     
                       Have the test for immunity  
                     
                      
                        
                         If any component of the MMR Titer test is not positive, BOTH immunizations must be
                           taken again
                         
                        
                         If the Varicella Zoster test is not positive, BOTH immunizations must be taken again 
                        
                      
                     
                       Have both immunizations again  
                     
                      
                        
                         Because the immunizations must be given 30 days apart, after the student has their
                           first immunization, a temporary 30 day waiver will be given so the student can register
                           for full-time credit hours.
                         
                        
                      
                     
                       Vaccines are available from various organizations located in the Memphis community .  View a   partial list of immunization locations  .
                      
                     
                         Note:     Exemptions may be requested if the student has an allergy to a vaccine component,
                        has a valid medical condition which contraindicates receiving the vaccine, or has
                        religious tenets prohibiting vaccinations. 
                      
                     
                      When immunizations are medically contraindicated, a physician must provide a signed
                        written statement indicating which immunizations are contraindicated and why the immunizations
                        are contraindicated. The letter may be faxed to our office at 901.678.3124 or 901.678.1359.
                      
                     
                      Religious exemptions may be requested. An original signed and notarized statement,
                        affirmed under penalties of perjury that the vaccination conflicts with the religious
                        tenets and practices of the student must be submitted to Student Health Center.  
                      
                     
                      If an exempted student contracts Measles, Mumps, Rubella, or Varicella, or if a Measles,
                        Mumps, Rubella, or Varicella outbreak occurs, it will be the student’s responsibility
                        to remain off campus until a physician gives written permission for the student to
                        return to campus.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Immunization 
                         
                            
                               
                                Documentation Upload  
                                Certificate of Immunization  
                                Exemption  
                                Flu Shots  
                                Health Prerequisite Summary  
                                Hepatitis B - Meningitis  
                                Hepatitis B - Meningitis Form (under 18 only)  
                                MMR   Varicella  
                                TB Skin Test  
                                Vaccination Availability  
                            
                         
                      
                      
                      
                         
                            
                                Upload Immunization Documentation  
                               Upload state mandated immunizations documentation to meet certain health requirements. 
                            
                            
                                Family Planning Clinic  
                               Service provided by the Memphis and Shelby County Public Health Department. 
                            
                            
                                Health Education Resources  
                               Useful links to help bump up your health IQ! 
                            
                            
                                Contact Us  
                               Main contact information and hours of operation. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/7/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


