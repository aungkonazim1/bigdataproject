Taking a quiz | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Taking a quiz | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Quizzes    »    Quizzes basics   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Taking a quiz                       
        
        
       
          
     
           Printer-friendly version        
		On the Quiz List page, click on the quiz you want to take.
	 
	 
		Read the instructions and details for the quiz, and when you're ready, click  Start Quiz! .
	 
	 
		Answer each question. You can see which questions you have saved answers to in the Questions section of the quiz's left panel. You can also click the question number in the quiz's left panel to go back to the question.
		 
			 Note We recommend you click the corresponding  Save  button after answering a question. You can also click  Save All Responses  if you are not finished answering all questions on a single page, or if you are working on a time-consuming question and need to save all previous responses.
		 
	 
	 
		Click  Next Page  or  Previous Page  to navigate between pages.
	 
	 
		Click  Go to Submit Quiz  when you are ready to submit.
		 
			 Note If you try to submit a quiz with unanswered questions, you will see a warning at the top of the Submit Quiz page informing you about unanswered questions. Click each link beneath the warning to return to each unanswered question.
		 
	 
	 
		After you attempt to answer all quiz questions, click  Submit Quiz .
	 
  
	 NOTE  Your organization may require you to use the Respondus LockDown Browser to take quizzes. You can click the Respondus LockDown Browser link in the Quiz Requirements section to download and install it for free. You can then launch your quiz. If you have installed it already, the Start Quiz page will open automatically in the Respondus LockDown Browser.
 

 
	Watching the time
 

 
	If your quiz has a time-limit you may be prompted or forced to submit your quiz. If the quiz is set to auto-submit, at the end of the designated time period only saved questions submit.
 

 
	 Note  Although you can start a quiz and navigate away from it at any time during the attempt, the timer for the quiz does not pause and continues to record your  Time Taken .
 
     Audience:    Learner      

    
           

                   ‹ Accessing Quizzes 
        
                   up 
        
                   Viewing your submission information and graded quizzes › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Quizzes  
  
      Quizzes basics    Accessing Quizzes    Taking a quiz    Viewing your submission information and graded quizzes    Enabling notifications in Quizzes    Adding a quiz to ePortfolio      Using Quizzes    Managing quiz questions and sections    Viewing quizzes    Quizzes and Question Library     
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
