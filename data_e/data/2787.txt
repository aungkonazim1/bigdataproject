Black Holes, Waves of Gravity &amp; Other Warped Ideas of Dr. Einstein - Department of Physics and Materials Science - University of Memphis    










 
 
 
     



 
    
    
    Black Holes, Waves of Gravity   Other Warped Ideas of Dr. Einstein - 
      	Department of Physics and Materials Science
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Physics and Materials Science
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Undergraduate  
                          Graduate  
                          People  
                          Research  
                          Resources  
                          News  
                          SPS  
                      
                      
                         
                            News   Events   
                            
                               
                                  
                                   News  
                                   Materials Day  
                                   Events  
                                   Endowed Lecture  
                                   Trivia Night  
                                   Open House  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of Physics and Materials Science
                              	  
                          
                              	News
                              	  
                         Black Holes, Waves of Gravity   Other Warped Ideas of Dr. Einstein 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Black Holes, Waves of Gravity   Other Warped Ideas of Dr. Einstein 
                     
                         Einstein's theories of relativity have had a major impact on everything from popular
                        culture to everyday life to basic science. Songs, plays and movies proclaim Einstein
                        as the symbol of genius, while users of GPS navigation devices unknowingly take account
                        of Einstein's relativistic warpage of time. Two of the crazier ideas that come from
                        Einstein's theories are Gravitational Waves and the Black Hole. Today, international
                        teams of scientists are on a quest to verify these ideas. Using large-scale detectors
                        on the ground they have detected Einstein's gravity waves and are using them to reveal
                        the hidden secrets of black holes. Gravitational waves provide a remarkable new tool
                        for ''listening'' to Einstein's cosmic symphony.
                      
                      
                          
                     
                      About Dr. Will 
                     
                        Clifford Martin Will  is Distinguished Professor of Physics at the University of Florida, Chercheur Associé
                        at the Institut d'Astrophysique de Paris, and the James S. McDonnell Professor of
                        Space Sciences Emeritus at Washington University in St. Louis. Born in Hamilton, Canada
                        in 1946, he received his pre-college and college education there, obtaining a B.Sc.
                        in Applied Mathematics and Theoretical Physics from McMaster University in 1968. In
                        1971, he obtained a Ph.D. in Physics from the California Institute of Technology in
                        Pasadena, and remained at Caltech for one year as an Instructor in Physics. He was
                        an Enrico Fermi Postdoctoral Fellow at the University of Chicago from 1972 to 1974.
                        From 1974 to 1981 he was Assistant Professor of Physics at Stanford University. From
                        1975 to 1979, he was an Alfred P. Sloan Foundation Fellow, and during 1978-79 a Mellon
                        Foundation Junior Faculty Fellow. In 1981 he joined Washington University in St. Louis
                        as Associate Professor, in 1985 became Professor of Physics, from 1991 - 1996 and
                        1997 - 2002 served as Chairman, and from 2005 - 2012 was McDonnell Professor.
                      
                     
                      He was elected to the US National Academy of Sciences in 2007. He was elected a Fellow
                        of the American Physical Society in 1989, of the American Academy of Arts and Sciences
                        in 2002, and of the International Society on General Relativity and Gravitation in
                        2016.
                      
                     
                      He has published over 200 scientific articles, including 21 major review articles,
                        29 popular or semi-popular articles, and three books, Theory and Experiment in Gravitational
                        Physics (Cambridge University Press, 1981; 2nd Edition, 1993), Was Einstein Right?
                        (Basic Books, 1986; 2nd Edition, 1993), and Gravity: Newtonian, post-Newtonian, Relativistic,
                        with Eric Poisson (Cambridge University Press, 2014). Was Einstein Right? won the
                        1987 American Institute of Physics Science Writing Award, was selected one of the
                        200 best books for 1986 by the New York Times Book Review, and has undergone translation
                        into French, German, Italian, Japanese, Portuguese, Spanish, Korean, Greek, Persian,
                        and Chinese.
                      
                     
                      In recognition of his theoretical work related to the Hulse-Taylor Binary Pulsar,
                        he was an invited guest of the Nobel Foundation at the 1993 Nobel Prize Ceremonies
                        honoring discoverers J. Taylor and R. Hulse. During the academic year 1996-97, he
                        was awarded both a J. William Fulbright Fellowship and a John Simon Guggenheim Fellowship
                        for a sabbatical leave in Paris and Jerusalem. In 1996, he was named Distinguished
                        Alumnus in the Sciences by McMaster University. In 2004 he received the Fellows Award
                        of the St. Louis Academy of Sciences. In 2005, in celebration of the World Year of
                        Physics, he carried out a 4-week, 21-city National Public Lecture Tour of Canada,
                        sponsored by the Canadian Association of Physicists. In 2013 he was awarded the degree
                        Doctor of Science honoris causa by the University of Guelph, Canada.
                      
                     
                      His recent professional activities include: Editor-in-Chief of Classical and Quantum
                        Gravity from 2009; Chair, Division of Astrophysics, American Physical Society, 2012
                        to 2013; Member of Space Studies Board, National Academy of Sciences/National Research
                        Council, 2011 to 2015; President of the International Society on General Relativity
                        and Gravitation, 2004 to 2007, Deputy President from 2007-2010, and member of the
                        Governing Committee from 1995 to 2004; Chair of the Science Advisory Committee for
                        Gravity Probe-B (NASA) from 1998 to 2011; Divisional Associate Editor for Physical
                        Review D from 1999 to 2001; member of the National Academy of Sciences Committees
                        on Gravitational Physics from 1997 to 1999, Physics of the Universe from 2000 to 2002,
                        Beyond Einstein Program Assessment from 2006 to 2007, and ASTRO 2010 Decadal Survey
                        of Astronomy and Astrophysics (Cosmology and Fundamental Physics Science Panel) from
                        2009 to 2010.
                      
                     
                      His research interests are theoretical, encompassing the observational and astrophysical
                        implications of Einstein's general theory of relativity, including gravitational radiation,
                        black holes, cosmology, the physics of curved spacetime, and the theoretical interpretation
                        of experimental tests of general relativity. Website:  http://www.phys.ufl.edu/~cmw/  
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         News   Events 
                         
                            
                               
                                News  
                                Materials Day  
                                Events  
                                Endowed Lecture  
                                Trivia Night  
                                Open House  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now  
                               Now accepting applications for the Undergraduate and Graduate Programs, MS in Physics
                                 and Materials Science and PhD in Engineering Physics!
                               
                            
                            
                                Contact Us  
                               Main office location and numbers, undergraduate and graduate advising, and Alumni
                                 information update form
                               
                            
                            
                                Seminars   Events  
                               The department presents weekly seminars during the spring and fall semesters 
                            
                            
                                Careers in Physics and Materials Science  
                               Find out what opportunities are available to you with a degree in Physics and Materials
                                 Science
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


