WORKFORUM: University of Memphis Career Opportunities   
 
   
       
       WORKFORUM: University of Memphis Career Opportunities 
 
 

 
 
 
 
 
 
 
 
 

     

   
   
       
       
         
           Skip to Main Content 
         
         
           
             
   
     
       
         
           
         
       
       
         
          University of Memphis Career Opportunities  
       
     
   
 

           
           
             
   
     
       
         Toggle navigation 
          
          
          
       
     
     
       
    Home  
    Search Jobs  
      Create Account  
      Log In  
    Help  
 

       
             
               University Benefits 
             
             
               Equal Employment 
             
             
               Annual Security Report 
             
             
               Campus Map 
             
             
               About UofM 
             
       
     
   
 

             
               
                
                 
  Search Postings  (125) 
     
 
 View all open Postings below, or enter search criteria to narrow your search. 
    
   
   
     
       
         Keywords 
       
       
         
       
         
           Posted Within 
         
         
            Any time period 
 Last Day 
 Last Week 
 Last Month  
         
     
     
     
   
 

   
   
       
           
             Department 
           
           
             
                Academic Administration 
 Academic Affairs Finance 
 Academic Counseling Center 
 Academic Innovation   Support Services 
 Academic Technology Services 
 Accounting Office 
 Admissions 
 Adult and Commuter Student Services  
 Adult Services 
 Advanced Learning Center 
 Advancement Services 
 African   African American Studies 
 Air Force ROTC 
 Alumni and Constituent Relations 
 Alumni Programs and Special Events 
 Animal Care Facility 
 Anthropology 
 Architecture 
 Army ROTC 
 Art 
 Art Museum 
 Assoc Dean Education 
 Assoc VP Information Technology 
 Athletic Academic Services 
 Athletic Administration 
 Athletic Business Office 
 Athletic Committee 
 Athletic Communications 
 Athletic Director 
 Athletic External Affairs 
 Athletic Management Finance 
 Athletic Ticket Office 
 Auxiliary Services 
 AVP Admin Business Services 
 AVP Campus Planning Design 
 AVP Finance 
 AVP Physical Plant 
 AVP Student Affairs 
 Avron B. Fogelman Professional Development Center 
 Baseball 
 Basketball Men 
 Basketball Women 
 Benefits 
 Bioinformatics 
 Biology 
 Biomedical Engineering 
 Budget 
 Bureau Business Economic Research 
 Bursar 
 Business and Finance 
 Business Development 
 Business Information   Technology 
 Campus Recreation 
 Campus School 
 Career Services 
 Center for Academic Retention and Enrichment Services 
 Center for Community Health 
 Center for Counsel Learn Testing 
 Center for Research on Women (CROW) 
 Center Higher Education 
 Center Rehab Employment Research 
 Center Research Education Policy 
 CERI 
 Chemistry 
 Child Care Center 
 Chucalissa Museum 
 City   Regional Planning 
 Civil Engineering 
 COE Communicative Impaired 
 College of Arts and Sciences  
 College of Business Economics 
 College of Communication and Fine Arts 
 College of Education 
 College of Engineering 
 Communication 
 Communication Sciences and Disorders 
 Community Music School 
 Computer Science 
 Computer Systems Support 
 Conference and Event Services 
 Continuing Education 
 Counsel Education Psych Research 
 Criminal Justice 
 CRISTAL 
 Curriculum and Assessments 
 Dean of Students 
 Disability Resources for Students 
 Distance Education 
 Diversity Office 
 Earth Sciences 
 Economics 
 Educational Support Program 
 Electrical Computer Engineering 
 Engineering Technology 
 English 
 Enrollment Services 
 Enterprise Application Services 
 Enterprise Infrastructure Services 
 Environmental Health and Safety 
 Epidemiology   Biostatistics 
 Executive Assistant Student Affairs 
 Extended Programs 
 External Relations 
 Faculty Senate 
 Family and Consumer Sciences 
 FCBE Academic Administration 
 FCBE Academic Program 
 Fedex Institute of Technology 
 Feinstone Center 
 Finance Insurance Real Estate 
 Financial Aid Operations 
 Financial Planning 
 Finish Line Program 
 First Scholars 
 Food Services 
 Football 
 General Accounting 
 General Human Resources 
 General Payroll 
 General Plant 
 Golf Men 
 Golf Women 
 Government Affairs 
 Graduate Programs 
 Health Administration 
 Health Center 
 Health Sciences 
 Health Sport Sciences 
 History 
 Honors Program 
 Hooks Inst for Social Change 
 Hospitality and Resort Management 
 Hotel Conference Center 
 Human Resources 
 Innovation in Teaching   Learning 
 Institute for Intelligent Systems 
 Institutional Equity 
 Institutional Research 
 Instruction Curriculum Leadership 
 Integrated Microscopy Center 
 Intensive English for Internatls 
 Interdisciplinary Studies 
 Intermodal Freight Transportation Institute 
 Internal Audit 
 International Programs 
 International Studies 
 International Trade Center 
 Investment Banking Agreements 
 IT Planning and Budget 
 IT Security, ID Mgmt and Compliance 
 ITS Desktop and Smart Tech Support 
 ITS Service Desk 
 ITS Support Teaching and Learning 
 Journalism 
 Judaic Studies 
 Judicial Affairs 
 Keep TN Beautiful 
 Lambuth 
 Law Library 
 Leadership 
 Lipman School 
 Loewenberg College of Nursing 
 Management 
 Management Information Systems 
 Marketing and Communications 
 Marketing Supply Management 
 Mathematics 
 Mechanical Engineering 
 Multicultural Affairs 
 Navy ROTC 
 Network Services 
 Office for Institutional Equity 
 Office of President 
 Office of Student Conduct 
 Office of University Development 
 Olympic Sports 
 One Stop Shop 
 Parking 
 Payroll Office 
 Philosophy 
 Physical Plant 
 Physics and Materials Science 
 Police Services 
 Political Science 
 PP Business Operations 
 PP Crafts Trades 
 PP Custodial 
 PP Landscape 
 Principal Gifts 
 Procurement and Contract Services 
 Provost Office 
 Psychology 
 Public Administration 
 Regional Economic Development Ctr 
 Registrar Office 
 Residence Life 
 Revenue and Waivers 
 Rifle 
 RODP Admin 
 Scholarships 
 School of Accountancy 
 School of Health Studies 
 School of Law 
 School of Music 
 School of Public Health 
 School of Urban Affairs 
 Shared Services Center 
 Soccer Men 
 Soccer Women 
 Social   Behavioral Sciences 
 Social Work 
 Sociology 
 Softball 
 Space Planning 
 Sport   Leisure Management 
 Staff Senate 
 Stores Inventory 
 Student Financial Aid 
 Student Leadership 
 Student Success Programs 
 Teacher Education 
 Tennis Men 
 Tennis Women 
 Testing Center 
 Theatre and Dance  
 Tiger Copy and Graphics 
 Track Men 
 Track Women 
 Undergraduate Programs 
 University Admin Applications Svcs 
 University Center 
 University College 
 University College Programs 
 University College RODP 
 University Counsel 
 University District Support 
 University Libraries 
 University of Memphis Foundation 
 Various 
 Vice President Research 
 Vice Provost Assessment, IR, Report 
 Vice Provost Extended Programs 
 Volleyball 
 VP Advancement 
 VP Business and Finance 
 VP Communications, PR, Marketing 
 VP Information Technology 
 VP Research 
 VP Student Affairs 
 Weight Room Operations 
 Women and Gender Studies 
 Women Athletics and Compliance 
 World Languages   Literatures  
             
           
           
             Category 
           
           
             
                Staff (Hourly/Monthly) 
 Full-Time Faculty 
 Part-Time Faculty 
 Administrative Executive 
 Academic Executive 
 Athletic Contract 
 Temporary Employees  
             
           
       
       
           
             Campus Location 
           
           
             
                Main Campus (Memphis, TN) 
 Chucalissa Museum   Indian Village (Memphis, TN) 
 Collierville Campus (Collierville, TN) 
 Lambuth Campus (Jackson, TN) 
 Meeman Biological Field Station (Millington, TN) 
 Millington Center (Millington, TN) 
 Park Avenue Campus (Memphis, TN) 
 School of Law (Downtown Memphis) 
 May vary by position  
             
           
       
   
 

   
   
   Previous    1   2   3   4   5    Next   
 View Results  (125)  
  
 
   
       
   
     
       Closing Date 
     
     
       Category 
     
     
       Department 
     
     
       Hiring Range ↓
     
 


 
       
   
     
       
         
             Assistant Professor - Astrophysics 
         
       
         
            12/15/2017
         
         
            Full-Time Faculty
         
         
            Physics and Materials Science
         
         
            Commensurate with qualifications
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Assistant or Associate Professor 
         
       
         
            02/15/2018
         
         
            Full-Time Faculty
         
         
            Communication Sciences and Disorders
         
         
            Commensurate with experience and credentials
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Dean School of Communication Sciences and Disorders 
         
       
         
            
         
         
            Academic Executive
         
         
            Communication Sciences and Disorders
         
         
            Commensurate with experience
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Clinical Asst./Assoc. Professor 
         
       
         
            
         
         
            Full-Time Faculty
         
         
            Loewenberg College of Nursing
         
         
            Commensurate with experience
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Clinical Asst / Assoc. Professor 
         
       
         
            
         
         
            Full-Time Faculty
         
         
            Loewenberg College of Nursing
         
         
            Commensurate with experience
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Head Coach 
         
       
         
            
         
         
            Athletic Contract
         
         
            Volleyball
         
         
            Commensurate with experience
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Dean School of Public Health 
         
       
         
            
         
         
            Academic Executive
         
         
            School of Public Health
         
         
            Commensurate with experience
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Chief Financial Officer 
         
       
         
            
         
         
            Administrative Executive
         
         
            Business and Finance
         
         
            Commensurate with experience
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Director, Planning and Assessment for Business   Finance 
         
       
         
            
         
         
            Staff (Hourly/Monthly)
         
         
            VP Business and Finance
         
         
            Commensurate with experience
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Asst. / Assoc. Professor 
         
       
         
            
         
         
            Full-Time Faculty
         
         
            Loewenberg College of Nursing
         
         
            Commensurate with experience
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Assistant/Associate Professor 
         
       
         
            
         
         
            Full-Time Faculty
         
         
            Mechanical Engineering
         
         
            Commensurate with education and experience
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Chair of Excellence 
         
       
         
            
         
         
            Full-Time Faculty
         
         
            Biomedical Engineering
         
         
            Commensurate with education and experience
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Director, Procurement and Contract Services 
         
       
         
            
         
         
            Staff (Hourly/Monthly)
         
         
            Procurement and Contract Services
         
         
            $80,000 - $95,000 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Director of College Development II, College of Communications   Fine Arts 
         
       
         
            
         
         
            Staff (Hourly/Monthly)
         
         
            Office of University Development
         
         
            $77,000 - $81,500 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Senior Research Software Developer 
         
       
         
            12/04/2017
         
         
            Staff (Hourly/Monthly)
         
         
            College of Engineering
         
         
            $75,000- $85,000 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Network Systems Researcher 
         
       
         
            01/02/2018
         
         
            Staff (Hourly/Monthly)
         
         
            Computer Science
         
         
            $58,000 - $63,000 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Sr Internal Auditor 
         
       
         
            
         
         
            Staff (Hourly/Monthly)
         
         
            Internal Audit
         
         
            $55,000 to $65,000 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Systems Administrator I 
         
       
         
            
         
         
            Staff (Hourly/Monthly)
         
         
            Enterprise Infrastructure Services
         
         
            $55,000 - $68,000 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Instructional Technology Trainer 
         
       
         
            12/20/2017
         
         
            Staff (Hourly/Monthly)
         
         
            ITS Service Desk
         
         
            $51,500 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Service Desk Manager 
         
       
         
            12/13/2017
         
         
            Staff (Hourly/Monthly)
         
         
            ITS Service Desk
         
         
            $50,000 - $55,682 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             HRIS Analyst 
         
       
         
            
         
         
            Staff (Hourly/Monthly)
         
         
            Human Resources
         
         
            $50,000 - $55,000 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Sponsored Programs Coordinator 
         
       
         
            12/20/2017
         
         
            Staff (Hourly/Monthly)
         
         
            Vice President Research
         
         
            $50,000 - $53,000 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Systems Support Specialist II 
         
       
         
            12/18/2017
         
         
            Staff (Hourly/Monthly)
         
         
            Registrar Office
         
         
            $45,000 -$50,000 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Title IX Prevention Specialist 
         
       
         
            
         
         
            Staff (Hourly/Monthly)
         
         
            Office for Institutional Equity
         
         
            $45,000 - $55,000 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Business Officer I 
         
       
         
            12/21/2017
         
         
            Staff (Hourly/Monthly)
         
         
            Biology
         
         
            $45,000 - $47,000 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Assistant Registrar, Curriculum Data Management 
         
       
         
            12/11/2017
         
         
            Staff (Hourly/Monthly)
         
         
            Registrar Office
         
         
            $40,000 - $50,000 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Human Resources Associate, Benefits Administration 
         
       
         
            12/20/2017
         
         
            Staff (Hourly/Monthly)
         
         
            Human Resources
         
         
            $37,500 - $43,000 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Law Fellow 
         
       
         
            
         
         
            Staff (Hourly/Monthly)
         
         
            University Counsel
         
         
            $35,000 - $40,000 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Development Research Coordinator 
         
       
         
            
         
         
            Staff (Hourly/Monthly)
         
         
            Office of University Development
         
         
            $33,000 - $36,720 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Administrative Associate I 
         
       
         
            12/18/2017
         
         
            Staff (Hourly/Monthly)
         
         
            Electrical Computer Engineering
         
         
            $30,000- $35,000 per year
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   

 

   Previous    1   2   3   4   5    Next   

                  
               
                
             
           
           
             
   
     University of Memphis — Human Resources 
165 Administration Building, Memphis, TN 38152 
901.678.3573  workforce@memphis.edu  
 Follow us on Twitter  
      
      ©University of Memphis. All Rights Reserved. The University of Memphis is an Equal Opportunity/Affirmative Action university.  
   
 

           
         
       
         
 
        
  

  


    
      
    
     
   
 
