Panelists and Bios - MLK50 - University of Memphis    










 
 
 
     



 
    
    
    Panelists and Bios - 
      	MLK50
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			MLK50
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Events  
                          News  
                          Contact Us  
                      
                      
                         
                            Law Symposium   
                            
                               
                                  
                                   Schedule of Events  
                                   Panelists and Bios  
                                   Sponsors  
                                   Register Online  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	MLK50
                              	  
                          
                              	Law Symposium
                              	  
                         Panelists and Bios 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Panelists and Bios 
                     
                      MLK50 Symposium: Where Do We Go From Here? 
                     
                         Debo Adegbile – Partner, the law firm of WilmerHale.   
                     
                         Roy L. Austin – Partner, Harris, Wiltshire   Grannis LLP; former Deputy Assistant
                              to President Obama for the Office of Urban Affairs, Justice   Opportunity.   
                     
                        Cornell Brooks – Former President of the NAACP, 2014-2017.  
                     
                         Dorothy Brown – Professor of Law, Emory Law School.   
                     
                         Richard Hasen – Chancellor's Professor of Law and Political Science, University of
                              California – Irvine School of Law.   
                     
                        Sherrilyn Ifill - President and Director-Council of the NAACP Legal Defense and Educational
                           Fund, Inc.  
                     
                         Pamala Karlan – Kenneth and Harle Montgomery Professor of Public Interest Law, Co-Director,
                              Supreme Court Litigation Clinic, Stanford Law School.   
                     
                         Toussaint Losier – Assistant Professor, University of Amherst College of Humanities
                                Fine Arts, W.E.B. Du Bois Department of Afro-American Studies.   
                     
                         Tracey Maclin – Professor of Law   Joseph Lipsitt Faculty Research Scholar, Boston
                              University School of Law.   
                     
                         Dayna Matthews - William L. Matheson and Robert M. Morgenthau Distinguished Professor
                              of Law; F. Palmer Weber Research Professor of Civil Liberties and Human Rights, University
                              of Virginia Law School.   
                     
                         Tomiko Brown-Nagin - Daniel P.S. Paul Professor of Constitutional Law; Director, Charles
                              Hamilton Houston Institute for Race   Justice; Co-Director, Program in Law and History,
                              Harvard Law School; Professor of History, Faculty of Arts and Sciences, Harvard University.   
                     
                         Mark Osler - Professor and Robert and Marion Short Distinguished Chair in Law, University
                              of St. Thomas School of Law.   
                     
                         Dorothy Roberts - George A. Weiss University Professor of Law and Sociology and the
                              Raymond Pace and Sadie Tanner Mossell Alexander Professor of Civil Rights, University
                              of Pennsylvania School of Law.   
                     
                         Claude Steele – Professor of Psychology, University of California – Berkeley.   
                     
                         Beverly Tatum – President Emerita, Spelman College.    
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Law Symposium 
                         
                            
                               
                                Schedule of Events  
                                Panelists and Bios  
                                Sponsors  
                                Register Online  
                            
                         
                      
                      
                      
                         
                            
                                Law Symposium  
                               Learn more and register 
                            
                            
                                MLK50 Events  
                               Complete list of 50th Anniversary Events 
                            
                            
                                National Civil Rights Museum  
                               MLK 50 Yearlong Commemoration 
                            
                            
                                Contact Us  
                               Questions? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/7/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


