Information for Current Employees - University Benefits Administration - University of Memphis    










 
 
 
     



 
    
    
    Information for Current Employees - 
      	University Benefits Administration
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			University Benefits Administration
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Staff Directory  
                          Benefits Information  
                          Related Resources   Links  
                          Human Resources  
                      
                      
                         
                            Benefits Information   
                            
                               
                                   Prospective Employees  
                                   New Faculty  
                                   New Staff  
                                   Current Employees  
                                   Departing Employees  
                                   Retiring Employees  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	University Benefits Administration
                              	  
                          
                              	Benefits Information
                              	  
                         Information for Current Employees 
                      
                   
                   
                       
                      
                     		
                     
                     
                      Information for Current Employees 
                     
                      The information contained here will help you as a current employee understand the
                        benefits and services available to you.
                      
                     
                       Benefit Programs  
                     
                      
                        
                          401(k), 403(b), and 457  
                        
                          Educational Benefit Programs  
                        
                          Employee Assistance Program  
                        
                          Family Medical Leave Act  (FMLA)
                         
                        
                          Flexible Benefits Programs  
                        
                          Insurance Plans  
                        
                          Leaves of Absence  
                        
                          Retirement Plans  
                        
                          Worker's Compensation  
                        
                          Related Resources and Links  
                        
                          Questions/Comments    
                        
                      
                     
                       Employee Services  
                     
                      
                        
                          Campus Map  
                        
                          Campus Recreation Center  
                        
                          E-mail Account  
                        
                          Employee Discounts  
                        
                          Parking  
                        
                          University Holidays  
                        
                          University ID cards  
                        
                          Employee Self Service  (via the myMemphis portal)
                         
                        
                          Support for Nursing Mothers    
                        
                      
                     
                       Annual Events   
                     
                      
                        
                          Annual Enrollment Transfer Period  
                        
                         Benefits Fair 
                        
                      
                     
                     	
                      
                   
                
                
                   
                      
                         Benefits Information 
                         
                            
                                Prospective Employees  
                                New Faculty  
                                New Staff  
                                Current Employees  
                                Departing Employees  
                                Retiring Employees  
                            
                         
                      
                      
                      
                         
                            
                                Tiger Perks  
                               Exclusive discounts offered to UofM employees 
                            
                            
                                Affordable Care Act  
                               Important information related to the Affordable Care Act 
                            
                            
                                Forms  
                               All of B F's forms in one place 
                            
                            
                                Business   Finance  
                               The Division of Business   Finance at the UofM 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


