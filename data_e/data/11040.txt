News - Women's and Gender Studies - LibGuides at University of Memphis Libraries   
 
	 
         
         
         
        
		 News - Women's and Gender Studies - LibGuides at University of Memphis Libraries 
		 
		 
 
 
 
 
 
 
 
 
 
 
 

		 
		 
		 
		 
		 
		 
         
         
				 
				 
             
         
        
        
            
		
		 
		
		 
		 
		
		 
		
		
	   
		         
        	 
     
	 
		 Skip to main content                  
                 
         
  		 
             
                
			 
				
					 
						 University of Memphis Libraries
						 
					 
					 
						 LibGuides
						 
					 
					 
						 Women's and Gender Studies
						 
					 
					 News
					 
			              
			 
				 
					
                 
                     
                         
                             
                             Search this Guide  
                             
                                 Search 
                             
                         
                     
                 				 
							 
             
                 Women's and Gender Studies 
                 
                     This guide will help you with your WGS research. 
                 
             
         
         
          
         
             
                 
                     
                         
                             
                 
                     
                         Women's History Month 
                        
                     
                 
                 
                     
                         Background Sources 
                        
                     
                 
                 
                     
                         Find Books 
                        
                     
                 
                 
                     
                         Find Articles 
                        
                     
                 
                 
                     
                         Find Journals 
                        
                     
                 
                 
                     
                         Primary Sources 
                        
                     
                 
                 
                     
                         News 
                        
                     
                 
                 
                     
                         Writing Help 
                        
                     
                 
                 
                     
                         Videos and Media 
                        
                     
                  
                            
                 
                     
                         
                        
                         
                     
                                          
                     
                 
                 
                     
                         
                             
     
                  
					 
						 
							 To the Contrary
                                 
							 
								 
									
			 
				      Watch episodes online    
  "To The Contrary (TTC,) public television's successful all-female news analysis series...was launched in 1992, dubbed the 'Year of the Woman in American Politics' as women's representation in Congress blossomed.   
  "With women in the forefront of politics and on the cutting edge of national agendas, To The Contrary continues as an essential, timely forum for women to discuss national and international issues and policies" (About the Show: http://www.pbs.org/ttc/about_show.html).  
       
		    
								 
								
							 
						 
					   
					 
						 
							 Current Events
                                 
							 
								 
									
			 
				  Find out about current events relating to women and feminism.     
		     
                          
                        
			   National Organization for Women "Hot Topics"  
				  NOW was established in 1966 and has around 500,000 members. 
			  
                         
                        
			   Feminist Majority Foundation's "Feminist News"  
				  Founded in 1987, FMF is concerned with many aspects of social and political gender equality in the U.S. 
			  
                         
                        
			   Ms. Magazine's Feminist Daily Wire  
				  Ms. is a popular feminist magazine, first published in 1971. 
			  
                         
                        
			   The Guardians's WorldNews>>Feminism  
				  Articles in the Guardian newspaper (London) about women, feminism, and women's rights 
			  
                         
                        
			   The Feminist Wire  
				  Provides socio-political commentary from feminist, anti-racist, and anti-imperialist perspectives  on current events, politics, and popular culture 
			  
                         
                        
			   Women's E-News.org  
				  Major news stories from a woman's angle 
			  
                         
                        
			   New York Times "Times Topics: Feminism"  
				  Articles published in the NY Times from 1980 to the present about feminism and women's rights 
			  
                         
                        
			   Women News Network  
				  Look here for reports on topics you won't find in the major news outlets 
			  
                         
                        
			   Collection of Electronic Discussion Forums  
				  Gender-Related Electronic Forums compiled by Joan Korenman.  You can subscribe to these listservs to follow or participate in discussions about a wide variety of Women's & Gender Studies topics and issues. 
			  
                        
                             
                             
								 
								
							 
						 
					   
					 
						 
							 Search for video clips by keyword
                                 
							 
								 
									
			 
				  Looking for video clips?  
		     
                          
                        
        			 
        				 PBS Video   Search by keyword in the "I'd like to watch" search box.  Recommended searches: GENDER, WOMEN, FEMINISM, GIRLS 
        			  
                         
                        
        			 
        				 TED Talks   Browse the list of topics and tags to find short videos on a huge variety of subjects. 
        			  
                        
                             
                              
                          
                        
			   Feminist Frequency: Conversations with Pop Culture  
				  "Anita Sarkeesian is a feminist pop culture media critic who produces an ongoing web series of video commentaries from a feminist/fangirl perspective" (Feminist Frequency: About). 
			  
                         
                        
			   Feministing: Video  
				  Feministing is a blog for feminist conversation, meant  to connect feminists and their allies to promote activism for social justice issues with an emphasis on intersectionality. 
			  
                        
                             
                             
								 
								
							 
						 
					   
					 
						 
							 Newspapers and magazines
                                 
							 
								 
									 
                          
                        
        			 
        				 LexisNexis News   Provides full access to newspapers, magazines, and broadcast news sources from around the world.  Includes video and transcripts. 
        			  
                         
                        
        			 
        				 General OneFile   Multidisiplinary source for news and periodical articles on a wide range of topics. Subjects included: business, computers, current events, economics, education, environmental issues, health care, hobbies, humanities, law, literature and art, politics, science, social science, sports, technology, and many general interest topics. Coverage: varies.;"Provided by the Tennessee State Library & Archives and TennShare; part of the Tennessee Electronic Library" 
        			  
                         
                        
        			 
        				 National Newspaper Index   Indexes America's top five newspapers: The New York Times, The Wall Street Journal, The Christian Science Monitor, Los Angeles Times, and The Washington Post. 
        			  
                        
                             
                              
                          
                        
			   Commercial Appeal  
				  Get full text access to the past ten years of Memphis' Commercial Appeal newspaper. 
			  
                        
                             
                             
								 
								
							 
						 
					  
         
     
                          
                     
                    
                 
                     
                         Previous:  Primary Sources 
                     
                     
                      Next:  Writing Help    
                     
                                  
             
         
         
         
             
                 
                     
                         Last Updated:   Mar 31, 2017 11:00 AM                      
                     
                         URL:   http://libguides.memphis.edu/WGS                      
                     
                    	    Print Page                      	
                     
                 
                     Login to LibApps                  
             
             
                 
                     Report a problem  
                 
                 
                    
                     Subjects:  
                      Social Sciences ,  Women's & Gender Studies  
                                     
                 
                                     
             
         
         
        
			 
				 
					 
					    
					    
					 
				 
			          
                              
                                
                 
                

		 
  	 
 
