Importing and exporting metadata | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Importing and exporting metadata | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Metadata    »    Adding and removing metadata   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Importing and exporting metadata                       
        
        
       
        
     
              
	  Importing metadata from a file
 

 
	You can import metadata from an XML file, including a file created by exporting metadata from another resource.
 

 
	 Note  Importing metadata clears all existing metadata, including metadata not visible in the current view.
 

 
	Import metadata from a file
 

  
		On the Edit Metadata page for the module or topic you want to modify, click  Import Metadata .
	 
	 
		Click    Import XML File .
	 
	 
		Select whether the file is from your course or stored on your computer.
		  
				If the file is from your course, click  Select a File  to locate the file.
			 
			 
				If your file is stored on your computer, click  Browse  to locate the file.
			 
		  
	 
		Click  Import .
	 
  
	  Exporting a resource’s metadata
 

  
		On the Edit Metadata page for the module or topic you want to modify, click  Export Metadata .
	 
	 
		Enter a file name for the export file.
	 
	 
		Select the format you want to export metadata from.
	 
	 
		Click  Export .
	 
      Audience:     Instructor       

    
           

                   ‹ Copying metadata from a course, module, or topic 
        
                   up 
        
                   Clearing metadata from a resource › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Metadata  
  
      Metadata basics    Adding and removing metadata    Adding metadata to a resource    Copying metadata from a course, module, or topic    Importing and exporting metadata    Clearing metadata from a resource    Removing metadata from a resource      
                  
           
         

       
     

    
    
    
   
 
   
 
