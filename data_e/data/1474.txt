DRS Staff &amp; Contact Information - Disability Resources for Students - University of Memphis    










 
 
 
     



 
    
    
    DRS Staff   Contact Information - 
      	Disability Resources for Students
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Disability Resources for Students
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Access  
                          Accommodations  
                          Technology  
                          Policies  
                          Career Programs  
                          Faculty  
                      
                      
                         
                            About DRS   
                            
                               
                                  
                                   DRS Office  
                                   DRS Staff  
                                   DRS FAQs  
                                   Important Terms for DRS  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Disability Resources for Students
                              	  
                          
                              	About DRS
                              	  
                         DRS Staff   Contact Information 
                      
                   
                   
                       
                      
                     		
                     
                     
                      Staff   Contact Information 
                     
                       Disability Resources for Students  110 Wilder Tower Memphis, TN 38152-3520 Phone: 901-678-2880 V/TTY FAX: 901-678-3070
                      
                     
                        Dr. Justin Lawhead , Interim Director, E-mail:  drs@memphis.edu  
                     
                       Jennifer Murchison , Assistant Director and LD-ADHD and Autism Spectrum Disorders Coordinator, E-mail:
                         jmurchis@memphis.edu  
                     
                       Amanda Rodino , Disability Services Coordinator, (for students with mobility, sensory, psychiatric,
                        chronic health and TBI disabilities) and Assistive Technology Coordinator, E-mail:
                         arodino@memphis.edu     
                     
                       Steve Shaver , Educational Specialist, E-mail:  slshaver@memphis.edu  
                     
                       Nelda Scruggs , Math Tutor/Educational Assistant, E-mail:  nscruggs@memphis.edu  
                     
                       April Burns , Testing Coordinator, E-mail:  amburns1@memphis.edu  
                     
                       Nicole Hoyle , Administrative Associate, E-mail:  nhoyle@memphis.edu  
                     
                       Jennifer Yeoward , Graduate Assistant, Career Coach, Email:  jyeoward@memphis.edu  
                     
                       Madeline Rardin,  Graduate Assistant, Academic Coach, Email:  mrardin@memphis.edu     
                     
                       Marguerite Spiotta , Graduate Assistant, Front Desk, Email:  mspiotta@memphis.edu     
                     
                       Mary Elizabeth Goodman , Graduate Assistant, Academic Coach, Email:  mgodman1@memphis.edu     
                     
                       Kendra Vaughn , Graduate Assistant, Assistive Technology, Email:  kjvaughn@memphis.edu  
                     
                       Alicia Whetstone , Graduate Assistant, Notetaker Coordinator, Email:  whtstone@memphis.edu  
                     
                     	
                      
                   
                
                
                   
                      
                         About DRS 
                         
                            
                               
                                DRS Office  
                                DRS Staff  
                                DRS FAQs  
                                Important Terms for DRS  
                            
                         
                      
                      
                      
                         
                            
                                DRS Announcements  
                               Learn what's new at DRS and get important reminders too. 
                            
                            
                                Thanks to Our Volunteer Note-takers!  
                               See how DRS notetakers help our students. 
                            
                            
                                Student and Faculty Awards  
                               Read about the DRS Student and Faculty Awards. 
                            
                            
                                Contact DRS  
                               Information about our office hours and how to reach us. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/21/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


