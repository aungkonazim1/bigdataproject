Environmental Nutrition - SHS - University of Memphis    










 
 
 
     



 
    
    
    Environmental Nutrition - 
      	SHS
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			School of Health Studies
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          Students  
                          Faculty  
                          Research  
                          Centers  
                          News  
                          Camps  
                      
                      
                         
                            SHS Programs   
                            
                               
                                  
                                   Undergraduate  
                                         Dietetics  
                                         Exercise, Sport and Movement Sciences  
                                         Health Sciences  
                                         Healthcare Leadership  
                                         Physical Education Teacher Education  
                                     
                                  
                                   Graduate  
                                        
                                         Exercise, Sport and Movement Science  
                                        
                                         Health Promotion  
                                        
                                         Physical Education Teacher Education  
                                        
                                         Clinical Nutrition  
                                        
                                         Environmental Nutrition  
                                        
                                         Nutrition Science  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	SHS
                              	  
                          
                              	Programs
                              	  
                         Environmental Nutrition 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Environmental Nutrition (MS) 
                     
                      The master's degree in Environmental Nutrition is a fully online, 33-credit hour program
                        that emphasizes nutritional issues through an environmental lens. The program examines
                        the relationships between food, nutrition, and the environment. The impact of nutritional
                        practices on the environment and the impact of the environment on nutritional practices
                        are the program focuses.
                      
                     
                      Relevant research and theoretical foundations towards best practices guides the curriculum.
                        Issues related to food sustainability, traditional food practices, food justice, and
                        alternative and complementary approaches to nutrition are examined. Students must
                        complete an applied project and internship grounded in the practical application of
                        sustainable nutrition practices.
                      
                     
                       Background of Candidates  
                     
                      As a generalist program, Environmental Nutrition students have a variety of undergraduate
                        degrees including dietetics, nursing, health promotion, medicine, exercise science,
                        food science, biology, anthropology, psychology, and others. The program provides an advanced degree option for members of the work force with
                        food and nutrition responsibilities employed by schools, industry, health care, government,
                        and non-profit organizations. We invite applications from students who wish to emphasize applied work in sustainable
                        food and nutrition practices. Applicants must have a minimum of a bachelor's degree
                        with some background in the biological sciences and nutrition. Applicants will be
                        selected for admission to the program on the basis of grade point average, narrative
                        statement and personal goals, and letters of reference. Graduate record examination
                        (GRE) within the past five years may be submitted to support the application, but
                        is not required.
                      
                     
                       Registered Dietitians/Nutritionists (RD/RDN) may receive 6 credit hours of Experiential
                           Learning Credit (ELC).  
                     
                       Contact  Dr. Robin Roach  for additional information.  
                     
                       major   
                     
                       Nutrition   
                     
                      
                        
                         Concentration: Environmental Nutrition (online) 
                        
                         Degree: MS 
                        
                          Degree Sheet  
                        
                      
                     
                      *When completing the  MS application , your major will be Nutrition with a concentration in Environmental Nutrition.
                      
                     
                      Careers 
                     
                      Graduates with a Master’s degree in Environmental Nutrition are well prepared for
                        advancement in the workplace within:
                      
                     
                      
                        
                         Non-profit organizations with food and nutrition missions 
                        
                         Corporate positions with food and nutrition programs 
                        
                         Local, state, and national government food and nutrition programs 
                        
                         International food and nutrition programs/agencies 
                        
                         School food and nutrition services 
                        
                         Health care facilities 
                        
                         Wellness/health promotion programs 
                        
                      
                     
                      The online program delivery is designed to accommodate employees who could benefit
                        with job advancement or job security through acquiring an applied Master’s degree
                        in a flexible manner.
                      
                     
                      Testimonials 
                     
                      "I really love the Environmental Nutrition program at UofM! I'm very passionate about
                        nutrition, sustainability, and the environment, so for me, this degree is all encompassing.
                        I spent my undergrad at Ithaca College where I received a BA in biology and a minor
                        in nutrition; being able to continue my studies in a field I'm very enthusiastic about,
                        especially via their online campus, is what immediately attracted me to this program.
                        I was the first candidate to be awarded the Jane Nuckolls Endowed Scholarship in Environmental
                        Nutrition, which I'm greatly thankful for. I've recently started my own food and cooking
                        education business, teaching people the importance of sustainable, healthy practices." - Kristyn Polucha
                      
                     
                      "The Environmental Nutrition program at University of Memphis is truly one of a kind.
                        The program is flexible and broad in scope allowing me to explore a number of areas
                        within Environmental Nutrition. Being able to explore so many different areas helped
                        me pinpoint how I want to use my degree in the future. This has been a great experience!" - Angela Daniels Rivera
                      
                     
                      "After completing the MS program in Environmental Nutrition, I was accepted into the
                        Dietetic Internship Program at Iowa State University. Because of my relevant graduate
                        work, I received 4 weeks of prior -assessed learning credits and was recruited to
                        serve on a pilot project on Sustainable, Resilient, and Healthy Food and Water Systems
                        at Iowa State." - Lauren Hulen Boggs
                      
                     
                       
                           
                             
                     
                     
                     	
                      
                   
                
                
                   
                      
                         SHS Programs 
                         
                            
                               
                                Undergraduate  
                                      Dietetics  
                                      Exercise, Sport and Movement Sciences  
                                      Health Sciences  
                                      Healthcare Leadership  
                                      Physical Education Teacher Education  
                                  
                               
                                Graduate  
                                     
                                      Exercise, Sport and Movement Science  
                                     
                                      Health Promotion  
                                     
                                      Physical Education Teacher Education  
                                     
                                      Clinical Nutrition  
                                     
                                      Environmental Nutrition  
                                     
                                      Nutrition Science  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply to our Undergraduate Programs  
                               Submit your application today 
                            
                            
                                Apply to our Graduate Programs  
                               Earn your Masters Degree in one of our six programs! 
                            
                            
                                Contact Us  
                               Have questions? Call or email today! 
                            
                            
                                TIGUrS Garden  
                               Check out our on-campus urban garden 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/21/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


