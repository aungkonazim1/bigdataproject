Managing blog comments   
 
 
 
 
 
 Managing blog comments 
 













 
 

      
 
 Managing blog comments 
 Add a comment to a blog 
 
 Click the  Add Comment  link in the entry you want to comment on. 
 Enter your name in the  Name  field and your comment in the  Comment  field. 
 Click  Save Comment . 
 
 View a comment 
 On the Entry List page, click the  Comments  link for an entry. 
 Delete a comment 
 
 From the Entry List page, click the    Edit Entry  icon for the entry with the comment you want to delete. 
 Click the    Delete  icon beside the comment to delete it. 
 
  Note   You can only remove comments on your own blog; you cannot delete comments made on another user’s blog. 
 
   
 
 
 
  Desire2Learn Help  |  About Learning Environment  
 © 1999-2010 Desire2Learn Incorporated. All rights reserved. 
 

 
 
