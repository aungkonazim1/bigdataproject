Copying quizzes | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Copying quizzes | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Quizzes    »    Using Quizzes   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Copying quizzes                       
        
        
       
          
     
           Printer-friendly version       
	A copied quiz contains all of the questions from the original quiz. Copying is a quick and simple way to create a quiz that assesses prior and current understanding of course materials.
 

 
	 Note  You can only copy quizzes within the same course.
 

 
	Copy a quiz
 

  
		On the Manage Quizzes page, click    Copy  from the More Actions Button.
	 
	 
		Select your quiz from the  Quiz to Copy  drop-down list.
	 
	 
		Enter a  New Quiz Name .
	 
	 
		Select a status from the  New Quiz Status  drop-down list.
	 
	 
		Select  Edit Quiz after copy completes  if you want to go to the Edit page after creating your copy.
	 
	 
		Click  Save .
	 
      Audience:    Instructor      

    
           

                   ‹ Editing quizzes 
        
                   up 
        
                   Previewing quizzes › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Quizzes  
  
      Quizzes basics    Using Quizzes    Accessing Question Library    Grading a quiz    Creating quiz questions    Creating quizzes    Creating quiz sections    Editing quizzes    Copying quizzes    Previewing quizzes    Reordering quizzes    Deleting quizzes    Creating quiz categories    Editing a quiz category    Reordering quiz categories    Deleting a quiz category    Associating quizzes with learning objectives    Publishing quizzes to a learning object repository    Creating random sections in quizzes    Creating bonus quiz questions      Managing quiz questions and sections    Viewing quizzes    Quizzes and Question Library     
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
