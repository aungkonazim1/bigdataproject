Changing your email address   
 
 
 
 
 Changing your email address 
 











 
 
 

      
 
 Changing your email address 
 You may occasionally receive important login and enrollment information via email, so it is essential that you update your preferred email address if it has changed since registration. 
 
 From My Home, click the  Email Address  link in the  My Settings  widget. 
 Type your Learning Environment password in the  System Password  field. 
 Type your new email address in the  New Email  field. 
 Click the  Update Email  button. 
 
 Updating your email settings 
 After updating your email address, adjust your email settings to reflect the change. This ensures that all replies to emails you send from your Learning Environment account go directly to your updated email address. 
 Update your email settings 
 Do one of the following: 
 
 Select  Email  from the navigation bar and click  Settings . 
 Select  Preferences  from the  My Settings  widget, click the  Email  tab, and enter your updated email address into the  ‘Reply to’ Email Address  field. 
 
 
   
 
 
 
  Desire2Learn Help  |  About Learning Environment  
 © 1999-2010 Desire2Learn Incorporated. All rights reserved. 
 

 
 
