Graduate Courses - Computer Science - University of Memphis    










 
 
 
     



 
    
    
    Graduate Courses - 
      	Computer Science
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Computer Science
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          News  
                          Future Students  
                          Current Students  
                          Research  
                          People  
                          Outreach  
                      
                      
                         
                            Courses   
                            
                               
                                  
                                   Undergraduate Syllabi  
                                   Graduate Syllabi  
                                   Undergraduate Catalog Course Listings  
                                   Graduate Catalog Course Listings  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Computer Science
                              	  
                          
                              	Courses
                              	  
                         Graduate Courses 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Graduate Courses 
                     
                      Recent syllabi for our graduate courses are listed below. Official course descriptions
                        can be found in the University's   Graduate Catalog  .  For information about our graduate programs, please see the   Programs   section.
                      
                     
                      Note that students are   required   to bring laptops to class in COMP 4/6030, COMP 7/8012, and COMP 7/8085.
                      
                     
                      
                     
                      
                        
                           COMP 6001   - Computer Programming (Graduate)  (3)
                         
                        
                           COMP 6005   - Web Design and Development (Graduate)  (3)
                         
                        
                           COMP 6014   - Introduction to Java Programming (Graduate)  (3)
                         
                        
                           COMP 6030   - Design and Analysis of Algorithms (Graduate)  (3)
                         
                        
                           COMP 6040   - Programming Languages (Graduate)  (3)
                         
                        
                           COMP 6118  - Introduction to Data Mining (Graduate)  (3)
                         
                        
                           COMP 6242   - Introduction to Computer Graphics (Graduate)  (3)
                         
                        
                           COMP 6270   - Operating Systems (Graduate)  (3)
                         
                        
                           COMP 6272   - System Administration and UNIX Programming (Graduate)  (3)
                         
                        
                           COMP 6302   - Web Services and the Internet (Graduate)  (3)
                         
                        
                           COMP 6310   - Wireless and Mobile Computing (Graduate)  (3)
                         
                        
                           COMP 6410   - Computer Security (Graduate)  (3)
                         
                        
                           COMP 6601   - Models of Computation (Graduate)  (3)
                         
                        
                           COMP 6720  - Introduction to Artificial Intelligence (Graduate)  (3)
                         
                        
                           COMP 6731   - Data Visualization (Graduate)  (3)
                         
                        
                           COMP 6745  - Introduction to Machine Learning (Graduate)  (3)
                         
                        
                           COMP 6992  - Competitive Programming and Technical Interviews (Graduate)  (3)
                         
                        
                           COMP 7012/8012   - Foundations of Software Engineering  (3)
                         
                        
                           COMP 7081/8081   - Software Engineering Methodologies  (3)
                         
                        
                           COMP 7085/8085   - Program Comprehension  (3)
                         
                        
                           COMP 7087/8087   - Topics in Software Engineering  (3)
                         
                        
                           COMP 7115   - Database Systems  (3)
                         
                        
                           COMP 7116/8116   - Advanced Database Systems  (3)
                         
                        
                           COMP 7118/8118   - Topics in Data Mining  (3)
                         
                        
                           COMP 7120/8120   - Cryptography and Data Security  (3)
                         
                        
                           COMP 7125   - Computer Forensics  (3)
                         
                        
                           COMP 7130/8130   - Information Retrieval and Web Search  (3)
                         
                        
                           COMP 7150/8150   - Fundamentals of Data Science  (3)
                         
                        
                           COMP 7212/8212   - Operating and Distributed Systems  (3)
                         
                        
                         
                        
                           COMP 7282/8282   - Evolutionary Computation  (3)
                         
                        
                           COMP 7290/8290   - Molecular Computing  (3)
                         
                        
                           COMP 7295/8295   - Algorithms in Computational Biology and Bioinformatics  (3)
                         
                        
                           COMP 7311/8311   - Advanced Computer Networks  (3)
                         
                        
                           COMP 7313/8313   - Network Modeling and Performance Analysis  (3)
                         
                        
                           COMP 7327/8327   - Network and Internet Security  (3)
                         
                        
                           COMP 7514/8514   - Cognitive Science Seminar  (3)
                         
                        
                         
                        
                           COMP 7612/8612   - Foundations of Computing  (3)
                         
                        
                           COMP 7712/8712   - Algorithms and Problem Solving  (3)
                         
                        
                           COMP 7713/8713   - Advanced Topics in Algorithms  (3)
                         
                        
                           COMP 7720/8720   - Artificial Intelligence  (3)
                         
                        
                           COMP 7740/8740   - Neural Networks  (3)
                         
                        
                           COMP 7745/8745   - Machine Learning  (3)
                         
                        
                           COMP 7760/8760   - Control of Autonomous Agents  (3)
                         
                        
                           COMP 7770/8770   - Knowledge Representation and Reasoning  (3)
                         
                        
                           COMP 7780/8780   - Natural Language Processing  (3)
                         
                        
                           COMP 7950   - Research Methods in Computer Science  (1)
                         
                        
                           COMP 7980   - Master's Project  (1 - 3)
                         
                        
                           COMP 7992/8992   - Computational Complexity  (3)
                         
                        
                           COMP 7999/8999   - Computer Science Education Research  (3)
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Courses 
                         
                            
                               
                                Undergraduate Syllabi  
                                Graduate Syllabi  
                                Undergraduate Catalog Course Listings  
                                Graduate Catalog Course Listings  
                            
                         
                      
                      
                      
                         
                            
                                Career Opportunities  
                               Internship information and job postings for students 
                            
                            
                                Degree Programs  
                               Information on our degree and certificate programs 
                            
                            
                                Courses  
                               Recent syllabi for our courses 
                            
                            
                                Contact Us  
                               Whom to contact about what 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/21/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


