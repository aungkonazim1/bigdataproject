Research  ﻿ 
 
 
   Research 
   
   
 
 
   
           
       
            Home  
            About Me  
            Teaching  
            Research  
       
       
              
               
                            
                 
               
                   I am a low-dimensional topologist and recently have been working with two combinatorial ways of representing knots: grid diagrams and cube diagrams.  More recently, I've been studying knotted Legendrian tori via Lagrangian hypercube diagrams.  
                   A grid diagram is an nxn square grid together with a set of markings of two types, X and O such that each row and column gets exactly one of each type of mark. To obtain a link, in each column one joins an X marking to an O marking with a directed edge, and in each row, joins an O marking to an X marking with a directed edge. At any crossing of two edges, the vertical segment is defined to be the over crossing. Grid diagrams were introduced by Peter Cromwell to study embeddings of knots in open book decompositions of the 3-sphere. More recently they've been used to define a combinatorial version of Knot Floer homology and in the study of Legendrian knots. 
		   A cube diagram may be thought of as a 3-dimensional grid diagram.  It is a cubic lattice knot for which each of the projections to a coordinate plane is a grid diagram with a specified crossing condition (see  arXiv:0907.5401v1  for a full definition and examples).   
		   Since grid diagrams exist for any knot we can define an invariant, the arc index, which is the smallest integer n for which there is a grid diagram for a knot of size n. With a little more work one can show that cube diagrams exist or any knot, and define the cube number to be the smallest n for which there is a cube diagram of size n for the knot. It turns out that the cube number can detect some very interesting properties of knots. In all computed cases the cube number distinguishes between mirror images of knots. Also in some cases the cube number can distinguish between different Legendrian knots of the same topological type. 
		   Lagrangian hypercube diagrams may be viewed as a 4-dimensional version of a grid diagram.  There is a set of marking conditions that define and oriented loop in 4-space.  Of course, any loop in 4-space is trivial.  However, by carefully attaching rectangles to the loop defined by the hypercube diagram, one may obtain an immersed torus.  Under certain conditions, the torus lifts to an embedded Legendrian torus.  The structure of the hypercube diagram then allows for easy computation of invariants. 

               


                 
               
		    On the rotation class of knotted Legendrian tori in R^5 , Topology and its Applications, Volume 209, August 2016, 91-114 (joint with S. Baldridge).
		     An infinite family of Legendrian torus knots distinguished by cube number , Topology and its Applications, Volume 159, Issue 1, (2012), 162-174. 
		    Cube number can detect chirality and Legendrian type of knots , Journal of Knot Theory and its Ramifications, Vol. 21, Issue 03, (2012). 
		    Small Examples of Cube Diagrams of Knots , Topology Proceedings, Vol. 36 (2010), 213-228 (joint with S. Baldridge). 
		    Minimal Knotting Numbers , Jounal of Knot Theory and its Ramifications, Vol. 18 (2009), No. 8, 1159-1173 (joint with C. Mann, J. McLoud-Mann, R. Ranalli, and N. Smith). 
                    Metrics in Three-Dimensional Lattices , Journal for Geometry and Graphics, Vol. 12 (2008), No. 2, 133-140 (joint with C. Mann, J. McLoud-Mann, R. Ranalli, and N. Smith). 
               

	         
               
                   At various times I have found the ability to write computer code useful in my research.  I am familiar with C, C++, and Mathematica. 
		   2010:  I wrote a C++ program to apply all possible sequences of cyclic permutation and commutation mvoes (up to equivalence) starting with a given grid diagram. This was used to test a conjecture about whether or not such moves are sufficient to obtain all minimal grid diagrams of a Legendrian torus knot.   
                   2009:  I wrote a C++ program that constructs all possible cube diagrams of a given size and determines knot type.  This program generated several valuable results on cube number and chirality.  
		   2004:  As an undergraduate I worked with Casey Mann to write a program using C++ and a PVM (Parallel Virtual Machine) tool to construct all closed walks on the simple hexagonal lattice.  The program disproved a conjecture concerning the minimal length of a closed walk representing the trefoil. 
                


                 
              
             
               
        
       
	  	 
			Powered by  Free Website Templates 
		 
        
    
 
 
