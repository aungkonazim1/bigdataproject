Adding a course to Course Catalog | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Adding a course to Course Catalog | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Integrations    »    Course Catalog    »    Managing courses and programs in Course Catalog   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Adding a course to Course Catalog                       
        
        
       
          
     
           Printer-friendly version       
	Adding a course to Course Catalog and  activating it  makes the course available for learners to enroll in.
 

 
	 Notes 

	  
			E-commerce is disabled by default. Contact your administrator to enable it.
		 
		 
			You can offer a waitlist for any free course. If you add a price to a course and e-commerce is enabled, the waitlist will be disabled for that course.
		 
		 
			The ability to enroll in a program is not compatible with e-commerce; however, learners can still enroll in the program's courses individually.
		 
	  

  
		In the  Manage Catalog  area, do one of the following:
  
				In the  Admin Dashboard  area, click  + Add Course .
			 
			 
				In the   Courses  area, click  + Add Course .
			 
		  
	 
		Click on the course you want to add to Course Catalog.
	 
	 
		In the  Course Information  area, enter or update your course details.
 
		   Note  Separate tags using commas or by pressing the space bar on your keyboard.
		 
	 
	 
	In the  E-Commerce  area, enter a price for your course.  
	 
	In the  Course Features  area, select the course components you want to display on the Course Details page.  
	 
		In the  Course Overview  area, enter your course description details.
 
		 Note At this time, YouTube is the only supported video format.
		 
	 
	 
		In the  Instructors  area, do one of the following:
  To include an existing instructor for the course, click  + Add Existing Instructor .
			 
			 To include a new instructor for the course, click  + Add New Instructor .
			 
		  
	 
		Click  Save .
	 
  
	See also
 

  
		 Activating a course 
	 
      Audience:    Instructor      

    
           

                   ‹ Managing courses and programs in Course Catalog  
        
                   up 
        
                   Adding a program to Course Catalog › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Course Catalog  
  
      Course Catalog basics    Managing courses and programs in Course Catalog     Adding a course to Course Catalog    Adding a program to Course Catalog    Activating a course    Activating a program      Managing participants in Course Catalog    Managing course waitlists in Course Catalog    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
