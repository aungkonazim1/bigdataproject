Viewing items shared with you | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Viewing items shared with you | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    ePortfolio    »    Understanding the basic concepts in sharing   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Viewing items shared with you                       
        
        
       
          
     
           Printer-friendly version       
	There are various ways for you to discover, categorize, follow, and review content shared with you in ePortfolio.
 

 
	  Invites
 

 
	Invites enable you to promote the content you're sharing with specific people. When you share an item with others, you also have the option to send an invite to those you're sharing your item with from the Sharing Settings dialog. See  Sending invites  for more information.
 

 
	An invite contains information about who shared the item, when it was shared, what type of item it is, and any description or tags it has. The sender might also include a message letting you know why they shared the item.
 

 
	When you receive an invite from another user to view an item in their ePortfolio, a notification appears in the Message alerts on the minibar and the invite displays in the Unread Invites area of your dashboard. You might also receive an email informing you of the invite. You can subscribe to the  Invite RSS  feed from your  ePortfolio settings  to receive external updates about new invites.
 

 
	Subscriptions
 

 
	Subscriptions enable you to follow activity on items or from specific people that interest you. You can subscribe to user or item activity from your dashboard, the Explore page, or your invites.
 

  
		Subscribe to an item by clicking  Subscribe  in the context menu of the item.
	 
	 
		Subscribe to other users by toggling the  ​  Subscribe  icon beside their names.
	 
  
	Use the Subscriptions filter on your dashboard to view activity on items or from users you subscribed to. Recent subscription activity also appears in your minibar alerts.
 

 
	Unsubscribe to items or users by toggling the   ​icon or click  Unsubscribe .
 

 
	Friends List
 

 
	The Friends List filter on the dashboard enables you to view content shared with you or publicly from friends in your pager list.
 

 
	Minibar alerts
 

 
	Alerts on the minibar signal new activity related to your subscriptions and invites.
 

  
		The Message alerts display recently received invites.
	 
	 
		The Update alerts display assessments made on items or by people you've subscribed to.
	 
	 
		The Subscription alerts display comments made on items or by people you've subscribed to.
	 
  
	Email and SMS notifications
 

 
	If you want to receive ePortfolio updates externally, use the Notifications tool located in your personal menu on the minibar to set up the following instant notifications:
 

  
		 feedback added to subscribed items  Receive updates about comments and assessments made on items or by users you've subscribed to.
	 
	 
		 another user has subscribed to your updates   Receive a notification when someone has subscribed to you or one of your items.
	 
	 
		 feedback added to my items  Receive updates about comments and assessments made on your items.
	 
  
	See also
 

  
		 Understanding ePortfolio settings 
	 
      Audience:    Learner      

    
           

                   ‹ Understanding the basic concepts in sharing 
        
                   up 
        
                   Sharing with internal and external users › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        ePortfolio  
  
      ePortfolio basics    Using artifacts    Creating and editing presentations    Creating collections    Managing comments and assessments in ePortfolio    Understanding the basic concepts in sharing    Viewing items shared with you    Sharing with internal and external users    Creating quicklinks to ePortfolio items    Sharing permission options    Understanding cascading permissions    Setting up sharing groups    Removing sharing permissions    Ignoring and restoring items from users      Importing and exporting items    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
