Business Law Certificate - School of Law - University of Memphis  Business Law Certificate  










 
 
 
     



 
    
    
    Business Law Certificate - 
      	School of Law 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Cecil C. Humphreys School of Law
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Admissions  
                          Programs  
                          Current Students  
                          Faculty  
                          Careers  
                          Library  
                      
                      
                         
                            Programs   
                            
                               
                                  
                                   Degree Programs  
                                         JD  
                                         JD/MBA  
                                         JD/MA in Pol. Science  
                                         JD/MPH  
                                         MPH Fast-Track  
                                         Part-time Program  
                                     
                                  
                                   Certificate Programs  
                                        
                                         Advocacy  
                                        
                                         Business Law  
                                        
                                         Health Law  
                                        
                                         Tax Law  
                                     
                                  
                                   Experiential Learning  
                                         Experiential Learning Home  
                                         Legal Clinics  
                                         Externship Program  
                                     
                                  
                                   Law Review  
                                         Law Review Home  
                                         Editorial Board  
                                         Editor-in-Chief  
                                         Joining Law Review  
                                         Virtual Tour of Law Review Suite  
                                         Symposium  
                                         Subscriptions  
                                         Archives  
                                         Current Volume  
                                         Connect with Us  
                                     
                                  
                                   Advocacy Program  
                                         Moot Court Board  
                                         Joining Moot Court Board  
                                         In-School Competitions  
                                         Travel Team Competitions  
                                         Past Champions  
                                     
                                  
                                   Inst. for Health Law   Policy  
                                         Institute Home   
                                         Advisory Committee  
                                         Symposium  
                                         Pro Bono Opportunities  
                                         Health Law Certificate  
                                         Health Law Society  
                                         MBA Health Law Section  
                                     
                                  
                                   International Law Programs  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	School of Law 
                              	  
                          
                              	Programs
                              	  
                         Business Law Certificate 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      BUSINESS LAW CERTIFICATE 
                     
                      The goal of the Certificate in Business Law is to enable students who are interested
                        in a career involving business-related practice areas to follow a specialized course
                        of study, to work closely with other students interested in the area, and to receive
                        guidance from faculty members with an interest and expertise in business law. A student
                        who receives the Certificate in Business Law demonstrates knowledge of fundamental
                        principles of business-related legal issues and competence in the skills essential
                        to a business-related practice.
                      
                     
                      For more information about the Certificate in Business Law, please contact Prof. Kevin
                        Smith at  ksmith@memphis.edu .
                      
                     
                      COURSE REQUIREMENTS 
                     
                      A student must successfully complete business-related study by taking courses from
                        the lists below.
                      
                     
                      Required Courses: All the following courses. 
                     
                      
                        
                         Business Organizations I (3 hours) 
                        
                         Corporate Tax (3 hours)* 
                        
                         Commercial Law Survey (4 hours) or both Secured Transactions (3) and Sales(3) 
                        
                      
                     
                      Core Electives: At least two (2) of the following courses. 
                     
                      
                        
                         Administrative Law (3) 
                        
                         Debtor-Creditor or Problems in Bankruptcy (3) 
                        
                         Mergers   Acquisitions (3) 
                        
                         Non-profit Organizations (3) 
                        
                         Partnership Tax (3)* 
                        
                         Securities Regulations (3) 
                        
                      
                     
                      Skills Component: At least one of the following courses. 
                     
                      
                        
                         ADR-Arbitration (2-hour skills course) 
                        
                         ADR-Labor (2-hour skills course) 
                        
                         ADR-Mediation (2-hour skills course) 
                        
                         Business-related Externship (2-, 3-, or 4-hour skills course) 
                        
                         Contract Drafting (2-hour skills course) 
                        
                         Mediation Clinic (4-hour skills course) 
                        
                      
                     
                      The Director of the Business Law Program may add courses to these lists to reflect
                        new curricular offerings and may approve substitutions in compelling circumstances.
                      
                     
                      NON-COURSE REQUIREMENTS 
                     
                      Student must complete at least twenty-five hours of non-course work related to business
                        law while enrolled in the Certificate program. The student must complete at least
                        five hours of work each semester. Permissible activities include participating in
                        the VITA program and performing pro bono work that involves Memphis Area Legal Services
                        Consumer or Tax units and others in consultation with the Pro Bono Coordinator and
                        the Director of the Business Law Certificate Program.
                      
                     
                      Student must also attend one meeting of students who are enrolled in the Certificate
                        program each semester.
                      
                     
                      GRADE POINT REQUIREMENTS 
                     
                      A student must demonstrate successful understanding of the fundamentals of business-related
                        law by receiving grades of at least C, and achieving an overall GPA of at least 2.5
                        in the building block courses of Legal Methods I and II, Contracts, and Professional
                        Responsibility. Students must receive a GPA of at least 3.0 in courses taken to satisfy
                        the Certificate in Business Law.
                      
                     
                      A student will receive the Certificate in Business Law with Honors by completing the
                        graded courses taken to satisfy the Certificate with a GPA of 3.5 or higher and receiving
                        a grade of Excellent in at least two-thirds of the non-graded courses taken to satisfy
                        the Certificate requirements.
                      
                     
                      ENROLLMENT 
                     
                      Students in good academic standing may enroll in the Certificate in Business Law program
                        after completing one year of full-time law study (or thirty hours as a part-time student).
                        A student may not enroll after the add deadline in the student's fourth semester of
                        full-time study (or of the semester after a part-time student has completed forty-five
                        hours of study). A student enrolls by completing an enrollment form and submitting
                        it to the Director of the Business Law program. In completing the form, the student
                        certifies that he or she can satisfy the grade requirements for the Certificate program
                        and is committed to completing all of the Program's requirements. A student will be
                        dropped from the Program if he or she fails to meet the requirements.
                      
                     
                      *Both Corporate Tax and Partnership Tax require Income Tax 
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Programs 
                         
                            
                               
                                Degree Programs  
                                      JD  
                                      JD/MBA  
                                      JD/MA in Pol. Science  
                                      JD/MPH  
                                      MPH Fast-Track  
                                      Part-time Program  
                                  
                               
                                Certificate Programs  
                                     
                                      Advocacy  
                                     
                                      Business Law  
                                     
                                      Health Law  
                                     
                                      Tax Law  
                                  
                               
                                Experiential Learning  
                                      Experiential Learning Home  
                                      Legal Clinics  
                                      Externship Program  
                                  
                               
                                Law Review  
                                      Law Review Home  
                                      Editorial Board  
                                      Editor-in-Chief  
                                      Joining Law Review  
                                      Virtual Tour of Law Review Suite  
                                      Symposium  
                                      Subscriptions  
                                      Archives  
                                      Current Volume  
                                      Connect with Us  
                                  
                               
                                Advocacy Program  
                                      Moot Court Board  
                                      Joining Moot Court Board  
                                      In-School Competitions  
                                      Travel Team Competitions  
                                      Past Champions  
                                  
                               
                                Inst. for Health Law   Policy  
                                      Institute Home   
                                      Advisory Committee  
                                      Symposium  
                                      Pro Bono Opportunities  
                                      Health Law Certificate  
                                      Health Law Society  
                                      MBA Health Law Section  
                                  
                               
                                International Law Programs  
                            
                         
                      
                      
                      
                         
                            
                                Apply to Memphis Law  
                               Take the first step toward your legal education 
                            
                            
                                ABA Required Disclosures  
                               Information required by ABA Standard 509 Required Disclosures 
                            
                            
                                Alumni   Support  
                               Stay up to date with Memphis Law alumni 
                            
                            
                                Contact Us  
                               Questions about law school? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


