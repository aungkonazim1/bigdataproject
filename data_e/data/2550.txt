Recent Dissertations in History - Department of History - University of Memphis    










 
 
 
     



 
    
    
    Recent Dissertations in History - 
      	Department of History
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of History
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Why History?  
                          Undergraduate  
                          Graduate  
                          Online  
                          People  
                          HERC  
                          GAAAH  
                      
                      
                         
                            Graduate   
                            
                               
                                  
                                   Prospective Students  
                                   MA and PhD Programs  
                                         General Description  
                                         Focus Areas and Courses  
                                         MA Program  
                                         PhD Program  
                                     
                                  
                                   Scholarships and Awards  
                                   People  
                                         Current Students  
                                         Faculty  
                                         Alumni Placement   
                                     
                                  
                                   Student Organizations  
                                   Becoming a Historian  
                                   Forms and Applications  
                                   Contact Grad Studies Director  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of History
                              	  
                          
                              	Graduate
                              	  
                         Recent Dissertations in History 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Recent dissertations 
                     
                      For all of these dissertations, the database ProQuest Dissertations   Theses A I available
                        through the  University Libraries  allows the viewing of citations, abstracts, and full text. You may also purchase
                        copies in various formats from ProQuest. It appears to take several months for new
                        dissertations to be included in the database.
                      
                     
                      Beginning with the Fall 2010 semester, dissertations at The University of Memphis
                        must be submitted to the  Electronic Thesis and Dissertation Repository  (ETD). Authors may allow unrestricted access, allow access by University of Memphis
                        authorized users only, or deny access altogether, so a particular dissertation may
                        or may not be available through the repository.
                      
                      
                     
                      2015 
                     
                      
                        
                         Maria R. Carlenius, 2015. Egypt's Unusual Iron Age: From the Time of Hammering Prestige
                           Goods in the Predynastic Era and late Jenny Jobe Demilio, 2015. "Theophilus Hunter
                           Holmes: An Active Life of Plenty". Major Professor: Charles W. Crawford
                         
                        
                         Jeffrey Scott Frizzell, 2015. "The Impossible, Only Solution: School Busing and Racial
                           Integration in Memphis, Tennessee, 1972-1975". Major Professor: Aram Goudsouzian
                         
                        
                         Rita M. Hall, 2015. "Pragmatism, Professionalization, and Privatization in the Administration
                           of the Memphis Zoo, 1906-2016". Major Professor: Charles W. Crawford
                         
                        
                         Malcolm Frierson, 2015. Coming to the Stage: Dick Gregory, Bill Cosby, and the Politics
                           of African American Comedy, 1961-1975. Major Professor: Aram Goudsouzian
                         
                        
                         Roy Dean Dowdy, 2015. History of Enlisted Women in the Air National Guard, 1968-2012.
                           Major Professor: Stephen Stein
                         
                        
                         Harry Roger Young, 2015. Stories of the Trenton Six: Race and the Early Cold War,
                           1948-1953. Major Professor: Aram Goudsouzian
                         
                        
                         James Conway, 2015. Moderated Militants in the Age of Black Power: The Memphis NAACP,
                           1968-1975. Major Professor: Aram Goudsouzian
                         
                        
                         Le’Trice Donaldson, 2015. A Legacy All Their Own: African American Soldiers Fight
                           for Citizenship, Race, and Manhood, 1870-1920. Major Professor: Stephen Stein
                         
                        
                      
                     
                        
                     
                      2014 
                     
                        
                     
                      
                        
                         Kaylin T. Ewing, 2014. “Her Own Kind of Woman”: The Life of Alberta Hunter. Major
                           Professor: Beverly Bond
                         
                        
                         Michael Lejman, 2014. Being Different: The Life and Work of Albert Memmi. Major Professor:
                           Daniel Unowsky
                         
                        
                         MIchael Blum, 2014. “An Island of Peace in a Sea of Racial Strife” :  The Civil Rights Movement in Knoxville, Tennessee. Major Professor: Aram Goudsouzian
                         
                        
                         Jack Lorenzini, 2014. “Power Concedes Nothing Without a Demand”: Student Activities
                           at Memphis State University in the 1960s. Major Professor: Aram Goudsouzian
                         
                        
                         Rachel Jana Mittelman, 2014. "Ceramics as an Ethnic Identifier: Libyans in the Nile
                           Delta during the Third Intermediate Period", Major Professor: Suzanne Onstine
                         
                        
                         Nancy R. Parrish, 2014. "Cotton, Factory, and People: The Production of Bemis, Tennessee,
                           a Mill Town" Major Professor: Charles W. Crawford
                         
                        
                      
                     
                        
                     
                      2013 
                     
                        
                     
                      
                        
                         Brian Marc Edward McClure, 2013. Educating the Atlantic: Foreign Students and Global
                           Exchange at Tuskegee Institute. Major Professor: Aram Goudsouzian
                         
                        
                         Mark D. Janzen, 2013. The Iconography of Humiliation: The Depiction and Treatment
                           of Bound Foreigners in New Kingdom Egypt. Major Professor: Peter Brand
                         
                        
                         Shawn Fisher, 2013. The Battle of Little Rock. Major Professor: Aram Goudsouzian 
                        
                      
                     
                      2012 
                     
                      
                        
                         Richard L. Saunders, 2012. “Encouraged by a Little Progress”: Voting Rights and the
                           Contests over Social Place and Civil Society in Tennessee’s Fayette and Haywood Counties,
                           1958-1964. Major Professor: Aram Goudsouzian
                         
                        
                         Jared Krebsbach, 2012. Turmoil and Conflict: A Thematic and Chronological Study of
                           Dynastic Transition in Late Period Egypt. Major Professor: Peter Brand
                         
                        
                         Katherine J. Fox, 2012. Pidgin in the Classroom: Hawai’i’s English Standard Schools,
                           Americanization, and Hawaiian Identity, 1920-1960. Major Professor: Janann Sherman
                         
                        
                         Carl Edgar Brown, 2012. “Improving the Way to the Land of Opportunity”: Internal Improvements
                           in Antebellum Arkansas. Major Professor: Charles Crawford
                         
                        
                         Chrystal Elaine Goudsouzian, 2012. Becoming Isis: Myth, Magic, Medicine, and Reproduction
                           in Ancient Egypt. Major Professor: Susan Onstine
                         
                        
                         Kevin Johnson, 2012. Transition and Legitimation in Egypt’s Nineteenth and Twentieth
                           Dynasties: A Study of the Reigns of Siptah, Tausret, and Sethnakht. Major Professor:
                           Peter Brand
                         
                        
                         Sheena Harris, 2012. A Female Reformer in the Age of Booker T. Washington: The Life
                           and Times of Margaret Murray Washington. Major Professor: Beverly Bond
                         
                        
                         Richard Harold Nollan, 2012. Heart’s Blood: A Biography of Lemuel Whitley Diggs. Major
                           Professor: Charles Crawford
                         
                        
                         Doris Ann Youngblood Mulhearn, 2012. Southern Graces: Women, Faith, and the Quest
                           for Social Justice, Memphis, Tennessee, 1950-1969. Major Professor: Aram Goudsouzian
                         
                        
                         Marilyn Horton Taylor, 2012. George “Machine Gun” Kelly: His Life and Impact. Major
                           Professor: Charles Crawford
                         
                        
                         Maurice Brown, 2012. Plantation Schools: A History of Rural Black One-Room Schools
                           in the Mid-South and the Mississippi Delta from Reconstruction to 1968.
                         
                        
                         Darien Nikolaev Stephanov, 2012. Minorities, Majorities, and the Monarch: Nationalizing
                           Effects of the Late Ottoman Royal Public Ceremonies, 1808–1908. Major Professor: Kent
                           Schull
                         
                        
                         Cynthia J. Sadler, 2012. Standing in the Shadows: African American Informants and
                           Allies of the Mississippi State Sovereignty Commission. Major Professor: Dr Janann
                           Sherman
                         
                        
                         Leigh Ann Wilson, 2012. Fighting Two “Devils”: Eleuterio Escobar and the School Improvement
                           League’s Battle for Mexican and Mexican-American Students’ Educational Equality in
                           the San Antonio, Texas Public Schools, 1934 to 1958. Major Professor: Janann Sherman
                         
                        
                      
                     
                      2011 
                     
                      
                        
                         Darius Young, 2011. “The Gentleman from Memphis”: Robert R. Church Jr. and African
                           American Leadership during the Early Civil Rights Movement. Major Professor: Aram
                           Goudsouzian
                         
                        
                         Shirletta Kinchen, 2011. “We want what people generally refer to as Black Power”:
                           Youth Activism and the Impact of the Black Power Movement in Memphis, Tennessee, 1965-1975.
                           Major Professor: Aram Goudsouzian
                         
                        
                         Lyndel Fisher, 2011. The Theological Antecedents of the Assemblies of God: Baptist
                           and Presbyterian Roots. Major Professor: Charles W. Crawford
                         
                        
                         Reginald K. Ellis, 2011. James Edward Shepard and the Politics of Black Education
                           in North Carolina during the Jim Crow Era: 1875-1947. Major Professor: Beverly Bond
                         
                        
                         Daryl Carter, 2011. President Bill Clinton, African Americans, and the Politics of
                           Race and Class. Major Professor: Aram Goudsouzian
                         
                        
                      
                     
                      2010 
                     
                      
                        
                         Joe Frazer, Jr., 2010. The Fourth Parliament of Elizabeth I: A Legislative Statistical
                           History. Major Professor: James M. Blythe
                         
                        
                         Jayme Millsap Stone, 2010. “They Were Her Daughters”: The Student Nonviolent Coordinating
                           Committee and Grassroots Organizing for Social Justice in the Arkansas Delta, 1963-1967.
                           Major Professor: Janann Sherman
                         
                        
                         Carol Ciscel, 2010. Inseparable Companion: The Consolation of Heloise. Major Professor:
                           James M. Blythe
                         
                        
                         Roy Hopper, 2010. The Monuments of Amenmesse and Seti II: A Historical Inquiry. Major
                           Professor: Peter J. Brand
                         
                        
                         Chris Ivanes, 2010. National Ideology and The Making of a Nation: Simion Barnutiu
                           and the Romanian Revolution of 1848-1849 in Transylvania. Major Professor: Daniel
                           Unowsky
                         
                        
                      
                     
                      2009 
                     
                      
                        
                         Kimberly Ellen Nichols, 2009. The Civil Rights Underground: The Movement for Compliance
                           with the Civil Rights Act of 1964. Major Professor: Janann Sherman
                         
                        
                         Robert Calvin Griffin, 2009. The Worship of Syro-Canaanite Deities in Egypt: Iconographic,
                           Epigraphic, and Historical Analyses of the New Kingdom Evidence. Major Professor:
                           Peter J. Brand
                         
                        
                         Louise Marie Cooper, 2009. Great Overlord of the Name: The Office of Nomarch During
                           the First Intermediate Period. Major Professor: Peter J. Brand
                         
                        
                          John Lawrence Bass, 2009. Bolsheviks on the Bluff — A History of the Memphis Communists
                           and Their Labor and Civil Rights Contributions, 1930-1957. Major Professor: Charles
                           W. Crawford
                         
                        
                         Horace K. Houston, 2009. Catalyst for Antebellum Conflict: The Fugitive Slave Law
                           of 1850. Major Professor: Charles W. Crawford
                         
                        
                         Josh Gorman, 2009. Museums and the Chickasaw Nation of Oklahoma. Major Professor:
                           James E. Fickle
                         
                        
                         Yuan Gao, 2009. Deconstructing the Provincial Identity: A Case Study of Post-Mao Shaanxi.
                           Major Professor: James M. Blythe; Adjunct Co-Mentor: Lung-kee Sun
                         
                        
                      
                     
                      2008 
                     
                      
                        
                         Larry Powers, 2008. George C. Krick — American Guitarist: 1871-1962. Major Professor:
                           Dr Charles W. Crawford
                         
                        
                         Matthew Daniel Mason, 2008. A Partial Presentation of the Past: A Critical Examination
                           of  Wisconsin Death Trip . Major Professor: Dr Janann Sherman.
                         
                        
                         Ed Hamelrath, 2008. From Dictatorship to Democracy: A Reform of the German Volkspolizei
                           in the State of Saxony after the Fall of the GDR, 1989-1994. Major Professor: Daniel
                           Unowsky
                         
                        
                         Jonathan Weems, 2008. A Challenge Constantly Renewed: Medicare and the Struggle for
                           National Health Insurance. Major Professor: Janann Sherman
                         
                        
                         Donna Reeves, 2008. Battle for an Image: Black Memphians Define Their Place in Southern
                           History. Major Professor: Janann Sherman
                         
                        
                         Whitney Huey, 2008. Virtue and Authority: A Consideration of Catherine of Siena’s
                           Letters as Political Theory. Major Professor: James M. Blythe
                         
                        
                         Keith Sisson, 2008. Giles of Rome’s Hierocratic Theory of Universal Papal Monarchy.
                           Major Professor: James M. Blythe
                         
                        
                      
                     
                      2007 
                     
                      
                        
                         Paul White, 2007. Kennedy Army Hospital. Major Professor: Charles W. Crawford 
                        
                         Elton Weaver, 2007. “Mark the Perfect Man, and Behold the Upright”: Bishop C.H. Mason
                           and the Emergence of the Church of God in Christ in Memphis, Tennessee. Major Professor:
                           Beverly G. Bond
                         
                        
                      
                     
                      2006 
                     
                      
                        
                         Raybon Joel Newman, 2006. Race and the Assemblies of God Church: The Journey from
                           Azusa Street to the “Miracle of Memphis.” Major Professor: Charles W. Crawford
                         
                        
                      
                     
                      2005 
                     
                      
                        
                         John Has-Ellison, 2005. True Art is Always an Aristocratic Matter: Nobles and the
                           Fine Arts in Bavaria, 1890-1914. Major Professor: Daniel Unowsky
                         
                        
                         Marcel Oyono, 2005. Colonization and Ethnic Rivalries in Cameroon since 1884. Major
                           Professor: Janann Sherman
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Graduate 
                         
                            
                               
                                Prospective Students  
                                MA and PhD Programs  
                                      General Description  
                                      Focus Areas and Courses  
                                      MA Program  
                                      PhD Program  
                                  
                               
                                Scholarships and Awards  
                                People  
                                      Current Students  
                                      Faculty  
                                      Alumni Placement   
                                  
                               
                                Student Organizations  
                                Becoming a Historian  
                                Forms and Applications  
                                Contact Grad Studies Director  
                            
                         
                      
                      
                      
                         
                            
                                History Happenings  
                               Significant happenings that involve our faculty, students, staff, and alumni. 
                            
                            
                                Newsletter  
                               The department newsletter is filled with interesting articles and information about
                                 our award-winning faculty and students.
                               
                            
                            
                                Event Calendar  
                               Check here often for upcoming events hosted by the Department of History 
                            
                            
                                Contact Us  
                               Contact the Department of History at The University of Memphis. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


