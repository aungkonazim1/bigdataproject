Software - 3D Printing @UofMLibraries - LibGuides at University of Memphis Libraries   
 
	 
         
         
         
        
		 Software - 3D Printing @UofMLibraries - LibGuides at University of Memphis Libraries 
		 
		 
 
 
 
 
 
 
 
 
 
 
 

		 
		 
		 
		 
		 
		 
         
         
				 
				 
             
         
        
        
            
		
		 
		
		 
		 
		
		 
		
		
	   
		         
        	 
     
	 
		 Skip to main content                  
                 
         
  		 
             
                
			 
				
					 
						 University of Memphis Libraries
						 
					 
					 
						 LibGuides
						 
					 
					 
						 3D Printing @UofMLibraries
						 
					 
					 Software
					 
			              
			 
				 
					
                 
                     
                         
                             
                             Search this Guide  
                             
                                 Search 
                             
                         
                     
                 				 
							 
             
                 3D Printing @UofMLibraries 
                 
                     Guide to the 3D Printing Lab at the University of Memphis 
                 
             
         
         
          
         
             
                 
                     
                         
                             
                 
                     
                         Welcome 
                        
                     
                 
                 
                     
                         Schedule 
                        
                     
                 
                 
                     
                         3D Job Form 
                        
                     
                 
                 
                     
                         How to Print 
                        
                     
                 
                 
                     
                         How to Scan 
                        
                     
                 
                 
                     
                         Tips & Tricks 
                        
                     
                 
                 
                     
                         FAQ 
                        
                     
                 
                 
                     
                         Software 
                        
                     
                  
                            
                 
                     
                         
                        
							 
					 
						 
							 Libguides Homepage Box
                                 
							 
								 
									
			 
				     

        
E-Mail Casey at  ckprkman@memphis.edu  

   

   

   

     
		    
								 
								
							 
						 
					 
							 
                         
                     
                                          
                     
                 
                 
                     
                         
                             
     
                  
					 
						 
							 Makerbot Programs
                                 
							 
								 
									
			 
				     
   DOWNLOAD PRINTER/SCANNER SOFTWARE FREE   
		    
								 
								
							 
						 
					   
					 
						 
							 Free CAD Software
                                 
							 
								 
									
			 
				     
  
   Sketchup - Make:    
  The free version of Sketchup, Sketchup Make is a great beginners tool for 3D Computer Assisted Design.     
  Steps to Use:  
 
   Download Sketchup Make   
  Follow instillation instructions  
   Download the STL Importer/Exporter   
  Import STL files from Thingiverse, Makerbot Digitizer, etc.  
  Edit Files  
  Export as 3D model in .obj file type  
 
       
  
  Sculptris Alpha Six  
 3D design software for the more artistically inclined. Great for making sculptures from the ground up. 
  Steps to Use:  
 
   Dowload Sculptris Alpha Six   
  Design your file (.obj files can be imported)  
  Export completed design as .obj  
 
     
     
   Blender   
 Blender is the most advanced of the free CAD programs. If you are comfortable with 3D Design this is the software for you 
  Steps to Use:  
 
   Download Blender   
  Design your file (.obj files can be imported)  
  Export completed design as .obj  
 
  
  
		    
								 
								
							 
						 
					  
         
     
                          
                     
                    
                 
                     
                         Previous:  FAQ 
                     
                     
                    
                     
                                  
             
         
         
         
             
                 
                     
                         Last Updated:   Oct 13, 2016 10:23 AM                      
                     
                         URL:   http://libguides.memphis.edu/3DLab                      
                     
                    	    Print Page                      	
                     
                 
                     Login to LibApps                  
             
             
                 
                     Report a problem  
                 
                 
                    
                     Subjects:  
                      Help/How-To Guides ,  Technology  
                                     
                 
                    
                     Tags:  
                      3d ,  3d scanner ,  cad ,  computer ,  computer science ,  emerging technology ,  engineering ,  how to ,  lab ,  printing ,  science ,  sciences ,  stem ,  technology ,  web of science ,  workshops  
                                     
             
         
         
        
			 
				 
					 
					    
					    
					 
				 
			          
                              
                                
                 
                

		 
  	 
 
