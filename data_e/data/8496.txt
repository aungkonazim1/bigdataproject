Uploading files | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Uploading files | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    ePortfolio    »    Using artifacts   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Uploading files                       
        
        
       
          
     
           Printer-friendly version       Upload files from your computer, personal storage device, or Learning Environment locker area that demonstrate your improvement or mastery in an area of interest. 
 Upload files 
  On the My Items page, click  File Upload  from the Add button. 
 Click  Upload  to browse for the file you want to add. 
 Click  Done . 
 Confirm that you selected the correct file, then click  Next . 
 Give the artifact a  Name  and  Description . 
 Add any tags you want the artifact to have. 
 Click  Save . 
      Audience:    Learner      

    
           

                   ‹ Adding linked web addresses 
        
                   up 
        
                   Creating web documents › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        ePortfolio  
  
      ePortfolio basics    Using artifacts    Adding reflections    Adding linked web addresses    Uploading files    Creating web documents    Adding audio recordings    Adding form responses    Importing course content    Associating reflections with items    Using learning objectives in ePortfolio      Creating and editing presentations    Creating collections    Managing comments and assessments in ePortfolio    Understanding the basic concepts in sharing    Importing and exporting items    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
