STEM Teacher Leadership Graduate Certificate - ICL - University of Memphis    










 
 
 
     



 
    
    
    STEM Teacher Leadership Graduate Certificate - 
      	ICL
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Instruction and Curriculum Leadership 
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Programs  
                          Students  
                          Faculty  
                          ICL Faculty Research  
                          A-Z List  
                      
                      
                         
                            ICL Programs   
                            
                               
                                  
                                   Early Childhood  
                                   Elementary Education  
                                   Instruction and Curriculum  
                                   Instructional Design   Technology  
                                   Reading  
                                   School Library Specialist  
                                   Secondary Education  
                                   Special Education  
                                   Graduate Certificates  
                                        
                                         Urban Education  
                                        
                                         IDT  
                                        
                                         Literacy, Leadership,   Coaching  
                                        
                                         Autism Studies  
                                        
                                         School Library Information Specialist  
                                        
                                         STEM Teacher Leadership  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	ICL
                              	  
                          
                              	Programs
                              	  
                         STEM Teacher Leadership Graduate Certificate 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      STEM Teacher Leadership Graduate Certificate 
                     
                       The Department of Instruction and Curriculum Leadership is proud to offer a Graduate
                           Certificate in STEM Teacher Leadership. The goal of the STEM (Science, Technology,
                           Engineering, and Mathematics) Teacher Leadership coursework is to develop the knowledge
                           and skills of secondary mathematics and science teachers who are currently teaching
                           in local area schools. The coursework is designed to promote in-depth understanding
                           of current standards and instructional practices with a particular focus on building
                           teacher leaders in STEM education. Designed as a professional learning opportunity
                           for in-service teachers, the coursework reflects the research-based characteristics
                           of effective professional development. In particular, the professional development
                           is sustained and long-term; involves active learning; is content-focused; and is tied
                           to the local teaching context.  
                     
                       Key features of the coursework include:   • Focus on equity and opportunity to learn in urban schools   • Opportunities to deepen pedagogical content knowledge in STEM disciplines (including
                           knowledge of content and students; knowledge of content and curriculum; knowledge
                           of content and teaching)   • Attention to models of STEM professional development and opportunities to design
                           and deliver professional development to peers.  
                     
                       As a result of completing the coursework, participants could expect to be better equipped
                           for greater effectiveness in the classroom and better prepared to take on roles as
                           teacher leaders within their schools.  
                     
                      Program Admission 
                     
                       Students interested in receiving a Certificate in STEM Teacher Leadership must hold
                           a current teaching license. The courses may be completed as part of a degree program
                           with the advisor's approval.  
                     
                       To be admitted students should apply to the  Graduate School  as a Master's Degree seeking applicant. Under 'Planned Course of Study' the student
                           should select 'STEM Teacher Leadership CERT'.  
                     
                      Program Requirements 
                     
                       A total of 4 courses are required for the certificate:  
                     
                       ICL 7720/8720 STEM Curriculum Leadership   ICL 7721/8721 Seminar in STEM Teacher Development   ICL 7722/8722 Teaching and Learning in STEM Classrooms   ICL 7723/8723 Equity in STEM Education  
                     
                      Contact Us 
                     
                       Graduate Office: 901-678-4861   icl-graduate-studies@memphis.edu  
                     
                        
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         ICL Programs 
                         
                            
                               
                                Early Childhood  
                                Elementary Education  
                                Instruction and Curriculum  
                                Instructional Design   Technology  
                                Reading  
                                School Library Specialist  
                                Secondary Education  
                                Special Education  
                                Graduate Certificates  
                                     
                                      Urban Education  
                                     
                                      IDT  
                                     
                                      Literacy, Leadership,   Coaching  
                                     
                                      Autism Studies  
                                     
                                      School Library Information Specialist  
                                     
                                      STEM Teacher Leadership  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                ICL Degrees  
                               Broaden your career choices by gaining the confidence you to need to succeed 
                            
                            
                                Apply Now  
                               Every journey starts with a step, now take yours! 
                            
                            
                                Advising  
                               Your academic success begins with advising. 
                            
                            
                                Contact Us  
                               Questions? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


