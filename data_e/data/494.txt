Readmit Students - Undergraduate Admissions and Orientation - University of Memphis  Continue your education the as a readmit student at the University of Memphis, learn more about the Readmission process, and Apply Now.  










 
 
 
     



 
    
    
    Readmit Students - 
      	Undergraduate Admissions and Orientation
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Undergraduate Admissions and Orientation
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Freshmen  
                          Transfer  
                          Readmit  
                          Graduate  
                          Law  
                          International  
                          High School  
                      
                      
                         
                            Readmission   
                            
                               
                                   Getting Started  
                                         Do I need to reapply?  
                                         Application Checklist  
                                         Academic Programs  
                                         Undergraduate Catalog  
                                     
                                  
                                   Residency Information  
                                         Residency Policy  
                                         Residency Appeal Form  
                                         Border County Waiver  
                                         EVEA  
                                     
                                  
                                   Tuition and Fees  
                                         Tuition Estimator  
                                         Fee Charts  
                                     
                                  
                                   Financing Your Education  
                                         Financial Aid  
                                     
                                  
                                   Apply for Readmission  
                                         Readmission Requirements  
                                         Application Deadline  
                                         Apply Now!  
                                     
                                  
                                   What to Expect  
                                         Acknowledgement  
                                         Application Review  
                                         Admission Decision  
                                     
                                  
                                   Transfer Credit  
                                         Equivalency Table  
                                          Transfer Credit Evaluation   
                                     
                                  
                                    After Acceptance   
                                         Advising  
                                          Registration   
                                          ID Card   
                                          Parking   
                                          Fee Payment   
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Undergraduate Admissions and Orientation
                              	  
                         
                           	Readmission
                           	
                         
                      
                   
                   
                       
                      
                     		
                     
                     
                      Readmit Students 
 A readmit student is someone who has attended the University of Memphis in the past and is reapplying for admission.  Please click “ Apply Now ” to begin that process. 
 We appreciate your interest in continuing your education at the University of Memphis.  If you need assistance, please contact the Admissions Office at 901.678.2111 or 1.800.669.2678.                      
                     
                     	
                      
                   
                
                
                   
                      
                         Readmission 
                         
                            
                                Getting Started  
                                      Do I need to reapply?  
                                      Application Checklist  
                                      Academic Programs  
                                      Undergraduate Catalog  
                                  
                               
                                Residency Information  
                                      Residency Policy  
                                      Residency Appeal Form  
                                      Border County Waiver  
                                      EVEA  
                                  
                               
                                Tuition and Fees  
                                      Tuition Estimator  
                                      Fee Charts  
                                  
                               
                                Financing Your Education  
                                      Financial Aid  
                                  
                               
                                Apply for Readmission  
                                      Readmission Requirements  
                                      Application Deadline  
                                      Apply Now!  
                                  
                               
                                What to Expect  
                                      Acknowledgement  
                                      Application Review  
                                      Admission Decision  
                                  
                               
                                Transfer Credit  
                                      Equivalency Table  
                                       Transfer Credit Evaluation   
                                  
                               
                                 After Acceptance   
                                      Advising  
                                       Registration   
                                       ID Card   
                                       Parking   
                                       Fee Payment   
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Meet Your Counselor  
                               Our team can help you navigate the college planning process. 
                            
                            
                                #MEMBOUND?  
                               Sign up as a True Blue Tiger to receive information and updates. 
                            
                            
                                Campus Visits and Events  
                               Schedule a tour and view upcoming events. 
                            
                            
                                Contact Us  
                               Question?  The Office of Undergraduate Admissions and Orientation can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


