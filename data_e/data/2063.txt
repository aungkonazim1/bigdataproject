The University's "Great Expectations" of You - ACAD1100 - University of Memphis    










 
 
 
     



 
    
    
    The University's "Great Expectations" of You  - 
      	ACAD1100
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			ACAD1100
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          ACAD Courses  
                          Student Resources  
                          Faculty Resources  
                          Meet the Faculty  
                          Meet the Staff  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	ACAD1100
                              	  
                         The University's "Great Expectations" of You  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Great expectations of you. 
                     
                      Every new college student needs to be prepared for what the University will expect
                        from him or her. Introducing yourself into the University setting requires that you
                        abide by certain codes of conduct, both academically and morally. By accepting admittance
                        and enrolling, you have voluntarily committed yourself to be a part of the University
                        of Memphis community. With this commitment comes responsibility and a realization
                        that you are willingly applying yourself to the pursuit of elevated educational and
                        social goals. These include actively searching for higher knowledge, exhibiting behavior
                        that demonstrates respect towards other students, faculty, and staff of the University,
                        and generally following the University's procedural guidelines.
                      
                     
                       What are the major sources of documented information that new students receive from
                           the University of Memphis?  
                     
                      All students can access the online University Catalog, which contains degree program
                        planning and a list of all courses and areas of study, a Student Handbook, including
                        the regulations for academic and social conduct on campus, and a Schedule of Classes
                        for the current semester. It is the student's responsibility to read each of these
                        documents and understand which courses are required for his/her major, how courses
                        in a particular curriculum can be scheduled during a given semester, and what the
                        code of conduct is for all courses at this University. Because curriculum requirements
                        can change, the student is responsible for being knowledgeable about current information
                        to best complete a program. Regularly meeting with one's advisor is the best way to
                        avoid problems in this area.
                      
                     
                       What are specific academic expectations at the University of Memphis?  
                     
                      Success in college relies on the student's ability to utilize resources and to be
                        responsible for his/her own progress. Allowing study time of two to three hours for
                        each hour spent in class is recommended and cumulative GPAs that drop below a "C"
                        will be cause for academic suspension from the University once the student has earned
                        more than 68 hours. Financial Aid and Scholarships are also subject to review and
                        revocation depending on one's academic standing. Your ACAD section and the Educational
                        Support Program are instituted to help prevent academic hardship.
                      
                     
                       What is academic status?  
                     
                      The academic progress of students at the University of Memphis is monitored at the
                        conclusion of each term enrolled to determine their academic status. Students' overall
                        combined grade point average AND term grade point average are considered in determining
                        status. There are four progressive levels of academic status: (1) Good Standing (2) Academic Warning (3) Academic Probation (4) Academic Suspension Your academic status will be listed on your grade report in myMemphis at the end of
                        each semester. More information about academic levels can be found in the University
                        Catalog.
                      
                     
                       What are the basic educational requirements for completing a degree program?  
                     
                      Although one's particular major will determine the majority of courses that should
                        be taken, exposure to a variety of subjects is part of the General Education requirement.
                        Such courses ensure that the student becomes a well-educated member of society with
                        knowledge extending beyond the expertise nurtured in one area of study. This General
                        Education core curriculum was developed at the University of Memphis in 1989 to enrich
                        the undergraduate experience. In addition, students entering this University who demonstrate,
                        through testing, that they are not prepared for college-level work in math, English,
                        or reading must begin their studies with courses offered by the Transitional Academic
                        Studies Department.
                      
                     
                       What type of degree programs are available to students at the University of Memphis?  
                     
                      The University of Memphis offers undergraduates a choice of 54 majors and 73 concentrations.
                        Degrees available include Bachelor of Arts, Bachelor of Fine Arts, Bachelor of Music,
                        Bachelor of Business Administration, Bachelor or Liberal Studies, Bachelor of Professional
                        Studies, and a variety of Bachelor of Science programs. While most majors require
                        completion of at least 120 semester hours.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Register for ACAD1100  
                                
                            
                            
                                GPA Calculator  
                                
                            
                            
                                Contact Us  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/21/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


