Information for Survivors of Sexual Misconduct - Office of Student Conduct - University of Memphis    










 
 
 
     



 
    
    
    Information for Survivors of Sexual Misconduct - 
      	Office of Student Conduct
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Office of Student Conduct
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Academic Misconduct  
                          Sexual Misconduct  
                          Behavioral Intervention Team  
                          Code of Rights  
                      
                      
                         
                            Sexual Misconduct   
                            
                               
                                  
                                   Introduction  
                                   Notice of Non-Discrimination  
                                   Official Policy  
                                   Definitions  
                                   Enforcement of Policy  
                                   Communications  
                                   Information for Survivors  
                                   Reporting Misconduct  
                                   Prevention   Intervention  
                                   Sexual Misconduct Resources  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Office of Student Conduct
                              	  
                          
                              	Sexual Misconduct
                              	  
                         Information for Survivors of Sexual Misconduct 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Information for Survivors of Sexual Misconduct 
                     
                      
                        
                         Get to a safe place.
                           
                            
                              
                               Contact 911 if you are in immediate danger or you need assistance. If the incident
                                 occurred on campus, call University Police Services at 901.678.4357.
                               
                              
                            
                           
                         
                        
                         Call the University of Memphis Counseling Center.
                           
                            
                              
                               Counselors at the University Counseling Center  [http://www.memphis.edu/counseling /] provide confidential counseling services for students at the University of Memphis.
                                 If you wish to speak with a counselor after regular business hours, you may do so
                                 by calling 901.678.4357 (Police Services 24-hour emergency number). You will not be
                                 asked to provide information as to why you are requesting to speak with a counselor.
                               
                              
                            
                           
                         
                        
                         Contact the Shelby County Rape Crisis Center.
                           
                            
                              
                               Another resource available to you in the local community is the Shelby County Rape
                                 Crisis Center [ http://www.shelbycountytn.gov/index.aspx?NID=737 ] which can be contacted at 901.222.4450.
                               
                              
                            
                           
                         
                        
                         Seek medical attention.
                           
                            
                              
                               Any person who has been the victim of a sex-based offense should go directly to the
                                 emergency department of any local hospital for medical attention, collection of evidence,
                                 and treatment. Before going to the hospital, try to preserve any physical evidence:
                                 do not wash, bathe, go to the bathroom, or change clothing. It is best to collect
                                 evidence as quickly as possible; preferable within 72 hours; however, an exam can
                                 be completed up to five days after an assault. Regardless of whether or not you file
                                 a police report, you are encouraged to seek medical care in order to receive treatment
                                 and medication, if needed. Student Health Services at the University of Memphis cannot
                                 do the sexual assault exam, but their medical staff can provide important information
                                 about available resources and options you have for reporting the incident to the authorities
                                 and assist you in referral to an emergency department.
                               
                              
                            
                           
                         
                        
                      
                     
                      If you are not considering filing criminal charges, Student Health Center can attend
                        to certain medical concerns including physical exams, treatment of sexually transmitted
                        infections, and pregnancy testing. Student Health Center is available for assistance
                        Monday through Friday, 8:00 am to 4:30 pm and will keep your medical records confidential
                        to the extent possible and allowed by law.  [ http://www.memphis.edu/health ]
                      
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Sexual Misconduct 
                         
                            
                               
                                Introduction  
                                Notice of Non-Discrimination  
                                Official Policy  
                                Definitions  
                                Enforcement of Policy  
                                Communications  
                                Information for Survivors  
                                Reporting Misconduct  
                                Prevention   Intervention  
                                Sexual Misconduct Resources  
                            
                         
                      
                      
                      
                         
                            
                                Parking Citation Appeals  
                               Information on how to file a Parking Citation Appeal. 
                            
                            
                                Ethics In Action Online Workbook  
                               For UofM students who have been in contact with OSC. 
                            
                            
                                Resources  
                               OSC related information for our campus and community. 
                            
                            
                                Contact Information  
                               Here is how to reach the OSC staff and Student Court members. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


