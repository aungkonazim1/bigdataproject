Profiles in Philanthropy - Development - University of Memphis    










 
 
 
     



 
    
    
    Profiles in Philanthropy - 
      	Development
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Development
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Donate Now  
                          Giving Options  
                          Ways to Give  
                          Donor Recognition  
                          About Us  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Development
                              	  
                          
                              	Giving Options
                              	  
                         Profiles in Philanthropy 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Profiles in Philanthropy 
                     
                       The giving of women's talent and time continues to make a difference at the University
                           of Memphis.  
                     
                        Donna Abney  (BA '74, MBA '83) Ms. Abney has served on the board of directors for the University
                        of Memphis Research Foundation and has volunteered with our undergraduate leadership
                        program. In 1995, Ms. Abney joined Methodist Healthcare as a senior vice president
                        and rose to executive vice president of Methodist Le Bonheur Healthcare Systems. She
                        has taught healthcare courses at the University of Memphis and has helped secure significant
                        grant funding from Methodist Le Bonheur Healthcare Systems. She is a life member of
                        the Alumni Association. Ms. Abney was recently honored during the 15th annual Memphis
                        Business Journal Health Care Heroes awards ceremony. Click  here  to read the full announcement. She was also recognized at the Memphis Business Journal
                        "Super Women in Business" awards. This program honors women in different industries
                        who have achieved significant success through leadership in business and community
                        contribution. Click  here  to read the full announcement. When asked why she gives to the university she said,
                        "I am greatly indebted to the University of Memphis for the great education it provided
                        me. I learned so much in my MBA program which I apply every day. I am honored to help
                        the University in whatever way possible".
                      
                     
                        Pauline Weaver  (BA '71, JD '79) Ms. Weaver was the second woman elected president of the Student
                        Government Association (SGA). She is a major supporter of the Cecil C. Humphreys School
                        of Law and a member of the Society of Shield. The Student Bar Association office is
                        named for her. She was also a member of the Centennial 100 Committee of the Empowering
                        the Dream Centennial Campaign. She is now a Law Alumni Board Member (as of 2013) and
                        was named one of the 100 Women Who Make a Difference (2012). She is retired from the
                        public defender's office and has opened her own firm, Law Office of Pauline Weaver.
                        She is a previous member of the ABA Board of Governors. She now serves as 2014 National
                        Law Day Chair for the ABA. Summer 2013, she was elected Treasurer of the Government
                        and Public Sector Lawyers Division for the ABA and was the recipient of the Nelson
                        Award from the Division for outstanding service to the ABA. She is a member of Alpha
                        Gamma Delta Fraternity and was named a Distinguished Citizen in 1991. Ms. Weaver was
                        named Outstanding Alumnae for the University in 1993. When asked why she gives to
                        the university she said, "As an undergraduate, the University gave me the educational
                        opportunity, the maturity, the skills and the courage to pursue my dreams. I really
                        blossomed in college and learned that I could do whatever I wanted to do. Whatever
                        I have become, I owe to the University and the Law School. I got an incredible education
                        without incurring a mountain of debt and my giving back is a way of making sure that
                        someone else gets to pursue their dreams".
                      
                     
                         Anita Vaughn  (BA '77, MPA '82) Ms. Vaughn, CEO of Baptist Memorial Hospital for Women, was selected
                        as Outstanding Alumna of the College of Arts and Sciences in 2005. She currently serves
                        on the University of Memphis LEAD Advisory Board. She is president of the National
                        Alumni Association. She previously served on the College of Arts and Sciences Advisory
                        Board and the Board of Trustees. She is a life member of the Alumni Association. She
                        was just selected for the Presidential Search Committee by the Board of Regents. Anita
                        states, "I support the University for many reasons. It offered flexibility and convenience
                        allowing me to continue working full time and while obtaining 2 quality degrees. The
                        University is a gem in the crown of Memphis. It is a beautiful campus. I am amazed
                        and love the unique research projects that I have learned about since being involved
                        in the Boards. I am proud of the partnership Baptist Women's has with the internships
                        from the Health Care Administration Masters Program. We have had such quality students.
                        I have wanted to hire all of them. A tremendous thanks for all the University does
                        for our city and students. GO TIGERS!!!"
                      
                     
                        Laurie Tucker  (BBA '78, MBA '83) Ms. Tucker as been one of the University of Memphis' most consistent
                        supporters. Laurie is senior vice president of corporate marketing for FedEx, a member
                        of the University's Board of Visitors and the Foundation Board of Trustees, and a
                        member of the Fogelman College Executive Advisory Board. She was also a member of
                        the Centennial 100 Committee of the Empowering the Dream Centennial Campaign. Click
                         here  to view the University of Memphis' Centennial publication featuring Laurie. When
                        Ms. Tucker walks across the University of Memphis campus, she is struck by the incredible
                        changes that have occurred since she was a student. "Campus development has been so
                        significant in the past decade," she says proudly. "I wish all of our alumni could
                        see how impressive it is now. The campus itself is so beautiful, and the programs
                        are really something to be proud of." When asked why she supports the university she
                        said, "It has been a joy to serve my Alma Mater as a member of the Board of Visitors
                        during the past fourteen years. I am inspired by the growth and innovation I see on
                        campus, and by the dedication of the leadership at the University of Memphis. Giving
                        back to my University provides me with a small way of paying forward the success of
                        others who will also benefit from their education at the UofM."
                      
                     
                        Rita Sparks  Ms. Sparks exemplifies the power of women's leadership and philanthropy. In 2010,
                        she received both the  University of Memphis Distinguished Friend Award  and the Tennessee Chancellor's Award for Excellence in Philanthropy for her support
                        of higher education. She was the also the Memphis recipient of the 1999 Outstanding
                        Volunteer Fund Raiser from the National Society for Fund Raising Executives and a
                        member of the 1996 Leadership Memphis class. Ms. Sparks has been a licensed commodity
                        broker since 1980. In 1977, she and her late husband, Willard, formed Sparks Companies
                        Inc., an agricultural research information and consulting firm. After Willard's death
                        in 2005, she formed her own investment company, Sparks Enterprises Inc. Ms. Sparks
                        has also held several key accounting positions in the retail business and in the commodities
                        field. She serves on the board of Metropolitan Bank, the University of Memphis Board
                        of Visitors, the UofM Foundation Board of Trustees, the Tennessee Sports Hall of Fame,
                        and is an ambassador for the UofM's Athletic Department. She is involved in a variety
                        of community projects. She has served on the boards of the Memphis Development Foundation,
                        the Baddour Center, the Memphis Redbirds Foundation and the Campbell Foundation. She
                        is a past president of the Ronald McDonald House and was instrumental in securing
                        the funds to build the first Ronald McDonald House in Memphis.
                      
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Staff Directory  
                               Meet the Development Staff 
                            
                            
                                University of Memphis Foundation  
                               Learn about the University of Memphis Foundation 
                            
                            
                                Alumni Association  
                               Learn about the Alumni Association 
                            
                            
                                Glossary  
                               Click here for a Glossary of Terms 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/26/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


