Air Force ROTC - Air Force ROTC - University of Memphis  Air Force ROTC  










 
 
 
     



 
    
    
    Air Force ROTC - 
      	Air Force ROTC
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Air Force Reserve Officer Training Corps
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Prospective Cadets  
                          Cadre   Staff  
                          Photo Gallery  
                          Scholarships  
                          Resources   Links  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                         
                           	Air Force ROTC
                           	
                         
                      
                   
                   
                      
                         
                              
                             
                            
                             
                            
                            
                                
                            
                         
                         
                             
                         
                      
                      
                     		
                     
                     
                      DETACHMENT 785 Flying Tigers Welcomes You 
                     
                        
                     
                        
                     
                      The Department of Aerospace Studies provides a four-year program of instruction for
                        all qualified U.S. citizens, male and female, divided into two phases, each of two
                        years duration. The first, termed the General Military Course, offers instruction
                        in the foundation of leadership and Aerospace-age citizenship. Secondly, termed the
                        Professional Officer Course, builds upon these foundations in developing upperclassmen
                        who are to become Air Force officers and serve on active duty upon graduation and
                        commissioning.
                      
                     
                      Instruction in Aerospace Studies has been an important phase of the curriculum at
                        the University of Memphis since 1951. Active duty Air Force personnel, approved by
                        the University president, are detailed by the Department of the Air Force to administer
                        the instructional program. Air Force officers serve under appointment by the University
                        as Professor or Assistant Professor of Aerospace Studies.
                      
                     
                      Please take a moment and explore our website, and for more information visit the websites
                        below:
                      
                     
                      
                        
                          http://www.af.mil  
                        
                          http://www.airforce.com  
                        
                          http://www.au.af.mil/au/holmcenter  
                        
                      
                     
                      Feel free to contact us if you have any questions! 
                     
                      Air Force ROTC-Detachment 785 
                     
                      Phone: 901.678.2681 
                     
                      Email:  afrotc@memphis.edu  
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                DET 785 Brochure  
                               What's it like being in the Air Force Reserved Officer Training? Can I get a scholarship?
                                 These questions and more are answered here...
                               
                            
                            
                                Careers and Pay  
                               Air Force ROTC prepares you to become an Officer in the United States Air Force. Find
                                 out more about your promising future as an Officer.
                               
                            
                            
                                Privacy and Security Notice  
                               Information presented on the Detachment 785 Website is considered public information
                                 and may be distributed or copied.
                               
                            
                            
                                Contact Us  
                               Main office contact and location. 
                            
                         
                      
                      
                      
                      
                         Follow Us Online 
                         
                              Facebook   
                         
                                         
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/9/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


