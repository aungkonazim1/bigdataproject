Campus Updates &#8211; The University of Memphis President&#039;s Blog   


 

 
 
 
 


 
 
 
 

 



 Campus Updates   The University of Memphis President s Blog 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 



 
 
  
 
 
 

 
 
 
 
			

         
 		
		



 
 
 
 

 

 


  Skip to content  




 

	
	 

		
		 

			
			 

				
						  The University of Memphis President s Blog  

					
			  

		  

		
		 

			 Primary Menu 
			 Menu 

			
			 
				 
					 
				 
					 Search for: 
					 
				 
				 
			 				 
			 

			 

			  
  Home    About the President  
  

		  

		 

		
		
			
				 

			
		
		 

		
	  

	
	 
	 

		
		 

			
			
	
	 

		
		
		 

			
			 Campus Updates 
			
			 

				   Author   Jeanine Hornish Rakow     Published on    July 8, 2016     Leave a comment  
			  

			
		  

		
		 

			 Dear Faculty and Staff: 
 Last week we analyzed the UofM graduation numbers and rates since 2000, revealing consistent and steady improvement over the past decade. From 2000-2003, the average annual number of graduates was 3,125. From 2008-2010, the annual average improved 16% to 3,631. Over the past three years we have experienced record numbers, with each year exceeding 4,000. From 2014-2016, the average annual number of graduates has reached 4,229, a 16% increase over the previous three-year average from 2008-2010. As you might agree, that’s remarkable progress over the past decade and a half. We still have some work to do to reach our goal of a graduation rate well above the national average, but we are making excellent progress. At the heart of our mission is not only maintaining access to an affordable, high-quality, comprehensive college experience for our students, but also making sure our students are successful in finishing what they have started and launching a commitment to lifelong learning. Thank you for your hard work and dedication to our students and community. The evidence indicates your efforts are paying off, helping to develop students prepared to contribute fully to a democratic society and shape the lives of thousands of students and families. 
 I also want to share our success with private fundraising for the University of Memphis. As you know, last year we set a record with  $36,774,472  raised. This year, our team surpassed that with a total of  $38,536,048  raised. I want to thank our Chief Development Officer Bobby Prince and his team for an exceptional job, with two consecutive record years. Needless to say, their efforts have profound impact on our university, community and the lives of many, many students. 
 Thank you again for all you do for the University of Memphis and our community. I hope the summer months are going well, and I look forward to the start of another academic year in late August. 
 
 
 
 M. David Rudd 
President | Distinguished University Professor 
 
 
 

		  

		
		 

			  Published on    July 8, 2016      Author   Jeanine Hornish Rakow   
			
		  

		
	  

	
				
	 
		 Post navigation 
		    Previous article:  TBR Approves Tuition Increase for 2016-17      Next article:  Campus Response to Recent Violence    
	 
				

 

	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  


			
			
		  

		
	  


	
		
		
		 

		 Main Sidebar 

			
			  
				 
					 Search for: 
					 
				 
				 
			  		 		 Recent Posts 		 
					 
				 UofM Quality Indicator Score 
						 
					 
				 University of Memphis Research Foundation Announces Grand Opening of Student-Operated FedEx Call Center 
						 
					 
				 Pause to Grieve 
						 
					 
				 University of Memphis Magazine: Fall 2017 
						 
					 
				 Campus Update 
						 
				 
		 		  Recent Comments      Archives 		 
			  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  July 2014  
	  June 2014  
	  May 2014  
	  April 2014  
		 
		   Categories 		 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 
			
		  

		
		  

	
	
	 

		
		 Footer Content 

		 
		
			
				
				
								
			
		  
	
		 

			
			
			Using  Tiny Framework     
			
			   Log in  

		  
		
		 

			
			

 

	 Social Links Menu 

	      i class= fa fa-twitter   /i    
  
  

			
		  

		
	  

	
  


        

        









		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 
 