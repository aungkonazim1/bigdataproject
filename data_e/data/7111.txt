Viewing CaptureCast presentations offline and in mobile playback | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Viewing CaptureCast presentations offline and in mobile playback | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Capture    »    Participating in CaptureCast presentations   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Viewing CaptureCast presentations offline and in mobile playback                       
        
        
       
          
     
           Printer-friendly version       
	
 

 
	You can download MP3 and MP4 versions of an on-demand CaptureCast presentation to your personal computer or mobile device after it is published to Capture Portal.
 

 
	 Note Depending on your institution's Capture configuration, users might not have permission to download CaptureCast presentations or access Capture Portal from a mobile device. If a presentation does not have the download option available, or if you cannot view the mobile Capture Portal interface, contact your instructor or Capture administrator.
 

 
	Download an on-demand CaptureCast presentation
 

  
		Navigate to your Capture Portal from your computer or mobile device and log in.
	 
	 
		On the Home page or Published Events page, select the presentation you want to download.
	 
	 
		On the presentation viewing page, click  Download (MP4/480p)  for video and audio, or  Download (MP3)  for just audio.
	 
	 
		Save the file.
	 
  
	 Note The Capture Portal mobile interface and offline viewing interface display video and slide layouts without the option to toggle or rearrange views.
 

 
	View a CaptureCast presentation from a mobile device
 

  
		Navigate to your Capture Portal from your mobile device and log in.
	 
	 
		On the Home page or Published Events page, select the presentation you want to view.
	 
	 
		Your presentation should start automatically. If it does not start, click the    Play  icon.
	 
  
	 Note The Capture Portal mobile interface and offline viewing interface display video and slide layouts without the option to toggle or rearrange views.
 

 
	
 
     Audience:    Learner      

    
           

                   ‹ Viewing a CaptureCast presentation 
        
                   up 
        
                   Participating in a live event chat › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Capture  
  
      Participating in CaptureCast presentations    Accessing Capture Portal    Viewing a CaptureCast presentation    Viewing CaptureCast presentations offline and in mobile playback    Participating in a live event chat    Adding comments and replies to a CaptureCast presentation      Understanding Capture components    Using Capture    Capture Station    Editing in post-production    Capture Central in Learning Environment    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
