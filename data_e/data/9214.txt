Earthworm System: Pidpau commands   
 
 Earthworm System: Pidpau commands 
 

 
    
    Pidpau Configuration File 
   (last revised May 25, 2006) 
 

 If one isn't specified as an optional parameter, Pidpau uses startstop's configuration
  file, startstop*.d to enable it to attach to the shared memory regions.
 Pidpau needs the process id (PID) of the module it's attempting to stop. You
  can find out the process id by getting the status from typing Enter in an interactive
  startstop session, or by running the 'status' command, or by looking at UNIX's
  process list (with ps) or by looking at Windows Task Manager with display of
  Process ID activated.
 
   Usage:  pidpau  process_id   optional:startstop config file   
 
 
 

 
   
     Module Index  |  Pidpau
    Overview 
   
   
   
Contact:   Questions? Issues?  Subscribe to the Earthworm Google Groups List.     
 
 
