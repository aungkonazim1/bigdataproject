Surveys reports | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Surveys reports | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Insights    »    Standard reports   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Surveys reports                       
        
        
       
          
     
           Printer-friendly version       
	Survey reports enable survey creators to gather all data for survey submissions in one document. This report is available in regular and tabular view. Use survey reports to view user submissions for both anonymous and non-anonymous surveys.
 

 
	Individual Survey Responses
 

 
	Displays individual survey responses for both shared, and course offering specific, surveys. This report is available in the Insights Portal and the Insights Console.
 

 
	The report can support the following question types:
 

  
		True/False
	 
	 
		Multiple Choice
	 
	 
		Long Answer
	 
	 
		Short Answer
	 
	 
		Likert
	 
  
	  

 
	Individual Survey Responses report
 

 
	  

 
	Individual Survey Responses report - tabular view
 
     Audience:    Instructor      

    
           

                   ‹ Rubrics reports 
        
                   up 
        
                   Tool Access reports › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Insights   
  
      Using Insights Portal    Standard reports    Standard reports    Accreditation reports    Competencies reports    Corporate reports    Engagement reports    Enrollments reports    Quizzing reports    RiskAnalysis reports    Rubrics reports    Surveys reports    Tool Access reports      Data Mining reports    Understanding the Student Success System    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
