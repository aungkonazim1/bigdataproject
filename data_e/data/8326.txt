Financial Aid Programs - Financial Aid - University of Memphis    










 
 
 
     



 
    
    
    Financial Aid Programs - 
      	Financial Aid
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Financial Aid
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Undergraduate  
                          Graduate  
                          Law  
                          Scholarships  
                          Student Employment  
                      
                      
                         
                            Graduate   
                            
                               
                                  
                                   When to Apply  
                                   How to Apply  
                                   Aid Programs  
                                   Eligibility  
                                   Study Abroad  
                                   Grad Certificate Program  
                                   Teachers Certificate Program  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Financial Aid
                              	  
                          
                              	Graduate
                              	  
                         Financial Aid Programs 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Aid Programs - Graduate Students 
                     
                      Once financial aid eligibility is determined, the Financial Aid Office offers students
                        a Financial Aid Package which is posted on their  myMemphis  account.  It consists mainly of student loans to meet as much of the student's need
                        as possible. Enrolled hours, residency classification, cumulative grade point average,
                        and other financial assistance already received or expected will affect the amount
                        and type of financial aid students can receive to meet their financial need.  All
                        students must be admitted to an eligible degree seeking program for Federal Student
                        Aid eligibility.  Students seeking an additional degree must be admitted to a new
                        eligible degree program for aid eligibility.
                      
                     
                      Loans    
                     
                       Note: Effective 7/1/2012, Subsidized Stafford Loans are no longer available to graduate/professional
                           students.    
                     
                      Unsubsidized Stafford Loan    
                     
                      A variable interest loan available to degree seeking students enrolled at least half-time
                        (5 or more graduate level credit hours), not based on financial need. The interest
                        rate changes every July 1st and will be charged from the time the loan is disbursed
                        until it is paid in full. The interest can be paid while the student is still in school.
                        Repayment of principal and interest begins 6 months after enrollment ceases to be
                        at least half-time.   The current interest rate on this loan is 6% .  
                     
                       MAXIMUM LOAN LIMITS for an ACADEMIC YEAR:   
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  
                                    
                                     
                                       
                                        
                                            Graduate Academic Year    Loan Limits  
                                        
                                       
                                        
                                           Total Unsub Loan 
                                        
                                       
                                        
                                          
                                           
                                             
                                              $20,500 
                                             
                                           
                                          
                                        
                                       
                                     
                                    
                                  
                                 
                               
                              
                                 
                              
                               
                                 
                                  
                                    
                                     
                                       
                                        
                                            Graduate Aggregate Loan Limits  
                                        
                                       
                                        
                                           Base Loan 
                                           Base + Extended 
                                        
                                       
                                        
                                          
                                           
                                             
                                              $65,500 
                                             
                                           
                                          
                                           
                                             
                                              $138,500 
                                             
                                           
                                          
                                        
                                       
                                     
                                    
                                  
                                 
                               
                              
                            
                           
                         
                        
                      
                     
                        NOTE   :  
                     
                      
                        
                         Borrowing the maximum amount for Fall and Spring leaves zero eligibility for Summer. 
                        
                         Your total financial aid, including Federal Direct Loans, cannot exceed your cost
                           of attendance or "budget" (as determined by the institution).
                         
                        
                          The aggregate lifetime limit includes the total amount of undergraduate loans borrowed.  
                        
                         Loans are awarded per academic year and students are not eligible to receive more
                           than half a year's limit in one semester. For example, a graduate student may not
                           receive more than $10,250 in Loans (half of $20,500) during one semester.
                         
                        
                         Monitor your loan history on the National Student Loan Data System  here .
                         
                        
                      
                     
                         Graduate PLUS Loan    
                     
                      A supplemental loan made available to graduate or professional students who need to
                        borrow more than the maximum unsubsidized loan amounts to meet educational costs that
                        are not covered by other financial aid.  Students interested in this loan must complete
                        the FAFSA and must be enrolled at least half time (5 or more graduate level credit hours) in
                        a graduate or professional program.  Students in the teacher certification program
                        do not qualify. There is no grace period for the Graduate PLUS loan.   The interest rate for the PLUS loan is fixed at 7.0% and a credit check is required  .
                      
                     
                       The Graduate PLUS application must be completed on-line:   http://www.studentloans.gov .
                      
                     
                        
                     
                      TEACH Grant 
                     
                      A grant available to students who intend to teach full time for at least four years
                        in high-need subject areas in a public or private elementary or secondary school that
                        serves students from low income families. This is available to only two majors at
                        the University of Memphis: Master of Arts in Teaching (MAT) Degree Program and Master
                        of Science (MS) in Education Degree Program, with a concentration in Reading.
                      
                     
                      Work Programs    
                     
                      Federal Work Study and Regular Student Employment    
                     
                      For more information, you may view the  Student Employment  web site.
                      
                     
                      Graduate Assistantship (GA) Work Study    
                     
                      A federally funded, need based, university administered program. Priority given to
                        early applicants with financial need (as determined by the FAFSA). Academic department
                        actually awards assistantship. Work study award can be used to fund the stipend amount.
                        Federal government covers 75% while the academic department covers 25% of monthly
                        stipend amount. Must be a U.S. citizen or eligible non-citizen to qualify for this
                        program.
                      
                     
                      Scholarships    
                     
                      The Student Financial Aid Office only handles undergraduate scholarship programs.
                        Check  Graduate Assistantships and Fellowships  at the  Graduate School  web site for information on what graduate scholarship programs are available at The
                        University of Memphis.
                      
                     
                      Many foundations, corporations, and service organizations provide funds for scholarships
                        or tuition assistance. Students generally apply directly to the potential donor, and
                        usually there is a direct relationship between the student and the sponsoring organization.
                         FastWEB  is a searchable database for more than $1 billion in private sector scholarships,
                        fellowships, grants and loans.
                      
                     
                      Alternative Loans    
                     
                      Private loans is an option for graduate students who do not show financial aid eligibility
                        for other types of financial aid, or who need additional funds to meet educational
                        expenses. Most private alternative loans require a credit check.
                      
                     
                      Many of these alternative loan programs also look at what financial aid you are already
                        receiving for that particular period. Other financial aid assistance is deducted from
                        the cost of attendance (as determined by the institution) before aid eligibility is
                        determined for other programs.  The following link includes the names and contact
                        information of lenders frequently used by students.
                      
                     
                      Partial list of  Alternative Loan  Lenders
                      
                     
                       NOTE:  Our office does NOT endorse any particular lender. This information is only provided
                        for your convenience and can change without notice. You should request current information
                        directly from the lender or company offering the program.
                      
                     
                        IMPORTANT NOTICES:   The Federal Tax Reform Act of 1986 significantly changed the tax status of scholarships,
                        fellowships, and grants you receive from the university. Prior to the Act, they were
                        generally not subject to income tax. Since then scholarship, fellowship, and grant
                        amounts exceeding tuition, fees, and course-related books, supplies, and equipment
                        have become taxable.
                      
                     
                      Financial assistance received from other sources includes vocational rehabilitation
                        benefits, veteran's benefits, outside scholarships, employee fee waivers, teacher/child
                        tuition discounts, graduate assistant fee waivers and any other assistance. All outside
                        aid amounts must be considered in determining total aid eligibility.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Graduate 
                         
                            
                               
                                When to Apply  
                                How to Apply  
                                Aid Programs  
                                Eligibility  
                                Study Abroad  
                                Grad Certificate Program  
                                Teachers Certificate Program  
                            
                         
                      
                      
                      
                         
                            
                                Financial Aid Forms  
                               Manage your forms in myMemphis 
                            
                            
                                Financial Aid Checklist  
                               Step-by-step guide to receiving financial aid 
                            
                            
                                Frequently Asked Questions  
                               Answers to the most common questions 
                            
                            
                                Contact Us  
                               Phone: 901.678.4825  |  financialaid@memphis.edu 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


