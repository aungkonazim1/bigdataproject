Accessing the Evaluate Submission page | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Accessing the Evaluate Submission page | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Dropbox    »    Evaluating dropbox folder submissions   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Accessing the Evaluate Submission page                       
        
        
       
          
     
           Printer-friendly version       
	On the Folder Submissions page, click the  Evaluate  link beside the name of the user you want to leave feedback for.
 
     Audience:    Instructor      

    
           

                   ‹ Viewing dropbox folder file submissions 
        
                   up 
        
                   Leaving feedback and grading dropbox submissions › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Dropbox  
  
      Dropbox basics    Creating and managing Dropbox    Evaluating dropbox folder submissions    Viewing dropbox folder file submissions    Accessing the Evaluate Submission page    Leaving feedback and grading dropbox submissions    Evaluating non-submissions and external submissions    Bulk publishing dropbox feedback evaluations    Downloading dropbox folder submission files    Uploading and attaching feedback from downloaded submission files    Retracting published feedback    Emailing users from a dropbox folder    Marking dropbox folder submissions as read or unread    Flagging dropbox folder submissions    Using Dropbox GradeMark    Using Dropbox OriginalityCheck      
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
