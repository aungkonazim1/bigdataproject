Marketing Manager Assignments - Marketing and Communication - University of Memphis    










 
 
 
     



 
    
    
    Marketing Manager Assignments - 
      	Marketing and Communication
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Marketing and Communication
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          The Brand  
                          Our Team  
                          Resources  
                          Social Media  
                          Web  
                          Events  
                          News  
                      
                      
                         
                            Our Team   
                            
                               
                                  
                                   What We Do  
                                   Working Together  
                                   Marketing Manager Assignments  
                                   Project Timelines  
                                   Project Request Form  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Marketing and Communication
                              	  
                          
                              	Our Team
                              	  
                         Marketing Manager Assignments 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Marketing Manager Assignments 
                     
                      When planning for or looking to begin a new project with the Office of Marketing and
                        Communication, your marketing manager should be your first point of contact. Search
                        for your college, school, division or area below to begin.
                      
                     
                       Susan Prater  Director of Marketing and Communication 901.678.5581  srprater@memphis.edu  
                     
                      University of Memphis UofM Lambuth Office of the President Office of Board Services/Board of Trustees Government Relations UofM Athletics Advancement/Development Enrollment Services Internal Audit Office of Institutional Research Office of Legal Counsel UofM Global
                      
                     
                       Jazmine Phillips  Marketing Manager 901.678.3577  jpphllp2@memphis.edu  
                     
                      UofM Lambuth Cecil C. Humphreys School of Law Loewenberg College of Nursing School of Communication Sciences and Disorders Alumni Association Student Affairs School of Health Studies School of Public Health Center for International Education Services Annual Giving Planned Giving Office of Institutional Equity Enrollment Services*
                      
                     
                        
                     
                       Cole Pinner  Marketing Manager 901.678.2387  mpinner@memphis.edu  
                     
                      Herff College of Engineering Fogelman College of Business   Economics College of Communication and Fine Arts Rudi E. Scheidt School of Music University College Academic Affairs College of Arts and Sciences Kemmons Wilson School of Hospitality and Resort Management FedEx Institute of Technology College of Education Graduate School Business and Finance University Libraries ITS
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Our Team 
                         
                            
                               
                                What We Do  
                                Working Together  
                                Marketing Manager Assignments  
                                Project Timelines  
                                Project Request Form  
                            
                         
                      
                      
                      
                         
                            
                                Media Resources  
                               News, Publications and Upcoming Events 
                            
                            
                                Brand Standards  
                               Everything you need to know about utilizing the UofM brand 
                            
                            
                                Marketing Toolkit  
                               Download templates, find a vendor and more 
                            
                            
                                Contact Us  
                               Let us help with your marketing needs 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


