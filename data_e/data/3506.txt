Law School&#8217;s Experiential Learning Program Has Stellar Year &#8211; The University of Memphis President&#039;s Blog   


 

 
 
 
 


 
 
 
 

 



 Law School s Experiential Learning Program Has Stellar Year   The University of Memphis President s Blog 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 



 
 
  
 
 
 

 
 
 
 
			

         
 		
		



 
 
 
 

 

 


  Skip to content  




 

	
	 

		
		 

			
			 

				
						  The University of Memphis President s Blog  

					
			  

		  

		
		 

			 Primary Menu 
			 Menu 

			
			 
				 
					 
				 
					 Search for: 
					 
				 
				 
			 				 
			 

			 

			  
  Home    About the President  
  

		  

		 

		
		
			
				 

			
		
		 

		
	  

	
	 
	 

		
		 

			
			
	
	 

		
		
		 

			
			 Law School s Experiential Learning Program Has Stellar Year 
			
			 

				   Author   Jeanine Hornish Rakow     Published on    June 2, 2014  May 30, 2014     Leave a comment  
			  

			
		  

		
		 

			 As we highlight recent student successes all over our campus, I am pleased to share that a record number of law students earned academic credit and valuable legal experience this year through The Cecil C. Humphreys School of Law’s Experiential Learning Program. 
 The Law School’s Experiential Learning Curriculum centers around two distinctive course offerings: The University of Memphis Legal Clinic, a professional law office housed within the walls of the law school, and The University of Memphis Externship Program.  Among the 2013-14 graduating classes, some 104 students -- or 78% of the overall class -- completed either a Clinic or Externship, and 39% completed more than one Clinic or Externship.  Including the 27 students currently enrolled in the Summer Externship Course, Memphis Law students are on pace to complete 139 Externship field placements in Memphis and across Tennessee during the 2013-14 academic year. 
 According to Professor Daniel Schaffzin, the Law School’s Director of Experiential Learning, these statistics are consistent with trends across the national law school landscape and suggest that our law students have never been more engaged in learning through experience. 
 “Students emerge from experiential learning courses that much more ready to practice law and to meet the high expectations of the future clients and employers on whose behalf they will work following graduation,” said Professor Schaffzin.  “As evidenced by the remarkable year we have had and the tremendous growth of our experiential learning curriculum over the last several years, I think that both our students and devoted community legal partners are seeing the advantages of offering a robust set of Legal Clinic and Externship courses.  The Law School is well-positioned to continue building the community collaborations that form the core of our experiential classroom.” 
 Special thanks to Professor Donna Harkness (Director of the Elder Law Clinic), Professor Chris Zawisza (Director of the Child and Family Litigation Clinic), and Professor Steve Shields (Director of the Mediation Clinic) for their incredible teaching, inspired lawyering, and hard work within the University of Memphis Legal Clinic, and to Sandy Love, the Legal Clinic’s Administrative Assistant for her tireless devotion to the Experiential Learning Program.  And I particularly want to recognize Professor Schaffzin for his leadership of a program that brings distinction to the university and advances our commitment to the success of the Memphis community. 
 You can learn more about the University of Memphis School of Law's Experiential Learning Program at  http://www.memphis.edu/law/experiential/index.php.  
 Go Tigers! 
 M. David Rudd, President 

		  

		
		 

			  Published on    June 2, 2014  May 30, 2014      Author   Jeanine Hornish Rakow   
			
		  

		
	  

	
				
	 
		 Post navigation 
		    Previous article:  Looking good, Lambuth Campus!      Next article:  Committed to Healthy Living at the UofM    
	 
				

 

	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  


			
			
		  

		
	  


	
		
		
		 

		 Main Sidebar 

			
			  
				 
					 Search for: 
					 
				 
				 
			  		 		 Recent Posts 		 
					 
				 UofM Quality Indicator Score 
						 
					 
				 University of Memphis Research Foundation Announces Grand Opening of Student-Operated FedEx Call Center 
						 
					 
				 Pause to Grieve 
						 
					 
				 University of Memphis Magazine: Fall 2017 
						 
					 
				 Campus Update 
						 
				 
		 		  Recent Comments      Archives 		 
			  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  July 2014  
	  June 2014  
	  May 2014  
	  April 2014  
		 
		   Categories 		 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 
			
		  

		
		  

	
	
	 

		
		 Footer Content 

		 
		
			
				
				
								
			
		  
	
		 

			
			
			Using  Tiny Framework     
			
			   Log in  

		  
		
		 

			
			

 

	 Social Links Menu 

	      i class= fa fa-twitter   /i    
  
  

			
		  

		
	  

	
  


        

        









		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 
 