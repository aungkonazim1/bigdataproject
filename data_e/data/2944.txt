Beale Street Caravan - WUMR - University of Memphis    










 
 
 
     



 
    
    
    Beale Street Caravan - 
      	WUMR
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			WUMR
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          On-Air  
                          DJ Profiles  
                          News  
                          Supporters  
                          Underwriting  
                          CCFA  
                      
                      
                         
                            Specialty Shows   
                            
                               
                                  
                                   Anything Goes  
                                   Beale Street Caravan  
                                   Focus on the Arts  
                                   Jazz 10 Countdown  
                                   Jazz 20 Countdown  
                                   Let's Talk Health  
                                   New Urban Jazz Lounge  
                                   Radio Deluxe  
                                   Riffin' On Jazz  
                                   Sportsdesk  
                                   Spotlight 92  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	WUMR
                              	  
                          
                              	On-Air
                              	  
                         Beale Street Caravan 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Beale Street Caravan 
                     
                       WUMR broadcasts the locally produced  Beale Street Caravan . One of the most widely distributed Blues radio programs in the world, Beale Street
                        Caravan airs on WUMR every Wednesday at 6 p.m.
                      
                     
                      Each week, Beale Street Caravan brings its audience the music and the stories from
                        the greatest Blues venues in Memphis and the United States. Musicians are featured
                        in intimate performances and recordings at events such as the W.C. Handy Blues Awards
                        in Memphis, the Telluride Blues and Brews Festival in Telluride, Colorado; the Mississippi
                        Valley Blues Festival in Davenport, Iowa; the North Atlantic Blues Festival in Portland,
                        Maine; and the King Biscuit Blues Festival in Helena, Arkansas. The program's lively
                        format delivers great music from the biggest names in Blues like legendary harpist
                        Charlie Musselwhite, Texas music master Clarence "Gatemouth" Brown, guitar-great Joe
                        Louis Walker, and singing sensation Shemekia Copeland.
                      
                     
                      Pat Mitchell, the show's host, is joined each week by some of the most respected people
                        in the industry. Past feature hosts include: Sam Phillips, Jerry Wexler, Peter Guralnick,
                        John Hammond, Jecssica Harris, Richard Hite and many others.
                      
                     
                      On the roster for the upcoming shows are musicians such as Porter, Batiste, and Stoltz;
                        The Derek Trucks Band; Drive By Truckers; and Cyril Neville   Tribe 13. Feature hosts
                        include Cybill Shepherd, Bob Porter, and Robert Gordon.
                      
                     
                      Beale Street Caravan is a not-for-profit organization located in Memphis. Memphis
                        musician Sid Selvidge is the show's executive producer and Pat Mitchell is the host.
                        Beale Street Caravan is sponsored by various civic organizations of the City of Memphis
                        as well as by private foundations and individuals.
                      
                     
                      For more information check out:  BealeStreetCaravan.com  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Specialty Shows 
                         
                            
                               
                                Anything Goes  
                                Beale Street Caravan  
                                Focus on the Arts  
                                Jazz 10 Countdown  
                                Jazz 20 Countdown  
                                Let's Talk Health  
                                New Urban Jazz Lounge  
                                Radio Deluxe  
                                Riffin' On Jazz  
                                Sportsdesk  
                                Spotlight 92  
                            
                         
                      
                      
                      
                         
                            
                                Listen Now!  
                               Tune into U92 online 24/7! 
                            
                            
                                Make A Gift  
                                
                            
                            
                                Listener Request Line  
                               (901) 678-4867 
                            
                            
                                Contact WUMR U92 FM  
                               (901) 678-2560 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


