Deleting learning objectives | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Deleting learning objectives | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Competencies    »    Creating and managing competency structure elements   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Deleting learning objectives                       
        
        
       
        
     
              
	The following learning objective types are disabled and cannot be deleted:
 

  
		Learning objectives attached to an Approved or Archived competency (deletion would change the competency’s definition).
	 
	 
		Learning objectives shared to you from another org unit.
	 
  
	Deleting elements that are part of a competency structure can cause re-evaluations. If you delete all learning objectives associated with a competency, users who completed the competency will have their results changed to  Incomplete . This is because an element is only considered complete if all of its children are complete; elements that have no children cannot be completed.
 

 
	Delete a learning objective
 

  
		On the Competency Home page, click    Delete  from the More Actions button.
	 
	 
		Select the check boxes beside the learning objectives you want to delete.
	 
	 
		Click  Delete Selected .
	 
      Audience:     Instructor       

    
           

                   ‹ Managing learning objectives 
        
                   up 
        
                   Creating activities › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Competencies  
  
      Competency structure basics    Automating competency structure evaluation    Creating and managing competency structure elements    Understanding competency status settings    Creating competencies    Managing competencies    Deleting competencies    Creating learning objectives    Managing learning objectives    Deleting learning objectives    Creating activities    Adding associations between competency structure elements    Removing associations between competency structure elements    Sharing competency structures    Tracking competency versions    Viewing competency structure results    Overriding competency structure results    Managing independent learning objectives and independent activities      Evaluating competency structure activities    
                  
           
         

       
     

    
    
    
   
 
   
 
