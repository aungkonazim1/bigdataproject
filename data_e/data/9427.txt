Earthworm Modules: <P>activated_scripts overview  
 
 
 Earthworm Modules:  activated_scripts overview 
 
 
  Earthworm Module:  activated_scripts Overview 
 (last revised 17 May, 2011)  
When the activated_scripts module reads a message from the specified ring, the body of which
is a list of command-line arguments, it executes a sequence of scripts using those arguments,
and writes a completion message to a ring when done. 
 
 Module Index  |  Activated_scripts Commands 

 

 
 
 
Contact:    Questions? Issues?  Subscribe to the Earthworm Google Groups List.     
 
 
 
