Employment as a Historian - Department of History - University of Memphis    










 
 
 
     



 
    
    
    Employment as a Historian - 
      	Department of History
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of History
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Why History?  
                          Undergraduate  
                          Graduate  
                          Online  
                          People  
                          HERC  
                          GAAAH  
                      
                      
                         
                            Graduate   
                            
                               
                                  
                                   Prospective Students  
                                   MA and PhD Programs  
                                         General Description  
                                         Focus Areas and Courses  
                                         MA Program  
                                         PhD Program  
                                     
                                  
                                   Scholarships and Awards  
                                   People  
                                         Current Students  
                                         Faculty  
                                         Alumni Placement   
                                     
                                  
                                   Student Organizations  
                                   Becoming a Historian  
                                   Forms and Applications  
                                   Contact Grad Studies Director  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of History
                              	  
                          
                              	Graduate
                              	  
                         Employment as a Historian 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Employment as a Historian 
                     
                        The possibilities   Advice for job-seekers   Searching for a position   Interviewing at conventions   
                     
                      The possibilities 
                     
                      For a thorough discussion of all the careers that are open to history majors, see  Careers for Students of History,  written by Constance Schulz, Page Putnam Miller, Aaron Marrs, and Kevin Allen, and
                        published by the American Historical Association, The National Council for Public
                        History, and the Public History Program, University of South Carolina.
                      
                     
                      The American Historical Association also has an article on  Careers for History Majors . It has recently begun a project on  Career Diversity for Historians  which seeks to “expand the occupational presence of humanists beyond the academy
                        by broadening the professional options that students commonly imagine for themselves
                        and aspire to.”
                      
                     
                      Until a few years ago historians do not receive much attention from the  Occupational Outlook Handbook  prepared by the U.S. Department of Labor’s Bureau of Labor Statistics. Historians
                        were included in the section on “Social Scientists, Other” and were discussed rather
                        briefly. Now the Bureau has a separate  listing for historians  (it still regards historians as social scientists — the words “humanists” or “humanities”
                        do not appear in its A-Z index). You might find it amusing, sobering, or frightening
                        to read how the Bureau regards historians.
                      
                     
                      The blog  “In the Service of Clio: Essays on Career Management in the Historical Profession”  by Nicholas Evan Sarantakes often has essays about careers in historical endeavors,
                        intended especially for persons in the early phases of their careers.
                      
                     
                      Alexandra Lord and Julie Taddeo, both of whom have a Ph.D. in history and left academe
                        after a few years of employment, said when they started  Beyond Academe  in 2003: “We have both come to love life ‘outside the box’ and we heartily recommend
                        it to others!” They created the site to give advice and encouragement to those who
                        either choose or are forced by necessity to find employment outside the classroom.
                      
                     
                       “What Doors Does a PhD in History Open?”  by L. Maren Wood, a PhD in history, describes and tabulates the career outcomes of
                        PhD graduates in history between 1990 and 2010 from history departments at Duke University,
                        Ohio State University, the University of North Carolina at Chapel Hill, and the University
                        of California at Santa Barbara. She has another article, “ The Ph.D.’s Guide to a Nonfaculty Job Search ,” that offers advice that emerged from her “Boot Camp for the Postacademic Job Seeker.”
                        
                      
                     
                      The Columbia Center for Career Education has an extensive listing of  Non-Academic Career Options for PhDs in the Humanities and Social Sciences .
                      
                     
                      The American Historical Association has an article on  Careers in Public History .
                      
                     
                       “No More Plan B: A Very Modest Proposal for Graduate Programs in History,”  an article by Anthony T. Grafton and James Grossman, advocates that history departments
                        actively prepare their students for non-teaching employment as part of the regular
                        curriculum. 
                      
                     
                      Advice for job-seekers 
                     
                      There are many good sites for advice on the art of seeking an academic position. Among
                        them are:
                      
                     
                      
                        
                          Preparing for the Job Market  (American Historical Association)
                         
                        
                          A Guide to the Academic Job Search  (UMASS Amherst)
                         
                        
                          Academic Job Search  (Career Services, University of California-Berkeley)
                         
                        
                          Preparing for the Academic Job Market  (Tufts University Career Center)
                         
                        
                          Guide to Public History  (Yvonne Tu)
                         
                        
                          Job Interviewing Handout  (Michael Gamer and Anne K. Krook)
                         
                        
                          Going on the Job Market, ABD  and  Going on the Job Market, ABD -- II  (Gerry Canavan) 
                         
                        
                      
                     
                      Searching for a position 
                     
                      You will find many online resources for available positions in the field of history.
                        Of them, the following are free services to job-seekers:
                      
                     
                      
                        
                          H-Net Job Guide  
                        
                          Job Search page of the Organization of American Historians  
                        
                          Chronicle Careers page of the Chronicle of Higher Education  (the  Chronicle  lists positions in all disciplines, so you will need to search on the keyword   history  )
                         
                        
                          Inside Higher Ed Job Seekers  (this site also lists positions in all disciplines; searching on the keyword   history   alone results in many extraneous “hits” — you will need to combine it with other
                           terms, such as the state in which you are seeking a position or an institution’s name)
                         
                        
                          JBHE Employment Zone of the Journal of Blacks in Higher Education  (this site also lists positions in all disciplines, but its search feature searches
                           the entire site, not just job listings, so you will need to scan the listings by eye)
                         
                        
                          HigherEdJobs.com  
                        
                          Jobs Search page of the American Association for State and Local History  
                        
                          Careers and Training page of the National Council on Public History  
                        
                      
                     
                      Another resource, the Online Job Ads section of the American Historical Association,
                        was restricted to members of the AHA before September 2011. Non-members will need
                        first to create a  free user account . Once that account is created, non-members will see only the job ads, not links to
                        other online member benefits, such as access to the  American Historical Review  and discounts on publications and the annual meeting.
                      
                     
                      Interviewing at conventions 
                     
                      Many history departments conduct interviews with applicants during the annual conventions
                        of historical associations. Some will interview “drop-ins”; others will interview
                        only those candidates who have already applied for positions and have been selected
                        for personal interviews. Among the leading conventions are those of the
                      
                     
                      
                        
                          American Historical Association , usually held in early January
                         
                        
                          Organization of American Historians , usually held in early April
                         
                        
                          Southern Historical Association , usually held in early November.
                         
                        
                      
                     
                      Departments that are searching for a candidate in a specific field will often interview
                        at the convention of the professional organization for that field. Examples of such
                        organizations include:
                      
                     
                      
                        
                          American Studies Association  
                        
                          The Association for Asian Studies  
                        
                          Latin American Studies Association  
                        
                          Middle East Studies Association  
                        
                      
                     
                      Your major advisor will be aware of conventions and meetings within his or her discipline
                        and may know of positions that are not advertised widely.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Graduate 
                         
                            
                               
                                Prospective Students  
                                MA and PhD Programs  
                                      General Description  
                                      Focus Areas and Courses  
                                      MA Program  
                                      PhD Program  
                                  
                               
                                Scholarships and Awards  
                                People  
                                      Current Students  
                                      Faculty  
                                      Alumni Placement   
                                  
                               
                                Student Organizations  
                                Becoming a Historian  
                                Forms and Applications  
                                Contact Grad Studies Director  
                            
                         
                      
                      
                      
                         
                            
                                History Happenings  
                               Significant happenings that involve our faculty, students, staff, and alumni. 
                            
                            
                                Newsletter  
                               The department newsletter is filled with interesting articles and information about
                                 our award-winning faculty and students.
                               
                            
                            
                                Event Calendar  
                               Check here often for upcoming events hosted by the Department of History 
                            
                            
                                Contact Us  
                               Contact the Department of History at The University of Memphis. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


