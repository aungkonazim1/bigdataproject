Overriding competency structure results | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Overriding competency structure results | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Competencies    »    Creating and managing competency structure elements   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Overriding competency structure results                       
        
        
       
          
     
           Printer-friendly version       
	You can override a user’s result for any competency, learning objective, or activity, and manually set it to Complete or Incomplete. An override always takes precedence over a user’s actual result; if you override an activity, the system ignores the user’s actual assessment; if you override a learning objective or a competency, it ignores the number of child associations the user has achieved.
 

 
	 Note  Override results are displayed with a red asterisk ( ).
 

 
	Override a user’s result
 

  
		On the Competency Home page, click on a competency or learning objective with results you want to override.
		 
			 Note  If you want to change results for an activity, click    Structure  from the Edit Competency or Edit Learning Objective page, then click on the name of the activity you want to view.
		 
	 
	 
		Click    Results .
	 
	 
		On the Results page, select the check box beside the users with results you want to override.
	 
	 
		Click the    Manually override selected users' achievement to Complete  icon or    Manually override selected users' achievement to Incomplete  icon at the top of the list.
	 
  
	Clear an override
 

  
		On the Results page, select the check box beside the users with results you want to clear an override for.
	 
	 
		Click the    Clear Override  icon at the top of the list.
	 
      Audience:    Instructor      

    
           

                   ‹ Viewing competency structure results 
        
                   up 
        
                   Managing independent learning objectives and independent activities › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Competencies  
  
      Competency structure basics    Automating competency structure evaluation    Creating and managing competency structure elements    Understanding competency status settings    Creating competencies    Editing a Draft or In Review competency s details    Hiding or showing an Approved competency    Modifying an Approved competency    Copying a competency    Archiving a competency    Deleting competencies    Creating learning objectives    Editing a learning objective s details    Copying a learning objective    Deleting learning objectives    Creating activities    Adding associations between competency structure elements    Removing associations between competency structure elements    Sharing competency structures    Tracking competency versions    Viewing competency structure results    Overriding competency structure results    Managing independent learning objectives and independent activities      Evaluating competency structure activities    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
