Graduate Programs - COE - University of Memphis  COE Office of Graduate Programs  










 
 
 
     



 
    
    
    Graduate Programs - 
      	COE
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			College of Education
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Departments  
                          Degrees  
                          Scholarships  
                          Research  
                          Alumni  
                          Faculty  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	COE
                              	  
                         Graduate Programs 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      COE Office of Graduate Programs 
                     
                           Stormey Warren       Ball Hall 215      (901) 678-2363 - Phone      (901) 678-4778 - Fax       shutsell@memphis.edu  
                     
                      The Office of Graduate Programs in the College of Education (COE) is responsible for
                        the effectiveness and integrity of the graduate programs offered by the college's
                        three departments: Counseling, Educational Psychology and Research, Instruction and
                        Curriculum Leadership, and Leadership.
                      
                     
                      Students, faculty, and staff look to the COE Office of Graduate Programs to interpret
                        and enforce policies as they pertain to the graduate programs and for notification
                        of impending deadlines and policy changes. It also functions as an initial source
                        for inquiries regarding the programs, degrees, and roles served by the departments
                        within the College.
                      
                     
                      Although the University Council, the COE Graduate Studies Council, and the Dean of
                        the Graduate School set policy, the COE Office of Graduate Programs serves as liaison
                        for the faculty, staff, students, and prospective students in communicating their
                        requirements and judgments pertaining to issues affecting them to those entities. 
                      
                     
                      Important Links 
                     
                      
                        
                         
                           
                             COE Graduate Catalog  
                           
                         
                        
                         
                           
                             Graduate Handbook Doctoral Degree Programs  
                           
                         
                        
                         
                           
                             Graduate School Forms Page  
                           
                         
                        
                         
                           
                             Doctoral Program of Studies  
                           
                         
                        
                         
                           
                             Change of Program of Studies  
                           
                         
                        
                         
                           
                             Doctoral Final Defense Announcement  
                           
                         
                        
                         
                           
                             Education Specialist Form  
                           
                         
                        
                         
                           
                              Procedures for Late Registration   
                           
                         
                        
                      
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Apply to our Undergraduate Programs  
                               Please visit the Undergraduate Admissions website for more information!  
                            
                            
                                Apply to our Masters and Doctoral Programs  
                               Earn your Masters Degree from any of our three departments or take your education
                                 to the next level with a doctorate degree.
                               
                            
                            
                                COE Online  
                               Earn a degree on YOUR time. 
                            
                            
                                Contact Us  
                                Have questions?  Call or email today! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/10/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


