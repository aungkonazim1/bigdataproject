February 2017 &#8211; The University of Memphis President&#039;s Blog   


 

 
 
 
 


 
 
 
 

 



 February 2017   The University of Memphis President s Blog 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 



 
 
  
 

			

         
 		
		



 
 
 
 

 

 


  Skip to content  




 

	
	 

		
		 

			
			 

				
						  The University of Memphis President s Blog  

					
			  

		  

		
		 

			 Primary Menu 
			 Menu 

			
			 
				 
					 
				 
					 Search for: 
					 
				 
				 
			 				 
			 

			 

			  
  Home    About the President  
  

		  

		 

		
		
			
				 

			
		
		 

		
	  

	
	 
	 

		
		 

			
		
			 
				 Month: February 2017 			  

			
	
	 

		
		
		 

			
			 

				 Thank You 

				
			 

			
			 

				   Author   Jeanine Hornish Rakow     Published on    February 19, 2017  March 27, 2017     Leave a comment  
			  

			
		  

		
		 

			  Dear UofM Students: 
 
 Thank you for the pride you take in our campus and your considerate, civil discourse about the delay in the new recreation facility. Universities are places for debate and discussion, places for the full range of positions and opinions, places to clarify and confirm your values. You have consistently engaged in a process that is characterized by thoughtful and thorough evaluation, regardless of the position you support. 
 This is not the first challenge we have experienced on our campus, in our community or across our nation. Our Critical Conversations series included issues of law enforcement and race relations, social justice and freedom of speech. In each and every case over the past two and a half years, I have been both impressed and grateful for your active engagement, your passionate investment in our community, your support for this University and your commitment to civility. Thank you. 
 I am proud to be a part of the University of Memphis and the City of Memphis. With each and every challenge we face, I feel more confident and assured about the future of our community and our country. 
 Warm Regards, 
 M. David Rudd 
President | Distinguished University Professor 
 
 

		  

		
		 

			  Published on    February 19, 2017  March 27, 2017      Author   Jeanine Hornish Rakow     Leave a comment  
			
		  

		
	  

	
	
	 

		
		
		 

			
			 

				 Update on Campus Building Projects, Tuition and Fees 

				
			 

			
			 

				   Author   Jeanine Hornish Rakow     Published on    February 12, 2017  March 27, 2017     Leave a comment  
			  

			
		  

		
		 

			 Dear Campus Community: 
 Last week contracts were released for the land bridge and parking garage projects. Both projects will be moving forward as quickly as is possible. Plans for the new student recreation center have been returned for redesign, with a request to lower costs to fit within the financial model constraints, along with accommodating retention of the current facility. Costs for the project ballooned to well over $60M, exceeding the boundaries of the original financial model. To date, the student fee supporting the land bridge and recreation center has generated well below projections, an estimated $15M. 
 Several variables have converged requiring us to sequence these projects in a careful and thoughtful manner. Most importantly, it is our commitment to financial responsibility and containing student costs/debt. Three years ago the UofM had no tuition increase, a first in 34 years. The last two years we have had tuition increases under 3% each year. In contrast, over the previous 15 years the average tuition increase was 8% annually. 
 The current recreation facility has debt service that will not be retired until the year 2030. Accordingly, we will not be demolishing the building. We will immediately begin limited improvements that will see the facility through the next decade. Redesign of the new recreation facility will delay that project by approximately 18 months. This delay not only allows us to establish much needed parking in advance of the facility, but also to implement a responsible financial model that will NOT demand additional tuition and fee increases to fund the project. Just a few weeks ago I rejected a request for a new student fee to fund the operational costs of a new facility. Since the Governor has recommended funding for our new music center this year, resolving the parking challenges are paramount. As you are likely aware, the recreation center will result in the loss of up to 1,000 parking spaces, with the new music center absorbing another 800. It is critical we have new parking in place prior to these facilities breaking ground. 
 I have received questions about the new basketball facility, which is on pace to be completed this fall. The facility is funded entirely through private donations. 
 If you have questions, please don't hesitate to let me know. There is more construction on campus and in our neighborhood district than at any time in our history. We have made a firm commitment to the growth of our University. Accordingly, it is critical that we be thoughtful and careful in our planning. In particular, it is imperative we live within our financial constraints. Our commitment to containing tuition and fee costs cannot be understated. 
 I will be speaking with the SGA on Thursday to address any questions or concerns. Again, we are moving forward with all projects. We are simply modifying the sequencing, along with implementing an 18-month delay on the student recreation center. 
 Warm Regards, 
 
 
 
 M. David Rudd 
President | Distinguished University Professor 
 
 
 

		  

		
		 

			  Published on    February 12, 2017  March 27, 2017      Author   Jeanine Hornish Rakow     Leave a comment  
			
		  

		
	  

	
	
	 

		
		
		 

			
			 

				 Helen Hardin Honors College 

				
			 

			
			 

				   Author   Jeanine Hornish Rakow     Published on    February 10, 2017  March 27, 2017     Leave a comment  
			  

			
		  

		
		 

			 Dear Campus Community: 
 The Helen Hardin Honors Program at the University of Memphis has received approval for designation as an Honors College. With the elevation to college status, the Helen Hardin Honors College will become the largest honors college in Tennessee. Nowhere else in the state will top academic performers find a richer, more diverse experience than the Helen Hardin Honors Program at the UofM. This designation will further distinguish us as a University where rich educational opportunities flourish. 
 While the honors program has been in existence for more than four decades, in 2008 it was renamed the Helen Hardin Honors Program in recognition of a significant gift from Helen Hardin, a local philanthropist and co-founder of Hardin-Sysco. Under the leadership of Dr. Melinda Jones, the program has been recognized for its enriched curriculum, smaller classes and opportunities beyond the classroom such as Study Abroad, independent research and internships. 
 The Honors Program has a strong reputation for attracting the brightest students, which includes more than 2,000 undergraduates from all departments and majors at the UofM and approximately 500 freshmen who enter the program each year. The quality of the student body is evidenced by the mean high school grade point average and ACT of the 2016 entering class (3.90 GPA/28 ACT). Once enrolled, honors students are committed to a curriculum that includes at least 25 hours of honors courses out of the 120 hours typically required for graduation. 
 The Helen Hardin Honors Program has a designated building, Honors Hall, which houses the administrative offices, reading and study lounges, a classroom and a computer lab for students. The program also has a designated residential facility, the Living Learning Complex, which serves more than 250 students. Additionally, the Honors Program serves as the administrative office that coordinates undergraduate research on campus, and faculty and students affiliated with the program publish the UofM's undergraduate research journal, QuaesitUM (Latin for "to seek, to inquire"). 
 Due to the success and national recognition of the Helen Hardin Honors Program, the University of Memphis was selected to host the National Conference for Undergraduate Research, which is expected to attract more than 4,200 undergraduate students and their faculty mentors to Memphis April 6-8. 
 Certainly, the Helen Hardin Honors College, the largest in the State of Tennessee, is evidence of the growth and influence of the UofM. 
 Warm Regards, 
 M. David Rudd 
President | Distinguished University Professor 

		  

		
		 

			  Published on    February 10, 2017  March 27, 2017      Author   Jeanine Hornish Rakow     Leave a comment  
			
		  

		
	  

	
	
	 

		
		
		 

			
			 

				 Governor s Proposed Budget 

				
			 

			
			 

				   Author   Jeanine Hornish Rakow     Published on    February 6, 2017  March 27, 2017     Leave a comment  
			  

			
		  

		
		 

			 Dear Faculty and Staff: 
 Last week was the Governor's State of the State Address. As is custom, the Governor unveiled his proposed budget for the upcoming fiscal year. Attached is a summary of the Governor's  budget recommendations  distributed by the TN Higher Education Commission. In short, the Governor has proposed a budget that is very promising not only for higher education in general, but the University of Memphis as well. Not only will our recurring state allocation increase, the Governor has also recommended $29 million in state funds for our new music center and $14 million for capital maintenance. The importance of the maintenance funds cannot be overstated. 
 Our goal for the coming fiscal year budget will be to generate a three percent salary pool. As you know, the UofM has a number of additional requests that we will be pursuing in the coming months. A word of caution, the Governor's budget recommendations are only that, recommendations. The state budget will not be finalized until spring. I will keep you updated as the legislature moves forward. 
 I hope the semester is going smoothly. Again, thank you for your dedication to our students, University and community. 
 Warm Regards, 
 M. David Rudd 
President | Distinguished University Professor 

		  

		
		 

			  Published on    February 6, 2017  March 27, 2017      Author   Jeanine Hornish Rakow     Leave a comment  
			
		  

		
	  

	
			
		
			
		  

		
	  


	
		
		
		 

		 Main Sidebar 

			
			  
				 
					 Search for: 
					 
				 
				 
			  		 		 Recent Posts 		 
					 
				 UofM Quality Indicator Score 
						 
					 
				 University of Memphis Research Foundation Announces Grand Opening of Student-Operated FedEx Call Center 
						 
					 
				 Pause to Grieve 
						 
					 
				 University of Memphis Magazine: Fall 2017 
						 
					 
				 Campus Update 
						 
				 
		 		  Recent Comments      Archives 		 
			  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  July 2014  
	  June 2014  
	  May 2014  
	  April 2014  
		 
		   Categories 		 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 
			
		  

		
		  

	
	
	 

		
		 Footer Content 

		 
		
			
				
				
								
			
		  
	
		 

			
			
			Using  Tiny Framework     
			
			   Log in  

		  
		
		 

			
			

 

	 Social Links Menu 

	      i class= fa fa-twitter   /i    
  
  

			
		  

		
	  

	
  


        

        








		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 
 