Parent Events - Parent &amp; Family Services - University of Memphis    










 
 
 
     



 
    
    
    Parent Events - 
      	Parent   Family Services
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Parent   Family Services
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Events   Services  
                          Get Involved  
                          News  
                          Resources  
                          Support Us  
                          Visit UofM  
                      
                      
                         
                            Events   Services   
                            
                               
                                  
                                   Parent   Family Weekend  
                                   Bouncing with the Tigers Parent   Family Spring Day  
                                   Finals Care Package  
                                   Hugs from Home  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Parent   Family Services
                              	  
                         
                           	Events   Services
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Events   Services 
                     
                        
                     
                      Parent   Family Services works to engage and connect parents and family members in
                        the University of Memphis community throughout the year. Join us for our next event
                        or take advantage of one of our services to support your student!
                      
                     
                      Fall 
                     
                      Parent   Family Weekend, September 15-17, 2017, 2018 TBA 
                     
                      Whether you are a local or from outside the Mid-South,  Parent and Family Weekend  is a wonderful time to see your student, visit campus and get to experience life
                        as a Tiger! You will have the opportunity to meet other Tiger families, faculty and
                        University administrators. You won't want to miss this fun-filled weekend!
                      
                     
                      Football Game Days 
                     
                      Experience a football game with your student each fall! Students may purchase up to
                        two guest tickets with seats in the Student Section for each game (limit one vs. Ole
                        Miss) at the Athletic Ticket Office for $10 ($25 for the Ole Miss football game),
                        while supplies last. Need more tickets?  Click here to purchase tickets  for the whole family. 
                      
                     
                      Spring 
                     
                      Bouncing with the Tigers, Parent   Family Spring Day, Sunday, February 11, 2018 
                     
                      Parent   Family Services invites you to join us for a day of Tiger spirit and fun
                        with your student at  Bouncing with the Tigers ! 
                      
                     
                      Support Your Student 
                     
                       Finals Care Packages  
                     
                      Surprise and support your student during finals with a care package from Parent  
                        Family Services!
                      
                     
                       Hugs from Home  
                     
                      Send your student some encouragement throughout the school year with a "Hugs from
                        Home" bottle!
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Events   Services 
                         
                            
                               
                                Parent   Family Weekend  
                                Bouncing with the Tigers Parent   Family Spring Day  
                                Finals Care Package  
                                Hugs from Home  
                            
                         
                      
                      
                      
                         
                            
                                Parent   Family Association  
                               Stay connected and engaged with us through the Parent   Family Association 
                            
                            
                                Parent   Family Handbook  
                               Learn about opportunities and services available to students 
                            
                            
                                In the Now  
                               What you need to know this month to help you help your student 
                            
                            
                                Contact Us  
                               Got questions, comments, or concerns?  We're here to help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/1/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


