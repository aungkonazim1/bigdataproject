Procurement and Contract Services Purchasing Card Program - Procurement and Contract Services - University of Memphis    










 
 
 
     



 
    
    
    Procurement and Contract Services Purchasing Card Program - 
      	Procurement and Contract Services
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Procurement and Contract Services
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Staff Directory  
                          Contract Services  
                          Tigerbuy and Bids  
                          Purchasing Card  
                          Travel Services  
                      
                      
                         
                            Purchasing Card Policies   Forms   
                            
                               
                                  
                                   University's Procurement and Contract Services Policy  
                                   University's Purchasing Card Guideline  
                                   Purchasing Cardholder Enrollment Form  
                                   Agreement to Accept Visa Purchasing Card Form  
                                   Purchasing Cardholder Change Form  
                                   Regions Purchasing Card Customer Dispute Form  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Procurement and Contract Services
                              	  
                         
                           	Purchasing Card
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      
                        
                          INTERSECT Cardholder User Guide  [PDF]
                         
                        
                          INTERSECT Department Admin User Guide  [PDF]
                         
                        
                          INTERSECT Login User Guide  [PDF]
                         
                        
                          INTERSECT Reviewer User Guide  [PDF]
                         
                        
                          P-Card Responsibilities and Upcoming Process Changes  [PDF]
                         
                        
                      
                     
                      Purchasing Card Program 
                     
                      The University of Memphis Purchasing Card Program has been established to provide
                        University employees with a convenient means to make legitimate business purchases
                        and, at the same time, reduce the costs associated with initiating and paying for
                        those purchases. Use of the Purchasing Card for small dollar purchases less than $5,000
                        consolidates paperwork and helps facilitate vendor payment. The VISA cards are issued
                        by Regions Bank.
                      
                     
                      
                        
                         Procurement and Contract Services is responsible for managing the Purchasing Card
                           Program. For assistance, contact the Procurement Specialist at 901.678.3673.
                         
                        
                         Cardholders are responsible for utilizing sound business decisions in making small-dollar
                           purchases for University use, checking all transactions against supporting documentation
                           to verify accuracy and propriety, and safeguarding the purchasing card and the associated
                           account number in a secure location at all times.
                         
                        
                         Card administrators are responsible for verifying that all charges against the cardholder's
                           account are substantiated by supporting documentation, reallocation of charges are
                           made to department FOAP/account codes, and the documentation is retained within the
                           department for audit purposes.
                         
                        
                         Card reviewers are responsible for the final independent review and approval of purchasing
                           card statements and receipts; reviewing, signing, and dating the cardholder's monthly
                           statements; and forwarding the statement and supporting documentation to the card
                           administrator to retain for audit purposes.
                         
                        
                         All purchases made with the purchasing card must be for official University business
                           in accordance with University policies and procedures and must NEVER be used for personal
                           purchases.
                         
                        
                         The purchasing card cannot be used if the purchase requires a signed contract or agreement
                           (See University's Contracts and Signatory Authority Policy).
                         
                        
                         The University prohibits the use of the purchasing card with out-of-country vendors. 
                        
                         Regions Bank transmits purchasing card statements to cardholders via a secure Internet
                           site.
                         
                        
                         Charges are accumulated monthly and charged to "default" FOAP (Fund, Org, Account,
                           Program) account codes and data is transmitted to the Banner Financial System.
                         
                        
                         If the purchasing card is lost, stolen, or damaged, cardholder must notify Regions'
                           Bank immediately at 1.888.934.1087. Then, notify the Procurement Specialist as soon
                           as possible.
                         
                        
                         All documentation pertaining to purchasing card transactions shall be available for
                           review and audit by Procurement and Contract Services, Internal Audit, State/Federal
                           Audit and Legal Counsel.
                         
                        
                         A cardholder who makes an unauthorized purchase or uses the purchasing card in an
                           inappropriate manner will be subject to disciplinary action including possible termination
                           of employment at the University and criminal prosecution.
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Purchasing Card Policies   Forms 
                         
                            
                               
                                University's Procurement and Contract Services Policy  
                                University's Purchasing Card Guideline  
                                Purchasing Cardholder Enrollment Form  
                                Agreement to Accept Visa Purchasing Card Form  
                                Purchasing Cardholder Change Form  
                                Regions Purchasing Card Customer Dispute Form  
                            
                         
                      
                      
                      
                         
                            
                                Doing Business with the University of Memphis  
                               Information on how to do business with the U of M and our e-procurement and bidding
                                 system, Tigerbuy
                               
                            
                            
                                University of Memphis Contracts Report  
                               Contracts listing for the UofM 
                            
                            
                                Forms  
                               All of B F's forms in one place 
                            
                            
                                Business   Finance  
                               The Division of Business   Finance at the UofM 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


