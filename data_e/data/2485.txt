 Electrical &amp; Computer Engineering - Herff College of Engineering - University of Memphis    










 
 
 
     



 
    
    
     Electrical   Computer Engineering - 
      	Herff College of Engineering
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Herff College of Engineering
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Future Students  
                          Students  
                          Programs  
                          Research  
                          Alumni  
                          Events  
                      
                      
                         
                            Engineering Departments   
                            
                               
                                  
                                   Biomedical Engineering  
                                   Civil Engineering  
                                   Electrical and Computer Engineering  
                                   Engineering Technology  
                                   Mechanical Engineering  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Herff College of Engineering
                              	  
                          
                              	Engineering Departments
                              	  
                          Electrical   Computer Engineering 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                       Electrical Engineering   Computer Engineering  
                     
                      provide the technology that created the information age in which we live. From the
                        telephone, television and radio to smartphones, gaming consoles and the Internet,
                        electrical engineering and computer engineering continue to drive the technical advances
                        that radically alter our lives. For the future, electrical and computer engineers
                        will develop more sustainable sources of energy, like solar and wind power, and more
                        efficient devices, like appliances and computers that will help maintain our world
                        and extend our natural resources. They contribute to medical technology that helps
                        keep us healthy or cure us when we are ill. Do you listen to music on your smartphone
                        or tablet? An electrical and computer engineer designed the hardware and software
                        that allow you to enjoy your tunes that way. Our programs provide graduates with a
                        thorough understanding of the theoretical and philosophical principles of computer
                        hardware and software, electronics, electric power and signal processing and communications,
                        so they are prepared to apply their knowledge through design to a variety of applications.
                      
                     
                      MORE ABOUT COMPUTER ENGINEERING 
                     
                      Computer engineers design smartphones, computers, gaming consoles and the software
                        that runs on them. They also put their mark on computerbased systems like those found
                        in cars, planes, appliances, electronics, phones, communication networks and all kinds
                        of other products.
                      
                     
                      CAREER OUTLOOK 
                     
                      Our computer engineering graduates can be found in high-tech companies across the
                        globe, like Intel, Compaq, Microsoft, Sun and Texas Instruments, and also in industries
                        and agencies that build or use computer-based systems, like FedEx, NASA, and other
                        telecommunications, automotive and aerospace companies.
                      
                     
                      MORE ABOUT ELECTRICAL ENGINEERING 
                     
                      Electrical engineers design the electronics that run your smartphone, the networking
                        hardware that powers the Internet, the power systems that light, heat and cool your
                        home, and the algorithms that detect cancer in medical images.
                      
                     
                      CAREER OUTLOOK 
                     
                      Graduates launch their careers in all kinds of fields, including aerospace, automotive,
                        chemical, construction, defense, electronics, consumer goods, marine, materials and
                        metals, power and energy, and pharmaceuticals and telecom industries.
                      
                     
                      BY THE NUMBERS 
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  194 
                                 
                               
                              
                               
                                 
                                  Herff is home to 194 students pursuing degrees in electrical and computer engineering. 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  2.7M 
                                 
                               
                              
                               
                                 
                                  Received $2.7 million toward research in 2012–2013. 
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  05 
                                 
                               
                              
                               
                                 
                                  There are five dynamic research focus areas: Biomedical Imaging and Signal Processing, Nanotechnology, Electric Power, Biomedical Electronics and Advanced Sensors.
                                  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                   01 
                                 
                               
                              
                               
                                 
                                  The program is affiliated with one state-of-the art research center—the Center for
                                    Advanced Sensors, which is uniquely positioned as a leader in researching and developing future
                                    military imaging systems. An online graduate certificate is also offered in Imaging and Signal
                                    Processing.
                                  
                                 
                               
                              
                            
                           
                            
                              
                               
                                 
                                  60 
                                 
                               
                              
                               
                                 
                                  Herff's Electrical and Computer program supports 60 amazing graduate students. 
                                 
                               
                              
                            
                           
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Engineering Departments 
                         
                            
                               
                                Biomedical Engineering  
                                Civil Engineering  
                                Electrical and Computer Engineering  
                                Engineering Technology  
                                Mechanical Engineering  
                            
                         
                      
                      
                      
                         
                            
                                Visit Herff  
                               See what Herff has to offer YOU! 
                            
                            
                                Apply to UofM  
                               Submit your application and enroll in the College of Engineering 
                            
                            
                                2017 E-day Results  
                               Thanks to 3,000 student participants!  
                            
                            
                                Contact Us  
                               Have Questions? We can help!  
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/29/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


