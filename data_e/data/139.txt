Spring 2018 Dates and Deadlines - Registrar - University of Memphis    










 
 
 
     



 
    
    
    Spring 2018 Dates and Deadlines - 
      	Registrar
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Registrar
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Register  
                          Students  
                          Veterans  
                          Faculty   Staff  
                          Forms  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Registrar
                              	  
                          
                              	Calendars
                              	  
                         Spring 2018 Dates and Deadlines 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Spring 2018 Dates and Deadlines 
                     
                      Refer to this calendar for your first day to register as well as deadlines for enrollment
                        major activities--last day to add or drop, last day to withdraw, semester beginning
                        and end dates, etc. Where appropriate, dates are given for each   part of term  --FULL, 1ST, 2ND, and TN eCampus (formerly RODP).
                      
                     
                       Note :  For Fee Payment and Refund deadlines, refer to the   Bursar's Calendars  .  Degree-seeking students must be advised before registering, Undergraduate students
                        should visit   Academic Advising   for advising information.  When registration opens for your classification, it opens at  8:00 am  and remains open throughout the regular registration period.
                      
                     
                      Check Your Registration Status Before Registering 
                     
                      1st Day to Register: 
                     
                      
                        
                         Degree Seeking Graduate  -   November 13, 2017   + Honors Students  + Veterans   
                        
                         Seniors  -   November 14, 2017     
                        
                         Juniors  -   November 15, 2017    
                        
                         Sophomores  -   November 16, 2017     
                        
                         Freshmen  -   November 20, 2017    
                        
                         Non-Degree Seeking  -   November 22, 2017    
                        
                         PC191/TN Employees   Senior Citizens  -   December 19, 2017  
                        
                      
                     
                      Last Day of Regular Registration: 
                     
                      
                        
                         FULL  -   January 12, 2018  
                        
                         1ST  -   January 12, 2018  
                        
                         2ND  -   March 9, 2018  
                        
                         TN eCampus (RODP)  -   January 12, 2018  
                        
                      
                     
                      Late Registration - $100 Late Registration Fee: 
                     
                      
                        
                         FULL  -   January 13-19, 2018   
                        
                         1ST  -   January 13-17, 2018   
                        
                         2ND  -   March 10-13, 2018   
                        
                         TN eCampus (RODP)  -   January 13-19, 2018   
                        
                      
                     
                      Last Day to Add or Change Sections: 
                     
                      
                        
                         FULL  -    January 19, 2018   
                        
                         1ST  -    January 17, 2018   
                        
                         2ND  -     March 13, 2018   
                        
                         TN eCampus (RODP)  -    January 19, 2018   
                        
                      
                     
                      SUMMER 2018 Regular Registration Begins:  April 2, 2018, 8:00am  
                     
                      FALL 2018 Regular Registration Begins:  April 2, 2018, 8:00am  
                     
                      Fee Payment / Financial Aid 
                     
                      For Enrollment Fee Payment and Financial Aid Disbursement schedules, visit the   Bursar's website  .
                      
                     
                      Drop / Withdraw / Refund Deadlines 
                     
                      Refunds: Visit the   Bursar's website   for 100%, 75%, and 25% Refund Deadlines.
                      
                     
                      Drop Period - No grade(s) assigned; course(s) will not appear on your transcript: 
                     
                      
                        
                         FULL  -   January 16-29, 2018  
                        
                         1ST  -   January 16-29, 2018  
                        
                         2ND  -   March 12-25, 2018  
                        
                         TN eCampus (RODP)  -   January 16-29, 2018  
                        
                      
                     
                      Withdrawal Period - "W" grade(s) assigned; course(s) will appear on your transcript: 
                     
                      
                        
                         FULL  -   January 30 - March 16, 2018  
                        
                         1ST  -   January 30 - February 8, 2018  
                        
                         2ND  -   March 26 - April 3, 2018  
                        
                         TN eCampus (RODP)  -   January 30 - March 16, 2018  
                        
                      
                     
                      Class Meetings, Breaks,   Final Exams 
                     
                      M. L. King Holiday (All University Offices CLOSED):  January 15, 2018  
                     
                      Term/Part of Term Lengths: 
                     
                      
                        
                         FULL  -   January 16 - May 3, 2018  
                        
                         1ST  -   January 16 - March 2, 2018  
                        
                         2ND  -   March 12 - April 25, 2018  
                        
                         TN eCampus (RODP)  -   January 16 - May 3, 2018  
                        
                      
                     
                      Classes Begin: 
                     
                      
                        
                         FULL  -   January 16, 2018  
                        
                         1ST  -   January 16, 2018  
                        
                         2ND  -   March 12, 2018  
                        
                         TN eCampus (RODP)  -   January 16, 2018  
                        
                      
                     
                      Spring Break (Monday - Sunday): 
                     
                      
                        
                         FULL  -   March 5-11, 2018  
                        
                         1ST  -   n/a  
                        
                         2ND  -   n/a  
                        
                         TN eCampus (RODP)  -   March 5-11, 2018  
                        
                      
                     
                      Daylight Saving Time Begins (Spring Forward):  March 11, 2018  
                     
                      Classes End: 
                     
                      
                        
                         FULL  -   April 25, 2018  
                        
                         1ST  -   March 2, 2018  
                        
                         2ND  -   April 25, 2018  
                        
                         TN eCampus (RODP)  -   April 25, 2018  
                        
                      
                     
                      Study Day (Full Term - Classes do not meet) -  April 26, 2018  
                     
                      Final Exams: 
                     
                      
                        
                         FULL  -     View Schedule .  
                        
                         1ST  -   March 2, 2018  OR  last class meeting  
                        
                         2ND  -   April 25, 2018  OR  last class meeting  
                        
                         TN eCampus (RODP)  -   Refer to your course syllabus.  
                        
                      
                     
                      Graduation / Commencement 
                     
                        Undergraduate/Graduate Degree-Filing Deadlines   
                     
                        Commencement Ceremony   
                     
                      Grades   Attendance 
                     
                      Faculty Report Initial Class Attendance:  First Day Class Meets  
                     
                      Remove FALL 2017 UNDERGRADUATE "I" Grades:  January 31, 2018  
                     
                      Remove FALL 2017 GRADUATE "I" Grades:  March 19, 2018  
                     
                      Faculty Deadline for Web Grade Entry: 
                     
                      
                        
                         FULL  -   May 7, 2018  
                        
                         1ST  -   March 5, 2018  
                        
                         2ND  -   May 7, 2018  
                        
                         TN eCampus (RODP)  -   n/a  
                        
                      
                     
                      Final, Official Grades Available (Thursday following the term's last exam): 
                     
                      
                        
                         FULL  -   May 10, 2018  
                        
                         1ST  -   March 8, 2018  
                        
                         2ND  -   May 10, 2018  
                        
                         TN eCampus (RODP)  -   May 10, 2018  
                        
                      
                     
                      Dorms 
                     
                      Residence Hall Check-in:  January 11-12, 2018  
                     
                      Residence Hall Check-out:  May 4, 2018  
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Calendars  
                               View Academic Year calendars; semester dates/deadlines; final exam schedules. 
                            
                            
                                Transcripts  
                               Request copies of your official transcript. 
                            
                            
                                Notice!  
                                Check for alerts and reminders. 
                            
                            
                                Contact Us   
                               Need additional assistance or information? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


