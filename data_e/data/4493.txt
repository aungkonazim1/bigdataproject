Certificate in African American Literature - Department of English - University of Memphis    










 
 
 
     



 
    
    
    Certificate in African American Literature  - 
      	Department of English
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of English
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Undergraduate  
                          Graduate  
                          People  
                          Community  
                          News/Events  
                      
                      
                         
                            Graduate Students   
                            
                               
                                  
                                   Application Process  
                                   MA  
                                         Composition Studies  
                                         ESL  
                                         Linguistics  
                                         Literature  
                                         Professional Writing  
                                     
                                  
                                   MFA  
                                         Creative Writing  
                                     
                                  
                                   PhD  
                                         Applied Linguistics  
                                         Composition Studies  
                                         Professional Writing  
                                         Literary and Cultural Studies  
                                     
                                  
                                   Certificate  
                                        
                                         African American Literature  
                                        
                                         Teaching English as a Second/Foreign Language  
                                     
                                  
                                   Online Degree Programs  
                                   Advising  
                                   Financial Aid and Scholarships  
                                   Graduate Courses  
                                         Two-Year Template 2016-2018  
                                         Two-Year Template 2018-2020  
                                         Graduate Course Descriptions  
                                     
                                  
                                   Organizations  
                                         UM English Graduate Organization  
                                          The Pinch   
                                         Society for Technical Communication  
                                     
                                  
                                   Forms  
                                         UofM Graduate Online Forms  
                                         English Teaching/Graduate Assistantship Application  
                                         Graduate Advising Worksheet  
                                         Graduate Independent Study Form  
                                         Thesis Hour Registration and Prospectus Form  
                                         Dissertation Hour Registration Form  
                                     
                                  
                                   Graduate Catalog  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of English
                              	  
                          
                              	Graduate Students
                              	  
                         Certificate in African American Literature  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Certificate in African American Literature 
                     
                      The graduate certificate program provides training to students interested in teaching
                        African American literature. The 15 credit-hour program provides students with the
                        background necessary to develop African American literature programs for elementary
                        or secondary classrooms. The certificate is an ideal way for current and future teachers
                        to highlight their expertise in African American literature for career advancement
                        and employment.
                      
                     
                      Students must choose 12 credit hours from graduate African American literature courses. 
                     
                      
                        
                         ENGL 7325 African American Literature, 1930-1960 
                        
                         ENGL 7326 African American Literature of Memphis and the Mid-South 
                        
                         ENGL 7327 Studies in Form and Genre: African American Literature 
                        
                         ENGL 7328 Studies in Major Authors: African American Literature 
                        
                         ENGL 7329 African American Literature, Beginnings to 1850 
                        
                         ENGL 7330 African American Literature, 1850-1900 
                        
                         ENGL 7331 Frederick Douglass 
                        
                         ENGL 7332 Literature of the African Diaspora 
                        
                         ENGL 7333 Amiri Baraka 
                        
                         ENGL 7334 The Black Arts Movement 
                        
                         ENGL 7335 African American Literature, 1989-Present 
                        
                         ENGL 7336 African American Literary Theory 
                        
                         ENGL 7465 African American Literature 1960 to 1988 
                        
                         ENGL 7468 Literature of the Harlem Renaissance 
                        
                         ENGL 7469 African American Women Writers 
                        
                      
                     
                      Three (3) elective hours may be selected from an additional African American literature
                        course or one of the following courses, provided it has an African American Literature
                        component:
                      
                     
                      
                        
                         ENGL 7323 American Literature to 1865 
                        
                         ENGL 7324 American Literature, 1865-1914 
                        
                         ENGL 7391 Modern American Novel 
                        
                         ENGL 7392 American Poetry 
                        
                         ENGL 7393 American Drama 
                        
                         ENGL 7464 Contemporary American Literature 
                        
                      
                     
                      Students are encouraged to work closely with their advisor and the Coordinator of
                        the African American Literature Concentration to meet these goals.
                      
                     
                       Admissions Requirements  
                     
                      
                        
                         Applicants should send a letter of intent and two letters of recommendation to the
                           Department of English Graduate Office. Applicants need to apply to both the University
                           of Memphis Graduate School and the Department of English Graduate Office.
                         
                        
                         An overall minimum grade point average of 2.75 in English or a related area is recommended
                           at the undergraduate level.
                         
                        
                         Since up to 12 credit hours from the certificate program may count toward the MA,
                           MFA, or PhD degrees, it is expected that many already-admitted students will earn
                           the certificate on their way to their degrees. Such students wishing to earn the Certificate
                           must apply to the Graduate School using the  Change of Status  form.   
                        
                      
                     
                      For more information about the African American Literature certificate program, contact
                         Dr. Terrence Tucker , Patterson 414, or at 678.3029. For more information about the English graduate programs,
                        contact  Ms. Sarah Ellis  in the English Graduate Office, Patterson 461, 901.678.1448.
                      
                     
                       Learn More:   Video Introduction to the African American Literature Concentration   For more information about the Certificate program in African American Literature
                           requirements, visit the   Graduate Catalog   (Sec VII, part B).  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Graduate Students 
                         
                            
                               
                                Application Process  
                                MA  
                                      Composition Studies  
                                      ESL  
                                      Linguistics  
                                      Literature  
                                      Professional Writing  
                                  
                               
                                MFA  
                                      Creative Writing  
                                  
                               
                                PhD  
                                      Applied Linguistics  
                                      Composition Studies  
                                      Professional Writing  
                                      Literary and Cultural Studies  
                                  
                               
                                Certificate  
                                     
                                      African American Literature  
                                     
                                      Teaching English as a Second/Foreign Language  
                                  
                               
                                Online Degree Programs  
                                Advising  
                                Financial Aid and Scholarships  
                                Graduate Courses  
                                      Two-Year Template 2016-2018  
                                      Two-Year Template 2018-2020  
                                      Graduate Course Descriptions  
                                  
                               
                                Organizations  
                                      UM English Graduate Organization  
                                       The Pinch   
                                      Society for Technical Communication  
                                  
                               
                                Forms  
                                      UofM Graduate Online Forms  
                                      English Teaching/Graduate Assistantship Application  
                                      Graduate Advising Worksheet  
                                      Graduate Independent Study Form  
                                      Thesis Hour Registration and Prospectus Form  
                                      Dissertation Hour Registration Form  
                                  
                               
                                Graduate Catalog  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now  
                                
                            
                            
                                Alumni and Friends  
                                
                            
                            
                                Course Offerings  
                                
                            
                            
                                Contact Us  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/30/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


