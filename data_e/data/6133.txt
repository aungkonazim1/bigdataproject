K nearest neighbor percolation - Math Sciences - University of Memphis    










 
 
 
     



 
    
    
    K nearest neighbor percolation - 
      	Math Sciences
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Mathematical Sciences
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          People  
                          Undergraduate  
                          Graduate  
                          Research  
                          News  
                      
                      
                         
                            People   
                            
                               
                                  
                                   Research Faculty  
                                   Instructors  
                                   Visitors   Postdocs  
                                   Graduate Students  
                                   Staff  
                                   A-Z List  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Math Sciences
                              	  
                         
                           	People
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                     
                     
                     
                       k -nearest neighbor percolation
                      
                     
                     
                      This page is designed to give more information on the derivation of the results
                        in  [1] . In particular, the C source files for
                        the programs used to obtain the results stated there.
                        For notation and mathematical details, see  [1] .
                      
                     
                     
                      Rigorous Bounds 
                     
                     
                      Documentation is included with the source file.
                        The test to perform and parameters used are #define'd
                        in the first few lines. (All parameters are hard-coded in the source file.)
                        The following parameters need to be set before compiling the code:
                      
                     
                      
                        	
                          X = one of 'U', 'O', 'I', 'B', defining the type of percolation. Note that there
                           is no 'S' option.
                         
                        	
                          KMIN = minimum value of  k .
                         
                        	
                          KMAX = maximum value of  k .
                         
                        	
                          ACC = an accuracy parameter. Increase to make bounds tighter (but program slower). 
                        
                      
                     
                      The program calculates the bounds in Table 1 of the paper for a range of  k  values.
                        The results (for suitable values of the parameters) should be exactly as given in
                        the paper.
                        Success (proof of percolation) is achieved if the bound given is at most
                        (1 - 0.8639)/2 = 0.06805.
                        
                      
                     
                      The running time of program is O(ACC^4) while the accuracy of the bound is O(ACC^{-2}). 
                     
                      C source   
                      Output 
                     
                     
                      High Confidence Results 
                     
                     
                      Documentation is included with the source file.
                        The test to perform and parameters used are #define'd
                        in the first few lines. (All parameters are hard-coded in the source file.)
                        The following parameters need to be set before compiling the code:
                      
                     
                      
                        	
                          K =  k  is the number of nearest neighbors.
                         
                        	
                          X = one of 'U', 'O', 'I', 'B', defining the type of percolation. Note that there
                           is no 'S' option.
                         
                        	
                          BND = either 'L' for the lower bound proof (showing percolation fails for this value
                           of  k ),
                           		or 'U' for the upper bound proof (showing percolation succeeds for this value of
                            k ).
                         
                        	
                          R =  r  is the radius of the central disk in Figure 2 of the paper.
                         
                        	
                          S =  s  is the distance between the central disk and the boundary
                           	of the rectangle in Figure 2 of the paper.
                         
                        	
                          ITER = the number of trials that will be performed. 
                        
                      
                     
                      Values of these parameters and the results obtained are given in   [1] .
                        The random number seed (SEED) can be changed to give independent tests, otherwise
                        the results (for the same parameters)
                        should be exactly as given in   [1] .
                        
                      
                     
                      C source 
                     
                     
                      Reference   
                     
                     
                      [1] Paul Balister, Béla Bollobás.
                         Percolation in the  k -nearest neighbor graph .
                        In  Recent Results in Designs and Graphs: a Tribute to Lucia Gionfriddo ,
                        Quaderni di Matematica, Volume 28.
                        Editors: Marco Buratti, Curt Lindner, Francesco Mazzocca, and Nicola Melone, (2013),
                        83–100.
                        
                      
                     
                     
                     
                     
                     		
                     
                     
                     	
                      
                   
                
                
                   
                      
                         People 
                         
                            
                               
                                Research Faculty  
                                Instructors  
                                Visitors   Postdocs  
                                Graduate Students  
                                Staff  
                                A-Z List  
                            
                         
                      
                      
                      
                         
                            
                                Student FAQ  
                               Student resources for advising, tutoring, and registering for classes. 
                            
                            
                                ALEKS  
                               Information on the ALEKS computerized placement test. 
                            
                            
                                Contact Us  
                               How to contact us, and whom to contact about what. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/14/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


