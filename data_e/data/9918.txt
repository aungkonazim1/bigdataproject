Managing dropbox folder submission handling | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Managing dropbox folder submission handling | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Dropbox    »    Creating and managing Dropbox   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Managing dropbox folder submission handling                       
        
        
       
        
     
              
	Restrict the number of files allowed per submission
 

  
		On the New Folder or Edit Folder page, go to the Submission Options section in the Properties tab to select one of the following  Files allowed per submission  options:

		  
				Unlimited
			 
			 
				One file per submission
			 
		  
	 
		Click  Save .
	 
  
	Change how subsequent file submissions are handled
 

  
		On the New Folder or Edit Folder page, go to the Submission Options section in the Properties tab to select one of the following  Submissions  options:

		  
				Keep all submissions
			 
			 
				Overwrite submissions
			 
			 
				Only one submission allowed
			 
		  
	 
		Click  Save .
	 
      Audience:     Instructor       

    
           

                   ‹ Creating dropbox folders 
        
                   up 
        
                   Setting dropbox folder availability and due dates › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Dropbox  
  
      Dropbox basics    Creating and managing Dropbox    Creating dropbox categories    Creating dropbox folders    Managing dropbox folder submission handling    Setting dropbox folder availability and due dates    Setting release conditions for a dropbox folder    Adding special access permissions to a dropbox folder    Editing dropbox categories and folders    Reordering dropbox categories and folders    Deleting dropbox categories and folders    Restoring deleted dropbox folders    Viewing the Dropbox event log    Associating dropbox folders with learning objectives    Previewing dropbox folders and submissions      Evaluating dropbox folder submissions    
                  
           
         

       
     

    
    
    
   
 
   
 
