Part-Time Faculty Pool, Political Science   
 
   
       
       Part-Time Faculty Pool, Political Science 
 
 
   
   
   
   
   
   
   

 
 
 
 
 
 
 
 
 

     

   
   
       
       
         
           Skip to Main Content 
         
         
           
             
   
     
       
         
           
         
       
       
         
          University of Memphis Career Opportunities  
       
     
   
 

           
           
             
   
     
       
         Toggle navigation 
          
          
          
       
     
     
       
    Home  
    Search Jobs  
      Create Account  
      Log In  
    Help  
 

       
             
               University Benefits 
             
             
               Equal Employment 
             
             
               Annual Security Report 
             
             
               Campus Map 
             
             
               About UofM 
             
       
     
   
 

             
               
                
                   Part-Time Faculty Pool, Political Science 
  
  
   
       Bookmark this Posting  |
     Print Preview  |
         Apply for this Job 
   


   
     
       Posting Details 
        
       
            

               Posting Information 
                
               
                     
                       Posting Number 
                         PTF325 
                     
                     
                       Advertised Title 
                         Part-Time Faculty Pool, Political Science 
                     
                     
                       Position Number 
                         004681 
                     
                     
                       Department 
                         Political Science 
                     
                     
                       Job Summary 
                          Teach one or two courses for the University of Memphis, generally at the undergraduate level. 
  THIS   POSTING  IS  NOT  A  GUARANTEE  OF AN  OPEN   POSITION . Applications for part-time faculty positions are accepted on a continuous basis and reviewed by the department chair when openings become available. Future teaching assignments are on a term-by-term basis, which may include fall, spring and summer. 
 Postings close yearly on January 31; to maintain your application within the system, you will need to re-apply each year. 
 At the time of application, applicants should be prepared to upload a cover letter, resume/CV, references list, statement of teaching philosophy,   an unofficial transcript.  
                     
                     
                       Minimum Position Qualifications 
                          Master’s degree in the teaching discipline or master’s degree in another discipline with 18 hours of graduate work in the teaching discipline. Previous higher education teaching experience strongly desired. 
 Selected candidates must agree to and pass a criminal background check in order to be employed.  
                     
                     
                       Special Conditions 
                          
                     
                     
                       Posting Date 
                         03/01/2017 
                     
                     
                       Closing Date 
                         01/31/2018 
                     
                     
                       Open Until Screening Begins 
                         No 
                     
                     
                       Hiring Range 
                         From $700 per credit hour 
                     
                     
                       Special Instructions to Applicants 
                          
                     
                     
                       Is this posting for UofM employees only? 
                         No 
                     
                 
            
       
     
  
     Supplemental Questions 
       
            Required fields are indicated with an asterisk (*).  
 
       *  
  At what location(s) would you be able to teach for the University?
     
         All locations, including on-site, satellite, and online courses 
         On-site, main campus courses only 
         On-site, main campus AND Memphis area satellite locations 
         On-site, main campus AND all satellite locations, including Lambuth   Dyersburg campuses 
         Lambuth campus only 
         Dyersburg campus only 
         Online courses only 
     
 


     *  
  What is your current employment status at the University of Memphis?
     
         I have never been employed by the University of Memphis. 
         Former employee (not currently employed) 
         Current full-time/part-time employee 
         Current full-time/part-time temporary employee 
         Current graduate assistant 
         Current student worker 
     
 


     *  
  Do you have previous college teaching experience?
     
         Yes 
         No 
     
 


     *  
  Do you have at least a master s degree in the teaching discipline or a master s degree with a concentration in the teaching discipline?
     
         Yes 
         No 
     
 


     *  
  Have you ever been an employee, student, or alumnus of the University of Memphis, and/or had a  memphis.edu  email address or ID number?
     
         Yes 
         No 
     
 



 

       
     Applicant Documents 
       
           Required Documents 
     
     Resume 
     Cover Letter 
     Teaching Philosophy 
     References List 
     Unofficial Transcript 
 

 Optional Documents 
     
 



       


 


                  
               
                
             
           
           
             
   
     University of Memphis — Human Resources 
165 Administration Building, Memphis, TN 38152 
901.678.3573  workforce@memphis.edu  
 Follow us on Twitter  
      
      ©University of Memphis. All Rights Reserved. The University of Memphis is an Equal Opportunity/Affirmative Action university.  
   
 

           
         
       
         
 
        
  

  


    
      
    
     
   
 
