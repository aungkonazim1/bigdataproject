Graduate - Department of Psychology - University of Memphis    










 
 
 
     



 
    
    
    Graduate - 
      	Department of Psychology
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Psychology
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Undergraduate  
                          Graduate  
                          People  
                          Centers  
                          Research  
                          Resources  
                      
                      
                         
                            Graduate   
                            
                               
                                  
                                   PhD in Clinical Psychology  
                                         About the Program  
                                         Clinical Faculty  
                                         Child Clinical  
                                         Clinical Health Psychology  
                                         Psychotherapy Research  
                                         Admission  
                                         Clinical Student Handbook  
                                         Clinical Forum Schedule  
                                         Clinical Program Announcements  
                                         Clinical Alumni News  
                                     
                                  
                                   PhD in Experimental Psychology  
                                         About the Program  
                                         Experimental Faculty  
                                         Cognitive  
                                         Behavioral Neuroscience  
                                         Required Courses  
                                         Admission  
                                         Financial Support  
                                     
                                  
                                   PhD in School Psychology  
                                         About the Programs  
                                         School Faculty  
                                         Historical Statement  
                                         Financial Support  
                                         M.S./Ph.D. Program Philosophy and Training Model  
                                         M.S./Ph.D. Program Goals and Objectives  
                                         Doctoral Handbook  
                                         School Psychology Association  
                                         Child   Family Studies Research Group  
                                     
                                  
                                   MA/Ed.S. in School Psychology  
                                         About the Programs  
                                         School Faculty  
                                         M.A./Ed.S. Program Goals  
                                         Historical Statement  
                                         Financial Support  
                                         School Psychology Association  
                                         What is a School Psychologist?  
                                         Child   Family Studies Research Group  
                                     
                                  
                                   Masters in General Psychology  
                                         About the Program  
                                         Program Goals  
                                         Required Courses  
                                         Admission  
                                     
                                  
                                   Apply to Psychology Graduate Programs  
                                         Instructions for Applying  
                                         Department Application  
                                         Graduate School Application  
                                     
                                  
                                   Graduate Student Coordinating Committee  
                                         About GSCC  
                                         GSCC Concerns/Complaints  
                                         GSCC Travel Funding  
                                     
                                  
                                   Graduate Program Forms and Resources  
                                   Clinical Student Admissions, Outcomes and Other Data  
                                   School Student Admissions, Outcomes and Other Data  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of Psychology
                              	  
                         
                           	Graduate
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Graduate Programs 
                     
                      The  Department of Psychology  is home to six  Graduate Programs:   
                     
                      Doctoral Program in  Clinical Psychology   • Research interest areas in Child and Family Studies, Clinical Health Psychology,
                        and Psychotherapy Research  • Accredited by the American Psychological Association  •  Director:  Dr. James Murphy,  jgmurphy@memphis.edu   •  Application Deadline:  December 5
                      
                     
                      Doctoral Program in  Experimental Psychology   • Research interest areas in Behavioral Neuroscience and Cognitive Psychology  •  Director:  Dr. Robert Cohen,  rcohen@memphis.edu   •  Application   Deadline:  January 15
                      
                     
                      Doctoral Program in  School Psychology   • Accredited by the American Psychological Association  •  Director:  Dr. Randy Floyd,  rgfloyd@memphis.edu   •  Application Deadline:  December 15
                      
                     
                      Master's Degree Program in General Psychology  •  Director:  Dr. Meghan McDevitt-Murphy,  mmcdvttm@memphis.edu   •  Application Deadline:  May 1
                      
                     
                      Master of Arts Program in School Psychology  • Approved by the National Association of School Psychologists  •  Director:  Dr. Thomas Fagan,  tfagan@memphis.edu   •  Application Deadline:  June 15
                      
                     
                      Educational Specialist Program in School Psychology • Approved by the National Association of School Psychologists •  Director:  Dr. Thomas Fagan,  tfagan@memphis.edu  • Application Deadline:  June 15
                      
                     
                      The  Department of Psychology  at the  University of Memphis:  
                     
                      • is a  Center of Excellence  in the State of Tennessee with over  30 full-time faculty   • provides a minimum of  four years of assistantship funding, a tuition waiver, and a health plan  to students in the doctoral programs  • provides  office space and conference travel funds  to graduate students  • fosters  involvement in research  during students' first year while developing individual research projects with their
                        primary mentor and other faculty  • in addition to excellent Department facilities, has  extensive collaborative research and training opportunities  including:  o Institute of Intelligent Systems at the FedEx Institute of Technology (on campus)
                         o St. Jude Children's Research Hospital  o University of Tennessee Health Sciences in Memphis  o Shelby County Schools  o Methodist/Le Bonheur Healthcare  o VA Hospital  o Shelby County Head Start   Issues of Licensure.  Please note that both the Clinical Doctoral Program and the School Psychology Programs
                        in the Department of Psychology may lead to professional licensure. The following
                        questions relate to your potential ability to receive licensure.   1.     Have you ever been convicted of a felony or crime(s) other than minor traffic
                        offenses?   2.     Have you ever been denied licensure for the profession for which you might
                        apply for licensure or had discipline imposed by another state's licensing board?
                          3.     Have you ever had a civil suit judgment entered against you or entered into
                        an adverse civil settlement?  If you answered "yes" to any of the questions above,
                        it is possible that you may be denied licensure at the conclusion of the degree program
                        to which you are applying.  Please review the licensure requirements specific to the
                        discipline by contacting the specific licensing board.  It is your responsibility
                        to understand the criteria necessary for licensure and to ensure compliance with those
                        criteria.     If you have any questions about the licensure requirements and your ability to be
                        licensed, please contact:  Clinical Program: Dr. James Murphy ( jgmurphy@memphis.edu )  School Program: Dr. Tom Fagan ( tfagan@memphis.edu ).   Also, you should be aware that criminal convictions may make you ineligible to participate
                        in any clinical experiences included in your program, therefore necessitating your
                        removal from the program and/or impacting your ability to successfully complete course
                        and program requirements.  Your signature on the application shall serve as your acknowledgment
                        that you have answered the above questions truthfully, have conducted due diligence
                        in determining your potential eligibility for licensure, and that you are aware that
                        certain factors may prevent your eligibility for licensure.     
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Graduate 
                         
                            
                               
                                PhD in Clinical Psychology  
                                      About the Program  
                                      Clinical Faculty  
                                      Child Clinical  
                                      Clinical Health Psychology  
                                      Psychotherapy Research  
                                      Admission  
                                      Clinical Student Handbook  
                                      Clinical Forum Schedule  
                                      Clinical Program Announcements  
                                      Clinical Alumni News  
                                  
                               
                                PhD in Experimental Psychology  
                                      About the Program  
                                      Experimental Faculty  
                                      Cognitive  
                                      Behavioral Neuroscience  
                                      Required Courses  
                                      Admission  
                                      Financial Support  
                                  
                               
                                PhD in School Psychology  
                                      About the Programs  
                                      School Faculty  
                                      Historical Statement  
                                      Financial Support  
                                      M.S./Ph.D. Program Philosophy and Training Model  
                                      M.S./Ph.D. Program Goals and Objectives  
                                      Doctoral Handbook  
                                      School Psychology Association  
                                      Child   Family Studies Research Group  
                                  
                               
                                MA/Ed.S. in School Psychology  
                                      About the Programs  
                                      School Faculty  
                                      M.A./Ed.S. Program Goals  
                                      Historical Statement  
                                      Financial Support  
                                      School Psychology Association  
                                      What is a School Psychologist?  
                                      Child   Family Studies Research Group  
                                  
                               
                                Masters in General Psychology  
                                      About the Program  
                                      Program Goals  
                                      Required Courses  
                                      Admission  
                                  
                               
                                Apply to Psychology Graduate Programs  
                                      Instructions for Applying  
                                      Department Application  
                                      Graduate School Application  
                                  
                               
                                Graduate Student Coordinating Committee  
                                      About GSCC  
                                      GSCC Concerns/Complaints  
                                      GSCC Travel Funding  
                                  
                               
                                Graduate Program Forms and Resources  
                                Clinical Student Admissions, Outcomes and Other Data  
                                School Student Admissions, Outcomes and Other Data  
                            
                         
                      
                      
                      
                         
                            
                                Psychology Graduate Programs Application  
                               Click on link to Apply to the Department of Psychology Graduate Program 
                            
                            
                                Academic Advising   Resource Center (AARC)  
                                The AARC provides advising to students helping them make the most of their undergraduate
                                 education at the UofM.
                               
                            
                            
                                The Psychological Services Center  
                               PSC provides general outpatient psychotherapeutic and psychological assessment services
                                 to individuals and families
                               
                            
                            
                                Teaching Take-Out  
                               This website is a resource for busy teachers who want to enrich their classes while
                                 preserving the time they need for research and other important professional activities.
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


