Flu Shots - Student Health Center - University of Memphis    










 
 
 
     



 
    
    
    Flu Shots - 
      	Student Health Center
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Student Health Center
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Emergency  
                          Immunization  
                          Clinics  
                          Policies  
                          Tiger Health 101  
                      
                      
                         
                            Immunization   
                            
                               
                                  
                                   Documentation Upload  
                                   Certificate of Immunization  
                                   Exemption  
                                   Flu Shots  
                                   Health Prerequisite Summary  
                                   Hepatitis B - Meningitis  
                                   Hepatitis B - Meningitis Form (under 18 only)  
                                   MMR   Varicella  
                                   TB Skin Test  
                                   Vaccination Availability  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Student Health Center
                              	  
                          
                              	Immunization
                              	  
                         Flu Shots 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Flu Shots 
                     
                      The University of Memphis Student Health Center highly recommends everyone get vaccinated
                        to protect themselves and those around them against influenza.  The immunization takes
                        about 10 days for immunity to develop, so the sooner you are immunized, the better.  Check with your doctor or another health care professional if you have questions about
                           which vaccine you should get.  
                     
                        View our 2017-2018 flu shot schedule, location, cost and payment information!   
                     
                      Learn about everything you need to know about the flu illness, including symptoms,
                        treatment and prevention  on the CDC website.  
                     
                       REMEMBER:  
                     
                      
                        
                         WASH YOUR HANDS with soap and water. 
                        
                         COVER YOUR COUGH with your elbow. 
                        
                         STAY AWAY FROM CROWDS when influenza is present. 
                        
                         STAY HOME if you are diagnosed with the flu.  
                        
                      
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Immunization 
                         
                            
                               
                                Documentation Upload  
                                Certificate of Immunization  
                                Exemption  
                                Flu Shots  
                                Health Prerequisite Summary  
                                Hepatitis B - Meningitis  
                                Hepatitis B - Meningitis Form (under 18 only)  
                                MMR   Varicella  
                                TB Skin Test  
                                Vaccination Availability  
                            
                         
                      
                      
                      
                         
                            
                                Upload Immunization Documentation  
                               Upload state mandated immunizations documentation to meet certain health requirements. 
                            
                            
                                Family Planning Clinic  
                               Service provided by the Memphis and Shelby County Public Health Department. 
                            
                            
                                Health Education Resources  
                               Useful links to help bump up your health IQ! 
                            
                            
                                Contact Us  
                               Main contact information and hours of operation. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


