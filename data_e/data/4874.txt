Joanne Rhodes - Department of Physics and Materials Science - University of Memphis    










 
 
 
     



 
    
    
    Joanne Rhodes - 
      	Department of Physics and Materials Science
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Physics and Materials Science
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Undergraduate  
                          Graduate  
                          People  
                          Research  
                          Resources  
                          News  
                          SPS  
                      
                      
                         
                            Joanne Rhodes   
                            
                               
                                  
                                   Profile  
                                   Teaching  
                                   Research  
                                   Publications  
                                   Department Faculty  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of Physics and Materials Science
                              	  
                         
                           	J. Rhodes
                           	
                         
                      
                   
                   
                       
                      
                     
                     	
                     
                     		  
                      
                        
                        
                         
                           
                           
                                                                
                              
                              
                               
                              
                              
                            
                           
                           
                            
                              
                               
                                 
                                 Joanne Rhodes
                                 
                               
                              
                              
                               
                                 
                                 Instructor
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Phone 
                                    
                                     
                                       
                                       (901) 678-2410
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Email 
                                    											
                                     
                                       
                                        joanne.rhodes.uofm@gmail.edu 
                                        
                                    
                                  
                                 
                                  
                                    
                                     Fax 
                                    
                                     
                                       
                                       (901) 678-4733
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Office 
                                    
                                     
                                       
                                       MN 208
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Office Hours 
                                    
                                     
                                       
                                       Call for hours
                                       
                                     
                                    
                                  
                                 
                               
                              									
                                Thomas W. Briggs Foundation Award Links  
                              
                               
                                 
                                 
                                 
                                 
                                 
                                   Press Release  
                                 
                                   Helmsman Article  
                                 
                                 
                               
                              									
                              															
                              									
                              
                            
                           
                           
                         
                        			 
                        			  
                        
                        
                         Biography 
                        
                         Joanne Farley Rhodes graduated from the University of Memphis with a BSEE and a BS
                           in physics. While an undergraduate student she worked in basic research on irradiated
                           thermoluminescent crystals in the Dept. of Physics and began publishing at that time.
                         
                        
                         After graduation she did research in advanced optical signal processing techniques
                           for DOD. A patent was awarded for her invention developed there with a colleague.
                           The Optical Adaptive Filter was the first real time optical feedback system. The invention
                           and experimental results have been included in two books on optical electronics.
                         
                        
                         Still at DOD, she returned to research with irradiated crystals for applications in
                           radiation dosimetry. After several years of publishing in the this field of research
                           she returned to the University of Memphis and completed her Master of Science in the
                           Dept. of Physics.
                         
                        
                         She began teaching part time in 1990 and enjoyed it so much that her interests shifted
                           toward math and physics education.
                         
                        
                         She has taught in the Dept. of Physics and/or the Dept. of Mathematics at several
                           colleges and universities in Memphis and in St. Louis. Most recently she joined the
                           faculty of the Dept. of Physics at University of Memphis and has been teaching classes
                           in astronomy and conceptual physics here since 2009. The first in the Dept. of Physics
                           to give online exams, she incorporates the use of technology into her classroom in
                           ways which are intended to improve the educational experience of students.
                         
                        
                         As either a student and/or alum of the University of Memphis Dept. of Physics since
                           1976 when she took the astronomy class she now teaches, she has seen many changes
                           and events in the Dept. of Physics. A favorite was the 1979 Einstein Centennial spearheaded
                           by the Dept. of Physics and produced in conjunction with other university departments
                           and the greater Memphis community. With an avid historical and current interest in
                           the Dept. of Physics, she coordinated its 2011 Einstein Birthday celebration and its
                           2012 University of Memphis Centennial event and Marchini Scholarship fundraiser.
                         
                        
                         She continues to enjoy sharing her passion about physics and astronomy with her students
                           and highly values increasing student science literacy in the process.
                         
                        
                        
                      
                     								
                     
                     
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Joanne Rhodes 
                         
                            
                               
                                Profile  
                                Teaching  
                                Research  
                                Publications  
                                Department Faculty  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now  
                               Now accepting applications for the Undergraduate and Graduate Programs, MS in Physics
                                 and Materials Science and PhD in Engineering Physics!
                               
                            
                            
                                Contact Us  
                               Main office location and numbers, undergraduate and graduate advising, and Alumni
                                 information update form
                               
                            
                            
                                Seminars   Events  
                               The department presents weekly seminars during the spring and fall semesters 
                            
                            
                                Careers in Physics and Materials Science  
                               Find out what opportunities are available to you with a degree in Physics and Materials
                                 Science
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


