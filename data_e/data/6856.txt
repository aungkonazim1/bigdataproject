Kas Saghafi - Philosophy - University of Memphis    










 
 
 
     



 
    
    
    Kas Saghafi - 
      	Philosophy
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Philosophy
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Undergraduate  
                          Graduate  
                          People  
                          Research  
                          News   Events  
                          Resources  
                          Contact  
                      
                      
                         
                            People   
                            
                               
                                  
                                   Core Faculty  
                                   Visiting Scholars  
                                   Instructors  
                                   Emeritus Faculty  
                                   Graduate Students  
                                   Staff  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Philosophy
                              	  
                          
                              	People
                              	  
                         Kas Saghafi 
                      
                   
                   
                       
                      
                     
                     	
                     
                     		  
                      
                        
                        
                         
                           
                           
                                                                
                              
                              
                               
                              
                              
                            
                           
                           
                            
                              
                               
                                 
                                 Kas Saghafi
                                 
                               
                              
                              
                               
                                 
                                 Associate Professor of Philosophy
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Phone 
                                    
                                     
                                       
                                       901.678.2535
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Email 
                                    
                                     
                                       
                                       ksaghafi@memphis.edu
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Fax 
                                    
                                     
                                       
                                       901.678.4365
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Office 
                                    
                                     
                                       
                                       301 Clement Hall
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Office Hours 
                                    
                                     
                                       
                                       MWF 11:00-11:30am, and by appointment
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 										
                                 
                                 
                                                                         
                                 
                               
                              
                            
                           
                           
                         
                        			 
                        			  
                        
                        
                         About Professor Saghafi 
                        
                         Professor Saghafi (PhD, DePaul University, Chicago) joined the faculty in 2006. He
                           researches and teaches in contemporary Continental philosophy, philosophy and literature,
                           aesthetics, and social/political philosophy. He is the author of Apparitions—Of Derrida's
                           Other (Fordham University Press, 2010). He has also published articles on contemporary
                           French thought in journals such as Bulletin de la Société Américaine de Philosophie
                           de Langue Française, International Studies in Philosophy, Mosaic, Parallax, Philosophy
                           Today, and Research in Phenomenology. Co-translator of Derrida's "A Europe of Hope"
                           and "Aletheia," Dr. Saghafi is a regular participant in the  Derrida Seminars Translation Project , held annually at the IMEC archives in Normandy, France.
                         
                        
                         Dr. Saghafi was a Visiting Professor at the Center for Comparative Literature and
                           Cultural Studies at Monash University in Australia. Previously, he held an Andrew
                           W. Mellon Post-doctoral Fellowship at Grinnell College, where he offered courses in
                           philosophy and the humanities. While earning his doctorate, he was awarded a Chateaubriand
                           dissertation research grant from the French government.
                         
                        
                         Dr. Saghafi teaches undergraduate courses in existentialism, philosophy and literature,
                           philosophy and film, and aesthetics. Recent graduate courses he has offered include:
                           Levinas' Totality and Infinity, Derrida on Sovereignty, Nancy and Blanchot, and Recent
                           Continental Philosophy on themes such as "the Other" and "World."
                         
                        
                         Selected Publications 
                        
                          Books  
                        
                         Apparitions—Of Derrida's Other (New York: Fordham University Press, 2010). 
                        
                          Edited Volumes  
                        
                         Editor,  "Derrida and the Theologico-political,"  Spindel Conference Proceedings, Southern Journal of Philosophy Supplement (2012).
                         
                        
                         Co-editor, Special Issue, "An Entrusted Responsibility: Reading and Remembering Jacques
                           Derrida," Epoché 10, 2 (Spring 2006).
                         
                        
                          Articles in Journals  
                        
                         " For a Time ," Journal of French and Francophone Philosophy vol. 23, no.2 (2015): 122-30.
                         
                        
                         " Dying Alive ," Mosaic 48.3 (September 2015): 15-26.
                         
                        
                         " Revolutionary Derrida ," Oxford Literary Review 36.2 (2014), Special Issue "A Decade After Derrida": 303-5.
                         
                        
                         " Thomas the Marvelous: Resurrection and Living-Death in Blanchot and Nancy ," Mosaic 45.3 (September 2012): 1-16.
                         
                        
                         " Incurable Haunting: Saluting Michel Deguy ,"Oxford Literary Review 33.2 (2011): 245-264.
                         
                        
                         " 'A Certain Spirit of a Certain Marx': Blanchot's Revolutionary Return in Specters
                              of Marx " Philosophy Today 55 (2011) Supplement, Selected Studies in Phenomenology and Existential
                           Philosophy: 183-91.
                         
                        
                         " The Chase: Rivalry and Conjuration ," Parallax 58 (January-March 2011): 33-41.
                         
                        
                         " Salut-ations ," Mosaic: A Journal for the Interdisciplinary Study of Literature 39, 3 (September
                           2006): 151-172. Special issue "After Derrida."
                         
                        
                         " The Ghost of Jacques Derrida " Epoché 10, 2 (Spring 2006): 263-286.
                         
                        
                         "' An Almost Unheard-of Analogy': Derrida Reading Levinas ," Bulletin de la Société Américaine de Philosophie de Langue Française 15, 1 (2005):
                           41-71.
                         
                        
                         " Phantasmaphotography ," Philosophy Today vol. 44 (2000) Supplement, Selected Studies in Phenomenology and
                           Existential Philosophy: 98-111.
                         
                        
                         " The 'Passion for the Outside': Foucault, Blanchot, and Exteriority ," International Studies in Philosophy, vol. XXVIII, no. 4 (1996): 79-92.
                         
                        
                          Chapters in Books  
                        
                         "The Desire for Survival?" in Desire in Ashes: Deconstruction, Psychoanalysis, Philosophy,
                           Edited by Simon Morgan Wortham and Chiara Alfano (New York   London: Bloomsbury, 2015):
                           139-160.
                         
                        
                         " Safe, Intact: Derrida, Nancy, and the 'Deconstruction of Christianity ,'" in A Companion to Derrida, Edited by Leonard Lawlor and Zeynep Direk (Walden,
                           MA: Wiley-Blackwell, 2014): 447-463.
                         
                        
                         "Maurice Blanchot (1907-2003)," Cambridge Foucault Lexicon, eds. Leonard Lawlor and
                           John Nale (Cambridge: Cambridge University Press, 2014): 52-6.
                         
                        
                          Translations  
                        
                         Jacques Derrida, "Aletheia," Oxford Literary Review 32.2 (2010), 169-88. Co-translated
                           with Pleshette DeArmitt.
                         
                        
                         Jacques Derrida, "A Europe of Hope," Epoché 10, 2 (Spring 2006): 407-12. Co-translated
                           with Pleshette DeArmitt, and Justine Malle.
                         
                        
                        
                      
                     								
                     
                     
                     
                     
                     	
                      
                   
                
                
                   
                      
                         People 
                         
                            
                               
                                Core Faculty  
                                Visiting Scholars  
                                Instructors  
                                Emeritus Faculty  
                                Graduate Students  
                                Staff  
                            
                         
                      
                      
                      
                         
                            
                                Apply to the Graduate Program  
                               Want to pursue a MA or PhD in Philosophy? Find out how to apply. 
                            
                            
                                Online B.A.  
                               Want to pursue your college degree and manage your busy lifestyle? Experience online
                                 classes at the UofM.
                               
                            
                            
                                Pre-law Advising  
                               Are you interested in a career in the legal profession? Consider a degree in Philosophy! 
                            
                            
                                Support the Department  
                               Find out how you can support the Department of Philosophy. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


