Kettinger, W. J. and Grover, V. 1995. "Towards and Theory of Business Process Change Management," Journal of Management Information Systems (12:1), pp. 9-30. - BIT - University of Memphis    










 
 
 
     



 
    
    
    Kettinger, W. J. and Grover, V. 1995. "Towards and Theory of Business Process Change
      Management," Journal of Management Information Systems (12:1), pp. 9-30.  - 
      	BIT
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - Department of Business Information and Technology
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          BIT  
                          Programs  
                          Faculty  
                          Research  
                          Prospective Students  
                          Current Students  
                          FCBE  
                      
                      
                         
                            Research Focus   
                            
                               
                                  
                                   MIS Quarterly  
                                   Information Systems Research  
                                   Journal of AIS  
                                   Journal of MIS  
                                   European Journal of Information Systems  
                                   Information Systems Journal  
                                   MIS Quarterly Executive  
                                   Sloan Management Review  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	BIT
                              	  
                          
                              	Research
                              	  
                         Kettinger, W. J. and Grover, V. 1995. "Towards and Theory of Business Process Change
                           Management," Journal of Management Information Systems (12:1), pp. 9-30. 
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     This special section comes during a period of tumultuous change in business. Global
                     competition, economic downturn, and the potential offered by emerging technologies
                     are pushing firms to fundamentally rethink their business processes. Many firms have
                     reached the conclusion that effecting business process change is the only way to leverage
                     their core competencies and achieve competitive advantage. This belief has led to
                     a near "re-engineering frenzy."
                     
                      Consultants, seeking to provide solutions to these issues, prescribe business process
                        re-engineering (BPR) as a means to re-engineer aging processes to achieve strategic
                        objectives. BPR practitioners typically repackage existing change theories and techniques
                        from organizational behavior and design, information system management, operations
                        research, quality and human resources disciples in a new synthesis directed at dramatic
                        improvements in business performance.
                      
                     
                      While earlier cross-functional process redesign conceptualizations including Porter's
                        [93] value chain analysis and Gibson and Jackson's [38] business transformation via
                        information technology (IT) existed, it was the writings of both Davenport and Short
                        [21] and Hammer [48] that triggered intense interest from both academia practitioners
                        in re-engineering. Similar to the "classic" success stories touted on strategic information
                        systems about a decade ago [64], early literature on BPR included many examples of
                        BPR successes, such as Ford, Hallmark, Bell Atlantic, Taco Bell [47], AT T [45], Kodak
                        [108], Texas Instruments, Merck and Cigna [104].
                      
                     
                      Despite the five odd years this phenomenon has been the rage, there is little research
                        support for its effectiveness beyond anecdotal evidence. This is in part because no
                        theory describing, explaining, and predicting the impact of BPR has been presented
                        to guide the progress of empirical research. In fact, the role that this phenomenon
                        has played in the formalization and advancement of management theory (or vice versa)
                        remains a relatively unexplored topic.
                      
                     
                      Clearly, the purpose of radical process change (re-engineering), as well as more incremental
                        business process improvement approaches (continuous improvement), is the transformation
                        of business processes. The desire to achieve such transformation has served to propel
                        practice ahead of formalized theory. We believe that the formalization of the theoretical
                        context of effective business process change management is essential for improved
                        implementation and, more generally, to advance systematic inquiry in the field.
                      
                     
                      The goal of this special section introduction is to present discussion that moves
                        toward a theory of business process change (BPC) management. The beginnings of such
                        a theory are based upon both conceptual synthesis of observations from practice as
                        well as drawn from research literature from several related social science disciples.
                      
                     
                      Our analysis leads to the conclusion that the theoretical basis of business process
                        change should concern the creation of an organizational environment that develops
                        a culture supportive of change through learning, knowledge sharing (including IT enablement),
                        and internal and external network partnering which facilitates the implementation
                        of effective process and change management practice, which leads to improvement in
                        business processes and greater stakeholder benefits, both of which are important in
                        achieving measurable performance improvements. Implied in this statement is the vital
                        role that the strategic leader plays in establishing strategic initiatives such as
                        communicating a vision to move the organization toward business process change and
                        providing tangible support to enable and maintain an organizational environment that
                        is receptive to BPC management practice.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Research Focus 
                         
                            
                               
                                MIS Quarterly  
                                Information Systems Research  
                                Journal of AIS  
                                Journal of MIS  
                                European Journal of Information Systems  
                                Information Systems Journal  
                                MIS Quarterly Executive  
                                Sloan Management Review  
                            
                         
                      
                      
                      
                         
                            
                                BIT News  
                               Publications, recognitions and spotlights about faculty and students of the BIT department. 
                            
                            
                                BIT Events  
                               Find out what's happening at the department and visit our weekly colloquium. 
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


