 Northern Embayment Lithospere Experiment (NELE) - CERI - University of Memphis    










 
 
 
     



 
    
    
     Northern Embayment Lithospere Experiment (NELE) - 
      	CERI
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Center for Earthquake Research and Information
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Study  
                          Research  
                          People  
                          Public  
                          News  
                          Donate  
                      
                      
                         
                            NELE Resources   
                            
                               
                                  
                                   Introduction  
                                   Abstract  
                                   Project Summary  
                                   Pieces of the Puzzle  
                                   Tectonic Setting  
                                   Questions   Hypotheses  
                                   Experimental Design  
                                   Instrument Deployment Updates  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	CERI
                              	  
                          
                              	NELE
                              	  
                          Northern Embayment Lithospere Experiment (NELE) 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Northern Embayment Lithospere Experiment (NELE) 
                     
                      Abstract 
                     
                      Passage of the EarthScope Transportable Array across the Mississippi Embayment is
                        providing an opportunity to investigate lithospheric structure associated with active
                        deformation and an ancient, intracratonic rift. The Mississippi Embayment is a first-order
                        geological structure of the North American continent with a long and complex geological
                        history affected by at least two periods of continental breakup. The reason for geologically
                        recent basin subsidence is enigmatic since subsidence began in Late Cretaceous and
                        is not linked to a major tectonic episode. In addition, the active New Madrid seismic
                        zone (NMSZ) is located in the upper crust within the northern Mississippi Embayment.
                        Lithospheric seismic velocity heterogeneity and anisotropy are likely indicators of
                        the dynamic processes associated with basin subsidence and earthquake genesis in the
                        region.
                      
                     
                        
                     
                      The seismic experiment incorporates stations of the USArray and a portable FlexArray
                        deployment to provide an overall framework of lithospheric velocity and anisotropy
                        structure to depths including the upper mantle transition zone. Detailed variations
                        in deep structure perpendicular and parallel to the embayment axis are imaged using
                        three dense lines of FlexArray stations that extend within and outside of the Mississippi
                        Embayment.
                      
                     
                      Major issues that are addressed include: 
                     
                      
                        
                         the primary differences in lithospheric structure between the embayment and the surrounding
                           region
                         
                        
                         the nature of early Cambrian rifting and relationship to pre-existing structure 
                        
                         the dynamic processes responsible for basin subsidence, and 
                        
                         the relationship of the NMSZ to large scale lithospheric structure. 
                        
                      
                     
                      This is accomplished through joint interpretation of compressional and shear velocity
                        tomography, potential fields, transfer/receiver functions, and depth-dependent anisotropy
                        images generated using traditional and new geophysical techniques.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         NELE Resources 
                         
                            
                               
                                Introduction  
                                Abstract  
                                Project Summary  
                                Pieces of the Puzzle  
                                Tectonic Setting  
                                Questions   Hypotheses  
                                Experimental Design  
                                Instrument Deployment Updates  
                            
                         
                      
                      
                      
                         
                            
                                Graduate Study at CERI  
                               Want to perform cutting edge research in a collegial and supportive environment? 
                            
                            
                                Seismic information  
                               View a list of Stations, Earthquake Catalogs, Monitoring Partners, and more... 
                            
                            
                                Earthquake Resources  
                               Recent Earthquakes, Education   Outreach, Preparedness, Data Products, Media Products,
                                 and more...
                               
                            
                            
                                Contact Us  
                               For questions or more information, please contact the main office of CERI 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


