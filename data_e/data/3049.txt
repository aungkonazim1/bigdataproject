iPrint Solutions - umTech - Information Technology Services - University of Memphis    










 
 
 
     



 
    
    
    iPrint Solutions - 
      	umTech - Information Technology Services 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			umTech - Information Technology Services 
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Service  
                          Support  
                          Teaching  
                          Smart Tech  
                          Students  
                          Solutions  
                      
                      
                         
                            Find Solutions   
                            
                               
                                  
                                   AskTom  
                                   iPrint  
                                   OU Campus  
                                   OnBase  
                                   Software  
                                   Hardware  
                                         Faculty/Staff  
                                         Students  
                                     
                                  
                                   Remedyforce  
                                   umApps  
                                   Skype for Business  
                                   umMail  
                                   umSurvey  
                                   umWiki  
                                   umWireless  
                                   Virtual Private Network (VPN)  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	umTech - Information Technology Services 
                              	  
                          
                              	Find Solutions
                              	  
                         iPrint Solutions 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                       iPrint 
                     
                         
                     
                          
                     
                      iPrint is a wireless printing system that allows students to take advantage of printing
                        services available at all Technology Access Fee (TAF) funded printers on campus. To
                        find a list of the these areas, visit the  Computer Labs/Smart Classrooms  website.
                      
                     
                      Getting Started with iPrint 
                     
                      Please note, printing requires an active account and a sufficient amount of Print
                        Funds in your free quota or personal balance. 
                      
                     
                      Email to Print 
                     
                      Email to Print is a wireless printing system that allows any web-enabled, mobile,
                        portable device to print documents simply by sending an email to any printing kiosks
                        located throughout campus.
                      
                     
                      Note: To use Email to Print, you must send the email from your University email account. 
                     
                      Supported file types: Microsoft Word, PowerPoint, Excel (2010+) 
                     
                       For step by step directions for Email to Print .
                      
                     
                       
                     
                      
                     
                      Web Print 
                     
                      Web Print is a wireless printing system that allows any web-enabled, mobile, portable
                        device to print documents simply by sending an email to any printing kiosks located
                        throughout campus.
                      
                     
                      To use Web Print: 
                     
                      Step 1 - Click My Account, log in with your username and password. 
                     
                      Step 2 - Click the Web Print Link and then click Submit a Job. 
                     
                      Step 3 - Complete the Web Print wizard by specifying the printer, number of copies
                        and selecting the document you wish to print.
                      
                     
                      Supported file types: Microsoft Word, PowerPoint, Excel (2010+) and PDF 
                     
                        For step by step directions for Web Print .  
                     
                       
                     
                      
                     
                      Print Funds 
                     
                        Checking Print Funds Balance   
                     
                              You can check your balance by logging into My Account    
                     
                              In addition, your balance will appear in the top, right corner of any TigerLAN
                        computer after log-in.
                      
                     
                        Free Quota   
                     
                              Currently enrolled students receive a printing allotment of 500 pages per
                        semester to use for the term.
                      
                     
                              The 500-page semester allotment is available the Friday before the semester
                        begins.
                      
                     
                              Any unused pages at the end of the semester are  non-refundable  and  do not roll over  to the next semester.
                      
                     
                        Purchasing Additional Funds/Pages   
                     
                              Funds may be purchased at two variations in increments of 100 pages ($5). 
                     
                      
                        
                         
                           
                            The University Bookstore in the form of a Print Funds Card (which are available at
                              the check out counter).
                            
                           
                            
                              
                               
                                 
                                  You may use Tiger Funds to purchase a Print Fund Card 
                                 
                               
                              
                               
                                 
                                  Follow the Steps on the card to redeem the code. Funds will be available for use immediately
                                    after card processing.
                                  
                                 
                               
                              
                            
                           
                         
                        
                         Online 
                        
                      
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  To add print funds online, go to My Account and select Add Print Funds from the top
                                    menu. 
                                  
                                 
                               
                              
                            
                           
                         
                        
                      
                     
                       
                     
                        Premium Printing   
                     
                      Premium printing (color printing) is now available. Color printing is only available
                        in the Technology Hub at the University Center room 265. To utilize color printing
                        you must add funds to your Color Print Balance. Funds must be added Online. Color
                        printing increments are available as follows: $5 = 15 pages, $10 = 30 pages, $15 =
                        45 pages, $20 = 50 pages. Color prints are only available in 8.5 x 11 size. 
                      
                     
                        Reimbursements:   Students may receive a refund for unused color printing in excess of 15 pages or $5.00.
                        For a monetary refund please complete, print, and bring the following  Premium Printing Reimbursement Form  to the ITS Service Desk located at 100 Administration Building. Once this form is
                        signed by a Supervisor and Director you will take it to Bursar for settlement.
                      
                     
                        Note: Monochrome (black   white) page purchases are non-refundable, but pages  will roll over  to the next semester. For amounts less than the minimum refund ($5.00), students
                              may request color pages be converted to standard monochrome (black and white) printing
                              by  entering a Self Service Request .   
                     
                      If added funds have not posted within 24 hours please contact the umTech Service Desk
                        at 901.678.8888.
                      
                     
                      Please keep the Responsible Printing Initiative in mind when printing. 
                     
                      The Responsible Printing Initiative (RPI) is an SGA/ITS cooperative designed to fairly
                        distribute printing resources, reduce waste and eliminate abuse.
                      
                     
                      Printing utilizes more than just paper. Printing costs cover the following: 
                     
                      
                        
                         Paper 
                        
                         Toner 
                        
                         Printers 
                        
                         Software 
                        
                         Servers 
                        
                         Maintenance 
                        
                      
                     
                      We hope to make printing easy for UofM students. However, if you need assistance,
                        please contact the umTech Service Desk at 901.678.8888.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Find Solutions 
                         
                            
                               
                                AskTom  
                                iPrint  
                                OU Campus  
                                OnBase  
                                Software  
                                Hardware  
                                      Faculty/Staff  
                                      Students  
                                  
                               
                                Remedyforce  
                                umApps  
                                Skype for Business  
                                umMail  
                                umSurvey  
                                umWiki  
                                umWireless  
                                Virtual Private Network (VPN)  
                            
                         
                      
                      
                      
                         
                            
                                Enter a Service Request  
                               Need Help with Technology? Request technical support via umHelpdesk, or search AskTom,
                                 a collection of questions and answers to technical problems.
                               
                            
                            
                                ITS Service Catalog  
                               Browse a collection of ITS services offered, learn how to access them and what Information
                                 is needed to fulfill service requests. 
                               
                            
                            
                                Preparing for Banner 9  
                               The UofM is upgrading to Banner 9. Keep up with the latest developments. 
                            
                            
                                Contact Us  
                               Contact the ITS Service Desk at 901.678.8888. (Available 24 hours a day, excluding
                                 posted holidays) Chat Live Monday - Friday 8:00 am - 7:00 pm CDT
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


