Spring 2017 University of Memphis Magazine &#8211; The University of Memphis President&#039;s Blog   


 

 
 
 
 


 
 
 
 

 



 Spring 2017 University of Memphis Magazine   The University of Memphis President s Blog 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 



 
 
  
 
 
 

 
 
 
 
			

         
 		
		



 
 
 
 

 

 


  Skip to content  




 

	
	 

		
		 

			
			 

				
						  The University of Memphis President s Blog  

					
			  

		  

		
		 

			 Primary Menu 
			 Menu 

			
			 
				 
					 
				 
					 Search for: 
					 
				 
				 
			 				 
			 

			 

			  
  Home    About the President  
  

		  

		 

		
		
			
				 

			
		
		 

		
	  

	
	 
	 

		
		 

			
			
	
	 

		
		
		 

			
			 Spring 2017 University of Memphis Magazine 
			
			 

				   Author   Jeanine Hornish Rakow     Published on    May 5, 2017  July 11, 2017     Leave a comment  
			  

			
		  

		
		 

			   
 
 
 
 Our latest issue of  The University of Memphis Magazine  highlights the extraordinary achievements of our students and the faculty and staff who support their many accomplishments. Get to know our Cheer team, which won the national championship of the Universal Cheerleaders Association, and our student athletes, whose athletic and academic success is recognized nationally. We have also chronicled the personal and professional journeys of two of our alums – Luther McClellan, the first African-American graduate of the University, and Donald Godwin, who provides a testimony to the benefit of persistence. 
 The University of Memphis has entered an exciting new era with the confirmation and inaugural meeting of our new governing board, the University of Memphis Board of Trustees. This board, made up of eight business and community leaders, a faculty member and a student representative, will position the University to grow and compete nationally. Read more about our board in the  Trustee Spotlight  section of our magazine. 
 We have always sought to form partnerships that strengthen the community. Throughout these pages, we discuss how researchers at the UofM are finding ways to overcome life circumstances that serve as barriers to success for many children in the region through partnerships with community organizations. 
 It truly is an exciting time to be part of the University. Go Tigers! 
 Sincerely, 
 M. David Rudd 
President 
The University of Memphis 
 
 
  
 
 
 

		  

		
		 

			  Published on    May 5, 2017  July 11, 2017      Author   Jeanine Hornish Rakow   
			
		  

		
	  

	
				
	 
		 Post navigation 
		    Previous article:  University of Memphis Board of Trustees      Next article:  Legislative Update    
	 
				

 

	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  


			
			
		  

		
	  


	
		
		
		 

		 Main Sidebar 

			
			  
				 
					 Search for: 
					 
				 
				 
			  		 		 Recent Posts 		 
					 
				 UofM Quality Indicator Score 
						 
					 
				 University of Memphis Research Foundation Announces Grand Opening of Student-Operated FedEx Call Center 
						 
					 
				 Pause to Grieve 
						 
					 
				 University of Memphis Magazine: Fall 2017 
						 
					 
				 Campus Update 
						 
				 
		 		  Recent Comments      Archives 		 
			  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  July 2014  
	  June 2014  
	  May 2014  
	  April 2014  
		 
		   Categories 		 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 
			
		  

		
		  

	
	
	 

		
		 Footer Content 

		 
		
			
				
				
								
			
		  
	
		 

			
			
			Using  Tiny Framework     
			
			   Log in  

		  
		
		 

			
			

 

	 Social Links Menu 

	      i class= fa fa-twitter   /i    
  
  

			
		  

		
	  

	
  


        

        









		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 
 