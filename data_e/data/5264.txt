Deleting dropbox categories and folders | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Deleting dropbox categories and folders | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Dropbox    »    Creating and managing Dropbox   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Deleting dropbox categories and folders                       
        
        
       
          
     
           Printer-friendly version       Delete a single dropbox folder 
  Click   Delete Folder  from the context menu of the dropbox folder you want to delete. 
 Click  Delete .   Delete multiple dropbox folders 
  Select the check boxes for the dropbox folders you want to delete, then click   Delete  from the More Actions button. 
	 Click  Delete . 
  Delete a dropbox category 
 On the Dropbox Folders page, click the   Delete  icon beside the name of the category you want to delete. 
  Note Dropbox folders in the deleted category will appear in the No Category area on the Dropbox Folders page.      Audience:    Instructor      

    
           

                   ‹ Reordering dropbox categories and folders 
        
                   up 
        
                   Restoring deleted dropbox folders › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Dropbox  
  
      Dropbox basics    Creating and managing Dropbox    Creating dropbox categories    Creating dropbox folders    Managing dropbox folder submission handling    Setting dropbox folder availability and due dates    Setting release conditions for a dropbox folder    Adding special access permissions to a dropbox folder    Editing dropbox categories and folders    Reordering dropbox categories and folders    Deleting dropbox categories and folders    Restoring deleted dropbox folders    Viewing the Dropbox event log    Associating dropbox folders with learning objectives    Previewing dropbox folders and submissions      Evaluating dropbox folder submissions    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
