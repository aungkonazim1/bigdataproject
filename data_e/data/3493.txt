New Cook Analytics &#038; Trading Lab to partner with Oxford Analytica &#8211; The University of Memphis President&#039;s Blog   


 

 
 
 
 


 
 
 
 

 



 New Cook Analytics   Trading Lab to partner with Oxford Analytica   The University of Memphis President s Blog 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 



 
 
  
 
 
 

 
 
 
 
			

         
 		
		



 
 
 
 

 

 


  Skip to content  




 

	
	 

		
		 

			
			 

				
						  The University of Memphis President s Blog  

					
			  

		  

		
		 

			 Primary Menu 
			 Menu 

			
			 
				 
					 
				 
					 Search for: 
					 
				 
				 
			 				 
			 

			 

			  
  Home    About the President  
  

		  

		 

		
		
			
				 

			
		
		 

		
	  

	
	 
	 

		
		 

			
			
	
	 

		
		
		 

			
			 New Cook Analytics   Trading Lab to partner with Oxford Analytica 
			
			 

				   Author   Jeanine Hornish Rakow     Published on    September 12, 2014  September 12, 2014     Leave a comment  
			  

			
		  

		
		 

			 Michael Cook, founder and CEO of SouthernSun Asset Management, has just completed negotiations with Oxford Analytica (OXAN) regarding an exclusive partnership with the Cook Analytics   Trading Lab (CA T) in the Fogelman College of Business and Economics (FCBE), to cooperate for an initial term of three years. Oxford Analytica ( www.oxan.com ) is a leading global analysis and advisory firm based in Oxford, England that draws on a worldwide network of experts to advise its clients on their strategy and performance. This new resource puts the University of Memphis among a select group of universities worldwide that have access to this unique, cutting-edge educational tool.  “Everyone at the table agrees that this partnership offers tremendous upside for all parties and most importantly our UofM students,” says Michael Cook.  Resources like Oxford Analytica help move us to the next level. 
 In this new partnership, OXAN will provide to the CA T Lab, in part, the following: 
 
 The Oxford Analytica Daily brief for up to 20 terminals in the CA T Lab; 
 Collaboration with FCBE faculty and staff to customize a series of workshops and lectures – conducted in the CA T Lab by OXAN staff throughout each academic year; 
 Access, on several occasions during the academic year, for Lab students to participate in high-level OXAN conference calls.  These calls usually focus on "hot" topics (e.g. Russian influence in Ukraine and potential economic and geopolitical impacts) and will normally have several high level special material experts from around the globe on the call; and 
 Make available three delegate positions to attend the annual Global Horizons Conference and Black Tie Dinner at Blenheim Palace – held in mid-September. Beginning in 2015, one student, one faculty or staff, and one leading local business executive shall be chosen at the end of the Spring term to attend the conference. 
 
 “Our hope is that we can raise the bar in an urban public university setting, offering our University of Memphis students a unique hands-on and unrivaled experience ” says Michael Cook.  “By so doing, we increase the probability of achieving our vision of producing high caliber, skilled, uniquely prepared graduates who can think independently, analytically, logically, and globally unlike any in the country. 
 Finally, it is important to thank my dear friend Dr. David Young, Founder and Chairman of Oxford Analytica for his original vision back in 1975, and then to recognize the management and staff of Oxford Analytica led by Mr. Graham Hutchings, the firm’s Managing Director, for their individual roles and collective effort in the evolution of this partnership up and to this point” 
 I shared earlier that Michael’s generous gift, establishing the Lab, provides a solid foundation for an innovative educational experience that will help prepare our students and provide them with the skills necessary for a highly competitive job market. This new arrangement with Oxford Analytica certainly takes that to the leading edge of technology and data availability. Michael’s commitment to U of M continues to bring a profound and enduring impact on the lives of our students and the broader Memphis community. Every Tiger owes him our deepest thanks. 
 M. David Rudd, President 

		  

		
		 

			  Published on    September 12, 2014  September 12, 2014      Author   Jeanine Hornish Rakow   
			
		  

		
	  

	
				
	 
		 Post navigation 
		    Previous article:  U of M the Largest Hub of Graduate Students in Our Region      Next article:  Year of Service    
	 
				

 

	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  


			
			
		  

		
	  


	
		
		
		 

		 Main Sidebar 

			
			  
				 
					 Search for: 
					 
				 
				 
			  		 		 Recent Posts 		 
					 
				 UofM Quality Indicator Score 
						 
					 
				 University of Memphis Research Foundation Announces Grand Opening of Student-Operated FedEx Call Center 
						 
					 
				 Pause to Grieve 
						 
					 
				 University of Memphis Magazine: Fall 2017 
						 
					 
				 Campus Update 
						 
				 
		 		  Recent Comments      Archives 		 
			  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  July 2014  
	  June 2014  
	  May 2014  
	  April 2014  
		 
		   Categories 		 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 
			
		  

		
		  

	
	
	 

		
		 Footer Content 

		 
		
			
				
				
								
			
		  
	
		 

			
			
			Using  Tiny Framework     
			
			   Log in  

		  
		
		 

			
			

 

	 Social Links Menu 

	      i class= fa fa-twitter   /i    
  
  

			
		  

		
	  

	
  


        

        









		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 
 