Strategic Plan 2007-2010 - Libraries - University of Memphis    










 
 
 
     



 
    
    
    Strategic Plan 2007-2010 - 
      	Libraries
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			University Libraries
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Services  
                          Resources  
                          Instruction  
                          Special Collections  
                          Technology  
                      
                      
                         
                            Administration   
                            
                               
                                  
                                   About Us  
                                         Staff  
                                         Hours and Location  
                                     
                                  
                                   Library Faculty and Staff Portal  
                                   Libraries Standing Committees  
                                   Libraries Organizational Chart  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Libraries
                              	  
                          
                              	Administration
                              	  
                         Strategic Plan 2007-2010 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Strategic Plan 2007- 2010 
                     
                       Mission Statement  
                     
                      The University Libraries is a leader in providing access to and managing information
                        services and resources to support teaching, learning, and research for the University
                        of Memphis community.
                      
                     
                       Statement of values  
                     
                      Providing the varied services, resources, and programs of the University Libraries
                        requires the involvement of many people. While each individual utilizes his or her
                        own particular expertise and background and adopts his or her unique way to accomplish
                        each task, the overall effort is guided by a common set of values that binds the whole
                        together with common purpose. As they go about meeting the Libraries’ established
                        objectives, University Libraries' faculty and staff are committed to the following
                        values:
                      
                     
                           Service  
                     
                      
                        
                         The University Libraries seeks excellence in providing assistance to all library users
                           while ensuring that access to information resources is provided in an open, receptive,
                           and courteous manner, with a commitment to freedom of information and equity of access.
                         
                        
                      
                     
                           Quality  
                     
                      
                        
                         The University Libraries strives to deliver effective user services, resources, and
                           programs, using the highest feasible standards of management, assessment, organization,
                           and delivery. We are simultaneously committed to the development of a knowledgeable,
                           versatile, and skilled faculty and staff.
                         
                        
                      
                     
                           Integrity  
                     
                      
                        
                         The University Libraries affirms the principles of academic freedom and provides all
                           services, programs, and operations with honesty, openness, and accountability.
                         
                        
                      
                     
                           Diversity  
                     
                      
                        
                         The University Libraries values and appreciates the differences, among our users,
                           our collections, and our faculty and staff. We endeavor to provide a climate of acceptance
                           and respect for all points of view and for all individuals, whether members of the
                           university community or the community-at-large, without regard to race, ethnicity,
                           gender, age, lifestyle choice, or physical abilities.
                         
                        
                      
                     
                           Collaboration  
                     
                      
                        
                         The University Libraries partners with the departments and colleges to provide resources
                           and services to support the academic programs of the University. We join with other
                           academic communities and libraries throughout Tennessee, the Mid-south region, and
                           the nation to enhance access to information resources beyond the University Campus.
                         
                        
                      
                     
                          Innovation  
                     
                      
                        
                         The University Libraries identifies, investigates, evaluates, and implements new and
                           emerging methods for obtaining appropriate resources and providing relevant services
                           to meet known and/or anticipated user needs.
                         
                        
                      
                     
                        
                     
                       GOALS  2007 - 2010  
                     
                      To accomplish the overall mission of the University Libraries, resources and energy
                        will be focused on achieving the following goals.
                      
                     
                      1. Build, preserve, and support collections that meet the needs of present and future
                        users.
                      
                     
                      2. Develop, explore, and implement new information technologies and resources. 
                     
                      3. Develop and implement strategies that maximize the effectiveness of the integrated
                        library system (ILS).
                      
                     
                      4. Develop, promote, and deliver instructional services and resources to meet changing
                        user needs.
                      
                     
                      5. Invest in library personnel to enhance their abilities to provide library services
                        and resources.
                      
                     
                      6. Collaborate with other units of the University, the urban community, the professional
                        community, the region, or the nation to improve access to information resources.
                      
                     
                      7. Develop and implement marketing and development plans that will enhance visibility
                        and image and increase the resource base of the University Libraries.
                      
                     
                        
                     
                       GOALS AND OBJECTIVES  
                     
                       I.  Build, preserve, and support collections that meet the needs of present and future
                           library users.  
                     
                       Objectives:       
                      
                     
                      1. Develop diverse collections that support the teaching, study, and research that
                        occur throughout the University including remote sites.
                      
                     
                      2. Create original information resources that add to the body of information resources
                        available for library users.
                      
                     
                      3. Seek additional funding for building and supporting library collections and programs
                        that meet the needs of the university community.
                      
                     
                      4. Develop a plan for an environmentally viable and secure storage facility to accommodate
                        lesser-used materials in support of our roles as a research library and the Regional
                        Depository for Federal Documents for the state of Tennessee. 
                      
                     
                      5. Update and maintain, in collaboration with the teaching and research faculty, comprehensive
                        collection development policies that will provide guidance for the ongoing growth
                        and development of all of the Libraries’ collections.
                      
                     
                      6. Work with staff in appropriate offices on campus (physical plant, space planning,
                        academic affairs, finance, etc.) to integrate the libraries’ space needs into campus
                        planning.
                      
                     
                      7. Evaluate electronic resources for purchase and retention in keeping with appropriate
                        Collection Development Policies, with special attention to archiving, accessing, and
                        purchasing collaboratively.
                      
                     
                      8. Collaborate with others to acquire and utilize newer technologies for the preservation
                        and reproduction of graphic and written archival materials, with emphasis on digital
                        preservation of materials.
                      
                     
                      9. Identify and implement appropriate collection assessment tools and techniques. 
                     
                        
                     
                       II. Develop, explore, and implement new information technologies and resources that contribute
                           to the success of the Libraries.  
                     
                       Objectives:  
                     
                      1. Improve and expand Web-based access to information resources for on-campus and
                        remote users.
                      
                     
                      2. Explore new opportunities for consortial purchases of electronic resources. 
                     
                      3. Implement a life cycle replacement plan for the servers and other system-wide backbone
                        components that support library programs and services.
                      
                     
                      4. Collaborate with ITD, the Provost’s Office, and other appropriate units to develop
                        and implement a plan that will support regular replacement and upgrading of the computers
                        and software needed by libraries’ staff.
                      
                     
                        
                     
                       III. Develop and implement strategies that maximize the effectiveness of the Libraries’
                           integrated library system (ILS).  
                     
                       Objectives:  
                     
                      1. Develop and adopt standards for creating and maintaining high quality data in the
                        integrated library system.
                      
                     
                      2. Improve the quality of the bibliographic and item records in the online catalog. 
                     
                      3. Add records to the libraries’ database for collections that are not currently represented
                        in the online catalog such as microform collections.
                      
                     
                      4. Develop and implement plans for maximizing the use of the integrated library system. 
                     
                      5. Provide leadership and oversight for the successful execution of contractual agreements
                        among the participants in the Memphis-III Partnership.   
                      
                     
                        
                     
                       IV. Develop, promote, and deliver instructional services and resources to meet changing
                           user needs.  
                     
                       Objectives:  
                     
                      1. Offer library instruction sessions and resources that meet the needs of library
                        users and enhance the information fluency of students and faculty of the University.
                      
                     
                      2. Take advantage of available technologies to improve user access to library instruction. 
                     
                      3. Develop and deliver instruction that helps classroom faculty to maximize the integration
                        of library resources into their courses.
                      
                     
                      4. Train selective depository personnel across the state in the use of electronic
                        access to government information in fulfillment of our responsibility as the Federal
                        Regional Depository for the state of Tennessee.
                      
                     
                      5. Offer library tours and focused visits to groups and individuals on the campus
                        and throughout the community.
                      
                     
                        
                     
                       V. Invest in library personnel to enhance their abilities to provide excellent customer
                           service along with appropriate library services and resources.  
                     
                       Objectives:  
                     
                      1. Update and maintain current position descriptions for all library personnel including
                        faculty, staff, and students.
                      
                     
                      2. Implement strategies to make sure that positions within the University Libraries
                        are properly classified and compensated.
                      
                     
                      3. Develop strategies for identifying and addressing organizational weaknesses. 
                     
                      4. Develop, conduct, sponsor, or coordinate training or staff development opportunities
                        for University Libraries’ personnel to assure essential competencies for all library
                        personnel in appropriate areas of responsibility.
                      
                     
                      5. Promote meaningful communication throughout the University Libraries in support
                        of an open, informative, and cooperative environment.
                      
                     
                      6. Make Libraries policies and procedures available to all library personnel in a
                        timely and consistence manner.
                      
                     
                      7. Ensure that Libraries’ faculty and staff have access to the supplies and equipment
                        needed to accomplish their jobs.
                      
                     
                        
                     
                       VI. Collaborate with other units of the University, the urban community, the professional
                           community, and the region to improve access to information resources.  
                     
                       Objectives:  
                     
                      1. Maintain active collaboration with the Faculty Senate’s Library Policy Committee
                        and other appropriate university organizations.
                      
                     
                      2. Participate fully in cooperative library programs and activities such as: the Memphis
                        Area Library Council (MALC); TENN-SHARE; West Tennessee Academic Library Collaborative
                        (WETALC); Tennessee Union List of Serials, Association of Southeastern Research Libraries
                        (ASERL); LYRASIS); and OCLC.
                      
                     
                      3. Serve as a source for federal government information to city, county, state, and
                        federal agencies and offices.
                      
                     
                      4. Collaborate with appropriate departments across the campus to create and support
                        joint programming that highlights the collections, services, and resources, of the
                        University Libraries.
                      
                     
                        
                     
                       VII. Develop and implement marketing and development plans that will enhance the visibility
                           and image and subsequently also increasing the resource base of the University Libraries.  
                     
                       Objectives:  
                     
                      1. Build on the relationship with the Friends of The University of Memphis Libraries
                        to enhance the continued growth and effectiveness of the collections, services, and
                        programs of the University Libraries.
                      
                     
                      2. Develop strategies that build on the University Libraries’ relationship with the
                        Office of Marketing, Advancement, and Government Relations to stimulate positive action
                        from that office on behalf of the University Libraries.
                      
                     
                      3. Develop and implement a comprehensive plan for communicating with constituents
                        both on and off campus.
                      
                     
                      4. Implement mechanisms that enable the University Libraries to respond to user needs
                        in a more timely and systematic manner.
                      
                     
                      5. Expand efforts to attract external funding for the programs and services of the
                        Libraries especially focusing on endowment funding.
                      
                     
                      6. Develop and implement a comprehensive plan for public programming that highlights
                        the resources of the University Libraries, attracts users into the library, and creates
                        opportunities to pursue external funding.
                      
                     
                      Revised - Oct 2009 
                     
                      2003-2006 Strategic Plan and Update: 
                     
                      
                        
                            05/25/05    
                        
                            07/28/05   ( PDF)
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Administration 
                         
                            
                               
                                About Us  
                                      Staff  
                                      Hours and Location  
                                  
                               
                                Library Faculty and Staff Portal  
                                Libraries Standing Committees  
                                Libraries Organizational Chart  
                            
                         
                      
                      
                      
                         
                            
                                Ask-a-Librarian  
                               Got a question? Got a problem? Ask Us! 
                            
                            
                                Interlibrary Loan  
                               Request books, articles, and other research materials from other libraries. 
                            
                            
                                Reserve space  
                               Group study and presentation spaces available in the library. 
                            
                            
                                My Library Account  
                               Review your library account for due dates and more. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


