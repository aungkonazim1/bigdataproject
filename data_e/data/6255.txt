Lee, J. S., Baskerville, R., &amp; Pries-Heje, J. (2015). The creativity passdown effect: Applying design theory in creating instance design. Information Technology &amp; People, 28(3), 529-543. - BIT - University of Memphis    










 
 
 
     



 
    
    
    Lee, J. S., Baskerville, R.,   Pries-Heje, J. (2015). The creativity passdown effect:
      Applying design theory in creating instance design. Information Technology   People,
      28(3), 529-543.  - 
      	BIT
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - Department of Business Information and Technology
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          BIT  
                          Programs  
                          Faculty  
                          Research  
                          Prospective Students  
                          Current Students  
                          FCBE  
                      
                      
                         
                            Research Focus   
                            
                               
                                  
                                   MIS Quarterly  
                                   Information Systems Research  
                                   Journal of AIS  
                                   Journal of MIS  
                                   European Journal of Information Systems  
                                   Information Systems Journal  
                                   MIS Quarterly Executive  
                                   Sloan Management Review  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	BIT
                              	  
                          
                              	Research
                              	  
                         Lee, J. S., Baskerville, R.,   Pries-Heje, J. (2015). The creativity passdown effect:
                           Applying design theory in creating instance design. Information Technology   People,
                           28(3), 529-543. 
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Lee, J. S., Baskerville, R.,   Pries-Heje, J. (2015). The creativity passdown effect:
                        Applying design theory in creating instance design. Information Technology   People,
                        28(3), 529-543.
                      
                     
                      Purpose 
                     
                      – The purpose of this paper is to suggest that translating a design theory (DT) into
                        practice (e.g. creating an instance design artifact (IDA)) is hardly straight-forward
                        and requires substantial creativity. Specifically the authors suggest that adopting
                        a DT embodies a creativity passdown effect in which the creative thinking of a team
                        of design theorist(s) inherent in DT invokes a creative mind of a team of artifact
                        instance designer(s) in creating an IDA. In this study, the authors empirically investigate
                        the creativity passdown effect through an action case in which a DT (DT nexus) was
                        applied in creating an IDA (multi-outsourcing decision-making tool).
                      
                     
                      Design/methodology/approach 
                     
                      – The case methodology applied here is described as an action case. An action case
                        is a hybrid research approach that combines action research and interpretive case
                        approaches. It combines intervention and interpretation in order to achieve both change
                        and understanding. It is a form of soft field experiment with less emphasis on iteration
                        and learning and more on trial and making. The approach is holistic in philosophy,
                        and prediction is not emphasized. The intervention in the case was that of an instance
                        designer team introducing a previously published DT as a basis for creating an IDA.
                      
                     
                      Findings 
                     
                      – The experience in the action case suggests that using a DT in creating an IDA may
                        encourage design thinking, and in certain way increase its power and practical relevance
                        by fostering the creative mind of instance designers. Indeed, DTs provide a scientific
                        basis for dealing with an instance problem, and this evokes the creativity mind of
                        instance designers. Without such a scientific basis, it is a lot more challenging
                        for instance artifact designers to deal with instance problems.
                      
                     
                      Research limitations/implications 
                     
                      – This study contributes to the literature concerning design science research, as
                        it challenges the notion that adopting scientific design knowledge limits creativity
                        inherent in creating IDA by illustrating creative elements involved in adopting DT
                        as a basis for creating IDAs.
                      
                     
                      Practical implications 
                     
                      – This study offers implications to practice, as it provides new insights regarding
                        how DT can be used in instance design activities.
                      
                     
                      Originality/value 
                     
                      – A report of this research previously appeared as a conference paper. However, the
                        attached journal version has been completely rewritten to additionally contribute
                        to the literature concerning design science research beyond the conference version.
                        More specifically, in this version, the authors conceptualize adopting a DT to build
                        an IDA as a theoretical basis, and the authors challenge the notion that adopting
                        scientific design knowledge limits creativity inherent in creating IDA by illustrating
                        creative elements involved in executing DT as a basis for creating IDAs.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Research Focus 
                         
                            
                               
                                MIS Quarterly  
                                Information Systems Research  
                                Journal of AIS  
                                Journal of MIS  
                                European Journal of Information Systems  
                                Information Systems Journal  
                                MIS Quarterly Executive  
                                Sloan Management Review  
                            
                         
                      
                      
                      
                         
                            
                                BIT News  
                               Publications, recognitions and spotlights about faculty and students of the BIT department. 
                            
                            
                                BIT Events  
                               Find out what's happening at the department and visit our weekly colloquium. 
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


