Library - LibGuides Mobile general information and policies  
 
	 
         
         
         
                     
                     
                     
                     
                    
                     
                              
         
         
          
        
		 
		 
		 
		                
        
        
        
        
        
         Library - LibGuides Mobile 
        
        
        
        	 
	 
		 
			This is the "Library" guide. 
			 Alternate Page for Screenreader Users  
			 Skip to Page Content  
		 
		 
                              LibGuides Mobile   
                         
                          
                 
                     
                     
                     
                     
                     
                
                     Library  general information and policies   
                      Last update:   Nov 29, 2017  
                      URL:   http://libguides.law.memphis.edu/content_mobile.php?pid=667976  
                    
                    
                     
                                         
                                             
                                              
                                            Hours
                                             
                                             
                                                 
                                                  
	 
		
		 
			
			   
			 Public Hours
			 
		 
			
		   
   7:30 am until 6:00 pm   
   Monday through Friday   
  
  Members of the public conducting legal research are welcome during these hours.  
  Students, faculty, and staff of the Cecil C. Humphreys School of Law have 24/7 access to the library.  
  The library is closed on university and major holidays and on inclement weather days when the university is closed.   
    
		 
				 Comments (0) 
		 
		  
		 
	   
                                                 
                                                 close 
                                             
                                         
                                     
                                     
                                         
                                             
                                              
                                            Contact Us
                                             
                                             
                                                 
                                                  
	 
		
		 
			
			   
			 Questions?
			 
		 
			
		  Library Service Desk 
  
 (901) 678-2426 
  lawcirc@memphis.edu    
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 Instructional Technology
			 
		 
			
		  Chris Patton 
  
 Local Support Provider 
 678-3245 
  cpatton2@!memphis.edu  
  
  
  
 Andrew Hughes 
  
 AudioVisual Services Coordinator 
 678-2823 
  tahughes@memphis.edu  
    
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 Library Staff
			 
		 
			
		  
 
 
 D.R. Jones 
 Associate Dean for Information Resourcesand Director of the Law Library 
 678-3244 
  drjones@memphis.edu  
 
 
 Deborah Harrison Barbee   
 Business Services Associate   
 678-5401    
  dbarbee@memphis.edu  
 
 
 Lucinda Valero    
 Assistant Director for Technical Services 
 678-2749 
  lvalero@memphis.edu  
 
 
 Rosalie Elia 
 Cataloging, Acquisitions and Government Documents Assistant 
 678-3242 
  relia@memphis.edu  
 
 
 Brandon Maas   
 Serials Manager 
 678-3027 
  bmaas@memphis.edu  
 
 
 Jan Stone 
 Assistant Director for Public Services 
 678-4937 
  jstone5@memphis.edu  
 
 
 Howard Bailey 
 Head of Access Services 
 678-5462 
  hbailey@memphis.edu  
 
 
 Steve Richardson 
 Information Services Librarian 
 678-2748 
  srchrds9@memphis.edu  
 
 
 April Parker  
 InterLibrary Loan Manager 
 678-1739 
  cparker3@memphis.edu  
 
 
 Stephanie Baker Rolen 
 Access Services Manager 
 678-2767 
  sabaker@memphis.edu  
 
 
 Meghan Cullen  
 Reserve Collection Manager 
 678-5392 
  mcullen@memphis.edu  
 
 
   
		 
				 Comments (0) 
		 
		  
		 
	   
                                                 
                                                 close 
                                             
                                         
                                     
                                     
                                         
                                             
                                              
                                            Check-out Privileges
                                             
                                             
                                                 
                                                  
	 
		
		 
			
			   
			 Circulation Privileges
			 
		 
			
		   Much of the Law Library collection is reference materials for in-library use only.   
  Resources which do circulate may be checked out as follows:  
  
 
 
 
    Patron   
   Loan period   
 
 
  Cecil C. Humphreys School of Law Students  
   2 weeks  
 
 
  Cecil C. Humphreys School of Law Full-Time Faculty  
  School year  
 
 
  Cecil C. Humphreys School of Law Adjunct Faculty and Staff  
   2 weeks  
 
 
  University of Memphis Faculty, Staff, Students with U of M ID  
  2 weeks  
 
 
  Memphis Metropolitan Area Lawyers with State Bar ID and Driver's License  
   2 weeks  
 
 
 
  
   Open Reserve  (Academic Success, Bar Exam Resources, and Professional Success) and  Course Reserve  resources have varying loan periods.  Please ask at the service desk.  
    
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 After-Hours Check-Out
			 
		 
			
		  Send the 14-digit barcode (type or take a picture) on the upper corner of the back cover of the book to  lawcirc@memphis.edu     
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 Renew Online
			 
		 
			
		      My Library Account    Username/Password = University of Memphis login (UUID)     
		 
				 Comments (0) 
		 
		  
		 
	   
                                                 
                                                 close 
                                             
                                         
                                     
                                     
                                         
                                             
                                              
                                            Floor Maps
                                             
                                             
                                                 
                                                  
	 
		
		 
			
			   
			 Three dimensions
			 
		 
			
		      Basement      First Floor      Second Floor      Third Floor      Fourth Floor       
		 
				 Comments (0) 
		 
		  
		 
	     
	 
		
		 
			
			   
			 Two dimensions
			 
		 
			
		      Basement      First Floor      Second Floor      Third Floor      Fourth Floor       
		 
				 Comments (0) 
		 
		  
		 
	   
                                                 
                                                 close 
                                             
                                         
                                     
                                             
             
                 Description       Loading content... please wait 
             
         
         
         
             
                         
                  More Information        Loading content... please wait  
                  Close window   
             
         
           
         
                          ^ Top     |     Home Page     |     Full Site  
                     
                    
                     
                         
                             Powered by  Springshare  and iWebKit. 
                             All rights reserved. 
                              Report a tech support issue.  
                             
                     
                    	 
 