WORKFORUM: University of Memphis Career Opportunities   
 
   
       
       WORKFORUM: University of Memphis Career Opportunities 
 
 

 
 
 
 
 
 
 
 
 

     

   
   
       
       
         
           Skip to Main Content 
         
         
           
             
   
     
       
         
           
         
       
       
         
          University of Memphis Career Opportunities  
       
     
   
 

           
           
             
   
     
       
         Toggle navigation 
          
          
          
       
     
     
       
    Home  
    Search Jobs  
      Create Account  
      Log In  
    Help  
 

       
             
               University Benefits 
             
             
               Equal Employment 
             
             
               Annual Security Report 
             
             
               Campus Map 
             
             
               About UofM 
             
       
     
   
 

             
               
                
                 
  Search Postings  (125) 
     
 
 View all open Postings below, or enter search criteria to narrow your search. 
    
   
   
     
       
         Keywords 
       
       
         
       
         
           Posted Within 
         
         
            Any time period 
 Last Day 
 Last Week 
 Last Month  
         
     
     
     
   
 

   
   
       
           
             Department 
           
           
             
                Academic Administration 
 Academic Affairs Finance 
 Academic Counseling Center 
 Academic Innovation   Support Services 
 Academic Technology Services 
 Accounting Office 
 Admissions 
 Adult and Commuter Student Services  
 Adult Services 
 Advanced Learning Center 
 Advancement Services 
 African   African American Studies 
 Air Force ROTC 
 Alumni and Constituent Relations 
 Alumni Programs and Special Events 
 Animal Care Facility 
 Anthropology 
 Architecture 
 Army ROTC 
 Art 
 Art Museum 
 Assoc Dean Education 
 Assoc VP Information Technology 
 Athletic Academic Services 
 Athletic Administration 
 Athletic Business Office 
 Athletic Committee 
 Athletic Communications 
 Athletic Director 
 Athletic External Affairs 
 Athletic Management Finance 
 Athletic Ticket Office 
 Auxiliary Services 
 AVP Admin Business Services 
 AVP Campus Planning Design 
 AVP Finance 
 AVP Physical Plant 
 AVP Student Affairs 
 Avron B. Fogelman Professional Development Center 
 Baseball 
 Basketball Men 
 Basketball Women 
 Benefits 
 Bioinformatics 
 Biology 
 Biomedical Engineering 
 Budget 
 Bureau Business Economic Research 
 Bursar 
 Business and Finance 
 Business Development 
 Business Information   Technology 
 Campus Recreation 
 Campus School 
 Career Services 
 Center for Academic Retention and Enrichment Services 
 Center for Community Health 
 Center for Counsel Learn Testing 
 Center for Research on Women (CROW) 
 Center Higher Education 
 Center Rehab Employment Research 
 Center Research Education Policy 
 CERI 
 Chemistry 
 Child Care Center 
 Chucalissa Museum 
 City   Regional Planning 
 Civil Engineering 
 COE Communicative Impaired 
 College of Arts and Sciences  
 College of Business Economics 
 College of Communication and Fine Arts 
 College of Education 
 College of Engineering 
 Communication 
 Communication Sciences and Disorders 
 Community Music School 
 Computer Science 
 Computer Systems Support 
 Conference and Event Services 
 Continuing Education 
 Counsel Education Psych Research 
 Criminal Justice 
 CRISTAL 
 Curriculum and Assessments 
 Dean of Students 
 Disability Resources for Students 
 Distance Education 
 Diversity Office 
 Earth Sciences 
 Economics 
 Educational Support Program 
 Electrical Computer Engineering 
 Engineering Technology 
 English 
 Enrollment Services 
 Enterprise Application Services 
 Enterprise Infrastructure Services 
 Environmental Health and Safety 
 Epidemiology   Biostatistics 
 Executive Assistant Student Affairs 
 Extended Programs 
 External Relations 
 Faculty Senate 
 Family and Consumer Sciences 
 FCBE Academic Administration 
 FCBE Academic Program 
 Fedex Institute of Technology 
 Feinstone Center 
 Finance Insurance Real Estate 
 Financial Aid Operations 
 Financial Planning 
 Finish Line Program 
 First Scholars 
 Food Services 
 Football 
 General Accounting 
 General Human Resources 
 General Payroll 
 General Plant 
 Golf Men 
 Golf Women 
 Government Affairs 
 Graduate Programs 
 Health Administration 
 Health Center 
 Health Sciences 
 Health Sport Sciences 
 History 
 Honors Program 
 Hooks Inst for Social Change 
 Hospitality and Resort Management 
 Hotel Conference Center 
 Human Resources 
 Innovation in Teaching   Learning 
 Institute for Intelligent Systems 
 Institutional Equity 
 Institutional Research 
 Instruction Curriculum Leadership 
 Integrated Microscopy Center 
 Intensive English for Internatls 
 Interdisciplinary Studies 
 Intermodal Freight Transportation Institute 
 Internal Audit 
 International Programs 
 International Studies 
 International Trade Center 
 Investment Banking Agreements 
 IT Planning and Budget 
 IT Security, ID Mgmt and Compliance 
 ITS Desktop and Smart Tech Support 
 ITS Service Desk 
 ITS Support Teaching and Learning 
 Journalism 
 Judaic Studies 
 Judicial Affairs 
 Keep TN Beautiful 
 Lambuth 
 Law Library 
 Leadership 
 Lipman School 
 Loewenberg College of Nursing 
 Management 
 Management Information Systems 
 Marketing and Communications 
 Marketing Supply Management 
 Mathematics 
 Mechanical Engineering 
 Multicultural Affairs 
 Navy ROTC 
 Network Services 
 Office for Institutional Equity 
 Office of President 
 Office of Student Conduct 
 Office of University Development 
 Olympic Sports 
 One Stop Shop 
 Parking 
 Payroll Office 
 Philosophy 
 Physical Plant 
 Physics and Materials Science 
 Police Services 
 Political Science 
 PP Business Operations 
 PP Crafts Trades 
 PP Custodial 
 PP Landscape 
 Principal Gifts 
 Procurement and Contract Services 
 Provost Office 
 Psychology 
 Public Administration 
 Regional Economic Development Ctr 
 Registrar Office 
 Residence Life 
 Revenue and Waivers 
 Rifle 
 RODP Admin 
 Scholarships 
 School of Accountancy 
 School of Health Studies 
 School of Law 
 School of Music 
 School of Public Health 
 School of Urban Affairs 
 Shared Services Center 
 Soccer Men 
 Soccer Women 
 Social   Behavioral Sciences 
 Social Work 
 Sociology 
 Softball 
 Space Planning 
 Sport   Leisure Management 
 Staff Senate 
 Stores Inventory 
 Student Financial Aid 
 Student Leadership 
 Student Success Programs 
 Teacher Education 
 Tennis Men 
 Tennis Women 
 Testing Center 
 Theatre and Dance  
 Tiger Copy and Graphics 
 Track Men 
 Track Women 
 Undergraduate Programs 
 University Admin Applications Svcs 
 University Center 
 University College 
 University College Programs 
 University College RODP 
 University Counsel 
 University District Support 
 University Libraries 
 University of Memphis Foundation 
 Various 
 Vice President Research 
 Vice Provost Assessment, IR, Report 
 Vice Provost Extended Programs 
 Volleyball 
 VP Advancement 
 VP Business and Finance 
 VP Communications, PR, Marketing 
 VP Information Technology 
 VP Research 
 VP Student Affairs 
 Weight Room Operations 
 Women and Gender Studies 
 Women Athletics and Compliance 
 World Languages   Literatures  
             
           
           
             Category 
           
           
             
                Staff (Hourly/Monthly) 
 Full-Time Faculty 
 Part-Time Faculty 
 Administrative Executive 
 Academic Executive 
 Athletic Contract 
 Temporary Employees  
             
           
       
       
           
             Campus Location 
           
           
             
                Main Campus (Memphis, TN) 
 Chucalissa Museum   Indian Village (Memphis, TN) 
 Collierville Campus (Collierville, TN) 
 Lambuth Campus (Jackson, TN) 
 Meeman Biological Field Station (Millington, TN) 
 Millington Center (Millington, TN) 
 Park Avenue Campus (Memphis, TN) 
 School of Law (Downtown Memphis) 
 May vary by position  
             
           
       
   
 

   
   
   Previous    1   2   3   4   5    Next   
 View Results  (125)  
  
 
   
       
   
     
       Closing Date 
     
     
       Category ↓
     
     
       Department 
     
     
       Hiring Range 
     
 


 
       
   
     
       
         
             Part-Time Faculty Pool, Counseling, Educational Psychology,   Research 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Counsel Education Psych Research
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Art 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Art
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Foreign Languages (German) 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            World Languages   Literatures
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Accountancy 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            School of Accountancy
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Philosophy 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Philosophy
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Honors Program 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Honors Program
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Mathematics 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Mathematics
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, School of Public Health 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            School of Public Health
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Foreign Languages (Chinese) 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            World Languages   Literatures
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Electrical Computer Engineering 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Electrical Computer Engineering
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Civil Engineering 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Civil Engineering
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Foreign Languages (Spanish) 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            World Languages   Literatures
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Leadership 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Leadership
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Chemistry 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Chemistry
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Foreign Languages (Korean) 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            World Languages   Literatures
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, English 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            English
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Journalism   Strategic Media 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Journalism
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Anthropology 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Anthropology
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Academic Strategies (ACAD) 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Academic Innovation   Support Services
         
         
            From $721.00 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Mechanical Engineering 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Mechanical Engineering
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Nursing 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Loewenberg College of Nursing
         
         
            From $1,200 to $1,500 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Foreign Languages (French) 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            World Languages   Literatures
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Sport   Leisure Management 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Sport   Leisure Management
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Intensive English for Internationals 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Intensive English for Internatls
         
         
            From $24 to $30 per teaching hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Marketing   Supply Chain Management 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Marketing Supply Management
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, City and Regional Planning 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            City   Regional Planning
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Foreign Languages (Russian) 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            World Languages   Literatures
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Biomedical Engineering 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Biomedical Engineering
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Foreign Languages (Italian) 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            World Languages   Literatures
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   
     
   
     
       
         
             Part-Time Faculty Pool, Architecture 
         
       
         
            01/31/2018
         
         
            Part-Time Faculty
         
         
            Architecture
         
         
            From $700 per credit hour
         
     

     
       
             
               
                
               
               
                   View Details 
                     Bookmark 
               
             
         
       
     
   

 

   Previous    1   2   3   4   5    Next   

                  
               
                
             
           
           
             
   
     University of Memphis — Human Resources 
165 Administration Building, Memphis, TN 38152 
901.678.3573  workforce@memphis.edu  
 Follow us on Twitter  
      
      ©University of Memphis. All Rights Reserved. The University of Memphis is an Equal Opportunity/Affirmative Action university.  
   
 

           
         
       
         
 
        
  

  


    
      
    
     
   
 
