Earthworm Modules: <P> fir overview  
 
 
 Earthworm Modules:   fir  overview 
 
 
  Earthworm Module:   Fir Overview 
 (last revised 12 December, 2008)  
 
The fir module implements a general-purpose FIR filter routine to
process wave data (TYPE_TRACEBUF2) coming from the InRing defined in
decimate.d. Decimated trace messages are written to the OutRing with their
SCNs changed as specified in fir.d. The filtered trace message retains
its TYPE_TRACEBUF2 type, but it (normally) has a new SCNL name.
This module can be configured as a low-pass, high-pass, multi-band pass or
notch filter. By using multiple Band commands in the  configuration file 
any of these filter types can be specified.
 
This FIR filter causes considerable time delay in the data.
Fortunately filter has linear phase so the delay does not change
the shape of the wave. This delay is removed by adjusting the timestamp of the
output data, resulting in zero phase delay. Note that there will be small
precursory artifacts before impulsive wave arrivals. See "Of Poles and Zeros"
by Frank Scherbaum for more details.
The FIR filter coefficients are determined using the
Remez Exchange algorithm. This produces an equi-ripple filter. The
coefficients and zeroes of the filter, as well as the effective delay, are
logged on startup. (FIR filters have no poles.)
 
 
 Module Index  |
 Fir Configuration File 

 

 
 
 
Contact:    Questions? Issues?  Subscribe to the Earthworm Google Groups List.     
 
 
 
