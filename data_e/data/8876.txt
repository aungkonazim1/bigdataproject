Adding users to a group in Wiggio | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Adding users to a group in Wiggio | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   English  
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Wiggio    »    Creating and using groups in Wiggio   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Adding users to a group in Wiggio                       
        
        
       
        
     
               
		Click on the group you want to add members to.
	 
	 
		Click the  Add members  link in the Members panel.
	 
	 
		Do one of the following:
		  
				In the Email tab, enter the email addresses of anyone you want to add to the group. You can also select recipients from your email address book(s) by clicking the  email contacts  link or import recipients from a spreadsheet by clicking the  spreadsheet  link.
			 
			 
				In the Link tab, copy the group link provided. Share the link with anyone you want to add to the group.
				 
					 Note  To disable this link at any time, change the password for your group.
				 
			 
		  
      Audience:     Learner       
    
         
               ‹ Creating a subgroup in Wiggio  
                     up 
                     Finding your group email address in Wiggio › 
           
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Wiggio  
  
      Wiggio basics    Adding resources to Wiggio    Using polls in Wiggio    Using to-do lists in Wiggio    Creating and using folders in Wiggio    Creating and using groups in Wiggio    Creating a group in Wiggio    Creating a subgroup in Wiggio     Adding users to a group in Wiggio    Finding your group email address in Wiggio    Sharing a group in Wiggio    Joining a group in Wiggio    Leaving a group in Wiggio    Adding a subgroup to a conversation in Wiggio      Managing groups in Wiggio     Managing events in Wiggio    Importing and exporting calendars in Wiggio    
                  
           
         

       
     

    
    
    
   
 
   
 
