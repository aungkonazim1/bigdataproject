Contact Us - FCBE - University of Memphis  This page is designed to help you find answers to the most frequently asked questions about Fogelman College of Business &amp; Economics.  










 
 
 
     



 
    
    
    Contact Us  - 
      	FCBE
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College of Business   Economics
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          Students  
                          Departments  
                          Faculty   Staff  
                          Research  
                          News  
                      
                      
                         
                            About FCBE   
                            
                               
                                  
                                   Dean's Message  
                                   Strategic Plan  
                                   Facts   Figures  
                                   General Information  
                                         A to Z List  
                                         Driving Directions  
                                         FAQs  
                                     
                                  
                                   Job Announcements  
                                   Social Media Policy  
                                         Faculty and Staff  
                                         Student  
                                     
                                  
                                   Contact Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	FCBE
                              	  
                          
                              	About FCBE
                              	  
                         Contact Us  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Contact The FCBE 
                     
                      This page is designed to help you find answers to the most frequently asked questions
                        about Fogelman College of Business   Economics. Please make use of the links below,
                        they are your fastest pathways to a helpful answer.
                      
                     
                      Legend 
                     
                      
                        
                          Contacting Fogelman College by Mail or Phone  
                        
                          Contacting Fogelman College by E-mail  
                        
                          Applying to Fogelman College of Business   Economics  
                        
                          Course Information/General Catalog  
                        
                          Visiting The University of Memphis  
                        
                      
                     
                       Need to Contact Fogelman College by Mail or Phone?    Top of Page   
                     
                      For general inquiries, write to: 
                     
                      Individual's Name (optional) Fogelman College of Business   Economics, The University of Memphis, Memphis, TN 38152
                      
                     
                      For general phone inquiries, call: 
                     
                      901.678.2432 
                      
                     
                       Need to Contact Fogelman College by E-mail?    Top of Page   
                     
                      If your questions do not pertain directly to the main college web page, please direct
                        them to the relevant department on campus. You can find e-mail contact information
                        by visiting a department or unit's web site. To find a department or unit specific
                        web site, use our  Departments Web Page  or our search engines.
                      
                      
                     
                       Interested in Applying to Fogelman College of Business   Economics?    Top of Page   
                     
                      If you want to apply to Fogelman College Of Business   Economics, or if you have questions
                        about admissions requirements, fees, financial aid, or the transfer process, please
                        visit our  Applying to The University of Memphis  page, or contact the following resources directly:
                      
                     
                      
                        
                           Undergraduate Student Services Office  :  
                             Email:  fcbeadvising@memphis.edu  Phone: 901.678.2855 Fax: 901.678.4344  114 Fogelman College of Business   Economics The University of Memphis Memphis, TN 38152
                         
                        
                           Graduate Programs Office  : Fax: 901.678.4705   Dr. Balaji Krishnan, Director  Email:   krishnan@memphis.edu  Phone:  901.678.2786
                         
                        
                         
                        
                          Marja Martin, Professional and Online MBA Programs  Email:   mnmartin@memphis.edu  Phone:  901.678.3405    
                        
                          Ashley Holloway, Graduate Programs Academic Advisor, IMBA  Email:    anhollwy@memphis.edu   Phone:  901.678.3656
                         
                        
                          Anna Myers, CD-MBA  Email:    ammyers@memphis.edu   Phone:  901.678.3442
                         
                        
                         The University of Memphis Fogelman College of Business   Economics 101 Fogelman College Admin. Bldg. Memphis, TN 38152-3120
                         
                        
                           Executive MBA Programs Office   : Email:  emba@memphis.edu  Phone: 901.678.4866    Dynisha Brown-Woods, Executive MBA Programs  Email:  dabrown1@memphis.edu  Phone: 901.678.5280 Fax: 901.678.5290
                         
                        
                         357 Fogelman College of Business   Economics The University of Memphis Memphis, TN 38152
                         
                        
                           Doctoral Program Office   :  Ashley Holloway, Graduate Programs Academic Advisor  Email:  fcbephd@memphis.edu  Phone: 901.678.3656 Fax: 901.678.3759
                         
                        
                         101E Fogelman College of Business   Economics The University of Memphis Memphis, TN 38152
                         
                        
                      
                     You can also find  resources for international students  online.   
                     
                       Need Course Information or a General Catalog?   Top of Page   
                     
                      The General Catalog lists courses and curricula for all academic departments, units,
                        groups, and interdisciplinary and special studies programs offering undergraduate
                        and graduate degrees. It also includes extensive information on applications and admissions,
                        degree requirements, financial aid, and student services. You can explore the online
                        catalog
                      
                      
                     
                       Planning to Visit The University Of Memphis?   Top of Page   
                     
                      The  Visitor Services  web site contains information about tours, traveling to campus, and parking near
                        campus. A  campus map  and  campus tours  are also available.
                      
                     
                      
                        
                          Street Address:  3675 Central Avenue Memphis, TN 38152-3120
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         About FCBE 
                         
                            
                               
                                Dean's Message  
                                Strategic Plan  
                                Facts   Figures  
                                General Information  
                                      A to Z List  
                                      Driving Directions  
                                      FAQs  
                                  
                               
                                Job Announcements  
                                Social Media Policy  
                                      Faculty and Staff  
                                      Student  
                                  
                               
                                Contact Us  
                            
                         
                      
                      
                      
                         
                            
                                Apply to our Undergraduate Programs    
                               Enroll at the university and engage yourself with our six different departments, student
                                 enrichment, and more. 
                               
                            
                            
                                Apply to our Masters Programs    
                               Earn your degree with one of our five MBA tracks or with one of our four specialized
                                 Masters programs!
                               
                            
                            
                                Apply to our Doctoral Programs    
                               Interested in a Ph.D.? Start the journey towards your Ph.D. here! 
                            
                            
                                Contact Us    
                               Have questions? Call or email today!  
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/10/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


