Learning Disabilities and Attention Deficit/Hyperactivity Disorder - Disability Resources for Students - University of Memphis    










 
 
 
     



 
    
    
    Learning Disabilities and Attention Deficit/Hyperactivity Disorder - 
      	Disability Resources for Students
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Disability Resources for Students
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Access  
                          Accommodations  
                          Technology  
                          Policies  
                          Career Programs  
                          Faculty  
                      
                      
                         
                            Disability Accommodations and Services   
                            
                               
                                  
                                   How to Obtain Accommodations and Services  
                                        
                                          
                                           
                                             
                                               Getting Started  
                                             
                                               Each Semester  
                                             
                                               Notetaking Policy  
                                             
                                               Alternate Format Textbooks Policy and Procedures  
                                             
                                               Alternate Format Textbooks Contract  
                                             
                                               Alternative Format Request Form  
                                             
                                           
                                          
                                        
                                     
                                  
                                   Services for Specific Disabilities  
                                        
                                         Overview  
                                        
                                         Blindness and Visual Impairments  
                                        
                                         Chronic Health Impairments  
                                        
                                         Communication Disorders  
                                        
                                         Learning Disabilities and ADHD  
                                        
                                         Mobility Impairments  
                                        
                                         Psychiatric Disorders  
                                        
                                         Traumatic Brain Injury  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Disability Resources for Students
                              	  
                          
                              	Disability Accommodations and Services
                              	  
                         Learning Disabilities and Attention Deficit/Hyperactivity Disorder 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Learning Disabilities and Attention Deficit/Hyperactivity Disorder 
                     
                      To obtain disability services and accommodations, students must register with DRS
                        by providing current and appropriate medical or professional documentation and by
                        meeting with an DRS coordinator for an intake appointment. Based on the individual
                        student's documentation, the coordinator will assess the student's functional limitations
                        and academic needs, and from those, will determine the necessary services and accommodations
                        for which the student is eligible. After registering for classes each semester, students
                        are responsible for scheduling an appointment with their coordinator to arrange reasonable
                        accommodations and services for that semester.
                      
                     
                      In addition to the general services available to all students registered with DRS,
                        the following services may be available to students who have learning disabilities
                        or attention deficit/hyperactivity disorder:
                      
                     
                      
                        
                         Special advising and strategic scheduling of classes 
                        
                         Preferential classroom seating 
                        
                         Permission to audio record lectures 
                        
                         Kurzweil reading machines 
                        
                         Books on CD/DVD or E-text 
                        
                         Access to Dragon Naturally Speaking voice recognition software 
                        
                         Access to large screen monitors and screen enlargement 
                        
                         Access to Inspiration-software that organizes or outlines by visual mapping to prepare
                           to write papers or essays
                         
                        
                         Loaner equipment
                           
                            
                              
                               CD/DVD Players for use with Learning Ally Audio books 
                              
                               Digital recorders 
                              
                            
                           
                         
                        
                         Notetaker service 
                        
                         Assistance with developing study strategies for specific courses 
                        
                         Assistance with organization and time management skills 
                        
                         Weekly meetings with professional staff to monitor progress 
                        
                         Coaching to deal with AD/HD symptoms 
                        
                         Tutor referral 
                        
                         Test Accommodations
                           
                            
                              
                               Extended time 
                              
                               Low stimulus environment 
                              
                               Computer 
                              
                               Spell checker 
                              
                               Calculator 
                              
                               Screen reader 
                              
                               Alternative to scantron answer sheet 
                              
                            
                           
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Disability Accommodations and Services 
                         
                            
                               
                                How to Obtain Accommodations and Services  
                                     
                                       
                                        
                                          
                                            Getting Started  
                                          
                                            Each Semester  
                                          
                                            Notetaking Policy  
                                          
                                            Alternate Format Textbooks Policy and Procedures  
                                          
                                            Alternate Format Textbooks Contract  
                                          
                                            Alternative Format Request Form  
                                          
                                        
                                       
                                     
                                  
                               
                                Services for Specific Disabilities  
                                     
                                      Overview  
                                     
                                      Blindness and Visual Impairments  
                                     
                                      Chronic Health Impairments  
                                     
                                      Communication Disorders  
                                     
                                      Learning Disabilities and ADHD  
                                     
                                      Mobility Impairments  
                                     
                                      Psychiatric Disorders  
                                     
                                      Traumatic Brain Injury  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                DRS Announcements  
                               Learn what's new at DRS and get important reminders too. 
                            
                            
                                Thanks to Our Volunteer Note-takers!  
                               See how DRS notetakers help our students. 
                            
                            
                                Student and Faculty Awards  
                               Read about the DRS Student and Faculty Awards. 
                            
                            
                                Contact DRS  
                               Information about our office hours and how to reach us. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/21/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


