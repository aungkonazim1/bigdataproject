Importing a vCard | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Importing a vCard | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Metadata    »    Metadata basics   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Importing a vCard                       
        
        
       
        
     
              
	A vCard is a virtual business card. The vCard has become a standard format for storing information about individuals or corporations and can contain a person’s name, organization, and miscellaneous identifying information. Some fields in some views accept vCard data.
 

 
	 Note  Importing vCard data into a field clears any existing data in the field.
 

 
	Import a vCard
 

  
		On the Edit Metadata page for the module or topic you want to modify, click the name of a vCard field, then click  Import vCard .
	 
	 
		Select whether the file is from your course or stored on your computer. If the file is from your course, click  Select a File  to locate the file. If your file is stored on your computer, click  Browse  to locate the file.
	 
	 
		Click  Import .
	 
      Audience:     Instructor       

    
           

                   ‹ Troubleshooting metadata conflicts 
        
                   up 
        
                   Adding and removing metadata › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Metadata  
  
      Metadata basics    Accessing Metadata    Metadata interfaces    Setting metadata interface preferences    Changing your metadata interface    Troubleshooting metadata conflicts    Importing a vCard      Adding and removing metadata    
                  
           
         

       
     

    
    
    
   
 
   
 
