Setting final grade display options for users | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Setting final grade display options for users | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Grades    »    Managing final grades   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Setting final grade display options for users                       
        
        
       
          
     
           Printer-friendly version        On the Manage Grades page, click on the name of the Final Calculated or Final Adjusted grade. 
 If you want to show the class average for the final grade in users’ view of the grade book, select  Display class average to users . 
 If you want to display a graph showing how grades were distributed between different percentiles in users’ view of the grade book, select  Display grade distribution to users . 
   Note  These options are only available for the final grade you release to users. This might be either the final calculated or final adjusted grade depending on the setup options you selected for your course.      Audience:    Instructor      

    
           

                   ‹ Releasing a final grade 
        
                   up 
        
                   Setting display options for your view of the grade book › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Grades  
  
      Finding my grades    Creating a grade book    Creating grade items and grade book categories    Managing grade items and grade book categories    Managing grade schemes    Managing users  grades    Managing final grades    Accessing the Final Grades page    Calculating final grades    Additional options on the Final Grades page    Editing the calculated or adjusted final grade    Excluding grade items from the final grade    Releasing a final grade    Setting final grade display options for users    Setting display options for your view of the grade book    Setting release conditions for the final grade    Viewing grade statistics      Changing Grades settings and display options    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
