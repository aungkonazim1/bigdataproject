Resources - CAS - University of Memphis    










 
 
 
     



 
    
    
    Resources - 
      	CAS
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			College of Arts   Sciences
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Areas of Study  
                          Research  
                          Scholarships  
                          Students  
                          Resources  
                          News  
                      
                      
                         
                            Resources   
                            
                               
                                  
                                   Academic Department Chairs, Program Directors  
                                   ACCOLADES Archive  
                                   Awards  
                                   Chair Leadership/Development  
                                   Council: Research and Graduate Studies  
                                   Council: Undergraduate Curriculum  
                                   Dean's Office Calendar  
                                   Departments  
                                   Evaluations  
                                   Faculty Recruitment  
                                   Information and Policies  
                                   Institutes and Centers  
                                   Research Support Services  
                                   President's Blog  
                                   Research, Grants, Funding  
                                   Tenure and Promotion  
                                   UMdrive  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	CAS
                              	  
                         
                           	Resources
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Faculty Resources 
                     
                          Awards   Accolades
                      
                     
                      The following award recipients information can be found on the College of Arts   Sciences
                        Accolades  website .
                      
                     
                      
                        
                          Thomas W. Briggs Excellence in Teaching Awards  
                        
                          Alumni Association Awards for Distinguished Research and Creative Achievement (formerly:
                              Distinguished Research Award)  
                        
                          Arts and Sciences - Chairs of Excellence  
                        
                          Dean's Award for Advising Excellence  
                        
                          Distinguished Research Award (CASDRA)  
                        
                          Distinguished Teaching Award  
                        
                          Dunavant University Professorships - Arts and Sciences  
                        
                          Early Career Research Award  
                        
                          Eminent Faculty Award  
                        
                          Excellence in Engaged Scholarship  
                        
                          Graduate Assistant Meritorious Teaching Award  
                        
                          Learn and Serve Faculty Award  
                        
                          Meritorious Faculty Award  
                        
                          W. Russell Smith Teaching Award  
                        
                          Atkins University Professorships  
                        
                          Faudree University Professorships  
                        
                          First Tennessee University Professorships  
                        
                      
                     
                          Chair Leadership Development
                      
                     
                      
                        
                          CAS Chair Handbook  
                        
                          Surviving Evaluation Season (powerpoint by Dr. Joy Clay)  
                        
                          Workshop #1 Faculty Evaluations  
                        
                          Workshop #2 High Performance Staff Management  
                        
                          Workshop #3 Creating a departmental culture that supports research among faculty and
                              students  
                        
                      
                     
                         Councils
                      
                     
                      
                        
                          Research and Graduate Studies  
                        
                          Undergraduate Curriculum  
                        
                      
                     
                          Evaluations
                      
                     
                        NOTE: The Evaluation Server for archived evaluations is temporarily unavailable. If
                              you need a copy of an evaluation, please contact Debra Turner at  dmturner@memphis.edu  or 678.5449   
                     
                      
                        
                         Calendar Year 2011-Present Archive Chairs/Directors Evaluation of Faculty (Contact
                           Debra Turner  dmturner@memphis.edu )
                         
                        
                          Chairs/Directors Evaluation of Faculty  
                        
                          Chairs/Directors Evaluation of Dean  
                        
                         Head of SUAPP Evaluation of Chairs/Directors 
                        
                          Faculty Evaluation of Chair/Director Guidelines  
                        
                          Faculty Planning Report Form  - Word document
                         
                        
                          Faculty Planning Report Form  - Excel version
                         
                        
                          Faculty CV Application  
                        
                      
                     
                        **If you need a copy of an evaluation, please contact Debra Turner at  dmturner@memphis.edu  or 678.5449   
                     
                          Information and Policies
                      
                     
                      
                        
                          Faculty Handbook  (Academic Affairs)
                         
                        
                          Faculty Teaching Load Guidelines  
                        
                          Guidelines on Shared Faculty Appointments  
                        
                          Part-time instruction forms and directions  
                        
                          Part-time Instructor Information and Guide  
                        
                          University of Memphis Policies and Procedures  
                        
                      
                     
                          Faculty Recruitment
                      
                     
                      
                        
                          Affirmative Action - Office of the President  
                        
                          Department Chair and Program Director Searches  
                        
                          Step by Step Instructions for Faculty Hires in workForum  
                        
                          Faculty Search Committee Code of Conduct  
                        
                          Codes for Rejected Applicants  
                        
                      
                     
                          Research, Grants and Funding
                      
                     
                      
                        
                          CAS Research Grants Instructions 
                           
                            
                              
                                CAS Research Grants Electronic Form  
                              
                            
                           
                         
                        
                          Council for Graduate Studies and Research By-Laws  
                        
                          Travel Enrichment Funds Application (Formerly Donovan Travel Enrichment Funds)  
                        
                          Professional Development Assignment  
                        
                      
                     
                          Tenure and Promotion
                      
                     
                      
                        
                          Mid-Tenure Dossier Instructions  
                        
                          Tenure and Promotion Committee Chairs Information  
                        
                          Tenure and Promotion Dossier Submission  
                        
                          Tenure and Promotion Calendar   (updated Aug 2017)  
                        
                          Current College Committee Members  
                        
                          Tenure and Promotion Guidelines  
                        
                          Tenure and Promotion Guides and Forms  (provost's website)
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Resources 
                         
                            
                               
                                Academic Department Chairs, Program Directors  
                                ACCOLADES Archive  
                                Awards  
                                Chair Leadership/Development  
                                Council: Research and Graduate Studies  
                                Council: Undergraduate Curriculum  
                                Dean's Office Calendar  
                                Departments  
                                Evaluations  
                                Faculty Recruitment  
                                Information and Policies  
                                Institutes and Centers  
                                Research Support Services  
                                President's Blog  
                                Research, Grants, Funding  
                                Tenure and Promotion  
                                UMdrive  
                            
                         
                      
                      
                      
                         
                            
                                Apply now for an Undergraduate Degree  
                               Pursue your dream, broaden your career choices and gain the confidence you need to
                                 succeed
                               
                            
                            
                                Offering Master's and Doctoral Degrees  
                               Enhance your knowledge, skills and experience 
                            
                            
                                Community Engagement  
                               Teaching and serving the Memphis and Mid-South community through programs and events 
                            
                            
                                Contact Us  
                               Dean's Office Contacts, Advising, Department Chairs... 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/27/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


