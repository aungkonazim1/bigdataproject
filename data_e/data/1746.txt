 Admissions - Theatre &amp; Dance - University of Memphis    










 
 
 
     



 
    
    
     Admissions - 
      	Theatre   Dance
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Theatre   Dance
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Academic Programs  
                          Admissions  
                          People  
                          Production Archives  
                          Purchase Tickets  
                            
                      
                      
                         
                            Admissions   
                            
                               
                                  
                                   Application and Audition Process  
                                         BFA Performance   
                                         BFA Musical Theatre  
                                         BFA Design and Technical Production  
                                         BFA Dance  
                                         All MFA Graduate Studies  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Theatre   Dance
                              	  
                         
                           	admissions
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                        
                     
                      Application and Audition Process 
                     
                      Admission to the BFA in theatre at the University of Memphis is a two step process.
                        Students should apply first to the University of Memphis and then separately to the
                        Department of Theatre   Dance. Current Memphis students wishing to transfer into,
                        or add the BFA program need only complete the departmental application. Current BFA
                        students wishing to change their program/concentration need only complete the departmental
                        application. Acceptance to any of our programs is contingent upon successful acceptance
                        to the University of Memphis. When applying to the department students should choose
                        between four areas of study:
                      
                     
                         All Prospective Students Please Follow this  Link        
                     
                       Performance  (acting)  Design and Technical Production   Musical Theatre     Dance    *Graduate admission information can be found  Here  
                     
                      All programs require an entrance audition and/or interview. Students are automatically
                        considered for all scholarship opportunities as part of their application and audition/interview
                        provided they complete the in-person or recorded audition/interview by the last scholarship
                        date (March 17, 2018 for students beginning Fall 2018).
                      
                     
                      The 2017 on-campus audition/interview dates for  late admission to the 2017 Fall semester  are:  Monday, August 18, 2017 (no scholarship consideration)
                      
                     
                      The 2017 on-campus audition/interview dates for  admission to the 2018 Spring semester  are:
                      
                     
                      Monday, November 13, 2017 (no scholarship consideration) 
                     
                      The 2018 on-campus audition/interview dates for  admission and full scholarship consideration for the 2018 Fall semester  are:
                      
                     
                      Saturday, February 17, 2018 Saturday, March 17, 2018
                      
                     
                      The 2018 on-campus audition/interview dates for  entrance consideration ONLY for the 2018 Fall semester ,  no scholarship consideration are:
                      
                     
                      Monday, April 16, 2018 – Not available for Musical Theatre applicants 
                     
                      2017-2018 Regional auditions/interviews for the 2018 Fall semester may be completed
                        at the following festivals:
                      
                     
                      North Texas Auditions - Greater Dallas Area – Thursday and Friday, November 16 and
                        17, 2017 Greater Houston Area Auditions - High School for the Performing and Visual Arts (HSPVA)
                        – Sunday, November 19, 2017 Greater San Antonio Area Auditions - NEISD High School - Monday, November 20, 2017 South Eastern Theatre Conference (SETC) – March 7-11, 2018 - Mobile, AL Tennessee Thespian Festival – MTSU - January 12-13, 2018
                      
                     
                      Online recorded auditions are accepted for students living more the 150 miles from
                        Memphis. Recorded auditions must be received by March 17, 2018 to be considered for
                        scholarships.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Admissions 
                         
                            
                               
                                Application and Audition Process  
                                      BFA Performance   
                                      BFA Musical Theatre  
                                      BFA Design and Technical Production  
                                      BFA Dance  
                                      All MFA Graduate Studies  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                2017-2018 Season  
                               Join us for our 2017-2018 Theatre   Dance Season 
                            
                            
                                Prospective Students  
                               Thinking of majoring in Theatre   Dance? Click here to receive more information about
                                 our academic programs.
                               
                            
                            
                                Watch Me – Theatre Video  
                               Take a closer look at the UofM Department of Theatre   Dance 
                            
                            
                                Dance at the University of Memphis  
                               Check out this short video focusing on our dance program. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


