Summer 2017 Final Exams - Registrar - University of Memphis    










 
 
 
     



 
    
    
    Summer 2017 Final Exams - 
      	Registrar
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Registrar
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Register  
                          Students  
                          Veterans  
                          Faculty   Staff  
                          Forms  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Registrar
                              	  
                          
                              	Calendars
                              	  
                         Summer 2017 Final Exams 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Summer 2017 Final Exams 
                     
                      The semester's final exam schedule follows. Please be aware of these final exams rules
                        and guidelines:
                      
                     
                      
                        
                         Attendance is compulsory. 
                        
                         Final exams will be given during the regular class time on the dates listed for ALL
                           weekday and evening courses regardless of the days of the week the class normally
                           meets. However, if a class does not normally meet on Friday, the instructor may hold
                           the exam on the last regular class date, provided that: (1) the instructor communicates
                           this in the course syllabus and (2) the class meets for the minimum number of required
                           contact hours as stated on the   Contact Minutes page  .
                         
                        
                         No exam period, as scheduled below, may be changed without the written permission
                           of the department chair and the college dean.
                         
                        
                         Common final exams are given for certain courses and are noted in the   Class Schedule Search  .
                         
                        
                      
                     
                      Exam Dates 
                     
                      
                        
                         
                           
                            
                              
                                Part of Term      
                               
                              
                                Day  /  Date  
                              
                            
                           
                            
                              
                               Pre Summer 
                              
                               F /  May 26   
                              
                            
                           
                            
                              
                               Full 
                              
                               F /  August 4  
                              
                            
                           
                            
                              
                               1st Half 
                              
                               F /  June 30   or  last class meeting
                               
                              
                            
                           
                            
                              
                               2nd Half 
                              
                               F /  August 4   or  last class meeting
                               
                              
                            
                           
                            
                              
                               1st Teacher 
                              
                               F /  June 16   or  last class meeting
                               
                              
                            
                           
                            
                              
                               2nd Teacher 
                              
                               F /  July 21   or  last class meeting
                               
                              
                            
                           
                            
                              
                               TN eCampus 
                              
                               Based on course syllabus 
                              
                            
                           
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Calendars  
                               View Academic Year calendars; semester dates/deadlines; final exam schedules. 
                            
                            
                                Transcripts  
                               Request copies of your official transcript. 
                            
                            
                                Notice!  
                                Check for alerts and reminders. 
                            
                            
                                Contact Us   
                               Need additional assistance or information? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


