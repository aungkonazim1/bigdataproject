Marie Gill - The Loewenberg College of Nursing - University of Memphis  Marie E. Gill, Ph.D., RN Assistant Professor  










 
 
 
     



 
    
    
    Marie Gill - 
      	The Loewenberg College of Nursing
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			The Loewenberg College of Nursing
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About Us  
                          Programs  
                          Students  
                          Faculty and Staff  
                          Research  
                          Alumni  
                          News  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	The Loewenberg College of Nursing
                              	  
                          
                              	Faculty   Staff
                              	  
                         Marie Gill 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Marie E. Gill, Ph.D., RN Assistant Professor
                      
                     
                      
                        
                         
                           
                            
                              
                               
                                 
                                  
                                    
                                     
                                       
                                        
                                           Phone:  
                                          
                                           901-678-3106 
                                          
                                        
                                       
                                        
                                           Fax:  
                                          
                                           901-678-4906 
                                          
                                        
                                       
                                        
                                           E-mail:  
                                          
                                            mgill@memphis.edu  
                                          
                                        
                                       
                                        
                                           Office:  
                                          
                                           3560 Community Health Building 
                                          
                                        
                                       
                                     
                                    
                                  
                                 
                               
                              
                                  
                              
                            
                           
                         
                        
                      
                     
                       Dr. Gill joined the Loewenberg College of Nursing in August 2014 as Assistant Professor.
                        She has experience in cardiovascular nursing, patient education, and nursing education
                        for undergraduate students, and staff development. Dr. Gill also has experience for
                        post-award research grant management.
                      
                     
                       Research:  
                     
                      Dr. Gill's program of research focuses on health literacy interventions to promote
                        sobriety in Drug Court Diversion Program clients. Drug Courts are addiction treatment
                        alternatives to incarceration for drug-related crimes. She has worked with counselors
                        and treatment providers in the Shelby County Drug Court Program to identify clients
                        at risk for dropping out of the program and who need increased Drug Court interventions
                        to complete the 1-year program.
                      
                     
                       Current Research:  
                     
                      Dr. Gill has completed a research project "Developing a Low Literacy Client Handbook
                        for Shelby County Drug Court" that was funded by the Shelby County Drug Court Foundation.
                        As a researcher, Dr. Gill is involved with the Adverse Childhood Experiences (ACE)
                        Awareness Program, an initiative developed by the Loewenberg College of Nursing and
                        funded by The Urban Child Institute of Memphis. The purpose of the ACE program is
                        to educate nursing faculty, students, and community stakeholders about adverse childhood
                        experiences and empower them with knowledge, skills, and attitudes necessary for ACE
                        prevention.
                      
                     
                       Publications:  
                     
                      Gill, M. E. (2016). Predictors of drug court graduation. Journal of Offender Rehabilitation,
                        55(8), 564-588.  http://dx.doi.org/10.1080/10509674.2016.1229710  Gill, M.,   Engle, V. (2011, February). A Multidimensional Health Literacy Model to
                        Guide Research. Presentation at the Southern Nursing Research Society in Jacksonville,
                        Florida. Gill, M. (20ll, June). Drug court treatment programs. Presentation for the Tennessee
                        Institute for Pre-Professionals at the University of Tennessee Health Science Center,
                        Memphis, TN. Gill, M. (2011, November). PhD student experiences. Podium session presented at The
                        University of Tennessee Health Science Center, College of Nursing, Community Advisory
                        Council, Memphis, TN. Gill, M., Engle, V.F., Speck, P.M.,   Cunningham, P. (2011, November). Navigating
                        drug court treatment programs: Health literacy issues. Podium session presented at
                        the Coahoma Community College Healthcare Conference, Tunica, MS. Gill, M., Engle, V.F., Speck, P.M.,   Cunningham, P. (2011, October). Navigating treatment
                        programs: Health literacy issues for drug court clients. Poster session presented
                        at the Boston University Medical Campus Annual Health Literacy Research Conference,
                        Chicago, IL (refereed). Gill, M.,   Engle, V. F. (2011, February). A multidimensional health literacy model
                        to guide research. Poster session presented at the University of Tennessee Health
                        Science Center Graduate Research Day, Memphis, TN. Gill, M., Speck, P. M.,   Engle, V. F. (2011, April). Literacy, health literacy and
                        health in a Mid-South drug court population. Roundtable discussion presented at the
                        Academy on Violence and Abuse Meeting, Minneapolis, MN. (refereed) Gill, M. E. (2012, December). Predictors of drug court graduation. Roundtable discussion
                        presented at the Shelby County Drug Court, Memphis, TN. Gill, M. E. (2013, March). Predictors of drug court graduation. Roundtable discussion
                        presented at the Shelby County Drug Court Foundation Board Meeting, Memphis, TN.
                      
                     
                       Teaching:  
                     
                      Dr. Gill teaches undergraduate BSN students in Evidence-Based Practice, Professional
                        Nursing Seminar, and Transitions Into Nursing Professional Practice. She teaches Evidence-Based
                        Practice at the Lambuth Campus. She has also taught Leadership/Management clinical
                        courses and online courses for Introduction to Nursing Research.
                      
                     
                       Service:  
                     
                      Dr. Gill serves on the Undergraduate Curriculum Committee, BSN Coordinators Committee,
                        Evaluation Committee, and the University Engaged Scholarship Committee. She is a member
                        of Sigma Theta Tau Nursing Honor Society Beta Theta Chapter, American Nurses Association,
                        Tennessee Nurses Association, and is a volunteer for Crosslink Memphis to help link
                        medical supplies to the impoverished and needy.
                      
                     
                       Education:  
                     
                      Dr. Gill received her Bachelor of Science in Nursing from the University of Tennessee
                        Health Science Center in 1988. She obtained her Master of Science in Education and
                        Masters of Science in Nursing from the University of Memphis in 1995 and 2006, respectively.
                        Dr. Gill earned her Ph.D. in Nursing and Public Health minor from the University of
                        Tennessee Health Science Center in December, 2012.
                      
                     
                       Personal:  
                     
                      Dr. Gill enjoys spending time with her husband, daughter, and family. She also enjoys
                        music, cooking, hiking, trail running, indoor cycling, yoga, and rebuilding houses
                        through Appalachian Service Project.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Apply Now  
                               Take the first step to your new career 
                            
                            
                                RN-BSN Program  
                               Learn more about our flexible, accessible and affordable online program 
                            
                            
                                Lambuth Campus Nursing  
                               Earn your degree in Jackson, TN 
                            
                            
                                Contact Us  
                               Our team is here to help 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/12/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


