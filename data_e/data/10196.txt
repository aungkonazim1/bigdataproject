 Events of the Benjamin L. Hooks Institute for Social Change - Ben Hooks - University of Memphis    










 
 
 
     



 
    
    
     Events of the Benjamin L. Hooks Institute for Social Change - 
      	Ben Hooks
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			The Benjamin L. Hooks Institute for Social Change
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Programs  
                          Events  
                          HAAMI  
                          Documentaries  
                          Collections  
                          About  
                          Newsroom  
                      
                      
                         
                            Archives   
                            
                               
                                  
                                   2017  
                                   2016  
                                   2015  
                                   2014  
                                   2013  
                                   2012  
                                   2011  
                                   2010  
                                   2009  
                                   2008  
                                   2006  
                                   2005  
                                   2004  
                                   Make a gift to the Hooks Institute  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Ben Hooks
                              	  
                         
                           	Events
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      2017 Events 
                     
                      About Hooks Events 
                     
                      Protecting and promoting civil rights and social justice requires vigilance by each
                        of us. The Hooks Institute hopes you will stay engaged with and connected to efforts
                        to promote a just, fair and democratic society. We encourage you to join us at our
                        public events.
                      
                     
                       Hooks Institute Events | September 2017 - March 2018  
                     
                       Click here to view upcoming and past Hooks African American Male Initiative events.  
                     
                      Upcoming Events 
                     
                      October 2017 
                     
                      October 19, 2017 
                     
                        Hooks National Book Award Lecture | Russell Rickford  
                     
                       Thurs., Oct. 19, 2017 | Reception 11:30 am | Lecture   Book Signing Noon   University Center (300) River Room | University of Memphis  
                     
                      Selected by the Hooks Institute's National Book Award Committee as the book published
                        in 2016 that best furthers the legacy of the American Civil Rights Movement, Russell
                        Rickford's  We Are an African People: Independent Education, Black Power, and the Radical Imagination  examines the motivation behind independent black schools that emerged in the 1970s
                        as an alternative mechanism for African American uplift.
                      
                     
                      The event is co-sponsored by the UofM African   African American Studies Program,
                        the UofM College of Education, the UofM History Department and the UofM Marcus Orr
                        Center for the Humanities.
                      
                     
                       Free and Open to the public. Convenient and affordable parking is available in the
                           public garage on Zach Curlin Street.  
                     
                      
                     
                      October 18-20, 2017 
                     
                        
                     
                      18th Annual Graduate Association for African American History for African American
                        History Conference
                      
                     
                      October 18-20, 2017 | The University of Memphis 
                     
                      Keynote Speaker: Paula J. Giddings 
                     
                       Sponsors:  African and African American Studies Program Department of History Benjamin Hooks
                        Institute for Social Change Marcus Orr Center for the Humanities Memphis Alumnae Chapter,
                        Delta Sigma Theta Sorority, Inc.
                      
                     
                      Past Events 
                     
                      October 2017 
                     
                      October 5, 2017 
                     
                        2017 Hooks Institute Open House   Release of the Hooks Policy Papers  
                     
                       Thurs., Oct. 5, 2017 | Reception 5:30 pm | Panel Discussion 6 pm    University Center (304) Bluff Room | University of Memphis  
                     
                      Join us for the release of the Hooks Institute Policy Papers (3rd edition). Authors
                        of the Policy Papers will present their research on critical civil rights issues of
                        today including the Trump Administration's roll back on LGBT rights, the impact of
                        immigration on a small Arkansas town, the influence of Muslims and Islam on the nation's
                        fabric, and what we can learn from welfare in Native American communities.
                      
                     
                       Free and Open to the public. Convenient and affordable parking is available in the
                           public garage on Zach Curlin Street.  
                     
                      
                     
                      October 3, 2017 
                     
                       Drawing the Protest Line:  Freedom of Assembly, Speech, and Expression
                      
                     
                      Tuesday, October 3, 2017, 12:00 pm – 1:00 pm | UC River Room (300) 
                     
                      What is free speech and why do we value it? What is the distinction between free speech
                        and hate speech? How can students voice dissent without threatening the university's
                        culture of inclusivity? What are guidelines for protest at public institutions?
                      
                     
                      Join us for a panel discussion on peaceful protest in university settings. Students
                        from representative organizations will pose questions to a number of faculty and legal
                        experts to facilitate a dialogue about the applications and limits of free speech.
                        Topics include: "Unite the Right" Riot in Charlottesville, Virginia; NFL "Take a Knee"
                        Protest of Police Brutality; Free Speech/Hate Speech on Campus; Peaceful Protest
                      
                     
                       Faculty/Legal Panelists: Otis Sanford, Hardin Chair of Excellence in Economic and
                        Managerial Journalism, Melanie Murry, J.D., University Counsel, Latosha Dexter, J.D.,
                        Deputy University Counsel, John Michael Ryall, J.D., Assistant University Counsel,
                        Daphene McFerren, J.D., Executive Director, Benjamin L. Hooks Institute for Social
                        Justice, Justin Lawhead, Ed.D., Dean of Students, and other active scholars and attorneys
                      
                     
                      Student Panelists: Kevyanna Rawls, Vice President of The Student Government Association
                        and Leaders of other Student Organizations.
                      
                     
                      
                     
                      September 2017 
                     
                      September 28, 2017 
                     
                      Friends of the Hooks Institute Reception | Thurs., Sept. 28, 2017 | 6 pm–8 pm 
                     
                      This is a donor appreciation event for community members and sponsors who have helped
                        to fund Institute programs.  Stay tuned for more information.  
                     
                      
                     
                      September 21, 2017 
                     
                      UofM Faculty Reception | Thurs., Sept. 21, 2017 | 1 pm–3 pm  Hooks Institute | Scates Hall, Rm 204 | University of Memphis
                      
                     
                      UofM faculty are invited to a reception at the Hooks Institute to explore opportunities
                        for partnerships and collaborations that advance social justice and civil rights on
                        campus and in the community.  This event is open to all UofM faculty.  
                     
                      
                     
                        
                     
                      September 8, 2017 
                     
                      DACA: Its Impact on University Communities 
                     
                      September 8, 2017 | Noon – 1:30 pm | Zone of the FedEx Institute of Technology (FIT) 
                     
                      On September 8, 2017, from 12:00 p.m. to 1:30 p.m. in the Zone of the FedEx Institute
                        of Technology (FIT), the Critical Conversations Committee will host the first in a
                        series of sessionsto discuss the Deferred Action for Childhood Arrivals (DACA) program
                        and its impact on University communities.
                      
                     
                       The featured speaker is Tatine Darker, Esq., an immigration specialist.  University
                        administrators, scholars, and Legal Counsel will also be on hand to facilitate the
                        discussion and to address students’ questions in particular. We encourage all students,
                        faculty and staff to attend.
                      
                     
                      
                     
                      july 2017 
                     
                      July 24, 2017 
                     
                      Presentation TN History for Kids in Service for Tennessee Teachers. 
                     
                      Errol Rivers, Hooks Graduate Assistant, presented the history of the Fayette County,
                        TN Civil Rights Movement at the TN History for Kids in service for TN Teachers. The
                        Hooks Institute is spreading the word on how this movement is relevant today and how
                        the civil rights movement in the Mid-South created greater equality throughout the
                        nation.
                      
                     
                      
                     
                      June 2017 
                     
                       June 15-June 18  
                     
                        1st Annual TriState Black Pride Festival  
                     
                      The Hooks Institute was proud to co-sponsor the 1st Annual TriState Black Pride Festival
                        in Memphis, TN (June 15 - 18, 2017) at the National Civil Rights Museum. This LGBTQIA
                        event brought awareness, with the intent of providing some proposed solutions, to
                        health, employment and social challenges facing the black LGBTIA community.
                      
                     
                      For more information on Tri-State Black Pride, please visit  www.tristateblackpride.com .   
                     
                      
                     
                      may 2017 
                     
                      May 18 
                     
                      Senior Signing Day: Freedom Preparatory Academy May 18, 2017 | University Center Ballroom, University of Memphis
                      
                     
                        
                     
                      Congrats to Graduating Seniors of Freedom Prep Academy! The Hooks Institute was honored
                        to host Freedom Prep Academy's first Senior Signing Day at the University of Memphis.
                        Impressively, every senior of the school has been accepted into college! Several will
                        join us at the University of Memphis!  The event was sponsored in part by the Hooks Institute.  
                     
                      
                     
                      APRIL 2017 
                     
                      April 20 
                     
                       2017 Hooks Annual Join Hands For Change Gala   Pursuing the Dream to Reality  
                     
                        
                     
                      Honored Guest   Keynote Speaker Ambassador Andrew J. Young
                      
                     
                      Thursday, April 20, 2017 | 7 PM Holiday Inn at the University of Memphis!
                      
                     
                      HOOKS INSTITUTE: 20 YEARS AND COUNTING 
                     
                      In celebration of Hooks Institute's 20th anniversary, the 2017 Join Hands for Change
                        Gala will be an exciting evening to celebrate the Institute's work over the last two
                        decades. The event will be held on the evening of Thursday, April 20, 2017 at 7pm
                        at the University of Memphis Holiday Inn. We are excited that this year's honoree
                        and featured keynote will be Ambassador Andrew J. Young, civil rights pioneer, politician,
                        diplomat and two-time mayor of Atlanta.
                      
                     
                      
                     
                       April 19  
                     
                       Symposium on Creating Greater Outcomes for   
                     
                       African American Male Achievement  
                     
                      April 19, 2017 | 9 am - 4:30 pm UC Bluff Room 304 | University of Memphis
                      
                     
                        
                     
                      
                     
                       April 19  
                     
                       Official Launch of    the Benjamin L. Hooks Papers website  
                     
                        
                     
                      Launch of the Benjamin L. Hooks Papers Website April 19, 2017 | Reception 5:30 pm | Presentation 6 pm University Center (304) Bluff Room The University of Memphis | 499 University St | Memphis, TN 38152
                      
                     
                      From his tenure as the first African American appointed to the Federal Communications
                        Commission, to the leadership of the National Association for the Advancement of Colored
                        People, Benjamin L. Hooks career took him from the segregated streets of Memphis,
                        TN to the height of American public and civic life. Now, for the first time, portions
                        of Hooks vivid career will be made widely available to the public through the Benjamin
                        L. Hooks Papers Digitization Website. The digitization project is a collaboration
                        between the Hooks Institute and the University of Memphis Libraries.
                      
                     
                      
                     
                      April 6 - 8 
                     
                      National Conference on Undergraduate Research 
                     
                        
                     
                      Keynote Speaker: Daphene R. McFerren, Executive Director Hooks Institute 
                     
                       The National Conference on Undergraduate Research  The National Conference on Undergraduate Research, established in 1987, is dedicated
                        to promoting undergraduate research, scholarship, and creative activity in all fields
                        of study by sponsoring an annual conference for students. Through this annual conference,
                        NCUR creates a unique environment for the celebration and promotion of undergraduate
                        student achievement, provides models of exemplary research and scholarship, and helps
                        to improve the state of undergraduate education.
                      
                     
                      
                     
                      MARCH 2017 
                     
                      March 9–12 
                     
                       125 Anniversary of the People's Grocery Lynching Memorial Service
                      
                     
                      In March of 1892, in a mixed race neighborhood called the Curve, near Mississippi
                        Blvd and Walker Avenue, a white grocer named William Barrett found his business shrinking
                        because of the success of grocery nearby run by three black men, Will Stewart, Tommie
                        Moss, and Calvin McDowell. Rumors and trumped up charges sent a large group of armed
                        white men into the store. Gunshots were traded and several white men were injured.
                        The three black grocers, all family men, were arrested and jailed. Three days later
                        the downtown jail was stormed and Stewart, Moss, and McDowell were dragged out and
                        taken to the nearby Chesapeake and Ohio rail yards.
                      
                     
                      The three men fought back, but eventually, McDowell was shot point blank by a shotgun.
                        Will Stewart resisted until he was shot in the neck. Tommie Moss was asked if he had
                        any final words. He said "Tell my people to go west. There is no justice for them
                        here." He was shot and left with the others under a pile of brush.  For more information
                          click here .   
                     
                      
                     
                      February 2017 
                     
                      February 23 
                     
                        
                     
                       Ibram X. Kendi Book Talk and Discussion   
                     
                       Stamped from the Beginning: The Definitive History of Racist Ideas in America  
                     
                      Thursday, February 23, 2017 | 5:30 pm Reception | 6:00 pm Lecture University Center Auditorium, The University of Memphis Free and Open to the Public
                      
                     
                        Stamped from the Beginning: The Definitive History of Racist Ideas in America.  Presented by the Department of History in conjunction with the program in African
                           and African American Studies, the Benjamin L. Hooks Institute for Social Change, the
                           Marcus W. Orr Center for the Humanities, the Graduate Association for African-American
                           History, and the Departments of Anthropology, Communication, Philosophy, Political
                           Science, and Sociology.    
                     
                      
                     
                       February 22  
                     
                        Hilton Worldwide African American History Event | Hilton Worldwide, Memphis, TN  
                     
                      Daphene R. McFerren speaks about her families role in the Fayette County Civil RIghts
                        movement and the importance of learning and sharing your families history as an honored
                        guest at Hilton Worldwide's African American History event, Feb. 22, 2017.
                      
                     
                      
                     
                       February 14 - 20  
                     
                        Duty of the Hour   WKNO-TV Broadcast Times and Dates
                      
                     
                          
                     
                      7 pm | Tuesday, February 14, 2017 | WKNO-HD 12 am |Wednesday, February 15, 2017 | WKNO-HD 7 pm | Wednesday, February 15, 2017 | WKNO2 7 pm | Monday, February 20, 2017 | WKNO-HD
                      
                     
                      
                     
                      February 9 - 24 
                     
                      Duty of the Hour Community Screenings   
                     
                      White Station High School - February 9, 2017 at 9 am | Closed to the public  Booker T Washington – February 14, 2017 | Closed to the public LeMoyne-Owen College – February 22, 2017 | Closed to the public  Melrose High School – February 24, 2017  | Closed to the public
                      
                     
                      February 10 - 14 
                     
                       Duty of the Hour  KCET- Los Angeles | Broadcast Times and Dates
                      
                     
                        
                     
                      12 pm | Friday, February 10, 2017 | KCET 3 am | Tuesday, February 14, 2017 | KCET For more information please visit  https://www.kcet.org/shows/link-voices/episodes/duty-of-the-hour  
                     
                      
                     
                      February 2 
                     
                      Critical Conversations | The Impact of the Executive Order on Immigration  February 8, 2017, 11:00 am | University Center (350) Fountain Room | University of
                        Memphis
                      
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Archives 
                         
                            
                               
                                2017  
                                2016  
                                2015  
                                2014  
                                2013  
                                2012  
                                2011  
                                2010  
                                2009  
                                2008  
                                2006  
                                2005  
                                2004  
                                Make a gift to the Hooks Institute  
                            
                         
                      
                      
                      
                         
                            
                                Make a gift to the Hooks Institute  
                               Join us as we work to uplift Memphis and uplift the nation. Make a gift of any amount
                                 to the Hooks Institute.
                               
                            
                            
                                Critical Conversations  
                               Where is your voice? A University-wide initiative to promote and sustain an inclusive
                                 and democratic society.
                               
                            
                            
                                Benjamin L. Hooks Papers Website  
                               Explore the Civil Rights Movement and beyond through the eyes of Benjamin L. Hooks. 
                            
                            
                                2017 Hooks Institute Policy Papers  
                               Read the 2017 Hooks Institute Policy Papers Online! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


