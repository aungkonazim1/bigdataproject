Kathy Lou Schultz - Department of English - University of Memphis    










 
 
 
     



 
    
    
    Kathy Lou Schultz - 
      	Department of English
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of English
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Undergraduate  
                          Graduate  
                          People  
                          Community  
                          News/Events  
                      
                      
                         
                            People   
                            
                               
                                  
                                   Staff  
                                   African American Literature  
                                   Applied Linguistics  
                                   Creative Writing  
                                   Composition Studies and Professional Writing  
                                   Literary Cultural Studies  
                                   Instructors  
                                   Lambuth Campus  
                                   General Inquiries  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of English
                              	  
                          
                              	People
                              	  
                         Kathy Lou Schultz 
                      
                   
                   
                       
                      
                     
                     	
                     
                     		  
                      
                        
                        
                         
                           
                           
                                                                
                              
                              
                               
                              
                              
                            
                           
                           
                            
                              
                               
                                 
                                 Kathy Schultz
                                 
                               
                              
                              
                               
                                 
                                 Associate Professor, Director of Women's and Gender Studies
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Phone 
                                    
                                     
                                       
                                       (901) 678-2651
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Email 
                                    
                                     
                                       
                                       klschltz@memphis.edu
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Fax 
                                    
                                     
                                       
                                       (901) 678-2226
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     Office 
                                    
                                     
                                       
                                       Patterson 443
                                       
                                     
                                    
                                  
                                 
                                  
                                    
                                     Office Hours 
                                    
                                     
                                       
                                       Call for Hours
                                       
                                     
                                    
                                  
                                 
                               
                              
                              
                               
                                 										
                                 
                                 
                                   Website  
                                                                         
                                 
                               
                              
                            
                           
                           
                         
                        			 
                        			  
                        
                        
                         Education 
                        
                         B.A.,1990, Oberlin College M.F.A., 1996, San Francisco State University Ph.D., 2006, University of Pennsylvania
                         
                        
                         Academic Summary 
                        
                         Poet-scholar Dr. Kathy Lou Schultz's areas of research include African American and
                           Afro-Diasporic literature and culture, multi-ethnic American literature, poetry and
                           poetics, modernism, critical theory, gender studies, and the epic. She has a continuing
                           interest in the relationship between literary histories and national histories, as
                           well as the role of the arts, especially poetry, in the social world.
                         
                        
                         Select Publications 
                        
                         Monograph 
                        
                         
                           
                             The Afro-Modernist Epic and Literary History: Tolson, Hughes, Baraka  (New York: Palgrave Macmillan, Series in Modern and Contemporary Poetry and Poetics,
                              forthcoming 2012)
                            
                           
                         
                        
                         Poetry Collections 
                        
                         
                           
                             Biting Midge  (New York: Belladonna, 2008)
                            
                           
                             Some Vague Wife  (Berkeley: Atelos Press, 2002)
                            
                           
                         
                        
                         Articles and Book Chapters 
                        
                         
                           
                            "Melvin B. Tolson."  Companion to Modernist Poetry . Eds. David Chinitz and Gail McDonald. Blackwell, forthcoming.
                            
                           
                            "Amiri Baraka's  Wise Why's Wise : Lineages of the Afro-Modernist Epic."  Journal of Modern Literature  35.3 (Spring 2012): 25-50.
                            
                           
                            "To Save and Destroy: Melvin B. Tolson, Langston Hughes, and Theories of the Archive."
                               Contemporary Literature  Vol. 52 No. 1 (Spring 2011): 108-45.
                            
                           
                            "'I have questions': Kalamu ya Salaam and Langston Hughes."  Jacket2 . Kelly Writers House, University of Pennsylvania. 7 Nov. 2011. Web.
                            
                           
                            "'My Epic': Aaron Shurin, Robert Duncan, and the New College of California"  Jacket2 . Kelly Writers House, University of Pennsylvania. 4 Oct 2011. Web.
                            
                           
                            "Rhapsodes, Griots, and Modes of Performance."  Jacket2 . Kelly Writers House, University of Pennsylvania. 11 Sept 2011. Web.
                            
                           
                            "Kathy Lou Schultz On Myung My Kim."  Efforts and Affections: Women Poets on Mentorship . Eds. Arielle Greenberg and Rachel Zucker. Iowa City: University of Iowa Press, 2008.
                              207-216.
                            
                           
                            "Small Press, Big Wor(l)ds: African American Poetry from Publication to Archive."
                               Rainbow Darkness: An Anthology of African American Poetry . Ed. Keith Tuma. Oxford, OH: Miami University Press, 2005. 185-196.
                            
                           
                            "Proceed Queerly: The Sentence As Compositional Unit."  Biting the Error: Writers Explore Narrative . Ed. Mary Burger, Robert Gluck, Camille Roy, and GailScott. Toronto, ON: Coach House
                              Press. 2004.
                            
                           
                            "Talking Trash, Talking Class: What's a Working Class Poetic and Where Would I Find
                              One?"  Tripwire: A Journal of Poetics  No. 1, February 1998; Reprinted in HOW2 1.2 (September 1999)
                            
                           
                         
                        
                         Recordings 
                        
                         
                           
                            "Trio" (three poems). Dr. Guy's MusiQology.  The Colored Waiting Room . 2012. CD.
                            
                           
                         
                        
                         Poems in journals including  New American Writing, OnandOnScreen, Fence Magazine, Hambone, Electronic Poetry Review,
                              The Philadelphia Inquirer, Mirage #4/Period(ical), Fourteen Hills , and others.
                         
                        
                        
                      
                     								
                     
                     
                     
                     
                     	
                      
                   
                
                
                   
                      
                         People 
                         
                            
                               
                                Staff  
                                African American Literature  
                                Applied Linguistics  
                                Creative Writing  
                                Composition Studies and Professional Writing  
                                Literary Cultural Studies  
                                Instructors  
                                Lambuth Campus  
                                General Inquiries  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now  
                                
                            
                            
                                Alumni and Friends  
                                
                            
                            
                                Course Offerings  
                                
                            
                            
                                Contact Us  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/13/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


