Writing Competitions &#8211; On Legal Grounds | Memphis Law Events &amp; Announcements    
 

 
	 
	 
	 
	 
	
	 Writing Competitions   On Legal Grounds | Memphis Law Events   Announcements 
 

 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 










 
 
  
 
 
    

 

 

 

	 
	
				
		 
			 
				
				 
					     
					 On Legal Grounds | Memphis Law Events   Announcements 									 
				
									 
						    
						   
						    Student Organizations 
 
	  AWA  
	  BLSA  
	  Bus.   Tax Law Society  
	  Christian Legal Society  
	  Federalist Society  
	  Health Law Society  
	  HLSA  
	  Honor Council  
	  ILS  
	  Law Review  
	  Memphis Law +1  
	  Moot Court Board  
	  OutLaw  
	  PAD  
	  PALS  
	  SELS  
	  SBA  
	  SGA  
	  Street Law  
	  TACDL  
 
 
  Law School Announcements 
 
	  Academic Affairs 
	 
		  Academic Affairs Calendar  
	 
 
	  Academic Success Program 
	 
		  Academic Success Program Calendar  
	 
 
	  Career Services Office 
	 
		  Career Services Office Calendar  
	 
 
	  Information Technology 
	 
		  Information Technology Calendar  
	 
 
	  Law Admissions, Recruiting,   Scholarships 
	 
		  Law Admissions, Recruiting, and Scholarships Calendar  
	 
 
	  Law School Registrar 
	 
		  Law School Registrar Calendar  
	 
 
	  Library 
	 
		  Library Calendar  
	 
 
	  Office of the Dean 
	 
		  Office of the Dean Calendar  
	 
 
	  Office of Diversity 
	 
		  Office of Diversity Calendar  
	 
 
	  Pro Bono Office 
	 
		  Pro Bono Office Calendar  
	 
 
	  Student Affairs 
	 
		  Student Affairs Calendar  
	 
 
 
 
  Blog Information  
  Writing Competitions  
  Events  
  
					  
								
			  
		  
		
	  
	
	 
		 			
			 
				 
 

	 

			   Category:  Writing Competitions  

	
  	
	 		
		
				
				
			 
				 				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Writing Competitions  
			 19 Mar, 2016 
		  
		
		 
			 Animals and the Law Student Writing Competition 
		  
		
				 				
			 Competition: 2016 New York State Bar Association Committee on Animals and the Law   Student Writing Competition Topic: Submissions may cover    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Writing Competitions  
			 19 Mar, 2016 
		  
		
		 
			 Religious Liberty Student Writing Competition 
		  
		
				 				
			 Competition: Seventh Annual Religious Liberty Student Writing Competition Topic: Submissions should relate to a topic of domestic or international religious liberty,    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Writing Competitions  
			 20 Nov, 2015 
		  
		
		 
			 2016 Bloomberg Law® Write-On Competition 
		  
		
				 				
			 Ever thought about being published? Have a paper you ve written, or something you re interested in that impacts the legal landscape? Bloomberg    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Writing Competitions  
			 30 Mar, 2015 
		  
		
		 
			 Public Interest Essay Competition 
		  
		
				 				
			 Competition: University of Pennsylvania Law Review Public Interest Essay Competition Topic: Submissions must focus on a specific legal issue within the    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Writing Competitions  
			 21 Mar, 2015 
		  
		
		 
			 International Humanitarian Law Student Writing Competition 
		  
		
				 				
			 Competition: Fifth Annual International Humanitarian Law Student Writing Competition Topic: Submissions should address  The Intersection of International Humanitarian Law and Gender.  Deadline:    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Writing Competitions  
			 23 Feb, 2015 
		  
		
		 
			 Bankruptcy Writing Competition 
		  
		
				 				
			 Competition: National Association of Chapter 13 Trustees Writing Competition Topic: Entries should address an issue concerning Chapter 13 of the Bankruptcy Code. Deadline: April 30,    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Writing Competitions  
			 12 Feb, 2015 
		  
		
		 
			 Warren E. Burger Writing Competition 
		  
		
				 				
			 Competition: Warren E. Burger Writing Competition Topic: Entries should address an issue of legal excellence, civility, ethics, and professionalism. Deadline: July 1, 2015    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Writing Competitions  
			 11 Feb, 2015 
		  
		
		 
			 2015 Selma Moidel Smith Student Writing Competition 
		  
		
				 				
			 Competition: 2015 Selma Moidel Smith Student Writing Competition Topic: Entries should address an issue concerning women’s rights or the status of women in the    
		  
				
	  	
  					  				 	
	 
		
		 
			 
																			 
					  
		
		 
			  Writing Competitions  
			 10 Feb, 2015 
		  
		
		 
			 Writing Competition   Broad Range of Permissible Topics 
		  
		
				 				
			 Competition: International Association of Defense Counsel 2015 Writing Competition Topic: Entries should address any issue in tort law, insurance law, civil procedure,    
		  
				
	  	
  									 	
	 
		
		 
			 
																			 
					  
		
		 
			  Writing Competitions  
			 10 Feb, 2015 
		  
		
		 
			 Cultural Heritage Preservation Writing Competition 
		  
		
				 				
			 Competition: 2015 Law Student Writing Competition of the Lawyers  Committee for Cultural Heritage Preservation Topic: Entries may address any aspect of cultural heritage    
		  
				
	  	
  					   			  
		
			 
			 
 Page 1 of 3  1  2  3    
 	  
			
				
	  
	
  


	 
		
		    
		
		 
			
						 
				 Follow: 
							 
						
						
						
			   Law School Website                     		 		 Recent Posts 		 
					 
				 1L Minority Clerkship Program   Reception (Chattanooga) 
						 
					 
				 New resources! 
						 
					 
				 Community Legal Center Volunteer Opportunity 
						 
					 
				 Law School Bookstore 2 Day Sale   Dec. 6th and 7th! 
						 
					 
				 St. Jude Memphis Marathon/Downtown Street Closures 
						 
				 
		 		  Archives 		 
			  December 2017  
	  November 2017  
	  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
		 
		   Categories 		 
	  Academic Affairs 
 
	  Academic Success Program 
 
	  AWA 
 
	  BLSA 
 
	  Career Services Office 
 
	  Christian Legal Society 
 
	  Experiential Learning 
 
	  Federal Bar Association 
 
	  Federalist Society 
 
	  Health Law Society 
 
	  HLSA 
 
	  Honor Council 
 
	  ILS 
 
	  Information Technology 
 
	  Law Admissions, Recruiting,   Scholarships 
 
	  Law Review 
 
	  Law School Announcements 
 
	  Law School Registrar 
 
	  Library 
 
	  Memphis Law +1 
 
	  Mock Trial 
 
	  Moot Court Board 
 
	  National Lawyer s Guild 
 
	  Office of Diversity 
 
	  Office of the Dean 
 
	  OutLaw 
 
	  Outside Organizations 
 
	  PAD 
 
	  PALS 
 
	  Pro Bono Office 
 
	  SBA 
 
	  Sports   Entertainment Law Society 
 
	  Street Law 
 
	  Student Affairs 
 
	  Student Organizations 
 
	  Tennessee Association of Criminal Defense Lawyers 
 
	  Writing Center 
 
	  Writing Competitions 
 
		 
 			
		  
		
	  

	
 
	
	    
	
	 
		
				 
			 More 
		 
				
				
		  Search   
	 
		 
	 
    Login 					 
					 
										 
					 
					 
					  Username:  
					  
					  Password:  
					  
										  
					  Remember me  
															   
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
											  Don't have an account?  
										  Lost your password?  
					 
					
					 
					
										 
				
					 
					  Choose username:  
					  
					  Your Email:  
					  
										   
					 
					 
					 
					 
					 
					 
					 
					 
					   Have an account?   
					 
										
					 
			
					 
					  Enter your username or email:  
					  
										   
					 
					 
					 
					 
					 
					 
					 
					 
					   Back to login   
					 
					
					 
					
										  
					   
					 
					   Subscribe 	
	
 
	 
		 
		 
		 		
		
	 
		
		 
					  

		
 
		 Email Address *  
	 
  
 
		 First Name 
	 
  
 
		 Last Name 
	 
  
 
		 Middle Name 
	 
  			 
				* = required field			  
			
		 
			 
		  
	
	
				
	  
	  
  
	    Blog Feedback                     		
	  
	
  	

				  
			  			
		  
	  

	 
		
				
				
				
		 
			 
				
				    
				
				 
					
					 
						
												
						 
															 On Legal Grounds | Memphis Law Events   Announcements   2017. All Rights Reserved. 
													  
						
												 
							 Powered by  WordPress . Theme by  Alx . 
						  
												
					 
					
					 	
											 
				
				  
				
			  
		  
		
	  

  

 		
		










 
 
 