Check-out Privileges - Library - LibGuides at University of Memphis Law Library</title> <script type="text/javascript" src="/js/dojo-161/dojo/dojo.js" djConfig="parseOnLoad: true"></script> <script type="text/javascript" src="//code.jquery.com/jquery-1.7.min.js"></script> <script type="text/javascript" src="/include/lang.php" ></script> <script type="text/javascript" src="/js112b/lgpublic.min.js"></script> <script type="text/javascript" src="/include/lib_spring_track.php?action=init&iid=2952"></script> <script> // content_config is instantiated in content.js. content_config.pid = 667976; content_config.sid = '5530526'; content_config.iid = 2952; content_config.la_iid = 0; content_config.gid = 0; content_config.hitsource = 'r'; content_config.myboxshape = 's'; var myGNAME = "Library"; var myTNAME = "Check-out+Privileges"; var myIID = '2952'; var myPID = '667976'; var mySID = '5530526'; // SpringTracking SpringTrack.init({ _st_sid : content_config.sid, _st_iid : content_config.iid, _st_pid : content_config.pid }); </script> <title>Memphis School of Law: ResearchGuides general information and policies  
 
	 
    
         
		
		 
		 
		 
		         
         
         
          
                 
         
         
         
         
         
        
         
         
         
         
         
         
         
         
         
         
          
         
                
                
         
         
        
		        
         Check-out Privileges - Library - LibGuides at University of Memphis Law Library 
		
		
        
        
        
        
                
		
		 Memphis School of Law: ResearchGuides 

        	 
	 
				 
                    This is the "Check-out Privileges" page of the "Library" guide. 
                     Alternate Page for Screenreader Users  
                     Skip to Page Navigation  
                     Skip to Page Content  
                 		 
			 
                  
				   
				                   
						 
							 Admin Sign In 
						    
					 Library 
					   
				  
					 LibGuides 
					   
				  Library  
			
		                  
                         
                             Library   
                             Tags:  library ,  library privileges ,  library resources ,  library services    
                         
                                      general information and policies 				
		 
			 Last Updated: Nov 29, 2017 
			 URL: http://libguides.law.memphis.edu/libinfo 
			 
				 
					 Print Guide
					 
				 
			    RSS Updates      Email Alerts   
		 				    
		 Hours     
		 Contact Us     
		 Check-out Privileges     
		 Floor Maps                      
		 
			 
        		 
					 
						 Check-out Privileges 
							 
								 
									 
										 Comments(0)
										 
									 
								 
							 
						 
							 
								 Print Page 
							  
						 
					 
					 
		 
			 
				 
				 Search Text 
				 Search Type 
				 
					 
						 
							 
								  Search: 
							 
						 
						 
							 
								  
							 
						 
						 
							 
								 
									 This Guide 
                All Guides 
               
								 
							 
						 
						 
							 
								 Search 
							 
						 
					 
				 
			 
		 
					 
				 
			 
		                   
                 
                     
                                        
                                         
                                             
                                         
                                        
                                      
                                        
                                         
                                             
	 
		
		 
			
			   
			 Circulation Privileges
			 
		 
			
		   Much of the Law Library collection is reference materials for in-library use only.   
  Resources which do circulate may be checked out as follows:  
  
 
 
 
    Patron   
   Loan period   
 
 
  Cecil C. Humphreys School of Law Students  
   2 weeks  
 
 
  Cecil C. Humphreys School of Law Full-Time Faculty  
  School year  
 
 
  Cecil C. Humphreys School of Law Adjunct Faculty and Staff  
   2 weeks  
 
 
  University of Memphis Faculty, Staff, Students with U of M ID  
  2 weeks  
 
 
  Memphis Metropolitan Area Lawyers with State Bar ID and Driver's License  
   2 weeks  
 
 
 
  
   Open Reserve  (Academic Success, Bar Exam Resources, and Professional Success) and  Course Reserve  resources have varying loan periods.  Please ask at the service desk.  
    
		 
				 Comments (0) 
		 
		  
		 
	  
                                         
                                        
                                      
                                          
                                         
                                             
	 
		
		 
			
			   
			 After-Hours Check-Out
			 
		 
			
		  Send the 14-digit barcode (type or take a picture) on the upper corner of the back cover of the book to  lawcirc@memphis.edu     
		 
				 Comments (0) 
		 
		  
		 
	   
	 
		
		 
			
			   
			 Renew Online
			 
		 
			
		      My Library Account                 
		 
				 Comments (0) 
		 
		  
		 
	  
                                         
                                        
                                     				 
                
 
	 
    	Powered by  Springshare ; All rights reserved. 
    	 
        	 Report a tech support issue .      	 
  	 
	
	 View this page in a format suitable for  printers and screen-readers  or  mobile devices .  

  
	  				 
                	 
                    	 Description 
                   	 
                     
                    	    Loading... 
                   	 
             	 
				 
				 
                	 
                    	 
                        	 
                            	 
                                	   
                               	 
                           	 
                      	 
                         
                        	 More Information 
                       	 
                 	 
                     
                    	 
                        	   Loading...                      	  
						  Close  
                   	 
              	 
			 
		 
                   
        
		
	 
 