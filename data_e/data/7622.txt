Student Success - umTech - Information Technology Services - University of Memphis    










 
 
 
     



 
    
    
    Student Success  - 
      	umTech - Information Technology Services 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			umTech - Information Technology Services 
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Service  
                          Support  
                          Teaching  
                          Smart Tech  
                          Students  
                          Solutions  
                      
                      
                         
                            Student Success   
                            
                               
                                  
                                   Academic Integrity  
                                   eCourseware Access  
                                   Clickers  
                                   Media Services  
                                   Qualtrics  
                                   Hardware Recommendations  
                                   Web Conferencing  
                                   Additional Services  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	umTech - Information Technology Services 
                              	  
                         
                           	Student Success
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                        
                     
                      Helping Students Find Their Technology Edge 
                     
                      Technology is moving at a crazy pace; we are here to help our students keep up and
                        use technology to their advantage. You've come to the University to launch the career
                        of your dreams, and technology will have a big part in achieving those goals. Whatever
                        questions you might have about the various technologies being used on campus, just
                        turn to the Service Desk. Call, come by, or chat online for help.  You may also reference AskTom  for self-help articles.
                      
                     
                      
                     
                      ITS Launching New Wireless Access Pilot 
                     
                      Information Technology Services (ITS) is launching a new wireless access network pilot
                        with an additional layer of security. Effective Aug. 14, uofm-secure will provide
                        wireless users enhanced security for passwords and protection of University data.
                        ITS welcomes your participation and cooperation in helping to keep both you and the
                        University more secure with improved encryption. Existing UofM and uofm-guest wireless
                        networks remain in place while the new uofm-secure network is tested. The need for
                        two older networks will be addressed during the fall semester.  To learn more visit our uofm-secure wireless page .
                      
                     
                      
                     
                      Important Announcement: Change in Password Reset Process 
                     
                      In an effort to enhance security of passwords and protect University data, ITS will
                        implement a change in its Password Reset service. Effective immediately, the ITS Service
                        Desk (Tier 1) will no longer, administratively, change passwords. All UofM affiliates
                        (faculty, staff, students, alumni, former students, etc.) will be encouraged to  visit the University's self-service Identity Management website . Here, you can initialize your account, reset your password, retrieve your UUID,
                        and answer your security questions to regain access to your account.
                      
                     
                      In addition, remember these important tips to ensure your data is secure: • Never share your password with anyone. This includes a UofM employee, friend, or
                        relative. • Beware of phishing emails that attempt to lure you to malicious websites. • Watch for email notifications about your password changing. If you did not change
                        your password, please contact the Service Desk immediately. • Never respond to an email request for your username and password, or any other request
                        to "verify" your account.
                      
                     
                      For more information about IT security,  please visit the ITS security website .
                      
                     
                      What technology is My Instructor Using or Asking Me to Use? 
                     
                      
                        
                         
                           
                             eCourseware is our current learning management system.  This technology is commonly used to store information, documents, and communication
                              for your registered course(s). Look here to find your course syllabus, to email the
                              instructor, submit an assignment, and much more.
                            
                           
                         
                        
                         
                           
                            Podcast Central, MediaSite, and Ensemble are all  media services  used for posting digital media content for instructional purposes. Your instructor
                              may direct you to one of these to access MP3, MP4, flash content, videos, licensed
                              movies, etc. Information on how to access and/or use these services may be found in
                              your course syllabus. The Service Desk and  AskTom  can also help with questions.
                            
                           
                         
                        
                         
                           
                            Do you need to create a survey as an assignment for one of your courses?  We use Qualtrics  to create, distribute, and analyze surveys. Information on how to access and use
                              this software can be found on the  find solution  page.
                            
                           
                         
                        
                         
                           
                             BlueJeans  and  umMeet (Adobe Connect)  are used for web conferencing. If you're a member of a blended, hybrid, or fully
                              online course, you may be directed to use one of these services for virtual meetings
                              or collaborations. More information on these services can be found on the  umMedia Services  page.
                            
                           
                         
                        
                         
                           
                            Blogging anyone? The UofM provides a blog development solution through EduBlogs. There's
                              a finesse to effective blogging. Check out the helpful tips on  getting started  and maintaining your blog.
                            
                           
                         
                        
                         
                           
                            Cheating hurts everyone. Technology can help your instructor with discernment and
                              concerns of  academic integrity , through the use of products like  Turnitin .
                            
                           
                         
                        
                      
                     
                      Will I Have to Purchase Any of the Software Listed Above While I'm in School? 
                     
                      No. The software titles listed and referenced above are fully supported by the University.
                        This means we help you learn how to use it, and we also provide access to it while
                        you're a currently enrolled student with the UofM.
                      
                     
                      What Hardware or Software Might My Instructor Ask Me to Use or Buy? 
                     
                      Trying to determine if your current device or software will work, or if you should
                        buy new, can be a tough task. Here are a few helpful tips to guide you in making your
                        choice:
                      
                     
                      
                        
                         
                           
                            Most modern ( 7 years old) computers, laptops, tablets, or mobile devices will perform
                              sufficiently on our network. Older devices should be well maintained and free from
                              viruses, spyware, adware, malware, and extra toolbars or search engines.
                            
                           
                         
                        
                      
                     
                      
                        
                         
                           
                            Is a clicker required for your course? Clickers are audience response tools used during
                              your class. Instructors may use it to take attendance, capture in-class survey responses
                              and more. Clickers should be purchased at the on-campus bookstore.
                            
                           
                         
                        
                         
                           
                            Depending on your program of study, you may be required to purchase specific hardware
                              and/or software to successfully complete course/program requirements. These are normally
                              communicated through your instructor.
                            
                           
                         
                        
                         
                           
                            You can find more information about recommendations on the  Recommended Hardware/Software  for Students page.
                            
                           
                         
                        
                      
                     
                      Are There Places on (or off) Campus I Can Use to Access Hardware and Software? 
                     
                      Yes. The University provides access to over 1,300 computers to its students, using
                        TAF funded, general purpose labs. These labs can be found in several buildings at
                        each UofM campus and center.
                      
                     
                      In addition, we provide access to (25) software packages that can be accessed using
                        our  umApps  software service. To use this service, you need a computer, Internet access, and
                        an active account.
                      
                     
                      How Can I Stay "plugged in" to New and Trending Technologies? 
                     
                      Getting connected with us can help you stay informed about software updates and new
                        and trending technologies. We have several ways to stay connected:
                      
                     
                      
                        
                         
                           
                             Facebook  (Service Desk page)
                            
                           
                         
                        
                         
                           
                             Twitter  (SD feed)
                            
                           
                         
                        
                         
                           
                            Browsing websites like this and the  Service Desk  
                            
                           
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Student Success 
                         
                            
                               
                                Academic Integrity  
                                eCourseware Access  
                                Clickers  
                                Media Services  
                                Qualtrics  
                                Hardware Recommendations  
                                Web Conferencing  
                                Additional Services  
                            
                         
                      
                      
                      
                         
                            
                                Enter a Service Request  
                               Need Help with Technology? Request technical support via umHelpdesk, or search AskTom,
                                 a collection of questions and answers to technical problems.
                               
                            
                            
                                ITS Service Catalog  
                               Browse a collection of ITS services offered, learn how to access them and what Information
                                 is needed to fulfill service requests. 
                               
                            
                            
                                Preparing for Banner 9  
                               The UofM is upgrading to Banner 9. Keep up with the latest developments. 
                            
                            
                                Contact Us  
                               Contact the ITS Service Desk at 901.678.8888. (Available 24 hours a day, excluding
                                 posted holidays) Chat Live Monday - Friday 8:00 am - 7:00 pm CDT
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


