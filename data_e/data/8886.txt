Updating a subgroup in Wiggio | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Updating a subgroup in Wiggio | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   English  
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Wiggio    »    Managing groups in Wiggio   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Updating a subgroup in Wiggio                       
        
        
       
        
     
             
  Note For any group with subgroups, the group administrator is also the administrator for the subgroup(s). 
  Click   Group Settings  in the group header. 
	 In the Subgroups tab, click  edit  or  delete  for the subgroup you want to edit or delete. 
      Audience:     Learner       
    
         
               ‹ Deleting a group in Wiggio 
                     up 
                     Managing events in Wiggio › 
           
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Wiggio  
  
      Wiggio basics    Adding resources to Wiggio    Using polls in Wiggio    Using to-do lists in Wiggio    Creating and using folders in Wiggio    Creating and using groups in Wiggio    Managing groups in Wiggio     Editing settings for a group in Wiggio    Updating membership for a group in Wiggio    Changing the owner for a group in Wiggio    Deleting a group in Wiggio    Updating a subgroup in Wiggio      Managing events in Wiggio    Importing and exporting calendars in Wiggio    
                  
           
         

       
     

    
    
    
   
 
   
 
