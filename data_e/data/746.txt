Activities on Campus - UofM Lambuth - University of Memphis  Our intimate campus is a vibrant, fun place to be—made up of diverse groups. You will live, play and connect in a classic college environment.  










 
 
 
     



 
    
    
    Activities on Campus - 
      	UofM Lambuth
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			University of Memphis Lambuth
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Admissions  
                          Programs  
                          Campus Life  
                          Resources  
                          News  
                          Contact Us  
                      
                      
                         
                            Campus Life   
                            
                               
                                  
                                   Student Organizations  
                                   Campus Rec  
                                   On-Campus Housing  
                                   Meal Plan  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	UofM Lambuth
                              	  
                          
                              	Campus Life
                              	  
                         Activities on Campus 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                        
                     
                      Get involved. 
                     
                      Looking for ways to get involved on campus, meet other students, and gain leadership
                        experience? Join a student organization or attend an event on campus! See the link
                        below for a calendar of events for the spring semester.
                      
                      
                     
                       Alpha Epsilon Delta- Lambuth Satellite Chapter (AED Lambuth)  We are the satellite chapter of the pre-health honor society.
                      
                     
                       Campus Activities Board (CAB)  The Campus Activities Board is an organization, the sole purpose of which is to insure
                        that students have the most enjoyable college experience they can.
                      
                     
                       Kappa Delta Pi International Education Honor Society  
                     
                       Lambuth Fiction Society (LFS)  The LFS emphasizes the exploration and appreciation of created worlds.
                      
                     
                       Lambuth Student Government Council  This council serves part of the Student Government Association at UofM.
                      
                     
                       Model United Nations  Model UN offers students the opportunity to learn about the UN, international law,
                        diplomacy, and negotiation in preparation for a regional United Nations simulation.
                      
                     
                       Multicultural Student Association (MSA)  The Multicultural Student Association (MSA) provides detailed information on political,
                        social, and cultural affairs of International communities.
                      
                     
                       Pi Delta Mu (Pi D)  Promoting sisterhood, demonstrating excellence, and motivating women.
                      
                     
                       Psi Chi  National Honor Society in Psychology
                      
                     
                       Public Relations Student Society of America Lambuth (PRSSA Lambuth)  PRSSA is the foremost student organization for students interested in public relations
                        and communications. It seeks to help students develop professional skills and a code
                        of ethics that will advance their careers.
                      
                     
                       Soccer Club at the University of Memphis Lambuth (UMLS)  The Soccer Club at University of Memphis Lambuth is a Co-ed, recreational soccer club
                        for all skill levels to compete and have fun.
                      
                     
                       Sigma Theta Tau  International Nursing Honor Society
                      
                     
                       Student Nurses' Association - Memphis (SNA)  The Student Nurses' Association chapter at Loewenberg College of Nursing seeks to
                        support nursing students by providing mentorship, resources, activities, and leadership
                        opportunities to assist in their academic careers and beyond.
                      
                     
                       Student Tennessee Education Association (STEA)  
                     
                       The Black Student Association of the University of Memphis-Lambuth (BSA)  BSA of Lambuth provides an opportunity for students of all races to celebrate black
                        culture, lifestyle and history. BSA-Lambuth will foster student growth and development
                        through diversity, academics, and community service.
                      
                     
                       Tiger's Pep   Activities Council (TPAC)  The organization shall exist in order to plan and host spirit events on the campus
                        of the University of Memphis Lambuth.
                      
                     
                       UM Lambuth Biological Society  UM Lambuth Biological Society is an organization that encourages the exploration and
                        learning of different areas of the Science field. We consist of biology majors, but
                        any student at Lambuth is able to join upon request.
                      
                     
                       UM Lambuth Psychology Club (Psych Club)  This organization was formed to bring together students that have an interest in psychology.
                        This club is not specific to psychology majors or minors, and all students are welcome.
                      
                     
                       University of Memphis Lambuth Social Work Society (LSWS)  The LSWS strives to raise awareness of the Social Work Program at the University of
                        Memphis Lambuth Campus, while serving its community and giving social work majors
                        and other interested students an outlet in which to become involved on campus.  
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Campus Life 
                         
                            
                               
                                Student Organizations  
                                Campus Rec  
                                On-Campus Housing  
                                Meal Plan  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now  
                               Take the first step toward your new future 
                            
                            
                                Lambuth Academy  
                               High School Students: Learn more about dual enrollment 
                            
                            
                                News   Events  
                               See what's happening at Lambuth 
                            
                            
                                Contact Us  
                               Questions? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/27/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


