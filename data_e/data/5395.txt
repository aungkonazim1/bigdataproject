Understanding &quot;Question in Use&quot; | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Understanding  Question in Use  | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Quizzes    »    Managing quiz questions and sections   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Understanding  Question in Use                        
        
        
       
          
     
           Printer-friendly version       
	The  Question In Use  page displays when you save changes to a shared question or information item. You can select the check box beside each quiz, self assessment, survey, and the Question Library in which you want to apply your current changes and click  Save .
 
     Audience:    Instructor      

    
           

                   ‹ Managing quiz questions and sections 
        
                   up 
        
                   Editing quiz questions and sections › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Quizzes  
  
      Quizzes basics    Using Quizzes    Managing quiz questions and sections    Understanding  Question in Use     Editing quiz questions and sections    Previewing quiz questions    Reordering quiz questions and sections    Deleting quiz questions and sections    Importing quiz questions    Publishing questions and sections in Quizzes to a learning object repository    Importing questions from Respondus      Viewing quizzes    Quizzes and Question Library     
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
