Course Descriptions - Kemmons Wilson School of Hospitality &amp; Resort Management - University of Memphis    










 
 
 
     



 
    
    
    Course Descriptions - 
      	Kemmons Wilson School of Hospitality   Resort Management
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Kemmons Wilson School of Hospitality   Resort Management 
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Hospitality  
                          Sport   Leisure  
                          Students  
                          Careers  
                          Faculty  
                          Contact Us  
                      
                      
                         
                            Programs   
                            
                               
                                  
                                   Hospitality and Resort Management  
                                         Bachelor of Arts in Hospitality and Resort Management  
                                         Minor in Hospitality and Resort Management  
                                         Master of Arts in Liberal Studies (MALS)  
                                         Graduate Certificate in Liberal Studies  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Kemmons Wilson School of Hospitality   Resort Management
                              	  
                          
                              	Programs
                              	  
                         Course Descriptions 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Hospitality and Resort Management Minor Sequence 
                     
                        
                     
                       REQUIRED COURSES :
                      
                     
                       HPRM 1050 - Business of Hospitality (3)   Varied aspects of lodging, food service, clubs, cruise lines, natural attractions,
                           man-made   attraction, transportation, infrastructure; retail businesses; sports; special events
                           and activities;  outfitters, tour operators, travel management; destination marketing organizations.  
                     
                       HPRM 2050 - Food Production and Service (4)   Theory, application and understanding of safe food production methods and terminology;
                           culinary   hand tools and equipment operation including knife usage; principles and applied fundamentals
                           of   property service techniques and beverage handling. PREREQUISITE: HPRM 1050.  
                     
                       HPRM 2330 - Managing Hotel/Resort Ops (3)   Management of critical resources for running hotel and resort operation in effective
                           and efficient   manner; customer services, service quality, and service delivery.  
                     
                       HPRM 4320 - Hospitality Services Marketing (3)   Principles and models of services marketing with focus on applications to hospitality
                           services   industry; expansion of traditional marketing mix variables into additional development
                           of   hospitality service concepts, marketing plans, and service quality assessments. Permit
                           required.  
                     
                       HPRM 4620 - Hospitality Operational Analysis (3)   Management tools in analyzing operational effectiveness of hotels and resorts. PREREQUISITE:  HPRM 2330.  
                     
                       HPRM 4700 - Integrative Challenge (3)   Capstone course integrating academic and experiential learning through service learning
                           projects   building on leadership skills. This course is to be taken last. Permit required.  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Programs 
                         
                            
                               
                                Hospitality and Resort Management  
                                      Bachelor of Arts in Hospitality and Resort Management  
                                      Minor in Hospitality and Resort Management  
                                      Master of Arts in Liberal Studies (MALS)  
                                      Graduate Certificate in Liberal Studies  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply to the KWS Program  
                               Choose the degree program that's right for you 
                            
                            
                                Academic Advising  
                               Schedule an appointment with your advisor 
                            
                            
                                Research Highlights  
                               KWS faculty are leaders in their fields of study 
                            
                            
                                Contact Us  
                               Questions? Our team can help. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/21/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


