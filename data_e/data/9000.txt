Creating survey sections | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Creating survey sections | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Surveys    »    Using Surveys   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Creating survey sections                       
        
        
       
        
     
              
	Create survey sections to organize your questions into folders. You can do this in the Question Library or while creating a survey. Both of these areas are identical in functionality: one creates folders within the Question Library, while the other creates folders within a survey.
 

 
	You can also import sections from the Question Library directly into a quiz, survey, or self assessment. Importing a section transfers the section folder and any properties associated with the section (section name, messages, images, or feedback). You can still import all of the questions within a section separate from the section itself by creating the section within a quiz and clicking  Add/Edit Questions .
 

 
	 Note Although you can create subsections within sections, we recommend you keep question organization simple and intuitive.
 

 
	Create a section in surveys
 

  
		On the Manage Surveys page, click on the survey you want to create a section for.
	 
	 
		In the Properties tab, click  Add/Edit Questions .
	 
	 
		Click New, then select  Section .
	 
	 
		Enter a  Section Name .
	 
	 
		Enter a  Message . You can choose to display this message to users at the beginning of the section or repeat it before each question in the section (refer to Step 8).
	 
	 
		Enter  Private Comments . These comments are for your personal use and view only.
	 
	 
		Click  Add a File  and select the image you want to use. You can choose to display this image at the beginning of the section or repeat it before each question in the section (refer to Step 8).
	 
	 
		Set your  Display Options :
		  
				 show section name  Displays the section name in surveys that contain this section.
			 
			 
				 insert a line break after section name  Inserts extra space below the section name.
			 
			 
				 display message and image 
			 
			 
				  
						Select  display section message and image once  to display these items once at the beginning of the section.
					 
					 
						Select  repeat section message and image before each question  to repeat both the message and image before each question in the section.
					 
				  
		  
	 
		Click  Expand section feedback  to enter section feedback and comments.
	 
	 
		Click  Save .
	 
  
	 Note Managing survey sections (renaming, reordering, publishing, deleting) is similar to managing survey questions.
 
     Audience:     Instructor       

    
           

                   ‹ Creating surveys 
        
                   up 
        
                   Editing surveys › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        Surveys  
  
      Participating in Surveys    Using Surveys    Accessing Question Library    Creating survey questions    Creating surveys    Creating survey sections    Editing surveys    Copying surveys    Previewing surveys    Reordering surveys    Deleting surveys    Creating and managing survey categories    Setting survey branching    Associating surveys with learning objectives      Managing survey questions and sections    Viewing surveys    
                  
           
         

       
     

    
    
    
   
 
   
 
