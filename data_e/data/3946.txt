Benjamin L. Hooks Institute for Social Change Staff - Ben Hooks - University of Memphis    










 
 
 
     



 
    
    
    Benjamin L. Hooks Institute for Social Change Staff - 
      	Ben Hooks
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			The Benjamin L. Hooks Institute for Social Change
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Programs  
                          Events  
                          HAAMI  
                          Documentaries  
                          Collections  
                          About  
                          Newsroom  
                      
                      
                         
                            HAAMI   
                            
                               
                                  
                                   HAAMI  
                                   Join HAAMI  
                                   Events  
                                   Newsroom  
                                   Support Team  
                                   Make a gift to the Hooks Institute  
                                   Contact Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Ben Hooks
                              	  
                          
                              	HAAMI
                              	  
                         Benjamin L. Hooks Institute for Social Change Staff 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Hooks Institute Staff 
                     
                       
                             Daphene R. McFerren, J.D.   Executive Director  
                     
                       Contact:  drmcfrrn@memphis.edu   
                     
                      Daphene R. McFerren is the executive director of the Benjamin L. Hooks Institute for
                        Social Change. McFerren manages the Hooks Institute's strategic planning, oversees
                        program creation and implementation, leads local and national fundraising efforts,
                        monitors program expenses, and administers all other work of the Hooks Institute.
                        Under her leadership, the Hooks Institute has increased its focus on innovative ways
                        to promote civil discourse on civil and human rights and expanded impactful community
                        service programs. McFerren served as executive director on several Hooks Institute
                        documentary films including   Duty of the Hour   (2016),   a documentary on the life of civil rights activist Benjamin L. Hooks. McFerren is
                        a 1995 National Endowment for the Humanities Summer Research Award recipient. Prior
                        to joining the Hooks Institute, McFerren litigated complex civil litigation matters
                        in private practice and with the U.S. Department of Justice. She is also a former
                        federal prosecutor and secured convictions in human trafficking cases of national
                        and international interest. From 1999 to 2001, McFerren served as counsel to the Honorable
                        Janet Reno, Attorney General of the United States.
                      
                     
                      
                     
                        Rorie Trammel   Assistant Director | HAAMI Coordinator  
                     
                       Contact:  rtrammel@memphis.edu   
                     
                      Rorie Trammel is the assistant director of the Benjamin L. Hooks Institute for Social
                        Change. Trammel plays an integral role in the activities of the Hooks Institute including
                        administrative and operations duties, fundraising and donor relations, and coordination
                        of the Institute's National Book Award. Trammel, also, oversees strategic planning
                        and implementation of the Hooks African American Male Initiative (HAAMI). She earned
                        her bachelor's and master's degrees from the University of Memphis (UofM). She is
                        also a former UofM employee, having worked in the Office of Development for fourteen
                        years. Rorie worked for the YMCA of Memphis   the Mid-South for fourteen and a half
                        years, first as executive director of urban programming and later as vice president
                        for advancement. For many years, Rorie could be heard as a volunteer radio reader
                        for WYPL, the radio station at the Benjamin L. Hooks Central Library. She is a member
                        of the New Memphis Institute and, previously, served on the boards of directors for
                        Partners in Public Education (PIPE), the Association of Fundraising Professionals,
                        Le Bonheur Center for Children and Parents, and the Shelby Farms Park Conservancy.
                      
                     
                      
                     
                        M. Elena Delavega, PhD   Associate Director | HAAMI Program Evaluator  
                     
                       Contact:  mdlavega@memphis.edu   
                     
                      Elena Delavega, PhD, MSW is an assistant professor at the Department of Social Work
                        at the University of Memphis (UofM), where she teaches social welfare policy, advanced
                        community practice, and poverty. Her research is complex and multifaceted and consists
                        of three broad areas including understanding poverty, social and economic exclusion,
                        and promoting social and economic development. Delavga evaluates the research component
                        of the  Hooks African American Male Initiative , heads the  Hooks Institute's Faculty Advisory Board , assists in Hooks Institute grants, and plays an integral role in the writing and
                        publication of the Hooks Institute's Policy Papers.
                      
                     
                      
                     
                        Nathaniel C. Ball, MA   Media and Programs Coordinator  
                     
                       Contact:  ncball@memphis.edu   
                     
                      Nathaniel C. Ball, MA started as the media and programs coordinator at the Hooks Institute
                        in June 2015. Ball assists the Hooks Institute executive director and staff in the
                        creation, planning, and implementation of Hooks Institute programs. Ball plays a major
                        role in the planning, writing, design, and implementation of Hooks Institute social
                        media accounts, websites,  newsletters ,  press releases , and other materials. He is heavily involved in the production of the Hooks Institute
                        films, where he has produced, written, and/or edited several documentaries and short
                        films. Currently, Ball is producing, researching, and writing the Hooks Institute's
                        upcoming  documentary film on civil and women's rights activist Ida B. Wells , set for a premiere in February 2018. Ball coordinates the Hooks Institute grant
                        priorities, and assists or authors Hooks Institute grants. Ball earned a BA in Communications
                        with a focus on Film and Video Production (2011) and an MA in History (2015) from
                        the University of Memphis.
                      
                     
                      
                     
                       Pamela DeShields      Administrative Coordinator  
                     
                       Contact:  pdeshlds@memphis.edu   
                     
                      Pamela Deshields began her journey with the University of Memphis in 2002, as a Secretary
                        for the College of Education. During her term at the University of Memphis, Pamela
                        was afforded the opportunity to work in various areas where she was able to build
                        relationships and become more efficient in her interactions with faculty, staff, and
                        the student body. Pamela was first introduced to the Hooks Institute as a volunteer
                        for their 2015 Annual Gala. She was truly intrigued by their works and passion for
                        the HAAMI program. Pamela joined the Hooks Institute, November 2016, fulfilling the
                        role of Administrative Coordinator. Through her creative design skill set, she assists
                        in the creation of outreach marketing materials that promote awareness of Hooks Institute
                        programs in the community. Pamela is a first generation college graduate and was awarded
                        her BPS in Organizational Leadership from the University of Memphis in May 2017.
                      
                     
                      
                     
                        David A. Rose, II   HAAMI Program Services Specialist  
                     
                       Contact:  darose1@memphis.edu   
                     
                      David Rose joined the Hooks Institute in August 2017. David has worked closely with
                        the UofM's CREWS Center for Entrepreneurship and developed a hybrid implantable cardioverter
                        defibrillator while participating in BioWorks Foundation Zero to 510 medical accelerator
                        initiative. David graduated from the University of Memphis in May 2017. David also
                        heads up his own nonprofit to empower youth. David served on the Hooks 2017 Gala Steering
                        Committee and was a volunteer session leader in the spring for HAAMI.
                      
                     
                      
                     
                        Gregory Washington, PhD   HAAMI Advisor  
                     
                       Contact:  gwshngt1@memphis.edu   
                     
                      Gregory Washington, PhD, LCSW, is an associate professor in the Department of Social
                        Work at the University of Memphis and director of the Center for the Advancement and
                        Youth Development (CAYD). He is also a licensed clinical social worker (LCSW) that
                        works as a clinical consultant. He has practiced as an individual, family, and group
                        therapist in Illinois, Georgia, Arkansas, and Tennessee. His research interests include
                        culturally centered empowerment methods, and the risk and protective factors associated
                        with youth development. A major goal of Washington's work is to identify and promote
                        the use of innovative culturally centered group interventions that reduce the risk
                        for disparities in behavioral health and incarceration outcomes among young people
                        of color.
                      
                     
                      
                     
                        Jasmine Stansberry   Graduate Assistant  
                     
                       Contact:  jpstnsbr@memphis.edu   
                     
                      Jasmine Stansberry is a graduate student, currently pursuing a Masters in History
                        at the U of M. Her interests are Twentieth century African-American history, with
                        an emphasis on the Civil Rights and Black Power movements. Jasmine is also a member
                        of GAAAH (Graduate Association for African-American History) at the U of M, and will
                        be working with the Lynching Sites Project of Memphis to help survey civil rights
                        era cold cases. Working with the community is of great importance to her.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         HAAMI 
                         
                            
                               
                                HAAMI  
                                Join HAAMI  
                                Events  
                                Newsroom  
                                Support Team  
                                Make a gift to the Hooks Institute  
                                Contact Us  
                            
                         
                      
                      
                      
                         
                            
                                Make a gift to the Hooks Institute  
                               Join us as we work to uplift Memphis and uplift the nation. Make a gift of any amount
                                 to the Hooks Institute.
                               
                            
                            
                                Critical Conversations  
                               Where is your voice? A University-wide initiative to promote and sustain an inclusive
                                 and democratic society.
                               
                            
                            
                                Benjamin L. Hooks Papers Website  
                               Explore the Civil Rights Movement and beyond through the eyes of Benjamin L. Hooks. 
                            
                            
                                2017 Hooks Institute Policy Papers  
                               Read the 2017 Hooks Institute Policy Papers Online! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


