Security Awareness Training - ITS - University of Memphis    










 
 
 
     



 
    
    
    Security Awareness Training - 
      	ITS
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Information Technology Services
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          umTech  
                          Security  
                          Governance  
                          Resources  
                      
                      
                         
                            Security   
                            
                               
                                  
                                   Report a Security Incident  
                                   Alerts and Warnings  
                                         Current Alerts  
                                         Phishing  
                                         Personal Information Security  
                                         FTC Phone Call Scams Site  
                                         FBI Internet Fraud Site  
                                     
                                  
                                   Duo Account Security  
                                   Policies and Guidelines  
                                   Security Best Practices  
                                   Security Awareness Training  
                                   Access Termination  
                                   Web Security Guidelines  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	ITS
                              	  
                          
                              	Security
                              	  
                         Security Awareness Training 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Security Awareness Training 
                     
                      Training provides important information to clients to educate them about potential
                        security issues and also helps to provide an awareness of University security policies.
                         Information Technology Services (ITS) offers the following security awareness training:
                      
                     
                      IT SECURITY AWARENESS TRAINING 
                     
                      Certain University employees are required to complete security awareness training
                        in order to receive or retain access to University systems or data. ITS has selected
                        Securing the Human by the SANS Institute for our online security awareness training.
                        Employees will be notified if they are required to complete any training modules.
                      
                     
                          Click  here  to access the online IT security awareness training modules using your UofM single
                                    sign-on credentials.     
                     
                      face-to-face Training sessions 
                     
                      ITS also offers face-to-face security awareness training to University employees by
                        request.  During these sessions, the University's IT security policies are reviewed
                        and opportunities are provided for security-related questions and answers.  Please
                        contact  securitytraining@memphis.edu  if you are interested in scheduling a session.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Security 
                         
                            
                               
                                Report a Security Incident  
                                Alerts and Warnings  
                                      Current Alerts  
                                      Phishing  
                                      Personal Information Security  
                                      FTC Phone Call Scams Site  
                                      FBI Internet Fraud Site  
                                  
                               
                                Duo Account Security  
                                Policies and Guidelines  
                                Security Best Practices  
                                Security Awareness Training  
                                Access Termination  
                                Web Security Guidelines  
                            
                         
                      
                      
                      
                         
                            
                                Security Information  
                               Review security policies and guidelines 
                            
                            
                                Need Help with Technology?  
                               Request technical support via UMhelpdesk 
                            
                            
                                Service Catalog   
                               Review the various solutions provided by ITS 
                            
                            
                                Contact Us  
                               For technical assistance, contact the umTech Service Desk: 100 Administration Bldg.,
                                 901.678.8888. To discuss other topics, contact the Office of the CIO: 901.678.8324
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


