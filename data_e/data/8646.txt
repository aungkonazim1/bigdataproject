About new award notifications | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 About new award notifications | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   English  
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Awards    »    Awards   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          About new award notifications                       
        
        
       
          
     
           Printer-friendly version       
	When you earn a new award, you are notified in Brightspace Learning Environment, and depending on how your instructor has configured the notification, you may also get a notification email.
 

 
	When you log into the course offering in which you earned the new award, a popup appears to congratulate you. You can click View Awards to open the My Awards tool and view all your awards. If you have earned more than one award, a Next button appears on the pop-up so you can move ahead to view subsequent awards.
 

 
	Subscriptions alerts on the minibar also displays a notification about new awards.
 
     Audience:    Learner      
    
         
               ‹ About My Awards 
                     up 
                     Sharing earned awards › 
           
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Awards  
  
      About Awards    Create a new award    Add release conditions to an award    Hide an award from the Available Awards page    Add an existing award to a course    View awards for a classlist    Manually grant an award    Revoke an award    Troubleshooting: Certificate text is too long    About My Awards    About new award notifications    Sharing earned awards    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
