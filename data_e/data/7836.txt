Hooks Institute Documentaries - Ben Hooks - University of Memphis    










 
 
 
     



 
    
    
    Hooks Institute Documentaries - 
      	Ben Hooks
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			The Benjamin L. Hooks Institute for Social Change
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Programs  
                          Events  
                          HAAMI  
                          Documentaries  
                          Collections  
                          About  
                          Newsroom  
                      
                      
                         
                            Documentaries   
                            
                               
                                  
                                   Duty of the Hour  
                                   Ida B. Wells in Memphis  
                                   Freedom's Front Line  
                                   Memphis 13  
                                   A Cultural Revolution  
                                   Support the Hooks Institute  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Ben Hooks
                              	  
                         
                           	Documentaries
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Hooks Institute Documentaries 
                     
                        
                     
                      Overview 
                     
                      One of the best ways to carry out the Hooks Institute's mission of teaching, studying,
                        and promoting civil rights and social change is to hear the diverse voices of those
                        who demonstrated courage and perseverance while demanding a more just nation. The
                        Hooks Institute played a pivotal role in creating, funding, and supporting the documentaries
                        featured here.
                      
                     
                      You hear the voices of seasoned activists, including Dr. Benjamin L. Hooks, Julian
                        Bond, Maxine Smith, and Russell Sugarmon; the voices of those who were children, but
                        were called upon to dismantle adult practice of segregation; national and local civil
                        rights historian; and government officials including President Jimmy Carter, Senator
                        Lamar Alexander, and Congressman Steve Cohen. These voices are a reminder of a past
                        not to be repeated, the stories of heroes against all odds and a vision of a more
                        just future to be created.  For more information on the Hooks Institute's documentaries, please go to the upper
                        left navigational bar and click on the names of the films.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Documentaries 
                         
                            
                               
                                Duty of the Hour  
                                Ida B. Wells in Memphis  
                                Freedom's Front Line  
                                Memphis 13  
                                A Cultural Revolution  
                                Support the Hooks Institute  
                            
                         
                      
                      
                      
                         
                            
                                Make A Gift  
                               Join us as we work to uplift Memphis and uplift the nation. Make a gift of any amount
                                 to the Hooks Institute.
                               
                            
                            
                                Critical Conversations  
                               Where is your voice? A University-wide initiative to promote and sustain an inclusive
                                 and democratic society.
                               
                            
                            
                                Benjamin L. Hooks Papers Website  
                               Explore the Civil Rights Movement and beyond through the eyes of Benjamin L. Hooks. 
                            
                            
                                2017 Hooks Institute Policy Papers  
                               Read the 2017 Hooks Institute Policy Papers Online! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/4/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


