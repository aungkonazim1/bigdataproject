Advocacy Certificate - School of Law - University of Memphis  Advocacy Certificate  










 
 
 
     



 
    
    
    Advocacy Certificate - 
      	School of Law 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Cecil C. Humphreys School of Law
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Admissions  
                          Programs  
                          Current Students  
                          Faculty  
                          Careers  
                          Library  
                      
                      
                         
                            Programs   
                            
                               
                                  
                                   Degree Programs  
                                         JD  
                                         JD/MBA  
                                         JD/MA in Pol. Science  
                                         JD/MPH  
                                         MPH Fast-Track  
                                         Part-time Program  
                                     
                                  
                                   Certificate Programs  
                                        
                                         Advocacy  
                                        
                                         Business Law  
                                        
                                         Health Law  
                                        
                                         Tax Law  
                                     
                                  
                                   Experiential Learning  
                                         Experiential Learning Home  
                                         Legal Clinics  
                                         Externship Program  
                                     
                                  
                                   Law Review  
                                         Law Review Home  
                                         Editorial Board  
                                         Editor-in-Chief  
                                         Joining Law Review  
                                         Virtual Tour of Law Review Suite  
                                         Symposium  
                                         Subscriptions  
                                         Archives  
                                         Current Volume  
                                         Connect with Us  
                                     
                                  
                                   Advocacy Program  
                                         Moot Court Board  
                                         Joining Moot Court Board  
                                         In-School Competitions  
                                         Travel Team Competitions  
                                         Past Champions  
                                     
                                  
                                   Inst. for Health Law   Policy  
                                         Institute Home   
                                         Advisory Committee  
                                         Symposium  
                                         Pro Bono Opportunities  
                                         Health Law Certificate  
                                         Health Law Society  
                                         MBA Health Law Section  
                                     
                                  
                                   International Law Programs  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	School of Law 
                              	  
                          
                              	Programs
                              	  
                         Advocacy Certificate 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      CERTIFICATE IN ADVOCACY 
                     
                        
                     
                      The goal of the Certificate in Advocacy program is to enable students who are interested
                        in a career in litigation or the study of advocacy to follow a specialized course
                        of study, to work closely with other students interested in the area and to receive
                        guidance from faculty members with an interest and expertise in the area.
                      
                     
                      This program will enable you to work alongside other students and professors to prepare
                        you for a career in litigation. A student who receives the Certificate in Advocacy
                        demonstrates knowledge of fundamental principles of trial and appellate advocacy,
                        as well as competence in the skills essential to a career in litigation. In turn,
                        Certificate in Advocacy graduates are attractive to law firms seeking to hire litigation
                        attorneys who enter the practice of law with practical training.
                      
                     
                      For more information about the Certificate in Advocacy program, please contact Prof.
                        Barbara Kritchevsky at  bkrtchvs@memphis.edu  or (901) 678-3239.
                      
                     
                      Curriculum 
                     
                      Students in the Certificate in Advocacy Program not only take courses that teach legal
                        rules and principles in a traditional classroom setting but also put those ideas into
                        practice through client representation, skills courses and participation in Moot Court,
                        Mock Trial and Alternative Dispute Resolution (ADR) travel teams. The out of class
                        component enables students to work with practicing attorneys and make contacts in
                        the legal community.
                      
                     
                      Course Requirements 
                     
                      A student must successfully complete at least 15 hours of advocacy courses. At least
                        2 hours of study must be in trial advocacy courses and 2 hours must be in appellate
                        advocacy courses
                      
                     
                      Non-class Requirements 
                     
                      Students seeking the certificate must also complete work outside the classroom, allowing
                        them to gain invaluable experience and see advocacy in action. A student must complete
                        25 hours of non class work in the advocacy field. The student must complete at least
                        5 hours each semester. Students in the program must keep a log of their activities
                        in accordance with the Director of Advocacy's guidelines and must attend one meeting
                        each semester with other students enrolled in the Certificate of Advocacy program.
                      
                     
                      Grade Point Requirement 
                     
                      To satisfy the requirements of the Certificate in Advocacy, each student must demonstrate
                        a successful understanding of the fundamentals of advocacy by receiving a grade of
                        at least a C and obtaining an overall GPA of at least 2.5 in the building-block courses
                        of Legal Methods I and II, Civil Procedure I and II, Evidence and Professional Responsibility.
                        Students must receive at least a 3.0 GPA in all courses taken to satisfy the certificate.
                      
                     
                      To receive the certificate with honors, a student must complete the graded courses
                        to satisfy the certificate with a GPA of 3.5 or higher and receive a grade of Excellent
                        in at least two-thirds of the non-graded coursework taken to satisfy the certificate
                        requirements.
                      
                     
                      How to Enroll 
                     
                      Students in good academic standing may enroll in the Certificate for Advocacy program
                        after completing one year of full-time law study (or 30 hours for part-time students).
                        A student may not enroll after the add deadline in the student's fourth semester of
                        full-time study (or the semester after a part-time student has completed 45 hours
                        of study).
                      
                     
                      A student enrolls by submitting a completed  enrollment form  to the Director of Advocacy. In completing the form, the student must certify that
                        he/she can satisfy the requirements of the certificate program and is committed to
                        fulfilling its requirements. A student will be dropped from the program if he/she
                        fails to meet the requirements.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Programs 
                         
                            
                               
                                Degree Programs  
                                      JD  
                                      JD/MBA  
                                      JD/MA in Pol. Science  
                                      JD/MPH  
                                      MPH Fast-Track  
                                      Part-time Program  
                                  
                               
                                Certificate Programs  
                                     
                                      Advocacy  
                                     
                                      Business Law  
                                     
                                      Health Law  
                                     
                                      Tax Law  
                                  
                               
                                Experiential Learning  
                                      Experiential Learning Home  
                                      Legal Clinics  
                                      Externship Program  
                                  
                               
                                Law Review  
                                      Law Review Home  
                                      Editorial Board  
                                      Editor-in-Chief  
                                      Joining Law Review  
                                      Virtual Tour of Law Review Suite  
                                      Symposium  
                                      Subscriptions  
                                      Archives  
                                      Current Volume  
                                      Connect with Us  
                                  
                               
                                Advocacy Program  
                                      Moot Court Board  
                                      Joining Moot Court Board  
                                      In-School Competitions  
                                      Travel Team Competitions  
                                      Past Champions  
                                  
                               
                                Inst. for Health Law   Policy  
                                      Institute Home   
                                      Advisory Committee  
                                      Symposium  
                                      Pro Bono Opportunities  
                                      Health Law Certificate  
                                      Health Law Society  
                                      MBA Health Law Section  
                                  
                               
                                International Law Programs  
                            
                         
                      
                      
                      
                         
                            
                                Apply to Memphis Law  
                               Take the first step toward your legal education 
                            
                            
                                ABA Required Disclosures  
                               Information required by ABA Standard 509 Required Disclosures 
                            
                            
                                Alumni   Support  
                               Stay up to date with Memphis Law alumni 
                            
                            
                                Contact Us  
                               Questions about law school? We can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/11/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


