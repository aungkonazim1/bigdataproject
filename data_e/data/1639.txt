School of Nursing - Graduate School - University of Memphis    










 
 
 
     



 
    
    
    School of Nursing  - 
      	Graduate School 
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Graduate School
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Future Students  
                          Current Students  
                          Degrees  
                          News   Events  
                          Resources  
                          Contact Us  
                      
                      
                         
                            Resources   
                            
                               
                                  
                                   Graduate School Calendar  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Graduate School 
                              	  
                          
                              	Resources
                              	  
                         School of Nursing  
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Loewenberg College of Nursing 
                     
                      The University bestows the designation "Graduate Faculty" upon faculty following review
                        of their credentials and recommendation by their colleagues. The University maintains
                        six levels of graduate faculty: (1) Full, (2) Associate, (3) Adjunct, (4) Adjunct
                        Research Co-Mentor, (5) Affiliate, and (6) Adjunct Teaching.
                      
                     
                      Only Full graduate faculty members may chair doctoral committees. Adjunct Research
                        Co-Mentor members may serve as co-chair of a master's or doctoral committee. Full
                        or Associate graduate faculty may chair master's committees. Only one adjunct or affiliate
                        graduate faculty member may serve as a voting member on any master's or doctoral committee.
                      
                     
                      Teaching adjunct members may not serve on graduate committees. 
                     
                      
                     
                      Full MEMBERS 
                     
                      L. ANTOINETTE BARGAGLIOTTI, Professor, PhD (1984), University of California at San
                        Francisco [2019]
                      
                     
                      TRACY COLLINS, Associate Professor, DNP (2014), The University of Alabama at Tusculoosa
                        [2023]
                      
                     
                      JILL DAPREMONT, Associate Professor, EdD (2008), Argosy University [2018] 
                     
                      SHIRLEATHA T. LEE, Associate Professor, PhD (2009), The University of Tennessee Health
                        Science Center [2022]
                      
                     
                      ANNAPOORNA MARY, Assistant Professor, PhD (2007), University of Tennessee [2023] 
                     
                      LESLIE MCKEON, Associate Professor, PhD (2004), University of Tennessee [2020] 
                     
                      GAYLE H. SHIBA, Associate Professor, DNSc (1997), The University of California-San
                        Francisco [2015]
                      
                     
                      BEVERLY WEST, Assistant Professor, PhD (2017) Capella University [2023] 
                     
                      LIN ZHAN, Professor, PhD (1993), Boston College [2018] 
                     
                      
                     
                      ASSOCIATE MEMBERS 
                     
                      GLORIA CARR, MSN (1997), The University of Tennessee Health Science Center, Memphis
                        [2023]
                      
                     
                      HOI SING CHUNG, PhD (2001) Chinese University of Hong Kong [2023] 
                     
                      BELINDA FLEMING, Assistant Professor, PhD (2009), The University of Tennessee Health
                        Science Center [2022]
                      
                     
                      BRAD HARRELL, Associate Professor, PhD (2009), The University of Tennessee Health
                        Science Center [2022]
                      
                     
                      MARJORIE F. LUTTRELL, Professor, PhD (1986), The University of Texas at Austin [2015] 
                     
                      SARAH MILLER, Assistant Professor, PhD (2007), University of Florida [2020] 
                     
                      TERESA RICHARSON, Clinical Associate Professor, DNP (2010), Vanderbilt University,
                        School of Nursing [2022]
                      
                     
                      CAROLYN SIMONTON, Associate Professor, DNP (2001), University of Tennessee Center
                        for Health Sciences [2022]
                      
                     
                      GENAE D. STRONG, PhD (2005], University of Tennessee-Memphis [2023] 
                     
                      
                     
                        
                     
                      ADJUNCT MEMBERS 
                     
                      RACHEL BARBER, DNP (2014), Union University [2019] 
                     
                      LORI FERGUSON, PharmD (2004), University of Tennessee, College of Pharmacy [2017] 
                     
                      LINDA FINCH, PhD (2002), University of Memphis [2019] 
                     
                      COURTNEY GOODE, DNP  (2016), Union University 
                     
                      TRACIE L. MOORE, DNP (2010), University of Tennessee Health Science Center [2017] 
                     
                      CATHERIN MURPHY, MBA (1998), Anna Maria College [2020] 
                     
                      LEIGH POWERS, DNP (2011), Vanderbilt University [2015] 
                     
                      LOIS WAGNER, PhD (2004), Union University [2019] 
                     
                      
                     
                      ADJUNCT TEACHING MEMBERS 
                     
                      ASHLEA ANDERSON, MSN (2009), The University of Memphis [2016] 
                     
                      LEIGH BRECKENRIDGE, MSN (1994), University of Kentucky [2018] 
                     
                      PATRICIA GAUNTLETT BEARE, PhD (1978), North Texas State University [2016] 
                     
                      CORY M. BUNCH, MSN (2005), University of Memphis [2016] 
                     
                      JANE THOMAS CASH, PhD (1988), University of Alabama [2016] 
                     
                      SARA DAY, PhD (2010), University of Tennessee Health Science Center [2015] 
                     
                      SUSAN P. FERGUSON, MS (2008), Warren National University [2017] 
                     
                      WANDA HARRELL, MS (1977), University of Tennessee [2019] 
                     
                      DEBRA HENDREN, DNP (2012), Union University Jackson [2019] 
                     
                      LAWANDA HERRON, PhD (2007), University of Mississippi [2014] 
                     
                      JUDITH JENKINS, MS (1974), Boston University [2017] 
                     
                      CATHERINE A. LIPSEY, MSN (2005), The University of Memphis [2018] 
                     
                      LAURA LONG, MSN (2010), The University of Memphis [2015] 
                     
                      SAMUEL LOUIS MACERI, DNSc (2002), The University of Tennessee Health Science Center,
                        Memphis [2016]
                      
                     
                      BELINDA MANDRELL, PhD (2008), University of Tennessee [2015] 
                     
                      MELINDA MANZO, MSN (2006), The University of Memphis [2019] 
                     
                      CAROLYN RUTH MARCUM, MSN (2007), University of Memphis [2018] 
                     
                      ANGELA HOPE MILLER, MSN (2001), Vanderbilt University [2016] 
                     
                      LAUCHLAND ROBERTS, Pharm.D. (2009), Xavier University [2016] 
                     
                      STACY M. SCREWS, MA (2006), Arkansas State University [2019] 
                     
                      KEISHA SMITH, Assistant Professor, MSN (2007) University of Memphis [2016] 
                     
                      TERESA STRICKLAND, MSN (2009), University of Memphis [2017] 
                     
                      RENITA TALLEY, MS (2007), Middle Tennessee State University [2015] 
                     
                      CHRISLA TIDWELL, DNP (2011), University of Tennessee Health Science Center [2016] 
                     
                      SHAYLA WILLIAMSON, MSN (2005) University of Memphis [2018] 
                     
                      TINA WOODS, MSN (2005), University of Pennsylvania [2017] 
                     
                      
                     
                      AFFILIATE MEMBERS 
                     
                      REBECCA ADKINS, Assistant Clinical Professor, MSN (1997), University of Tennessee
                        [2020]
                      
                     
                      LISA D. BEASLEY, Associate Professor, PhD (2015) University of Alabama [2020] 
                     
                      KATHY BUTLER, Assistant Clinical Professor, DNP (2010), Oakland University [2017] 
                     
                      MARY C. ELLIOTTE, MSN (2005), University of Memphis [2017] 
                     
                      NORMA HUNT, MSN (2006), The University of Memphis [2019] 
                     
                      FELESHA PERRY, DNP (2016), University of Alabama Capstone College of Nursing [2019] 
                     
                      CANDACE MCGOWEN, MSN (2009), University of Memphis [2020] 
                     
                      
                     
                       NOTE:  Expiration date of Graduate Faculty membership is indicated in brackets.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Resources 
                         
                            
                               
                                Graduate School Calendar  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now!  
                               Take the first step toward your advanced degree. 
                            
                            
                                Degree Programs  
                               Explore our graduate catalog. 
                            
                            
                                Support Graduate Education  
                               Your gift makes a difference! 
                            
                            
                                Contact Us  
                               Questions? The Grad School Staff can help! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


