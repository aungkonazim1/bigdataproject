Managing discussion subscriptions | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Managing discussion subscriptions | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Discussions    »    Following discussions   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Managing discussion subscriptions                       
        
        
       
          
     
           Printer-friendly version       
	You can make changes to your subscriptions in  Notifications  and in the  Subscriptions  area of Discussions.
 

 
	Change default frequency of notifications and email address
 

  
		Do one of the following:
		  
				Click  Subscriptions  from the tool navigation in Discussions, then click  Change your notification settings .

				 
					 Note   You must be subscribed to at least one forum, topic, or thread to view this option.
				 
			 
			 
				Click  Notifications  from the personal menu on the minibar.
			 
		  
	 
		Modify your  Notification Frequency  setting and email address.
	 
	 
		Click  Save .
	 
  
	Manage subscriptions for forums, topics, or threads
 

  
		Click  Subscriptions  from the tool navigation.
	 
	 
		Modify your  Default Notification Method  settings, or unsubscribe from any forum, topic, or thread you do not want to follow.
	 
  
	See also
 

  
		 Enabling notifications in Discussions 
	 
      Audience:    Learner      

    
           

                   ‹ Marking discussion threads and posts as read or unread 
        
                   up 
        
                   Subscribing and unsubscribing to discussion forums, topics, and threads › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Discussions  
  
      Participating in discussions    Following discussions    Flagging a discussion post    Marking discussion threads and posts as read or unread    Managing discussion subscriptions    Subscribing and unsubscribing to discussion forums, topics, and threads    Enabling notifications in Discussions      Creating and managing discussions    Monitoring discussions    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
