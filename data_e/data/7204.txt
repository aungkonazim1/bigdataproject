Copy a predictive model from a past course | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Copy a predictive model from a past course | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   English  
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Insights    Integrations    LeaP    Learning Environment    Learning Repository   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Insights    »    Understanding the Student Success System   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Copy a predictive model from a past course                       
        
        
       
          
     
           Printer-friendly version        
		From the    Admin Tools  menu, click  Student Success System .
	 
	 
		Click  Add Course  or select the course you want to configure a predictive model for.
	 
	 
		From the context menu of the course you want to work with, click  Configure Model .
	 
	 
		Click  Copy Model Configuration .
	 
	 
		Select the past course with the model configuration you want to copy.
	 
	 
		Optionally, select the check box  Include the selected course as an additional historic course .
	 
	 
		Click  Copy .
	 
	 
		Click  Save and Continue .
	 
      Audience:    Instructor      
    
         
               ‹ Configure a predictive model for a course 
                     up 
                     Students at Risk Widget › 
           
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Insights   
  
      Using Insights Portal    Standard reports    Data Mining reports    Understanding the Student Success System    Student Success System    Accessing the Student Success System class dashboard    Student Success System domains    Understanding the class dashboard    Understanding the student dashboard    Configure a predictive model for a course    Copy a predictive model from a past course    Students at Risk Widget      
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
