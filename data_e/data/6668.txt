Customized Training - UMCE - University of Memphis    










 
 
 
     



 
    
    
    Customized Training - 
      	UMCE
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Professional and Continuing Education
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Customized Training  
                          Driver Programs  
                          Leader Excellence  
                          Registration  
                          About Us  
                      
                      
                         
                            Customized Training   
                            
                               
                                  
                                   Meet Some of Our Team  
                                   Customized Training Courses  
                                   Client Testimonials  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	UMCE
                              	  
                         
                           	Customized Training
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Customized Training 
                     
                       Increase Your Company's Potential!  
                     
                      Customized Training Programs 
                     
                      for Businesses, Industry, and Government 
                     
                       The Training You Need... When You Need It... Where You Need It  
                     
                      Companies with a competitive edge have learned to work smarter. They know that a highly-trained
                        workforce is essential in today's marketplace. The University of Memphis Customized
                        Training program is dedicated to helping corporate clients gain the professional advantage.
                        We offer responsive, innovative, cutting-edge solutions that meet your specific training
                        needs.
                      
                     
                      Our Customized Training... 
                     
                      ... establishes partnerships with business and industry to develop training and education
                        courses unique to the organization's specific needs. Since 2000, the UofM has been
                        one of the leading providers of practical training solution. We have been helping
                        businesses and organizations throughout the Mid-South stay informed and educated about
                        advances and best practices pertinent to workplace skills, leadership, organization
                        sustainability and business solutions.. A customized training program can take your
                        company — and your employees — to the next level.
                      
                     
                      ... is one of the most affordable and effective ways to provide your staff with the
                        leadership and professional development training they need to help sharpen your company's
                        competitive edge.
                      
                     
                      ... enables organizations to achieve their performance goals by developing learning
                        solutions, which add value and reduce costs. Whether your organization is incurring
                        large training expenses or experiencing long training cycles, our staff will develop
                        customized training solutions that address the challenges in your business.
                      
                     
                      We offer what you value in a training partner: 
                     
                      
                        
                         an established reputation as a leader in education. 
                        
                         a strong record of workforce development through quality training programs. 
                        
                         instructors with industry experience and practical knowledge, in their areas of expertise. 
                        
                         focus on quality and effectiveness of the teaching and learning process over profit. 
                        
                         state-of-the-art facilities (or we can bring the training to you!). 
                        
                         comprehensive services and program packages that offer the best training value. 
                        
                      
                     
                       When   Where You Need It
                      
                     
                      Training can be delivered on-site or at the university. Training can be scheduled
                        during the day, at night, or on weekends — at times that are most convenient for your
                        employees.
                      
                     
                      Available University facilities include: 
                     
                      
                        
                         UofM Main Campus 
                        
                         Fogelman Executive Center 
                        
                         FedEx Institute of Technology 
                        
                         University Holiday Inn 
                        
                         UofM Carrier Center, Collierville 
                        
                         UofM Campus, Millington 
                        
                      
                     
                        
                     
                      About Our Training Staff 
                     
                      Our trainers/facilitators are highly-skilled leaders with real-world experience. Each
                        instructor's expertise will provide your company with the latest in best practices
                        and corporate trends.
                      
                     
                      Special Arrangements 
                     
                      Need to bring some of your employees in from out of town? We have hotel accommodations
                        on campus and can provide a training package that includes meals and snacks.
                      
                     
                      Benefits of Customized Training 
                     
                      
                        
                         Course content and duration of training are tailored to your specific requirements. 
                        
                         The content is designed to meet the individual needs of your employees. 
                        
                         The training is conducted on-site or at a convenient site of your choice. 
                        
                         You choose the date and time of training. 
                        
                         You benefit from cost advantages of group training. 
                        
                      
                     
                      Recent Clients 
                     
                      Join the hundreds of professional who look to our Customized Training Program each
                        year to enhance their skills and stay ahead of the evolving trends. Our clients include:
                      
                     
                      AutoZone FedEx St. Jude ALSAC Technicolor U.S. Navy Corp Engineers Levenger Kroger Brother International Memphis City Schools
                      
                     
                      Our 10 Most Requested Training Courses 
                     
                      Project Management Leadership Essentials Presentation Skills Goal Setting Techniques Dealing With Difficult People Understanding Generational Differences Stepping Up To Supervisor Conflict Resolution Business Writing The Power of Communication
                      
                     
                       Let us help improve your workforce!  
                     
                       For all your training needs, contact Roy Bowery at  rbowery@memphis.edu  or (901) 678-2783.  
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Customized Training 
                         
                            
                               
                                Meet Some of Our Team  
                                Customized Training Courses  
                                Client Testimonials  
                            
                         
                      
                      
                      
                         
                            
                                Continuing Education Units  
                               Gain additional education for re-licensing and certification. 
                            
                            
                                myMemphis  
                               Your Source for Personalized online resources at the University of Memphis! 
                            
                            
                                Make a Gift!  
                               Thank you for choosing to support the University of Memphis! 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


