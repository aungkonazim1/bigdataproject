Inserting media files in the HTML Editor | Desire2Learn Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Inserting media files in the HTML Editor | Desire2Learn Resource Center 






 

 
 
 
 
 

 

 

 





 
 
 
   
     Skip to main content 
   
     
   

           
         
              
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Ideas Library    Insights    Integrations    Learning Environment    Learning Repository   
             
       
    
     
       

         

                       

                               
                                      
              
                               

                                        Desire2Learn Resource Center  
                  
                  
                 
              
             
          
                
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      
         

       
     

    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    HTML Editor    »    Creating content in the HTML Editor   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Inserting media files in the HTML Editor                       
        
        
       
        
     
              
	You can use HTML Editor's    Insert Stuff  option to embed media files:
 

 
	Table of contents
 

  
		 Insert a media file from your computer 
	 
	 
		 Insert a media file from your course offering files 
	 
	 
		 Insert a media file from your shared files root path 
	 
	 
		 Insert a media file from Learning Repository 
	 
	 
		 Create and insert a video note 
	 
	 
		 Insert an existing video note 
	 
	 
		 Insert a YouTube video 
	 
	 
		 Insert a URL link 
	 
  
	  Insert a media file from your computer
 

  
		Click the    Insert Stuff  icon.
	 
	 
		Select    My Computer  to browse and select a file from your computer.
	 
	 
		After you select a file, click  Choose Destination  to select or create a storage location for the file.
	 
	 
		Click  Upload .
	 
	 
		Click  Insert .
	 
  
	  Insert a media file from your course offering files
 

  
		Click the    Insert Stuff  icon.
	 
	 
		Click    Course Offering Files  to browse and select a file from your current org unit file path.
	 
	 
		Select your file from the file directory or click    Upload  to upload a file to the directory. You can also select a file type from the  Media Type  drop-down list and click  Apply  to filter your directory search.
	 
	 
		After you choose a file, click  Next .
	 
	 
		Enter a  Link Text  and an  Alternate Text .
	 
	 
		Click  Insert .
	 
  
	  Insert a media file from your shared files root path
 

  
		Click the    Insert Stuff  icon.
	 
	 
		Click    Shared Files  to browse and select a file from your shared files root path.
	 
	 
		Select your file from the file directory or click    Upload  to upload a file to the directory. You can also select a file type from the  Media Type  drop-down list and click  Apply  to filter your directory search.
	 
	 
		After you choose a file, click  Next .
	 
	 
		Enter a  Link Text , if desired.
	 
	 
		Click  Insert .
	 
  
	  Insert a media file from Learning Repository
 

  
		Click the    Insert Stuff  icon.
	 
	 
		Click    Learning Repository  to browse available learning object repositories for objects and files.
	 
	 
		Select your file from the repository and click  Next .
	 
	 
		Enter a  Link Text  and an  Alternative Text . If your text is decorative, you can select the  This is decorative  check box to avoid entering alternative text. Select  Start playing automatically  if you want your media file to play as soon as the content item opens.
	 
	 
		Click  Insert .
	 
  
	  Create and insert a video note
 

 
	 Note  After this step your video note saves to Video Note Search. If you have permission to search and insert video notes, you can reuse this video in the future.
 

  
		Click the    Insert Stuff  icon.
	 
	 
		Click    Video Note .
	 
	 
		Click  Allow  when the Flash player prompts you to allow camera and microphone access.
	 
	 
		Click  Record  and record your video. Click  Stop  when you finish recording.
	 
	 
		You can click  Play  to preview your video note.
	 
	 
		If you want to re-record, click  Clear . If you are satisfied with your recording, click  Next .
	 
	 
		Enter a  Title  and  Description  of your video note. Text you provide in these fields become searchable if users have permission to search and insert other people's video notes.
	 
	 
		Click  Next .
	 
	 
		You can preview the video note before you insert it.
	 
	 
		Click  Insert .
	 
  
	  Insert an existing video note
 

  
		Click the    Insert Stuff  icon.
	 
	 
		Click    Video Note Search .
	 
	 
		Enter your search terms in the  Search Notes  field, then click  Search . You can also perform a blank  Search  to see all search results.
	 
	 
		Select the video you want to insert and click  Next .
	 
	 
		You can preview the video note before you insert it.
	 
	 
		Click  Insert .
	 
  
	  Insert a YouTube video
 

  
		Click the    Insert Stuff  icon.
	 
	 
		Click    YouTube  to browse and select a video you want to insert.
	 
	 
		Click  Next . You can preview the YouTube video from the YouTube Properties page.
	 
	 
		Click  Insert .
	 
  
	  Insert a URL Link
 

  
		Click the    Insert Stuff  icon.
	 
	 
		Click    Insert Link  to insert the URL for an online media file.
	 
	 
		Enter the URL in the  URL  field and click  Next .
	 
	 
		Click  Insert .
	 
      Audience:     Instructor       

    
           

                   ‹ Inserting images in the HTML Editor 
        
                   up 
        
                   Creating tables in the HTML Editor › 
        
       
    
   
     

              Printer-friendly version    
    
    
   
 

                          

                      
                     
                   

                 

                      
  
    
	    Copyright © 1999-2013 Desire2Learn Incorporated. All rights reserved.
 
 
      
               
             

                  
        HTML Editor  
  
      HTML Editor basics    Creating content in the HTML Editor    Creating content in the design view of the HTML Editor    Pasting Content into the HTML Editor    Inserting images in the HTML Editor    Inserting media files in the HTML Editor      Creating tables in the HTML Editor    Creating quicklinks in the HTML Editor    Creating equations in the HTML Editor    
                  
           
         

       
     

    
    
    
   
 
   
 
