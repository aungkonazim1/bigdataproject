The Future of Our Great University &#8211; The University of Memphis President&#039;s Blog   


 

 
 
 
 


 
 
 
 

 



 The Future of Our Great University   The University of Memphis President s Blog 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 



 
 
  
 
 
 

 
 
 
 
			

         
 		
		



 
 
 
 

 

 


  Skip to content  




 

	
	 

		
		 

			
			 

				
						  The University of Memphis President s Blog  

					
			  

		  

		
		 

			 Primary Menu 
			 Menu 

			
			 
				 
					 
				 
					 Search for: 
					 
				 
				 
			 				 
			 

			 

			  
  Home    About the President  
  

		  

		 

		
		
			
				 

			
		
		 

		
	  

	
	 
	 

		
		 

			
			
	
	 

		
		
		 

			
			 The Future of Our Great University 
			
			 

				   Author   Jeanine Hornish Rakow     Published on    December 7, 2015     Leave a comment  
			  

			
		  

		
		 

			 Campus Community: 
 As we wrap up another semester and move into the holiday season, I want to provide an update regarding a few issues.  Several weeks ago I forwarded an update on the Tennessee Higher Education Commission (THEC) funding recommendations for the University of Memphis. Our performance has improved significantly over the past two years and the recommendations are promising. This past week, we received news that Governor Haslam is recommending legislation that would provide for a local governing board for the UofM, a move that facilitates much greater autonomy, community investment and the flexibility needed for us to compete at a national level.  We have worked vigorously over the last several years to move this possibility forward, and I’m thrilled with the Governor’s recommendation.  I will be serving on the transition committee chaired by Governor Haslam, with an anticipated start date in January. 
 The long-term vision for our great University is to become the next great national research university in Tennessee, a goal that is clearly attainable.  We have launched a number of initiatives to grow community involvement and investment in the UofM, along with efforts to expand our national visibility. In addition to the above, there are other promising signs.  Including our recently opened Community Health Building and the soon to be opened Centennial Place Residence Hall, over $500 million of university and private funds will be invested on campus and in the university district over the next five years.  On almost a daily basis I receive an email from presidents around the country praising our national visibility campaign.  Similarly, each and every day there is new evidence of remarkable scientific, creative and scholarly contributions of our faculty. We are optimistic that we can meet our 10% growth goal in the coming years. 
 As you know our football program is completing a historic two-year run, something that has allowed us to attract one of the most outstanding young coaches in the country.  We have leveraged that success to the benefit of our academic mission and community. It is an exciting time for the University of Memphis.  Thank you for your hard work and dedication to our students, university and community. I wish you and your families the best for the coming holiday season. 
 Go Tigers! 
 M. David Rudd | President 

		  

		
		 

			  Published on    December 7, 2015      Author   Jeanine Hornish Rakow   
			
		  

		
	  

	
				
	 
		 Post navigation 
		    Previous article:  THEC Funding Recommendations for 2016-2017      Next article:  SACSCOC Reaffirmation    
	 
				

 

	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  


			
			
		  

		
	  


	
		
		
		 

		 Main Sidebar 

			
			  
				 
					 Search for: 
					 
				 
				 
			  		 		 Recent Posts 		 
					 
				 UofM Quality Indicator Score 
						 
					 
				 University of Memphis Research Foundation Announces Grand Opening of Student-Operated FedEx Call Center 
						 
					 
				 Pause to Grieve 
						 
					 
				 University of Memphis Magazine: Fall 2017 
						 
					 
				 Campus Update 
						 
				 
		 		  Recent Comments      Archives 		 
			  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  July 2014  
	  June 2014  
	  May 2014  
	  April 2014  
		 
		   Categories 		 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 
			
		  

		
		  

	
	
	 

		
		 Footer Content 

		 
		
			
				
				
								
			
		  
	
		 

			
			
			Using  Tiny Framework     
			
			   Log in  

		  
		
		 

			
			

 

	 Social Links Menu 

	      i class= fa fa-twitter   /i    
  
  

			
		  

		
	  

	
  


        

        









		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 
 