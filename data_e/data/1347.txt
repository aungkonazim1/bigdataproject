 - Office of Student Conduct - University of Memphis  Information and resources about the University’s process and procedures for dealing with Academic Misconduct.  










 
 
 
     



 
    
    
      - 
      	Office of Student Conduct
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Office of Student Conduct
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Academic Misconduct  
                          Sexual Misconduct  
                          Behavioral Intervention Team  
                          Code of Rights  
                      
                      
                         
                            Academic Misconduct   
                            
                               
                                  
                                   Academic Help  
                                   Integrity Resources  
                                   Process   Procedures  
                                   Appeals  
                                   Summary Discipline letters  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Office of Student Conduct
                              	  
                         
                           	Academic Misconduct
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Maintaining high standards at all times. 
                     
                      What is Academic Misconduct? 
                     
                       Plagiarism  - The adoption or reproduction of ideas, words, statements, images, or works of another
                        person as one's own without proper attribution.
                      
                     
                       Cheating  - Using or attempting to use unauthorized materials, information, or aids in any
                        academic exercise or test/examination. The term academic exercise includes all forms
                        of work submitted for credit or hours.
                      
                     
                       Fabrication  - Unauthorized falsification or invention of any information or citation in an academic
                        exercise.
                      
                     
                      What is expected of UofM students? 
                     
                      You and your classmates are expected to conduct yourselves with character and integrity
                        in academics and in everyday life. Our distinguished scholastic pursuits mean everything
                        to the University of Memphis, and we do not take the issue of integrity lightly. You
                        are expected to uphold our high academic standards and complete your assignments,
                        exams and other scholastic efforts with 100% honesty. You must cite sources and acknowledge
                        the contributions of others when used in your scholastic work. Academic dishonesty
                        will not be tolerated on any level. By rising to these standards each and every day,
                        you are helping to build our reputation as a world-class major research university
                        advancing knowledge, innovation, creativity, discovery and artistic expression in
                        the Mid-South and beyond.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Academic Misconduct 
                         
                            
                               
                                Academic Help  
                                Integrity Resources  
                                Process   Procedures  
                                Appeals  
                                Summary Discipline letters  
                            
                         
                      
                      
                      
                         
                            
                                Parking Citation Appeals  
                               Information on how to file a Parking Citation Appeal. 
                            
                            
                                Ethics In Action Online Workbook  
                               For UofM students who have been in contact with OSC. 
                            
                            
                                Resources  
                               OSC related information for our campus and community. 
                            
                            
                                Contact Information  
                               Here is how to reach the OSC staff and Student Court members. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


