Quantitative Certificate - CEPR - University of Memphis    










 
 
 
     



 
    
    
    Quantitative Certificate - 
      	CEPR
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Counseling, Educational Psychology   Research
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Counseling  
                          Counseling Psychology  
                          Ed Psych   Research  
                          Faculty  
                          Research  
                      
                      
                         
                            EDPR Menu   
                            
                               
                                  
                                   About EDPR  
                                   Degrees   Certificates  
                                        
                                         M.S.  
                                        
                                         Online M.S.  
                                        
                                         Ph.D.  
                                        
                                         Graduate Certificate in Qualitative Research  
                                        
                                         Graduate Certificate in Quantitative Research  
                                     
                                  
                                   Courses  
                                   Faculty  
                                   News  
                                   Student Resources  
                                   Admissions  
                                         Apply  
                                         M.S. Application  
                                         M.S. Online Application  
                                         Ph.D. Application  
                                         International Students  
                                         Graduate Assistantships  
                                         Graduate Catalog  
                                     
                                  
                                   Research  
                                         EDPR Colloquia  
                                         Childhood Study Group  
                                         QUIC  
                                         SPIRIT  
                                         Professional Associations  
                                         Statistical Consulting  
                                         Undergraduate Educational Research Team  
                                        
                                          
                                           
                                             
                                               Youth Development   Social Media Group  
                                             
                                           
                                          
                                        
                                     
                                  
                                   View our brochure!  
                                   Like us on Facebook!  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	CEPR
                              	  
                          
                              	EDPR
                              	  
                         Quantitative Certificate 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Graduate Certificate in Quantitative Methods (GCQM) 
                     
                      This 15-credit-hour certificate program is constructed to provide students with formal
                        training in quantitative research methods and data analysis in the social sciences
                        and prepare them for a variety of employment opportunities in private and public sectors.
                        The certificate program will be open to all graduate students at The University of
                        Memphis and graduate students from other institutions. Students will receive a certificate
                        in quantitative methods upon completion of the requirements.
                      
                     
                      Graduates of the certificate program will be prepared to: 
                     
                      
                        
                         Design a wide range of research studies and program evaluations, both observational
                           and experimental in nature
                         
                        
                         Build or validate measures for social and psychological constructs based on psychometric
                           theory and models
                         
                        
                         Apply appropriate statistical analyses to various data sources for descriptive and
                           inferential purposes
                         
                        
                         Use computer software to implement a wide range of univariate and multivariate statistical
                           routines
                         
                        
                         Translate statistical evidence into usable information for stakeholders 
                        
                      
                     
                      Certificate Coordinators 
                     
                      
                        
                         Dr. Yonghong Jade Xu 
                        
                         Dr. Leigh Harrell-Williams 
                        
                      
                     
                       This is a 2 step application process. Successful applicants must:  
                     
                      
                        
                         visit the  Graduate School application page  to apply.
                         
                        
                         have a minimum GPA of 3.0 
                        
                         have completed EDPR 7521 and EDPR 7/8541 on campus (or equivalent to be approved by
                           program--for transfer students only) with a grade of B or above
                         
                        
                         submit a 2-3 page personal essay that discusses your research interests, prior preparation
                           and experience related to this certificate program, and your long-range career/professional
                           plans
                         
                        
                         complete the   Graduate Certificate in Quantitative Methods Application Form   
                        
                      
                     
                      Following a review, your application will be either approved or returned to you for
                        further discussion and revision. If you have any questions about the application process,
                        please contact  Dr. Yonghong Jade Xu   or  Dr. Leigh Harrell-Williams  
                     
                      Submit all application materials to: 
                     
                      Melynda Whitwell 100 Ball Hall University of Memphis Memphis, TN 38152
                      
                     
                       Certificate Course Requirements  
                     
                       Prerequisites: EDPR 7/8541 or equivalent and EDPR 7521 or equivalent.  
                     
                      The certificate requires 15 semester hours of credit. Below are the required and elective
                        courses available to students:
                      
                     
                       Required core courses (6 credits):  
                     
                      
                        
                         EDPR 7/8542: Statistical Methods Applied to Education II 
                        
                         EDPR 7/8511: Introduction to Measurement and Evaluation 
                        
                      
                     
                       Electives (9 credits are selected from):  
                     
                      
                        
                         EDPR 8549: Multivariate Methods in Education 
                        
                         EDPR 8544: Application of Multiple Regression 
                        
                         EDPR 7/8531: Computer as a Research Tool 
                        
                         EDPR 7/8543: Research Design and Analysis 
                        
                         EDPR 7/8554: Nonparametric Statistics 
                        
                      
                     
                      If a student feels that a course outside of EDPR is appropriate as an elective, the
                        student can complete the   Petition for Elective   form to be reviewed and approved by the certificate coordinators. 
                      
                     
                      After you complete all the certificate requirements: 
                     
                      Please fill out  the completion form , have it signed by Dr. Yonghong Jade Xu, and take it to Jennifer Mueller or Melynda
                        Whitwell in Ball Hall 100.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         EDPR Menu 
                         
                            
                               
                                About EDPR  
                                Degrees   Certificates  
                                     
                                      M.S.  
                                     
                                      Online M.S.  
                                     
                                      Ph.D.  
                                     
                                      Graduate Certificate in Qualitative Research  
                                     
                                      Graduate Certificate in Quantitative Research  
                                  
                               
                                Courses  
                                Faculty  
                                News  
                                Student Resources  
                                Admissions  
                                      Apply  
                                      M.S. Application  
                                      M.S. Online Application  
                                      Ph.D. Application  
                                      International Students  
                                      Graduate Assistantships  
                                      Graduate Catalog  
                                  
                               
                                Research  
                                      EDPR Colloquia  
                                      Childhood Study Group  
                                      QUIC  
                                      SPIRIT  
                                      Professional Associations  
                                      Statistical Consulting  
                                      Undergraduate Educational Research Team  
                                     
                                       
                                        
                                          
                                            Youth Development   Social Media Group  
                                          
                                        
                                       
                                     
                                  
                               
                                View our brochure!  
                                Like us on Facebook!  
                            
                         
                      
                      
                      
                         
                            
                                About the Department  
                                
                            
                            
                                Student Organizations in CEPR  
                                
                            
                            
                                Department News  
                                
                            
                            
                                Contact Us  
                                
                            
                         
                      
                      
                      
                      
                        	
                         
                           		
                           
                           
                           
                           		
                            
                              
                               
                                 
                                  
                                     
                                    
                                  
                                 
                                  
                                    
                                  
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                     #9 
                                    
                                     BestCollege.com 
                                    
                                     Online Master's in Educational Psychology 
                                    
                                  
                                 
                                  
                                    
                                     #10 
                                    
                                     BestCollegesOnline.org 
                                    
                                     Most Affordable Online Master's in Educational Psychology 
                                    
                                  
                                 
                                  
                                    
                                     #9 
                                    
                                     BestMastersDegrees.com 
                                    
                                     Most Affordable Online Master's in Educational Psychology 
                                    
                                  
                                 									 
                                  
                                    
                                     #14 
                                    
                                     BestMastersinPsychology.com 
                                    
                                     Best Online Master's in Educational Psychology 
                                    
                                  
                                 
                               
                              
                            
                           	
                           
                         
                        
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


