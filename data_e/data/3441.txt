Office of Inclusion and Diversity &#8211; The University of Memphis President&#039;s Blog   


 

 
 
 
 


 
 
 
 

 



 Office of Inclusion and Diversity   The University of Memphis President s Blog 
 
 
 
 
 
		
		
 
 
 
 
 
 
 
 
 



 
 
  
 
 
 

 
 
 
 
			

         
 		
		



 
 
 
 

 

 


  Skip to content  




 

	
	 

		
		 

			
			 

				
						  The University of Memphis President s Blog  

					
			  

		  

		
		 

			 Primary Menu 
			 Menu 

			
			 
				 
					 
				 
					 Search for: 
					 
				 
				 
			 				 
			 

			 

			  
  Home    About the President  
  

		  

		 

		
		
			
				 

			
		
		 

		
	  

	
	 
	 

		
		 

			
			
	
	 

		
		
		 

			
			 Office of Inclusion and Diversity 
			
			 

				   Author   Jeanine Hornish Rakow     Published on    February 8, 2016     Leave a comment  
			  

			
		  

		
		 

			 Dear Campus Community: 
 The University of Memphis is dedicated to helping students, faculty and staff create a vibrant, welcoming and inclusive environment. Currently, we are exploring options for an Office of Inclusion and Sexual Diversity at the UofM. The Student Government Association recently passed a bill in support of this office, which would develop and coordinate programs for the LGBTQA community, promote dialogue on campus about diversity and help build support for equality.  As always, I'm grateful to our student leadership for their thoughtful dedication to our campus. 
 As shared previously, I am committed to containing administrative growth at our university, particularly given the broad range of needs on campus. However, we are actively exploring the current organizational structure and related resource allocation for an Office of Inclusion and Sexual Diversity due to the importance of this issue. 
 Plans call for a graduate assistant to be appointed by July 1. The GA will research services and trends provided through offices of inclusion on other college campuses. Other details regarding the exact structure and staffing of the Office of Inclusion and Sexual Diversity are still being finalized, with a keen eye on administrative concerns raised above. The promotion of diversity on campus is critical to the University’s success, and we are committed to expanding knowledge, understanding and the well-being of all. 
 Thank you, 
 M. David Rudd | President 

		  

		
		 

			  Published on    February 8, 2016      Author   Jeanine Hornish Rakow   
			
		  

		
	  

	
				
	 
		 Post navigation 
		    Previous article:  UofM Community Contributes 602,904 Hours of Community Service      Next article:  University of Memphis: Doing Is Maintaining    
	 
				

 

	
		 
		 Leave a Reply   Cancel reply   			 
				  Your email address will not be published.  Required fields are marked  *    Comment       Name  *     
  Email  *     
    
 
 

	 
	 
	 Notify me of followup comments via e-mail 
	 


			 
			  
	
  


			
			
		  

		
	  


	
		
		
		 

		 Main Sidebar 

			
			  
				 
					 Search for: 
					 
				 
				 
			  		 		 Recent Posts 		 
					 
				 UofM Quality Indicator Score 
						 
					 
				 University of Memphis Research Foundation Announces Grand Opening of Student-Operated FedEx Call Center 
						 
					 
				 Pause to Grieve 
						 
					 
				 University of Memphis Magazine: Fall 2017 
						 
					 
				 Campus Update 
						 
				 
		 		  Recent Comments      Archives 		 
			  October 2017  
	  September 2017  
	  August 2017  
	  July 2017  
	  June 2017  
	  May 2017  
	  April 2017  
	  March 2017  
	  February 2017  
	  January 2017  
	  December 2016  
	  November 2016  
	  October 2016  
	  September 2016  
	  August 2016  
	  July 2016  
	  June 2016  
	  May 2016  
	  April 2016  
	  March 2016  
	  February 2016  
	  January 2016  
	  December 2015  
	  November 2015  
	  October 2015  
	  September 2015  
	  August 2015  
	  July 2015  
	  June 2015  
	  May 2015  
	  April 2015  
	  March 2015  
	  February 2015  
	  January 2015  
	  December 2014  
	  November 2014  
	  October 2014  
	  September 2014  
	  August 2014  
	  July 2014  
	  June 2014  
	  May 2014  
	  April 2014  
		 
		   Categories 		 
	  Uncategorized 
 
		 
   Meta 			 
						  Log in  
			  Entries  RSS   
			  Comments  RSS   
			  blogs.memphis.edu  
						 
 
			
		  

		
		  

	
	
	 

		
		 Footer Content 

		 
		
			
				
				
								
			
		  
	
		 

			
			
			Using  Tiny Framework     
			
			   Log in  

		  
		
		 

			
			

 

	 Social Links Menu 

	      i class= fa fa-twitter   /i    
  
  

			
		  

		
	  

	
  


        

        









		 
							 Skip to toolbar 
						 
				 
		  University of Memphis   
		  University Home 		 
		  Memphis Blogs 		 
		  Blogging Help 		   		 
		  Log In 		   
		     Search    		  			 
					 

		
 
 
 