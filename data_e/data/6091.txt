The Profile of Dipankar Dasgupta   
 
 The Profile of Dipankar Dasgupta 
 
 




 
 



 


 

 
 Dr. Dipankar Dasgupta 
 

 
 * Research Sites * 
 
 
  Artificial Immune Systems (AIS)  
  Center for Information Assurance (CfIA)  
 
 
            


 
 
 
Dr. Dipankar Dasgupta 
333 Dunn Hall 
Memphis, TN 38152-3240 
phone: (901) 678-4147 
fax: (901) 678-1506 
 dasgupta@memphis.edu 
 
 
 
  Dr. Pat E. Burlison  Professor of   Computer Science  
 Director,  Center for Information Assurance  
 Director,  Intelligent Security Systems Research Laboratory  
 


       
  Elevated to IEEE Fellow(Batch of 2015) 
   Distinguished ACM Speaker  
  Recipient of 2012 Willard R. Sparks Eminent Faculty Award.  
 
 
  Advisory Board Member of  MIT in Cyber Security   
  Editorial Board of journals  


 

 Evolutionary Intelligence, Springer-Verlag 

 Evolutionary Optimization, Polish Academy of Science.  

 Recent Patents on Computer Science, online journal Bentham Science Publishers Ltd.  

 Swarm and Evolutionary Computing - Elsevier Press 
 
 
  Announcement:  
 
  Research Assistant Professor Position  (in Cyber Security) Available
  
 
 

 
 * Principal Investigator * 
 
 
  Act Online  
 
 
      


 

 
 
  Home  
  Events 
	 
	  News  
	  Professional Activities  
	  Invited Talks  
	 
 
  Research 
	 
	  Interests 
		 
		  Artificial Immune Systems  
		  Evolutionary Computation  
		  Immunological Computation  
		  Intrusion Detection  
		  Fault Detection  
		 
	 
	  Projects 
		 		
		  Current Projects  
                  Previous Projects  
		 
	 
	
	  Grants  
	  Publications  
 
	 
 
  Teaching 
	 
	  Courses  
	 
 

  Staff/Students  
  Books 
         
          AUA Book  
          IC Book  
          AIS Book  
          EC Book  
         
 
  Recognitions 
         
          Awards  
          Videos  
	 
 
  About  
  
 

 
 
Dr. Dasgupta will Organize IEEE Symposium on  Computational Intelligence in Cyber Security (CICS 2017)  at Hawaii, USA from November 27-December 1, 2017.
Program Committee Member of the 1st IEEE International Workshop on  Cyber Resiliency Economics (CRE 2016)  , Vienna, Austria, August 1-3, 2016.
Prof. Dasgupta will give an invited talk at the  Computer Science Department, University of Tennessee, Knoxville, TN, April 7, 2016
   
Prof. Dasgupta will present a research paper at 11th Annual Cyber and Information Security Research  (CISR)  Conference will be held at the conference center at Oak Ridge National Laboratory, Oak Ridge, TN, April 4 - 6, 2016.
   
Prof. Dasgupta will give invited talk at  Regional Symposium  "Graduate Education and Research in Information Security",'GERIS'16, on  March 8, 2016, at Binghamton University,Binghamton, New York.
   
Announcement for the available position in  Research Assitant Professor  (in Cyber Security)
   
Prof. Dasgupta was interviewed by a local TV Channel (FOX 13) and telecast on Feb. 19, 2016.  Click here for Video. 
   
Organized  "Cybersecurity Certificate Course"  foundational program at FedEx Institute of Technology,UofM, February 1-5, 2016.
   
Prof. Dasgupta gave an invited talk on  5th International Conference on Fuzzy and Neural Computing , FANCCO-2015, December 16-19, 2015.
   
Cluster to Advance Cyber Security & Testing (CAST)  hosted  Cybersecurity Lightning Talks  at the FedEx Institute of Technology, afternoon of December 3, 2015
   
CfIA Receives Cyber Security Training Grant from FEMA
   
UofM's CfIA Will Develop Course for  Mobile Device Security and Privacy Issues 
   
Prof. Dasgupta gave an invited talk on  Adaptive Multi-Factor Authentication  at the  Department of Electrical Engineering and Computer Science and CASE Center, Syracuse University, Syracuse, NY 13224-5040 November 18, 2015
   
Organize a Symposium on Computational Intelligence in Cyber Security (CICS) at IEEE Symposium Series on Computational Intelligence ( SSCI, ), December 7-10, 2015 at Cap Town, South Africa
   
Gave keynote speech at St. Louis at  Cyber Security workshop (STL-CyberCon) , University of Missouri-St. Louis, November 20, 2015
   
Prof. Dasgupta attended the  NIST-NICE  conference at San Diego from November 1-4, 2015
   
Prof. Dasgupta gave an invited talk at 9th  International Research Workshop  on Advances and Innovations in Systems Testing at FedEx Institute of Technology, the University of Memphis, October 20, 2015
   
Our   Cyber Security Team  got a   second position   on Cyber Defense Competition  @CANSec 2015 , held on 24th October at University of Arkansas at Little Rock
  
 

 


 Evolutionary Computation 

Book:  Evolutionary Algorithms in Engineering Applications 

 Structured Genetic Algorithms (st. GA) 

 The field of biological evolution brought a new age in adaptive computation (AC). Among different evolutionary computation approaches, Genetic Algorithms (GA) are receiving much attention both in academic and industries. Genetic algorithms are general-purpose search procedures based on the mechanisms of natural selection and population genetics. Genetic algorithm-based tools have started growing impact in companies - predicting financial market, in factories - job scheduling etc. with their power of search, optimization, adaptation and learning. For the users of diversified fields, genetic algorithms are appealing because of their simplicity, easy to interface and ease to extensibility. 

 Despite their generally robust character, as the application increases, there found many domains where formal GAs perform poorly. Several modifications have been suggested to alleviate the difficulties both in the manipulation of encoded information and the ways of representing problem spaces. A number of different models, namely, Messy GAs and Genetic Programmings developed recently which addressed the representation issue of GAs. 

 Dipankar Dasgupta has been involved in the investigation of a more biologically motivated genetic search model - called the  Structured Genetic Algorithm  (sGA). The model uses some complex mechanisms of biological systems for developing a more efficient genetic search technique. Specifically, this model incorporates redundant genetic material and a gene activation mechanism which utilizes multi-layered genomic structures for the chromosome. The additional genetic material has many advantages in search and optimization. It mainly serves two purposes: primarily, it can maintain genetic diversity at all time during the search process, where the expected variability largely depends on the amount of redundancy incorporated in the encoding. 

 The following paragraphs summarize some aspects and advantages of Structured Genetic Algorithms: 

  

 A chromosome is represented by a set of substrings which act as different levels to establish a kind of switch, controlling the expression of down stream genes. These during reproduction are modified by the genetic operators - crossover and mutation etc. - exactly as in simple GAs.  

 In decoding to the phenotype, a chromosome is interpreted as a hierarchical genomic structure of the genetic material. Only those genes currently {\em active} in the chromosome contribute to the fitness of the phenotype. The {\em passive} genes are apparently neutral and carried along as redundant genetic material during the evolutionary process. 

 Genetic operations altering high-level genes result in changes to the active elements of the genomic structures. Particularly, the role of mutation is twofold: it changes the allele value of any gene, but when it occurs at a higher level it acts as a dominance operator and changes the expression of a set of gene values at the lower level. 

 Even when a population converges to its phenotypic space, genotypic diversity still exists which is a unique characteristic of the model. In most other formal genetic models, phenotypic convergence implies genotypic convergence with consequent impoverishment of diversity within the population. 

 It can maintain a balance between exploration and exploitation resulting in efficient searching of potential areas of the phenotypic space. Being trapped at a local optimum which causes premature convergence can be avoided. 

 The sGA provides a long-term mechanism for preserving and retrieving alternative solutions or previously-expressed building blocks within the chromosomal structures. In non-stationary optimizations, a sGA provides a means of rapid long jump adaptation; whereas a simple GA with the dominance and diploidy used so far can store or retrieve one allele independently, and thus may provide only a short-term preservation. 

 Co-evolution can also occur easily among species by simultaneously sampling and preserving different areas of search space in a multi-global fitness landscape. In effect, it can retain multiple optional solutions (or parameter spaces) in function optimization. 

 It can achieve optimization of multi-stage problems by defining search spaces in its different layers and can explore and exploit them in a single evolutionary process. 

 

 One school of thought (Darwinian) believes that evolutionary changes are gradual; another (Punctuated Equilibria) postulates that evolutionary changes go in sudden bursts, punctuating long periods of stasis when very small evolutionary changes take place in a given lineage. The new model provides a good framework for carrying out studies that could bridge these two theories. 

 Structured GA's results to date are very encouraging, though there remain many issues for further investigation. It appears to an enhancement of the formal genetic model with a number of practical advantages. This approach has also received favorable attention in the field of evolutionary computation. However, the studies on structured GAs done so far are only the first step toward the broader goal of developing a more efficient genetic search. Further research to understand the behavior of the model and to determine its search properties is in progress. 



 

 
 






 
 
