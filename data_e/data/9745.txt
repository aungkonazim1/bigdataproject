Time and Leave - CAS - University of Memphis    










 
 
 
     



 
    
    
    Time and Leave - 
      	CAS
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			College of Arts   Sciences
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Areas of Study  
                          Research  
                          Scholarships  
                          Students  
                          Resources  
                          News  
                      
                      
                         
                            CAS Chair Handbook   
                            
                               
                                  
                                   Part I  
                                         Preface  
                                         Roles and Responsibilities  
                                         Survival Strategies  
                                         Departmental Culture   Chair Leadership Style  
                                         Length of Term and Transition to a New Chair  
                                     
                                  
                                   Part II  
                                        
                                         Management Introduction  
                                        
                                         Major Event Timeline  
                                        
                                         Attendance Expectations   Absence Reporting  
                                        
                                         Department Meetings  
                                        
                                         Budget Responsibilities 
                                          
                                           
                                             
                                               Understanding Your Budget  
                                             
                                               Viewing Your Budgets in Self-Service Banner  
                                             
                                               Monitoring Payrolls and Budgets in e~Print  
                                             
                                           
                                          
                                        
                                        
                                         Staff Management  
                                        
                                         Time and Leave  
                                        
                                         Annual Evaluations (Faculty and Staff)  
                                        
                                         Dealing with Complaints (Student, Faculty, and Staff)  
                                        
                                         Development (Donor Relations)  
                                     
                                  
                                   Part III  
                                         Faculty Recruitment  
                                         Joint Appointments  
                                         Tenure   Promotion  
                                         Teaching Load Guidelines  
                                         Tenure Track Faculty  
                                         Non-tenure Track Full-Time Faculty  
                                         Tenured Faculty  
                                         Part-Time Faculty  
                                         Retirement and Post-Retirement  
                                         Emeritus status for retired faculty members  
                                     
                                  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	CAS
                              	  
                          
                              	CAS Chair Handbook
                              	  
                         Time and Leave 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Time and Leave (Faculty and Staff) 
                     
                      General notes on time and leave: 
                     
                      
                        
                         Staff members in benefits-accruing positions accrue both sick and annual leave. Salaried
                           and hourly-paid employees accrue leave at different rates. See  UM Policy 1262  for detailed information on accrual rates. Bi-weekly employees report time and leave
                           every other Friday through an on-line timesheet. Salaried employees submit an on-line
                           leave report each month.  
                         
                        
                         Nine-month faculty accrue sick leave only; they do not accrue annual leave. Faculty
                           sick leave is not reported on-online. The department chair is responsible for reporting
                           faculty sick leave to HR.  
                         
                        
                         Twelve-month faculty members, except postdocs, accrue sick and annual leave. They
                           must complete an on-line leave report each month.  
                         
                        
                         Postdocs accrue sick leave only. Postdocs must complete an on-line leave report each
                           month.  
                         
                        
                         Chairs are responsible for reviewing and approving timesheets and leave reports. This
                           responsibility may be delegated to a designee. However, the chair must still approve
                           the leave report of his/her designee. No one may approve his/her own time and leave.  
                         
                        
                      
                     
                      Professional Development Assignments (PDA)—Faculty only 
                     
                      
                        
                          http://www.memphis.edu/cas/professional-development/index.php   
                         
                        
                      
                     
                      Leave without Pay (LWOP)—Staff and Faculty 
                     
                      It is the policy of the University of Memphis to provide approved, unpaid time off
                        to regular employees for such reasons as (1) illness or disability of an employee
                        who has insufficient accumulated annual and/or sick leave; (2) leave for educational
                        purposes; and/or (3) leave for other justifiable personal reasons. See  UM Policy 1572  for details.
                      
                     
                      
                        
                         Staff LWOP. For staff requesting unpaid leave, contact HR to process the appropriate
                           paperwork. Notify the dean's office. If the staff absence necessitates hiring a temporary
                           employee to cover duties, you may request funds from the dean's office. But be aware,
                           salary lapse from staff positions does not go to the department or the dean: 85% goes
                           to the financial planning office and 15% goes to the provost's office. There is no
                           guarantee the dean's office can provide you with funds for temporary staff hires.  
                         
                        
                         Faculty LWOP. For faculty requesting LWOP of six months or less, prepare a memo to
                           the dean with approval lines for the dean and provost. For faculty LWOP of six-to-twelve
                           months, prepare a memo to the dean with approval lines for dean, provost, and president.
                           Leave requests for more than 12 months will not be considered. If a faculty member
                           requests LWOP because s/he has accepted a position at another university and wants
                           a year to see if thing work out, beware! If you agree to the request, you are tying
                           up a faculty line for a year and it is highly improbable the faculty member will return.
                           You want to think this through very carefully and consult with the dean first. Faculty
                           salary lapse reverts to the provost's office.  
                         
                        
                      
                     
                      Family Medical Leave Act (FMLA)—Staff and Faculty 
                     
                      In compliance with the Family and Medical Leave Act of 1993 (FMLA), it is the policy
                        of the University of Memphis to provide eligible employees up to 12 workweeks of unpaid
                        leave during a 12-month period for specified family and medical reasons.
                      
                     
                      Unpaid FMLA may be compensated if the employee has accumulated sick and/or annual
                        leave. Such leave shall be used in accordance with policies governing the use of sick
                        and annual leave. Starting on the first day of FMLA, the employee must begin to take
                        accrued leave as part of the 12-week FMLA entitlement. University leave policies and
                        the FMLA leave policy shall, therefore, be applied concurrently and not consecutively.
                        In no event may an employee take any unpaid FMLA leave until he or she has used all
                        accrued sick and annual leave. See  UM Policy 1569  for details.
                      
                     
                      If a staff or faculty member tells you s/he may need to go on an extended medical
                        leave, send that person directly to HR Benefits for a consultation. This protects
                        the employee and the university.
                      
                     
                      
                        
                         Assign a staff member to take minutes   
                         
                        
                         Assign responsibility and deadlines for action items.   
                         
                        
                         Circulate the meeting minutes within one week after the meeting so people don't forget
                           action items and deadlines.
                         
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         CAS Chair Handbook 
                         
                            
                               
                                Part I  
                                      Preface  
                                      Roles and Responsibilities  
                                      Survival Strategies  
                                      Departmental Culture   Chair Leadership Style  
                                      Length of Term and Transition to a New Chair  
                                  
                               
                                Part II  
                                     
                                      Management Introduction  
                                     
                                      Major Event Timeline  
                                     
                                      Attendance Expectations   Absence Reporting  
                                     
                                      Department Meetings  
                                     
                                      Budget Responsibilities 
                                       
                                        
                                          
                                            Understanding Your Budget  
                                          
                                            Viewing Your Budgets in Self-Service Banner  
                                          
                                            Monitoring Payrolls and Budgets in e~Print  
                                          
                                        
                                       
                                     
                                     
                                      Staff Management  
                                     
                                      Time and Leave  
                                     
                                      Annual Evaluations (Faculty and Staff)  
                                     
                                      Dealing with Complaints (Student, Faculty, and Staff)  
                                     
                                      Development (Donor Relations)  
                                  
                               
                                Part III  
                                      Faculty Recruitment  
                                      Joint Appointments  
                                      Tenure   Promotion  
                                      Teaching Load Guidelines  
                                      Tenure Track Faculty  
                                      Non-tenure Track Full-Time Faculty  
                                      Tenured Faculty  
                                      Part-Time Faculty  
                                      Retirement and Post-Retirement  
                                      Emeritus status for retired faculty members  
                                  
                               
                            
                         
                      
                      
                      
                         
                            
                                Apply now for an Undergraduate Degree  
                               Pursue your dream, broaden your career choices and gain the confidence you need to
                                 succeed
                               
                            
                            
                                Offering Master's and Doctoral Degrees  
                               Enhance your knowledge, skills and experience 
                            
                            
                                Community Engagement  
                               Teaching and serving the Memphis and Mid-South community through programs and events 
                            
                            
                                Contact Us  
                               Dean's Office Contacts, Advising, Department Chairs... 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/22/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


