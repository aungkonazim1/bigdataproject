Epigraphy Methods - Hypostyle - University of Memphis    










 
 
 
     



 
    
    
    Epigraphy Methods - 
      	Hypostyle
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			The Karnak Great Hypostyle Hall Project
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About Hypostyle  
                          Meaning   Function  
                          Reliefs   Inscriptions  
                          The Project  
                      
                      
                         
                            The Project   
                            
                               
                                  
                                   Origins of the Project  
                                   Early Recording and Restoration  
                                   Endangered Monuments  
                                   Epigraphy Methods  
                                   Salvage Epigraphy  
                                   Field Reports  
                                   Glossary  
                                   Bibliography  
                                   Staff Bios  
                                   Reference Links  
                                   Contact  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Hypostyle
                              	  
                          
                              	Project
                              	  
                         Epigraphy Methods 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Epigraphy Methods  
                     
                      Recording 
                     
                       "Why don't you just take a photograph?"  
                     
                      --Hundreds of Tourists 
                     
                      Many a tourist asked this question while watching an epigraphist engaged in the exacting
                        process of making a perfectly faithful drawing of the inscription on the wall in front
                        of them.
                      
                     
                      Photography is the simplest and most direct means of recording well preserved inscriptions
                        and it revolutionized the recording of Egypt's monumental legacy when it first appeared
                        in the early Nineteenth Century. Unfortunately, its not the most scientifically reliable
                        method of recording.
                      
                     
                        
                     
                      The Limitations of Photography for Recording Inscriptions 
                     
                      Photography has severe limitations as an accurate method of recording Egyptian monuments.
                        The strong light and dark shadows created by the intense sunlight of Egypt conspire
                        to hide much information the photographer seeks to record.
                      
                     
                      In the bright afternoon sunshine, even the most pronounced sunk relief carvings are
                        "washed out" by the strong light. Deep shadow does much the same thing. Only when
                        the sun shines perpendicular to the wall, creating a raking light which brings out
                        the details of the carvings, is photography most effective as a recording medium.
                        Even then, there are limits.
                      
                     
                      
                        
                         
                           
                            
                              
                                    
                              
                            
                           
                            
                              
                                Shadows and visual distractions like damage to the wall surface obscure details in
                                    the photo of a palimpsest relief on the left. It is much easier to see the two different
                                    versions of the relief in the drawing on the right.  
                              
                            
                           
                         
                        
                      
                     
                      If the reliefs are severely damaged, altered or eroded, light and shadow conspire
                        to hide many details of the carved relief. The solution is to produce precise archival
                        drawings which can reveal every detail including those that the best photograph cannot.
                      
                     
                      By closely scrutinizing the reliefs, often "nose to wall," the epigraphist can find
                        the subtlest traces of faded or erased inscriptions, traces of carving in a badly
                        damaged or eroded patch of stone, or even remnants of paint. Sometimes, one can quite
                        literally feel very faint traces of carved lines that can no longer be seen with the
                        naked eye!
                      
                     
                      As a long-time Chicago House alumnus, the Hypostyle Hall Project's founding director
                        Dr. William Murnane endeavored to produce a record of the Hypostyle Hall reliefs approaching
                        the highest standards of the Epigraphic Survey. With resources much smaller than Chicago
                        House it was obvious that a somewhat scaled down process would be necessary.
                      
                     
                      Initially, the Project used a slightly modified version of the Chicago House method.
                        The scenes were traced in pencil on 16 x 20" photographic enlargements by epigraphists.
                        Next, the photographic emulsion was chemically bleached out leaving only the pencil
                        drawing on the whitened emulsion on the surface of the photographic paper. Copies
                        of each drawing are next checked at the wall-- a process called collation.
                      
                     
                        
                     
                      
                        
                         
                           
                            
                              
                                   
                              
                            
                           
                            
                              
                                A blank collation sheet, ready to use  
                              
                            
                           
                         
                        
                      
                     
                          
                     
                      First, blue prints of the drawings are made. These, in turn, are used to make "collation
                        sheets" by cutting them up into smaller sections and mounting each one on 8.5 x 11"sheets
                        of paper leaving most of the page free to make comments and sketches. From these collation
                        sheets teams of epigraphists carefully check the drawings against the wall, making
                        adjustments to the drawings, detailed notations of the necessary changes and often
                        sketches of individual hieroglyphs and other detailed elements of the reliefs.
                      
                     
                      This process usually involves independent collations by at least two Egyptologists,
                        who correct and add details that were improperly drawn or missed in the initial drawing.
                        Disagreements are resolved in conference "at the wall," sometimes in consultation
                        with another Egyptologist on the team.
                      
                     
                      
                        
                         
                           
                            
                              
                                 
                              
                            
                           
                            
                              
                                A finished collation sheet.  
                              
                            
                           
                         
                        
                      
                     
                      When all corrections are agreed upon, the artist transfers them from the collation
                        sheets to the original drawings. After the drawing has been checked yet again (and
                        any lingering questions resolved at the wall, if necessary) it is traced in ink on
                        sheets of mylar. This medium enhances the drawing's clarity and permits the use of
                        different weighted lines— thicker and thinner ones— to show readers the type of relief
                        used in the original, traces of erased figures and painted lines.
                      
                     
                      After a final check for accuracy and neatness, the drawing is ready for publication.
                        Obviously, this is a laborious and elaborate process, but the objective was not merely
                        to record the basic information of the scenes and inscriptions, but to capture even
                        the most subtle epigraphic details of the carvings and to reproduce faithfully the
                        paleography and artistic style of the reliefs.
                      
                     
                      
                        
                         
                           
                            
                              
                                 
                              
                            
                           
                            
                              
                                A final inked drawing of a relief from the Second Pylon.  
                              
                            
                           
                         
                        
                      
                     
                      After the 1997 season, we decided on a further modification of our working method.
                        The size of the photographic enlargements on which the drawings were based was increased
                        to 20 x 24". Henceforth, instead of tracing directly on the enlargements, the initial
                        drawings by tracing them in pencil on mylar laid over the photo.
                      
                     
                      This was advantageous for a number of reasons: (1) it eliminated the need for the
                        costly bleaching out of the enlargements and for an extra set of these to be made.
                        (2) All of the original drawings could be made by an artist. (3)
                      
                     
                      By working at home, the expense of supporting a staff in the field to complete the
                        initial drawings was eliminated allowing more field time for the crucial process of
                        collation.
                      
                     
                      
                        
                         
                           
                            
                              
                                 
                              
                            
                           
                            
                              
                                Ramesses II offering a tray of food. The names in the cartouches were originally Seti
                                    I. Preliminary drawing made in 2001 as part of the salvage epigraphy program.  
                              
                            
                           
                         
                        
                      
                     
                      Following a successful trial of this process in 1999, the newly modified process was
                        used to great advantage in collating drawings of reliefs on the south-east corner
                        of the Hypostyle Hall. The result was to save months of field time and to produce
                        more accurate drawings. A further advantage of this process arose from the fact that
                        the original photos were never bleached away. It was possible to check the preliminary
                        drawing on the mylar by removing it from the photo or by inserting a sheet of paper
                        between the mylar and the photo.
                      
                     
                      
                        
                         
                           
                            
                              
                                 
                              
                            
                           
                            
                              
                                Ramesses II leading Syrian prisoners. Scene from the south-east gate collated in 2000.  
                              
                            
                           
                         
                        
                      
                     
                      This allowed the artist to more easily refine his work and to inspect the drawing
                        for any stray elements which had accidentally been omitted. Such details are easy
                        to miss when drawings in pencil are made on a black and white photograph. After the
                        first collation, the proportional accuracy of the artist's corrections to the drawings
                        could be checked by overlaying the drawings on the photograph. This prevented many
                        subtle inaccuracies from creeping in during the correction process. Finally, during
                        the inking process, overlaying the drawing on the enlargement makes it easier for
                        the artist to render various types of damage— hacking, erosion and incidental damage—more
                        accurately and distinctly.
                      
                     
                      
                        
                         
                           
                            
                              
                                 
                              
                            
                           
                            
                              
                                Lyla Brock drawing a relief on mylar.  
                              
                            
                           
                         
                        
                      
                       
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         The Project 
                         
                            
                               
                                Origins of the Project  
                                Early Recording and Restoration  
                                Endangered Monuments  
                                Epigraphy Methods  
                                Salvage Epigraphy  
                                Field Reports  
                                Glossary  
                                Bibliography  
                                Staff Bios  
                                Reference Links  
                                Contact  
                            
                         
                      
                      
                      
                         
                            
                                Welcome to the Karnak Great Hypostyle Hall Project!  
                               In antiquity, Karnak was the largest religious sanctuary in Egypt's imperial capital
                                 of Thebes (modern Luxor) and was home to the god Amun-Re, king of the Egyptian pantheon.
                               
                            
                            
                                About the Architecture  
                               Who designed the Hypostyle Hall and why did they do it? Read about how Sety I and
                                 his architects built this magnificent structure between two pylon gateways which once
                                 served as the front entrance to Karnak Temple. 
                               
                            
                            
                                Tour the Hall  
                               The Hypostyle Hall is thousands of miles away -- but you can tour entire structure
                                 without leaving your seat! Click here to begin a virtual tour of the historic Hypostyle
                                 Hall. 
                               
                            
                            
                                Epigraphy Methods  
                               "Why don't you just take a photograph?" Why traditional photography is not the most
                                 scientifically reliable method of recording.
                               
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


