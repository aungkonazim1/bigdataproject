Finding and reading discussion posts and threads | Resource Center   
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 Finding and reading discussion posts and threads | Resource Center 








 

 
 
 
 
 

 

 

 








 
 
 
   
     Skip to main content 
   
     
   
  
	      
       Select your language 
  
      
   العربية  English  Português  Español  Français  
 
 
 
 
 
 
 
 
 
 
   
      	
     
       
         
                       
								 
				     				 
											
				                 

                                        Resource Center  
                  
                  
                 
							 
          
		  			    
       Main menu 
  
     Home    Accessibility    Capture    ePortfolio    Integrations    Learning Environment   
    		  
         
       
     

  	 
	 


    
    
    
     
       

         
           

             
               

                
                 

                  
                   
                     

                      
                       You are here    Home    »    Learning Environment    »    Discussions    »    Participating in discussions   
                      
                      
                      
                      
                       
                           
  
   
   

    
               

                   
                          Finding and reading discussion posts and threads                       
        
        
       
          
     
           Printer-friendly version       
	Once a discussion gets going, new posts can come in very quickly. There are a number of places in Learning Environment that identify new posts to help you keep on top of things:
 

  
		The Updates widget on your course homepage lists the total number of unread posts for all discussion topics in your course.
	 
	 
		The number of unread posts appears beneath each topic in the Discussions List. To see only topics with unread posts, click  Unread  in the Filter by tool navigation.
	 
	 
		All topics that contain unread posts appear bolded if you have the Discussions List pane visible when viewing topics or threads.
	 
	 
		Inside a topic, click  Unread  in the Filter by tool navigation.
	 
	 
		You can subscribe to specific discussion forums, topics, or threads to receive an email, SMS, or minibar notification whenever there is a new post.
	 
  
	Reading posts
 

 
	There are two ways to view posts inside a topic: using Grid View or Reading View. See  Changing Discussions settings  to learn how to change your default view.
 

 
	In the Grid View, a list of posts appears at the top of the page, showing the subject, author, and date for each post. Click on a subject line to read the post; the full text appears in the preview pane or in a new window, depending on your personal settings.
 

 
	 Note   If you do not see this option, your organization may not have enabled Grid View and will default to Reading View. The Reading View is the only available view if you access a post via the Content tool.
 

 
	Also in the Grid View, use the  View  drop-down list to switch between Threaded view, where posts are grouped with their replies, and one of the Unthreaded views (All Posts, Unread Only, Flagged Only, Unapproved Only).
 

 
	In the Reading View, the full text of every post always displays once you click on a thread.
 

 
	Find discussion posts
 

  
		On the Discussions List page, click on the topic you want to search for posts.
	 
	 
		Enter the terms you want to find in the search field and click the    Search  icon.
	 
  
	To see all posts again, click the    Clear  icon in the search field.
 

 
	Read discussion threads
 

 
	Click on a topic to display the View Topic page. Then, click on a thread to display all replies to the thread, along with the original post.
 
     Audience:    Learner      

    
           

                   ‹ Accessing Discussions  
        
                   up 
        
                   Creating and replying to discussion threads › 
        
       
    
   
     

    
    
   
 

                          

                      
                     
                   

                 

                
               
             

                  
        Discussions  
  
      Participating in discussions    Accessing Discussions     Finding and reading discussion posts and threads    Creating and replying to discussion threads    Saving a draft discussion thread    Rating discussion posts    Printing discussion threads and posts     Changing Discussions settings      Following discussions    
                  
           
         

       
     

    
    
           
         
                       
           
         
       
	 
		 
		 
			 
				 
					 Links 
					 	
						   Printer-friendly version   
						  							
						  							
					 
				 
				 
					 Contact Us 
					 Want to reach a member of the Client Enablement team?  Contact us via the Brightspace Community site, email or Twitter. 
					  Brightspace Community      Community Email      @BrightspaceHelp  
				 
			 
			  The D2L family of companies includes D2L Corporation, D2L Ltd, D2L Australia Pty Ltd, D2L Europe Ltd, D2L Asia Pte Ltd and D2L Brasil Solu  es de Tecnologia para Educa  o Ltda.    1999-2015 D2L Corporation. |   Privacy Statement   |   Community Rules   |   About    Brightspace, D2L, and other marks ("D2L marks") are trademarks of D2L Corporation, registered in the U.S. and other countries.  Please visit  www.d2l.com/trademarks  for a list of other D2L marks.
			 
			 			
		 
	 
	 
    
   
 
   
 
