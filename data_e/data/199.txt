Graduation - UofM Global - University of Memphis    










 
 
 
     



 
    
    
    Graduation - 
      	UofM Global
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






 

   
   
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
 
   
   
   
   
 

   
   
   
    
      
      
       
 
	
	
	
 
 

 


 
 

 
  
 
	 
		 
			 
				 
					     
				 
				     
				 
					 
						 
							 
								 
									   UofM Home  
										  myMemphis  
										  eCourseware  
										  Webmail  
										  Contact  
								     
								 
								 
									 Search 
									 
									 
									    
									 Site Index 
								 																
								  
							 
						 
					 
					 
						  
							 
								   About UofM Global      
									 
										  UofM Global  
										  Ranked Online Degrees  	
										  The UofM Global Experience  
										  Our Faculty  
										  Tuition   Fees  	
										  Accreditations  
									 
								 
								   Online Degrees      
									 
										  Online Degrees  
										  Undergraduate Degrees  
										  Graduate Degrees  
										  Graduate Certificates  
										  Online Admissions  
										  Contact Us  
									 
								 
								   Online Admissions      
									 
										  Online Admissions  
										  Apply Now   
										  Smart Start  
										  Financial Aid  
										  All Online Services  
									 
								 
								   Current Students      
									 
										  Student Services and Support  
										  Online Advising  
										  Course Registration  
										  Alternative Degree Pathways  
										  Learning Support  
										  Technology Support  
										  Library Services  
										  Career Services  
										  Additional Services  
										  Graduation  
									 
								 
								  Request Info  
							 
						 
					 
				 
			 
		 
		 
			 
				 
					  Online Degrees  
				 
			 
		 
	 
 
 
      
      
      
    
    
       
          
             
                
                   
                       
                           			UofM Global
                           	  
                   
                
             
          
       
       
          
             
                
                   
                      
                         
                             Home  
                             
                                 	UofM Global
                                 	  
                            
                              	Student Services
                              	
                            
                         
                      
                      
                         
                            Student Services and Support   
                            
                               
                                  
                                   Student Services and Support  
                                   Online Advising Menu  
                                         Overview  
                                         Degree Planning  
                                         Find My Advisor  
                                     
                                  
                                   Course Registration  
                                         Overview  
                                         Online Course Search  
                                         Undergraduate Catalog  
                                         Graduate Catalog  
                                         Dates and Deadlines  
                                         Tuition   Fees  
                                         Scholarships  
                                     
                                  
                                   Alternative Degree Pathways  
                                         Overview  
                                         Finish Line Program  
                                         Prior Learning Assessment  
                                     
                                  
                                   Learning Support  
                                         Overview  
                                         Readiness Assessment  
                                         Success Strategies  
                                         Get Connected  
                                         Online Tutoring  
                                     
                                  
                                   Technology Support  
                                         Overview  
                                         Technology Requirements  
                                         Additional Services  
                                         Online Course Preview  
                                     
                                  
                                   Library Services  
                                         Overview  
                                         Ask-a-Librarian  
                                         Electronic Delivery  
                                         Research Consultations  
                                     
                                  
                                   Career Services  
                                   Additional Services  
                                         Overview  
                                         Adult and Veteran Students  
                                         Disability Resources  
                                     
                                  
                                   Graduation  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                     
                     		
                     
                     
                        
                     
                      Graduation 
                     
                      Congratulations! You are now close enough to completing your degree requirements to
                        be included in an upcoming commencement ceremony. Receiving your degree will depend
                        upon your successful fulfillment of all of the requirements for receiving a degree
                        in your field as described in the Undergraduate Catalog of the University of Memphis.
                        There are   4 Steps   to Graduate from the University of Memphis.   LEARN MORE...  
                      
                     
                      Each student is assigned a Graduation Analyst within his or her college or school.
                        The Graduation Analyst will receive your application for graduation and check your
                        status and ensure you are enrolled in the correct number of hours for the term in
                        which you have filed to graduate.   LEARN MORE...  
                      
                     
                      The Commencement Office's goal is to provide all graduates with diplomas in a timely
                        manner. Recent ceremony diplomas are provided 2-3 months after each commencement ceremony.
                        The diploma mailing schedules for each graduation term are listed on the Commencement
                        Office website.   LEARN MORE...  
                     
                      Commencement Office 
                     
                      The Commencement Office handles all questions and concerns related to Commencement
                        activities/graduation, Apply to Graduate, Cap  Gowns (Regalia), Diploma Mailing, Diploma
                        Reorders, Honors Assembly, Alpha Lambda Delta Honors Society, Convocations, and related
                        topics.
                      
                     
                       Commencement Office  101 Brister Hall Memphis, TN 38152 901.678.5547  commencement@memphis.edu  
                     
                      Monday - Friday 8:00 am - 4:30 pm
                      
                     
                        
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Student Services and Support 
                         
                            
                               
                                Student Services and Support  
                                Online Advising Menu  
                                      Overview  
                                      Degree Planning  
                                      Find My Advisor  
                                  
                               
                                Course Registration  
                                      Overview  
                                      Online Course Search  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Dates and Deadlines  
                                      Tuition   Fees  
                                      Scholarships  
                                  
                               
                                Alternative Degree Pathways  
                                      Overview  
                                      Finish Line Program  
                                      Prior Learning Assessment  
                                  
                               
                                Learning Support  
                                      Overview  
                                      Readiness Assessment  
                                      Success Strategies  
                                      Get Connected  
                                      Online Tutoring  
                                  
                               
                                Technology Support  
                                      Overview  
                                      Technology Requirements  
                                      Additional Services  
                                      Online Course Preview  
                                  
                               
                                Library Services  
                                      Overview  
                                      Ask-a-Librarian  
                                      Electronic Delivery  
                                      Research Consultations  
                                  
                               
                                Career Services  
                                Additional Services  
                                      Overview  
                                      Adult and Veteran Students  
                                      Disability Resources  
                                  
                               
                                Graduation  
                            
                         
                      
                      
                     
                     
                      
                                 Need Help? 
                                 
                                     
                                         
											     Call 844-302-3886 
                                         
                                     
                                     
                                         
                                                 Email Us 
                                         
                                     
                                 
                             
                     
                     
                     
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
 
 
 
 
  Full sitemap  
 
 
 
 
  About UofM Global  
 
 
  UofM Global  
  Ranked Online Degrees  	
  The UofM Global Experience  
  Our Faculty  
  Tuition   Fees  	
  Accreditations  
 
  
 
  Online Degrees  
 
 
  Online Degrees  
  Undergraduate Degrees  
  Graduate Degrees  
  Graduate Certificates  
  Online Admissions  
  Contact Us  
 
  
 
 
 
  Online Admissions  
 
 
  Online Admissions  
  Apply Now   
  Smart Start  
  Financial Aid  
  All Online Services  
 
  
  Contact Us  
 
  Tuition  
  Financial Aid  
  
 
	  Current Students  
 
 
  Student Services and Support  
  Online Advising  
  Course Registration  
  Alternative Degree Pathways  
  Learning Support  
  Technology Support  
  Library Services  
  Career Services  
  Additional Services  
  Graduation  
 
  
 
 
 
 
 
   
 
 
   
 
 
   
 
 
 
 
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/29/17 
                                  
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


