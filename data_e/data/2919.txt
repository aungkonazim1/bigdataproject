Director of College Development II, College of Communications &amp; Fine Arts   
 
   
       
       Director of College Development II, College of Communications   Fine Arts 
 
 
   
   
   
   
   
   
   

 
 
 
 
 
 
 
 
 

     

   
   
       
       
         
           Skip to Main Content 
         
         
           
             
   
     
       
         
           
         
       
       
         
          University of Memphis Career Opportunities  
       
     
   
 

           
           
             
   
     
       
         Toggle navigation 
          
          
          
       
     
     
       
    Home  
    Search Jobs  
      Create Account  
      Log In  
    Help  
 

       
             
               University Benefits 
             
             
               Equal Employment 
             
             
               Annual Security Report 
             
             
               Campus Map 
             
             
               About UofM 
             
       
     
   
 

             
               
                
                   Director of College Development II, College of Communications & Fine Arts 
  
  
   
       Bookmark this Posting  |
     Print Preview  |
         Apply for this Job 
   


   
     
       Posting Details 
        
       
            

               Posting Information 
                
               
                     
                       Posting Number 
                         HMAE1465 
                     
                     
                       Advertised Title 
                         Director of College Development II, College of Communications & Fine Arts 
                     
                     
                       Campus Location 
                         Main Campus (Memphis, TN) 
                     
                     
                       Position Number 
                         018175 
                     
                     
                       Category 
                         Staff (Hourly/Monthly) 
                     
                     
                       Department 
                         Office of University Development 
                     
                     
                       Minimum Position Qualifications 
                          Bachelor’s degree and seven years of experience in development or a related field.  
                     
                     
                       Special Conditions 
                          Open until filled, screening will begin on 11/30/2017. 
 The department is especially interested in candidates with a Master’s degree and higher education development/advancement experience. 
 *Must travel and work irregular hours, as needed. 
Work under pressure of deadlines and dollar goals. 
*Must be able to determine how best to develop and maintain effective relationships with donors and determine when and how a prospective donor can be successfully solicited for a gift. 
*Demonstrate ability to relate well to and understand the needs and interests of donors in order to develop or strengthen relationships between them and the University. 
*Must be able to motivate volunteers. 
*Ability to participate in all aspects of the gift cycle. 
*Requires proficient computer and database management skills. 
*Possess excellent verbal and written communication skills as well as professional interpersonal skills.  
                     
                     
                       Work Schedule 
                          Monday-Friday 8:00 a.m.-4:30 p.m. 
Requires some evenings/weekends and occasional out-of-town travel.  
                     
                     
                       Posting Date 
                         11/22/2017 
                     
                     
                       Closing Date 
                          
                     
                     
                       Open Until Screening Begins 
                         Yes 
                     
                     
                       Hiring Range 
                         $77,000 - $81,500 per year 
                     
                     
                       Full-Time/Part-Time 
                         Full-Time: Benefits Eligible 
                     
                     
                       Working Conditions 
                          While performing the duties of this job, the employee is regularly required to sit, use hands to handle or feel, and talk or hear. The employee frequently is required to walk. The employee is occasionally required to stand, reach with hands and arms, and stoop, kneel, or crouch. The employee must occasionally lift and/or move up to 10 pounds. Specific vision abilities required by this job include close vision.  
                     
                     
                       Additional Working Conditions 
                          
                     
                     
                       Special Instructions to Applicants 
                          All applications must be submitted online at workforum.memphis.edu. 
 Applicants must complete all applicable sections of the online application to be considered for a position. Please upload a cover letter, resume, and reference list after completing your application, if required by the system. Required work experience is based on full time hours. Part time work experience will be prorated as listed. 
 Candidates who are called for an interview must notify the Department of Human Resources in writing of any reasonable accommodation needed prior to the date of the interview.  
                     
                     
                       Is this posting for UofM employees only? 
                         No 
                     
                     
                       Positions Supervised 
                          May supervise a Development Officer and/or other staff.  
                     
                     
                       Knowledge, Skills, and Abilities 
                          *Must travel and work irregular hours, as needed.  
Work under pressure of deadlines and dollar goals.  
*Must be able to determine how best to develop and maintain effective relationships with donors and determine when and how a prospective donor can be successfully solicited for a gift.  
*Demonstrate ability to relate well to and understand the needs and interests of donors in order to develop or strengthen relationships between them and the University.  
*Must be able to motivate volunteers.  
*Ability to participate in all aspects of the gift cycle. 
*Requires proficient computer and database management skills.  
*Possess excellent verbal and written communication skills as well as professional interpersonal skills.  
                     
                     
                       Additional Position Information 
                          
                     
                 
            
            

               Job Duties 
               The duties and responsibilities listed are intended to describe the general nature and level of work to be performed in this position and are not to be construed as an exhaustive list of the requirements of this job. 
                 
                     
                       Duties   Responsibilities 
                        Responsible for managing a portfolio of 100-125 prospects and donors. Identifies, cultivates, and designs successful solicitation strategies for donors and prospects in support of university-wide initiatives or for one or more assigned units, with a focus on gifts of $25,000 and above. Executes a target of 95 personal visits annually and prepares and delivers a target of 18-22 written proposals annually. Works with supervisor to agree upon overall annual and stretch goals in key areas.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Develops strong working relationship with the Dean of the assigned unit. Works closely with department heads and faculty and provides counsel and coordination for all development efforts for the units. Schedules joint prospects visits with the Dean and/or other senior administrators when appropriate. Identifies, recruits and trains volunteers for fundraising initiatives. Also collaborates with Corporate   Foundation Relations and Planned Giving staff as needed when developing proposals.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Coordinates prospect management and donor movement with the Development Office. Supervises the activities of a Director of College Development I, Development Officer, or other staff. Utilizes department systems and software to track record all contacts, including proposals submitted, and prospect activity.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Develops and coordinates an effective program for recognition, involvement and stewardship of major gift donors for their assigned college, school or unit(s). Organizes on and off-campus cultivation and recognition events for assigned units and participates in University-wide development activities. Collaborates with the Donor Relations unit to execute stewardship, recognition, and engagement activities.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Prepares and presents reports to University administration. Serves on university wide committees as assigned.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Works with Annual Giving staff to plan and execute an annual giving plan for the unit/s. Works with Alumni Affairs and Public Relations/Mktg to promote the assigned units to its alumni and other constituents.  
                     
                 
                 
                     
                       Duties   Responsibilities 
                        Other duties as assigned.  
                     
                 
            
       
     
  
     Supplemental Questions 
       
            Required fields are indicated with an asterisk (*).  
 
       *  
  Describe your work experience in development.
     (Open Ended Question) 
 



 

       
     Applicant Documents 
       
           Required Documents 
     
     Resume 
     Cover Letter 
     References List 
 

 Optional Documents 
     
     Unofficial Transcript 
 



       


 


                  
               
                
             
           
           
             
   
     University of Memphis — Human Resources 
165 Administration Building, Memphis, TN 38152 
901.678.3573  workforce@memphis.edu  
 Follow us on Twitter  
      
      ©University of Memphis. All Rights Reserved. The University of Memphis is an Equal Opportunity/Affirmative Action university.  
   
 

           
         
       
         
 
        
  

  


    
      
    
     
   
 
