Join   
  
 
 
 
 Join 
 

 
 
 

 

 
      
   
     
           
              
           
                 
       Contact us!       
           
           
           
     
    
   
      Join the CSA! 
      
                
       
    Top 10 Reasons to Get Involved with CSA   
     
     
       
         1. 
         Be involved on campus! Taking pride in your school and your city makes college so much more enjoyable. 
       
       
         2. 
         Meet people who have things in common with you but also expand your horizons. 
       
       
         3. 
         Be a philanthropist. CSA helps in many different ways to give back to our community. 
       
       
         4. 
         Get an awesome t-shirt! 
       
       
         5. 
         Looks really good on a resume! Especially if you work your way up to the Executive Board. 
       
       
         6. 
         Make better grades! Being involved keeps you motivated and having a whole network of study buddies helps too. 
       
       
         7. 
         Students who are involved are more likely to stay until graduation.  
       
       
         8. 
         Have a place to belong! CSA has many events and hang out spots. You'll know you've found a niche on campus. 
       
       
         9. 
         We program fun and meaningful events that are designed for a commuter's schedule. 
       
       
         10. 
         Learn new skills! Being a part of our sub-committees (see FAQ) will give you new opportunities to learn things you may not be able to anywhere else.  
       
     
       
     How do I sign up?     
     Download the Commuter Student Association application form and bring into room 243 in the University Center along with your dues of $10 (which covers your membership for a whole year) and you'll receive a free commuter t-shirt right on the spot!  
         
       
      
     
       
       
   
    
   
      Adult and Commuter Student Services  |  U of M Student Involvement  |  Off-Campus Housing  
      U of Memphis Commuter Student Association© 2012-14 | All Rights Reserved  
    
    
 
  
