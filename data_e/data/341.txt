Department of Philosophy - Philosophy - University of Memphis    










 
 
 
     



 
    
    
    Department of Philosophy - 
      	Philosophy
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Philosophy
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          Undergraduate  
                          Graduate  
                          People  
                          Research  
                          News   Events  
                          Resources  
                          Contact  
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                         
                           	Philosophy
                           	
                         
                      
                   
                   
                      
                         
                              
                             
                            
                             
                            
                            
                                
                            
                         
                         
                             
                         
                      
                      
                     		
                     
                     
                      About the Department of Philosophy 
                     
                      The Department of Philosophy at the University of Memphis is committed to providing
                        students, the profession of philosophy, and the community with innovative and high-quality
                        educational programs, scholarship, and service, which enhance the development of philosophy
                        as a discipline and the quality of the local, as well as global, intellectual environment.
                      
                     
                      Overview 
                     
                      As part of the University's General Education Program, the Department strives to imbue
                        students with skills required for critical thinking and problem solving and an appreciation
                        of their human and cultural heritage.
                      
                     
                      At the BA level, we introduce students to subdivisions within the discipline. At the
                        MA level, the emphasis is on specific and applied aspects of the subdivisions which
                        prepare graduates to continue the study of philosophy and enhance their intellectual
                        lives. The PhD program trains professional philosophers who produce high-quality scholarly
                        work and are able to teach courses not only in their specialty but also a wide variety
                        of basic courses.
                      
                     
                      Pluralism 
                     
                      A schism between Continental philosophy and analytic philosophy has divided the American
                        philosophical community for several decades. That this schism has been detrimental
                        to the discipline is now widely recognized, and the Department of Philosophy at the
                        University of Memphis is one of several philosophy departments around the country
                        that seeks to engender an appreciation of both traditions in our students. The history
                        of philosophy is an important component of our program because it serves as the common
                        language of the two traditions. Our high level of research productivity and conscientious
                        training of students also contribute to the vitality of the Department and the profession.
                      
                     
                      Diversity 
                     
                      In addition to overcoming the divergence between the Continental and analytic traditions,
                        the Department of Philosophy at the University of Memphis seeks to cultivate inclusiveness
                        (notably, in terms of race and gender) in all facets of the Department. Since feminist
                        philosophy and race theory are areas of philosophy where the boundaries separating
                        analytic and continental philosophy are regularly crossed, we have found that our
                        commitments to philosophical pluralism and to diversity complement one another. Adopting
                        this kind of inclusive stance in which a plurality of philosophical methodologies
                        and styles are respected and encouraged is the centerpiece of the University of Memphis
                        program. It is evident in the curriculum and in the research and scholarly activities
                        of the faculty.
                      
                     
                      The Department's achievements in cultivating an inclusive and pluralistic environment
                        constitute one of its unique features. These successes have been recognized across
                        the global philosophical community, and we have been gratified to see other programs
                        emulate our practices and aspirations. We encourage applications from students who
                        share these interests in pluralism and diversity.
                      
                     
                     	
                      
                   
                
                
                   
                      
                      
                         
                            
                                Apply to the Graduate Program  
                               Want to pursue a MA or PhD in Philosophy? Find out how to apply. 
                            
                            
                                Online B.A.  
                               Want to pursue your college degree and manage your busy lifestyle? Experience online
                                 classes at the UofM.
                               
                            
                            
                                Pre-law Advising  
                               Are you interested in a career in the legal profession? Consider a degree in Philosophy! 
                            
                            
                                Support the Department  
                               Find out how you can support the Department of Philosophy. 
                            
                         
                      
                      
                      
                        	
                         
                           		
                           
                           
                           
                           		
                            
                              
                               
                                 
                                  
                                     
                                    
                                  
                                 
                                  
                                    
                                  
                                 
                               
                              
                              
                               
                                 									 
                                  
                                    
                                     #5 
                                    
                                     BestColleges.com 
                                    
                                     Online Bachelor's in Philosophy 
                                    
                                  
                                 
                               
                              
                            
                           	
                           
                         
                        
                      
                      
                         Follow Us Online 
                         
                              Facebook   
                         
                                               Events Calendar  
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/6/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


