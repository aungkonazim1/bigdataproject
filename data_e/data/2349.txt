What kind of work do Planner do? - Planning - University of Memphis    










 
 
 
     



 
    
    
    What kind of work do Planner do? - 
      	Planning
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			City and Regional Planning
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          People  
                          Research/Outreach  
                          Giving  
                          Design Collaborative  
                      
                      
                         
                            About   
                            
                               
                                  
                                   Mission and News  
                                   What is City Planning?  
                                   Where do Planners work?  
                                   What kind of work do Planners do?  
                                   Annual Report  
                                         2016  
                                         2015  
                                         2014  
                                     
                                  
                                   Program Statistics  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Planning
                              	  
                          
                              	About
                              	  
                         What kind of work do Planner do? 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      What kind of work do Planners do? 
                     
                      The kind of work a planner does depends on his/her workplace setting and whether he/she
                        has a particular specialization.
                      
                     
                      Land Use planning is the most traditional venue for planning practice. Some land use
                        planners work on long-range comprehensive plans, developing policies to help communities
                        achieve a future vision. Others develop and implement land use controls, such as zoning
                        regulations, to guide development in the short-term.
                      
                     
                      Environmental planners work to manage and protect natural resources. They measure
                        the impacts of development on the community's physical environment, and develop policies
                        to mitigate negative impacts.
                      
                     
                      Transportation planners develop infrastructure to efficiently move people and goods
                        within and between communities. Because of the link between transportation networks
                        and development patterns, there is a great deal of overlap between transportation
                        planning and land use planning.
                      
                     
                      Neighborhood planners work to preserve or revitalize neighborhoods and provide a foundation
                        for positive social change. Neighborhood planners commonly work with citizen groups
                        and community development corporations – non-profit agencies that coordinate neighborhood-based
                        housing and community development programs.
                      
                     
                      Economic development planners analyze the strengths and weaknesses of local economies.
                        They develop policies and programs to improve the economic well-being of community
                        residents. Others planning specializations include, growth management, capital facilities
                        planning, housing, and historic preservation.
                      
                     
                      Many planners are generalists, with a broad range of knowledge about the impacts of
                        physical development. Good planning involves a great deal of coordination among experts
                        from various fields and agencies. Generalist planners help to facilitate this coordination.
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         About 
                         
                            
                               
                                Mission and News  
                                What is City Planning?  
                                Where do Planners work?  
                                What kind of work do Planners do?  
                                Annual Report  
                                      2016  
                                      2015  
                                      2014  
                                  
                               
                                Program Statistics  
                            
                         
                      
                      
                      
                         
                            
                                Apply to Our Master's Program  
                               Applying for admission to the program is easy 
                            
                            
                                Assistantships and Financial Support  
                               Learn about Graduate Assistantships, Fellowships and Tuition Savings Program for Students 
                            
                            
                                Contact Us!  
                               Main office contact and location information. 
                            
                            
                                SUAPP  
                               City   Regional Planning is a part of the School of Urban Affairs and Public Policy. 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


