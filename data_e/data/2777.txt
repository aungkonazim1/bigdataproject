Mission, Vision, Values &amp; Goals - The Loewenberg College of Nursing - University of Memphis    










 
 
 
     



 
    
    
    Mission, Vision, Values   Goals - 
      	The Loewenberg College of Nursing
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			The Loewenberg College of Nursing
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About Us  
                          Programs  
                          Students  
                          Faculty and Staff  
                          Research  
                          Alumni  
                          News  
                      
                      
                         
                            About Us   
                            
                               
                                  
                                   Dean's Message  
                                   College Facts  
                                   Dean’s Advisory Council  
                                   Mission, Vision, Values   Goals  
                                   Accreditation  
                                   College History  
                                   Partnerships  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	The Loewenberg College of Nursing
                              	  
                          
                              	About Us
                              	  
                         Mission, Vision, Values   Goals 
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Mission, Vision, Values   Goals 
                     
                      Mission 
                     
                       The LCON provides accessible and innovative higher education preparing leaders who
                           promote health of the global community through excellent teaching, rigorous research,
                           and collaborative practice/service.  
                     
                      To fulfill the mission, we are committed to 
                     
                      
                        
                         Creating a learner –centered educational environment 
                        
                         Inspiring life-long learning through excellence in teaching 
                        
                         Shaping practice through innovation and partnerships 
                        
                         Creating and disseminating knowledge through research and engaged scholarship 
                        
                         Embracing diversity and cultural competence 
                        
                      
                     
                      Vision 
                     
                      The LCON is creating a center of excellence where education, research /scholarship,
                        practice, and partnerships integrate to advance nursing science and to promote health
                        of the global community.
                      
                     
                      Core Values 
                     
                      The LCON identifies caring, diversity, integrity, and leadership as core values. These
                        core values are defined as follows:
                      
                     
                        Caring   is a human state, a moral imperative or ideal, an affect, an interpersonal relationship,
                        and a nursing intervention.
                      
                     
                        Diversity   is embracing the recognition, acceptance, and respect of human differences.
                      
                     
                        Integrity   is acting in accordance with an appropriate professional code of ethics and accepted
                        standards of practice.
                      
                     
                        Leadership   is influencing the actions of individuals and organizations in order to achieve desired
                        goals.
                      
                     
                      Strategic Goals 
                     
                      
                        
                         Innovative and High Quality Academic Programs 
                        
                         Student Success: Recruitment, Retention, Degree Completion, and Employment 
                        
                         Integration of Cutting-Edge Technology 
                        
                         Programs of Research/Scholarly Work 
                        
                         Productive Partnerships from local to global communities 
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         About Us 
                         
                            
                               
                                Dean's Message  
                                College Facts  
                                Dean’s Advisory Council  
                                Mission, Vision, Values   Goals  
                                Accreditation  
                                College History  
                                Partnerships  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now  
                               Take the first step to your new career 
                            
                            
                                RN-BSN Program  
                               Learn more about our flexible, accessible and affordable online program 
                            
                            
                                Lambuth Campus Nursing  
                               Earn your degree in Jackson, TN 
                            
                            
                                Contact Us  
                               Our team is here to help 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/12/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


