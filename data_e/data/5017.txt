James T. Thompson Chair for Excellence in Accounting Education   
 
   
       
       James T. Thompson Chair for Excellence in Accounting Education 
 
 
   
   
   
   
   
   
   

 
 
 
 
 
 
 
 
 

     

   
   
       
       
         
           Skip to Main Content 
         
         
           
             
   
     
       
         
           
         
       
       
         
          University of Memphis Career Opportunities  
       
     
   
 

           
           
             
   
     
       
         Toggle navigation 
          
          
          
       
     
     
       
    Home  
    Search Jobs  
      Create Account  
      Log In  
    Help  
 

       
             
               University Benefits 
             
             
               Equal Employment 
             
             
               Annual Security Report 
             
             
               Campus Map 
             
             
               About UofM 
             
       
     
   
 

             
               
                
                   James T. Thompson Chair for Excellence in Accounting Education 
  
  
   
       Bookmark this Posting  |
     Print Preview  |
         Apply for this Job 
   


   
     
       Posting Details 
        
       
            

               Posting Information 
                
               
                     
                       Posting Number 
                         FAECC659 
                     
                     
                       Advertised Title 
                         James T. Thompson Chair for Excellence in Accounting Education 
                     
                     
                       Campus Location 
                         Main Campus (Memphis, TN) 
                     
                     
                       Position Number 
                         003459 
                     
                     
                       Category 
                         Full-Time Faculty 
                     
                     
                       Department 
                         School of Accountancy 
                     
                     
                       Position Summary 
                          The School of Accountancy invites applications for the James T. Thompson Chair for Excellence in Accounting Education, effective fall 2018, pending available funding. All specializations in accounting will be considered. Responsibilities include effectively teaching undergraduate and graduate courses, conducting doctoral seminars, mentoring doctoral students, maintaining a strong research program, and fulfilling professional and university service duties. The School’s faculty includes 19 full-time faculty members. For more information about the department and its programs, visit www.memphis.edu/accountancy.  
                     
                     
                       Minimum Position Qualifications 
                          Applicants should have an earned doctorate in accounting from an  AACSB -accredited institution and education in or experience with U.S.  GAAP , tax law, or auditing standards. Professional certification is highly desired ( CPA  preferred). Candidates must have a strong research record as demonstrated by publications in high-quality accounting journals, an interest in teaching doctoral seminars, the capability to mentor doctoral students and other faculty, and professional experience appropriate to a senior appointment.  
                     
                     
                       Special Conditions 
                          
                     
                     
                       Posting Date 
                         06/23/2017 
                     
                     
                       Closing Date 
                          
                     
                     
                       Open Until Screening Begins 
                         No 
                     
                     
                       Hiring Range 
                         This is a 12-month appointment and offers competitive compensation, research support, and an attractive benefits program. 
                     
                     
                       Full-Time/Part-Time 
                         Full-Time: Benefits Eligible 
                     
                     
                       Special Instructions to Applicants 
                          
                     
                     
                       Instructions to Applicant regarding references 
                          
                     
                     
                       Is this posting for UofM employees only? 
                         No 
                     
                 
            
       
     
  
     Supplemental Questions 
       
            Required fields are indicated with an asterisk (*).  
 
  
 

       
     Applicant Documents 
       
           Required Documents 
     
     Curriculum Vitae 
     Cover Letter 
     References List 
     Unofficial Transcript 
     Article Reprint 1 
 

 Optional Documents 
     
 



       


 


                  
               
                
             
           
           
             
   
     University of Memphis — Human Resources 
165 Administration Building, Memphis, TN 38152 
901.678.3573  workforce@memphis.edu  
 Follow us on Twitter  
      
      ©University of Memphis. All Rights Reserved. The University of Memphis is an Equal Opportunity/Affirmative Action university.  
   
 

           
         
       
         
 
        
  

  


    
      
    
     
   
 
