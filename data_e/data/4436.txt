Presentations   
 
 
 
 
    
 
 


 
 Presentations 

 
 

 
 

 
 
 
 
		
		
 
 
 
 



 
 
  
 
 
 
 
			

         
 
		




 

 

	
	 
		
		 

				
			 
	
					
					 
						
						   Ryan M. Parish, Ph.D.   						
					  
					
					
	
	 

		
		    Introduction  
  Research  
  Courses  
  Publications  
  Presentations  
  Funded Projects  
  Related Links  
  
		
	  

	
					
					  Assistant Professor of Archaeology :: Department of Earth Sciences :: University of Memphis  	
						
				   Home        Presentations  	
			  
	
				
				
			 
	
				
	
	 

		
		 

			
				
					
					 

						
						 Presentations 
						 
							
							 
 
 
  2017      Investigating Hopewell interaction at the Crib Mound Site through source analysis of chert cache bifaces . 82 nd  Annual Meeting, Society for American Archaeology, Vancouver, British Columbia, Canada, March 29 th    April 2 nd , 2017. 
  Discovering prehistoric behavior through rock source .     Memphis Archaeological Geologic Society Meeting, Memphis, TN, January 13, 2017. 
  2016     A chert type database for the Southeast.  73 rd  Annual Meeting, Southeastern Archaeology Conference, Athens, GA, October 26-29, 2016. 
  Hopewell chert disc caches; discovering interaction through rock source . Old Stone Fort Knap-in, Manchester, TN, October 8, 2016. 
  Chert sourcing using reflectance spectroscopy and the applications toward understanding human behavior from these data sets.  Department of Geosciences, Seminar Series, Murray State University, Murray, KY, September 30 th , 2016. 
  Pleistocene-Early Holocene Adaptations in the Lower Mid-South, United States . 81 st  Annual Meeting, Society for American Archaeology, Orlando, FL, April 6 -10, 2016. 
  2015      Chert research in Tennessee . Jackson Archaeological Society Meeting, Jackson, TN, October 20, 2015. 
  Prehistoric human behavior as viewed through tool-stone resources . Department of Anthropology and Middle Eastern Cultures 2 nd  Annual Lecture Series, Mississippi State University, Starkville, MS, October 16 th , 2015. 
  Chert resources in Tennessee . Old Stone Fort Knap-in, Manchester, TN, October 3, 2015. 
  Chert characterization using Reflectance Spectroscopy . 10 th  Meeting of the International Symposium on Knappable Materials, Barcelona, Spain, September 7-12, 2015. 
  Lithic Procurement Patterning as a Proxy for Identifying Late Paleoindian Group Mobility along the Lower Tennessee River Valley  80 th  Annual Meeting, Society for American Archaeology, San Francisco, CA, April 15-19, 2015. 
  Rocks, Lasers and Prehistory . Arkansas Archaeology Society Meeting, Jonesboro, AR, January 23, 2015. 
  The Parris collection: A life-long dedication to Archaeology . 27 th  Annual Meeting, Current Research in Tennessee Archaeology, Nashville, TN, January 24, 2015. 
  2014       Not Indiana Hornstone; Spectral Source Characterizations of Kentucky and Tennessee Ste. Genevieve and Upper   St. Louis Chert  71 st  Annual Meeting, Southeastern Archaeology Conference, Greenville, SC, November 12-15, With Ellis Durham      
  Provenance of Mississippian Chert Sword-form Bifaces Illustrating Selection Diversity in Inter-regional Resource Procurement . 79 th  Annual Meeting, Society for American Archaeology, Austin, TX, April 23-27, 2014. 
  Sourcing the Sacred: A chert source analysis of Mississippian sword-form bifaces from Tennessee . 26 th  Annual Meeting, Current Research in Tennessee Archaeology, Nashville, TN, January 25, 2014. 
  2013     Quantifying   Regional Variation in Late Paleoindian Assemblages from the Lower Tennessee River Valley Using Geometric Morphometrics and Chert Sourcing  70 th  Annual Meeting, Southeastern Archaeology Conference, Tampa, FL, November 7-9, 2013.     With Adam Finn 
  2012      Testing the ‘single source theory’ for Middle Mississippian Duck River swords  69 th  Annual Meeting, Southeastern Archaeology Conference, Baton Rouge, LA, November 7-10, 2012. 
  Analytical testing of the ‘single source model’ for the Middle Mississippian chert ‘swords’ . 33 rd  Annual Meeting, Mid-South Archaeology Conference, Tunica, MS June 2, 2012. 
  FTIR reflectance spectroscopy analysis of Dover and Ft. Payne chert . 77 th  Annual Meeting, Society for American Archaeology, Memphis, TN, April 18-22, 2012. 
  2011      The problems with visual identification; Dover and Ft. Payne chert.  68 th  Annual Meeting, Southeastern Archaeology Conference, Jacksonville, FL, November 2-5, 2011. 
  Non-destructive chert sourcing using Fourier Transform Infrared spectroscopy . Developing International Geoarchaeology Conference, Knoxville, TN, September 20-24, 2011. 
  Dover chert and the prehistoric quarry sites of Stewart County, Tennessee .     July meeting of the Memphis Archaeological and Geological Society, Memphis, TN, July 8, 2011. 
  Placing Middle Mississippian “Sword” Artifacts and Iconography in an Anthropologic Context . 32 nd  Annual Meeting, Mid-South Archaeology Conference, Memphis, TN, June 25, 2011. 
  Relationships between Procurement Strategies and Geologic Provenience at the Dover Quarry Sites, Tennessee . 76 th  Annual Meeting, Society for American Archaeology, Sacramento, CA, March 30   April 3, 2011. 
  Non-Destructive Provenance Analysis of a Mississippian Sword Fragment from the Link Farm Site .     Poster presented at the 23 rd  Annual Meeting, Current Research in Tennessee Archaeology, Nashville, TN, January 28-29, 2011. 
  2010      Chert Patina Formation and Its Implications for FT-IR Spectroscopic Provenance Studies . 67 th  Annual Meeting, Southeastern Archaeology Conference, Lexington, KY, October 27-30, 2010. 
  Exploring the Application of Fourier Transform Infrared (FT-IR) Spectroscopy to the Detection of Thermal Alteration in Chert . 75 th  Annual Meeting, Society for American Archaeology, St. Louis, MO, April 14-17, 2010. 
  Evidence for Prehistoric Quarrying Activity above Dunbar Cave Montgomery County, Tennessee .     22 nd  Annual Meeting, Current Research in Tennessee Archaeology: Nashville, TN, January 22-23, 2010. With William Lawerence. 
  2009      A New Approach to an Old Problem: The Application of Visible/Near-Infrared Reflectance (VNIR) Spectroscopy to Chert Sourcing . 66 th  Annual Meeting, Southeastern Archaeology Conference: Mobile, Alabama, November 4-7, 2009. 
  The Case for a Paleoindian Component at the Savage Cave Site  (15Lo11). 26 th  Annual Kentucky Heritage Council Meeting: Murray, KY, March 2009. 
  Quantitative and Geologic Descriptions of Four Dover Chert Quarries in Stewart County ,Tennessee. 21 st  Annual Meeting, Current Research in Tennessee Archaeology: Nashville, TN, January 30-31, 2009. 
  The Application of Geographic Information Systems in Prehistoric Quarry Research ; A Case Study of the Dover Chert Quarries, Stewart County Tennessee.     26 th  Annual Kentucky Heritage Council Meeting: Murray, KY, March 2009. 
  2008     Chert Sourcing Investigations Using Visible/Near-Infrared Reflectance Spectroscopy.  Poster presented at the 65 th  Annual Meeting of the Southeastern Archaeological Conference: Charlotte, NC, November 12-15. 
 Chert Sourcing Investigations of the Lower Tennessee and Cumberland River Valleys Using VNIR Spectroscopy. Poster presented at Murray State University’s Scholar’s Week Sigma Xi Poster Competition. 
 
 
 
							
														
						  

						  
						
					  

					
										
					
				
			
		  

		
	  

	
				
	
	 

		
		   Ryan Parish 			   
		        Introduction    Research    Courses    Publications    Presentations    Funded Projects    Related Links      
		
	  

	

				
		  

		
				

		
		 

			
			 
				
				 Copyright © 2017   Ryan M. Parish, Ph.D.   

 Powered by   WordPress   and   Origin   
				
			 

			
		  

				
		  

	  

	
	
        

        







	
 
 