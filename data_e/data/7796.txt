About ePortfolio   
 
 
 
 
 
 About ePortfolio 
 













 
 

      
 
 About ePortfolio 
 
 
 Product help pages 
  ePortfolio  
  Learning Environment  
  LiveRoom  
 
 
 ePortfolio is a personal portfolio tool for storing, organizing, reflecting on and sharing items that represent your learning. You may include documents, graphics, audio files, videos, presentations, course work, etc. that demonstrate your improvement or mastery in a certain area. You decide what items you want to include in your portfolio, how you want to organize them, and who you want to share them with. When you share items with your peers, mentors, potential employers, etc. you may give them permission to view items, edit items, see or add comments and see or add assessments depending on what type of feedback you want. 
 Access ePortfolio 
 Click the  ePortfolio  link on the navigation bar or  My Settings  widget. 
 ePortfolio help topics 
 Learning 
 
  Creating_artifacts  
  Creating reflections  
  Adding tags  
  Creating collections  
  Creating presentations  
  Adding items to presentations  
  Modifying the layout of presentation pages  
  Creating and modifying presentation banners  
  Selecting and modifying presentation themes  
  Managing presentations  
  Allowing comments and rubric assessments  
  Sharing items (setting permissions)  
  Setting up permission profiles  
  Removing permissions  
  Viewing feedback  
  Making revisions  
  Personalizing your dashboard and other settings  
  Viewing, commenting on, assessing and editing others' shared items  
  Submitting items to a dropbox folder for assessment  
  Importing and exporting ePortfolio items  
  Exporting presentations to HTML  
 
 
   
 
 
 
  Desire2Learn Help  
 © 1999-2010 Desire2Learn Incorporated. All rights reserved. 
 

 
 
