Marcus Wicker - Department of English - University of Memphis    










 
 
 
     



 
    
    
    Marcus Wicker - 
      	Department of English
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of English
                           	 
                           
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Undergraduate  
                          Graduate  
                          People  
                          Community  
                          News/Events  
                      
                      
                         
                            People   
                            
                               
                                  
                                   Staff  
                                   African American Literature  
                                   Applied Linguistics  
                                   Creative Writing  
                                   Composition Studies and Professional Writing  
                                   Literary Cultural Studies  
                                   Instructors  
                                   Lambuth Campus  
                                   General Inquiries  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of English
                              	  
                          
                              	People
                              	  
                         Marcus Wicker 
                      
                   
                   
                       
                      
                      
                        
                        	
                        
                        		  
                         
                           
                           
                            
                              
                              
                                                                   
                                 
                                 
                                  
                                 
                                 
                               
                              
                              
                               
                                 
                                  
                                    
                                    Marcus Wicker
                                    
                                  
                                 
                                 
                                  
                                    
                                    Assistant Professor
                                    
                                  
                                 
                                 
                                  
                                    
                                     
                                       
                                        Phone 
                                       
                                        
                                          
                                          901.678.2651
                                          
                                        
                                       
                                     
                                    
                                     
                                       
                                        Email 
                                       
                                         mwicker1@memphis.edu  
                                       
                                     
                                    
                                     
                                       
                                        Fax 
                                       
                                        
                                          
                                          901.678.2226
                                          
                                        
                                       
                                     
                                    
                                  
                                 
                                 
                                  
                                    
                                     
                                       
                                        Office 
                                       
                                        
                                          
                                          Patterson 426
                                          
                                        
                                       
                                     
                                    
                                     
                                       
                                        Office Hours 
                                       
                                        
                                          
                                          By appointment
                                          
                                        
                                       
                                     
                                    
                                  
                                 
                                 
                                  
                                    										
                                    
                                    
                                      Website  
                                                                            
                                    
                                  
                                 
                               
                              
                              
                            
                           			 
                           			  
                           
                           
                            Education 
                           
                            B.A. 2006, Western Michigan University MFA 2010, Indiana University
                            
                           
                            Academic Summary 
                           
                            Marcus Wicker is the recipient of a Ruth Lilly Fellowship from the Poetry Foundation,
                              a Pushcart Prize,  The Missouri Review 's Miller Audio Prize, as well as fellowships from Cave Canem and the Fine Arts Work
                              Center. His first book  Maybe the Saddest Thing  (2012), a National Poetry Series winner, was a finalist for an NAACP Image Award.
                              Wicker's poems have appeared in  The Nation, Poetry, American Poetry Review, Oxford American , and  Boston Review . His most recent collection,  Silencer  (2017), was released from Houghton Mifflin Harcourt. Marcus teaches in the MFA program.
                              He is the poetry editor of  Southern Indiana Review  and co-founding editor at SIR Press.
                            
                           
                            Reviews of  Silencer  
                           
                            "These fiercely lyrical narratives stand in the crosshairs of the political moment."
                              -  starred review in  Publisher's Weekly   
                           
                            "Few books of poetry will disarm readers, render them devastated, then just as easily
                              restore a sense of passion and reverie as this collection by Wicker, a profoundly
                              talented and inimitable author." - Diego Báez,  starred review in  Booklist   
                           
                            "Over and over, Wicker's rage alchemizes into a stunning rhythmic lifeblood that gives
                              pulse to every verse. And yet, woven throughout the collection are gentler lines,
                              too, ones of warmth and of love, that will break your heart twice over." - Caitlin
                              Youngquist,   The Paris Review   
                           
                            Select Publications 
                           
                            
                              
                               " Conjecture on the Stained Glass Image of White Christ at Ebenezer Baptist Church ",  Poetry  
                              
                               " Film Noir at Gallop Park, On the Edge ",  The Nation  
                              
                               " Silencer To The Heart While Jogging Through A Park ",  Oxford American  
                              
                               " Deer Ode, Tangled   Horned ",  Academy of American Poets  
                              
                               " Plea to My Jealous Heart ",  American Poetry Review  
                              
                               " Stakes Is High ",  The Boston Review  
                              
                            
                           
                            Media 
                           
                            
                              
                               "The Punch Like a Kiss on the Collarbone: An Interview with Marcus Wicker by Cate
                                 Lycurgus"   32 Poems   
                              
                               Podcast:  Poetry Foundation Editors Discuss "Conjecture..."   Poetry  
                              
                               2016 Miller Audio Prize Winning Entry,   The Missouri Review   
                              
                               Writer's Bone Podcast:   Silencer  Interview  
                              
                            
                           
                              
                           
                           
                         
                        								
                        
                        
                        
                        
                        	
                      
                      
                   
                
                
                   
                      
                         People 
                         
                            
                               
                                Staff  
                                African American Literature  
                                Applied Linguistics  
                                Creative Writing  
                                Composition Studies and Professional Writing  
                                Literary Cultural Studies  
                                Instructors  
                                Lambuth Campus  
                                General Inquiries  
                            
                         
                      
                      
                      
                         
                            
                                Apply Now  
                                
                            
                            
                                Alumni and Friends  
                                
                            
                            
                                Course Offerings  
                                
                            
                            
                                Contact Us  
                                
                            
                         
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/7/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


