How To Apply - CDMBA - University of Memphis    










 
 
 
     



 
    
    
    How To Apply - 
      	CDMBA
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - Customer-Driven MBA
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Welcome  
                          How to Apply  
                          Pre-Screening  
                          Curriculum  
                          FAQs  
                          FCBE  
                      
                      
                         
                            Admissions   
                            
                               
                                  
                                   Admissions  
                                         How to Apply  
                                         Pre-Screening Form  
                                     
                                  
                                   Curriculum  
                                   Corporate Partners  
                                   Media  
                                   FAQs  
                                   Contact Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	CDMBA
                              	  
                         
                           	Admissions
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      How To Apply 
                     
                      step 1: customer-Driven MBA Pre-Screening Test 
                     
                      Complete the  pre-screening form .
                      
                     
                      Step 2: Supplemental Materials 
                     
                      Submit the following supplemental materials to the Graduate Programs Office in the
                        Fogelman College of Business   Economics:
                      
                     
                      
                        
                         Current Professional Resume 
                        
                         Statement of Interest ( 500 word essay describing career aspirations, educational goals and why you should
                              be chosen for the CD-MBA program )
                         
                        
                         At least two letters of recommendation ( academic or professional contacts )
                         
                        
                      
                     
                      The supplemental materials can be emailed to  Anna Myers  or mailed directly to:
                      
                     
                      
                        
                         Graduate Programs Office Attn: Anna Myers 101 Fogelman College Administration Building University of Memphis Memphis, TN 38152.
                         
                        
                      
                     
                      Step 3: Graduate School Application 
                     
                       Apply to Graduate School . (Application fees for initial admission are $35 application fee for domestic students
                        and $60 for international students). 
                      
                     
                      Step 4: GMAT or GRE Admission Test 
                     
                      If you have already taken the GMAT or GRE, please have your scores sent to Graduate
                        Admissions. If you have not taken the GMAT or GRE please schedule an appointment to
                        take one, and have the results sent to Graduate Admissions.
                      
                     
                       *Please note: If you already have a Master's degree or higher from an accredited United
                           States school, you do not have to take the GMAT or GRE   *For GMAT or GRE score requirements, please visit our Admission Information page.  
                     
                      Locally, you may schedule your GMAT exam at: 
                     
                      
                        
                         The Pearson Professional Testing Center 6060 Poplar Avenue Suite LL01 Memphis, TN 38119 Phone: 901-761-3372
                         
                        
                      
                     
                      Or online at  http://www.mba.com/  
                     
                      Locally, you may schedule your GRE exam at: 
                     
                      
                        
                         The University of Memphis Prometric Center 111C Brister Hall Phone: 901-678-1457
                         
                        
                          or  
                        
                         Prometric Testing Center 8176 Old Dexter Road Suite 101 Memphis, TN 38016 Phone: 901-756-1425 Toll Free: 1 -800-867-1100
                         
                        
                      
                     
                      Or online at  http://www.ets.org/gre/  
                     
                      Have your GMAT or GRE scores sent to: 
                     
                      
                        
                         Graduate Admissions The University of Memphis FedEx Institute of Technology Building, Suite 201 Memphis, TN 38152
                         
                        
                      
                     
                      Step 5: Transcripts 
                     
                      Request that your official transcripts from all other colleges and/or universities
                        you have attended be sent to:
                      
                     
                      
                        
                         Graduate Admissions The University of Memphis FedEx Institute of Technology Building, Suite 201 Memphis, TN 38152
                         
                        
                      
                     
                      
                     
                       *Please note: You must have ALL of these items turned in before we can process your
                           application and send your information to the sponsoring companies.*  
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Admissions 
                         
                            
                               
                                Admissions  
                                      How to Apply  
                                      Pre-Screening Form  
                                  
                               
                                Curriculum  
                                Corporate Partners  
                                Media  
                                FAQs  
                                Contact Us  
                            
                         
                      
                      
                      
                         
                            
                                How To Apply  
                               What's your first step? Find out here! 
                            
                            
                                Corporate Sponsors  
                               Looking to develop the best and brightest in the Memphis area?  Let us help you! 
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


