Majors and Minors - FCBE USSO - University of Memphis    










 
 
 
     



 
    
    
    Majors and Minors - 
      	FCBE USSO
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Fogelman College - Undergraduate Student Services
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          Welcome  
                          Advising  
                          Graduation  
                          Permits  
                          Majors/Minors  
                          Honors  
                          FCBE  
                      
                      
                         
                            Majors   Minors   
                            
                               
                                  
                                   Academics  
                                         Honors Program  
                                         Majors and Minors  
                                         BBA Degree Learning Outcomes  
                                         BBA Accountancy Degree Learning Outcomes  
                                         FCBE Internships  
                                         MOS Exam Information  
                                         CLEP Testing  
                                         Fogelman Professional Development Center  
                                     
                                  
                                   Advising  
                                         How to Get Advised  
                                         Make an Online Appointment  
                                         Join Our Text Messaging Service  
                                         Request Email Advising  
                                         Request Permit  
                                         Advising Handbook  
                                     
                                  
                                   Graduation  
                                   Forms  
                                   Important Dates   Events  
                                   FCBE Scholarships  
                                   Study Abroad Options  
                                   Contact Us  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	FCBE USSO
                              	  
                         
                           	Majors   Minors
                           	
                         
                      
                   
                   
                       
                      
                     
                     		
                     
                     
                      Majors and Minors 
                     
                      
                        
                         Accounting:  2017 ,  2016 ,  2015 ,  2014 ,  2013 ,  2012 ,  2010     
                        
                         Business Economics:  2017 ,  2016 ,  2015 ,  2014 ,  2013 ,  2012 ,  2010  
                        
                         
                           
                            
                              
                               Financial Economics concentration:  2017 ,  2016 ,  2015 ,  2014 ,  2013 ,  2012 ,  2010     
                              
                            
                           
                         
                        
                         Business Information and Technology (formerly MIS):  2017 ,  2016 ,  2015       
                        
                         Finance major with concentration in: 
                           
                            
                              
                               Banking and Financial Services:  2017 ,  2016 ,  2015 ,  2014 ,  2013 ,  2012 ,  2010  
                              
                               Business Finance:  2017 ,  2016 ,  2015 ,  2014 ,  2013 ,  2012 ,  2010  
                              
                               Real Estate:  2017 ,  2016 ,  2015 ,  2014 ,  2013 ,  2012 ,  2010  
                              
                               Risk Management   Insurance:   2014 ,  2013 ,  2012 ,  2010     
                              
                            
                           
                         
                        
                         International Business:  2017 ,  2016 ,  2015 ,  2014 ,  2013 ,  2012 ,  2010     
                        
                         Management:  2017 ,  2016 ,  2015 ,  2014 ,  2013 ,  2012 ,  2010  
                        
                         
                           
                            Human Resources concentration:  2017 ,  2016 ,  2015     
                           
                         
                        
                         Management Information Systems:  
                              2014 ,  2013 ,  2012 ,  2010     
                        
                         Marketing Management:  2017 ,  2016 ,  2015 ,  2014 ,  2013 ,  2012 ,  2010     
                        
                         Supply Chain Management:  2017 ,  2016 ,  2015 ,  2014 ,  2013 ,  2012 ,  2010     
                        
                         FCBE Minors:   2017 ,  2016 ,  2015 ,  2014  
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Majors   Minors 
                         
                            
                               
                                Academics  
                                      Honors Program  
                                      Majors and Minors  
                                      BBA Degree Learning Outcomes  
                                      BBA Accountancy Degree Learning Outcomes  
                                      FCBE Internships  
                                      MOS Exam Information  
                                      CLEP Testing  
                                      Fogelman Professional Development Center  
                                  
                               
                                Advising  
                                      How to Get Advised  
                                      Make an Online Appointment  
                                      Join Our Text Messaging Service  
                                      Request Email Advising  
                                      Request Permit  
                                      Advising Handbook  
                                  
                               
                                Graduation  
                                Forms  
                                Important Dates   Events  
                                FCBE Scholarships  
                                Study Abroad Options  
                                Contact Us  
                            
                         
                      
                      
                      
                         
                            
                                Request Advising  
                               Don't forget to get advised before registering for classes! 
                            
                            
                                Forms  
                               Need a form?  We have you covered! 
                            
                            
                                Contact Us  
                               Do you have questions? Please feel free to contact us! 
                            
                            
                                Return to FCBE  
                                
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 11/3/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


