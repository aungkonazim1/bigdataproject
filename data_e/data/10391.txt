MFA in Sculpture - Department of Art - University of Memphis    










 
 
 
     



 
    
    
    MFA in Sculpture - 
      	Department of Art
      	 - University of Memphis
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
   
   






   
   
   
    
    
 
 
   
   
   
   
 
    
   
   
    
      
      
          
     
    
       
          
             
                
				    
					     
                   
      
                
                
                    
                
                
                   
                      
                         
                            
                               
                                   Lambuth Campus  
                                   myMemphis  
                                   Webmail  
                                   Faculty   Staff  
                                   Contact  
                                   Directories  
                               
                            
                            
                               Search 
                               
							   
							      
						 Site Index 
                            
                             
                         
                      
                   
                   
                      
                         
                            
                                Academics    
                                  
                                      Academics Overview  
                                      Colleges   Schools  
                                      Undergraduate Catalog  
                                      Graduate Catalog  
                                      Honors Program  
								      UofM Global  
                                  
                               
                                Admissions    
                                  
                                      Admissions Information  
                                      Undergraduate Students  
                                      Graduate Students  
                                      Law Students  
                                      International Students  
                                  
                               
                                Athletics    
                                  
                                      Tiger Athletics  
                                      Ticket Information  
                                      Intramurals  
                                      Make A Gift  
                                      Rec Center  
                                      gotigersgo.com  
                                  
                               
                                Research    
                                  
                                      Research   Sponsored Programs  
                                      Research Resources  
                                      Centers/Chairs of Excellence  

                                      Centers and Institutes  
									  FedEx Institute of Technology  
                                      electronic Research Administration (eRA)  
									  Office of Institutional Research  
                                  
                               
                                Support UofM    
                                  
                                      Development  
                                      Make a Gift  
                                      Alumni Association  
                                      Athletics Development  
                                  
                               
                                Libraries    
                                  
                                      University Libraries  
                                      Libraries Resources  
                                      Libraries Services  
                                      Special Collections  
                                      Ask a Librarian  
                                  
                               
                            
                         
                      
                   
                
             
             
                
                   
                      Resources for... 
                      
                          Prospective Students  
                          Current and Returning Students  
                          Parents  
                          Alumni  
                          Veterans  
                      
                       Expand Menu  
                   
				   			   
                
             
          
       
    
          
      
          
    
       
          
             
                
                   
                       
                           			Department of Art
                           	 
                          
                   
                
             
          
       
       
          
             
                
                   
                      
                          About  
                          Programs  
                          People  
                          Facilities  
                          Resources  
                          News/Events  
                      
                      
                         
                            Programs   
                            
                               
                                  
                                   Undergraduate  
                                         Art Education  
                                         Art History  
                                         Foundation Studies  
                                         Graphic Design  
                                         Photography  
                                         Studio Arts  
                                     
                                  
                                   Graduate  
                                        
                                         Art Education  
                                        
                                         Art History  
                                        
                                         Ceramics  
                                        
                                         Graphic Design  
                                        
                                         Painting  
                                        
                                         Photography  
                                        
                                         Printmaking  
                                        
                                         Sculpture  
                                        
                                         Studio Arts  
                                     
                                  
                                   Undergraduate Catalog  
                                         Art  
                                         Art History  
                                     
                                  
                                   Graduate Catalog  
                                   Certificate Program Museum Studies  
                               
                            
                         
                      
                   
                
             
          
          
             
                
                   
                      
                          Home  
                          
                              	Department of Art
                              	  
                          
                              	Programs
                              	  
                         MFA in Sculpture 
                      
                   
                   
                       
                      
                     
                     		
                     
                      
                     
                      MFA in Sculpture 
                     
                      In the 3-D areas, students discover the broad scope of form in space. They explore
                        a variety of means through which ideas and inspiration are conceptualized in sculpture
                        and ceramics. Contemporary trends and evaluations of artistic development are woven
                        into class projects.
                      
                     
                      Assignments challenge creativity as well as provide a basic understanding of the different
                        tools, materials, and techniques which are used to communicate through three-dimensional
                        form. Undergraduates share and interact with graduate students in large studios which
                        are equipped for welding metals, modeling and firing works in clay, casting in permanent
                        materials, and constructing in wood.
                      
                     
                      The horizons of sculpture have grown to include different kinds of art production,
                        resulting in a broadening of aesthetic concepts. In response to contemporary trends,
                        sculpture as an art form is redefining itself to include environments, installations,
                        and new media. Our graduate and undergraduate sculpture programs are designed to encourage
                        the exploration of the broadest possibilities of individual expression as well as
                        the investigation and understanding of contemporary art issues. Students are challenged
                        to excel in their production and in the articulation of concepts and concerns that
                        affect and form their work.
                      
                     
                       ENTRANCE REQUIREMENTS  
                     
                      
                        
                         Baccalaureate degree in chosen concentration or related discipline 
                        
                         Official undergraduate transcripts 
                        
                         Portfolio of 20—30 slides 
                        
                         2 letters of reference 
                        
                         Statement of intent 
                        
                      
                     
                       ASSISTANTSHIP OPPORTUNITIES  
                     
                      Teaching and service assistantships are available to students enrolled in 12 credit
                        hours of coursework.
                      
                     
                       COURSE REQUIREMENTS  
                     
                      
                        
                         36 hours of 7000 level studio courses 
                        
                         9 hours of graduate level art history 
                        
                         9 hours of 6000 or 7000 level electives 
                        
                         6 hours of ART 7996, Thesis 
                        
                      
                     
                     
                     	
                      
                   
                
                
                   
                      
                         Programs 
                         
                            
                               
                                Undergraduate  
                                      Art Education  
                                      Art History  
                                      Foundation Studies  
                                      Graphic Design  
                                      Photography  
                                      Studio Arts  
                                  
                               
                                Graduate  
                                     
                                      Art Education  
                                     
                                      Art History  
                                     
                                      Ceramics  
                                     
                                      Graphic Design  
                                     
                                      Painting  
                                     
                                      Photography  
                                     
                                      Printmaking  
                                     
                                      Sculpture  
                                     
                                      Studio Arts  
                                  
                               
                                Undergraduate Catalog  
                                      Art  
                                      Art History  
                                  
                               
                                Graduate Catalog  
                                Certificate Program Museum Studies  
                            
                         
                      
                      
                      
                         
                            
                                Apply to the UofM  
                               Be part of our artistic community. 
                            
                            
                                Art Appreciation  
                               Visit the Martha and Robert Fogelman Galleries of Contemporary Art 
                            
                            
                                Support the College  
                               Your gift supports future professional artists. 
                            
                            
                                Contact Us  
                               Office hours and location 
                            
                         
                      
                      
                      
                   
                   
                
                
             
             
          
          
       
    
    
    
      
      
       
 
    
       
          
             
                 Full sitemap   
             
             
                
                   
                      Admissions 
                      
                         
                             Prospective Students  
                             Undergraduate  
                             Graduate  
                             Law School  
                             International  
                             Parents  
                             Scholarship   Financial Aid  
                             Tuition   Fee Payment  
                             FAQs  
                             About UofM  
                         
                      
                   
                   
                      Academics 
                      
                         
                             Provost's Office  
                             Libraries  
                             Transcripts  
                             Undergraduate Catalog  
                             Graduate Catalog  
                             Academic Calendars  
                             Course Schedule  
                             Financial Aid  
                             Graduation  
                             Honors Program  
						     eCourseware  
                         
                      
                   
                   
                      Athletics 
                      
                         
                             Gotigersgo.com  
                             Ticket Information  
                             Intramural Sports  
                             Recreation Center  
                             Athletic Academic Support  
                             Former Tigers  
                             Facilities  
                             Tiger Scholarship Fund  
                             Media  
                         
                      
                   
				    
                   
                      Research 
                      
                         
                             Sponsored Programs  
                             Research Resources  
                             Centers   Institutes  
                             Chairs of Excellence  
                             FedEx Institute of Technology  
                             Libraries  
                             Grants Accounting  
                             Environmental Health  
                             Office of Institutional Research  
                         
                      
                   
                   
                      Support UofM 
                      
                         
                             Make a Gift  
                             Alumni Association  
                             Year of Service  
                         
                      
                   
                   
                      Administrative Support 
                      
                         
                             President's Office  
                             Academic Affairs  
                             Business   Finance  
                             Career Opportunities  
                             Conference   Event Services  
                             Corporate Partnerships  
  Development Office  
                             Government Relations  
                             Information Technology Services  
                             Media and Marketing  
                             Student Affairs  
                         
                      
                   
                
             
          
          
             
				    
                  
                
             
             
                   
                  
                
             
             
                
                   Follow UofM Online 
                     UofM Facebook     UofM Twitter     UofM YouTube   
                    
                   
                     UofM Instagram     UofM Pinterest     UofM LinkedIn   
                
             
              
                   
                      
                   
                
      
          
       
    
 
 
      
      
      
       
          
             
                
                   
                      
                         
                            
                               
                                 
                                 
                                  
  Print  
  Got a Question? Ask TOM  
  Copyright © 2017 University of Memphis  
  Important Notice  
                                  
                                 
                                 
                                  
                                     Last Updated: 12/8/17 
                                  
                                 
                                 
                                  
  University of Memphis  
 Memphis, TN 38152 
 Phone: 901.678.2000 
 
  
 The University of Memphis does not discriminate against students, employees, or applicants for admission or employment on the basis of race, color, religion, creed, national origin, sex, sexual orientation, gender identity/expression, disability, age, status as a protected veteran, genetic information, or any other legally protected class with respect to all employment, programs and activities sponsored by the University of Memphis. The Office for Institutional Equity has been designated to handle inquiries regarding non-discrimination policies. For more information, visit  University of Memphis Equal Opportunity and Affirmative Action .  
Title IX of the Education Amendments of 1972 protects people from discrimination based on sex in education programs or activities which receive Federal financial assistance. Title IX states: "No person in the United States shall, on the basis of sex, be excluded from participation in, be denied the benefits of, or be subjected to discrimination under any education program or activity receiving Federal financial assistance..." 20 U.S.C. § 1681 - To Learn More, visit  Title IX and Sexual Misconduct .
 
 
                                 
                                 
                                 
                               
                            
                         
                      
                   
                
             
          
       
    
   
   
   
   
   
   
 



 


